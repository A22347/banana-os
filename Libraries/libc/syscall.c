//
//  syscall.c
//  Banana libc
//
//  Created by Alex Boxall on 12/7/18.
//  Copyright © 2018 Alex Boxall. All rights reserved.
//

#include "syscall.h"
#include <stdint.h>
#include <stddef.h>
#include <stdbool.h>
#include "../../System/core/syscalldef.h"


int SystemCall (uint16_t callno, uint32_t ebx, uint32_t ecx, void* edx)
{
	int res;

	asm volatile (
		"int $96"  :				//assembly
		"=a" (res) :				//output
		"a" (callno), 				//input
		"b" (ebx), 					//input
		"c" (ecx), 					//input
		"d" (edx) :					//input
		"memory", "cc");			//clobbers	

	return res;
}

void __i_system_call_exit(int code) {
	SystemCall (SC_ExitTerminalSession, code, 0, 0);
    SystemCall(SC_Abort, code, 0, 0);
}

void __i_system_call_abort()
{
	__i_system_call_exit(255);
}

int __i_system_call_system(const char* string) {
    return SystemCall(SC_System, 0, 0, (void*) string);
}

char* __i_system_call_getenv(const char* name) {
    return (char*) (size_t) SystemCall(SC_GetEnv, 0, 0, (void*) name);
}

int __i_system_call_unlink(const char* filename) {
    return SystemCall(SC_NewFileDelete, 0, 0, (void*) filename);
}

int __i_system_call_rename(const char* old, const char* new_) {
    return SystemCall(SC_Rename, 0, (size_t) old, (void*) new_);
}

int __i_system_call_openfile(unsigned long long* fdout, const char* filename, unsigned char mode) {
	return SystemCall (SC_OpenFileFromFilename, (size_t) fdout, mode, (void*) filename);
}

int __i_system_call_readfileorpipe(unsigned long long fd, unsigned char* bf, unsigned long long len) {
    return SystemCall (SC_ReadFileOrPipeObjectFromFilename, (int) len, (size_t) &fd, (void*) bf);
}

int __i_system_call_writefileorpipe(unsigned long long fd, const unsigned char* bf, unsigned long long len) {
    return SystemCall(SC_WriteFileOrPipeObjectFromFilename, (int) len, (size_t) &fd, (void*) bf);
}

int __i_system_call_getstream (unsigned long long* output, int type) {
    return SystemCall(SC_GetStdstreamObjectForProcess, type, 0, output);
}

int __i_system_call_malloc_location (size_t* base, size_t* size) {
    return SystemCall(SC_Malloc, 0, (size_t) base, (void*) size);
}

int __i_system_call_time (unsigned long long* timeInSecs, unsigned long long* ticksSinceStart) {
    return SystemCall(SC_Time, 0, (size_t) timeInSecs, (void*) ticksSinceStart);
}

int __i_system_call_tell (unsigned long long* fd, unsigned long long* outputPtr) {
	return SystemCall(SC_Tell, 0, (size_t) outputPtr, (void*) fd);
}

int __i_system_call_seek (unsigned long long* fd, unsigned long long where, int fromWhence) {
	unsigned long long* w = &where;
	return SystemCall(SC_Seek, fromWhence, (size_t) w, (void*) fd);
}

#include "string.h"
#include "stdlib.h"

int __i_system_call_getcwd (int size, char* location)
{
	char* buf = malloc(300);
	memset (buf, 0, 299);
	int res = SystemCall (SC_GetCwd, 0, 0, (void*) buf);
	memset (location, 0, size);
	memcpy (location, buf, size - 1);
	return res;
}

int __i_system_call_setcwd (char* location)
{
	return SystemCall (SC_SetCwd, 0, 0, (void*) location);
}

int __i_system_call_opendir (unsigned long long* fdout, const char* filename)
{
	return SystemCall (SC_OpenDir, (size_t) fdout, 0, (void*) filename);
}

int __i_system_call_readdir (unsigned long long* fd, void* direntObject)
{
	return SystemCall (SC_ReadDir, 0, (size_t) fd, direntObject);
}

int __i_system_call_closedir (unsigned long long* fdout)
{
	return SystemCall (SC_CloseDir, (size_t) fdout, 0, 0);
}

void __i_system_call_execve (char* path, char** argv, char** envr)
{
	SystemCall(SC_Execve, (size_t) (void*) envr, (size_t) (void*) argv, (void*) path);
}

void __i_system_call_wait(int pid)
{
	SystemCall(SC_Wait, pid, 0, 0);
}

int __i_system_call_spawn(char* path)
{
	return SystemCall(SC_Spawn, 0, 0, (void*) path);
}

int __i_system_call_fork()
{
	return SystemCall(SC_Fork, 0, 0, 0);
}

int __i_system_call_pipe(long long unsigned int* a, long long unsigned int* b)
{
	return SystemCall(SC_Pipe, (size_t) a, (size_t) b, 0);
}