//
//  search.c
//  Banana libc
//
//  Created by Alex Boxall on 12/7/18.
//  Copyright © 2018 Alex Boxall. All rights reserved.
//

#include "string.h"
#include "assert.h"

void *memchr(const void *s, int c, size_t n) {
    for (size_t i = 0; i < n; ++i) {
        if (((unsigned char*)s)[i] == c) {
            return (unsigned char*)s + i;
        }
    }
    return 0;
}

char *strchr(const char *s, int c) {
    for (int i = 0; s[i]; ++i) {
        if (s[i] == (char) c) {
            return (char*) s + i;
        }
    }
    return 0;
}

char *strrchr(const char *s, int c) {
    char* result = 0;
    for (int i = 0; s[i]; ++i) {
        if (s[i] == (char) c) {
            result = (char*) s + i;
        }
    }
    return result;
}

size_t strcspn(const char *s1, const char *s2)
{
    size_t count = 0;
    for (; s1[count]; ++count) {
        _Bool found = 0;
        for (size_t i = 0; s2[i]; ++i) {
            if (s2[i] == s1[count]) {
                found = 1;
                break;
            }
        }
        if (found) {
            return count;
        }
    }
    return count;
}

char *strpbrk(const char *s1, const char *s2) {
    size_t count = 0;
    for (; s1[count]; ++count) {
        _Bool found = 0;
        for (size_t i = 0; s2[i]; ++i) {
            if (s2[i] == s1[count]) {
                found = 1;
                break;
            }
        }
        if (found) {
            return (char*) s1 + count;
        }
    }
    return (char*) s1 + count;
}

size_t strspn(const char *s1, const char *s2) {
    size_t count = 0;
    for (; s1[count]; ++count) {
        _Bool found = 0;
        for (size_t i = 0; s2[i]; ++i) {
            if (s2[i] == s1[count]) {
                found = 1;
                break;
            }
        }
        if (!found) {
            return count;
        }
    }
    return count;
}

char *strstr(const char *big, const char *small) {
    for (int i = 0; big[i]; ++i) {
        _Bool failed = 0;
        for (size_t a = 0; small[a]; ++a) {
            if (big[i + a] != small[a]) {
                failed = 1;
                break;
            }
        }
        if (!failed) {
            return (char*) big + i;
        }
    }
    return 0;
}

char *strtok(char * restrict s, const char * restrict delim) {
    static char* last = 0;
    if (s) {
        last = s;
    } else if (!last) {
        return 0;
    }
    s = last + strspn (last, delim);
    last = s + strcspn (s, delim);
    if (last == s) {
        return last = 0;
    }
    if (*last) {
        *last = 0;
        ++last;
    } else {
        last = 0;
    }
    return s;
}
