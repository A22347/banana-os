#include <time.h>
#include <stdlib.h>

/*

typedef struct datetime_t {
    uint8_t day;
    uint8_t month;
    uint16_t year;
    uint8_t hour;
    uint8_t minute;
    uint8_t second;
} datetime_t;

datetime_t currentTime();
datetime_t secondsToDatetime (uint64_t data);
uint64_t datetimeToSeconds (datetime_t d);
*/

clock_t clock() {
    return 0;        //return SystemCall(SC_TicksSinceStart, 0, 0, 0);
}

double difftime (time_t end, time_t beginning) {
    return end - beginning;
}

time_t mktime (tm* timeptr) {
    datetime_t date;
    date.day     = timeptr->tm_mday;        // -1? (tm.mday = 1-31)
    date.month   = timeptr->tm_mon + 1;     // +1? (tm.mon  = 0-11)
    date.year    = timeptr->tm_year + 1900;
    date.hour    = timeptr->tm_hour;
    date.minute  = timeptr->tm_min;
    date.second  = timeptr->tm_sec;
    
    return datetimeToSeconds (date);
}

time_t time (time_t* timer) {
    datetimeToSeconds (currentTime());
}

char* asctime (const tm* timeptr) {
    static const char wday_name[][4] = {
        "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"
    };
    static const char mon_name[][4] = {
        "Jan", "Feb", "Mar", "Apr", "May", "Jun",
        "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
    };
    
    static char result[26];
    sprintf(result, "%.3s %.3s%3d %.2d:%.2d:%.2d %d\n",
        wday_name[timeptr->tm_wday],
        mon_name[timeptr->tm_mon],
        timeptr->tm_mday, timeptr->tm_hour,
        timeptr->tm_min, timeptr->tm_sec,
        1900 + timeptr->tm_year);
    return result;
}

char* ctime (const time_t* timer) {
    tm* t = gmtime(timer);
    return asctime(t);
}

tm* gmtime (const time_t* timer) {
    datetime_t d = secondsToDatetime(&timer);
    tm* t = malloc(sizeof(t));
    
    t->tm_mday = d.day;
    t->tm_mon = d.month - 1;
    t->tm_year = d.year - 1900;
    t->tm_hour = d.hour;
    t->tm_min = d.minute;
    t->tm_sec = d.second;
    
    return t;
}

tm* localtime (const time_t* timer) {
    return gmtime (timer);
}

size_t strftime (char* ptr, size_t maxsize, const char* format, const tm* timeptr) {
    return 0;
}

int clock_gettime (clockid_t clk_id, struct timespec *tp)
{
	static uint64_t l = 55;		//only used to give away unique dummy values (and should be changed to real code later)

	if (!tp) return -1;
	tp->tv_nsec = rand ();
	switch (clk_id) {
	case CLOCK_REALTIME:
		tp->tv_sec = datetimeToSeconds (currentTime ());
		break;
	case CLOCK_MONOTONIC:
		tp->tv_sec = datetimeToSeconds (currentTime ());
		break;
	case CLOCK_PROCESS_CPUTIME_ID:
		tp->tv_sec = l;
		l += rand () % 5;
		break;
	case CLOCK_THREAD_CPUTIME_ID:
		tp->tv_sec = l;
		l += rand () % 7;
		break;
	default:
		break;
	}
	return 0;
}