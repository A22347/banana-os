bits 32

cli
jmp start

extern kernel_early
extern kernel_main_c
extern paging_setup
extern int32
global testa
global _e_systicks
global returnToLongMode
global returnToProtectedMode
global gotoProtectedMode

global GDT64
global GDT64Pointer

global TSS

_e_systicks dd 0

GDT64:                           ; Global Descriptor Table (64-bit).
    .Null: equ $ - GDT64         ; The null descriptor.
    dw 0                         ; Limit (low).
    dw 0                         ; Base (low).
    db 0                         ; Base (middle)
    db 0                         ; Access.
    db 0                         ; Granularity.
    db 0                         ; Base (high).
    .Code: equ $ - GDT64         ; The code descriptor.
    dw 0                         ; Limit (low).
    dw 0                         ; Base (low).
    db 0                         ; Base (middle)
    db 10011010b                 ; Access (exec/read).
    db 00100000b                 ; Granularity.
    db 0                         ; Base (high).
    .Data: equ $ - GDT64         ; The data descriptor.
    dw 0                         ; Limit (low).
    dw 0                         ; Base (low).
    db 0                         ; Base (middle)
    db 10010010b                 ; Access (read/write).
    db 00000000b                 ; Granularity.
    db 0                         ; Base (high).
    
    .UCode: equ $ - GDT64         ; The code descriptor.
    dw 0                         ; Limit (low).
    dw 0                         ; Base (low).
    db 0                         ; Base (middle)
    db 11111010b                 ; Access (exec/read).
    db 00100000b                 ; Granularity.
    db 0                         ; Base (high).
    .UData: equ $ - GDT64         ; The data descriptor.
    dw 0                         ; Limit (low).
    dw 0                         ; Base (low).
    db 0                         ; Base (middle)
    db 11110010b                 ; Access (read/write).
    db 00000000b                 ; Granularity.
    db 0                         ; Base (high).
    
    ;TSS' address as "base", TSS' size as "limit"
    
	; CPU 0
    TSS: equ $ - GDT64          ; The data descriptor.
    dw 0x6A                     ; Limit (low).
    dw 0xB000                         ; Base (low).
    db 0                         ; Base (middle)
    db 10001001b                 ; Access (read/write).
    db 00000000b                 ; Granularity.
    db 0						; Base (high).
	dq 0x0000000000000000


	;CPU 1
	TSS2: equ $ - GDT64          ; The data descriptor.
    dw 0x6A                     ; Limit (low).
    dw 0xB100                         ; Base (low).
    db 0                         ; Base (middle)
    db 10001001b                 ; Access (read/write).
    db 00000000b                 ; Granularity.
    db 0                         ; Base (high).
	dq 0x0000000000000000

	; CPU 2
    TSS3: equ $ - GDT64          ; The data descriptor.
    dw 0x6A                     ; Limit (low).
    dw 0xB200                         ; Base (low).
    db 0                         ; Base (middle)
    db 10001001b                 ; Access (read/write).
    db 00000000b                 ; Granularity.
    db 0						; Base (high).
	dq 0x0000000000000000


	;CPU 3
	TSS4: equ $ - GDT64          ; The data descriptor.
    dw 0x6A                     ; Limit (low).
    dw 0xB300                         ; Base (low).
    db 0                         ; Base (middle)
    db 10001001b                 ; Access (read/write).
    db 00000000b                 ; Granularity.
    db 0                         ; Base (high).
	dq 0x0000000000000000

	.Code32: equ $ - GDT64         ; The code descriptor.
    dw 0xFFFF                    ; Limit (low).
    dw 0                         ; Base (low).
    db 0                         ; Base (middle)
    db 0x9A                      ; Access (exec/read).
    db 0xCF                      ; Granularity.
    db 0                         ; Base (high).
    .Data32: equ $ - GDT64         ; The data descriptor.
    dw 0xFFFF                    ; Limit (low).
    dw 0                         ; Base (low).
    db 0                         ; Base (middle)
    db 0x92                      ; Access (read/write).
    db 0xCF                      ; Granularity.
    db 0                         ; Base (high).


GDT64Pointer:                    ; The GDT-pointer.
    dw $ - GDT64 - 1             ; Limit.
    dq GDT64                     ; Base.
    
point1 dd 0xA000
point2 dd 0x1000000
point3 dd 0x1000007
point4 dd 0x1400000
point5 dd 0x1400007
point6 dd 0x1800000
point7 dd 0x1800007

;0xA000 -> 0x2000000 -> 0x2400000 -> 0x2800000 -> DATA
paging_setupx:
    mov eax, 0
    mov ebx, [point3]    ;remember you have to ' | 3'
    mov ecx, [point1]
    
    ;lines 16 - 18 of C code
    lp1:
        mov [ecx], ebx
        mov [ecx + 4], dword 0

        add ebx, 8
        add ecx, 8
        inc eax
        cmp eax, 512
        je ed1
        jmp lp1
        
    ed1:
    mov eax, 0
    mov ebx, [point5]
    mov ecx, [point2]	
    mov edx, [point7]
    
    lp2:
        mov [ecx], ebx
        mov [ecx + 4], dword 0
        
        mov edi, 0
        in1:
            mov [ebx - 7], edx
            mov [ebx - 3], dword 0
            add ebx, 8
            add edx, 4096
            
            inc edi
            cmp edi, 512
            je ou1
            jmp in1
            
        ou1:
        add ecx, 8
        inc eax
        cmp eax, 512
        je ed2
        jmp lp2
    
    ed2:
    mov eax, 0
    mov ebx, 7
    mov ecx, [point6]
    mov edx, 0
    
    lp3:
        mov [ecx], ebx
        mov [ecx + 4], edx

        add ebx, 4096
        add ecx, 8

        inc eax
        
        cmp eax, 262144 * 4
        je ed3
        
        cmp ebx, 7
        je fx3

        jmp lp3
        
    fx3:
        inc edx
        jmp lp3
    
    ed3:
    ret
	 

toput dd 0

start:
    cli
    mov	ax, 0x10		; set data segments to data selector (0x10)
    mov	ds, ax
    mov	ss, ax
    mov	es, ax
    mov	esp, 0x0800000 ;0 x4C00000  ;0x9FFFF0

	mov eax, 0
	mov al, byte [0x7D01]	;CPU ID
	shl eax, 20				;1 MB
	sub esp, eax			;CPU 0 = 0x500..., CPU 1 = 0x4C0..., CPU 2 = 0x480..., CPU 3 = 0x440...
	;MAXIMUM OF 4 CPUs

    mov ebp, esp

	cmp byte [0x7D01], 0		;only boot CPU 0 (the bootloader sets this byte correctly)
	jne skipLoadingGDTPointer

	mov [0x7D04], dword GDT64Pointer
    
skipLoadingGDTPointer:
	cmp byte [0x7D01], 0
	je cpu0paging

	jmp dopaging

cpu0paging:
	mov [point1], dword 0xA000
	mov [point2], dword 0x1000000
	mov [point3], dword 0x1000007
	mov [point4], dword 0x1400000
	mov [point5], dword 0x1400007
	mov [point6], dword 0x1800000
	mov [point7], dword 0x1800007

	mov [toput], dword 0xA000

dopaging:
    call paging_setupx

	mov eax, [toput]
    mov cr3, eax

    
    ;turn on PAE
    mov eax, cr4
    or eax, 1 << 5
    mov cr4, eax
    
    mov ecx, 0xC0000080
    rdmsr
    or eax, 1 << 8			    ;set the long mode bit
	;or eax, 1 << 11				;NX bit enable
    wrmsr
    
    ;enable paging
    mov eax, cr0
    or eax, 1 << 31
    mov cr0, eax
    
    lgdt [GDT64Pointer]
    jmp GDT64.Code:Full64BitMode
    
    
bits 64
align 0x200
Full64BitMode:    
    cli

    mov ax, GDT64.Data
    mov ds, ax
    mov es, ax
    mov fs, ax
    mov gs, ax
    mov ss, ax

	cmp byte [0x7D01], 0		;only boot CPU 0 (the bootloader sets this byte correctly)
	jne secondCPULoop
    
	;although not neccessary, 
	;allow the kernel to write to readonly pages
	;(it is enabled by default, but just in case)
	;this means the kernel can write but apps can't
	mov rax, cr0
	and rax, ~(1 << 16)
	mov cr0, rax

	;mov rax, cr4
	;bit 11 crashes hardware when set,
	;move this stuff into C (using inline assembler)
	;so we can detect UMIP using CPUID (it's only avaliable on newer CPUs, input EAX=7 ECX=0, output ECX bit 2)
	;also, we should check CPUID for TSC as well. (input EAX=1, output EDX bit 4)

	;or rax, 1 << 2			;disable RDTSC in usermode
	;or rax, 1 << 11		;UMIP enable. this disables SGDT, SIDT, SLDT, SMSW and STR in usermode
	;mov cr4, rax

    call kernel_early
    call kernel_main_c

EndlessLoop:
    cli
    hlt
    jmp EndlessLoop

    
extern doCoolStuffWithCPU2
secondCPULoop:
	call doCoolStuffWithCPU2
	jmp $