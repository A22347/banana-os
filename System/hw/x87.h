#pragma once

#ifdef __cplusplus
extern "C" {
#endif

	int x87Avaliable ();
	void x87Open ();
	void x87Read (void* ptr);
	void x87Write (void* ptr);

#ifdef __cplusplus
}
#endif