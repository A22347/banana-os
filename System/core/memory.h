#pragma once

#include <core/kernel.h>
#include <core/string.h>

#define BLOCK_SIZE 128
#define MEMORY (MAX_KMALLOC_DATA_LOCATION - KMALLOC_DATA_LOCATION)
#define BLOCKS (MEMORY / BLOCK_SIZE)

void _free (void* pointer);
void* _malloc (size_t bytes);
void* taskdatamalloc (size_t);

bool validMemory (uint8_t* address);
void* malloc (size_t size);
void* calloc (size_t size, size_t block);
void free (void *blk);
void freee (void *blk);
void heapInit ();
void* realloc (void *ptr, size_t size);