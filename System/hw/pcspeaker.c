#include <hw/pcspeaker.h>
#include <hw/ports.h>

void openPCSpeaker (Device* self, int a, int b, void* c)
{

}

void writePCSpeaker (Device* self, int a, int b, void* c)
{
	if (a) {
		startPCSpeaker (a);
	} else {
		stopPCSpeaker ();
	}
}

void startPCSpeaker (uint32_t hertz)
{
	uint32_t Div = 0;
	uint8_t tmp = 0;

	//Set the PIT to the desired frequency
	Div = 1193180 / hertz;
	outb (0x43, 0xb6);
	outb (0x42, (uint8_t) (Div));
	outb (0x42, (uint8_t) (Div >> 8));

	//And play the sound using the PC speaker
	tmp = inb (0x61);
	if (tmp != (tmp | 3)) {
		outb (0x61, tmp | 3);
	}
}

void stopPCSpeaker ()
{
	uint8_t tmp = inb (0x61) & 0xFC;
	outb (0x61, tmp);
}
