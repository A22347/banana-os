//
//  conversion.c
//  Banana libc
//
//  Created by Alex Boxall on 12/7/18.
//  Copyright © 2018 Alex Boxall. All rights reserved.
//

#include "stdlib.h"
#include "errno.h"
#include "ctype.h"
#include "string.h"
#include "math.h"

double atof(const char *nptr) {
    int prevErrno = errno;
    double res = strtod(nptr, (char **)NULL);
    errno = prevErrno;
    return res;
}

int atoi(const char *nptr) {
    int prevErrno = errno;
    int res = (int)strtol(nptr, (char **)NULL, 10);
    errno = prevErrno;
    return res;
}

long int atol(const char *nptr) {
    int prevErrno = errno;
    long int res = strtol(nptr, (char **)NULL, 10);
    errno = prevErrno;
    return res;
}

long long int atoll(const char *nptr) {
    int prevErrno = errno;
    long long int res = strtoll(nptr, (char **)NULL, 10);
    errno = prevErrno;
    return res;
}

double strtod(const char * restrict nptr, char ** restrict endptr) {
    double output = 0;
    int i = 0;
    _Bool negative = 0;
    
    //skip over whitespace
    while (isspace(nptr[i])) ++i;
    
    //the sign comes next
    if (nptr[i] == '-') {
        negative = 1;
        ++i;
    } else if (nptr[i] == '+') ++i;
    
    if (nptr[i] == '0' && tolower(nptr[i+1]) == 'x') {
        i += 2;
        //hexadecimal floats...
        
    } else if (!strncasecmp(nptr+i, "NAN", 3)) {
        output = NAN;
        negative = 0;
        
    } else if (!strncasecmp(nptr+i, "INF", 3) || \
               !strncasecmp(nptr+i, "INFINITY", 3)) {
        output = INFINITY;
        //negative infinities?
        //negative = 0;
        
    } else {
        _Bool dpEncounted = 0;
        int timesPast = 1;
        while (isdigit(nptr[i]) || (nptr[i] == '.' && !dpEncounted)) {
            if (nptr[i] == '.') {
                dpEncounted = 1;
                ++nptr;
                continue;
            }
            output *= 10;
            output += nptr[i] - '0';
            
            //if it's past the decimal point, multiply the a counter by 10 to generate
            //a power of ten at the end so we can divide output by it
            if (dpEncounted) {
                timesPast *= 10;
            }
            
            ++nptr;
        }
        
        output /= timesPast;
    }
    
    if (endptr != NULL) {
        *endptr = malloc(strlen(nptr) - i + 1);
        strcpy(*endptr, nptr + i);
    }
    
    return negative ? -output : output;
}

float strtof(const char * restrict nptr, char ** restrict endptr) {
    float output = 0;
    int i = 0;
    _Bool negative = 0;
    
    //skip over whitespace
    while (isspace(nptr[i])) ++i;
    
    //the sign comes next
    if (nptr[i] == '-') {
        negative = 1;
        ++i;
    } else if (nptr[i] == '+') ++i;
    
    if (nptr[i] == '0' && tolower(nptr[i+1]) == 'x') {
        i += 2;
        //hexadecimal floats...
        
    } else if (!strncmp(nptr+i, "NAN", 3) || !strncmp(nptr+i, "nan", 3)) {
        output = NAN;
        negative = 0;
        
    } else if (!strncmp(nptr+i, "INF", 3) || !strncmp(nptr+i, "inf", 3) || \
               !strncmp(nptr+i, "INFINITY", 3) || !strncmp(nptr+i, "infinity", 3)) {
        output = INFINITY;
        //negative infinities?
        //negative = 0;
        
    } else {
        _Bool dpEncounted = 0;
        int timesPast = 1;
        while (isdigit(nptr[i]) || (nptr[i] == '.' && !dpEncounted)) {
            if (nptr[i] == '.') {
                dpEncounted = 1;
                ++nptr;
                continue;
            }
            output *= 10;
            output += nptr[i] - '0';
            
            //if it's past the decimal point, multiply the a counter by 10 to generate
            //a power of ten at the end so we can divide output by it
            if (dpEncounted) {
                timesPast *= 10;
            }
            
            ++nptr;
        }
        
        output /= timesPast;
    }
    
    if (endptr != NULL) {
        *endptr = malloc(strlen(nptr) - i + 1);
        strcpy(*endptr, nptr + i);
    }
    
    return negative ? -output : output;
}

long double strtold(const char * restrict nptr, char ** restrict endptr) {
    long double output = 0;
    int i = 0;
    _Bool negative = 0;
    
    //skip over whitespace
    while (isspace(nptr[i])) ++i;
    
    //the sign comes next
    if (nptr[i] == '-') {
        negative = 1;
        ++i;
    } else if (nptr[i] == '+') ++i;
    
    if (nptr[i] == '0' && tolower(nptr[i+1]) == 'x') {
        i += 2;
        //hexadecimal floats...
        
    } else if (!strncmp(nptr+i, "NAN", 3) || !strncmp(nptr+i, "nan", 3)) {
        output = NAN;
        negative = 0;
        
    } else if (!strncmp(nptr+i, "INF", 3) || !strncmp(nptr+i, "inf", 3) || \
               !strncmp(nptr+i, "INFINITY", 3) || !strncmp(nptr+i, "infinity", 3)) {
        output = INFINITY;
        //negative infinities?
        //negative = 0;
        
    } else {
        _Bool dpEncounted = 0;
        int timesPast = 1;
        while (isdigit(nptr[i]) || (nptr[i] == '.' && !dpEncounted)) {
            if (nptr[i] == '.') {
                dpEncounted = 1;
                ++nptr;
                continue;
            }
            /*if (output / 10 >= ) {
                errno = ERANGE;
                return HUGE_VALL;
            }*/
            output *= 10;
            output += nptr[i] - '0';
            
            //if it's past the decimal point, multiply the a counter by 10 to generate
            //a power of ten at the end so we can divide output by it
            if (dpEncounted) {
                timesPast *= 10;
            }
            
            ++nptr;
        }
        
        output /= timesPast;
    }
    
    if (endptr != NULL) {
        *endptr = malloc(strlen(nptr) - i + 1);
        strcpy(*endptr, nptr + i);
    }
    
    return negative ? -output : output;
}

//NEW!!!
long int strtol(const char * restrict nptr, char ** restrict endptr, int base) {
    long int output = 0;
    int i = 0;
    _Bool negative = 0;
    
    //skip over whitespace
    while (isspace(nptr[i])) ++i;
    
    //the sign comes next
    if (nptr[i] == '-') {
        negative = 1;
        ++i;
    } else if (nptr[i] == '+') ++i;
    
    //the standard says so
    if (nptr[i] == '0' && tolower(nptr[i+1]) == 'x' && base == 16) {
        i += 2;
    }
    
    int timesPast = 1;
    while (isdigit(nptr[i]) || nptr[i] == '.') {
        output *= base;
        
        if (nptr[i] >= '0' && nptr[i] <= '9') {
            output += nptr[i] - '0';
        } else if (nptr[i] >= 'A' && nptr[i] <= 'Z') {
            output += nptr[i] - 'A' + 10;
        } else if (nptr[i] >= 'a' && nptr[i] <= 'z') {
            output += nptr[i] - 'a' + 10;
        }
        
        ++nptr;
    }
    
    output /= timesPast;
    
    if (endptr != NULL) {
        *endptr = malloc(strlen(nptr) - i + 1);
        strcpy(*endptr, nptr + i);
    }
    
    return negative ? -output : output;
}

long long int strtoll(const char * restrict nptr, char ** restrict endptr, int base) {
    long long int output = 0;
    int i = 0;
    _Bool negative = 0;
    
    //skip over whitespace
    while (isspace(nptr[i])) ++i;
    
    //the sign comes next
    if (nptr[i] == '-') {
        negative = 1;
        ++i;
    } else if (nptr[i] == '+') ++i;
    
    //the standard says so
    if (nptr[i] == '0' && tolower(nptr[i+1]) == 'x' && base == 16) {
        i += 2;
    }
    
    int timesPast = 1;
    while (isdigit(nptr[i]) || nptr[i] == '.') {
        output *= base;
        
        if (nptr[i] >= '0' && nptr[i] <= '9') {
            output += nptr[i] - '0';
        } else if (nptr[i] >= 'A' && nptr[i] <= 'Z') {
            output += nptr[i] - 'A' + 10;
        } else if (nptr[i] >= 'a' && nptr[i] <= 'z') {
            output += nptr[i] - 'a' + 10;
        }
        
        ++nptr;
    }
    
    output /= timesPast;
    
    if (endptr != NULL) {
        *endptr = malloc(strlen(nptr) - i + 1);
        strcpy(*endptr, nptr + i);
    }
    
    return negative ? -output : output;
}

unsigned long int strtoul(const char * restrict nptr, char ** restrict endptr, int base) {
    unsigned long int output = 0;
    int i = 0;
    _Bool negative = 0;
    
    //skip over whitespace
    while (isspace(nptr[i])) ++i;
    
    //the sign comes next
    if (nptr[i] == '-') {
        negative = 1;
        ++i;
    } else if (nptr[i] == '+') ++i;
    
    //the standard says so
    if (nptr[i] == '0' && tolower(nptr[i+1]) == 'x' && base == 16) {
        i += 2;
    }
    
    int timesPast = 1;
    while (isdigit(nptr[i]) || nptr[i] == '.') {
        output *= base;
        
        if (nptr[i] >= '0' && nptr[i] <= '9') {
            output += nptr[i] - '0';
        } else if (nptr[i] >= 'A' && nptr[i] <= 'Z') {
            output += nptr[i] - 'A' + 10;
        } else if (nptr[i] >= 'a' && nptr[i] <= 'z') {
            output += nptr[i] - 'a' + 10;
        }
        
        ++nptr;
    }
    
    output /= timesPast;
    
    if (endptr != NULL) {
        *endptr = malloc(strlen(nptr) - i + 1);
        strcpy(*endptr, nptr + i);
    }
    
    return negative ? -output : output;
}

unsigned long long int strtoull(const char * restrict nptr, char ** restrict endptr, int base) {
    unsigned long long int output = 0;
    int i = 0;
    _Bool negative = 0;
    
    //skip over whitespace
    while (isspace(nptr[i])) ++i;
    
    //the sign comes next
    if (nptr[i] == '-') {
        negative = 1;
        ++i;
    } else if (nptr[i] == '+') ++i;
    
    //the standard says so
    if (nptr[i] == '0' && tolower(nptr[i+1]) == 'x' && base == 16) {
        i += 2;
    }
    
    int timesPast = 1;
    while (isdigit(nptr[i]) || nptr[i] == '.') {
        output *= base;
        
        if (nptr[i] >= '0' && nptr[i] <= '9') {
            output += nptr[i] - '0';
        } else if (nptr[i] >= 'A' && nptr[i] <= 'Z') {
            output += nptr[i] - 'A' + 10;
        } else if (nptr[i] >= 'a' && nptr[i] <= 'z') {
            output += nptr[i] - 'a' + 10;
        }
        
        ++nptr;
    }
    
    output /= timesPast;
    
    if (endptr != NULL) {
        *endptr = malloc(strlen(nptr) - i + 1);
        strcpy(*endptr, nptr + i);
    }
    
    return negative ? -output : output;
}
