#include <hw/ps2key.h>
#include <hw/ps2.h>
#include <hw/ports.h>
#include <debug/log.h>
#include <hal/interrupts.h>

bool extended = false;
bool ignoreNext = false;
bool nextIsARelease = false;
bool caps = 0;
bool ctrl = 0;
bool capslk = 0;
bool alt = 0;
bool altR = 0;

uint8_t internalMapperLower[256] = "              `      q1   zsaw2  cxde43   vftr5  nbhgy6   mju78  ,kio09  ./l;p-   ' [=     ] \\           1 47   0.2568   +3-*9             -";
uint8_t internalMapperUpper[256] = "              ~      Q!   ZSAW@  CXDE$#   VFTR%  NBHGY^   MJU&*  <KIO)(  >?L:P_   \" {+     } |           1 47   0.2568   +3-*9              ";
uint8_t internalMapCapLower[256] = "              `      Q1   ZSAW2  CXDE43   VFTR5  NBHGY6   MJU78  ,KIO09  ./L;P-   ' [=     ] \\           1 47   0.2568   +3-*9             -";
uint8_t internalMapCapUpper[256] = "              ~      Q!   zsaw@  cxde$#   vftr%  nbhgy^   mju&*  <kio)(  >?l:p_   \" {+     } |           1 47   0.2568   +3-*9              ";

void updateKeyboardLEDs ()
{
	uint8_t v = capslk << 2;
	v |= scrollLock << 1;
	v |= numberLock;

	writePS2Device(0xED, false);
	waitForACK();
	writePS2Device(v, false);
	waitForACK();
}

void updateRepeatRates ()
{
	uint8_t send = (repeatRate & 0b11111) | ((repeatDelay & 0b11) << 5);

	writePS2Device(0xF3, false);
	waitForACK();
	writePS2Device(send, false);
	waitForACK();
}

/*
\b is backspace	(previously it was \1)
\t is tab			(previously it was \2)
(char)3 is left
(char)4 is right
(char)5 is up
(char)6 is down
7 = \a
8 = backspace = \b
*/

//grabs key then converts it into a keyboard and keymap independent scancode
void ps2_keyboard_handler(struct regs* r)
{

	uint8_t status = inb(0x64);
	if ((status & 1) == 0) return;	//no data
	if ((status & 0x20)) return;	//mouse data

	logk("TODO: timeSinceLastInteraction ps2key");
	//timeSinceLastInteraction = 0;

	uint8_t c = inb(0x60);

	if (c == PS2_SCANCODE_extension) {
		extended = true;
		return;
	}
	if (c == PS2_SCANCODE_breakcode) {
		nextIsARelease = true;
		return;
	}

	switch (c) {
	case 0x14:
		ctrl = nextIsARelease ^ 1;
		sendKey(KEYCODE_CTRL, nextIsARelease);
		nextIsARelease = false;
		extended = false;
		return;

	case 0x12:                    //SHIFT
		caps = nextIsARelease ^ 1;
		sendKey(KEYCODE_SHIFT, nextIsARelease);
		nextIsARelease = false;
		extended = false;
		return;

	case 0x11:
		altR = nextIsARelease ^ 1;
		alt = nextIsARelease ^ 1;
		sendKey(KEYCODE_ALT, nextIsARelease);
		nextIsARelease = false;
		extended = false;
		return;
	}


	if (extended) {
		switch (c) {
		case 0x75:		//Up
			sendKey(KEYCODE_UP, nextIsARelease);
			break;

		case 0x72:		//Down
			sendKey(KEYCODE_DOWN, nextIsARelease);
			break;

		case 0x6B:		//Left
			sendKey(KEYCODE_LEFT, nextIsARelease);
			break;

		case 0x74:		//Right
			sendKey(KEYCODE_RIGHT, nextIsARelease);
			break;
		}
		nextIsARelease = false;
		extended = false;
		return;
	}


	if (c == PS2_SCANCODE_backspace) {
		sendKey(KEYCODE_BACKSPACE, nextIsARelease);

	} else if (c == PS2_SCANCODE_tab) {        //tab
		sendKey(KEYCODE_TAB, nextIsARelease);

	} else if (c == PS2_SCANCODE_capslock) {       //CAPS
		if (!nextIsARelease) {		//only toggle on press, not release
			capslk ^= 1;
			updateKeyboardLEDs();
		}
		sendKey(KEYCODE_CAPSLOCK, nextIsARelease);

	} else if (c == PS2_SCANCODE_numberlock) {
		if (!nextIsARelease) {		//only toggle on press, not release
			numberLock ^= 1;
			updateKeyboardLEDs();
		}
		sendKey(KEYCODE_NUMBERLOCK, nextIsARelease);

	} else if (c == PS2_SCANCODE_scrolllock) {
		if (!nextIsARelease) {		//only toggle on press, not release
			scrollLock ^= 1;
			updateKeyboardLEDs();
		}
		sendKey(KEYCODE_SCROLLLOCK, nextIsARelease);

	} else if (c == PS2_SCANCODE_f1) {
		sendKey(KEYCODE_F1, nextIsARelease);

	} else if (c == PS2_SCANCODE_f2) {
		sendKey(KEYCODE_F2, nextIsARelease);

	} else if (c == PS2_SCANCODE_f3) {
		sendKey(KEYCODE_F3, nextIsARelease);

	} else if (c == PS2_SCANCODE_f4) {
		sendKey(KEYCODE_F4, nextIsARelease);

	} else if (c == PS2_SCANCODE_f5) {
		sendKey(KEYCODE_F5, nextIsARelease);

	} else if (c == PS2_SCANCODE_f6) {
		sendKey(KEYCODE_F6, nextIsARelease);

	} else if (c == PS2_SCANCODE_f7) {
		sendKey(KEYCODE_F7, nextIsARelease);

	} else if (c == PS2_SCANCODE_f8) {
		sendKey(KEYCODE_F8, nextIsARelease);

	} else if (c == PS2_SCANCODE_f9) {
		sendKey(KEYCODE_F9, nextIsARelease);

	} else if (c == PS2_SCANCODE_f10) {
		sendKey(KEYCODE_F10, nextIsARelease);

	} else if (c == PS2_SCANCODE_f11) {
		sendKey(KEYCODE_F11, nextIsARelease);

	} else if (c == PS2_SCANCODE_f12) {
		sendKey(KEYCODE_F12, nextIsARelease);

	} else if (c == PS2_SCANCODE_newline) {
		logk("Newline scancode!\n");
		sendKey(KEYCODE_NEWLINE, nextIsARelease);

	} else if (c == 0xF0) {		//if a release scancode slips through somehow, catch it here
		logk("CAUGHT AN 0xF0 in the handler somehow!!\n");

	} else {
		logk("scancode = 0x%X, upper = %c, lower = %c\n", c, internalMapperUpper[c], internalMapperLower[c]);

		sendKey(caps && capslk ? internalMapCapUpper[c] :
				caps && !capslk ? internalMapperUpper[c] :
				!caps && capslk ? internalMapCapLower[c] :
				internalMapperLower[c], nextIsARelease);
	}

	nextIsARelease = false;
	extended = false;

	logk("keys were sent!\n");
}

void openPS2Keyboard(struct Device* self, int a, int b, void* c)
{
	updateKeyboardLEDs();
	updateRepeatRates();

	//we're wanting scancode set 2
	writePS2Device(0xF0, false);
	waitForACK();
	writePS2Device(2, false);

	installHardwareInterruptHandler(legacyIRQRemaps[1], ps2_keyboard_handler);

	writePS2Device(PS2_DEVICE_ENABLE_SCANNING, false);
}