//
//  main.c
//  iso9660
//
//  Created by Alex Boxall on 23/8/18.
//  Copyright � 2018 Alex Boxall. All rights reserved.
//

//THE OUTPUT MUST BE SMALLER THAN 6KB (6144 bytes)

#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>

void* __malloc (int size)
{
	static int lastMalloc = 0x400000;
	size /= 8;
	size *= 8;
	size += 64;

	int a = lastMalloc;
	lastMalloc += size;
	return a;
}

void* __memset (void *b, int c, int len)
{
	unsigned char *p = b;
	while (len > 0) {
		*p = c;
		p++;
		len--;
	}
	return b;
}

void* __memcpy (void* destination, const void* source, size_t num)
{
	char* dst8 = (char*) destination;
	char* src8 = (char*) source;

	while (num--) {
		*dst8++ = *src8++;
	}
	return destination;
}

int __strlen (char* string)
{
	int a = 0;
	for (; string[a]; ++a);
	return a;
}

uint8_t* __memmem (uint8_t* big, int bigLen, uint8_t* small, int smallLen)
{
	//may have an 'off by one' error, but this won't matter, as all
	//names end in ';1'
	for (int i = 0; i < bigLen - smallLen; ++i) {
		bool yes = true;
		for (int j = 0; j < smallLen; ++j) {
			if (small[j] != big[i + j]) {
				yes = false;
				break;
			}
		}
		if (yes) {
			return big + i;
		}
	}
	return 0;
}

typedef struct __attribute__ ((packed))
{
	unsigned short di, si, bp, sp, bx, dx, cx, ax;
	unsigned short gs, fs, es, ds, eflags;

} regs16_t;

// tell compiler our int32 function is external
extern void int32 (unsigned char intnum, regs16_t *regs);

/*extern uint16_t blkcnt;
extern uint16_t db_add;
extern uint16_t db_seg;
extern uint32_t db_lba;
extern void* DAPACK;
extern uint8_t driveNumber;
*/
/*
; THIS MUST BE IN THE FIRST 2048 BYTES!
DAPACK:
db	0x10
db	0
blkcnt : dw	4; int 13 resets this to # of blocks actually read / written
	db_add : dw	0x8400; memory buffer destination address (7C0:800)
	db_seg:	dw	0; in memory page zero
	d_lba : dd	0x1A + 1; put the lba to read in this spot
	dd	0; more storage bytes only for big lbas (> 4 bytes)
	DAEND:*/

void readSectorFromCDROM (uint32_t sector, uint8_t* data)
{
	/*uint8_t* ptr = 0x5200;
	*ptr++ = 0x10;		//?
	*ptr++ = 0x0;		//?

	*ptr++ = 0x1;		//count
	*ptr++ = 0x0;		//count

	*ptr++ = 0x0;		//offset
	*ptr++ = 0x70;		//offset
	*ptr++ = 0x0;		//segment
	*ptr++ = 0x0;		//segment

	uint32_t* ptr32 = (uint32_t*) ptr;
	*ptr32++ = sector;		//lba
	*ptr32++ = 0;			//lba

	ptr = (uint8_t*) 0x5000;

	//send off the packet
	regs16_t regs;
	regs.ax = 0x4200;		//ah = 0x42
	regs.es = 0;			//packet is in segment 0
	regs.si = 0x5200;		//packet is at 0x5200
	regs.dx = *ptr;			//drive number
	int32 (0x13, &regs);

	while (1);*/

	//copy to correct location
	__memcpy (data, 0x80000 + (sector - 16) * 2048, 2048);
}

bool readRoot (uint32_t* lbaOut, uint32_t* lenOut)
{
	uint8_t sector[2048];
	readSectorFromCDROM (16, sector);
	uint8_t root[34];
	__memcpy (root, sector + 156, 34);

	*lbaOut = root[2] | (root[3] << 8) | (root[4] << 16) | (root[5] << 24);
	*lenOut = root[10] | (root[11] << 8) | (root[12] << 16) | (root[13] << 24);

	return true;
}

bool readRecursively (char* filename, uint32_t startSec, uint32_t startLen, \
					  uint32_t* lbaOut, uint32_t* lenOut)
{

	char firstPart[256];
	__memset (firstPart, 0, 256);
	bool dir = false;
	for (int i = 0; filename[i]; ++i) {
		if (filename[i] == '/') {
			filename += i + 1;
			dir = true;
			break;
		}
		firstPart[i] = filename[i];
	}

	uint32_t newLba, newLen;
	uint8_t* data = __malloc (startLen);

	for (int i = 0; i < (startLen + 2047) / 2048; ++i) {
		readSectorFromCDROM (startSec + i, data + i * 2048);
	}

	uint8_t* o = __memmem (data, startLen, firstPart, __strlen (firstPart));
	if (o == 0) {
		return false;
	}
	o -= 33;                    //33 = filename start, 2 = lba start, 31 = difference

	newLba = o[2] | (o[3] << 8) | (o[4] << 16) | (o[5] << 24);
	newLen = o[10] | (o[11] << 8) | (o[12] << 16) | (o[13] << 24);

	if (dir) {
		return readRecursively (filename, newLba, newLen, lbaOut, lenOut);
	} else {
		*lbaOut = newLba;      //data
		*lenOut = newLen;      //data
		return true;
	}

	return false;
}

bool getFileData (char* filename, uint32_t* lbaOut, uint32_t* lenOut)
{
	uint32_t lba = 0, len = 0;
	*lbaOut = -1;
	*lenOut = -1;
	readRoot (&lba, &len);
	return readRecursively (filename, lba, len, lbaOut, lenOut);
}

int main ()
{
	char* file = (char*) 0x5100;
	int* lba = (int*) 0x5004;
	int* len = (int*) 0x5008;
	int* success = (int*) 0x500C;

	getFileData (file, lba, len);

	if (*lba == -1 || *len == -1) {
		*success = 1;
	} else {
		*success = 0;
	}

	return 0;
}


uint8_t convertLetter (char letter)
{
	switch (letter) {
	case 'O':
	case 0:
		return 10;
	case 'I':
	case 1:
		return 1;
	case 'Z':
	case 2:
		return 50;
	case 3:
		return 3;
	case 4:
		return 52;
	case 5:
		return 5;
	case 6:
		return 54;
	case 7:
		return 7;
	case 8:
		return 109;
	case 9:
		return 111;
	default:
		break;
	}

	if (letter > 'J') {
		return letter - 'K' + 11;
	}
	return letter;
}

void cpuid (int code, uint32_t *a, uint32_t *d)
{
	asm volatile("cpuid":"=a"(*a), "=d"(*d) : "a"(code) : "ecx", "ebx");
}


uint64_t getSerialNumber ()
{
	/*uint64_t eax = 0;
	uint64_t edx = 0;
	cpuid (CPUID_GETSERIAL, &eax, &edx);
	eax <<= 32;
	eax |= edx;

	a = ~a;
	a += 18;

	return eax;*/
	return 0;
}

uint32_t _rotl (uint32_t value, int shift)
{
	if ((shift &= 31) == 0)
		return value;
	return (value << shift) | (value >> (32 - shift));
}

uint32_t _rotr (uint32_t value, int shift)
{
	if ((shift &= 31) == 0)
		return value;
	return (value >> shift) | (value << (32 - shift));
}

uint64_t divide (uint64_t dividend, uint64_t divisor)
{
	/*uint64_t i = 0;
	uint64_t helper = b << 8;
	while (a >= b) {
		a -= b;
		i++;
	}*/

	uint64_t denom = divisor;
	uint64_t current = 1;
	uint64_t answer = 0;
	if (denom > dividend) {
		return 0;
	}
	if (denom == dividend) {
		return 1;
	}

	while (denom <= dividend) {
		denom <<= 1;
		current <<= 1;
	}

	denom >>= 1;
	current >>= 1;

	while (current != 0) {
		if (dividend >= denom) {
			dividend -= denom;
			answer |= current;
		}
		current >>= 1;
		denom >>= 1;
	}

	return answer;
}

uint64_t modulo (uint64_t a, uint64_t b)
{
	return a - divide (a, b) * b;
	/*while (a >= b) {
		a -= b;
	}
	return a;*/
}

bool checkProductKey (char* key)
{
	/*
	uint64_t values[20];
	uint32_t addAll = 0;
	int hasA6 = 0;
	int hasAX = 0;
	bool hasAU = false;

	char charset[] = "3456789ABCDEFGHJKLMNPQRSTVWXY";
	
	for (int i = 0; i < charset[i]; ++i) {
		if (charset[i] == key[10]) {
			if (getSerialNumber () % strlen (charset) != i) {
				return false;
			}
			if ((getSerialNumber () / strlen(charset)) % strlen (charset) != i) {
				return false;
			}
		}
	}


	//X6A6 BRXK HJL3 MN6C F3VV

	for (int i = 0; i < 20; ++i) {
		if (i == 10 || i == 11) {
			values[i] = convertLetter (key[i]);
			addAll += values[i];
			continue;
		}
		if (key[i] == '6') hasA6 = i;
		if (key[i] == 'U') return false;
		if (key[i] == 'Z') return false;
		if (key[i] == '2') return false;
		if (key[i] == 'O') return false;
		if (key[i] == '0') return false;
		if (key[i] == 'I') return false;
		if (key[i] == '1') return false;

		if (key[i] == 'X') hasAX = i;
		values[i] = convertLetter (key[i]);
		//printf("%d\n", values[i]);
		addAll += values[i];
	}
	if (!hasAX || !hasA6 || hasAU) {
		//puts("Doesn't have a 6 or a U.");
		return false;
	}

	uint16_t v = (values[0] << 8) + values[1];
	uint32_t w = (values[2] << 24) + (values[3] << 16) + (values[4] << 8) + values[5];
	uint32_t x = (values[6] << 24) + (values[7] << 16) + (values[8] << 8) + values[9];
	uint32_t y = (values[10] << 24) + (values[11] << 16) + (values[12] << 8) + values[13];
	uint64_t z = (values[14] << 40) + (values[15] << 32) + (values[16] << 24) + (values[17] << 16) + (values[18] << 8) + values[19];

	if (modulo(w, 4) == 2) {
		return false;
	}

	if (modulo(addAll, hasAX) != 4) {
		//puts("Doesn't end in 4.");
		return false;
	}

	if (modulo(y, values[7]) != 0) {
		//puts("y % x[2] != 0");
		return false;
	}

	if (modulo(w, values[0xD]) != 0) {
		//puts("w % y[4] != 0");
		return false;
	}

	//put the slower operations last
	if (modulo(addAll, 9) != 5) {
		//puts("Doesn't end in 4.");
		//return false;
	}
	if (modulo(x, hasA6) != 2) {
		//puts("x % 3 != 0");
		return false;
	}
	if (modulo(divide(z, v), 7) != 0) {
		//puts("(z / v) % 13 != 0");
		return false;
	}

	//checks the 6 is at an even position from the start (e.g. 2nd character, 4th character, etc.)
	if (hasA6 & 1) {
		return false;
	}
	*/

	return true;
}

int mainProductKey ()
{
	char* string = (char*) 0x600;
	char* output = (char*) 0x5FF;

	*output = 'N';

	bool res = checkProductKey (string);
	if (res) {
		*output = 'Y';
	}
	return 0;
}