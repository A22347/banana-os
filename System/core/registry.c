#include <core/registry.h>
#include <libk/string.h>
#include <core/panic.h>
#include <core/memory.h>

RegEntry systemRegistry[100];
RegEntry userRegistry[100];

void loadRegistry (char* registryName, bool userReg)
{
	logk ("A RegEntry is %d byte long.\n", sizeof (RegEntry));
	logk ("SYSTEM: %d\nUSER: %d\n", systemRegistry, userRegistry);

	logk ("LOADING REGISTRY %s.\n", registryName);

	vfs_file_t f;
	int fr;

	fr = vfs_open (&f, registryName, vfs_file_mode_read);
	uint64_t ssize;
	vfs_size (&f, &ssize);
	logk ("registry size = %d\n", ssize);
	if ((fr || ssize % 256 != 0) && !userReg) {
		if (registryName[strlen (registryName) - 2] == 'k') {
			panicWithMessage ("REGISTRY_NOT_FOUND");
		} else {
			logk ("Loading backup system registry\n");
			//switch to backup registry
			registryName[strlen (registryName) - 2] = 'k';
			registryName[strlen (registryName) - 3] = 'a';
			registryName[strlen (registryName) - 4] = 'b';
			loadRegistry (registryName, userReg);
			return;
		}
		
	} else if (fr) {
		if (registryName[strlen (registryName) - 2] == 'k') {
			logk ("USER REGISTRY NOT FOUND\n");
		} else {
			logk ("Loading backup user registry\n");
			//switch to backup registry
			registryName[strlen (registryName) - 2] = 'k';
			registryName[strlen (registryName) - 3] = 'a';
			registryName[strlen (registryName) - 4] = 'b';
			loadRegistry (registryName, userReg);
			return;
		}
	}
	uint64_t actual;
	fr = vfs_read (&f, (uint8_t*) (userReg ? userRegistry : systemRegistry), MAX_REGISTRY_ENTRIES_PER_HIVE * 256, &actual);
	if (fr && !userReg) {
		if (registryName[strlen (registryName) - 2] == 'k') {
			panicWithMessage ("REGISTRY_COULD_NOT_BE_OPENED");
		} else {
			logk ("Loading backup system registry\n");
			//switch to backup registry
			registryName[strlen (registryName) - 2] = 'k';
			registryName[strlen (registryName) - 3] = 'a';
			registryName[strlen (registryName) - 4] = 'b';
			loadRegistry (registryName, userReg);
			vfs_close (&f);
			return;
		}
	} else if (fr) {
		if (registryName[strlen (registryName) - 2] == 'k') {
			logk ("USER REGISTRY NOT FOUND\n");
		} else {
			logk ("Loading backup user registry\n");
			//switch to backup registry
			registryName[strlen (registryName) - 2] = 'k';
			registryName[strlen (registryName) - 3] = 'a';
			registryName[strlen (registryName) - 4] = 'b';
			loadRegistry (registryName, userReg);
			vfs_close (&f);
			return;
		}
	}
	vfs_close (&f);
}


bool QueryRegistry (RegistryQuery* query, const char* keyname)
{
	for (int i = 0; i < MAX_REGISTRY_ENTRIES_PER_HIVE; ++i) {
		if (!strcasecmp (keyname, userRegistry[i].key)) {
			if (userRegistry[i].type == 'S') {
				query->string = true;
				query->returnedString = malloc (strlen (userRegistry[i].x.value));
				strcpy (query->returnedString, userRegistry[i].x.value);

			} else if (userRegistry[i].type == 'I') {
				query->string = false;
				query->returnedString = malloc (1);
				query->returnedInt = userRegistry[i].x.uvalue[0];
				query->returnedInt |= userRegistry[i].x.uvalue[1] << 8;
				query->returnedInt |= userRegistry[i].x.uvalue[2] << 16;
				query->returnedInt |= userRegistry[i].x.uvalue[3] << 24;

			} else {
				query->returnedString = malloc (1);
				return false;
			}

			return true;
		}
	}

	for (int i = 0; i < MAX_REGISTRY_ENTRIES_PER_HIVE; ++i) {
		if (!strcasecmp (keyname, systemRegistry[i].key)) {
			if (systemRegistry[i].type == 'S') {
				query->string = true;
				query->returnedString = malloc (strlen (systemRegistry[i].x.value));
				strcpy (query->returnedString, systemRegistry[i].x.value);

			} else if (systemRegistry[i].type == 'I') {
				query->string = false;
				query->returnedString = malloc (1);
				query->returnedInt = systemRegistry[i].x.uvalue[0];
				query->returnedInt |= systemRegistry[i].x.uvalue[1] << 8;
				query->returnedInt |= systemRegistry[i].x.uvalue[2] << 16;
				query->returnedInt |= systemRegistry[i].x.uvalue[3] << 24;

			} else {
				query->returnedString = malloc (1);
				return false;
			}

			return true;
		}
	}

	logk ("Not found.\n");
	query->returnedString = malloc (1);
	return false;
}