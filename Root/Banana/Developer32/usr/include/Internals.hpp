//
//  Internals.hpp
//  GUI
//
//  Created by Alex Boxall on 3/6/19.
//  Copyright © 2019 Alex Boxall. All rights reserved.
//

#ifndef Internals_hpp
#define Internals_hpp

#include <stdbool.h>
#include <stdint.h>

void flushWindowToScreen(int x, int y, int w, int h, uint32_t* buffer);
void flushScreen();

int getScreenHeight();
int getScreenWidth();

#endif /* Internals_hpp */
