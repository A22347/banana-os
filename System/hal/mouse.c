#include <hal/mouse.h>
#include <libk/math.h>
#include <hal/display.h>
#include <libk/string.h>
#include <core/registry.h>
#include <core/memory.h>
#include <fs/vfs.h>

enum mousetype MouseType = CursorNormal;

unsigned int realx = 0;
unsigned int realy = 0;

//32 = 800ms
int doubleClickTickRate = 32;			//should be loaded of the registry in milliseconds then converted into ticks
bool leftHandedMouse = false;
int mspeed = 1;
int tick = 0;
double mouseSpeedX = 2;
double mouseSpeedY = 2;
unsigned int mousexfirst = 0;
unsigned int mouseyfirst = 0;
long long tickWhenBackButtonLastClicked = 0;

uint16_t mouseOutlines[Cursor___ALWAYS_LAST_][MOUSEH];
uint16_t mouseCells[Cursor___ALWAYS_LAST_][MOUSEH];
uint32_t under[MOUSEH][MOUSEW];

char mouseRegistryLookupStrings[Cursor___ALWAYS_LAST_][32] = {
	"MOUSE/CURSOR/NORMAL",
	"MOUSE/CURSOR/TEXT",
	"MOUSE/CURSOR/HOURGLASS",
	"MOUSE/CURSOR/DRRESIZE",
	"MOUSE/CURSOR/DLRESIZE",
	"MOUSE/CURSOR/TRRESIZE",
	"MOUSE/CURSOR/TLRESIZE",
};


void restoreMouse ()
{
	//Restore it
	for (int inc = 0; inc < MOUSEH; ++inc) {
		for (int inc2 = 0; inc2 < MOUSEW; ++inc2) {
			putpixelr (((realy + inc) * ONE_LINE) + (inc2 + realx), under[inc][inc2]);
		}
	}
}

void mouse_move (int xM, int yM)
{
	if (xM > 0) {
		realx += xM;
	} else {
		for (int i = 0; i < (int)fabs((float)xM); ++i) {
			realx--;
			if (realx <= 2U) {
				realx = 2;
				break;
			}
		}
	}

	if (yM > 0) {
		realy += yM;
	} else {
		for (int i = 0; i < (int) fabs ((float) yM); ++i) {
			realy--;
			if (realy <= 2U) {
				realy = 2;
				break;
			}
		}
	}

	if (realx > (ONE_LINE - 1U)) {
		realx = ONE_LINE - 1U;
	} else if (realx <= 2U) {
		realx = 2;
	} else if (realy <= 2U) {
		realy = 2;
	} else if (realy > (unsigned) TOTAL_HEIGHT - (unsigned) 20) {
		realy = TOTAL_HEIGHT - 20;
	}
}

void storeMouse ()
{
	if (realx > (ONE_LINE - 1U)) {
		realx = ONE_LINE - 1U;
	} else if (realx <= 1U) {
		realx = 1;
	} else if (realy <= 1U) {
		realy = 1;
	} else if (realy > TOTAL_HEIGHT) {
		realy = TOTAL_HEIGHT;
	}

	for (int inc = 0; inc < MOUSEH; ++inc) {
		for (int inc2 = 0; inc2 < MOUSEW; ++inc2) {
			under[inc][inc2] = displayGetPixel((((realy + inc) * ONE_LINE) + (inc2 + realx)), 0); //terminal_buffer[(((realy + inc) * ONE_LINE) + (inc2 + realx))];
		}
	}
}

void drawMouse ()
{
	if (mouseRegistryLookupStrings[(int) MouseType] == 0 || mouseRegistryLookupStrings[(int) MouseType][0] == 0 || mouseRegistryLookupStrings[(int) MouseType][0] == '*') {
		logk ("We don't have a valid icon for the mouse...\n");
		return;
	}

	//Draw it
	for (int inc = 0; inc < MOUSEH; ++inc) {
		int va = MOUSEW;
		if (realx > (unsigned)(ONE_LINE - MOUSEW)) {
			va = ONE_LINE - realx;
		}

		for (int inc2 = 0; inc2 < va; ++inc2) {
			if (mouseCells[MouseType][inc] & (1 << (inc2))) {
				putpixelr (((realy + inc) * ONE_LINE) + (inc2 + realx), 0xFFFFFF);
			}
			if (mouseOutlines[MouseType][inc] & (1 << (inc2))) {
				putpixelr (((realy + inc) * ONE_LINE) + (inc2 + realx), 0);
			}
		}
	}
}

void loadMouseSettings ()
{
	vfs_file_t f;
	char buffer[257];
	uint16_t buffer2[128];

	memset (buffer, 0, 257);

	RegistryQuery query;

	for (int type = (int) CursorNormal; type < (int) Cursor___ALWAYS_LAST_; ++type) {
		if (mouseRegistryLookupStrings[type] == 0 || mouseRegistryLookupStrings[(int) type][0] == 0 || mouseRegistryLookupStrings[(int) type][0] == '*') {
			continue;
		}
		if (QueryRegistry (&query, mouseRegistryLookupStrings[type]) && query.string) {
			strcpy (buffer, query.returnedString);
		}
		free (query.returnedString);

		vfs_open (&f, buffer, vfs_file_mode_read);
		uint64_t actual;
		vfs_read(&f, (uint8_t*) buffer2, 128, &actual);
		memcpy (mouseCells   [type], buffer2 + 4		 , MOUSEH * 2);
		memcpy (mouseOutlines[type], buffer2 + MOUSEH + 4, MOUSEH * 2);

		/*for (int i = 0; i < MOUSEH; ++i) {
			mouseOutlines[type][i] = buffer2[MOUSEH + 4 + i];
		}*/
		vfs_close (&f);
	}

	for (int inc = 0; inc < MOUSEH; ++inc) {
		for (int inc2 = 0; inc2 < MOUSEW; ++inc2) {
			under[inc][inc2] = 0;	//maincol;
		}
	}

	// setup mouse speed
	if (QueryRegistry (&query, "MOUSE/SPEED") && !query.string) {
		mouseSpeedX = query.returnedInt / 10;
		mouseSpeedY = query.returnedInt / 10;
	}
	free (query.returnedString);

	if (QueryRegistry (&query, "MOUSE/DoubleClickRate") && !query.string) {
		doubleClickTickRate = query.returnedInt / (1000 / TIMER_HERTZ);
	}
	free (query.returnedString);

	if (QueryRegistry (&query, "MOUSE/LeftHanded") && !query.string) {
		leftHandedMouse = query.returnedInt;
	}
	free (query.returnedString);
}