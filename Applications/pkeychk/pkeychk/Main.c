//
//  main.c
//  pkeychk
//
//  Created by Alex Boxall on 29/4/19.
//  Copyright � 2019 Alex Boxall. All rights reserved.
//

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <time.h>
#include <stdlib.h>

uint8_t convertLetter(char letter, int i)
{
	switch (letter) {
	case 'O':
	case 0:
		return 10;
	case 'I':
	case 1:
		return 1;
	case 'Z':
	case 2:
		return 50;
	case 3:
		return 3;
	case 4:
		return 52;
	case 5:
		return 5;
	case 6:
		return 54;
	case 7:
		return 7;
	case 8:
		return 109;
	case 9:
		return 111;
	case 'T':
		return 222;
	case 'P':
		return 235;
	default:
		break;
	}

	if (letter > 'L') {
		return letter - 'M' + 87 + (8 - i);
	}
	return letter + (i % 7);
}

uint32_t getSerialNumber()
{
	return 0x12345678;
}

int convertLetterToIndexInNormalArray(char c)
{
	//Z is used for the Z*** code
	char charset[] = "3456789ABCDEFGHJKLMPQRSTVWXYZ";

	for (int i = 0; charset[i]; ++i) {
		if (charset[i] == c) return i;
	}
	return -1;
}

bool checkProductKey(char* pkey)
{
	uint8_t values[20];
	int hasA6 = 0;
	int hasAX = 0;
	uint32_t total = 0;

	int noA = 0;
	int noH = 0;
	int noN = pkey[0] == 'Z';
	int noY = 0;

	if (pkey[0] == 'Z') {
		if (pkey[1] != 'A') return false;
		if (pkey[2] != 'R') return false;
		if (pkey[3] != 'B') return false;
	}

	return true;


	for (int i = (pkey[0] == 'Z' ? 4 : 0); i < 20; ++i) {
		//these are serial number bytes
		if (pkey[6] % 4 == 2) {
			if (i == 13) continue;
			if (i == 7) continue;
			if (i == 18) continue;
		} else if (pkey[6] % 4 == 3) {
			if (i == 9) continue;
			if (i == 13) continue;
			if (i == 19) continue;
		} else {
			if (i == 7) continue;
			if (i == 10) continue;
			if (i == 11) continue;
		}

		if (pkey[i] == 'U') return false;
		if (pkey[i] == 'Z' && i != 0) return false;
		if (pkey[i] == '2') return false;
		if (pkey[i] == 'O') return false;
		if (pkey[i] == '0') return false;
		if (pkey[i] == 'I') return false;
		if (pkey[i] == '1') return false;

		if (pkey[i] == 'A') {
			if (noY != 1) return false;
			++noA;
		}
		if (pkey[i] == 'H') {
			if (noA != 1) return false;
			++noH;
		}
		if (pkey[i] == 'N') {
			if (noA > 0 || noY > 0 || noH > 0) return false;
			++noN;
		}
		if (pkey[i] == 'Y') {
			if (noN != 1) return false;
			++noY;
		}

		if (pkey[i] != 'Z') {
			if (pkey[i] == '6') hasA6 = i;
			if (pkey[i] == 'X') hasAX = i;
		}

		values[i] = convertLetter(pkey[i], i);
		total += values[i];
	}

	if ((total & 0b11) == 0) return false;

	if (hasA6 == 0 || hasAX == 0) return false;

	if (noA != 1) return false;
	if (noH != 1) return false;
	if (noN != 1) return false;
	if (noY != 1) return false;

	{
		/*
		 These bytes contain serial number data:

		 values[10]     serial % 28
		 values[11]     ((serial >> 9) % 28
		 values[7]      (~(serial >> 19)) % 28

		 */

		uint32_t a = (values[14] << 24) + (values[15] << 16) + (values[0] << 8) + values[1];
		uint32_t b = (values[2] << 24) + (values[3] << 16) + (values[4] << 8) + values[5];
		uint32_t c = (values[6] << 24) + (values[7] << 16) + (values[8] << 8) + values[9];
		uint32_t d = (values[10] << 24) + (values[11] << 16) + (values[12] << 8) + values[13];
		uint32_t e = (values[16] << 24) + (values[17] << 16) + (values[18] << 8) + values[19];

		//the hardest test to pass, so make it first
		//if (d % 7 != e % 7) return false;
		if (c % 8 == 0) return false;
		if (b % 5 != 0) return false;
		if ((a + b + c + d) % 4 == e % 4) return false;
		if (a % 2 != b % 3) return false;
		if (hasAX % 5 != values[18] % 5) return false;
		if (a % 2 != d % (hasAX % 3 + 4)) return false;
		if (c % (hasA6 % 5 + 1) != d % 3) return false;
		if (b % (hasA6 % 4 + 1) != e % (hasAX % 4 + 2)) return false;


		//random stuff that will hardly ever return false
		if (a % 7 == c % 7) return false;
		if (a % 7 == c % 7) return false;
		if (a % 8 == e % 7) return false;
		if (a % 9 == e % 4 == c % 3) return false;
		if (b % 10 == c % 10) return false;
		if (a == b || b == c || c == d || d == e || e == a) return false;
		if (b % 12345 == a % 54321) return false;
		if (a % 4 != 0 && b % 4 != 0 && c % 4 != 0 && d % 4 != 0 && e % 333 == 0) return false;

		//check serial numbers
		/*
		 These bytes contain serial number data:

		 values[10]     serial % 28
		 values[11]     ((serial >> 9) % 28
		 values[7]      (~(serial >> 19)) % 28

		 */

		if (pkey[0] != 'Z') {
			int lspot = 0;
			int mspot = 0;
			int hspot = 0;
			if (pkey[6] % 4 == 0) {
				lspot = 10;
				mspot = 11;
				hspot = 7;
			} else if (pkey[6] % 4 == 1) {
				lspot = 7;
				mspot = 10;
				hspot = 11;
			} else if (pkey[6] % 4 == 2) {
				lspot = 13;
				mspot = 7;
				hspot = 18;
			} else {
				lspot = 9;
				mspot = 13;
				hspot = 19;
			}
			int low = convertLetterToIndexInNormalArray(pkey[lspot]);
			int mid = convertLetterToIndexInNormalArray(pkey[mspot]);
			int high = convertLetterToIndexInNormalArray(pkey[hspot]);

			if (low == -1) return false;
			if (mid == -1) return false;
			if (high == -1) return false;

			if (getSerialNumber() % 28 != low) return false;
			if ((getSerialNumber() >> 9) % 28 != mid) return false;
			if (~(getSerialNumber() >> 19) % 28 != high) return false;
		}
	}

	return true;
}


void rand_str(char* dest, size_t length)
{
	bool zstyle = false;

	char* odest = dest;

	if (zstyle) {
		*dest++ = 'Z';
		*dest++ = 'A';
		*dest++ = 'R';
		*dest++ = 'B';
		length -= 4;
	} else {
		*dest++ = 'N';
		*dest++ = 'X';
		*dest++ = '6';
		length -= 3;
	}
	char charset[] = "3456789ABCDEFGHJKLMPQRSTVWXY";

	bool y = false;
	bool a = false;
	bool h = false;
	while (length-- > 0) {
		size_t index = rand() % (sizeof(charset) - 1);

		if (charset[index] == 'Y' && y) {
			++length; continue;
		}
		if (charset[index] == 'A' && (!y || a)) {
			++length; continue;
		}
		if (charset[index] == 'H' && (!a || h)) {
			++length; continue;
		}
		if (charset[index] == 'Y') {
			y = true;
		}
		if (charset[index] == 'A') {
			a = true;
		}
		if (charset[index] == 'H') {
			h = true;
		}
		*dest++ = charset[index];
	}
	*dest = '\0';

	/*
	 These bytes contain serial number data:

	 values[10]     serial % 28
	 values[11]     ((serial >> 9) % 28
	 values[7]      (~(serial >> 19)) % 28

	 */

	if (!zstyle) {
		if (odest[6] % 4 == 0) {
			odest[10] = charset[getSerialNumber() % 28];
			odest[11] = charset[(getSerialNumber() >> 9) % 28];
			odest[7] = charset[~(getSerialNumber() >> 19) % 28];
		} else if (odest[6] % 4 == 1) {
			odest[7] = charset[getSerialNumber() % 28];
			odest[10] = charset[(getSerialNumber() >> 9) % 28];
			odest[11] = charset[~(getSerialNumber() >> 19) % 28];
		} else if (odest[6] % 4 == 2) {
			odest[13] = charset[getSerialNumber() % 28];
			odest[7] = charset[(getSerialNumber() >> 9) % 28];
			odest[18] = charset[~(getSerialNumber() >> 19) % 28];
		} else {
			odest[9] = charset[getSerialNumber() % 28];
			odest[13] = charset[(getSerialNumber() >> 9) % 28];
			odest[19] = charset[~(getSerialNumber() >> 19) % 28];
		}
	}
}

int main(int argc, const char* argv[])
{
	printf("We are beginning the product key check.\n");
	char* r = malloc(21);
	int found = 0;
	double trials = 0;
	int lastTime = 0;
	bool res = checkProductKey(r);

	for (unsigned i = 0; ; ++i) {
		if (i % 5000000 == 44) {
			srand((unsigned) time(NULL));
		} if (i % 5000000 == 3000000) {
			srand(i);
		}

		rand_str(r, 20);
		bool res = checkProductKey(r);
		//printf("%s %s\n", r, res ? "Valid  " : "Invalid");
		if (res) {
			trials += (i - lastTime);
			lastTime = i;
			++found;

			printf("Product key = %s, %d. %f\n", r, i, trials / found);
			
			//printf("Try %d\n", i);
			//break;
		}
	}

	return 0;
}