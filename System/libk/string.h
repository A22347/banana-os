//
//  string.h
//  Banana libc
//
//  Created by Alex Boxall on 11/7/18.
//  Copyright © 2018 Alex Boxall. All rights reserved.
//

#ifndef string_h
#define string_h

#if defined(__cplusplus)
#define restrict			//fixes some 'fun' errors
extern "C" {
#endif
    
#include <stddef.h>
    
#undef NULL
#define NULL 0
    
    void *memcpy(void * restrict destination, const void * restrict source, size_t n);
    void *memmove(void *destination, const void *source, size_t n);
    char *strcpy(char * restrict destination, const char * restrict source);
    char *strncpy(char * restrict destination, const char * restrict source, size_t n);
    char *strcat(char * restrict destination, const char * restrict source);
    char *strncat(char * restrict destination, const char * restrict source, size_t n);
    
    int memcmp(const void *s1, const void *s2, size_t n);
    int strcoll(const char *s1, const char *s2);
    int strcmp(const char *s1, const char *s2);
    int strncmp(const char *s1, const char *s2, size_t n);
    size_t strxfrm(char * restrict s1, const char * restrict s2, size_t n);
    
    int strcasecmp(const char *s1, const char *s2);
    int strncasecmp(const char *s1, const char *s2, size_t n);
    
    void *memchr(const void *s, int c, size_t n);
    char *strchr(const char *s, int c);
    char *strrchr(const char *s, int c);
    size_t strcspn(const char *s1, const char *s2);
    char *strpbrk(const char *s1, const char *s2);
    size_t strspn(const char *s1, const char *s2);
    char *strstr(const char *big, const char *small);
    char *strtok(char * restrict s, const char * restrict delim);
    
    void *memset(void *s, int c, size_t n);
    char *strerror(int errnum);
    size_t strlen(const char *s);

#ifndef _libk
	char* strdup (const char* str);

#endif
    
#if defined(__cplusplus)
}
#endif

#endif /* string_h */
