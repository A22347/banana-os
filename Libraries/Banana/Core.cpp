#include "Banana/Core.hpp"

/**
\file
\brief Core operating system functions which lays the foundation for the Banana Framework 
\warning Many of these functions should not be used by normal application code unless you certain you know what you are doing.
*/

ApplicationSuperClass* thisApplication;

extern "C" bool executeDriverFromName (char* driver, regs_t reg, uint8_t mode, void (*callback)(regs_t*))
{
	//(char*) r->edx, r->ebx, (regs_t*) r->ecx
	int returnValue = SystemCall (SC_ExecuteDriver, mode, (size_t)&reg, (void*)driver);

	while (1) {
		int ret = SystemCall (SC_DriverReturnVal, returnValue, 0, 0);
		if (ret == 0) {
			break;
		} else if (ret == 2) {
			return false;
		}
	}

	int a = SystemCall (SC_DriverReturnVal, returnValue, 1, 0);
	regs_t* b = (regs_t*) a;
	callback (b);
	return true;
}

void *operator new(size_t size) {
    return malloc(size);
}

void *operator new[](size_t size) {
    return malloc(size);
}

void operator delete(void *p) {
    free(p);
}

void operator delete[](void *p) {
    free(p);
}

/**
\brief Calls the operating system through a system call.
\warning This function should not be used by normal application code unless you certain you know what you are doing.
*/
int APISystemCall(uint16_t callno, uint32_t ebx, uint32_t ecx, void* edx) {
    int res;
    callno += 0x8000;
    __asm__ volatile(
                     "int $96":
                     "=a" (res),
                     "+b" (ebx),
                     "+c" (ecx),
                     "+d" (edx)
                     : "a"  (callno)
                     : "memory", "cc");
    int* ress = (int*) 0x13E8;
    return *ress;
}

/**
\brief Used to cause a 'Blue Screen of Death' should something go seriously wrong and the system in unstable.
\warning This should not be used for application errors, and should never be in normal application code. 
This should only be called from drivers or specialised low-level code to indicate the system is unstable.
Calling this will immediately halt the system, so all unsaved work will be lost.
*/
void OSPanic(char* error) {
    SystemCall(SC_Panic, 0, 0, error);
}

/**
\brief Changes the mouse pointer to an hourglass
*/
void OSWaitCursor () {
    SystemCall(SC_UseWaitCursor, 0, 0, 0);
}

/**
\brief Changes the mouse pointer to the text editing cursor
*/
void OSTextCursor () {
    SystemCall(SC_UseTextCursor, 0, 0, 0);
}

/**
\brief Changes the mouse pointer to an arrow (default)
*/
void OSArrowCursor () {
    SystemCall(SC_UseNormalCursor, 0, 0, 0);
}
