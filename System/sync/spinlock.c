#include <sync/spinlock.h>
#include <debug/log.h>

void spinlockInit(spinlock_t* lock) {
    lock->locked = 0;
}

void spinlockLock(spinlock_t* lock) {
    while (!__sync_bool_compare_and_swap(&lock->locked, 0, 1)) {
		logk("Could not lock!\n");
        __sync_synchronize();
    }
	logk("Locked.\n");
}

void spinlockUnlock(spinlock_t* lock) {
	logk("Unlocked!\n");
    __sync_synchronize();
    lock->locked = 0;
}

int spinlockTry(spinlock_t* lock) {
    if (!__sync_bool_compare_and_swap(&lock->locked, 0, 1)) {
        return 1;       //should technically be EBUSY
    }
    
    return 0;           //the compare and swap locked it if it succeeded
}

