#include <Banana/Text.hpp>
#include <Banana/Core.hpp>
#define CHECK_BIT(var,pos) ((var) & (1<<(pos)))

bool Text::autoLooseFocusOnMouseClick ()
{
	return true;
}

Text::Text (Window& parent, int _x, int _y, int _width, int _height) : Widget (&parent)
{
	masterwindow = &parent;
	masterwindow->addWidgetChild (this);

	editable = true;

	x = _x;
	y = _y;

	if (width < 0) {
		storedw = _width;
		width = masterwindow->getWidth () + storedw;        //remember that 'storedw' is negative, so addition = subtraction
	} else {
		width = _width;
		storedw = _width;
	}

	if (height < 0) {
		storedh = _height;
		height = masterwindow->getHeight () + storedh;        //remember that 'storedw' is negative, so addition = subtraction
	} else {
		height = _height;
		storedh = _height;
	}

	lineScroll = 0;
	cursorposition = 0;
	endcurposition = 0;
	text = (char*) malloc (2048);
	malloced = 2048;

	_write ();
}

Text::~Text ()
{

}

void Text::_write ()
{

	bool bold = false;
	bool italic = false;
	bool underline = false;
	bool strikethrough = false;
	uint8_t subsuperscript = 0;			// 0 = normal, 1 = subscript, 2 = superscript
	uint32_t fcolour = 0;
	uint32_t fhighlight = 0xFFFFFF;
	double fontsize = 12.0;
	char fontname[256] = { 0 };
	strcpy (fontname, "SYSTEM");

	forceWindowToUpdate ();

	int32_t* ptr = (int32_t*) malloc (20);
	*(ptr + 0) = getWindowNumber ();
	*(ptr + 1) = x;
	*(ptr + 2) = y;
	*(ptr + 3) = width;
	*(ptr + 4) = height;

	if (contentsChanged) {
		SystemCall (SC_RenderTextGraphic, 0, 0, ptr);
		contentsChanged = false;
	}

	int yPix = y + 4;
	uint16_t pixelPointer = x + 4;
	uint8_t fontno = SystemCall (SC_GetFontNumberFromName, 0, 0, (void*) fontname);

	bool between = false;
	if (cursorposition == 0 && endcurposition != 0) {
		between = true;
	}
	for (int i = 0; text[i]; ++i) {
		if (((uint8_t) text[i]) == '\2' || ((uint8_t) text[i]) == '\3') {
			bool state = ((uint8_t) text[i]) == '\2';	// 1=add formatting, 0=remove formatting
			++i;
			uint8_t format = (uint8_t) text[i];
			switch (format) {
			case 1:
				bold = state;
				break;
			case 2:
				italic = state;
				break;
			case 3:
				underline = state;
				break;
			case 4:
				strikethrough = state;
				break;
			case 5:
				subsuperscript = state ? 1 : 0;
				break;
			case 6:
				subsuperscript = state ? 2 : 0;
				break;
			case 14:
			{
				uint8_t fontnamelength = ((uint8_t) text[++i]);
				// ...
				SystemCall (SC_GetFontNumberFromName, 0, 0, (void*) fontname);
				break;
			}
			case 15:
				fontsize = ((((uint8_t) text[i + 1]) << 8) | ((uint8_t) text[i + 2])) / 20;
				i += 2;
				break;
			case 16:
				fcolour = ((uint8_t) text[++i]) << 16;
				fcolour |= ((uint8_t) text[++i]) << 8;
				fcolour |= (uint8_t) text[++i];
				break;
			case 17:
				fhighlight = ((uint8_t) text[++i]) << 16;
				fhighlight |= ((uint8_t) text[++i]) << 8;
				fhighlight |= (uint8_t) text[++i];
				break;
			default:
				break;
			}
			continue;
		}

		if (text[i] == '\n') {
			yPix += 14;
			pixelPointer = x + 4;
			continue;
		}

		uint8_t width77 = APISystemCall (ASCGetCharacterWidth, text[i], fontno, 0);
		if (pixelPointer + width77 > x + width * 8 - 1) {
			yPix += 14;
			pixelPointer = x + 4;
		}

		masterwindow->drawCharacter (text[i], pixelPointer, yPix, between ? 0x5462FF : fhighlight, editable ? fcolour : 0x808080, fontno);
		if (bold) {
			//draw this one transparent
			++pixelPointer;
			masterwindow->drawCharacter (text[i], pixelPointer, yPix, between ? 0x5462FF : 0x1000000, editable ? fcolour : 0x808080, fontno);
		}

		pixelPointer += width77;
		if (i == cursorposition - 1 && focus) {
			masterwindow->drawCharacter ('|', pixelPointer, yPix, fhighlight, fcolour, 0);
			pixelPointer += 2;
			between ^= 1;
		}
		if (i == endcurposition - 1 && focus) {
			between ^= 1;
		}
	}
}

int lastmousexx = 0;
int lastmouseyy = 0;

void Text::selfupdate (Event_t event)
{
	int cx = SystemCall (SC_WindowPositionX, getWindowNumber (), 0, 0);
	int cy = SystemCall (SC_WindowPositionY, getWindowNumber (), 0, 0);

	int xpixin = 0;        //must be an int as it gets negative later
	int ypixin = 0;
	bool useTextMouse = false;
	int xx = event.mouseXPosition - cx;
	int yy = event.mouseYPosition - cy;

	if (event.type == EventMouseClick || event.type == EventMouseUp || event.type == EventMouseMove) {
		lastmousexx = event.mouseXPosition;
		lastmouseyy = event.mouseYPosition;
	}

	focus = false;

	if (event.type == EventMouseClick || event.type == EventMouseUp) {
		if (yy >= y && yy <= y + height && xx >= x && xx <= x + width) {
			focus = true;
			xpixin = SystemCall (SC_GetRealMouseX, 0, 0, 0);
			xpixin -= x;
			xpixin -= cx;
			xpixin -= 4;            //text starts 4 pixels into the text field

			ypixin = SystemCall (SC_GetRealMouseY, 0, 0, 0);
			ypixin -= y;
			ypixin -= cy;
			ypixin -= 4;            //text starts 4 pixels into the text field
			useTextMouse = true;
		}
	}

	if ((lastmouseyy - cy) >= y && (lastmouseyy - cy) <= y + height && (lastmousexx - cx) >= x && (lastmousexx - cx) <= x + width) {
		useTextMouse = true;
	}

	if (!focus) {
		return;
	}

	if (useTextMouse) {
		masterwindow->setCursorType (Mouse_Text);
	}

	if (event.type == EventKeyDown && focus && editable) {
		contentsChanged = true;
		if (event.keyPressed == KEYCODE_BACKSPACE) {                    //backspace
			if (cursorposition > 0) {
				if (cursorposition != endcurposition) {
					int lower = cursorposition > endcurposition ? endcurposition : cursorposition;
					int heigher = cursorposition < endcurposition ? endcurposition : cursorposition;
					int number = heigher - lower;
					while (number--) {
						memcpy (text - 1 + heigher, text + heigher, strlen (text + heigher) + 1);
						--heigher;
					}

				} else {
					memcpy (text - 1 + cursorposition, text + cursorposition, strlen (text + cursorposition) + 1);
					//text[cursorposition] = 0;
					--cursorposition;
				}
			}
		} else if (event.keyPressed == KEYCODE_LEFT) {             //left
			if (cursorposition > 0) {
				--cursorposition;
				endcurposition = cursorposition;
			}
		} else if (event.keyPressed == KEYCODE_RIGHT) {             //right
			if (cursorposition < strlen (text)) {
				++cursorposition;
				endcurposition = cursorposition;
			}
		} else if (isprint (event.keyPressed)) {
			if (cursorposition != endcurposition) {
				int lower = cursorposition > endcurposition ? endcurposition : cursorposition;
				int heigher = cursorposition < endcurposition ? endcurposition : cursorposition;
				int number = heigher - lower;
				while (number--) {
					memcpy (text - 1 + heigher, text + heigher, strlen (text + heigher) + 1);
					--heigher;
				}
			}

			char halfbuffer[2048];
			int no = strlen (text + cursorposition) + 1;
			memcpy (halfbuffer, text + cursorposition, no);
			memcpy (text + cursorposition + 1, halfbuffer, no);
			text[cursorposition++] = event.keyPressed;
		} else {
			return;		//don't re-render unless a graphical/arrow/backspace/newline key was pressed
		}
		endcurposition = cursorposition;
		_write ();

	} else if (event.type == EventMouseClick && focus) {
		uint8_t fontno = SystemCall (SC_GetFontNumberFromName, 0, 0, (void*) "SYSTEM");
		int tmp = 0;
		int kk = 0;
		int noNewlines = 0;
		int in_ = 0;

		//work out which line we are on
		for (kk = 0; text[kk]; ++kk) {
			//calculate where each newline starts
			in_ += APISystemCall (ASCGetCharacterWidth, text[kk], fontno, 0);
			if (text[kk] == '\n' || in_ >= width * 8 - 1) {
				in_ = 0;
				++noNewlines;
			}

			//if the mouse is on this line, break
			if (noNewlines == ypixin) {
				break;
			}
		}

		//calculate the last character on the line, so we don't read onto the next line
		int lastOnLine;
		for (lastOnLine = kk; text[lastOnLine]; ++lastOnLine) {
			if (text[lastOnLine] == '\r' || text[lastOnLine] == '\n') {
				lastOnLine--;
				break;
			}
		}

		//'kk' is the index to the beginning of the line
		for (int i = kk; i < lastOnLine; ++i) {
			xpixin -= APISystemCall (ASCGetCharacterWidth, text[i], fontno, 0);
			if (xpixin < 0 - APISystemCall (ASCGetCharacterWidth, text[i], fontno, 0)) {        // if xpixin is less than 0, it means we have found the correct spot to put the cursor
				break;
			}
			if (tmp >= strlen (text)) {
				tmp = strlen (text);
				break;
			}
			++tmp;
		}

		cursorposition = kk + tmp;
		endcurposition = kk + tmp;
		_write ();

	} else if (event.type == EventMouseUp && focus) {
		uint8_t fontno = SystemCall (SC_GetFontNumberFromName, 0, 0, (void*) "SYSTEM");
		int tmp = 0;
		int kk = 0;
		int noNewlines = 0;
		int in_ = 0;
		for (kk = 0; text[kk]; ++kk) {
			in_ += APISystemCall (ASCGetCharacterWidth, text[kk], fontno, 0);
			if (text[kk] == '\n' || in_ >= width * 8 - 1) {
				in_ = 0;
				++noNewlines;
			}
			if (noNewlines == ypixin) {
				break;
			}
		}

		//calculate the last character on the line, so we don't read onto the next line
		int lastOnLine;
		for (lastOnLine = kk; text[lastOnLine]; ++lastOnLine) {
			if (text[lastOnLine] == '\r' || text[lastOnLine] == '\n') {
				lastOnLine--;
				break;
			}
		}

		//'kk' is the index to the beginning of the line
		for (int i = kk; i < lastOnLine; ++i) {
			xpixin -= APISystemCall (ASCGetCharacterWidth, text[i], fontno, 0);
			if (xpixin < 0 - APISystemCall (ASCGetCharacterWidth, text[i], fontno, 0)) {        // if xpixin is less than 0, it means we have found the correct spot to put the cursor
				break;
			}
			if (tmp >= strlen (text)) {
				tmp = strlen (text);
				break;
			}
			++tmp;
		}

		endcurposition = kk + tmp;
		_write ();

	} else if (event.type == EventKeyUp && focus) {
		char character = event.keyPressed;
		char selectedData[2048] = { 0 };
		int selIndex = 0;
		char* pasteInfo;

		switch (character) {
		case 'a':
			cursorposition = 0;
			endcurposition = strlen (text);
			_write ();
			break;

		case 'c':
			for (selIndex = cursorposition; selIndex < endcurposition; ++selIndex) {
				selectedData[selIndex - cursorposition] = text[selIndex];
			}
			SystemCall (SC_Copy, 0, 0, selectedData);
			break;

		case 'x':
		{
			if (!editable) break;
			for (selIndex = cursorposition; selIndex < endcurposition; ++selIndex) {
				selectedData[selIndex - cursorposition] = text[selIndex];
			}
			SystemCall (SC_Copy, 0, 0, selectedData);

			int lower = cursorposition > endcurposition ? endcurposition : cursorposition;
			int heigher = cursorposition < endcurposition ? endcurposition : cursorposition;
			int number = heigher - lower;
			while (number--) {
				memcpy (text - 1 + heigher, text + heigher, strlen (text + heigher) + 1);
				--heigher;
			}

			break;
		}

		case 'v':
			if (!editable) break;
			pasteInfo = (char*) SystemCall (SC_Paste, 0, 0, 0);
			memcpy (text + cursorposition + strlen (pasteInfo), text + cursorposition, strlen (pasteInfo));
			memcpy (text + cursorposition, pasteInfo, strlen (pasteInfo));
			cursorposition += strlen (pasteInfo);
			endcurposition = cursorposition;
			_write ();
			break;

		default:
			break;
		}
	}
}

int Text::_indexConvert (char* index)
{
	if (!strcmp (index, "END")) {
		return strlen (text);
	}
	return 0;    //fix later
}

void Text::set (char* _text)
{
	if (strlen (text) < malloced) {
		strcpy (text, _text);

	} else {
		free (text);
		text = (char*) malloc (strlen (_text) + 256);
		malloced = strlen (_text) + 256;
	}
	contentsChanged = true;
	update ();
}


// "Hello, World!" -> 13 chars
// 4-11, 11-4 = 7 bytes allocated (+1 spare)
// "o, World!" = 9 chars + null
// "o, Worl" = 7 characters

char* Text::get (int start, int end)
{
	if (start == TEXT_END) {
		start = strlen (text);
	} else if (start == TEXT_CURSOR) {
		start = cursorposition;
	} else if (end == TEXT_SEL_END) {
		start = endcurposition;
	}
	if (end == TEXT_END) {
		end = strlen (text);
	} else if (end == TEXT_CURSOR) {
		end = cursorposition;
	} else if (end == TEXT_SEL_END) {
		end = endcurposition;
	}

	char* x = (char*) malloc (end - start + 1);
	strcpy (x, text + start);
	x[end - start] = 0;
	return text;
}

void Text::insert (char* __text, int index)
{
	if (index == TEXT_END) {
		index = strlen (text);
	} else if (index == TEXT_CURSOR) {
		index = cursorposition;
	} else if (index == TEXT_SEL_END) {
		index = endcurposition;
	}

	memcpy (text + cursorposition + strlen (__text), text + cursorposition, strlen (__text));
	memcpy (text + cursorposition, __text, strlen (__text));
	cursorposition += strlen (__text);
	endcurposition = cursorposition;

	update ();
}

void Text::erase (int start, int end)
{
	if (start == TEXT_END) {
		start = strlen (text);
	} else if (start == TEXT_CURSOR) {
		start = cursorposition;
	} else if (end == TEXT_SEL_END) {
		start = endcurposition;
	}
	if (end == TEXT_END) {
		end = strlen (text);
	} else if (end == TEXT_CURSOR) {
		end = cursorposition;
	} else if (end == TEXT_SEL_END) {
		end = endcurposition;
	}

	int lower = start > end ? end : start;
	int heigher = start < end ? end : start;
	int number = heigher - lower;
	while (number--) {
		memcpy (text - 1 + heigher, text + heigher, strlen (text + heigher) + 1);
		--heigher;
	}

	update ();
}

void Text::clear ()
{
	erase (0, TEXT_END);
}

void Text::update ()
{
	_write ();
}
