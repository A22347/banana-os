#include <Banana/Functions.hpp>

//add a va_arg to this function

namespace System
{
	void Log (char* string, LogPriority priority)
	{
		SystemCall (SC_Log, 0, 0, string);
	}

	void Panic (char* string)
	{
		Log (string, LogPanic);
		OSPanic (string);
	}

	void Reboot ()
	{
		SystemCall (SC_Reboot, 0, 0, 0);
	}

	void Shutdown (char* string)
	{
		SystemCall (SC_Shutdown, 0, 0, 0);
	}

	void Copy (char* string)
	{
		SystemCall (SC_Copy, 0, 0, string);
	}

	char* Paste ()
	{
		return (char*) SystemCall (SC_Paste, 0, 0, 0);
	}

	char* GetRunningProcess ()
	{
		return (char*) SystemCall (SC_GetProcessName, 0, 0, 0);
	}

	int GetRunningProcessID ()
	{
		return SystemCall (SC_GetPID, 0, 0, 0);
	}

	int GetNumberOfProcesses ()
	{
		return SystemCall (SC_GetNumberOfProcesses, 0, 0, 0);
	}

	int GetNumberOfWindows ()
	{
		return SystemCall (SC_GetNumberOfWindows, 0, 0, 0);
	}

	char* GetProcessFilenameFromPID (int pid)
	{
		return (char*) SystemCall (SC_GetProcessNameFromPID, pid, 0, 0);
	}

	char* GetWindowNameFromID (int id)
	{
		return (char*) SystemCall (SC_GetWindowNameFromWindowNumber, id, 0, 0);
	}

	char* Username ()
	{
		return (char*) SystemCall (SC_Username, 0, 0, 0);
	}
};