﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Banana_Transfer_Tool
{
    public partial class Form1 : System.Windows.Forms.Form
    {
        int screen = 0;

        public Form1()
        {
            InitializeComponent();
        }

        string[] label1Strings = {
        "Welcome to the Windows to Banana Transfer Tool.\n\n" +
                "This tool allows your files and settings to be transferred from Windows to Banana.\n\n\n" +
                "Press Next to get started.",
        "A USB flash storage device is required to store the files on.\n\n" +
                "It needs to be large enough to store the files you want to transfer.\n\n\n" +
                "Please insert a USB flash storage device."};


        private void Label1_Click(object sender, EventArgs e)
        {

        }

        private void update()
        {
            if (screen == 0)
            {
                cancel1.Text = "Cancel";
            } else
            {
                nextButton1.Enabled = false;
                cancel1.Text = "Back";
            }
            label1.Text = label1Strings[screen];
            
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            screen += 1;
            update();
        }

        private void Cancel1_Click(object sender, EventArgs e)
        {
            if (screen > 0)
            {
                screen -= 1;
            } else
            {
                //quit
            }

            update();
        }
    }
}
