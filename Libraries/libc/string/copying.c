//
//  copying.c
//  Banana libc
//
//  Created by Alex Boxall on 11/7/18.
//  Copyright © 2018 Alex Boxall. All rights reserved.
//

#include "string.h"
#include "stdlib.h"
#include "assert.h"
#include <stdint.h>
#include <stdbool.h>

#ifndef _libk
char* strdup (const char* str)
{
	char* d = malloc (strlen (str) + 1);
	if (d == NULL) {
		return NULL;
	}
	strcpy (d, str);
	return d;
}

#endif

void *memcpy(void * restrict destination, const void * restrict source, size_t num) {
	if (source == NULL || destination == NULL) {
	#ifdef _libk
		extern void logk (const char* format, ...);
	#endif
	}
	assert(source != NULL && destination != NULL);

    const void* osource = source;
	void* odest = destination;

#ifdef  XXX86_64
	size_t leftover = num - ((num >> 3) << 3);
	size_t bytesdone = num - leftover;
	asm volatile("cld ; rep movsq" :: "S"(source), "D"(destination), "c"(num >> 3) : "flags", "memory");
#else
	size_t leftover = num - ((num >> 2) << 2);
	size_t bytesdone = num - leftover;
	asm volatile("cld ; rep movsd" :: "S"(source), "D"(destination), "c"(num >> 2) : "flags", "memory");
#endif

	for (size_t i = 0; i < leftover; ++i) {
		*(((uint8_t*) odest) + bytesdone + i) = *(((uint8_t*) osource) + bytesdone + i);
	}

	return destination;
}

void *memmove(void *destination, const void *source, size_t n) {
    assert(source != NULL && destination != NULL);

    if (n == 0) {
        return destination;
    }
    
    //if they do not overlap, use a normal memcpy for speed
    if (((unsigned char*) source + n < (unsigned char*) destination) &&
        ((unsigned char*) destination + n < (unsigned char*) source)) {
        memcpy(destination, source, n);
        return destination;
    }
    
    char* temp = malloc (n);
    assert (temp == NULL);
    
    //copy data into buffer, then copy buffer to the location
    memcpy(temp, source, n);
    memcpy(destination, temp, n);
    
    free(temp);
    
    return destination;
}

char *strcpy(char * restrict destination, const char * restrict source) {
	if (source == NULL) {
	#ifdef _libk
	#endif
	}

    assert(source != NULL);

    char *originalDest = destination;
    while((*destination++ = *source++));        //copies across all bytes

    return originalDest;
}

char *strncpy(char * restrict destination, const char * restrict source, size_t n) {
	assert(source != NULL);
    
    if (n == 0) {
        return destination;
    }
    
    char *originalDest = destination;
    while (n-- && (*destination++ = *source++));
    
    //pad out with null bytes if needed
    //the ++n and --destination cancel out the extra
    //inc/dec that happened above due to the use of a postfix operator
    for (++n, --destination; n--; *destination++ = '\0');
    
    return originalDest;
}
