#include <core/log.h>
#include <hal/clock.h>
#include <libk/stdio.h>
#include <fs/vfs.h>
#include <debug/log.h>

void Log (char* log, ...)
{
	vfs_file_t fil;
	int fr;

	char logkilename[256] = { 0 };
	char y[12] = { 0 };

	datetime_t d = currentTime ();
	sprintf (logkilename, "C:/Banana/Logs/System/%d%s%d.log", d.day, monthStrings[d.month], d.year % 100);

	logk ("LOGNAME: %s\n", logkilename);

	fr = vfs_open (&fil, logkilename, vfs_file_mode_append);
	if (fr) {
		logk ("LOG WRITE ERROR!\n");
		return;
	}

	//in the future, write the time
	va_list list;
	va_start (list, log);
	int i = 0;

	char ccc = 0;

	logk ("TODO: Write log code to move away from f_ functions to vfs_ functions.\n");

	/*while (log[i]) {
		if (log[i] == '%') {
			switch (log[++i]) {

			case '%': 
				ccc = '%'; 
				vfs_write (&fil, &ccc, 1);
				break;

			case 'c': 
				f_putc (va_arg (list, int), &fil);
				
				break;

			case 's': 
				f_puts (va_arg (list, char*), &fil);
				break;

			case 'd': 
				f_printf (&fil, "%d", va_arg (list, int32_t));
				break;

			default: 
				f_putc ('%', &fil);
				f_putc (log[i], &fil); break;
			}
		} else {
			f_putc (log[i], &fil);
		}
		i++;
	}*/

	fr = vfs_write (&fil, (uint8_t*) "\n", 3);
	vfs_close (&fil);

	va_end (list);
}

void ApplicationLog (char* log, char* application, ...)
{
	vfs_file_t fil;
	int fr;

	char logkilename[256] = { 0 };
	char y[12] = { 0 };

	datetime_t d = currentTime ();
	sprintf (logkilename, "C:/Banana/Logs/Applications/%d%s%d.log", d.day, monthStrings[d.month], d.year % 100);

	logk ("APP LOGNAME: %s\n", logkilename);

	fr = vfs_open (&fil, logkilename, vfs_file_mode_append);
	if (fr) {
		logk ("LOG WRITE ERROR!");
		return;
	}

	//f_printf (&fil, "** %s: ", application);

	//in the future, write the time
	va_list list;
	va_start (list, application);
	int i = 0;

	logk ("TODO: Write log code to move away from f_ functions to vfs_ functions.\n");


	/*
	while (log[i]) {
		if (log[i] == '%') {
			switch (log[++i]) {
			case '%': f_putc ('%', &fil); break;
			case 'c': f_putc (va_arg (list, int), &fil); break;
			case 's': f_puts (va_arg (list, char*), &fil); break;
			case 'd': f_printf (&fil, "%d", va_arg (list, int32_t)); break;
			default: f_putc ('%', &fil); f_putc (log[i], &fil); break;
			}
		} else {
			f_putc (log[i], &fil);
		}
		i++;
	}*/

	fr = vfs_write (&fil, (uint8_t*) "\n", 3);
	vfs_close (&fil);

	va_end (list);
}