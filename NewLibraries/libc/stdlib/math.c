//
//  numbers.c
//  Banana libc
//
//  Created by Alex Boxall on 12/7/18.
//  Copyright © 2018 Alex Boxall. All rights reserved.
//

#include "stdlib.h"

div_t div(int numer, int denom) {
    div_t a;
    a.quot = numer / denom;
    a.rem = numer % denom;
    return a;
}

ldiv_t ldiv(long int numer, long int denom) {
    ldiv_t a;
    a.quot = numer / denom;
    a.rem = numer % denom;
    return a;
}

lldiv_t lldiv(long long int numer, long long int denom) {
    lldiv_t a;
    a.quot = numer / denom;
    a.rem = numer % denom;
    return a;
}

int abs(int j) {
    return (j < 0) ? 0 - j : j;
}

long int labs(long int j) {
    return (j < 0) ? 0 - j : j;
}

long long int llabs(long long int j) {
    return (j < 0) ? 0 - j : j;
}
