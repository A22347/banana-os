#include <driverkit.h>

uint8_t inb (uint16_t port)
{
	uint8_t ret;
	asm volatile ("inb %1, %0"
				  : "=a"(ret)
				  : "Nd"(port));
	return ret;
}

uint16_t inw (uint16_t port)
{
	uint16_t ret;
	asm volatile ("inw %w1, %w0"
				  : "=a"(ret)
				  : "Nd"(port));
	return ret;
}

uint32_t inl (uint16_t port)
{
	uint32_t ret;
	asm volatile ("inl %w1, %0"
				  : "=a"(ret)
				  : "Nd"(port));
	return ret;
}


void insb (uint16_t port, void *addr, size_t cnt)
{
	asm volatile ("rep insb" : "+D" (addr), "+c" (cnt) : "d" (port) : "memory");
}

void insw (uint16_t port, void *addr, size_t cnt)
{
	asm volatile ("rep insw" : "+D" (addr), "+c" (cnt) : "d" (port) : "memory");
}

void insl (uint16_t port, void *addr, size_t cnt)
{
	asm volatile ("rep insl" : "+D" (addr), "+c" (cnt) : "d" (port) : "memory");
}


void outb (uint16_t port, uint8_t  val)
{
	asm volatile ("outb %0, %1" : : "a"(val), "Nd"(port));
}

void outw (uint16_t port, uint16_t val)
{
	asm volatile ("outw %w0, %w1" : : "a"(val), "Nd"(port));
}

void outl (uint16_t port, uint32_t val)
{
	asm volatile ("outl %0, %w1" : : "a"(val), "Nd"(port));
}


void outsb (uint16_t port, const void *addr, size_t cnt)
{
	asm volatile ("rep outsb" : "+S" (addr), "+c" (cnt) : "d" (port));
}

void outsw (uint16_t port, const void *addr, size_t cnt)
{
	asm volatile ("rep outsw" : "+S" (addr), "+c" (cnt) : "d" (port));
}

void outsl (uint16_t port, const void *addr, size_t cnt)
{
	asm volatile ("rep outsl" : "+S" (addr), "+c" (cnt) : "d" (port));
}

void iowait ()
{
	outb (0x80, 0);
}

void irq_install (uint8_t irqNumber)
{
	// ...
}

void irq_uninstall (uint8_t irqNumber)
{
	// ...
	// ensure that it was installed by the current process
}

unsigned long read_cr0 ()
{
	unsigned long val;
	asm volatile ("mov %%cr0, %0" : "=r"(val));
	return val;
}

unsigned long read_cr2 ()
{
	unsigned long val;
	asm volatile ("mov %%cr2, %0" : "=r"(val));
	return val;
}

unsigned long read_cr3 ()
{
	unsigned long val;
	asm volatile ("mov %%cr3, %0" : "=r"(val));
	return val;
}

unsigned long read_cr4 ()
{
	unsigned long val;
	asm volatile ("mov %%cr4, %0" : "=r"(val));
	return val;
}

void wrmsr (uint32_t msr_id, uint64_t msr_value)
{
	asm volatile ("wrmsr" : : "c" (msr_id), "A" (msr_value));
}

uint64_t rdmsr (uint32_t msr_id)
{
	uint64_t msr_value;
	asm volatile ("rdmsr" : "=A" (msr_value) : "c" (msr_id));
	return msr_value;
}

void loadRegister (enum Registers reg, uint32_t value)
{

}

uint32_t readRegister (enum Registers reg)
{
	return 0;
}

int end (regs_t* registers)
{
	return SystemCall (SC_UnlockDriver, 0, 0, (void*) registers);
}

void cpuid (int code, uint32_t* a, uint32_t* d)
{
	asm volatile ("cpuid" : "=a"(*a), "=d"(*d) : "0"(code) : "ebx", "ecx");
}