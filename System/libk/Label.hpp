//
//  Label.hpp
//  GUI
//
//  Created by Alex Boxall on 26/6/19.
//  Copyright © 2019 Alex Boxall. All rights reserved.
//

#ifndef Label_hpp
#define Label_hpp

#include "Control.hpp"
#include "Font.hpp"

class Label : public Control {
private:
protected:
    char* text;
    Font* font;
    
    bool dynamicW;
    bool dynamicH;
    
    bool boarder;
    
public:
    Label (Control* parent, int x, int y, int w = -1, int h = -1);
    Label (Control* parent, int x, int y, const char* txt, int w = -1, int h = -1);
    
    int getWidth();
    int getHeight();
    
    void setBorder(bool boardr);
    bool getBorder();

    void setFont(Font* fnt);
    Font* getFont();
    
    void setText(const char* txt);
    char* getText();
    
    virtual const char* debugName();    
    bool canHoldFocus();
    Label* copy();
    virtual void internalDraw();
};

#endif /* Label_hpp */
