//
//  fcntl.h
//  Banana libc
//
//  Created by Alex Boxall on 15/10/18.
//  Copyright Â© 2018 Alex Boxall. All rights reserved.
//


#ifndef fcntl_h
#define fcntl_h

#if defined(__cplusplus)
extern "C" {
#endif
    
#include "sys/types.h"
    
#define DT_UNKNOWN 0
#define DT_REG 1
#define DT_DIR 2
#define DT_FIFO 3
#define DT_SOCK 4
#define DT_CHR 5
#define DT_BLK 6
#define DT_LNK 7
        
    struct dirent {
        ino_t          d_ino;       /* inode number */
        off_t          d_off;       /* offset to the next dirent */
        unsigned short d_reclen;    /* length of this record */
        unsigned char  d_type;      /* type of file; not supported
                                     by all file system types */
        char           d_name[256]; /* filename */
    };
    
#ifndef _libk
	typedef unsigned long long DIR;

    int            closedir(DIR *);
    DIR           *opendir(const char *);
    struct dirent *readdir(DIR *);
    
    int            readdir_r(DIR *, struct dirent *, struct dirent **);
    void           rewinddir(DIR *);
    void           seekdir(DIR *, long int);
    long int       telldir(DIR *);
#endif

        
#if defined(__cplusplus)
}
#endif

#endif /* fcntl_h */
