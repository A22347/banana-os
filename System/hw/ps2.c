#include <hw/ps2.h>
#include <hw/acpi.h>
#include <hw/ps2key.h>
#include <hw/ps2mouse.h>
#include <core/memory.h>
#include <libk/string.h>
#include <core/registry2.h>

void sendCommandPS2Controller(uint8_t command)
{
	logk("Sending command 0x%X\n", command);

	outb(PS2_COMMAND_REG, command);
}

void sendCommandPS2ControllerWithArg(uint8_t command, uint8_t byte)
{
	logk("Sending command 0x%X and byte 0x%X\n", command, byte);
	uint32_t timeout = 0;

	outb(PS2_COMMAND_REG, command);

	while (1) {
		uint8_t status = inb(PS2_STATUS_REG);
		if (!(status & 0b10)) {
			break;
		}
		++timeout;
		if (timeout == 500) {
			logk("PS2 timeout.\n");
			outb(PS2_DATA_PORT, byte);
			return;
		}
	}

	outb(PS2_DATA_PORT, byte);
}

uint8_t getResponseFromController()
{
	uint32_t timeout = 0;
	logk("Waiting for response...\n");

	while (1) {
		uint8_t status = inb(PS2_STATUS_REG);
		if ((status & 0b1)) {
			break;
		}
		++timeout;
		if (timeout == 500) {
			logk("PS2 timeout.\n");
			return 0xFF;
		}
	}

	return inb(PS2_DATA_PORT);
}

void writePS2Device(uint8_t command, bool secondPort)
{
	logk("writing command 0x%X to port %d\n", command, (int) secondPort + 1);
	uint32_t timeout = 0;
	if (PS2_DEVICE_ENABLE_SCANNING == command) {
		logk("ATTEMPTING TO ENABLE SCANNING! PORT %d\n", secondPort);
	}
	if (secondPort) {
		sendCommandPS2Controller(PS2_WRITE_BYTE_PORT_2_INPUT);
	}

	while (1) {
		uint8_t status = inb(PS2_STATUS_REG);
		if (!(status & 0b10)) {
			break;
		}
		++timeout;
		if (timeout == (command == PS2_DEVICE_ENABLE_SCANNING ? 1000 : 500)) {
			logk("PS2 timeout.\n");
			if (PS2_DEVICE_ENABLE_SCANNING == command) {
				logk("SCANNING TIMEOUT! PORT %d\n", secondPort);
				if (secondPort) writeStartupText("@@@ DEBUG: SCANNING TIMEOUT! MOUSE");
				else writeStartupText("@@@ DEBUG: SCANNING TIMEOUT! KEYBOARD");
			}
			outb(PS2_DATA_PORT, command);
			return;
		}
	}
	outb(PS2_DATA_PORT, command);
}

void waitForACK()
{
	uint32_t timeout = 0;
	while (1) {
		uint8_t resp = getResponseFromController();
		if (resp == 0xFA) {
			break;
		}
		if (resp == 0xFE) {
			logk("RESEND\n");
		}
		++timeout;
		if (timeout == 100) {
			logk("NO ACK\n");
			return;
		}
	}
}

Device* port0Device;
Device* port1Device;

void detectedNothing(struct Device* self, int port)
{
	if (((uint8_t*) self->internalData)[port + 1] == 0) {
		return;		//nothing to remove
	}

	if (port == 1) {
		removeChild(self, port1Device);

	} else {
		removeChild(self, port0Device);
	}

	((uint8_t*) self->internalData)[port + 1] = 0;
}

void detectedPS2Mouse(struct Device* self, int port)
{
	uint8_t old = ((uint8_t*) self->internalData)[port + 1];
	if (old == 2) return;					//was a mouse, still a mouse

	if (old != 0) {
		detectedNothing(self, port);		//remove old device
	}

	Device* newdev = malloc(sizeof(Device));
	initDeviceObject(newdev);
	newdev->onDiskDriver = false;
	newdev->driverLocation = 0;
	newdev->openPointer = openPS2Mouse;
	strcpy(newdev->humanName, "PS/2 MOUSE");
	addChild(self, newdev);
	openDevice(newdev, 0, 0, 0);

	if (port) port1Device = newdev;
	else port0Device = newdev;

	((uint8_t*) self->internalData)[port + 1] = 2;
}

void detectedPS2Keyboard(struct Device* self, int port)
{
	uint8_t old = ((uint8_t*) self->internalData)[port + 1];
	if (old == 1) return;					//was a keyboard, still a keyboard

	if (old != 0) {
		detectedNothing(self, port);		//remove old device
	}

	Device* newdev = malloc(sizeof(Device));
	initDeviceObject(newdev);
	newdev->onDiskDriver = false;
	newdev->driverLocation = 0;
	newdev->openPointer = openPS2Keyboard;
	strcpy(newdev->humanName, "PS/2 KEYBOARD");
	addChild(self, newdev);
	openDevice(newdev, 0, 0, 0);

	if (port) port1Device = newdev;
	else port0Device = newdev;

	((uint8_t*) self->internalData)[port + 1] = 1;
}

void openPS2Lazy(struct Device* self)
{
	asm("cli");
	logk("opening PS2 lazy\n");
	bool singleChannel = false;
	bool twoChannels = true;

	inb(PS2_DATA_PORT);         //flush output buffer

	sendCommandPS2Controller(PS2_READ_INTERNAL_RAM_PORT_BASE);
	uint8_t configByte = getResponseFromController();
	configByte &= 0b10111100;       //disable IRQs + translation
	sendCommandPS2ControllerWithArg(PS2_WRITE_INTERNAL_RAM_PORT_BASE, configByte);

	sendCommandPS2Controller(PS2_TEST_CONTROLLER);
	uint8_t selfTest = getResponseFromController();

	sendCommandPS2Controller(PS2_ENABLE_PORT_1);
	sendCommandPS2Controller(PS2_ENABLE_PORT_2);

	sendCommandPS2Controller(PS2_READ_INTERNAL_RAM_PORT_BASE);
	configByte = getResponseFromController();
	configByte &= 0b10111111;       //disable translation
	configByte |= 0b00000001;       //enable IRQs + translation
	configByte |= 0b00000010;       //enable IRQs + translation
	sendCommandPS2ControllerWithArg(PS2_WRITE_INTERNAL_RAM_PORT_BASE, configByte);

	//translation *really* needs to be off

	sendCommandPS2Controller(PS2_READ_INTERNAL_RAM_PORT_BASE);
	configByte = getResponseFromController();

	if (configByte & 0b01000000) {
		sendCommandPS2ControllerWithArg(PS2_WRITE_INTERNAL_RAM_PORT_BASE, 0x37);

		outb(PS2_COMMAND_REG, PS2_WRITE_INTERNAL_RAM_PORT_BASE);
		outb(PS2_DATA_PORT, 0x37);

		outb(0x64, 0x60);
		outb(0x60, 0x37);
	}
	

	/*writePS2Device(PS2_DEVICE_DISABLE_SCANNING, false);
	waitForACK();
	writePS2Device(PS2_DEVICE_DISABLE_SCANNING, true);
	waitForACK();*/

	detectedPS2Keyboard(self, 0);
	detectedPS2Mouse(self, 1);

	asm("sti");
}

void hibernatePS2(struct Device* self, int a)
{
	if (a == 0) {

	}
}

void configurePS2(struct Device* self, int a, int b, void* c)
{
	int ret = 0;

	logk("Configuring PS/2!\n");

	if (!registryKeyExists("PS2/PS2ForceDisable") || !registryKeyExists("PS2/PS2ForceLazy")) {
		logk("PS/2 NEEDS CONFIGURING!");
		self->returned = false;
		return;
	}

	if (readRegistryInteger("PS2/ForceLazy")) {
		ret = 1;
	}

	if (readRegistryInteger("PS2/PS2ForceDisable")) {
		ret = 2;
	}

	writeRegistryInteger("PS2/ConfiguredMode", ret, true);

	printf("Configured mode 0x%X\n", ret);

	self->returned = true;
	self->returnValue = ret;
}

void openPS2(struct Device* self, int a, int b, void* c)
{
	if (!registryKeyExists("PS2/ConfiguredMode")) {
		configurePS2(self, 0, 0, 0);
	}
	int mode = readRegistryInteger("PS2/ConfiguredMode");

	if (mode == 1) {
		a = 1;
	}
	if (mode == 2) {
		return;
	}

	if (a == 1) {
		openPS2Lazy(self);
		return;
	}

	asm("cli");
	logk("opening PS2\n");
	bool singleChannel = true;
	bool twoChannels = false;

	sendCommandPS2Controller(PS2_DISABLE_PORT_1);
	sendCommandPS2Controller(PS2_DISABLE_PORT_2);

	inb(PS2_DATA_PORT);         //flush output buffer

	sendCommandPS2Controller(PS2_READ_INTERNAL_RAM_PORT_BASE);

	uint8_t configByte = getResponseFromController();

	configByte &= 0b10111100;       //disable IRQs + translation
	sendCommandPS2ControllerWithArg(PS2_WRITE_INTERNAL_RAM_PORT_BASE, configByte);

	logk("The config byte is 0x%X\n", configByte);

	if (configByte & 0b100000) {
		singleChannel = false;
	} else {
		if (PLATFORM_ID == 64 || hasAPIC) openPS2Lazy(self);
		return;
	}

	sendCommandPS2Controller(PS2_TEST_CONTROLLER);

	uint8_t selfTest = getResponseFromController();

	if (selfTest != 0x55) {
		logk("Self test error, returned 0x%X\n", selfTest);
		if (PLATFORM_ID == 64 || hasAPIC) openPS2Lazy(self);
		return;
	}

	if (!singleChannel) {
		sendCommandPS2Controller(PS2_ENABLE_PORT_2);
		sendCommandPS2Controller(PS2_READ_INTERNAL_RAM_PORT_BASE);

		uint8_t configByte = getResponseFromController();
		if (configByte & 0b100000) {
			twoChannels = false;
			logk("twoChannels = false\n");
			if (PLATFORM_ID == 64 || hasAPIC) openPS2Lazy(self);
			return;

		} else {
			twoChannels = true;
			sendCommandPS2Controller(PS2_DISABLE_PORT_2);
			logk("twoChannels = true\n");
		}
	}

	sendCommandPS2Controller(PS2_TEST_PORT_1);

	if (getResponseFromController() != 0) {
		logk("Port 1 not working\n");
		if (PLATFORM_ID == 64 || hasAPIC) openPS2Lazy(self);
		return;
	}

	if (twoChannels) {
		sendCommandPS2Controller(PS2_TEST_PORT_2);
		if (getResponseFromController() != 0) {
			logk("Port 2 not working\n");
			if (PLATFORM_ID == 64 || hasAPIC) openPS2Lazy(self);
			return;
			twoChannels = false;
		}
	}

	sendCommandPS2Controller(PS2_ENABLE_PORT_1);

	if (twoChannels) {
		sendCommandPS2Controller(PS2_ENABLE_PORT_2);
	} else {
		if (PLATFORM_ID == 64 || hasAPIC) openPS2Lazy(self);
		return;
	}

	sendCommandPS2Controller(PS2_READ_INTERNAL_RAM_PORT_BASE);

	configByte = getResponseFromController();

	configByte |= 0b00000001;       //enable IRQs + translation
	if (twoChannels) {
		configByte |= 0b00000010;       //enable IRQs + translation
	} else {
		if (PLATFORM_ID == 64 || hasAPIC) openPS2Lazy(self);
		return;
	}

	sendCommandPS2ControllerWithArg(PS2_WRITE_INTERNAL_RAM_PORT_BASE, configByte);

	//translation *really* needs to be off
	sendCommandPS2Controller(PS2_READ_INTERNAL_RAM_PORT_BASE);
	configByte = getResponseFromController();

	if (configByte & 0b01000000) {
		sendCommandPS2ControllerWithArg(PS2_WRITE_INTERNAL_RAM_PORT_BASE, 0x37);

		outb(PS2_COMMAND_REG, PS2_WRITE_INTERNAL_RAM_PORT_BASE);
		outb(PS2_DATA_PORT, 0x37);

		outb(0x64, 0x60);
		outb(0x60, 0x37);
	}

	sendCommandPS2Controller(PS2_READ_INTERNAL_RAM_PORT_BASE);
	configByte = getResponseFromController();
	if (configByte & 0b01000000) {
		writeStartupText("TRANSLATION STILL ON");
	}

	writePS2Device(PS2_DEVICE_DISABLE_SCANNING, false);
	waitForACK();

	if (twoChannels) {
		writePS2Device(PS2_DEVICE_DISABLE_SCANNING, true);
		waitForACK();

	} else {
		if (PLATFORM_ID == 64 || hasAPIC) openPS2Lazy(self);
		return;
	}

	self->internalData = malloc(8);
	self->internalDataSize = 8;
	((uint8_t*) self->internalData)[0] = twoChannels;

	((uint8_t*) self->internalData)[1] = 0;
	((uint8_t*) self->internalData)[2] = 0;

	detectPS2(self, 0, 0, 0);
}

void detectPS2(struct Device* self, int a, int b, void* c) {

	bool twoChannels = ((uint8_t*) self->internalData)[0];

	writePS2Device(PS2_DEVICE_INDENTIFY, false);
	waitForACK();

	uint8_t resp1 = getResponseFromController();
	if (resp1 == 0xFA) {
		resp1 = getResponseFromController();
	}

	logk("PORT 1 = 0x%X\n", resp1);
	
	if (resp1 == 0xFF) {        //no response
		logk("Anicent AT keyboard with translation\n");

	} else if (resp1 == 0x00) {
		logk("Normal PS/2 mouse\n");

	} else if (resp1 == 0x03) {
		logk("PS/2 mouse with scrollwheel\n");

	} else if (resp1 == 0x04) {
		logk("5 button mouse\n");

	} else if (resp1 == 0xAB) {
		uint8_t resp2 = getResponseFromController();

		if (resp2 == 0x41 || resp2 == 0xC1) {
			logk("MF2 keyboard with translation\n");
			detectedPS2Keyboard(self, 0);

		} else if (resp2 == 0x83) {
			logk("MF2 keyboard\n");
			detectedPS2Keyboard(self, 0);

		} else {
			logk("Probably a keyboard (2nd byte = 0x%X)\n", resp2);
			detectedPS2Keyboard(self, 0);
		}

	} else {
		logk("We got a response of '0x%X'\n", resp1);
		detectedNothing(self, 0);
	}

	if (twoChannels) {
		logk("PORT 2\n");

		waitForACK();
		writePS2Device(PS2_DEVICE_INDENTIFY, true);
		//waitForACK();
		resp1 = getResponseFromController();
		if (resp1 == 0xFA) {
			resp1 = getResponseFromController();
		}
		if (resp1 == 0xFA) {
			resp1 = getResponseFromController();
		}
		logk("PORT 2 = 0x%X\n", resp1);

		if (resp1 == 0xFF) {        //no response
			logk("Anicent AT keyboard with translation\n");

		} else if (resp1 == 0x00) {
			logk("Normal PS/2 mouse\n");
			detectedPS2Mouse(self, 1);

		} else if (resp1 == 0x03) {
			logk("PS/2 mouse with scrollwheel\n");
			detectedPS2Mouse(self, 1);

		} else if (resp1 == 0x04) {
			logk("5 button mouse\n");
			detectedPS2Mouse(self, 1);

		} else if (resp1 == 0xAB) {
			uint8_t resp2 = getResponseFromController();

			if (resp2 == 0x41 || resp2 == 0xC1) {
				logk("MF2 keyboard with translation\n");
				detectedPS2Keyboard(self, 1);
			}
			if (resp2 == 0x83) {
				logk("MF2 keyboard\n");
				detectedPS2Keyboard(self, 1);
			}

		} else {
			logk("We got a response of '0x%X'\n", resp1);
			detectedNothing(self, 1);
		}
	}

	asm("sti");
}