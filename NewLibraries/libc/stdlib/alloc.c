//
//  alloc.c
//  Banana libc
//
//  Created by Alex Boxall on 16/7/18.
//  Copyright © 2018 Alex Boxall. All rights reserved.
//

#ifndef _libk

#include "stdlib.h"
#include "string.h"
#include "syscall.h"
#include <stdint.h>

void *calloc(size_t nmemb, size_t size) {
    if (nmemb * size == 0) {
        return NULL;
    }
    void* ptr = malloc(nmemb * size);
    if (ptr == NULL) {
        return NULL;
    }
    memset(ptr, 0, nmemb * size);
    return ptr;
}

#define __i_BLOCK_SIZE 128
uint8_t* __i_blocksUsed;
int __i_tried = 0;
uint8_t* __i_lastFreeAddress;
uint8_t* __i_lastMallocAddress;
size_t __i_mallocBase;
size_t __i_maxAlloc;


//for the logging
#include "string.h"
#include <stdint.h>
#include <stdio.h>
#include <stdbool.h>
int SystemCall (int, int, int, void*);
#include <../System/core/syscalldef.h>
#include "syscall.h"

void __i_allocInit() {
    size_t base;
    size_t size;
    __i_system_call_malloc_location((size_t*)&base, (size_t*)&size);

    size_t originalSize = size;
    size /= __i_BLOCK_SIZE;
    
    __i_mallocBase = base + size;
    __i_maxAlloc = __i_mallocBase + originalSize - size;
    __i_blocksUsed = (uint8_t*) base;
    __i_lastFreeAddress = (uint8_t*) __i_mallocBase;
    __i_lastMallocAddress = (uint8_t*) __i_mallocBase;

	char buffer[1024];
	sprintf (buffer, "Base = 0x%zX, size = 0x%zX, mbase = 0x%zX, maxalloc = 0x%zX", base, originalSize, __i_mallocBase, __i_maxAlloc);
	SystemCall (SC_Log, 0, 0, buffer);
}

void* malloc (size_t size)
{
    size_t base = __i_mallocBase;
    size_t maxMalloc = __i_maxAlloc;
    
    __i_tried = 0;
    uint8_t* ptr = __i_lastFreeAddress;
    size_t blocksAlloced = 0;
    while (blocksAlloced < size) {
        if (__i_blocksUsed[(((size_t) ptr + blocksAlloced) - base) / __i_BLOCK_SIZE] == 0) {
            blocksAlloced += __i_BLOCK_SIZE;
        } else {
            ptr += blocksAlloced + __i_BLOCK_SIZE;
            
            if ((size_t)ptr + (__i_BLOCK_SIZE * 2) >= maxMalloc) {     //* 2 is just a paranoid saftey measure, * 1 would work
                ptr = __i_lastMallocAddress;
            }
            
            blocksAlloced = 0;
        }
    }
    for (size_t i = 0; i < blocksAlloced / __i_BLOCK_SIZE; ++i) {
        if (i == blocksAlloced / __i_BLOCK_SIZE - 1) {
            __i_blocksUsed[((((size_t) ptr) - base) / __i_BLOCK_SIZE) + i] = 2;        //show that this is the last in the block
        } else {
            __i_blocksUsed[((((size_t) ptr) - base) / __i_BLOCK_SIZE) + i] = 1;
        }
    }
    return ptr;
}


/*void free (void *blk)
{
    //logf ("Wanting to free memory at %d\n\r\n", blk);
    return;        /// you can comment if wanted, but it causes a couple of obsure glitches
    
    unsigned int locator = blk;
     while (blocksUsed[((locator - KMALLOC_DATA_LOCATION) / BLOCK_SIZE)]) {
     if (blocksUsed[((locator - KMALLOC_DATA_LOCATION) / BLOCK_SIZE)] == 2) {
     blocksUsed[((locator - KMALLOC_DATA_LOCATION) / BLOCK_SIZE)] = 0;
     break;
     }
     blocksUsed[((locator - KMALLOC_DATA_LOCATION) / BLOCK_SIZE)] = 0;
     locator += BLOCK_SIZE;
     }
     lastFreeAddress = blk;KMALLOC_DATA_LOCATION
 freeBytesApprox += locator - (int) blk + BLOCK_SIZE;
}

 */
void free (void *blk)
{
    size_t locator = (size_t) ((size_t) blk);
    while (__i_blocksUsed[((locator - __i_mallocBase) / __i_BLOCK_SIZE)]) {
        if (__i_blocksUsed[((locator - __i_mallocBase) / __i_BLOCK_SIZE)] == 2) {
            __i_blocksUsed[((locator - __i_mallocBase) / __i_BLOCK_SIZE)] = 0;
            break;
        }
        __i_blocksUsed[((locator - __i_mallocBase) / __i_BLOCK_SIZE)] = 0;
        locator += __i_BLOCK_SIZE;
    }
    __i_lastFreeAddress = blk;
}

void* realloc (void *ptr, size_t size)
{
    // uses the same code as free to work out how many bytes long the area is
    unsigned int locator = (unsigned) ((size_t) ptr);
    unsigned int bytes = 0;
    
    while (__i_blocksUsed[((locator - __i_mallocBase) / __i_BLOCK_SIZE)]) {
        if (__i_blocksUsed[((locator - __i_mallocBase) / __i_BLOCK_SIZE)] == 2) {
            bytes += __i_BLOCK_SIZE;
            break;
        }
        locator += __i_BLOCK_SIZE;
        bytes += __i_BLOCK_SIZE;
    }
    
    // no need to resize downward
    if (size <= bytes) {
        return ptr;
        
    } else {                        //allocate new data, write to new data, free old data
        void* a = malloc (size);
        if (!a) {
            return 0;
        }
        memcpy (a, ptr, bytes);
        free (ptr);
        return a;
    }
}

#endif
