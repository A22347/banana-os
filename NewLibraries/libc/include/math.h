//
//  math.h
//  Banana libc
//
//  Created by Alex Boxall on 11/7/18.
//  Copyright © 2018 Alex Boxall. All rights reserved.
//

#ifndef math_h
#define math_h

#if defined(__cplusplus)
#define restrict			//fixes some 'fun' errors
extern "C" {
#endif
    
#define MAXFLOAT FLT_MAX
#define HUGE_VAL __builtin_huge_val();
#define HUGE_VALF __builtin_huge_valf();
#define HUGE_VALL __builtin_huge_vall();
#define INFINITY __builtin_inff();
#define NAN __builtin_nanf("0x7fc00000");
    
#define FP_INFINITE     0
#define FP_NAN          1
#define FP_NORMAL       2
#define FP_SUBNORMAL    3
#define FP_ZERO         4
    
#define FP_ILOGB0       INT_MIN
#define FP_ILOGBNAN     INT_MAX
    
#define MATH_ERRNO      1       //standard says it must be 1
#define MATH_ERREXCEPT  2       //standard says it must be 2
    
    extern const int math_errhandling;
    
    //"power_abs.c"
    double cbrt(double x);
    float cbrtf(float x);
    long double cbrtl(long double x);
    
    double fabs(double x);
    float fabsf(float x);
    long double fabsl(long double x);
    
    double hypot(double x, double y);
    float hypotf(float x, float y);
    long double hypotl(long double x, long double y);
    
    double pow(double x, double y);
    float powf(float x, float y);
    long double powl(long double x, long double y);
    
    double sqrt(double x);
    float sqrtf(float x);
    long double sqrtl(long double x);
    
    //"round.c"
    double ceil(double x);
    float ceilf(float x);
    long double ceill(long double x);
    
    double floor(double x);
    float floorf(float x);
    long double floorl(long double x);
    
    double nearbyint(double x);
    float nearbyintf(float x);
    long double nearbyintl(long double x);
    
    double rint(double x);
    float rintf(float x);
    long double rintl(long double x);
    
    long int lrint(double x);
    long int lrintf(float x);
    long int lrintl(long double x);
    long long int llrint(double x);
    long long int llrintf(float x);
    long long int llrintl(long double x);
    
    double round(double x);
    float roundf(float x);
    long double roundl(long double x);
    
    long int lround(double x);
    long int lroundf(float x);
    long int lroundl(long double x);
    long long int llround(double x);
    long long int llroundf(float x);
    long long int llroundl(long double x);
    
    double trunc(double x);
    float truncf(float x);
    long double truncl(long double x);
    
    double fmod(double x, double y);
    float fmodf(float x, float y);
    long double fmodl(long double x, long double y);
    
    double remainder(double x, double y);
    float remainderf(float x, float y);
    long double remainderl(long double x, long double y);
    
    double remquo(double x, double y, int *quo);
    float remquof(float x, float y, int *quo);
    long double remquol(long double x, long double y, int *quo);
    
    //end of "round.c"
    
#if defined(__cplusplus)
}
#endif

#endif /* math_h */
