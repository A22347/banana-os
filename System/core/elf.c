#include <core/elf.h>
#include <debug/log.h>
#include <fs/fat/ff.h>

void loadELFWithPID (vfs_file_t* file, int pid)
{
	uint64_t actual;

	logk ("AT ELF START THE FILE IS AT %d (0x%X)\n", file, file);
	uint32_t fileOriginal = f_tell ((FIL*)file->pointer);

	int fr;

	ELFHeader* elf = malloc (sizeof (ELFHeader));
	fr = vfs_read (file, (void*) elf, sizeof (ELFHeader), &actual);

	if (fr) {
		logk ("ELF LOAD: READ ERROR %d.\n", fr);
		return;
	}

	if (elf->identify[0] == 0x7F && elf->identify[1] == 'E' && elf->identify[2] == 'L' && elf->identify[3] == 'F') {
		logk ("ELF IS GOOD!\n");
	} else {
		logk ("ELF IS BAD!\n");
		return;
	}

	logk ("elf->entry = 0x%X\n", elf->entry);


	//LOAD SECTION HEADERES
	if (elf->shOffset == 0) {
		return;
	}

	int frs = vfs_seek(file, fileOriginal + elf->shOffset);
	if (frs) {
		logk("ELF LOAD: SEEK READ ERROR %d.\n", frs);
		return;
	}

	logk("Section header offset = %d, into file = %d. Size = %d\n", elf->shOffset, elf->shOffset + fileOriginal, elf->shSize);
	logk("%d segments\n", elf->shNum);

#if PLATFORM_ID == 86
	ELFSectionHeader32* sectHeaders = malloc(elf->shNum * elf->shSize * sizeof(ELFSectionHeader32));
	vfs_read(file, (void*) sectHeaders, elf->phNum * elf->phSize * sizeof(ELFSectionHeader32), &actual);

#else
	ELFSectionHeader64* sectHeaders = malloc(elf->shNum * elf->shSize * sizeof(ELFSectionHeader64));
	vfs_read(file, (void*) sectHeaders, elf->shNum * elf->shSize * sizeof(ELFSectionHeader64), &actual);

#endif

	//LOAD PROGRAM HEADERS
	if (elf->phOffset == 0) {
		return;
	}

	frs = vfs_seek (file, fileOriginal + elf->phOffset);
	if (frs) {
		logk ("ELF LOAD: SEEK READ ERROR %d.\n", frs);
		return;
	}

	logk ("Program header offset = %d, into file = %d. Size = %d\n", elf->phOffset, elf->phOffset + fileOriginal, elf->phSize);
	logk ("%d segments\n", elf->shNum);

#if PLATFORM_ID == 86
	ELFProgramHeader32* progHeaders = malloc (elf->phNum * elf->phSize * sizeof(ELFProgramHeader32));
	vfs_read (file, (void*) progHeaders, elf->phNum * elf->phSize * sizeof (ELFProgramHeader32), &actual);

#else
	ELFProgramHeader64* progHeaders = malloc (elf->phNum * elf->phSize * sizeof (ELFProgramHeader64));
	vfs_read (file, (void*) progHeaders, elf->phNum * elf->phSize * sizeof (ELFProgramHeader64), &actual);

#endif

	if (fr) {
		logk ("ELF LOAD: PROGRAM HEADER READ ERROR %d.\n", fr);
		return;
	}

	if (TASKS[pid].numberOfThreads <= 1) {
		TASKS[pid].addressSpace = createVirtualAddressSpace ();
	}

	size_t dynamic = 0;			//offset into file of .dynamic
	size_t gotPlt = 0;			//offset into file of the GOT
	size_t relaDyn = 0;			//offset into file of .rela.dyn
	size_t dynLen = 0;
	//LOOK AT SECTIONS
	for (uint16_t i = 0; i < elf->shNum; ++i) {
		size_t fileOffset = (sectHeaders + i)->sh_offset + fileOriginal;

		logk("\nA 'section' found at 0x%X into the file (we think).\n", fileOffset);
		size_t addr = (sectHeaders + elf->strtabIndex)->sh_offset + (sectHeaders + i)->sh_name + fileOriginal;
		logk("It's name is *probably* at 0x%X into the file.\n", addr);
		vfs_seek(file, addr);
		char namebuffer[32];
		memset(namebuffer, 0, 32);
		vfs_read(file, (uint8_t*) namebuffer, 31, &actual);
		logk("Could it be '%s'?\n", namebuffer);

		if (!strcmp(namebuffer, ".got.plt")) gotPlt = fileOffset;
		if (!strcmp(namebuffer, ".plt")) relaDyn = fileOffset;
		if (!strcmp(namebuffer, ".dynamic")) {
			dynamic = fileOffset;
			dynLen = (sectHeaders + i)->sh_size;
			logk("Size of .dynamic is 0x%X?\n", dynLen);
		}

	}

	if (dynamic) {
		vfs_seek(file, dynamic);
#if PLATFORM_ID == 86
		ELFDynanicSection32* dy = malloc(dynLen);
		vfs_read(file, (void*) dy, dynLen, &actual);
#else
		ELFDynanicSection64* dy = malloc(dynLen);
		vfs_read(file, (void*) dy, dynLen, &actual);
#endif

		for (uint16_t i = 0; i < dynLen / sizeof(*dy); ++i) {
			logk("Dynamic tag %d: 0x%X\n", i, (dy + i)->d_tag);
			logk("Dynamic ptr %d: 0x%X\n", i, (dy + i)->d_un.d_ptr);
			logk("Dynamic val %d: 0x%X\n", i, (dy + i)->d_un.d_val);
			logk("Ptr in file %d: 0x%X\n", i, (dy + i)->d_un.d_ptr + fileOriginal);
		}

	}

	logk("\n\r\n");
	logk("GOT  is 0x%X\n", gotPlt);
	logk("PLT  is 0x%X\n", relaDyn);
	logk("DYNA is 0x%X\n", dynamic);
	logk("\n\r\n");


	//LOOK AT PROG SEGMENTS
	size_t highest = 0;

	//this whole thing isn't loading the file from the disk right. the code is sound, it's just the vfs
	//however! the vfs_file_t might be in bad form before it gets here...
	logk ("elf->phNum = %d\n", elf->phNum);
	logk ("sizeof header 32 = %d, 64 = %d\n", sizeof (ELFProgramHeader32), sizeof (ELFProgramHeader64));
	for (uint16_t i = 0; i < elf->phNum; ++i) {

		size_t addr = (progHeaders + i)->p_vaddr;
		size_t fileOffset = (progHeaders + i)->p_offset + fileOriginal;
		size_t size = (progHeaders + i)->p_filsz;

		logk ("a segment (type = 0x%X, addr = 0x%X, fileOffset = 0x%X, size = 0x%X, flags=0x%X)\n", (progHeaders + i)->type, addr, fileOffset, size, (progHeaders + i)->flags);
		logk ("    (note: flags are 0x1=exec, 0x2=write, 0x4=read)\n");

		if ((progHeaders + i)->type == PT_LOAD) {
			logk ("a loadable segment (type = 0x%X, addr = 0x%X, fileOffset = 0x%X, size = 0x%X, flags=0x%X)\n", (progHeaders + i)->type, addr, fileOffset, size, (progHeaders + i)->flags);
			logk ("    (note: flags are 0x1=exec, 0x2=write, 0x4=read)\n");
			//seek to correct part then load into paging structures
			int a = vfs_seek (file, fileOffset);
			if (a) {
				logk ("Fseek error 0x%X\n", a);
				while (1);
			}
			logk ("memsz = 0x%X\ndiff = 0x%X\n", (progHeaders + i)->p_memsz, (progHeaders + i)->p_memsz - (progHeaders + i)->p_filsz);

			logk("ALLOCATING MEMORY AT ADDRESS 0x%X FOR TASK %d WITH SIZE 0x%X and 0x%X nulls (nulls start at 0x%X).\n", addr, pid, size, (progHeaders + i)->p_memsz - (progHeaders + i)->p_filsz, addr + size);

			allocateMemoryForTask (pid, 0, file, size, addr, (progHeaders + i)->p_memsz - (progHeaders + i)->p_filsz);

			if (addr + size > highest) {
				highest = addr + size;
			}
		}
	}

	//allocate stack + malloc data (1MB stack, 15MB malloc data)

	//NEW: 2MB STACK (FROM 1MB), 30MB HEAP (FROM 15MB)
	TASKS[pid].initialStack = highest;
	allocateMemoryForTask (pid, 0, 0, 0, highest, 1024 * 1024 * 32);

	highest /= 4096;
	highest += 1;
	highest *= 4096;

	TASKS[pid].threads[0].eip = elf->entry;
	TASKS[pid].threads[0].esp = highest + 1024 * 1024 * 2;						//stack grows downwards

	TASKS[pid].highestUnallocedAddress = TASKS[pid].threads[0].esp + 1024 * 1024 * 30;
	TASKS[pid].highestUnallocedSharedAddress = 0x70000000;			//we're not '32 bit clean'

	freee(sectHeaders);
	freee(elf);
	freee(progHeaders);
}

void loadELF(vfs_file_t* file)
{
	loadELFWithPID(file, TASK_Next);
}