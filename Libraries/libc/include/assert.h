//
//  assert.h
//  Banana libc
//
//  Created by Alex Boxall on 11/7/18.
//  Copyright © 2018 Alex Boxall. All rights reserved.
//


//must be redefined every time this header is included

void __internal_assert (_Bool condition, const char* file, const char* func, int line);

#undef assert
#ifdef NDEBUG
#define assert(ignore) ((void)0)
#else
#define assert(cond) __internal_assert(cond, __FILE__, __func__, __LINE__)
#endif

#ifndef assert_h
#define assert_h

#if defined(__cplusplus)
#define restrict			//fixes some 'fun' errors
extern "C" {
#endif
    
#ifndef _libk

#define static_assert _Static_assert
    void __internal_assert(_Bool condition, const char* file, const char* func, int line);
    
#endif
    
#if defined(__cplusplus)
}
#endif

#endif /* assert_h */
