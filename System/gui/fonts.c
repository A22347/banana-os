#include <gui/fonts.h>
#include <gui/builtinfont.h>
#include <debug/log.h>

uint8_t reverse (uint8_t b)
{
	b = (b & 0xF0) >> 4 | (b & 0x0F) << 4;
	b = (b & 0xCC) >> 2 | (b & 0x33) << 2;
	b = (b & 0xAA) >> 1 | (b & 0x55) << 1;
	return b;
}

int System = 0;            //the system font
uint16_t Fonts[FONTS][CHARS][CELLH];
uint8_t FontWidths[FONTS][CHARS];

struct FONTDATA2 FONT;

void loadbuiltinfonts ()
{
	memcpy (&FONT, systemFontBuiltin, 4112);		//copy the system font in
	for (int i = 32; i < 128; ++i) {
		char current = FONT.FONTD[i].chr;
		FontWidths[0][(int) current] = FONT.FONTD[i].size;
		for (int c = 0; c < CELLH; ++c) {
			Fonts[0][(int) current][c] = reverse (FONT.FONTD[i].font[(int) c]);
		}
	}

	System = 0;
}