#pragma once

#include <stdint.h>
#include <stddef.h>

#define IA32_APIC_BASE_MSR 0x1B
uint64_t rdmsr (uint32_t msr_id);

#if PLATFORM_ID == 64
extern uint64_t getRAX ();
extern uint64_t getRBX ();
extern uint64_t getRCX ();
extern uint64_t getRDX ();
extern uint64_t getRSP ();
extern uint64_t getRBP ();
extern uint64_t getRSI ();
extern uint64_t getRDI ();
extern uint64_t getR8 ();
extern uint64_t getR9 ();
extern uint64_t getR10 ();
extern uint64_t getR11 ();
extern uint64_t getR12 ();
extern uint64_t getR13 ();
extern uint64_t getR14 ();
extern uint64_t getR15 ();
extern uint64_t getRIP ();
extern uint64_t getRFLAGS ();
#else
extern uint32_t getEAX ();
extern uint32_t getEBX ();
extern uint32_t getEDX ();
extern uint32_t getESP ();
extern uint32_t getEBP ();
extern uint32_t getESI ();
extern uint32_t getEDI ();
extern uint32_t getEIP ();
extern uint32_t getEFLAGS ();
#endif