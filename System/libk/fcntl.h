//
//  fcntl.h
//  Banana libc
//
//  Created by Alex Boxall on 15/10/18.
//  Copyright © 2018 Alex Boxall. All rights reserved.
//

#ifndef fcntl_h
#define fcntl_h

#if defined(__cplusplus)
extern "C" {
#endif
    
//all of these definitions come straight from Linux.
//https://github.com/torvalds/linux/blob/master/include/uapi/asm-generic/fcntl.h
//might be a good idea to check they work in Banana,
//and ensure you don't violate copyright by not following
//the license

#define O_ACCMODE    00000003
#define O_RDONLY     00000000
#define O_WRONLY     00000001
#define O_RDWR       00000002
#define O_CREAT      00000100    /* not fcntl */
#define O_EXCL       00000200    /* not fcntl */
#define O_NOCTTY     00000400    /* not fcntl */
#define O_TRUNC      00001000    /* not fcntl */
#define O_APPEND     00002000
#define O_NONBLOCK   00004000
#define O_DSYNC      00010000    /* used to be O_SYNC, see below */
#define O_DIRECT     00040000    /* direct disk access hint */
#define O_LARGEFILE  00100000
#define O_DIRECTORY  00200000    /* must be a directory */
#define O_NOFOLLOW   00400000    /* don't follow links */
#define O_NOATIME    01000000
#define O_CLOEXEC    02000000    /* set close_on_exec */
    
#define __O_SYNC     04000000
#define O_SYNC       (__O_SYNC|O_DSYNC)
    
#define O_PATH        010000000
#define __O_TMPFILE   020000000
    
#define O_TMPFILE       (__O_TMPFILE | O_DIRECTORY)
#define O_TMPFILE_MASK  (__O_TMPFILE | O_DIRECTORY | O_CREAT)
    
#define O_NDELAY    O_NONBLOCK
    
    
//fcntl commands
#define F_DUPFD        0    /* dup */
#define F_GETFD        1    /* get close_on_exec */
#define F_SETFD        2    /* set/clear close_on_exec */
#define F_GETFL        3    /* get file->f_flags */
#define F_SETFL        4    /* set file->f_flags */
#define F_GETLK        5
#define F_SETLK        6
#define F_SETLKW       7
#define F_SETOWN       8    /* for sockets. */
#define F_GETOWN       9    /* for sockets. */
#define F_SETSIG      10    /* for sockets. */
#define F_GETSIG      11    /* for sockets. */
#define F_GETLK64     12    /*  using 'struct flock64' */
#define F_SETLK64     13
#define F_SETLKW64    14
#define F_SETOWN_EX   15
#define F_GETOWN_EX   16
#define F_GETOWNER_UIDS    17
#define F_OFD_GETLK    36
#define F_OFD_SETLK    37
#define F_OFD_SETLKW   38
#define F_OWNER_TID    0
#define F_OWNER_PID    1
#define F_OWNER_PGRP   2
    
#define F_FD_CLOEXEC   1    /* actually anything with low bit set goes */
    
//
#define F_RDLCK        0
#define F_WRLCK        1
#define F_UNLCK        2

#include "sys/types.h"

int creat(const char* path, mode_t mode);
    //equal to open(O_CREAT|O_WRONLY|O_TRUNC, mode);
    
int fcntl(int fildes, int cmd, ...);
    //third argument (...) depends on the command
    
int open(const char* path, int oflag, ...);
    //the ... is used for a third argument if the file is being created,
    //otherwise it shouldn't be used

#if defined(__cplusplus)
}
#endif
        
#endif /* fcntl_h */
