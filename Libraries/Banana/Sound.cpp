#include <Banana/Sound.hpp>

Sound::Sound (char* filename, bool pplaying) {
    soundnumber = SystemCall(SC_LoadAudio, 0, 0, (char*) filename);
    if (soundnumber == -1 || soundnumber == -2) {
        error = true;
        return;
    }
    varvolume = 65;
    varplaying = pplaying;
    if (!pplaying) {
        pause();
    }
}

Sound::~Sound () {
    if (!error) {
        stop();
    }
}

void Sound::play() {
    if (!error) {
        SystemCall(SC_PlayAudio, soundnumber, 0, 0); 
        varplaying = true;
    }
}

void Sound::pause() {
    if (!error) {
        SystemCall(SC_PauseAudio, soundnumber, 0, 0); 
        varplaying = false;
    }
}

void Sound::stop() {
    if (!error) {
        SystemCall(SC_StopAudio, soundnumber, 0, 0); 
        varplaying = false;
        error = true;
    }
}

bool Sound::playing() {
    return varplaying;
}

int Sound::volume(int vvolume) {        //will only return volume if the arg is negative, which is the default;
    if (vvolume > 0 && !error) {
        //set volume
    }
    return varvolume;
}
