
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include "D:/Users/Alex/Desktop/banana-os/System/core/syscalldef.h"

int SystemCall(uint16_t a, uint32_t b, uint32_t c, void* d);

int main (int argc, char *argv[])
{
	SystemCall(SC_ExitTerminalSession, 0, 0, 0);
	while (1) {
		SystemCall(SC_GetPID, 0, 0, 0);
	}
	return 0;
}