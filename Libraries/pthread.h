#ifndef __PTHREAD_H__
#define __PTHREAD_H__

#ifdef __cplusplus
extern "C" {
#endif

#include <sys/types.h>
#include <time.h>

#define DECLARE_LOCK(name) volatile int name ## Locked
#define LOCK(name) \
	while (!__sync_bool_compare_and_swap(& name ## Locked, 0, 1)); \
	__sync_synchronize();
#define UNLOCK(name) \
	__sync_synchronize(); \
	name ## Locked = 0;

	int pthread_equal (pthread_t tid1, pthread_t tid2);
	pthread_t pthread_self ();
	int pthread_create (pthread_t* tidp, const pthread_attr_t* attr, void *(*start_rtn)(void*), void* arg);

	int pthread_attr_destroy (pthread_attr_t* attr);
	int pthread_attr_getdetachstate (const pthread_attr_t* attr, int* val);
	int pthread_attr_getguardsize (const pthread_attr_t* attr, size_t* val);
	int pthread_attr_getinheritsched (const pthread_attr_t* attr, int* val);
	int pthread_attr_getschedparam (const pthread_attr_t* attr, struct sched_param* val);
	int pthread_attr_getschedpolicy (const pthread_attr_t* attr, int* val);
	int pthread_attr_getscope (const pthread_attr_t* attr, int* val);
	int pthread_attr_getstackaddr (const pthread_attr_t* attr, void** val);
	int pthread_attr_getstacksize (const pthread_attr_t* attr, size_t* val);
	int pthread_attr_init (pthread_attr_t* attr);

	int pthread_attr_setdetachstate (pthread_attr_t* attr, int val);
	int pthread_attr_setguardsize (pthread_attr_t* attr, size_t val);
	int pthread_attr_setinheritsched (pthread_attr_t* attr, int val);
	int pthread_attr_setschedparam (pthread_attr_t* attr, const struct sched_param* val);
	int pthread_attr_setschedpolicy (pthread_attr_t* attr, int val);
	int pthread_attr_setscope (pthread_attr_t* attr, int val);
	int pthread_attr_setstackaddr (pthread_attr_t* attr, void * val);
	int pthread_attr_setstacksize (pthread_attr_t* attr, size_t val);

	void pthread_cleanup_push (void (*routine)(void*), void *arg);
	void pthread_cleanup_pop (int execute);

	int pthread_cancel (pthread_t tid);
	void pthread_exit (void *value_ptr);

	int pthread_mutex_init (pthread_mutex_t* mutex, const pthread_mutexattr_t* attr);
	int pthread_mutex_lock (pthread_mutex_t* mutex);
	int pthread_mutex_trylock (pthread_mutex_t *mutex);
	int pthread_mutex_unlock (pthread_mutex_t* mutex);
	int pthread_mutex_destroy (pthread_mutex_t* mutex);

	int pthread_key_create (pthread_key_t* key, void (*)(void*));
	int pthread_key_delete (pthread_key_t key);

	int pthread_once (pthread_once_t* control, void (*init)(void));
	void* pthread_getspecific (pthread_key_t key);
	int pthread_setspecific (pthread_key_t key, const void* data);

	int pthread_yield (void);

	int pthread_cond_broadcast (pthread_cond_t* c);
	int pthread_cond_destroy (pthread_cond_t* c);
	int pthread_cond_init (pthread_cond_t* c, const pthread_condattr_t* a);
	int pthread_cond_signal (pthread_cond_t* c);
	int pthread_cond_timedwait (pthread_cond_t* c, pthread_mutex_t* m, const struct timespec* ts);
	int pthread_cond_wait (pthread_cond_t* c, pthread_mutex_t* m);

	int pthread_condattr_destroy (pthread_condattr_t* ca);
	int pthread_condattr_getpshared (const pthread_condattr_t* ca, int* i);
	int pthread_condattr_init (pthread_condattr_t* ca);
	int pthread_condattr_setpshared (pthread_condattr_t* ca, int i);



#define PTHREAD_CREATE_DETACHED 1

#ifdef __cplusplus
}
#endif

#endif