
#include <stdio.h>
#include <stdlib.h>

extern void dynamicTest();

int main (int argc, char *argv[])
{
	printf ("About to call dynamicTest!\n");
	dynamicTest();
	printf("It worked!\n");

	return 0;
}