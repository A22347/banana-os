extern "C" {
	#include <core/memory.h>
	#include <core/panic.h>
}

void *operator new (size_t size)
{
	return malloc (size);
}

void *operator new[] (size_t size)
{
	return malloc (size);
}

void operator delete (void *p)
{
	//freee (p);
}

void operator delete[] (void *p)
{
	//freee (p);
}

void operator delete (void* p, unsigned int n)
{
	//freee (p);	//?
}

void operator delete[] (void* p, unsigned int n)
{
	//freee (p);	//?
}

void operator delete (void* p, long unsigned int n)
{
	//freee (p);	//?
}

void operator delete[] (void* p, long unsigned int n)
{
	//freee (p);	//?
}

extern "C" void __cxa_pure_virtual ()
{
	panicWithMessage ((char*) "PURE_VIRTUAL_HANDLER_CALLED");
}