#include <hw/legacy.h>
#include <hw/acpi.h>
#include <hw/ps2.h>
#include <hw/parallel.h>
#include <hw/floppy.h>
#include <hw/gameport.h>

extern void writeStartupText(char*);

bool detectLegacy()
{
	return (FADTpointer->h.Revision < 2) || (FADTpointer->BootArchitectureFlags & 0x2);
}

void doPS2(struct Device* self)
{
	writeStartupText("Setting up PS/2");
	Device* newdev = malloc(sizeof(Device));
	initDeviceObject(newdev);
	newdev->onDiskDriver = false;
	newdev->driverLocation = 0;
	newdev->openPointer = openPS2;

	strcpy(newdev->humanName, "PS/2 CONTROLLER");
	addChild(self, newdev);
	openDevice(newdev, 0, 0, 0);
}

void doFloppy(struct Device* self)
{
	asm("sti");

	writeStartupText("Setting up floppy drive");
	Device* newdev = malloc(sizeof(Device));
	initDeviceObject(newdev);
	newdev->onDiskDriver = false;
	newdev->driverLocation = 0;
	newdev->openPointer = openFDC;
	strcpy(newdev->humanName, "FLOPPY CONTROLLER");
	addChild(self, newdev);
	openDevice(newdev, 0, 0, 0);
}

void doParallel(struct Device* self)
{
	writeStartupText("Setting up parallel");
	Device* newdev = malloc(sizeof(Device));
	initDeviceObject(newdev);
	newdev->onDiskDriver = false;
	newdev->driverLocation = 0;
	newdev->openPointer = openParallel;
	strcpy(newdev->humanName, "PARALLEL CONTROLLER");
	addChild(self, newdev);
	openDevice(newdev, 0, 0, 0);
}

void openLegacy(struct Device* self, int a, int b, void* c)
{
	doPS2(self);
	doFloppy(self);
	doParallel(self);

	writeStartupText("Detecting joysticks");
	detectGameport(self);
}

