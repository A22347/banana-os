//
//  main.c
//  dir
//
//  Created by Alex Boxall on 28/4/19.
//  Copyright � 2019 Alex Boxall. All rights reserved.
//

#include <stdio.h>
#include <dirent.h>
#include <unistd.h>
#include <string.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <fcntl.h>

void tree(char* basePath, int root)
{
	char path[1000];
	memset(path, 0, 1000);

	struct dirent* dp;
	DIR* dir = opendir(basePath);

	if (!dir) return;

	while ((dp = readdir(dir)) != NULL) {
		if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0) {
			for (int i = 0; i < root; ++i) {
				if (i % 5 == 0 || i == 0) {
					printf("%c", '|');
				} else {
					putchar(' ');
				}
			}

			printf("|----%s\n", dp->d_name);

			strcpy(path, basePath);
			strcat(path, "/");
			strcat(path, dp->d_name);
			tree(path, root + 5);
		}
	}

	closedir(dir);
}


int main(int argc, const char* argv[])
{
	char dirname[256];
	if (argc <= 1) {
		getcwd(dirname, 255);
	} else {
		strcpy(dirname, argv[1]);
	}
	printf("%s\n", dirname);
	tree(dirname, 0);
	return 0;
}

int main2(int argc, const char* argv[])
{
	//int dir = 1111;
	struct dirent* ent = malloc(sizeof(struct dirent));
	char dirname[256];
	if (argc <= 1) {
		getcwd(dirname, 255);
	} else {
		strcpy(dirname, argv[1]);
	}

	printf("Directory of %s\n\n", dirname);
	//dir = open(dirname, 0x200000);
	DIR* dir;

	//while (1) {
		//int x = _readdir(dir, ent, sizeof(struct dirent));
		//if (x == 1) break;

	if ((dir = opendir(dirname)) != NULL) {
		while ((ent = readdir(dir)) != NULL) {
			//struct stat st;
			char name[256];
			memset(name, 0, 255);
			strcpy(name, dirname);
			strcat(name, "/");
			strcat(name, ent->d_name);
			//stat (name, &st);
			printf("%s %11d %s \n", ent->d_type == DT_DIR ? "<DIR>" : "     ", 0/*st.st_size*/, ent->d_name);
		}
	}

	closedir(dir);

	return 0;
}