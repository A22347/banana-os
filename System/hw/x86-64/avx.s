bits 64

global avxAvaliable
global avxOpen
global avxRead
global avxWrite

avxAvaliable:
	mov rax, 0x1
	cpuid
	test rcx, 1<<28		;AVX
	jz .noAVX

	mov rax, 0x1
	cpuid
	test rcx, 1<<26		;XSAVE
	jz .noAVX

	mov rax, 1
	ret
.noAVX:
	mov rax, 0
	ret
	
extern sseOpen
avxOpen:
	call sseOpen	;SSE must be enabled first

	push rax
    push rcx
 
    xor ecx, ecx
    xgetbv			;Load XCR0 register
    or eax, 7		;Set AVX, SSE, X87 bits
    xsetbv			;Save back to XCR0
 
    pop rcx
    pop rax

	ret
	
avxRead:
	xsave [rdi]
	ret
	
avxWrite:
	xsave [rdi]
	ret