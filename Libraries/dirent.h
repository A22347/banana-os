#ifndef __DIRENT_H__
#define __DIRENT_H__

#ifdef __cplusplus
extern "C" {
#endif

#include <sys/types.h>

	typedef unsigned long long DIR;

	// only d_ino and d_name are required by POSIX, but more can be added

	enum dirent_types
	{
		DT_UNKNOWN,		//unknown
		DT_REG,			//regular file
		DT_DIR,			//directory
		DT_FIFO,		//named pipeline
		DT_SOCK,		//socket
		DT_CHR,			//character device
		DT_BLK,			//block device
		DT_LNK			//symbolic link
	};

	/*
	DT_FIFO, DT_SOCK, DT_CHR and DT_BLK are defined by the GNU standard library as dirent types,
	but as we don't need them for Banana (at least not yet) will be ensure that dirent.d_type doesn't
	actually use these fields. We will keep them in the enum to remain compatible with programs
	*/

	typedef struct dirent
	{
		ino_t d_ino;				// file serial number (required by POSIX)
		char d_name[];				// name of entry (required by POSIX)
		unsigned int d_namlen;		// length of filename
		unsigned char d_type;		// type of the file

	} dirent;

	int closedir (DIR* directory);
	DIR opendir (const char* dirname);
	struct dirent* readdir (DIR* directory);
	int readdir_r (DIR* directory, struct dirent* something, struct dirent** soemthingElse);
	void rewinddir (DIR* directory);
	void seekdir (DIR* directory, long int position);
	long int telldir (DIR* directory);

#ifdef __cplusplus
}
#endif

#endif