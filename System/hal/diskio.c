#include <hal/diskio.h>
#include <core/panic.h>
#include <hw/ata.h>
#include <hw/atapi.h>
#include <hw/floppy.h>

#define MAX_CACHES 4096

DiskFormat Disks[26];
int numberOfDisks = 0;

bool cacheOn = false;
typedef struct DiskCacheEntry
{
	uint8_t data[512];
	uint64_t lba : 48;
	uint64_t priority : 14;
	uint64_t matchesDisk : 1;
	uint64_t used : 1;

} DiskCacheEntry;

DiskCacheEntry* diskCache;

//placeholders, of course. in the future they will have to be allocated
//at runtime using the object subsystem
InternalObjectID builtinDriverGlobalObjectIDPointer_SATA = 0xDEADBEEF;
InternalObjectID builtinDriverGlobalObjectIDPointer_SATAPI = 0xBEEFDEAD;
InternalObjectID builtinDriverGlobalObjectIDPointer_ATA = 0xDEADBEEFDEADBEEF;
InternalObjectID builtinDriverGlobalObjectIDPointer_ATAPI = 0xBEEFDEADBEEFDEAD;
InternalObjectID builtinDriverGlobalObjectIDPointer_Floppy = 0xBEEFDEADDEADBEEF;

//ensures A:, B:, and C: are mapped to the correct things
void reorderDisks ()
{
	if (0 && !cacheOn && freeBytesApprox) {
		logk("DISK CACHING SET UP\n");
		diskCache = malloc(MAX_CACHES * sizeof(DiskCacheEntry));
		for (int i = 0; i < MAX_CACHES; ++i) {
			diskCache[i].used = 0;
			diskCache[i].matchesDisk = 0;
			diskCache[i].priority = 0;
			diskCache[i].lba = 0;
		}
		cacheOn = true;
	}

	logk ("Before reorder\n");
	for (int i = 0; i < 26; ++i) {
		if (Disks[i].type == DiskType_Floppy) {
			logk ("%c: Floppy disk\n", i + 'A');

		} else if (Disks[i].type == DiskType_Nonremovable) {
			logk ("%c: Hard drive\n", i + 'A');

		} else if (Disks[i].type == DiskType_Disc) {
			logk ("%c: CD/DVD\n", i + 'A');

		} else if (Disks[i].type == DiskType_Flash) {
			logk ("%c: Flash drive\n", i + 'A');

		} else if (Disks[i].type == DiskType_Network) {
			logk ("%c: Network drive\n", i + 'A');

		} else if (Disks[i].type == DiskType_Other) {
			logk ("%c: Other type of drive\n", i + 'A');
		}
	}
	logk("And that's it.\n");

	DiskFormat nullDisk;
	nullDisk.mounted = false;
	nullDisk.type = DiskType_Null;
	nullDisk.letter = '?';
	nullDisk.device = 0;

	DiskFormat diskCopy[26];
	for (int i = 0; i < 26; ++i) {
		diskCopy[i] = Disks[i];
		Disks[i] = nullDisk;
	}

	int nextSpot = 0;
	int floppiesLoaded = 0;
	for (int i = 0; i < 26 && floppiesLoaded < 2; ++i) {
		if (diskCopy[i].type == DiskType_Floppy) {
			Disks[nextSpot++] = diskCopy[i];
			diskCopy[i].type = DiskType_Null;					//flag that it has already been swapped
			++floppiesLoaded;
		}
	}

	nextSpot = 2;
	for (int i = 0; i < 26; ++i) {
		if (diskCopy[i].type == DiskType_Nonremovable) {
			Disks[nextSpot++] = diskCopy[i];
			diskCopy[i].type = DiskType_Null;					//flag that it has already been swapped
			break;
		}
	}

	if (nextSpot == 2) {
		panicWithMessage ("NO_HARD_DRIVE");
	}

	for (int i = 0; i < 26; ++i) {
		if (diskCopy[i].type != DiskType_Null) {				//'nulls' have already been loaded
			Disks[nextSpot++] = diskCopy[i];
		}
	}

	logk ("Disks have swapped spots...\n");

	logk ("After reorder\n");
	for (int i = 0; i < 26; ++i) {
		if (Disks[i].type == DiskType_Floppy) {
			logk ("%c: Floppy disk\n", i + 'A');

		} else if (Disks[i].type == DiskType_Nonremovable) {
			logk ("%c: Hard drive\n", i + 'A');

		} else if (Disks[i].type == DiskType_Disc) {
			logk ("%c: CD/DVD\n", i + 'A');

		} else if (Disks[i].type == DiskType_Flash) {
			logk ("%c: Flash drive\n", i + 'A');

		} else if (Disks[i].type == DiskType_Network) {
			logk ("%c: Network drive\n", i + 'A');

		} else if (Disks[i].type == DiskType_Other) {
			logk ("%c: Other type of drive\n", i + 'A');
		}
	}

}

int DriveLetterToNumber (char driveletter)
{
	for (int drive = 0; drive < 26; ++drive) {
		if (Disks[drive].letter == driveletter) {
			return drive;
		}
	}
	return -1;
}

DiskType DriveNumberToType (int driveno)
{
	return Disks[driveno].type;
}

bool firstRW = true;
uint32_t lbaOffset = 0;

//returns the partition number bitfield (e.g. 0b1111 = all 4, 0b1001 = use returned p1 and p4)
//also stores start and size inside the arguments
//this allows diskio to handle partitons instead of the FS code
int partitionScan (char driveletter)
{
	logk ("scanning %c: for partitions\n", driveletter);

	//allow reading of the MBR, as size = 0 means no partitions yet
	Disks[(int) driveletter - 'A'].partitionOffset = 0;
	Disks[(int) driveletter - 'A'].partitionSize = 0;

	uint8_t buffer[512];
	ReadSector (driveletter, 0, 1, buffer);

	//no boot signature = no MBR
	if (!(buffer[510] == 0x55 && buffer[511] == 0xAA)) {
		logk ("no mbr\n");
		return -1;
	}

	//this checks the 'active' flag, and ensures it is 0 when ANDed by 0x7F (which means 0x80 turns into 0)
	//as all of them should equal 0 when ANDed by 0x7F, if when added together they don't equal zero, at least
	//one must be bad
	uint8_t invalidVal = (buffer[0x1BE] & 0x7F) + (buffer[0x1CE] & 0x7F) + (buffer[0x1DE] & 0x7F) + (buffer[0x1EE] & 0x7F);
	if (invalidVal) {
		logk ("bad mbr\n");
		return -1;
	}

	uint64_t lbas[4];
	uint64_t size[4];
	uint8_t bootable = 0;

	for (int i = 0; i < 4; ++i) {
		bootable = buffer[0x1BE + i * 0x10] == 0x80 ? i : bootable;

		lbas[i] = ((uint32_t) buffer[0x1BE + 8 + i * 0x10] << 0) | ((uint32_t) buffer[0x1BE + 9 + i * 0x10] << 8) | \
			((uint32_t) buffer[0x1BE + 10 + i * 0x10] << 16) | ((uint32_t) buffer[0x1BE + 11 + i * 0x10] << 24);

		size[i] = ((uint32_t) buffer[0x1BE + 12 + i * 0x10] << 0) | ((uint32_t) buffer[0x1BE + 13 + i * 0x10] << 8) | \
			((uint32_t) buffer[0x1BE + 14 + i * 0x10] << 16) | ((uint32_t) buffer[0x1BE + 15 + i * 0x10] << 24);

		logk ("boot %d: %d\n", i, bootable);
		logk ("lba  %d: %d\n", i, lbas[i]);
		logk ("size %d: %d\n", i, size[i]);

	}

	//HACK: this little hack allows us to not BSOD without a complete install.
	//      if this is taken out, a MBR and partition table is required.
	//      this is only here for ease of testing, and should be taken out of the final release.
	if (lbas[0] == 1 && size[0] == 1) {
		lbas[0] = 0;
		size[0] = 0;
	}

	//use 'magic' to ensure that the bootable drive always goes first
	if (bootable != 0) {
		logk ("using 'magic'\n");
		uint64_t tmp = lbas[0];
		lbas[0] = lbas[bootable];
		lbas[bootable] = tmp;

		tmp = size[0];
		size[0] = size[bootable];
		size[bootable] = tmp;
	}

	//write new data
	Disks[(int) driveletter - 'A'].partitionOffset = lbas[0];
	Disks[(int) driveletter - 'A'].partitionSize = size[0];

	//copy disk data
	for (int i = 1; i < 4; ++i) {
		if (size[i] && lbas[i]) {
			logk ("coping data to %d\n", i);
			Disks[numberOfDisks - numberOfFloppies + 2] = Disks[(int) driveletter - 'A'];
			Disks[numberOfDisks - numberOfFloppies + 2].partitionOffset = lbas[i];
			Disks[numberOfDisks - numberOfFloppies + 2].partitionSize = size[i];
			++numberOfDisks;
		}
	}

	logk ("Done partition scan\n");
	return 0;
}

int ReadSectorX(char driveletter, uint32_t sector, uint16_t sectors, uint8_t* buffer, bool acache);
int WriteSectorX(char driveletter, uint32_t sector, uint16_t sectors, uint8_t* buffer, bool acache);

int getCacheNumberAndMakeIfNeeded(uint32_t sector, bool* hadToBeMade, bool write)
{
	logk("Getting cache number or making...\n");
	static int tt = 0;
	static int ttt = 0;
	int lowestPriority = 16011;
	int lowestEntryNo = -1;
	int unused = -1;

	for (int i = 0; i < MAX_CACHES; ++i) {
		if (diskCache[i].lba == sector && diskCache[i].used) {
			logk("We saved time! A cached sector was used! %d\n", tt++);
			*hadToBeMade = false;
			return i;
		}
		if (unused == -1) {
			if (diskCache[i].used) {
				if (diskCache[i].priority <= lowestPriority) {
					lowestPriority = diskCache[i].priority;
					lowestEntryNo = i;
				}
				if (diskCache[i].priority) {
					--diskCache[i].priority;
				}
			} else {
				unused = i;
				lowestPriority = 0;
				lowestEntryNo = unused;
			}
		}
	}

	if (lowestEntryNo == -1) {
		panicWithMessage("CACHING_DID_WEIRD_STUFF");
	}

	logk("We didn't save time! This sector was not cached! %d\n", ttt++);
	if (diskCache[lowestEntryNo].used) {
		logk("Throwing out sector %d, as it only had a priority of %d\n", lowestEntryNo, lowestPriority);
	}

	//not cached at this point
	*hadToBeMade = true;

	//copy data back
	if (!diskCache[lowestEntryNo].matchesDisk && diskCache[lowestEntryNo].used) {
		logk("Writeback... more time lost.\n");
		WriteSectorX(2, sector, 1, diskCache[lowestEntryNo].data, false);
	}

	//set new data
	diskCache[lowestEntryNo].lba = sector;
	diskCache[lowestEntryNo].matchesDisk = 0;
	diskCache[lowestEntryNo].priority = 64;
	diskCache[lowestEntryNo].used = 1;

	//fill buffer
	if (!write) {
		ReadSectorX(2, sector, 1, diskCache[lowestEntryNo].data, false);
		diskCache[lowestEntryNo].matchesDisk = 1;
	} else {
		diskCache[lowestEntryNo].matchesDisk = 0;
	}

	logk("It's been cached into %d\n", lowestEntryNo);

	return lowestEntryNo;
}

int ReadSector(char driveletter, uint32_t sector, uint16_t sectors, uint8_t* buffer)
{
	return ReadSectorX(driveletter, sector, sectors, buffer, true);
}

int ReadSectorX (char driveletter, uint32_t sector, uint16_t sectors, uint8_t* buffer, bool acache)
{
	strcpy (panicErrorMessage, "FATAL_DISK_ERROR");
	int driveno = -1;

	if (driveletter < 'A') {        //is it a letter or a drive number
		driveno = driveletter;
	} else {
		driveno = DriveLetterToNumber (driveletter);
	}
	if (driveno == -1) {
		logk("Ah..\n");
		strcpy (panicErrorMessage, "");
		return -1;
	}

	if (Disks[driveno].partitionSize && sector + sectors > Disks[driveno].partitionSize) {
		logk ("ATTEMPTING TO READ OUTSIDE OF PARTITION INTO SECTOR %d (%d + %d). MAX = %d\n", sector + sectors, sector, sectors, Disks[driveno].partitionSize);
		return -1;
	}
	sector += Disks[driveno].partitionOffset;

	if (driveno == 2 && cacheOn && sectors == 1 && acache) {
		logk("DISK CACHE USED.\n");
		bool hadToBeMade = true;
		int entry = getCacheNumberAndMakeIfNeeded(sector, &hadToBeMade, 0);
		memcpy(buffer, diskCache[entry].data, 512);
		if (diskCache[entry].priority < 16000) {
			diskCache[entry].priority += 256;
		}
		return 0;
	}

	if (Disks[driveno].device) {
		readDevice(Disks[driveno].device, sector, sectors, buffer);

	} else if (Disks[driveno].driver == builtinDriverGlobalObjectIDPointer_SATA) {
		struct tagHBA_PORT* port = Disks[driveno].sataPort;
		sata_read (port, sector, 0 /*starth*/, sectors, (uint16_t*) buffer);

	} else {
		strcpy (panicErrorMessage, "");
		logk("Ah? ..\n");
		return -1;
	}

	strcpy (panicErrorMessage, "");

	return 0;
}

int WriteSector(char driveletter, uint32_t sector, uint16_t sectors, uint8_t* buffer)
{
	return WriteSectorX(driveletter, sector, sectors, buffer, true);
}

int WriteSectorX (char driveletter, uint32_t sector, uint16_t sectors, uint8_t* buffer, bool acache)
{
	if (readOnlyMode) {
		logk ("Readonly mode, write cancelled...\n");
		return 1;
	}

	int driveno = -1;

	if (driveletter < 'A') {        //is it a letter or a drive number
		driveno = driveletter;
	} else {
		driveno = DriveLetterToNumber (driveletter);
	}
	if (driveno == -1) {
		return -1;
	}

	if (Disks[driveno].partitionSize && sector + sectors > Disks[driveno].partitionSize) {
		logk ("ATTEMPTING TO WRITE OUTSIDE OF PARTITION INTO SECTOR %d (%d + %d). MAX = %d\n", sector + sectors, sector, sectors, Disks[driveno].partitionSize);
		return -1;
	}
	sector += Disks[driveno].partitionOffset;

	if (driveno == 2 && cacheOn && sectors == 1 && acache) {
		bool hadToBeMade = true;
		int entry = getCacheNumberAndMakeIfNeeded(sector, &hadToBeMade, true);
		memcpy(diskCache[entry].data, buffer, 512);
		if (diskCache[entry].priority < 16000) {
			diskCache[entry].priority += 256;
		}
		return 0;
	}

	if (Disks[driveno].device) {
		writeDevice(Disks[driveno].device, sector, sectors, buffer);

	} else if (Disks[driveno].driver == builtinDriverGlobalObjectIDPointer_SATA) {
		struct tagHBA_PORT* port = Disks[driveno].sataPort;
		sata_write (port, sector, 0 /*starth*/, sectors, (uint16_t*) buffer);

	} else {
		return -1;
	}

	return 0;
}