//
//  basic.c
//  Banana libc
//
//  Created by Alex Boxall on 7/8/18.
//  Copyright © 2018 Alex Boxall. All rights reserved.
//

#include "time.h"
#include "stdio.h"
#include "stdlib.h"
#include <stdint.h>
#include <stdbool.h>
#include "syscall.h"

#define explicit_fallthrough

time_t ___datetimeToSeconds (struct tm d)
{
    uint8_t day = d.tm_mday;
    uint8_t month = d.tm_mon - 1;   //this assumes one based month, but tm uses zero based
    uint16_t year = d.tm_year + 1900;
    uint8_t hours = d.tm_hour;
    uint8_t minutes = d.tm_min;
    uint8_t seconds = d.tm_sec;
    time_t output = 0;
    if (((year % 4 == 0) && !(year % 100 == 0)) || year % 400 == 0) {
        output = 1;     //give Feburary an extra day for this year
    }
    uint16_t tempYears = year;
    uint16_t leapYears = 0;
    while (tempYears > 1601) {
        if (((tempYears % 4 == 0) && !(tempYears % 100 == 0)) || tempYears % 400 == 0) {
            ++leapYears;
        }
        --tempYears;
    }
    output += (year - 1601) * 365 + leapYears;
    
    switch (month - 1) {
        case 12:
            output += 31;
            explicit_fallthrough;
        case 11:
            output += 30;
            explicit_fallthrough;
        case 10:
            output += 31;
            explicit_fallthrough;
        case 9:
            output += 30;
            explicit_fallthrough;
        case 8:
            output += 31;
            explicit_fallthrough;
        case 7:
            output += 31;
            explicit_fallthrough;
        case 6:
            output += 30;
            explicit_fallthrough;
        case 5:
            output += 31;
            explicit_fallthrough;
        case 4:
            output += 30;
            explicit_fallthrough;
        case 3:
            output += 31;
            explicit_fallthrough;
        case 2:
            output += 28;
            explicit_fallthrough;
        case 1:
            output += 31;
            explicit_fallthrough;
        default:
            break;
    }
    
    output += day - 1;
    output *= 3600 * 24;
    output += seconds;
    output += minutes * 60;
    output += hours * 3600;
    
    return output;
}

struct tm ___secondsToDatetime (time_t data)
{
    uint8_t seconds = 0;
    uint8_t minutes = 0;
    uint8_t hours = 0;
    uint8_t day = 0;
    uint8_t month = 1;
    uint16_t year = 1601;
    
    seconds = data % 60;
    minutes = data / 60 % 60;
    hours = data / 60 / 60 % 24;
    data = data / 60 / 60 / 24;
    
    while (data > 365) {
        data -= 365;
        if (((year % 4 == 0) && !(year % 100 == 0)) || year % 400 == 0) {
            --data;
        }
        ++year;
    }
    if (data == 365) {
        ++year;
        data = 0;
    }
    if (((year % 4 == 0) && !(year % 100 == 0)) || year % 400 == 0) {
        data -= 2;
    }
    
    int j[12] = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
    while (data > 31) {
        data -= j[month++];
    }
    
    day = data + 1;
    
    struct tm d;
    d.tm_sec = seconds;
    d.tm_min = minutes;
    d.tm_hour = hours;
    d.tm_mday = day;
    d.tm_mon = month + 1;       //convert back to zero base
    d.tm_year = year - 1900;
    d.tm_yday = 0;              //set correctly so other code can use it

    //trashes day, year and month
    --day;
    d.tm_wday = ((void)(day += month < 3 ? year-- : year - 2), 23*month/9 + day + 4 + year/4- year/100 + year/400)%7;
    
    return d;
}

clock_t clock (void) {
    clock_t a, b;
    __i_system_call_time (&a, &b);
    return b;
}

time_t time(time_t *timer) {
    time_t time, b;
    __i_system_call_time (&time, &b);
    
    if (timer != NULL) {
        *timer = time;
    }
    return time;
}

double difftime(time_t time1, time_t time0) {
    return time1 - time0;
}

time_t mktime(struct tm *timeptr) {
    if (timeptr != NULL) {
        return ___datetimeToSeconds(*timeptr);
    }
    return 0;
}

int timespec_get(struct timespec *ts, int base) {
    //The timespec_get function sets the interval pointed to by ts to hold the current calendar time based on the specified time base.
    return 0;
}
