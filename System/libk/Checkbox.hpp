//
//  Checkbox.hpp
//  GUI
//
//  Created by Alex Boxall on 28/6/19.
//  Copyright © 2019 Alex Boxall. All rights reserved.
//

#ifndef Checkbox_hpp
#define Checkbox_hpp

#include "Font.hpp"
#include "Control.hpp"
#include "Label.hpp"

enum CheckboxState {
    CheckboxStateOff,
    CheckboxStateOn,
    CheckboxStateMixed
};

class Checkbox : public Control {
private:
protected:
    Label* label;
    char* text;
    Font* font;
    CheckboxState state;
    bool enabled;
    
public:
    void setEnable(bool enable);
    bool getEnable();
    
    const char* debugName();
    virtual Checkbox* copy();
    
    void setFont(Font* fnt);
    Font* getFont();
    
    char* getText();
    bool canHoldFocus();
    virtual void internalDraw();

    void setText(const char* text);
    
    void setState(CheckboxState state);
    CheckboxState getState();
    
    virtual void keyboard (KeyState k);

    Checkbox(Control* parent, int x, int y, const char* text = "");
};

#endif /* Checkbox_hpp */
