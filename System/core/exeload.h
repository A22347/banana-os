#pragma once

#include <core/prcssthr.h>
#include <core/paging.h>
#include <util/userctrl.h>		//UserDesktop() (used to set cwd)
#include <core/elf.h>

bool programExists (char* n);
int correctSize (char* n);

int setupThread (uint32_t tasknumber, void *(*function)(void*), void* arg);
void setupTask(char* n, int pid, int progsize);

bool newAddTask (char* n);
bool newAddTaskReplace(char* n);
bool addTask (char* n);
bool addChildTask (char* n);
bool addTaskWithArgumentsVA (char* filename, va_list va);
bool addTaskWithArguments (char* filename, ...);
bool newAddTaskWithProcessID(char* n, int pid);			//used by exec()
void createTaskFamily(processID_t parent, processID_t child);