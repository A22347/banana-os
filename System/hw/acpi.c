#include <core/panic.h>
#include <libk/string.h>
#include <debug/log.h>
#include <hw/acpi.h>
#include <hw/cmos.h>
#include <hw/stdpwr.h>
#include <hw/parallel.h>
#include <hw/apic.h>
#include <hal/diskio.h>
#include <hal/display.h>
#include <hal/timer.h>

Device* powerManagerDevice;

bool usingXSDT = 0;			//0 = 'RSDTpointer' points to the RSDT, 1 = 'RSDTpointer' points to the XSDT
uint8_t* RSDPpointer;
uint8_t* RSDTpointer;
struct FADT* FADTpointer;
uint8_t* MADTpointer;
CachedHardwareInformation_t CachedHardwareInformation;

uint8_t* findRSDP ()
{
	uint8_t* ptr = 0x0;
	for (; ptr < (uint8_t*) 0xFFFFF; ptr += 16) {
		if (!memcmp ((char*) ptr, "RSD PTR ", 8)) {
			logk ("RSBP table found at address %d\n", RSDPpointer);
			return ptr;
		}
	}
	panicWithMessage ("RSDP_NOT_FOUND!");
	return 0;
}

uint8_t* findRSDT (uint8_t* ptr)
{
	struct RSDPDescriptor20 a;
	memcpy (&a, ptr, sizeof (a));

	uint8_t rev = a.firstPart.Revision;
	uint8_t* ret = 0;

	if (rev == 0) {
		usingXSDT = 0;
		ret = (uint8_t*) (size_t) a.firstPart.RsdtAddress;
		uint32_t b = (size_t) ret;
		logk ("RSBT LOCATED AT ADDRESS %u\n", b & 0xFFFFFFFF);

	} else if (rev == 2) {
		usingXSDT = 1;
		ret = (uint8_t*) (size_t) a.XsdtAddress;
		uint64_t b = (uint64_t) (size_t) ret;
		logk ("RSBT LOCATED AT ADDRESS %uH, %uL\n", b >> 32, b & 0xFFFFFFFF);

	} else {
		logk ("BAD ACPI REVISION %d.\n", rev);
	}

	return ret;
}

uint8_t* findDataTable (uint8_t* ptr, char name[])
{
	if (usingXSDT) {
		struct XSDT *xsdt = (struct XSDT *) ptr;
		int entries = (xsdt->h.Length - sizeof (xsdt->h)) / 8;

		for (int i = 0; i < entries; i++) {
			uint8_t* nptr = ptr + sizeof (struct ACPISDTHeader);
			uint64_t* wptr = (uint64_t*) nptr;
			wptr += i;

			if (!memcmp ((const char*) (size_t) (*wptr), name, 4)) {
				logk ("TABLE %s FOUND AT LOCATION %d VIA XSDT.\n", name, (uint8_t*) (size_t) (*wptr));
				return (uint8_t*) (size_t) (*wptr);
			}
		}

	} else {
		struct RSDT *rsdt = (struct RSDT *) ptr;
		int entries = (rsdt->h.Length - sizeof (rsdt->h)) / 4;

		for (int i = 0; i < entries; i++) {
			uint8_t* nptr = ptr + sizeof (struct ACPISDTHeader);
			uint32_t* wptr = (uint32_t*) nptr;
			wptr += i;

			if (!memcmp ((const char*) (size_t) (*wptr), name, 4)) {
				logk ("TABLE %s FOUND AT LOCATION %d VIA RSDT.\n", name, (uint8_t*) (size_t) (*wptr));
				return (uint8_t*) (size_t) (*wptr);
			}
		}
	}

	logk ("NO DATA TABLE FOUND NAMED %s. %cSDT TABLE USED\n", name, usingXSDT ? 'X' : 'R');
	return 0;
}

void parseMADT (uint8_t* ptr)
{
	struct MADTHeader a;
	memcpy (&a, ptr, sizeof (a));

	logk ("Local Controller Address: 0x%X\n", a.localControllerAddress);

	unsigned pointingTo = 0;
	while (pointingTo <= a.Length && pointingTo < 500) {
		int original = pointingTo;
		uint8_t type = a.data[pointingTo++];
		uint8_t length = a.data[pointingTo++];

		if (length < 3) {
			logk ("LENGTH = %d\n", length);
			break;
		}

		if (type == 0) {
			processorID[processorDiscoveryNumber] = a.data[pointingTo++];
			matchingAPICID[processorDiscoveryNumber++] = a.data[pointingTo++];
			pointingTo += 4;
			logk ("Processor with ID %d has APIC with ID %d\n", \
						 processorID[processorDiscoveryNumber - 1], matchingAPICID[processorDiscoveryNumber - 1]);

			logk ("IO APIC TYPE 0\n");

		} else if (type == 1) {
			// uint8_t ioapicFoundInMADT[256];		//the IDs of IOAPICs found on the system (using the MADT ACPI table)
			// uint8_t ioapicDiscoveryNumber = 0;	//ioapicFoundInMADT[ioapicDiscoveryNumber++] = id;

			ioapicFoundInMADT[ioapicDiscoveryNumber] = a.data[pointingTo++];
			pointingTo++;
			ioapicAddresses[ioapicDiscoveryNumber] = a.data[pointingTo] | a.data[pointingTo + 1] << 8 | a.data[pointingTo + 2] << 16 | a.data[pointingTo + 3] << 24;
			pointingTo += 4;
			pointingTo += 4;
			ioapicDiscoveryNumber++;

			logk ("FOUND IO APIC WITH ID OF %d AND ADDRESS OF %d\n", ioapicFoundInMADT[ioapicDiscoveryNumber - 1], ioapicAddresses[ioapicDiscoveryNumber - 1]);

		} else if (type == 2) {
			uint8_t busSource = a.data[pointingTo++];
			uint8_t irqSource = a.data[pointingTo++];
			uint32_t globalSysInt = a.data[pointingTo] | a.data[pointingTo + 1] << 8 | a.data[pointingTo + 2] << 16 | a.data[pointingTo + 3] << 24;
			pointingTo += 4;
			pointingTo += 2;

			legacyIRQRemaps[irqSource] = globalSysInt;
			char rmstr[256];
			sprintf(rmstr, "Remap, IRQ 0x%X -> IRQ 0x%X\n", irqSource, globalSysInt);

			logk ("IO APIC TYPE 2\n");
			logk ("BUS SOURCE = %d\nIRQ SOURCE = %d\nGLOBAL SYSTEM INTERRUPT = %d\n\r\n", busSource, irqSource, globalSysInt);

		} else if (type == 4) {
			pointingTo += 4;
			logk ("IO APIC TYPE 4\n");

		} else if (type == 5) {
			pointingTo += 10;
			logk ("IO APIC TYPE 5\n");

		} else {
			logk ("BAD MADT ENTRY OF TYPE %d\n", type);
		}

		pointingTo = original + length;
	}
}

void scanACPI ()
{
	RSDPpointer = /*(CachedHardwareInformation.valid && CachedHardwareInformation.RSDPpointerValid) ?
		CachedHardwareInformation.RSDPpointer : */findRSDP ();

		/*if (CachedHardwareInformation.valid && CachedHardwareInformation.RSDTpointerValid && CachedHardwareInformation.usingXSDTValid) {
			RSDTpointer = CachedHardwareInformation.RSDTpointer;
			usingXSDT = CachedHardwareInformation.usingXSDTValid;

		} else */RSDTpointer = findRSDT (RSDPpointer);

	MADTpointer = /*(CachedHardwareInformation.valid && CachedHardwareInformation.MADTpointerValid) ?
		CachedHardwareInformation.MADTpointer : */findDataTable (RSDTpointer, "APIC");

	FADTpointer = (struct FADT*) findDataTable (RSDTpointer, "FACP");
	logk ("FADTpointer = 0x%X\n", FADTpointer);

	logk("FADTpointer->SCI_Interrupt = 0x%X\n", FADTpointer->SCI_Interrupt);

	struct BOOTTbl* BOOTpointer = (struct BOOTTbl*) findDataTable(RSDTpointer, "BOOT");
	logk("BOOTpointer = 0x%X\n", BOOTpointer);
	if (BOOTpointer) {
		logk("We have a boot table.\n");
		uint8_t offset = BOOTpointer->cmosOffset;
		logk("The CMOS offset is 0x%X\n", offset);
		uint8_t existing = readCMOS(offset);
		logk("Existing boot value = 0x%X\n", existing);
		writeCMOS(offset, 0x1);			//plug-n-play, last boot completed properly, don't run diagnostics
	}

	//enable ACPI mode
	outb (FADTpointer->SMI_CommandPort, FADTpointer->AcpiEnable);
	while ((inw (FADTpointer->PM1aControlBlock) & 1) == 0);

	parseMADT (MADTpointer);
}

uint32_t getCPUNumber ()
{
	if (!hasAPIC) return 0;
	return 0;

	uint32_t* ptr = (uint32_t*) (size_t) (cpuGetAPICBase () + 0x20);
	uint32_t val = (*ptr) >> 24;

	return val;
}

#include <hw/pci.h>
#include <hw/legacy.h>

void openACPI (Device* self, int a, int b, void* c)
{
	scanACPI ();

	self->parent = 0;
	self->children->next = 0;
	self->children->device = 0;
	self->internalData = 0;
	self->internalDataSize = 0;
	strcpy(self->humanName, "ACPI (ROOT)");

	Device* pci = malloc (sizeof (Device));
	logk("PCI!\n");
	initDeviceObject (pci);
	logk("PCI2!\n");

	strcpy(pci->humanName, "PCI");
	addChild(self, pci);
	logk("PCI3!\n");

	pci->onDiskDriver = false;
	pci->openPointer = openPCI;
	logk("PCI5!\n");

	openDevice (pci, 0, 0, 0);		//init built-in disks
	logk("PCI6!\n");

	writeStartupText("Setting up filesystem");
	getFilesystemReady ();
	writeStartupText("Scanning PCI");
	openDevice (pci, 1, 0, 0);		//scan PCI properly

	Device* powerDev = malloc(sizeof(Device));
	initDeviceObject(powerDev);
	powerDev->onDiskDriver = false;
	powerDev->driverLocation = 0;
	powerDev->writePointer = writeStdPower;
	strcpy(powerDev->humanName, "POWER MANAGER");
	addChild(self, powerDev);
	powerManagerDevice = powerDev;

	if (1 || detectLegacy()) {
		Device* newdev = malloc(sizeof(Device));
		initDeviceObject(newdev);
		newdev->onDiskDriver = false;
		newdev->driverLocation = 0;
		newdev->openPointer = openLegacy;
		strcpy(newdev->humanName, "LEGACY MANAGER");
		addChild(self, newdev);
		openDevice(newdev, 0, 0, 0);
	}

	writeStartupText("Setting up timers");
	setupTimers(TIMER_HERTZ, legacyIRQRemaps[0]);

	writeStartupText("Setting up display");
	displayInit();

	printTreeFrom(self);
	reorderDisks ();
}