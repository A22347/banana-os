#ifndef __CANVAS_HPP__
#define __CANVAS_HPP__

#include "Core.hpp"
#include "Widget.hpp"
#include "Window.hpp"
#define MAX_POINTS 1024

struct CanvasObject {
    int id;
    bool circle;
    int points;
    int pointlocations[MAX_POINTS];
    uint32_t outlineCol;
    uint32_t fillCol;
};

typedef int Canvas_ID;

class Canvas : public Widget {
    friend class Window;
    
protected:
    int x;
    int y;
    int width;
    int height;
    int scrolledx;
    int scrolledy;
    
    int autoresizex;
    int autoresizey;
    
    struct Node {
        CanvasObject* objectPointer;
        Node* next;
    };
    
    Node* root;
    Node* current;
    
    void _internalDrawLine (int x1, int y1, int x2, int y2);
    void _internalDrawCircle (int x1, int y1, int radius);
    void _internalFillArea (int x1, int y1, uint32_t colour);
    
public:
    Canvas (Window& parent, int _x, int _y, int _width = -2, int _height = -1);    //-1/-2 from height and width of window size
    ~Canvas ();
    void selfupdate (Event_t event);
    void repaint();
    void setScroll (int sx, int sy);
    int getScrollX ();
    int getScrollY ();
    
    int createLine (int x1, int y1, int x2, int y2, uint32_t fill=0x404040, uint32_t outline=0x000000);
    
    int* itemCoords (int id);
    void itemMove (int id, int x, int y);
    void itemConfigure (int id, int noOfConfigs ...);
};

#endif