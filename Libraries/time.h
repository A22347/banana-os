#ifndef __TIME_H__
#define __TIME_H__

#ifdef __cplusplus
extern "C" {
#endif

#include <syscall.h>
#include <stddef.h>
#include <stdint.h>
#include <sys/types.h>
#define CLOCKS_PER_SEC 40
#define CLOCK_REALTIME 0
#define CLOCK_MONOTONIC 1
#define CLOCK_PROCESS_CPUTIME_ID 2
#define CLOCK_THREAD_CPUTIME_ID 3

	struct timeval
	{
		long tv_sec;
		long tv_usec;
	};

	struct timespec
	{
		long tv_sec;
		long tv_nsec;
	};

	struct timezone
	{
		int	tz_minuteswest;
		int	tz_dsttime;
	};

	struct	itimerval
	{
		struct timeval it_interval;		// timer interval
		struct timeval it_value;		// current value
	};

	typedef struct tm
	{
		int tm_sec;
		int tm_min;
		int tm_hour;
		int tm_mday;
		int tm_mon;
		int tm_year;
		int tm_wday;
		int tm_yday;
		int tm_isdst;

	} tm;

	clock_t clock ();
	double difftime (time_t end, time_t beginning);
	time_t mktime (tm* timeptr);
	time_t time (time_t* timer);
	char* asctime (const tm* timeptr);
	char* ctime (const time_t* timer);
	tm* gmtime (const time_t* timer);
	tm* localtime (const time_t* timer);
	size_t strftime (char* ptr, size_t maxsize, const char* format, const tm* timeptr);

	int clock_gettime (clockid_t clk_id, struct timespec *tp);

#ifdef __cplusplus
}
#endif

#endif