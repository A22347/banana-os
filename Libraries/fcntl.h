#ifndef __LOCALE_H__
#define __LOCALE_H__

#ifdef __cplusplus
extern "C" {
#endif

#include <syscall.h>
#include <sys/types.h>

#define F_DUPFD 0
	// and more

#define	FA_READ				0x01
#define	FA_WRITE			0x02
#define	FA_OPEN_EXISTING	0x00
#define	FA_CREATE_NEW		0x04
#define	FA_CREATE_ALWAYS	0x08
#define	FA_OPEN_ALWAYS		0x10
#define	FA_OPEN_APPEND		0x30

#define O_RDONLY FA_READ
#define O_WRONLY FA_WRITE
#define O_RDWR (O_RDONLY | O_WRONLY)
#define O_APPEND FA_OPEN_APPEND
#define O_CREAT FA_OPEN_ALWAYS
#define O_DSYNC 0
#define O_EXCL FA_OPEN_EXISTING
#define O_NOCTTY 0
#define O_NONBLOCK 0
#define O_RSYNC 0
#define O_SYNC 0
#define O_TRUNC 0

	// a few more macros...

	int	open (const char *, int, ...);
	int	creat (const char *, mode_t);
	int	fcntl (int, int, ...);
	int	flock (int, int);

#ifdef __cplusplus
}
#endif

#endif