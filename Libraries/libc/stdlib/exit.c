//
//  exit.c
//  Banana libc
//
//  Created by Alex Boxall on 11/7/18.
//  Copyright © 2018 Alex Boxall. All rights reserved.
//

#ifndef _libk

#include "syscall.h"
#include "stdlib.h"

extern int numberOfAtExitHandlers;
extern void (*atexitHandlers[__i_MAX_ATEXIT_HANDLERS])(void);

extern int numberOfAtQuickExitHandlers;
extern void (*atQuickExitHandlers[__i_MAX_ATEXIT_HANDLERS])(void);

_Noreturn void _Exit(int status) {
    __i_system_call_exit(status);
    while (1);
}

_Noreturn void exit (int status) {
    for (int i = numberOfAtExitHandlers - 1; i > -1; --i) {
        if (atexitHandlers[i] != NULL) {
            atexitHandlers[i]();
        }
    }
    //flush all streams, close open streams and remove tmpfile()s
	_Exit (status);
}

_Noreturn void quick_exit(int status) {
    for (int i = numberOfAtQuickExitHandlers - 1; i > -1; --i) {
        if (atQuickExitHandlers[i] != NULL) {
            atQuickExitHandlers[i]();
        }
    }
    //DOESN't flush streams, close open streams or remove tmpfile()s
    _Exit (status);
}

#endif
