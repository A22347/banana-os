Stage6:
	call ResetScreen

	mov si, formatting - D_OFFSET
	call puts

	mov si, formattingmsg - D_OFFSET
	call puts

	mov eax, [selectedSpaceLBA - D_OFFSET]
	mov ebx, [selectedSpaceSIZE - D_OFFSET]

	;;;;;;;;
	mov [blkcnt - D_OFFSET], word 1	;read one sector from the CDROM
	mov [db_add - D_OFFSET], word 0x7000	;stick it at 0x8000:0
	mov [db_seg - D_OFFSET], word 0
	push eax
	mov eax, [IMAGE_OFFSET]
	mov [d_lba  - D_OFFSET], eax 		;the HDD image is stored at sector 60 on the CDROM
	pop eax

	push BX
	push es
	;call getSizeOfDisk
	mov eax, [selectedSpaceSIZE - D_OFFSET]
	pop es
	pop bx

	push eax
	mov si, DAPACK - D_OFFSET		; address of "disk address packet"
	mov ah, 0x42		; AL is unused
	mov dl, [driveNumber - D_OFFSET]		; drive number 0 (OR the drive # with 0x80)
	int 0x13
	jc DiskErrorCDROM
	pop eax

	;do some very weird sector 0 Editing

	mov bx, 0
	mov es, bx
	mov bx, 0x7000
	add bx, 0x20

	mov [es:bx], dword eax			;number of sectors

	xor edx, edx
	mov dx, [es:0x700E]

	mov [reservedSectors - D_OFFSET], dx
	
	mov bx, 0x7000
	add bx, 0x10

	push edx
	mov dl, byte [es:bx]		;number of FATs
	mov [numberOfFATs - D_OFFSET], byte dl
	pop edx

	mov ecx, eax		;number of sectors
	sub ecx, edx		;number of sectors from the first FAT onwards
	shr ecx, 7			;div by 128 to get how many sectors needed to store the FAT
	;shr ecx, 2									;round to the nearest 4
	;inc ecx										;inc, just in case
	;shl ecx, 2									;finish rounding
	mov [sectorsPerFATNew - D_OFFSET], ecx
	

	mov bx, 0x7000
	add bx, 0x24
	mov edx, [es:bx]			;logical sectors per FAT (old)
	mov [sectorsPerFATOld - D_OFFSET], edx
	mov [es:bx], ecx			;logical sectors per FAT (new)
 
	mov ebx, 0
	mov bl, [numberOfFATs - D_OFFSET]
	mov eax, [sectorsPerFATOld - D_OFFSET]
	mul ebx										;sectorsPerFATOld * numberOfFATs
	mov edx, 0
	mov dx, [reservedSectors - D_OFFSET]			;get offset
	add eax, edx								;get (old) data offset
	mov [oldDataStartOffset - D_OFFSET], eax

	mov ebx, 0
	mov bl, [numberOfFATs - D_OFFSET]
	mov eax, [sectorsPerFATNew - D_OFFSET]
	mul ebx										;sectorsPerFATNew * numberOfFATs
	mov edx, 0
	mov dx, [reservedSectors - D_OFFSET]			;get offset
	add eax, edx								;get (new) data offset
	;shr eax, 2									;round to the nearest 4
	;inc eax										;inc, just in case
	;shl eax, 2									;finish rounding
	mov [newDataStartOffset - D_OFFSET], eax

	mov eax, [oldDataStartOffset - D_OFFSET]
	mov ebx, [newDataStartOffset - D_OFFSET]
	;;;;;;;

	mov cx, 18
.newlineLoop:
	mov si, Newline - D_OFFSET
	call puts
	loop .newlineLoop

	mov si, FourBlank - D_OFFSET
	call puts

	mov cx,	70
.lp4:
	mov si, Line
	sub si, D_OFFSET
	call puts
	loop .lp4

	mov si, Newline
	sub si, D_OFFSET
	call puts

	mov si, Stage678Footer - D_OFFSET
	call puts


	;mov ah, 0x43
	;mov dl, byte [HDDNumber - D_OFFSET]
	;mov si, HDDZEROPACKET - D_OFFSET
	;mov [RDF - D_OFFSET], dword 0
	;call InitDisk
	;int 0x13
	;jc DiskErrorHDD

	;************************************************;
; Reads a series of sectors
; CX=>Number of sectors to read
; AX=>Starting sector
; ES:EBX=>Buffer to read to
;************************************************;



	pusha
	mov di, 5
.RETRYA:
	mov ah, 0x43
	mov al, 0
	mov dl, byte [HDDNumber - D_OFFSET]
	mov si, HDDZEROPACKET - D_OFFSET
	mov [BK_ - D_OFFSET], word 8					;blank out 8 sectors at a time
	mov [RDF - D_OFFSET], dword 0
	push EAX
	mov eax, [selectedSpaceLBA - D_OFFSET]
	mov [RDF - D_OFFSET], eax
	pop eax
		
	mov eax, [RDF - D_OFFSET]
	int 0x13
	mov [BK_ - D_OFFSET], word 1
	;jnc .DONEA
	;xor ax, ax
	;int 0x13
	;dec di
	;cmp di, 0
	;je DiskErrorHDDA
	;jmp .RETRYA
.DONEA:
	popa

	mov ecx, [newDataStartOffset - D_OFFSET]	;0x3FFFF

.clearLoop:
	mov [originalECX - D_OFFSET], ecx

	pusha
	pusha
	


	pusha
	mov di, 5
.RETRYB:
	mov ah, 0x43
	mov al, 0
	mov dl, byte [HDDNumber - D_OFFSET]
	mov si, HDDZEROPACKET - D_OFFSET
	mov [BK_ - D_OFFSET], word 8					;blank out 8 sectors at a time
	mov [RDF - D_OFFSET], ecx
	push eax
	mov eax, [selectedSpaceLBA - D_OFFSET]
	add [RDF - D_OFFSET], eax
	pop eax
	int 0x13
	mov [BK_ - D_OFFSET], word 1
	;jnc .DONEB
	;xor ax, ax
	;int 0x13
	;dec di
	;cmp di, 0
	;je DiskErrorHDDB
	;jmp .RETRYB
.DONEB:
	popa

	push ecx
	and ecx, 0x10
	cmp ecx, 0
	jne .skipMultiplyAndDisplay
	pop ecx

	mov eax, 0
	;mov edx, ECX
	;mov ecx, [newDataStartOffset - D_OFFSET]
	;sub ecx, edx
	mov edx, 0
.multiplyLoop:
	add eax, 56
	cmp eax, 56
	jl .overflowHappened
	jmp .doneOverflowHandler
.overflowHappened:
	inc edx					;EDX:EAX creates a 64 bit division
.doneOverflowHandler:
	dec ecx 
	cmp ecx, 0
	jne .multiplyLoop

	;sectors done * 56

	;EDX:EAX is set correctly for division here
	mov esi, [newDataStartOffset - D_OFFSET]
	div esi

	; EAX = total sectors / sectors done * 58
	

	mov si, lineReset - D_OFFSET
	pusha
	call puts
	popa
	
	;put the spinning character in place
	pusha
	mov bh, 0
	mov bl, byte [progressProgress - D_OFFSET]
	mov al, byte [progressCycle - D_OFFSET + bx]
	mov [Stage678Footer - D_OFFSET + 4], al
	inc bl
	cmp bl, 136
	jne .dontLoopAround
	mov bl, 8

.dontLoopAround:
	mov [progressProgress - D_OFFSET], byte bl
	popa

	;spinning character now in place...

	;show spinning character and the word 'progress'
	pusha
	mov si, Stage678Footer - D_OFFSET
	call puts
	popa 

	mov edi, eax
	mov eax, 56
	sub eax, edi

	;store the division from earlier
	mov [mulresult - D_OFFSET], eax

	;if 0 segments are to be drawn, skip it, if over 56 are to be drawn, fill it
	mov ecx, eax
	cmp ecx, 0
	je .noLoop
	cmp ecx, 56
	jge .setToECXMax
	
.outLoop:
	mov si, percentDoneBlock - D_OFFSET
	pusha
	call puts
	popa
	loop .outLoop
	popa

	pusha
	call .anotherloop
	popa

.noLoop
	popa

	mov ecx, [originalECX - D_OFFSET]

	sub ecx, 8		;8 sectors at a time
	cmp ecx, 8				;cut early so we do not go past it next time
	jg .clearLoop

	jmp Stage7

.skipMultiplyAndDisplay:
	pop ecx
	popa
	jmp .noLoop

.setToECXMax:
	mov ecx, 56
	jmp .outLoop

.nost:
	ret

;EAX = the multiplication result
.anotherloop:
	mov eax, [mulresult - D_OFFSET]
	mov ecx, 56
	sub ecx, eax

	cmp eax, 56
	jge .nost
	
	cmp ecx, 56
	jge .AsetToECXMax	;integer underflow?

.lastcheck:

	cmp ecx, 0
	je .AnoLoop
	
.AoutLoop:
	mov si, percentLeftBlock - D_OFFSET
	pusha
	call puts
	popa
	loop .AoutLoop

	ret

.AnoLoop:
	ret

.AskipMultiplyAndDisplay:
	jmp .AnoLoop

.AsetToECXMax:
	mov ecx, 0			;it must have underflowed to get to here
	ret