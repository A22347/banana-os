bits 64

global x87Avaliable
global x87Open
global x87Read
global x87Write

x87Avaliable:
	mov rax, 0x1
	cpuid
	test rdx, 1<<0
	jz .nox87
	mov rax, 1
	ret
.nox87:
	mov rax, 0
	ret
	
value_37F dw 0x37F
value_37E dw 0x37E
value_37A dw 0x37A

x87Open:
	finit
	mov rax, cr0
	and ax, 0xFFFB		;clear coprocessor emulation CR0.EM
	or ax, 0x22			;set coprocessor monitoring  CR0.MP + numeric errors (CRO.NE)
	mov cr0, rax

	clts				;clear task switched bit

	;allow interrupts for div by zero and invalid operands
	fldcw [value_37A]   ; writes 0x37a, both division by zero and invalid operands cause exceptions.

	ret
	
x87Read:
	fsave [rdi]
	ret
	
x87Write:
	frstor [rdi]
	ret