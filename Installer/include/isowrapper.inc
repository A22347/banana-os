;IN
;	EBX = pointer to string name
;	ECX = length of string name
;OUT
;	EAX = file location on disk
;	ECX = file size on disk
;	EDX = set to 0 on SUCCESS, 1 on FAILURE

readFromCD:
	mov ax, 0
	mov fs, ax
	mov [0x51A0], sp
	mov eax, esp
	mov cr3, eax
	cli

	push es
	;copy name
	mov edi, 0x5100
	mov esi, EBX
	mov ax, 0
	mov es, ax
	rep movsb
	pop es

	;copy CD-ROM
	mov [blkcnt - D_OFFSET], word 32		;32 sectors = 64KB
	mov [db_add - D_OFFSET], word 0x0		;stick it at 0x8000:0
	mov [db_seg - D_OFFSET], word 0x8000	;stick it at 0x8000:0
	mov [d_lba  - D_OFFSET], dword 16		;start at 16

	mov si, DAPACK - D_OFFSET				; address of "disk address packet"
	mov ah, 0x42							; AL is unused
	mov dl, [driveNumber - D_OFFSET]		; drive number 0 (OR the drive # with 0x80)
	int 0x13

	;copy driver
	mov si, isoDriver		;source	
	mov di, 0x5800			;destination
	mov ax, 0x0
	mov es, ax				;destination segment
	mov cx, 4096 * 2			;count
	rep movsb				;copy driver to 0x6000

	call enable_A20

	xor ax, ax 
	mov ds, ax
	mov es, ax
	mov ss, ax
	mov fs, ax

	;copy drive number
	mov al, [driveNumber - D_OFFSET]
	mov byte [fs:0x5000], AL

	mov [fs:0x500], dword 0
	mov [fs:0x504], dword 0

	;32 bit p-mode code
	mov [fs:0x508], word 0xFFFF
	mov [fs:0x50A], word 0
	mov [fs:0x50C], word 0x9A00
	mov [fs:0x50E], word 0xCF
	
	;32 bit p-mode code
	mov [fs:0x510], word 0xFFFF
	mov [fs:0x512], word 0
	mov [fs:0x514], word 0x9200
	mov [fs:0x516], word 0xCF
	
	;16 bit p-mode code
	mov [fs:0x518], word 0xFFFF
	mov [fs:0x51A], word 0
	mov [fs:0x51C], word 0x9A00
	mov [fs:0x51E], word 0x0F

	;16 bit p-mode data
	mov [fs:0x520], word 0xFFFF
	mov [fs:0x522], word 0
	mov [fs:0x524], word 0x9200
	mov [fs:0x526], word 0x0F

	mov [fs:0x528], word 0x27
	mov [fs:0x52A], dword 0x500

	mov eax, 0x528
	lgdt [eax]        ; load GDT into GDTR

	mov ax, 0x10
    mov ds, ax
    mov es, ax
    mov fs, ax
    mov gs, ax
    mov ss, ax
    jmp .flush2
	.flush2:

	cli
	mov eax, cr0 
	or al, 1		; set PE (Protection Enable) bit in CR0 (Control Register 0)
	mov cr0, eax

	jmp 0x8:ProtectedModeForCD + 0x7C00

align 8
bits 32
ProtectedModeForCD:
	cli
	mov	ax, 0x10
	mov	ds, ax
	mov	ss, ax
	mov	es, ax
	mov	esp, 0x3F0000  ;4MB
	cld

	;	0x5200 used internally by C code
	;
	;IN:
	;	0x5000 = drive number	(BYTE)
	;	0x5100 = filename		(STRING)
	;	0x5800 = driver code	(CODE)
	;OUT:
	;	0x5004 = start LBA		(DWORD)
	;	0x5008 = file size		(DWORD)
	;	0x500C = success?		(DWORD)

	mov eax, 1
	call isoDriver

	;VMWARE GETS TO AT LEAST HERE

	mov ebp, [0x5004]
	mov ecx, [0x5008]
	mov edx, [0x500C]

	cli ; 8.9.2. Step 1.

	mov eax,cr0 ; 8.9.2. Step 2.
	and eax,0x7FFFFFFF	;0x7FFFFFFF
	mov cr0,eax

	;jmp 0x18:prot16 + 0x7c00 ; 8.9.2. Step 3.
	jmp 0x18:prot16 + 0x7C00

[BITS 16]

prot16:
	mov ax,0x0020 ; 8.9.2. Step 4.
	mov ds,ax
	mov es,ax
	mov fs,ax
	mov gs,ax
	mov ss,ax

	mov eax,cr0 ; 8.9.2. Step 2.
	and al,0xFE	
	mov cr0,eax	;FREEZE!

	;jmp 0x50:0x50
	
	jmp word 0x7C0:real16		; 8.9.2. Step 7.

align 16
bits 16
real16:
	mov ax, 0x7C0
	mov ds, ax
	mov es, ax
	mov fs, ax
	mov gs, ax

	mov ax, 0x500
    mov ss, ax
    mov sp, 0			;will wrap around to 0xFFFF and on...
    sti

	;restore old stack (we must saved it before this whole journey)
	mov esp, 0
	mov sp, [0x51A0]
	mov eax, cr3
	mov esp, eax
	pop BX

	xor ax, AX
	mov fs, AX

	mov eax, [fs:0x5004]
	mov ecx, [fs:0x5008]
	mov edx, [fs:0x500C]

	cmp [cdReturnSpot], byte 0
	je Stage55
	cmp [cdReturnSpot], byte 1
	je cdR
	cmp [cdReturnSpot], byte 2
	je dfltmbrread

	push fs	
	push BX
	retf




	
bits 16
