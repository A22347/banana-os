#include <gui/guilink.h>
#include <gui/terminal.h>
#include <hal/mouse.h>
#include <hal/display.h>
#include <core/memory.h>

void guiReactToSecondPassed ()
{

}

void guiReactToTimerTick ()
{

}

void guiReactToKeyboardPress (uint16_t scancode)
{

}

void guiReactToKeyboardUp (uint16_t scancode)
{

}

void guiReactToKeyboardDown (uint16_t scancode)
{

}

void guiReactToMouseStart ()
{
	static bool firstMove = true;
	if (firstMove) {
		firstMove = false;
	} else {
		restoreMouse();
	}
}

void guiReactToMouseMove (int x, int y)
{
	mouse_move (x, y);
}

void guiReactToMiddleMouseButton ()
{

}

void guiReactToMouseDown ()
{

}

void guiReactToMouseDownFirstTime ()
{

}

void guiReactToMouseUp ()
{

}

void guiReactToRightClick ()
{

}

void guiReactToScrollWheel (int x, int y)
{

}

void guiReactToMouseEnd ()
{
	storeMouse ();
	drawMouse ();
}

void guiReactToMouseDrag ()
{

}

int guiGetFocusedWindow ()
{
	return 1;
}