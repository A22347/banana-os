//
//  conversion.c
//  Banana libc
//
//  Created by Alex Boxall on 17/9/18.
//  Copyright © 2018 Alex Boxall. All rights reserved.
//

#ifndef _libk

#include "time.h"
#include "stdlib.h"
#include "stdio.h"
#include <stdlib.h>
#include <stdio.h>

time_t ___datetimeToSeconds (struct tm d);
struct tm ___secondsToDatetime (time_t data);


char *asctime(const struct tm *timeptr) {
    static const char wday_name[7][4] = {
        "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"
    };
    static const char mon_name[12][4] = {
        "Jan", "Feb", "Mar", "Apr", "May", "Jun",
        "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
    };
    char* result = calloc(256, 1);

    //this doesn't put the zeros in...
    sprintf(result, "%.3s %.3s%3d %2d:%.2d:%.2d %d\n",
                    wday_name[timeptr->tm_wday],
                    mon_name[timeptr->tm_mon],
                    timeptr->tm_mday, timeptr->tm_hour,
                    timeptr->tm_min, timeptr->tm_sec,
                    1900 + timeptr->tm_year);

    return result;
}


char *ctime(const time_t *timer) {
    return asctime(localtime(timer));
}

struct tm *gmtime(const time_t *timer) {
    //The gmtime function converts the calendar time pointed to by timer into a broken- down time, expressed as UTC.
    //Returns
    //The gmtime function returns a pointer to the broken-down time, or a null pointer if the specified time cannot be converted to UTC.
    
    return NULL;
}

struct tm *localtime(const time_t *timer) {
    //The localtime function converts the calendar time pointed to by timer into a broken-down time, expressed as local time.
    //Returns
    //The localtime function returns a pointer to the broken-down time, or a null pointer if the specified time cannot be converted to local time.
    struct tm dt = ___secondsToDatetime(*timer);
    struct tm* dtp = malloc(sizeof(struct tm));
    *dtp = dt;
    return dtp;
}

void ___strftime_write (char chr, char * restrict buffer, size_t size, size_t max) {
    if (size >= max) {
        return;
    }
    buffer[size] = chr;
}

size_t strftime(char * restrict s,
                size_t maxsize,
                const char * restrict format,
                const struct tm * restrict timeptr) {
    //must be locale specific in the future
    static const char wday_name[7][4] = {
        "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"
    };
    static const char wday_name_full[7][12] = {
        "Sunday", "Mondau", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"
    };
    static const char mon_name[12][4] = {
        "Jan", "Feb", "Mar", "Apr", "May", "Jun",
        "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
    };
    static const char mon_name_full[12][12] = {
        "January", "February", "March", "April", "May", "June",
        "July", "August", "September", "October", "November", "December"
    };
    
    size_t size = 0;
    for (int i = 0; format[i]; ++i) {
        if (format[i] == '%') {
            ++i;
            if (format[i] == 'E') {
                //alternative format...
                ++i;
            }
            if (format[i] == 'O') {
                //second alternative format...
                ++i;
            }
            switch (format[i]) {
                case 'a':
                    //locale’s abbreviated weekday name (e.g. "Fri")
                    for (int j = 0; wday_name[timeptr->tm_wday][j]; ++j) {
                        ___strftime_write(wday_name[timeptr->tm_wday][j], s, size++, maxsize);
                    }
                    break;
                    
                case 'A':
                    //locale's weekday name (e.g. "Friday")
                    for (int j = 0; wday_name_full[timeptr->tm_wday][j]; ++j) {
                        ___strftime_write(wday_name_full[timeptr->tm_wday][j], s, size++, maxsize);
                    }
                    break;
                    
                case 'b':
                case 'h':
                    //locale’s abbreviated month name (e.g. "Jan")
                    for (int j = 0; mon_name[timeptr->tm_mon][j]; ++j) {
                        ___strftime_write(mon_name[timeptr->tm_mon][j], s, size++, maxsize);
                    }
                    break;
                    
                case 'B':
                    //locale's month name (e.g. "March")
                    for (int j = 0; mon_name_full[timeptr->tm_mon][j]; ++j) {
                        ___strftime_write(mon_name_full[timeptr->tm_mon][j], s, size++, maxsize);
                    }
                    break;
                    
                case 'c':
                    //full prepresentation
                    //that strange condition removes the newline
                    for (int j = 0; asctime(timeptr)[j]; ++j) {
                        ___strftime_write(asctime(timeptr)[j] == '\n' ? 0 : asctime(timeptr)[j], s, size++, maxsize);
                    }
                    break;
                
                case 'C':
                {
                    char buffer[8];
                    sprintf(buffer, "%d", (timeptr->tm_year + 1900) / 100);
                    for (int j = 0; buffer[j]; ++j) {
                        ___strftime_write(buffer[j], s, size++, maxsize);
                    }
                    break;
                }
                    
                case 'd':
                {
                    char buffer[8];
                    sprintf(buffer, "%02d", timeptr->tm_mday);
                    for (int j = 0; buffer[j]; ++j) {
                        ___strftime_write(buffer[j], s, size++, maxsize);
                    }
                    break;
                }
                    
                case 'D':
                {
                    char buffer[32];
                    sprintf(buffer, "%02d/%02d/%d", timeptr->tm_mday, timeptr->tm_mon, timeptr->tm_year + 1900);
                    for (int j = 0; buffer[j]; ++j) {
                        ___strftime_write(buffer[j], s, size++, maxsize);
                    }
                    break;
                }
                    
                case 'e':
                {
                    char buffer[8];
                    sprintf(buffer, "%2d", timeptr->tm_mday);
                    for (int j = 0; buffer[j]; ++j) {
                        ___strftime_write(buffer[j], s, size++, maxsize);
                    }
                    break;
                }
                    
                case 'F':
                {
                    char buffer[32];
                    sprintf(buffer, "%d-%02d-%02d", timeptr->tm_year  + 1900, timeptr->tm_mon, timeptr->tm_mday);
                    for (int j = 0; buffer[j]; ++j) {
                        ___strftime_write(buffer[j], s, size++, maxsize);
                    }
                    break;
                }
                    
                case 'g':
                {
                    char buffer[8];
                    sprintf(buffer, "%d", (timeptr->tm_year + 1900) % 100);
                    for (int j = 0; buffer[j]; ++j) {
                        ___strftime_write(buffer[j], s, size++, maxsize);
                    }
                    break;
                }
                    
                case 'G':
                {
                    char buffer[8];
                    sprintf(buffer, "%d", timeptr->tm_year + 1900);
                    for (int j = 0; buffer[j]; ++j) {
                        ___strftime_write(buffer[j], s, size++, maxsize);
                    }
                    break;
                }
                    
                case 'H':
                {
                    char buffer[8];
                    sprintf(buffer, "%02d", timeptr->tm_hour);
                    for (int j = 0; buffer[j]; ++j) {
                        ___strftime_write(buffer[j], s, size++, maxsize);
                    }
                    break;
                }
                    
                case 'I':
                {
                    char buffer[8];
                    sprintf(buffer, "%02d", timeptr->tm_hour % 12 == 0 ? 12 : timeptr->tm_hour % 12);
                    for (int j = 0; buffer[j]; ++j) {
                        ___strftime_write(buffer[j], s, size++, maxsize);
                    }
                    break;
                }
                    
                case '%':
                    ___strftime_write('%', s, size++, maxsize);
                    break;
                    
                default:
                    ___strftime_write(format[i], s, size++, maxsize);
                    break;
            }
        } else {
            ___strftime_write(format[i], s, size++, maxsize);
        }
    }
    ___strftime_write(0, s, size++, maxsize);

    return 0;
}

#endif