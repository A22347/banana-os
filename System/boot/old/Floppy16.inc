
;*******************************************************
;
;	Floppy16.inc
;		Floppy drive interface routines
;
;	OS Development Series
;*******************************************************

%ifndef __FLOPPY16_INC_67343546FDCC56AAB872_INCLUDED__
%define __FLOPPY16_INC_67343546FDCC56AAB872_INCLUDED__

bits	16

bsDriveNumber db 0x80

;A ROUTINE IN FAT12.INC WILL COPY THE CORRECT DATA IN
_bpb:
times 3 db 0
bpbOEM			db "BANANAOS"
bpbBytesPerSector:  	DW 512
bpbSectorsPerCluster: 	DB 1
bpbReservedSectors: 	DW 2
bpbNumberOfFATs: 	DB 2
bpbRootEntries: 	DW 0x200
bpbTotalSectors: 	DW 61440
bpbMedia: 		DB 0xf8  ;; 0xF1
bpbSectorsPerFAT: 	DW 239
bpbSectorsPerTrack: 	DW 18
bpbHeadsPerCylinder: 	DW 2
bpbHiddenSectors: 	DD 1
bpbTotalSectorsBig:     DD 0

;FAT32 EBPB
	_bpb.logicalSectorsPerFAT		dd 0x3C1
	_bpb.driveDescription			dw 0
	_bpb.version					dw 0
	_bpb.clusterOfRootDir			dd 2
	_bpb.fsInfoSectorSector			dw 1
	_bpb.sectorOfBootsectorCopy		dw 6
	times 12 db 0
	times 7 db 0
	times 11 db 0
	times 8 db 0

datasector  dw 0x0000
cluster     dd 0x0000

absoluteSector db 0x00
absoluteHead   db 0x00
absoluteTrack  db 0x00

;************************************************;
; Convert CHS to LBA
; LBA = (cluster - 2) * sectors per cluster
;************************************************;

ClusterLBA:
;FAT32 change
	xor	cx, cx
	mov     cl, BYTE [bpbSectorsPerCluster]
	sub ax, 2
	add ax, [dataLocationStart]
	ret

          ;sub     ax, 0x0002                          ; zero base cluster number
          ;xor     cx, cx
          ;mov     cl, BYTE [bpbSectorsPerCluster]     ; convert byte to word
          ;mul     cx
          ;add     ax, WORD [datasector]               ; base data sector
          ret

;************************************************;
; Convert LBA to CHS
; AX=>LBA Address to convert
;
; absolute sector = (logical sector / sectors per track) + 1
; absolute head   = (logical sector / sectors per track) MOD number of heads
; absolute track  = logical sector / (sectors per track * number of heads)
;
;************************************************;

LBACHS:
          xor     dx, dx                              ; prepare dx:ax for operation
          div     WORD [bpbSectorsPerTrack]           ; calculate
          inc     dl                                  ; adjust for sector 0
          mov     BYTE [absoluteSector], dl
          xor     dx, dx                              ; prepare dx:ax for operation
          div     WORD [bpbHeadsPerCylinder]          ; calculate
          mov     BYTE [absoluteHead], dl
          mov     BYTE [absoluteTrack], al
          ret

	
;************************************************;
; Reads a series of sectors
; CX=>Number of sectors to read
; AX=>Starting sector
; ES:EBX=>Buffer to read to
;************************************************;

ReadSectors:
	cmp cx, 0x3F
	jl .MAIN
	mov cx, 0x3F
     .MAIN:
          mov     di, 0x0005                          ; five retries for error
     .SECTORLOOP:
          push    ax
          push    bx
          push    cx
		  
          ;call    LBACHS                              ; convert starting sector to CHS
          ;mov     ah, 0x02                            ; BIOS read sector
          ;mov     al, 0x01                            ; read one sector
          ;mov     ch, BYTE [absoluteTrack]            ; track
          ;mov     cl, BYTE [absoluteSector]           ; sector
          ;mov     dh, BYTE [absoluteHead]             ; head
          ;mov     dl, BYTE [bsDriveNumber]            ; drive
          ;int     0x13                                ; invoke BIOS
		  
		  push ax
		  mov eax, 0
		  pop ax
		  
		  add eax, [bpbHiddenSectors]

		  mov [blkcnt], word cx
		  mov [d_lba ], dword eax
		  mov [db_add], word bx
		  mov [db_seg], word es
	
		  mov ah, 0x42
		  mov dl, [bsDriveNumber]
		  mov si, DAPACK
		  
		  int 0x13
		  
          jnc     .SUCCESS                            ; test for read error
		  
		  ;jmp $
		  
          xor     ax, ax                              ; BIOS reset disk
          int     0x13                                ; invoke BIOS
          dec     di                                  ; decrement error counter
          pop     cx
          pop     bx
          pop     ax
          jnz     .SECTORLOOP                         ; attempt to read again
          int     0x18
     .SUCCESS:
          pop     cx
          pop     bx
          pop     ax
          add     bx, WORD [bpbBytesPerSector]        ; queue next buffer
          inc     ax                                  ; queue next sector
          loop    .MAIN                               ; read next sector
          ret
		 
align 4
DAPACK:
	db	0x10
	db	0
blkcnt:	dw	1		; int 13 resets this to # of blocks actually read/written
db_add:	dw	0x200		; memory buffer destination address (0:7c00)
db_seg:	dw	0		; in memory page zero
d_lba:	dd	0		; put the lba to read in this spot
	dd	0		; more storage bytes only for big lba's ( > 4 bytes )

%endif		;__FLOPPY16_INC_67343546FDCC56AAB872_INCLUDED__

