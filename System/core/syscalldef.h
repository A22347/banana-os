
/*
	This is the ONLY file synced between kernel space and user space

	It contains:
		The system call list (the main part of the file)
*/

#ifndef __SYSCALLDEF_H__
#define __SYSCALLDEF_H__

#ifdef __cplusplus
extern "C" {
#endif

#include <stdbool.h>
#include <stdint.h>

	enum SystemCallList
	{
		SC_NULL_FOR_ALIGNMENT_OF_LATER_CALLS_IN_ENUM_ONLY_7,			///< NOT Deprecated, gives malloc area base and size to the program
		SC_Panic,			///< Causes the system to abort
		SC_GetPID,			///< Gets the processes ID
		SC_Log,				///< Logs a message to the filesystem
		SC_NULL_FOR_ALIGNMENT_OF_LATER_CALLS_IN_ENUM_ONLY_8,
		SC_NULL_FOR_ALIGNMENT_OF_LATER_CALLS_IN_ENUM_ONLY_9,
		SC_NULL_FOR_ALIGNMENT_OF_LATER_CALLS_IN_ENUM_ONLY_12,
		SC_NULL_FOR_ALIGNMENT_OF_LATER_CALLS_IN_ENUM_ONLY_10,
		SC_NULL_FOR_ALIGNMENT_OF_LATER_CALLS_IN_ENUM_ONLY_11,
		SC_NULL_FOR_ALIGNMENT_OF_LATER_CALLS_IN_ENUM_ONLY_5,
		SC_NULL_FOR_ALIGNMENT_OF_LATER_CALLS_IN_ENUM_ONLY_6,
		SC_GetTime,
		SC_NumberOfArguments = 49,
		SC_GetArgument = 50,
		SC_NULL_FOR_ALIGNMENT_OF_LATER_CALLS_IN_ENUM_ONLY_3,
		SC_NULL_FOR_ALIGNMENT_OF_LATER_CALLS_IN_ENUM_ONLY_4,
		SC_Username,
		SC_Abort,
		SC_Reboot,
		SC_Shutdown,
		SC_LauchTerminalSession,
		SC_ExitTerminalSession,
		SC_NULL_FOR_ALIGNMENT_OF_LATER_CALLS_IN_ENUM_ONLY_22,
		SC_System,									//SYSCALL 60
		SC_Rename,
		SC_NewFileDelete,
		SC_GetCwd,
		SC_SetCwd,
		SC_GetEnv,
		SC_NULL_FOR_ALIGNMENT_OF_LATER_CALLS_IN_ENUM_ONLY_1,
		SC_NULL_FOR_ALIGNMENT_OF_LATER_CALLS_IN_ENUM_ONLY_2,
		SC_NULL_FOR_ALIGNMENT_OF_LATER_CALLS_IN_ENUM_ONLY_13,
		SC_NULL_FOR_ALIGNMENT_OF_LATER_CALLS_IN_ENUM_ONLY_14,
		SC_NULL_FOR_ALIGNMENT_OF_LATER_CALLS_IN_ENUM_ONLY_15,
		SC_NULL_FOR_ALIGNMENT_OF_LATER_CALLS_IN_ENUM_ONLY_16,
		SC_NULL_FOR_ALIGNMENT_OF_LATER_CALLS_IN_ENUM_ONLY_17,
		SC_NULL_FOR_ALIGNMENT_OF_LATER_CALLS_IN_ENUM_ONLY_18,
		SC_NULL_FOR_ALIGNMENT_OF_LATER_CALLS_IN_ENUM_ONLY_19,
		SC_NULL_FOR_ALIGNMENT_OF_LATER_CALLS_IN_ENUM_ONLY_20,
		SC_NULL_FOR_ALIGNMENT_OF_LATER_CALLS_IN_ENUM_ONLY_21,
		SC_BlackMagic,
		SC_Sleep,
		SC_NULL_FOR_ALIGNMENT_OF_LATER_CALLS_IN_ENUM_ONLY_0,
		SC_GetStdstreamObjectForProcess,				//SYSCALL 80
		SC_WriteFileOrPipeObjectFromFilename,
		SC_ReadFileOrPipeObjectFromFilename,
		SC_OpenFileFromFilename,
		SC_Time,
		SC_Tell,
		SC_Seek,
		SC_OpenDir,
		SC_ReadDir,
		SC_CloseDir,
		SC_Execve,										//SYSCALL 90
		SC_GUIEXE_VidScanlineWidth,
		SC_GUIEXE_VidVisibleWidth,
		SC_GUIEXE_VidHeight,
		SC_GUIEXE_VidFlushBuffer,
		SC_Spawn,
		SC_Wait,
		SC_Fork,
		SC_Pipe,
		SC_AllocateSharedMemory,
		SC_AllocateSharedMemoryP2,
		SC_Sbrk,
		SC_Telldir,
		SC_Seekdir,
		SC_CloseFile,
		SC_FileSize,
		SC_MakeDir,
	};

#ifdef __cplusplus
}
#endif

#endif