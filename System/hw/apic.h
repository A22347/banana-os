#pragma once

#define APIC_REGISTER_LVT_TIMER					0x320
#define APIC_REGISTER_TIMER_INITCNT				0x380
#define APIC_REGISTER_TIMER_CURRCNT				0x390
#define APIC_REGISTER_TIMER_DIV					0x3E0
#define APIC_REGISTER_LVT_MASKED				0x10000
#define APIC_REGISTER_LVT_TIMER_MODE_PERIODIC	0x20000

#include <stdint.h>
#include <hw/registers.h>		//MSRs

void ioapicSetIRQ (uint8_t irq, uint64_t apic_id, uint8_t vector, uint32_t base);
void enableAPIC ();
void sendEOIForAPIC ();
uint32_t cpuGetAPICBase ();