//
//  threads.h
//  Banana libc
//
//  Created by Alex Boxall on 11/7/18.
//  Copyright © 2018 Alex Boxall. All rights reserved.
//

#ifndef threads_h
#define threads_h

// OPTIONAL AS PER THE STANDARD (http://www.open-std.org/jtc1/sc22/wg14/www/docs/n1570.pdf )

#if defined(__cplusplus)
#define restrict			//fixes some 'fun' errors
extern "C" {
#endif
    
#define thread_local _Thread_local
    
#if defined(__cplusplus)
}
#endif

#endif /* threads_h */
