#include <hal/power.h>
#include <fs/vfs.h>
#include <hw/acpi.h>
#include <hal/display.h>
#include <libk/string.h>
#include <gui/terminal.h>
#include <gui/fonts.h>

void turnOffWhiteScreen()
{
	int loc = 0;
	for (int y = 0; y < TOTAL_HEIGHT - 100; ++y) {
		for (int x = 0; x < TOTAL_WIDTH; ++x) {
			putpixelr(loc + x, 0xFFFFFF);
		}
		loc += ONE_LINE;
	}
	for (int y = 0; y < 12; ++y) {
		for (int x = 0; x < TOTAL_WIDTH; ++x) {
			putpixelr(loc + x, 0x0000C0);
		}
		loc += ONE_LINE;
	}
	for (int y = 0; y < 100 - 12; ++y) {
		for (int x = 0; x < TOTAL_WIDTH; ++x) {
			putpixelr(loc + x, 0xC0C0C0);
		}
		loc += ONE_LINE;
	}
}

void endBananaSession()
{
	int br;
	vfs_file_t f;
	vfs_open(&f, "C:/Banana/System/SHTDWNOK.SYS", vfs_file_mode_write);
	vfs_write(&f, (uint8_t*) ".", 1);
	vfs_close(&f);
}

void writeStartupText(char* text);

void reboot()
{
	turnOffWhiteScreen();
	writeStartupText("Restarting...");

	endBananaSession();
	writeDevice(powerManagerDevice, 0, 0, 0);

	int loc = 0;
	for (int y = 0; y < TOTAL_HEIGHT; ++y) {
		for (int x = 0; x < TOTAL_WIDTH; ++x) {
			putpixelr(loc + x, 0);
		}
		loc += ONE_LINE;
	}

	int datalen = strlen("PLEASE RESTART YOUR COMPUTER");

	int x = TOTAL_WIDTH / 2 - (datalen * 8) / 2;
	int y = TOTAL_HEIGHT / 2 - 4;

	for (int i = 0; i < datalen; i++) {
		terminal_putentryat_pixel_raw("PLEASE RESTART YOUR COMPUTER"[i], 0xFFFFFF, 0, x, y);
		x += FontWidths[0][(int) "PLEASE RESTART YOUR COMPUTER"[i]] + 1;
	}

	asm("cli");
	while (1) {
		//cause a triple fault in here...
	}
}

void shutdown ()
{
	turnOffWhiteScreen();
	writeStartupText("Shutting Down...");

	endBananaSession();
	writeDevice(powerManagerDevice, 1, 0, 0);

	int loc = 0;
	for (int y = 0; y < TOTAL_HEIGHT; ++y) {
		for (int x = 0; x < TOTAL_WIDTH; ++x) {
			putpixelr(loc + x, 0);
		}
		loc += ONE_LINE;
	}

	int datalen = strlen("YOU MAY NOW TURN OFF YOUR COMPUTER");

	int x = TOTAL_WIDTH / 2 - (datalen * 8) / 2;
	int y = TOTAL_HEIGHT / 2 - 4;

	for (int i = 0; i < datalen; i++) {
		terminal_putentryat_pixel_raw("YOU MAY NOW TURN OFF YOUR COMPUTER"[i], 0xFFFFFF, 0, x, y);
		x += FontWidths[0][(int) "YOU MAY NOW TURN OFF YOUR COMPUTER"[i]] + 1;
	}

	asm("cli");
	while (1);
}