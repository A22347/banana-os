#include <core/hwsecurity.h>
#include <core/cpuid.h>
#include <debug/log.h>

void umipEnableIfAvaliable ()
{
	logk("umipEnableIfAvaliable\n");
	size_t eax, ebx, ecx, edx;
	cpuid (CPUID_GETEXTENDED, &eax, &ebx, &ecx, &edx);
	if (edx & CPUID_UMIP_COMPARE_ECX) {
		size_t val;
		asm volatile ("mov %%cr4, %0" : "=r"(val));
		val |= 1 << 11;
		asm volatile ("mov %0, %%cr4" :: "r"(val));

		logk ("UMIP Eisbled\n");
	} else {
		logk ("No UMIP\n");
	}
}

void tscUsermodeDisable ()
{
	logk("tscUsermodeDisable\n");

	size_t eax, ebx, ecx, edx;
	cpuid (CPUID_GETFEATURES, &eax, &ebx, &ecx, &edx);
	if (edx & CPUID_TSC_COMPARE_EDX) {
		size_t val;
		asm volatile ("mov %%cr4, %0" : "=r"(val));
		val |= 1 << 2;
		asm volatile ("mov %0, %%cr4" :: "r"(val));

		logk ("TSC Usermode Disbled\n");
	} else {
		logk ("No TSC\n");
	}
}

void smepEnableIfAvaliable ()
{
	logk("smepEnableIfAvaliable\n");

	size_t eax, ebx, ecx, edx;
	cpuid (CPUID_GETEXTENDED, &eax, &ebx, &ecx, &edx);
	if (ebx & CPUID_SMEP_COMPARE_EBX) {
		size_t val;
		asm volatile ("mov %%cr4, %0" : "=r"(val));
		val |= 1 << 20;
		asm volatile ("mov %0, %%cr4" :: "r"(val));

		logk ("SMEP Enabled\n");
	} else {
		logk ("No SMEP\n");
	}
}

bool cpuHasMSR ()
{
	size_t eax, ebx, ecx, edx;
	cpuid (CPUID_GETFEATURES, &eax, &ebx, &ecx, &edx);
	return edx & CPUID_FLAG_MSR_EDX;
}

bool nxAvaliable = false;
void nxEnableIfAvaliable ()
{
	logk ("Checking for NX bit support.\n");
	nxAvaliable = false;

	uint32_t lo;
	uint32_t hi;

	//the CPU must have MSR support to turn it on
	if (!cpuHasMSR ()) {
		logk ("No MSR support!");
		return;
	}

	//ugly Intel hack time for dodgey BIOSes
	{
		//read Intel-specific MSR
		asm volatile ("rdmsr" : "=a"(lo), "=d"(hi) : "c"(0x01A0));

		//check if NX bit disable enable bit is set to 1
		if (hi & 4) {
			logk ("The dodgey Intel bit is set. Let's disable it.\n");
			//if it is, disable the disable bit to enable the check enable bit
			hi &= ~4;
			asm volatile ("wrmsr" :: "a"(lo), "d"(hi), "c"(0x01A0));
		}
	}

	size_t eax, ebx, ecx, edx;
	cpuid (CPUID_INTELEXTENDED, &eax, &ebx, &ecx, &edx);
	if (!(edx & CPUID_NXBIT_COMPARE_EDX)) {
		//NX not supported at all
		logk ("No NX support!");
		return;
	}

	logk ("It seems we can have it if we enable it.\n");

	//must be 64-bit to support it
	if (sizeof (size_t) != 8) {
		logk ("Not x86-64!\n");
		return;
	}

	//read EFER register
	asm volatile ("rdmsr" : "=a"(lo), "=d"(hi) : "c"(0xC0000080));

	//set NX bit enable + write back
	lo |= (1 << 11);
	asm volatile ("wrmsr" :: "a"(lo), "d"(hi), "c"(0xC0000080));

	logk ("We should have NX enabled now.\n");
	nxAvaliable = true;
}

void enableHardwareSecuritySystems ()
{
	//smepEnableIfAvaliable ();
	tscUsermodeDisable ();
	umipEnableIfAvaliable ();
	nxEnableIfAvaliable ();
}