
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main (int argc, char *argv[])
{
	char filename[256];
	memset(filename, 0, 256);
	if (argc != 2) {
		printf("The syntax of the command is incorrect.\n\n");
		exit(1);
	} else {
		strcpy(filename, argv[1]);
	}

	char buffer[1024];
	FILE* f = fopen(filename, "r");
	fread(buffer, 1, 1023, f);
	fclose(f);

	puts(buffer);

	return 0;
}
 