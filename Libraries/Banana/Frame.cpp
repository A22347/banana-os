
#include <Banana/Frame.hpp>

Frame::Frame (Window& parent, int _x, int _y, int _width, int _height) : Widget (&parent)
{
	masterwindow = &parent;
	masterwindow->addWidgetChild (this);

	x = _x;
	y = _y;
	w = _width;
	h = _height;
}

// these functions shouldn't be called by function code, but instead
// the widget code that needs to operate on a IS_A window (when it is actually a frame)

void Frame::setTitle (char* nTitle)
{
	masterwindow->setTitle (nTitle);
}

char* Frame::getTitle ()
{
	return masterwindow->getTitle ();
}

void Frame::setPosition (short int nX, short int nY)
{
	masterwindow->setPosition (nX, nY);
}

void Frame::movable (bool a)
{
	masterwindow->movable (a);
}

bool Frame::isMovable ()
{
	return masterwindow->isMovable ();
}

void Frame::closable (bool a)
{
	masterwindow->closable (a);
}

bool Frame::isClosable ()
{
	return masterwindow->isClosable ();
}

void Frame::display ()
{
	masterwindow->display ();
}

void Frame::kill ()
{
	masterwindow->kill ();
}

bool Frame::isAlive ()
{
	masterwindow->isAlive ();
}

void Frame::update ()
{
	masterwindow->update ();
}

void Frame::selfupdate (uint64_t ev)
{
	update ();
}

void Frame::drawPixel (uint16_t x, uint16_t y, uint32_t col)
{
	// ... (add offsets, ignore if it is scrolled off screen of set to not visible etc.)
}

void Frame::drawCharacter (char c, uint16_t x, uint16_t y, uint32_t bg = 0xFFFFFF, uint32_t fg = 0, int font = 0)
{
	// ... (add offsets, ignore if it is scrolled off screen of set to not visible etc.)
}

void Frame::addWidgetChild (Widget* child)
{
	// ...
}

void Frame::setCursorType (enum MouseTypes type)
{
	masterwindow->setCursorType (type);
}

enum MouseTypes Frame::getCursorType ()
{
	return masterwindow->getCursorType ();
}


/*

Frame* frame = new Frame(window, 2, 2);
Button* b = new Button (frame, "Hello", 5, 2);		//will be displayed at 7,2

*/