;code here will need to be changed to use a CPU by CPU version taskno
;see the x86-64 hardware.asm for reference

[bits 32]

global preMagic
global postMagic

extern afterExerunPtr
magicESPSave dd 0

preMagic:
	mov eax, [magicESPSave]
	pop eax		;return address
	mov [magicESPSave], eax		;ret addr.

	pusha
	
	mov eax, [magicESPSave] ;ret addr.
	mov [magicESPSave], esp

	jmp eax

postMagic:
	mov esp, [magicESPSave]

	popa
	add esp, 4					;get rid of the return address which called preMagic

	sti
	jmp dword [afterExerunPtr]

extern loadTaskFromASM
extern storeDataFromASM

global loadGDT
extern gdtDescriptor
loadGDT:
    lgdt [gdtDescriptor]
    mov ax, 0x10
    mov ds, ax
    mov es, ax
    mov fs, ax
    mov gs, ax
    mov ss, ax
    jmp flush2
flush2:
    ret


extern flagForCPUTimingSetByExecutionAssemblyStub
extern unlock_lock__0002

extern nextCPUToRun

%macro lockTheLock 1
%%acquireLock_%1:
    lock bts [%1], 0        ;Attempt to acquire the lock (in case lock is uncontended)
    jc %%spin_wait_%1            ;Spin if locked ( organize code such that conditional jumps are typically not taken ) 
    jmp %%__exit_%1                     ;Lock obtained
 
%%spin_wait:_%1
    test dword [%1],1      ;Is the lock free?
    jnz %%spin_wait_%1           ;no, wait
    jmp %%acquireLock_%1          ;maybe, retry ( could also repeat bts, jc, ret instructions here instead of jmp )

;%%acquireLock_%1:
;    lock bts dword [%1],0        ;Attempt to acquire the lock (in case lock is uncontended)
;    jc %%spin_with_pause_%1
;	jmp %%__exit_%1
; 
;%%spin_with_pause_%1:
;    pause                    ; Tell CPU we're spinning
;    test dword [%1],1      ; Is the lock free?
;    jnz %%spin_with_pause_%1     ; no, wait
;    jmp %%acquireLock_%1          ; retry

%%__exit_%1:
%endmacro

%macro unlockTheLock 1
    mov dword [%1], 0
	ret
%endmacro

global enableA20
enableA20:
        cli
 
        call    a20wait
        mov     al,0xAD
        out     0x64,al
 
        call    a20wait
        mov     al,0xD0
        out     0x64,al
 
        call    a20wait2
        in      al,0x60
        push    eax
 
        call    a20wait
        mov     al,0xD1
        out     0x64,al
 
        call    a20wait
        pop     eax
        or      al,2
        out     0x60,al
 
        call    a20wait
        mov     al,0xAE
        out     0x64,al
 
        call    a20wait
        sti
        ret
 
a20wait:
        in      al,0x64
        test    al,2
        jnz     a20wait
        ret
 
 
a20wait2:
        in      al,0x64
        test    al,1
        jz      a20wait2
        ret


extern hasAPIC

IOWAIT:
    pusha
    mov ecx, 500
    
.looping:
    mov dx, 0x80
    mov al, 0
    out dx, al
    
    loop .looping
    
    popa
    ret

extern timer_handler
extern DMAOutputISR

global pagingUpdate
global turnOnPaging
extern page_directory
extern testa

pagingUpdate:
    push eax
    mov eax, cr3
    mov cr3, eax
    pop eax
    ret

global exerun
global driverrun
global taskno

global st_eax
global st_ebx
global st_ecx
global st_edx
global st_eip
global st_esp
global st_ebp
global st_edi
global st_esi
global st_eflags

global st_cs
global st_ds
global st_es
global st_fs
global st_gs
global st_ss
global st_loadedBefore

stackptr dd 0x90000
stackbas dd 0x90000

taskno dd 0
stackinc dd 0

st_loadedBefore db 0
st_eax dd 0
st_ebx dd 0
st_ecx dd 0
st_edx dd 0
st_eip dd 0x8000        ;start the program at 0x8000 the first time
st_ebp dd 0
st_esp dd 0x80000       ;start the stack at 0x80000 (64KB below the kernel) the first time
st_edi dd 0
st_esi dd 0

st_cs dw 0
st_ds dw 0
st_es dw 0
st_fs dw 0
st_gs dw 0
st_ss dw 0

st_eflags dd 0
st_backupst dd 0

retloc dd 0

global saveSSERegisters
global loadSSERegisters

align 16
spot dd 0

saveSSERegisters:
    ret

loadSSERegisters:
    ret

ack:
    mov [taskno], dword 1
    int 32

exerun:    
	mov [taskno], dword 1    ;commenting this out sets it to monotasking mode

    mov [stackptr], esp
    mov [stackbas], ebp

    mov ebp, [st_ebp]

    mov eax, [st_eax]
    mov ebx, [st_ebx]
    mov ecx, [st_ecx]
    mov edx, [st_edx]
    mov esi, [st_esi]
    mov edi, [st_edi]

    mov ax, 0x23        ;0x23
    mov ds, ax
    mov es, ax
    mov fs, ax
    mov gs, ax
    
	mov eax, 0x10000000
	mov [st_eip], eax 
	mov eax, 0x202
	mov [st_eflags], eax

    mov eax, [st_eax]
    
    push dword 0x23        ;SS 23
    push dword [st_esp]          ;RSP
    push dword [st_eflags]    ;RFLAGS
    push dword 0x1b        ;CS 1b
    push dword [st_eip]    ;RIP

	mov byte [flagForCPUTimingSetByExecutionAssemblyStub], 1
	mov [taskModeActive], byte 1

    iretd

global tss_flush
tss_flush:
   mov ax, 0x2B
   ltr ax
   ret

global _isr0
global _isr1
global _isr2
global _isr3
global _isr4
global _isr5
global _isr6
global _isr7
global _isr8
global _isr9
global _isr10
global _isr11
global _isr12
global _isr13
global _isr14
global _isr15
global _isr16
global _isr17
global _isr18
global isr96
global asmyield

extern fault_handler
global timer_int
extern toticks

align 16
__vv dd 0

global tasknos
	tasknos:
	dd 0
	dd 0
	dd 0
	dd 0

acquireLock__vv:
    lock bts dword [__vv], 0        ;Attempt to acquire the lock (in case lock is uncontended)
    jc .spin_wait            ;Spin if locked ( organize code such that conditional jumps are typically not taken ) 
    ret                      ;Lock obtained
.spin_wait:
    test dword [__vv],1      ;Is the lock free?
    jnz .spin_wait           ;no, wait
    jmp acquireLock__vv          ;maybe, retry ( could also repeat bts, jc, ret instructions here instead of jmp )



	

global switchTask
;refine this so it will:
;	- only switch when task mode is active
;	- in an interrupt handler, ONLY when it yields
;	- nowhere else. this will always be called by an int. handler of some sort (including syscalls)
switchTask:
	sti
	cmp [taskno], dword 1
	je skipRET				;if we are not in the position to load a task, simply iret (which is all the rest of the function does, just in a complex way)
.doret:
	ret

skipRET:
	cli
	push dword [taskno]
	mov [taskno], dword 0

	pusha
	push ss
	push CS
	push ds
	push es
	push fs
	push gs
	pushfd		;mightn't be that helpful because the IRET will restore the userland EFLAGS, but it might be helpful to restore the kernel-land EFLAGS
	mov [st_esp], esp			;we still have to save the stack
	call storeDataFromASM
	call findNextTask
	call loadTaskFromASM
;---------------------------------------------------;
;				STATE HAS SWITCHED					;
;---------------------------------------------------;
global stateSwitchJumpPoint
stateSwitchJumpPoint:				;used to start loading tasks after the login task has finished
	cli
	;return to the kernel stack we had before the switch
	mov esp, [st_esp]
	;restore context
	pop eax			;pops flags
	or eax, 0x0200	;enables interrupts
	push eax
	popfd			;sets interrupts to on
	pop gs
	pop fs
	pop es
	pop ds
	pop CS
	pop ss

	sti

	cmp [st_loadedBefore], byte 0
	jne .loadedBefore

	push ax
	mov ax, 0x23        ;0x23f
    mov ds, ax
    mov es, ax
    mov fs, ax
    mov gs, ax
	pop ax

	;st_eip and st_eflags should already be set

	;set up the IRET
    push dword 0x23        ;SS 23
    push dword [st_esp]          ;RSP
    push dword [st_eflags]    ;RFLAGS
    push dword 0x1b        ;CS 1b
    push dword [st_eip]    ;RIP

	mov byte [flagForCPUTimingSetByExecutionAssemblyStub], 1
	mov [taskno], dword 1
	iretd		;returns interrupts

.loadedBefore:	
	pop dword [taskno]
	ret						;returns to before switchTask was called


extern loadTaskFromASM
extern storeDataFromASM
extern findNextTask

extern taskModeActive
global yieldFromKernel

yieldFromKernel:
	cmp [taskModeActive], byte 0
	je .noyield
	call skipRET			;skips the taskno check because the kernel SHOULD KNOW when is a good time to switch tasks without needing taskno
.noyield:
	ret

global irq0
global irq1
global irq2
global irq3
global irq4
global irq5
global irq6
global irq7
global irq8
global irq9
global irq10
global irq11
global irq12
global irq13
global irq14
global irq15
global irq16
global irq17
global irq18
global irq19
global irq20
global irq21
global irq22
global irq23

irq0:
    cli
    push dword 0
    push dword 32
    jmp irq_common_stub

irq1:
    cli
    push dword 0
    push dword 33
    jmp irq_common_stub

irq2:
    cli
    push dword 0
    push dword 34
    jmp irq_common_stub

irq3:
    cli
    push dword 0
    push dword 35
    jmp irq_common_stub

irq4:
    cli
    push dword 0
    push dword 36
    jmp irq_common_stub

irq5:
    cli
    push dword 0
    push dword 37
    jmp irq_common_stub

irq6:
    cli
    push dword 0
    push dword 38
    jmp irq_common_stub

irq7:
    cli
    push dword 0
    push dword 39
    jmp irq_common_stub

irq8:
    cli
    push dword 0
    push dword 40
    jmp irq_common_stub

irq9:
    cli
    push dword 0
    push dword 41
    jmp irq_common_stub

irq10:
    cli
    push dword 0
    push dword 42
    jmp irq_common_stub

irq11:
    cli
    push dword 0
    push dword 43
    jmp irq_common_stub

irq12:
    cli
    push dword 0
    push dword 44
    jmp irq_common_stub

irq13:
    cli
    push dword 0
    push dword 45
    jmp irq_common_stub

irq14:
    cli
    push dword 0
    push dword 46
    jmp irq_common_stub

irq15:
    cli
    push dword 0
    push dword 47
    jmp irq_common_stub

irq16:
    cli
    push dword 0
    push dword 48
    jmp irq_common_stub

irq17:
    cli
    push dword 0
    push dword 49
    jmp irq_common_stub

irq18:
    cli
    push dword 0
    push dword 50
    jmp irq_common_stub

irq19:
    cli
    push dword 0
    push dword 51
    jmp irq_common_stub

irq20:
    cli
    push dword 0
    push dword 52
    jmp irq_common_stub

irq21:
    cli
    push dword 0
    push dword 53
    jmp irq_common_stub

irq22:
    cli
    push dword 0
    push dword 54
    jmp irq_common_stub

irq23:
    cli
    push dword 0
    push dword 55
    jmp irq_common_stub

global idt_load
extern idtp
idt_load:
	mov [0x7D08], dword idtp
    lidt [idtp]
    ret



_isr_common_stub:
	sti

    pusha
    push ds
    push es
    push fs
    push gs

	;save taskno, then set to zero so the handler isn't pre-empted (handlers are cooperative)
	mov eax, [taskno]
	push eax
	mov [taskno], dword 0

    mov ax, 0x10   ; Load the Kernel Data Segment descriptor!
    mov ds, ax
    mov es, ax
    mov fs, ax
    mov gs, ax
    mov eax, esp   ; Push us the stack
    push eax
	
	cld

    mov eax, fault_handler
	call eax

   	;restore taskno
	pop eax
	mov [taskno], eax

    pop eax
	pop gs
    pop fs
    pop es
    pop ds
    popa

	add esp, 8			; Cleans up the pushed error code and pushed ISR number
	call switchTask
	iretd


	;uint32_t gs, fs, ed, ds;
	;uint32_t edi, esi, ebp, esp, ebx, edx, ecx, eax;
	;uint32_t int_no, err_code;

extern irq_handler
irq_common_stub:
	sti

    pusha
	push ds
    push es
    push fs
    push gs

	mov ax, 0x10   ; Load the Kernel Data Segment descriptor!
    mov ds, ax
    mov es, ax
    mov fs, ax
    mov gs, ax

	cld

	;save taskno, then set to zero so the handler isn't pre-empted (handlers are cooperative)
	mov eax, [taskno]
	push eax
	mov [taskno], dword 0

    mov eax, esp
    push eax

    mov eax, irq_handler
	call eax

	;restore taskno
	pop eax
	mov [taskno], eax

    pop eax
	pop gs
    pop fs
    pop es
    pop ds
    popa

	add esp, 8
	call switchTask
	iretd


global _isr0
_isr0:
    cli
    push dword 0
    push dword 0
    jmp _isr_common_stub

_isr1:
    cli
    push dword 0
    push dword 1
    jmp _isr_common_stub

_isr2:
    cli
    push dword 0
    push dword 2
    jmp _isr_common_stub

_isr3:
    cli
    push dword 0
    push dword 3
    jmp _isr_common_stub

_isr4:
    cli
    push dword 0
    push dword 4
    jmp _isr_common_stub

_isr5:
    cli
    push dword 0
    push dword 5
    jmp _isr_common_stub

_isr6:
    cli
    push dword 0
    push dword 6
    jmp _isr_common_stub

_isr7:
    cli
    push dword 0
    push dword 7
    jmp _isr_common_stub

_isr8:
    cli
    push dword 8
    jmp _isr_common_stub

_isr9:
    cli
    push dword 0
    push dword 9
    jmp _isr_common_stub

_isr10:
    cli
    push dword 10
    jmp _isr_common_stub

_isr11:
    cli
    push dword 11
    jmp _isr_common_stub

_isr12:
    cli
    push dword 12
    jmp _isr_common_stub

_isr13:
    cli
    push dword 13
    jmp _isr_common_stub

_isr14:
    cli
    push dword 14
    jmp _isr_common_stub

_isr15:
    cli
    push dword 0
    push dword 15
    jmp _isr_common_stub

_isr16:
    cli
    push dword 0
    push dword 16
    jmp _isr_common_stub

_isr17:
    cli
    push dword 0
    push dword 17
    jmp _isr_common_stub

_isr18:
    cli
    push dword 0
    push dword 18
    jmp _isr_common_stub

extern syscallreturn

align 16
__v1 dd 0

acquireLock__v1:
    lock bts dword [__v1], 0        ;Attempt to acquire the lock (in case lock is uncontended)
    jc .spin_wait            ;Spin if locked ( organize code such that conditional jumps are typically not taken ) 
    ret                      ;Lock obtained
.spin_wait:
    test dword [__v1],1      ;Is the lock free?
    jnz .spin_wait           ;no, wait
    jmp acquireLock__v1          ;maybe, retry ( could also repeat bts, jc, ret instructions here instead of jmp )

extern getCPUNumber
isr96:
	sti

    push dword 0
    push dword 96
   
    pusha
    push ds
    push es
    push fs
    push gs

	;save taskno, then set to zero so the handler isn't pre-empted (handlers are cooperative)
	mov eax, [taskno]
	push eax
	mov [taskno], dword 0

    mov ax, 0x10   ; Load the Kernel Data Segment descriptor!
    mov ds, ax
    mov es, ax
    mov fs, ax
    mov gs, ax
    mov eax, esp   ; Push us the stack
    push eax

	sti
    mov eax, fault_handler
    call eax       ; A special call, preserves the 'eip' register
    
	;restore taskno
	pop eax
	mov [taskno], eax

	pop eax
	pop gs
    pop fs
    pop es
    pop ds
    popa

	add esp, 8
	mov eax, [syscallreturn]
	call switchTask
	iretd


; EDI = buffer to put data
; ESI = starting sector
; EDX = number of sectors to 
global read_hard_disk
read_hard_disk:
    pusha
    
    mov edi, [0x600]
    mov esi, [0x604]
    mov edx, [0x608]
    
    mov eax, esi
    mov cl, dl

ata_lba_read:
    
    and eax, 0x0FFFFFFF
    mov ebx, eax         ; Save LBA in RBX

    mov edx, 0x01F6      ; Port to send drive and bit 24 - 27 of LBA
    shr eax, 24          ; Get bit 24 - 27 in al
    or al, 11100000b     ; Set bit 6 in al for LBA mode
    out dx, al

    mov edx, 0x01F2      ; Port to send number of sectors
    mov al, cl           ; Get number of sectors from CL
    out dx, al

    mov edx, 0x1F3       ; Port to send bit 0 - 7 of LBA
    mov eax, ebx         ; Get LBA from EBX
    out dx, al

    mov edx, 0x1F4       ; Port to send bit 8 - 15 of LBA
    mov eax, ebx         ; Get LBA from EBX
    shr eax, 8           ; Get bit 8 - 15 in AL
    out dx, al

    mov edx, 0x1F5       ; Port to send bit 16 - 23 of LBA
    mov eax, ebx         ; Get LBA from EBX
    shr eax, 16          ; Get bit 16 - 23 in AL
    out dx, al

    mov edx, 0x1F7       ; Command port
    mov al, 0x20         ; Read with retry.
    out dx, al

	call IOWAIT

.still_going:  in al, dx
    test al, 8           ; the sector buffer requires servicing.
    jz .still_going      ; until the sector buffer is ready.

    mov eax, 256         ; to read 256 words = 1 sector
    xor bx, bx
    mov bl, cl           ; read CL sectors
    mul bx
    mov ecx, eax         ; RCX is counter for INSW
    mov edx, 0x1F0       ; Data port, in and out
    rep insw             ; in to [RDI]

    popa
    ret

    
    
; CALLING CONVENTIONS NEED CHANGING!
; EDI = data buffer
; ESI = starting sector
; EDX = number of sectors to write

;From http://wiki.osdev.org/ATA_PIO_Mode
;
;WRITING 28 BIT LBA
;To write sectors in 28 bit PIO mode, send command "WRITE SECTORS" (0x30) to the Command port. 
;Do not use REP OUTSW to transfer data. There must be a tiny delay between each OUTSW output uint16_t. 
;A jmp $+2 size of delay. Make sure to do a Cache Flush (ATA command 0xE7) after each write command completes.
;
;CACHE FLUSHING
;On some drives it is necessary to "manually" flush the hardware write cache after every write command. 
;This is done by sending the 0xE7 command to the Command Register (then waiting for BSY to clear). 
;If a driver does not do this, then subsequent write commands can fail invisibly, 
;or "temporary bad sectors" can be created on your disk.


global write_hard_disk
write_hard_disk:
	pushf
    pusha
    
    mov edi, [0x600]
    mov esi, [0x604]
    mov edx, [0x608]
    
    mov eax, esi
    mov cl, dl
    
ata_lba_write:
    and eax, 0x0FFFFFFF

    mov ebx, eax         ; Save LBA in RBX

    mov edx, 0x01F6      ; Port to send drive and bit 24 - 27 of LBA
    shr eax, 24          ; Get bit 24 - 27 in al
    or al, 11100000b     ; Set bit 6 in al for LBA mode
    out dx, al

    mov edx, 0x01F2      ; Port to send number of sectors
    mov al, cl           ; Get number of sectors from CL
    out dx, al

    mov edx, 0x1F3       ; Port to send bit 0 - 7 of LBA
    mov eax, ebx         ; Get LBA from EBX
    out dx, al

    mov edx, 0x1F4       ; Port to send bit 8 - 15 of LBA
    mov eax, ebx         ; Get LBA from EBX
    shr eax, 8           ; Get bit 8 - 15 in AL
    out dx, al


    mov edx, 0x1F5       ; Port to send bit 16 - 23 of LBA
    mov eax, ebx         ; Get LBA from EBX
    shr eax, 16          ; Get bit 16 - 23 in AL
    out dx, al

    mov edx, 0x1F7       ; Command port
    mov al, 0x30         ; Write with retry.
    out dx, al

	call IOWAIT

.still_going:  in al, dx
    test al, 8           ; the sector buffer requires servicing.
    jz .still_going      ; until the sector buffer is ready.

    mov eax, 256         ; to read 256 words = 1 sector
    xor bx, bx
    mov bl, cl           ; read CL sectors
    mul bx
    mov ecx, eax         ; RCX is counter for INSW
    mov edx, 0x1F0       ; Data port, in and out
    mov esi, edi

.write_sector_loop:
    outsw
    jmp .write_sector_delay
    
.write_sector_delay:
    loop .write_sector_loop
    
    push edx
	push eax
	mov dx, 0x80
	mov al, 0
	out dx, al
	pop eax
	pop edx
    push edx
	push eax
	mov dx, 0x80
	mov al, 0
	out dx, al
	pop eax
	pop edx
    push edx
	push eax
	mov dx, 0x80
	mov al, 0
	out dx, al
	pop eax
	pop edx
    push edx
	push eax
	mov dx, 0x80
	mov al, 0
	out dx, al
	pop eax
	pop edx
    
    mov dx, 0x1F7
    mov al, 0xE7
    out dx, al
    
    push edx
	push eax
	mov dx, 0x80
	mov al, 0
	out dx, al
	pop eax
	pop edx

.flush_wait:
	in al, dx
	test al, 0x80		; bsy
	jnz .flush_wait
	test al, 0x01		; err
	jnz .flush_error
	test al, 0x20		; df
	jnz .flush_error

    in al, dx
	mov ah, al
	mov al, 0
    
    popa
    popf
    
    leave
    ret
    
.flush_error:
    popa
    popf
    
    leave
    ret
  