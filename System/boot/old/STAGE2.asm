; *** BANANA OS BOOTLOADER ***;

; Based on the bootloader from http://www.brokenthorn.com/Resources/OSDevIndex.html
; All rights reserved.

; If you are interested in using this code for your own projects, see the bootloader
; at the above URL. The original code is released under the Public Domain.

; http://programmersheaven.com/discussion/199085/bios-character-codes FOR BIOS SCANCODES (WHAT IS IN AX)

bits	16
org 0x0500

jmp	main

times 2048 db 0			;so the kernel info table doesn't overwrite what is in stdio.inc (it has to overwrite 'jmp main' though)

%include "stdio.inc"
%include "Gdt.inc"
%include "A20.inc"
%include "Fat12.inc"
%include "common.inc"


DetectRAM:
	pusha
	mov di, 0x600
	mov ax, 0;0x2800
	mov es, ax
	
	call do_e820
	mov eax, 0xDEADBEEF

	popa
	ret

; use the INT 0x15, eax= 0xE820 BIOS function to get a memory map
; inputs: es:di -> destination buffer for 24 byte entries
; outputs: bp = entry count, trashes all registers except esi
do_e820:
	xor ebx, ebx		; ebx must be 0 to start
	xor bp, bp		; keep an entry count in bp
	mov edx, 0x0534D4150	; Place "SMAP" into edx
	mov eax, 0xe820
	mov [es:di + 20], dword 1	; force a valid ACPI 3.X entry
	mov ecx, 24		; ask for 24 bytes
	int 0x15
	jc short .failed	; carry set on first call means "unsupported function"
	mov edx, 0x0534D4150	; Some BIOSes apparently trash this register?
	cmp eax, edx		; on success, eax must have been reset to "SMAP"
	jne short .failed
	test ebx, ebx		; ebx = 0 implies list is only 1 entry long (worthless)
	je short .failed
	jmp short .jmpin
.e820lp:
	mov eax, 0xe820		; eax, ecx get trashed on every int 0x15 call
	mov [es:di + 20], dword 1	; force a valid ACPI 3.X entry
	mov ecx, 24		; ask for 24 bytes again
	int 0x15
	jc short .e820f		; carry set means "end of list already reached"
	mov edx, 0x0534D4150	; repair potentially trashed register
.jmpin:
	jcxz .skipent		; skip any 0 length entries
	cmp cl, 20		; got a 24 byte ACPI 3.X response?
	jbe short .notext
	test byte [es:di + 20], 1	; if so: is the "ignore this data" bit clear?
	je short .skipent
.notext:
	mov ecx, [es:di + 8]	; get lower uint32_t of memory region length
	or ecx, [es:di + 12]	; "or" it with upper uint32_t to test for zero
	jz .skipent		; if length uint64_t is 0, skip entry
	inc bp			; got a good entry: ++count, move to next storage spot
	add di, 24
.skipent:
	test ebx, ebx		; if ebx resets to 0, list is complete
	jne short .e820lp
.e820f:
	mov [0x513], bp	; store the entry count
	clc			; there is "jc" on end of list to this point, so the carry must be cleared
	ret
.failed:
	mov [0x513], word 0
	stc			; "function unsupported" error exit
	ret

;	Get rid of this if you want to rest of the message -----v
LoadingMsg db 0x0D, 0x0A, 0, "Booting Banana OS...", 0xD, 0xA, 0, 0xD, 0xA, "GDT A20 TXT ", 0
msgFailure db 0x0D, 0x0A, "Fatal Error: Press Any Key to Reboot", 0x0D, 0x0A, 0x0A, 0x00

main:
	cli
	xor	ax, ax
	mov	ds, ax
	mov	es, ax
	mov	ss, ax
	mov	sp, 0xFFFF
	sti

	mov [bsDriveNumber], dl
    
    mov ax, 12
    int 0x10
    
    mov	si, LoadingMsg
	;call	Puts16

	call	InstallGDT
	call	EnableA20_KKbrd_Out
	call	DetectRAM
	call	LoadRoot
	mov	ebx, 0
    mov	bp, IMAGE_RMODE_BASE
	mov	si, ImageName
	call	LoadFile
	
	mov	dword [ImageSize], ecx
	cmp	ax, 0
	je	EnterStage3
	mov	si, msgFailure
	call	Puts16
	mov	ah, 0
	int     0x16
restart:
	mov ah, 0
	int     0x19
	cli
	hlt
	jmp $
    
align 256			;just to be safe, I don't think it needs any alignment, but you never know
MODEINFO:
ModeAttrib dw 0
WinAAttrib db 0
WinBAttrib db 0
WinGrand   dw 0
WinSize    dw 0
WinASeg    dw 0
WinBSeg    dw 0
WinFuncPtr dd 0
ByteScan   dw 0
XRes       dw 0
YRes       dw 0
XCharSize  db 0
YCharSize  db 0
PlaneNo    db 0
Bpp        db 0
NoBanks    db 0
MemModel   db 0
BankSize   db 0
NoImgPage  db 0
ResPage    db 0
RedMaskS   db 0
RedMaskP   db 0
GreenMaskS db 0
GreenMaskP db 0
BlueMaskS  db 0
BlueMaskP  db 0
ResMaskS   db 0
ResMaskP   db 0
DirColMode db 0
PhysBasPtr dd 0
OffScrMO   dd 0
OffScrMS   dw 0
times 206 db 0

times 10 dd 0 ;just to be safe

linearFrameBuffer dd 0

ProtectedModeBUFFER:
SetWindow dw 0
SetDisplayStart dw 0
SetPalette dw 0
IOInfo dw 0
OtherStuff dd 0

;int 0x16, ah = 0       get character (blocking)
;result:        ah = BIOS Scan Code         al = ASCII

boot_1 db "Banana OS Boot Options", 0xA, 0xA, 0xD, 0
boot_2 db "Use the arrow keys to select an option and then press ENTER.", 0xA, 0xA, 0xD, 0
option_1 db "Continue booting ", 0xA, 0xD, 0
option_2 db "Reboot the computer ", 0xA, 0xD, 0
option_3 db "Shutdown the computer ", 0xA, 0xD, 0
option_4 db "Start in Safe Mode ", 0xA, 0xD, 0
option_5 db "View information about the computer ", 0xA, 0xD, 0
option_6 db "Force 800x600 video mode ", 0
smallOff db							  "    NO ", 0xA, 0xD, 0
smallOn db							  "    YES ", 0xA, 0xD, 0
option_8 db "Enable the APIC          ", 0
smallOff2 db						  "    NO ", 0xA, 0xD, 0
smallOn2 db							  "    YES ", 0xA, 0xD, 0
option_9 db "Enable `Comic Sans mode' " , 0
option_10 db "Enable readonly mode     ", 0

enableAPIC db 1
comicSansMode db 0
sel db 0
cursorOn db  " ", 0x10, " ", 0
cursorOff db "   ", 0
safeToTurnOff db "It is now safe to turn off your computer.", 0
forceSmallRes db 0
readonly db 0 

;more definitions below

cursorIsOn1:
	mov si, cursorOn
	call Puts16
	jmp bk1
	
cursorIsOn2:
	mov si, cursorOn
	call Puts16
	jmp bk2
	
cursorIsOn3:
	mov si, cursorOn
	call Puts16
	jmp bk3
	
cursorIsOn4:
	mov si, cursorOn
	call Puts16
	jmp bk4
	
cursorIsOn5:
	mov si, cursorOn
	call Puts16
	jmp bk5
	
cursorIsOn6:
	mov si, cursorOn
	call Puts16
	jmp bk6
	
cursorIsOn7:
	mov si, cursorOn
	call Puts16
	jmp bk7
	
cursorIsOn9:
	mov si, cursorOn
	call Puts16
	jmp bk9

cursorIsOn10:
	mov si, cursorOn
	call Puts16
	jmp bk10

firstTimeBootOptions db 1

BootOptionsSetup:
	mov ax, 3
    int 0x10
	mov [firstTimeBootOptions], byte 0
	
BootOptions:
	cmp [firstTimeBootOptions], byte 1
	je BootOptionsSetup
	
	mov ah, 0x2
	mov bx, 0
	mov dh, 0
	mov dl, 0
	int 0x10
	
	mov si, boot_1
	call Puts16
	mov si, boot_2
	call Puts16
	
	cmp [sel], byte 0
	je cursorIsOn1
	mov si, cursorOff
	call Puts16
bk1:
	mov si, option_1
	call Puts16
	
	cmp [sel], byte 1
	je cursorIsOn2
	mov si, cursorOff
	call Puts16
bk2:
	mov si, option_2
	call Puts16
	

	cmp [sel], byte 2
	je cursorIsOn3
	mov si, cursorOff
	call Puts16
bk3:
	mov si, option_3
	call Puts16

	cmp [sel], byte 3
	je cursorIsOn4
	mov si, cursorOff
	call Puts16
bk4:
	mov si, option_4
	call Puts16
	
	cmp [sel], byte 4
	je cursorIsOn5
	mov si, cursorOff
	call Puts16
bk5:
	mov si, option_5
	call Puts16
	
	cmp [sel], byte 5
	je cursorIsOn6
	mov si, cursorOff
	call Puts16
bk6:
	mov si, option_6
	call Puts16
	
	cmp byte [forceSmallRes], 1
	je yes6
	
	mov si, smallOff
	call Puts16
	jmp done6
	
	yes6:
		mov si, smallOn
		call Puts16
		
	done6:
	

;HERE IS THE BREAK
	cmp [sel], byte 6
	je cursorIsOn7
	mov si, cursorOff
	call Puts16
bk7:
	mov si, option_8
	call Puts16

	cmp byte [enableAPIC], 1
	je yes8
	
	mov si, smallOff2
	call Puts16
	jmp done8
	
	yes8:
		mov si, smallOn2
		call Puts16
		
	done8:
;NEXT BREAK

	cmp [sel], byte 7
	je cursorIsOn9
	mov si, cursorOff
	call Puts16
bk9:
	mov si, option_9
	call Puts16

	cmp byte [comicSansMode], 1
	je yes9
	
	mov si, smallOff2
	call Puts16
	jmp done9
	
	yes9:
		mov si, smallOn2
		call Puts16
		
	done9:


	cmp [sel], byte 8
	je cursorIsOn10
	mov si, cursorOff
	call Puts16
bk10:
	mov si, option_10
	call Puts16

	cmp byte [readonly], 1
	je yes10
	
	mov si, smallOff2
	call Puts16
	jmp done10
	
	yes10:
		mov si, smallOn2
		call Puts16
		
	done10:

selectOption:
	mov ah, 0
    int 0x16
	
	cmp ah, 0x50
	je incOption
	cmp ah, 0x48
	je decOption
	cmp ah, 0x1C
	je doOption
	
    jmp selectOption
	
incOption:
	cmp [sel], byte 8
	je selectOption
	inc byte [sel]
	
	jmp BootOptions

decOption:
	cmp [sel], byte 0
	je selectOption
	dec byte [sel]
	
	jmp BootOptions
	
doOption:
	cmp [sel], byte 1
	je restart
	
	cmp [sel], byte 2
	je shutdown
	
	cmp [sel], byte 3
	je startInSafeMode
	
	cmp [sel], byte 4
	je showInfo
	
	cmp [sel], byte 5
	je forceResCode
	
	cmp [sel], byte 6
	je __enableAPIC

	cmp [sel], byte 7
	je enableComicSansMode
	
	cmp [sel], byte 8
	je enableReadonlyMode

	jmp startNormally
	
	
forceResCode:
	xor [forceSmallRes], byte 1
	jmp BootOptions

__enableAPIC:
	xor [enableAPIC], byte 1
	jmp BootOptions

enableComicSansMode:
	xor [comicSansMode], byte 1
	jmp BootOptions

enableReadonlyMode:
	xor [readonly], byte 1
	jmp BootOptions

exec_msg db "Execute Binary File", 0xA, 0xA, 0xD, "Type the file name like the following: BINARY.SYS", 0xA,0xD, "The binary file must be in the root directory of the first floppy.", 0xA, 0xD, "Press the ENTER key to continue or the ESCAPE key to cancel.", 0xA, 0xA, 0xD, "    Filename: ", 0
curpos db 0
filename times 50 db ' '		;will need to be modified for the correct format
db 0
endln db 0xD, "                        ", 0xD, "    Filename: ", 0
dotalready db 0

customExecute:
	mov ax, 3
	int 0x10
	
	mov byte [curpos], 0
	mov si, exec_msg
	call Puts16
	
excloop:
	mov ah, 0
    int 0x16
	
	cmp ah, 0x1
	je excloopcancel
	cmp ah, 0x1C
	je excexit
	cmp ah, 0x08
	je bkspaces
	jmp wr
	
	bkspaces:
		cmp byte [curpos], 0
		je excloop
		
		dec byte [curpos]
	
		mov bh, 0
		mov bl, byte [curpos]
		
		mov [filename + bx], byte ' '
		jmp printout
	
	wr:
	mov bh, 0
	mov bl, byte [curpos]
		
	cmp al, 'a'
	jge nx
	jmp w
	nx:
		cmp al, 'z'
		jle nnx
		jmp w
	nnx:
		and al, 223
	w:
	
	cmp byte [curpos], 50
	je printout
	
	mov [filename + bx], al
	inc byte [curpos]
	
	printout:
	mov si, endln
	call Puts16
	mov si, filename
	call Puts16

	jmp excloop
	
excloopcancel:
	mov [sel], byte 0
	mov ax, 3
	int 0x10
	jmp BootOptions
	
excexit:
	mov ax, 3
	int 0x10
	jmp BootOptions
	
vendor times 13 db 0	;13 for the null
newline_str db 0xA, 0xD, 0
info_msg db "Critical Computer Information", 0xA, 0xA, 0xD, 0
info_exit db 0xA, 0xD, "(Press any key to exit)", 0
info_vendor 	db "Vender ID:              ", 0
info_type 		db "Computer Type:          ", 0
info_os 		db "Operating System:       Banana OS v0.1 (64 bit)", 0xA, 0xD, 0

info_cpuid_edx  db 0xA, 0xD, "CPUID (eax = 1):", 0xA, 0xD, "edx = 0b", 0
info_cpuid_ecx  db "ecx = 0b", 0
type_32 		db "x86 (32 bit)", 0
type_64 		db "x86_64 (64 bit)", 0
type_unknown 	db "Unknown", 0

msg_zero db "0", 0
msg_one  db "1", 0

newline:
	pusha
	mov si, newline_str
	call Puts16
	popa
	ret

showInfo:
	mov ax, 3
	int 0x10
	
	mov si, info_msg
	call Puts16
	
	mov eax, 0x0
	cpuid
	
	mov si, info_vendor
	call Puts16
	mov [vendor], ebx
	mov [vendor + 4], edx
	mov [vendor + 8], ecx
	mov si, vendor
	call Puts16
	call newline
	
	mov si, info_type
	call Puts16
	
	mov eax, 0x80000000
	cpuid
	cmp eax, 0x80000001
	jb _32bit
	test edx, 1 << 29
	jz _32bit
	
	mov si, type_64
	call Puts16
	jmp next1
	
_32bit:
	mov si, type_32
	call Puts16	

_0:
	pusha
	mov si, msg_zero
	call Puts16
	popa
	jmp outzero
	
_1:
	pusha
	mov si, msg_one
	call Puts16
	popa
	ret
	
displayBinary:
	;EAX = data
	mov ebx, 32
	rol eax, 1
	
looping:
	mov edx, eax
	and edx, 1
	cmp edx, 0
	je _0
	call _1
	
outzero:
	rol eax, 1
	dec ebx
	cmp ebx, 0
	je endloop
	
	jmp looping
	
endloop:
	ret
	

next1:
	call newline
	
	mov si, info_os
	call Puts16
	
	;put more data here
	
	mov si, info_cpuid_edx
	call Puts16
	mov eax, 1
	cpuid
	mov eax, edx
	call displayBinary
	call newline
	
	mov si, info_cpuid_ecx
	call Puts16
	mov eax, 1
	cpuid
	mov eax, ecx
	call displayBinary
	call newline
	
	mov si, info_exit
	call Puts16
	
	mov [sel], byte 0
	
	mov ah, 0
	int 0x16
	mov ax, 3
	int 0x10
	jmp BootOptions
	
shutdown:
	mov ax, 3
	int 0x10
	
	mov ax, 0x5307
	mov bx, 1
	mov cx, 3
	int 0x15
	
	mov cx, 0x03
	mov dx, 0x4240
	mov ah, 0x86
	int 0x15
	
	mov si, safeToTurnOff
	call Puts16
	
	cli
	hlt
	jmp shutdown	;just in case we wake up somehow

pointer dw 0

findBestMode:
	mov cx, 0x4100
	mov dx, 800
	mov bx, 600
	mov ax, 0x4114
	mov si, 16
	
	cmp [forceSmallRes], byte 1
	je smallResNow
	
tryAgain:
	pusha
	mov ax, 0x4F01
    mov di, MODEINFO
    int 0x10
	popa

	push ax
	push bx

	mov bx, 0x2000
	mov fs, bx

	mov bx, [pointer]

	mov ax, [XRes]
	mov [fs:bx], byte ax
	add bx, 2

	mov ax, [YRes]
	mov [fs:bx], byte ax
	add bx, 2

	mov al, [Bpp]
	mov [fs:bx], byte al
	add bx, 1

	add bx, 3

	mov [pointer], bx

	pop bx
	pop ax
	
	
	cmp [Bpp], byte 24 ;24			;delete this in the future
	jle nope
	
	cmp [XRes], word 1600		;1600
	jg nope
	
	cmp [XRes], dx
	jge nextStep
	
nope:
	inc cx
	cmp cx, 0x43FF
	je endFindBest
	jmp tryAgain
	
nextStep:
	cmp [YRes], word 700		;700
	jge almost
	jmp nope
	
almost:
	cmp [YRes], word 800		;800
	jle foundNewBest
	jmp nope

foundNewBest:
	mov dx, [XRes]
	mov bx, [YRes]
	mov si, [Bpp]
	mov ax, cx
	jmp nope

endFindBest:
	mov cx, ax
	mov cx, 0x4192
	mov dx, 1920
	mov bx, 1080
	mov si,32
	ret
	
smallResNow:
	mov cx, 0x4114
	mov dx, 800
	mov bx, 600
	mov si, 16
	mov byte [cursorOn], 0
	mov byte [sel], 0
	ret

EnterStage3:
	mov [0x505], byte 0
	
	;mov cx, 0x0A
	;mov dx, 0x4240
	;mov ah, 0x86
	;int 0x15

    mov ah, 1
    int 0x16
    cmp ah, 0x42
    je BootOptions
	
	jmp startNormally

startInSafeMode:
	mov [0x505], byte 1
	mov cx, 0x4114
	mov dx, 800
	mov bx, 600
	mov si, 16
	jmp skipFind

startNormally:
	call findBestMode
	
skipFind:
	;mov cx, 0x4114
	;mov dx, 800
	;mov bx, 600
	;mov si, 16
	
	mov [0x50E], dx			;XRes
    mov [0x502], bx
	mov [0x50C], si
	mov byte [0x50D], 0		;BPP is stored in only one byte
	push cx;
	
    mov ax, 0x4F01
    mov di, MODEINFO
    int 0x10
	
	cmp ah, 1
	je ModeInfoFailed
    
    mov esi, [MODEINFO + 0x28]
    mov [0x506], esi
	
	push dx
	push ax
	mov dx, 0
	mov ax, [ByteScan]			;replace the X res value with the scanline length value (includes bytes between lines)
	movzx cx, [0x50C]				;bits per pixel
	shr cx, 3					;convert to bytes per pixel
	div cx
	mov [0x500], ax
	pop ax
	pop dx

	;the screen cannot be cleared at the moment because we're still in 16 bit mode
	;and the buffer is usually NOT in the reach of real mode addressing
	
    mov ax, 0x4F02
	pop bx
    int 0x10
    ;mov [0x506], esi
	
KEEP_US_IN_TEXT_MODE:

	;caluclates number floppies

	cli
	mov al,0x14 ;Command 14h of I/O port 70h is "Equipment Status Check"
	out 0x70,al ;Port 70h is the RTC Command Port (CMOS access via RTC) (write only)
	in al,0x71 ;Port 71h is the RTC Data I/O Port (read/write)
	mov ah, al
	and al, 1
	cmp al, 0
	je .noFloppies

	mov al, ah
	shl al, 6		;leave bits 6+7
	inc al			;bits 6+7 are 1 Based
	mov [0x50A], al

	jmp skip2
	
.noFloppies:
	mov [0x50A], byte 0
skip2:
	
noskips:
	mov ah, 0x8
	mov dl, 0x80
	mov bx, 0
	mov es, bx
	mov di, 0
	int 13				;???
	mov [0x50B], dl

	mov al, [enableAPIC]
	mov [0x510], al

	mov al, [comicSansMode]
	mov [0x511], al

	mov al, [readonly]
	mov [0x512], al
    
    cli
    mov eax, cr0	
    or eax, 1
    mov cr0, eax

    jmp CODE_DESC:Stage3
    
fail:
    cli
    hlt
    jmp fail
	
	
modeinfofailmsg db "Failed to load video mode information... (", 0
dot db ".", 0

ModeInfoFailed:
	push ax
	mov si, modeinfofailmsg
	call Puts16
	pop ax
	
	mov al, ah
	mov ah, 0
	
.looping:
	cmp ax, 0
	je .end
	dec ax
	
	push ax
	mov si, dot
	call Puts16
	pop ax
	
	jmp looping
	
.end:
	jmp $

bits 32

Stage3:
	mov	ax, DATA_DESC
	mov	ds, ax
	mov	ss, ax
	mov	es, ax
	mov	esp, 0x5000000  ;0x9FFFF0
	
	mov byte [0x7D01], 0
	
	mov edi, 0x300000              ; Set the destination index to 0xB8000.
    mov al, 0x4A   					; Set the A-register to 0x1F201F201F201F20.
    mov ecx, 0xF00000 - 0x300000 	            ; Set the C-register to 500.
	shr ecx, 2
    rep stosd                     ; Clear the screen.


	mov edi, 0x1000000              ; Set the destination index to 0xB8000.
    mov al, 0   					; Set the A-register to 0x1F201F201F201F20.
    mov ecx, 0x4C00000 - 0x1000000 	            ; Set the C-register to 500.
	shr ecx, 2
    rep stosd						 ; Clear the screen.

CopyImage:
    mov	eax, dword [ImageSize]
    movzx	ebx, word [bpbBytesPerSector]
    mul	ebx
    mov	ebx, 4
    div	ebx
    cld
    mov esi, IMAGE_RMODE_BASE
    mov	edi, IMAGE_PMODE_BASE
    mov	ecx, eax
    rep	movsd 
	
	cli
	jmp	CODE_DESC:IMAGE_PMODE_BASE

	hlt
	jmp $

