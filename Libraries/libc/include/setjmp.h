//
//  setjmp.h
//  Banana libc
//
//  Created by Alex Boxall on 11/7/18.
//  Copyright © 2018 Alex Boxall. All rights reserved.
//

#ifndef setjmp_h
#define setjmp_h

#if defined(__cplusplus)
#define restrict			//fixes some 'fun' errors
extern "C" {
#endif

#include <stdint.h>

	typedef uint64_t jmp_buf[16];
	int setjmp(jmp_buf env);
	void longjmp(jmp_buf env, int value);
    
#if defined(__cplusplus)
}
#endif

#endif /* setjmp_h */
