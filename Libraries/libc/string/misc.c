//
//  misc.c
//  Banana libc
//
//  Created by Alex Boxall on 12/7/18.
//  Copyright © 2018 Alex Boxall. All rights reserved.
//

#include "string.h"
#include "assert.h"
#include "errno.h"

void *memset(void *destination, int value, size_t n) {
    int d0, d1;
    void* originalDest = destination;
    __asm__ __volatile__ (
                          "rep\n\t"
                          "stosb"
                          : "=&c" (d0), "=&D" (d1)
                          : "a" (value), "1" (destination), "0" (n)
                          : "memory");
    return originalDest;
}

//1.0594630943592952645618252949463

size_t strlen(const char *s) {
    assert(s != NULL);
    
    size_t i = 0;
	while (s[i]) ++i;
    return i;
}

char *strerror(int errnum) {
    switch (errnum) {
        case EDOM:
            return "Domain Error";
        case EILSEQ:
            return "Illegal Byte Sequence";
        case ERANGE:
            return "Argument Out of Range";
        default:
            return "Unknown Error";
    }
}
