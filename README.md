# README #

### Developer's Note ###
READ THIS: https://stackoverflow.com/questions/132241/hidden-features-of-c

Look at this cool branch prediction trick!
```
#!c
#define likely(x)       __builtin_expect((x),1)
#define unlikely(x)     __builtin_expect((x),0)

void foo(int arg)
{
     if (unlikely(arg == 0)) {
           do_this();
           return;
     }
     do_that();
     ...
}
```

### What is Banana OS ###
Banana OS is an operating system for x86 and x86-64 systems. Banana has 32 and 64 bit editions. It uses a 16/24/32 bit colour GUI, thanks to the VESA video interface. The resolution of the display is dynamically chosen at boot. Applications are run is user mode to ensure programs cannot execute privileged instructions (like halting the system or mucking up important structures). Applications can be written for Banana OS using the API in C++, which is being constantly updated and improved. Now with limited sound support.

### License ###
The license is in the file ```LICENSE```. Read the license thoroughly before using Banana OS.

### Directory Structure ###

- Banana
    - Cursors
    - Drivers
    - Fonts
    - Icons
    - Keymaps
    - Programs
    - Settings
    - System
    
- Users
    - User's Name
        - Documents
        - Desktop

    - User's Name
        - Documents
        - Desktop

### Coding Style ###
The following is the coding style for the project.


**C:**

* Camelcase function and variable names

* Uppercase preprocessor definitions

* 1TBS brace style (like K&R except the opening brace for functions are on the same line as the function definition)


```
#!c

#define CONSTANT 47

//Comment
int function () {
    uint8_t variable = 54;
    return variable;
}

void functionForAPI (uint32_t arg) {
    if (arg == 5) {
        printf("arg = 5");
    } else if (arg <= 10) {
        printf("arg < 10");
    } else {
        printf("arg is greater than 10");
    }
        
}
```



**C++:**

The same as C with these additions:

* Class names capitalised

* Properties and methods in camel-case

* Use structs for for POD (plain old data)

* Always explicitly use ```protected``` and ```public``` in classes (even when not needed), put protected at the top



```
#!c++

class ClassForAPI {
protected:
    int functionName ();
    int protectedVariable;
    
public:
    void function ();
    
};

class NormalClass {
protected:
public:
    void function ();
    
};
```




**Assembly**

* Use a tab to indent

* Place comments just after a label or (if needed) after double newlines

* **Always** use Intel syntax with NASM

* Have two lines between 'function like' labels, and one line between 'inner function labels'

* Always pad hexadecimal values with zeros to ensure the length of the constant is a multiple of 2

* Inner labels indented by half a tab (usually two spaces)

* Prefer ```0x0A``` over ```0Ah```

* Use lowercase for registers and labels

```
#!nasm

first_label dd 0
mov eax, 5

other_label:
    ;part 1 - blah blah
    mov ax, 1
    mov bx, ax
    mov dword first_label, 50
    
    ;part 2 - more blah
    mov ax, 0x00
    mov bx, 0x10
    mov cx, 0x0A
    mov edx, 0x0AAA
    
    mov bx, 'A'
    
  start:
    mov ax, 1
    int 96
    cmp bx 'Z'
    je end
    inc bx
    jmp start
    
  end:
    ret
    

    
```