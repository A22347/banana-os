#include <hw/ata.h>
#include <hw/ports.h>
#include <hw/ide.h>
#include <hal/interrupts.h>
#include <stdbool.h>

unsigned char ide_ata_access(unsigned char direction, unsigned char drive, unsigned int lba,
							 unsigned char numsects, uint8_t* buffer)
{
	unsigned char lba_mode /* 0: CHS, 1:LBA28, 2: LBA48 */, dma /* 0: No DMA, 1: DMA */, cmd;
	unsigned char lba_io[6];
	unsigned int  channel = ide_devices[drive].channel; // Read the Channel.
	unsigned int  slavebit = ide_devices[drive].drive; // Read the Drive [Master/Slave]
	unsigned int  bus = ideChannels[channel].base; // Bus Base, like 0x1F0 which is also data port.
	unsigned int  words = 256; // Almost every ATA drive has a sector-size of 512-byte.
	unsigned short cyl, i;
	unsigned char head, sect, err;

	ide_write(channel, ATA_REG_CONTROL, ideChannels[channel].nIEN = (ide_irq_invoked = 0x0) + 0x02);

	// (I) Select one from LBA28, LBA48 or CHS;
	if (lba >= 0x10000000) { // Sure Drive should support LBA in this case, or you are
							 // giving a wrong LBA.
	   // LBA48:
		lba_mode = 2;
		lba_io[0] = (lba & 0x000000FF) >> 0;
		lba_io[1] = (lba & 0x0000FF00) >> 8;
		lba_io[2] = (lba & 0x00FF0000) >> 16;
		lba_io[3] = (lba & 0xFF000000) >> 24;
		lba_io[4] = 0; // LBA28 is integer, so 32-bits are enough to access 2TB.
		lba_io[5] = 0; // LBA28 is integer, so 32-bits are enough to access 2TB.
		head = 0; // Lower 4-bits of HDDEVSEL are not used here.
	} else if (ide_devices[drive].capabilities & 0x200) { // Drive supports LBA?
	   // LBA28:
		lba_mode = 1;
		lba_io[0] = (lba & 0x00000FF) >> 0;
		lba_io[1] = (lba & 0x000FF00) >> 8;
		lba_io[2] = (lba & 0x0FF0000) >> 16;
		lba_io[3] = 0; // These Registers are not used here.
		lba_io[4] = 0; // These Registers are not used here.
		lba_io[5] = 0; // These Registers are not used here.
		head = (lba & 0xF000000) >> 24;
	} else {
		// CHS:
		lba_mode = 0;
		sect = (lba % 63) + 1;
		cyl = (lba + 1 - sect) / (16 * 63);
		lba_io[0] = sect;
		lba_io[1] = (cyl >> 0) & 0xFF;
		lba_io[2] = (cyl >> 8) & 0xFF;
		lba_io[3] = 0;
		lba_io[4] = 0;
		lba_io[5] = 0;
		head = (lba + 1 - sect) % (16 * 63) / (63); // Head number is written to HDDEVSEL lower 4-bits.
	}

	// (II) See if drive supports DMA or not;
	dma = 0; // We don't support DMA

	// (III) Wait if the drive is busy;
	while (ide_read(channel, ATA_REG_STATUS) & ATA_SR_BSY)
		; // Wait if busy.

	   // (IV) Select Drive from the controller;
	if (lba_mode == 0)
		ide_write(channel, ATA_REG_HDDEVSEL, 0xA0 | (slavebit << 4) | head); // Drive & CHS.
	else
		ide_write(channel, ATA_REG_HDDEVSEL, 0xE0 | (slavebit << 4) | head); // Drive & LBA

	// (V) Write Parameters;
	if (lba_mode == 2) {
		ide_write(channel, ATA_REG_SECCOUNT1, 0);
		ide_write(channel, ATA_REG_LBA3, lba_io[3]);
		ide_write(channel, ATA_REG_LBA4, lba_io[4]);
		ide_write(channel, ATA_REG_LBA5, lba_io[5]);
	}
	ide_write(channel, ATA_REG_SECCOUNT0, numsects);
	ide_write(channel, ATA_REG_LBA0, lba_io[0]);
	ide_write(channel, ATA_REG_LBA1, lba_io[1]);
	ide_write(channel, ATA_REG_LBA2, lba_io[2]);

	if (lba_mode == 0 && dma == 0 && direction == 0) cmd = ATA_CMD_READ_PIO;
	if (lba_mode == 1 && dma == 0 && direction == 0) cmd = ATA_CMD_READ_PIO;
	if (lba_mode == 2 && dma == 0 && direction == 0) cmd = ATA_CMD_READ_PIO_EXT;
	if (lba_mode == 0 && dma == 1 && direction == 0) cmd = ATA_CMD_READ_DMA;
	if (lba_mode == 1 && dma == 1 && direction == 0) cmd = ATA_CMD_READ_DMA;
	if (lba_mode == 2 && dma == 1 && direction == 0) cmd = ATA_CMD_READ_DMA_EXT;
	if (lba_mode == 0 && dma == 0 && direction == 1) cmd = ATA_CMD_WRITE_PIO;
	if (lba_mode == 1 && dma == 0 && direction == 1) cmd = ATA_CMD_WRITE_PIO;
	if (lba_mode == 2 && dma == 0 && direction == 1) cmd = ATA_CMD_WRITE_PIO_EXT;
	if (lba_mode == 0 && dma == 1 && direction == 1) cmd = ATA_CMD_WRITE_DMA;
	if (lba_mode == 1 && dma == 1 && direction == 1) cmd = ATA_CMD_WRITE_DMA;
	if (lba_mode == 2 && dma == 1 && direction == 1) cmd = ATA_CMD_WRITE_DMA_EXT;
	ide_write(channel, ATA_REG_COMMAND, cmd);               // Send the Command.

	if (dma)
		if (direction == 0) {

		}
	// DMA Read.
		else {

		}
	// DMA Write.
	else
		if (direction == 0)
			// PIO Read.
			for (i = 0; i < numsects; i++) {
				if ((err = ide_polling(channel, 1)))
					return err; // Polling, set error and exit if there is.
				asm("rep insw" : : "c"(words), "d"(bus), "D"(buffer)); // Receive Data.
				buffer += (words * 2);
			} else {
				// PIO Write.
				for (i = 0; i < numsects; i++) {
					ide_polling(channel, 0); // Polling.
					//asm("pushw %ds");
					//asm("mov %%ax, %%ds"::"a"(selector));
					asm("rep outsw"::"c"(words), "d"(bus), "S"(buffer)); // Send Data
					//asm("popw %ds");
					buffer += (words * 2);
				}
				ide_write(channel, ATA_REG_COMMAND, (char[])
				{
					ATA_CMD_CACHE_FLUSH,
						ATA_CMD_CACHE_FLUSH,
						ATA_CMD_CACHE_FLUSH_EXT
				}[lba_mode]);
				ide_polling(channel, 0); // Polling.
			}

	return 0; // Easy, isn't it?
}

void ataReadSectors(uint8_t deviceNo, uint64_t sectorNumber, uint8_t count, void* buffer)
{
	ide_ata_access(0, deviceNo, sectorNumber, count, buffer);
	//ataReadWriteSector(deviceNo, sectorNumber, buffer, count, false);
}

void ataWriteSectors(uint8_t deviceNo, uint64_t sectorNumber, uint8_t count, void* buffer)
{
	ide_ata_access(1, deviceNo, sectorNumber, count, buffer);
	//ataReadWriteSector(deviceNo, sectorNumber, buffer, count, true);
}

void readATA(Device* self, int b, int c, void* d)
{
	logk("ATA READ\n");
	ataReadSectors(self->internalDataSize, b, c, d);
	logk("ATA READ DONE\n");
}

void writeATA(Device* self, int b, int c, void* d)
{
	ataWriteSectors(self->internalDataSize, b, c, d);
}

void ataInitDevice(uint8_t deviceNo)
{
	panicWithMessage("ataInitDevice SHOULD NOT BE CALLED");
}

void openATA(Device* self, int b, int c, void* d)
{
	self->internalDataSize = b;
}