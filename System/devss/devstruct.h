#pragma once

#include <stdint.h>
#include <stddef.h>
#include <stdbool.h>

#define MAX_PROPERTIES 1024
#define PROPERTY_LENGTH 16

typedef enum DriverMode
{
	DriverModeOpen,
	DriverModeRead,
	DriverModeWrite,
	DriverModeIoctl,
	DriverModeClose,
	DriverModeHibernate,

} DriverMode;

struct DeviceNode;
typedef struct DeviceNode DeviceNode;

typedef size_t (*PropertyHandler)(char*, int, int, void*);

//this kind of stuff can be looked up using AML bytecode
struct PortRange
{
	uint16_t rangeStart;
	uint8_t rangeLength;	//0 = not used
	uint8_t alignment : 6;	//used by Windows, no idea what it does
	uint8_t width : 2;		//in bytes: e.g 0 = 1, 1 = 2, 2 = 4, 3 = 8
};

struct MemoryRange
{
	uint64_t rangeStart;
	uint64_t rangeLength;
};

struct ResourceList
{
	struct PortRange ports[64];
	struct MemoryRange memory[64];

	int portsUsed;
	int memoryUsed;

	uint8_t irqs[16];
};

typedef struct Device
{
	struct Device* parent;
	DeviceNode* children;

	void* internalData;
	int internalDataSize;

	volatile bool returned;
	volatile int returnValue;

	bool onDiskDriver;

	//if on disk:
	char* driverLocation;

	//if not:
	void (*openPointer) (struct Device* self, int, int, void*);
	void (*readPointer) (struct Device* self, int, int, void*);
	void (*writePointer) (struct Device* self, int, int, void*);
	void (*ioctlPointer) (struct Device* self, int, int, void*);
	void (*hibernatePointer) (struct Device* self, int);

	struct ResourceList resources;

	uint16_t vendorID;
	uint8_t devClass;
	uint8_t subClass;
	uint8_t progIF;

	int interrupt;

	char humanName[256];

} Device;

typedef struct DeviceNode
{
	DeviceNode* next;
	Device* device;

} DeviceNode;

typedef struct DriverPointerTable
{
	void (*open) (struct Device* self, int, int, void*);
	void (*read) (struct Device* self, int, int, void*);
	void (*write) (struct Device* self, int, int, void*);
	void (*ioctl) (struct Device* self, int, int, void*);
	void (*close) (struct Device* self, int, int, void*);
	void (*detect) (struct Device* self, int, int, void*);
	void (*disableLegacy) (struct Device* self);
	void (*hibernate) (struct Device* self);
	void (*unhibernate) (struct Device* self);
	void (*supportsHibernation) (struct Device* self);
	void (*powerSaving) (struct Device* self, int, int, void*);

} DriverPointerTable;