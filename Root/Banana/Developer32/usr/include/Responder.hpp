//
//  Responder.hpp
//  Banana API (New)
//
//  Created by Alex Boxall on 18/5/18.
//  Copyright © 2018 Alex Boxall. All rights reserved.
//

#ifndef Responder_hpp
#define Responder_hpp

#include "Exception.hpp"
#include "Event.h"
#include "RequiredImporting.hpp"

/*! \file Responder.hpp
 \brief A header file defining the Responder class.
 */

class Responder;

/*!
 \typedef EventHandler
 \brief A type which holds a pointer to a function that is able to respond to a event
 */
typedef void (Responder::*EventHandler) (Event);

/*!
 \class Responder
 \brief Base class for objects that react to events.
 
 The Responder class is the base class for all other classes that need to
 react to events sent by the system. Responder objects are not usually used
 on their own, they are nearly always superclassed. 
 */

class Responder {
protected:
    EventHandler eventHandlers[_EventLast][8];      ///>_EventLast is always the last in the enum
    
public:
    Responder();
    void reactToEvent (Event event);
    void installEventHandler (EventType eventType, EventHandler eventHandler);
    
};

#endif /* Responder_hpp */
