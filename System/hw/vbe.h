#pragma once

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>

	int vbeAvaliable ();
	void vbeInit (uint8_t displayno);

	void vbeCopyBufferTo (uint32_t* buffer, uint32_t start, uint32_t size, uint8_t displayNo);
	void vbeCopyBufferFrom (uint32_t* buffer, uint32_t start, uint32_t size, uint8_t displayNo);

	void vbeSetPixel (uint32_t location, uint32_t colour, uint8_t displayNo);
	uint32_t vbeGetPixel (uint32_t location, uint8_t displayNo);

#ifdef __cplusplus
}
#endif