#include <core/smp.h>
#include <hw/ports.h>
#include <hw/apic.h>
#include <core/string.h>			//memset
#include <fs/vfs.h>
#include <debug/log.h>
#include <core/panic.h>

void delayForOneTick ()
{
	asm volatile ("sti");
	asm volatile ("hlt");
	asm volatile ("hlt");
}


//ALL LOCAL APICS ARE AT THE SAME ADDRESS, BUT EACH PROCESSOR 'SEES' THESES ADDRESSES DIFFERENTLY
//THE LOCAL APIC ADDRESS IS 0xFEE00000 BY DEFAULT (USE cpuGetAPICBase)

//THE I/O APICS ARE AT DIFFERNT ADDRESSES, THE FIRST USUALLY BEING AT 0xFEC00000, BUT CAN BE FOUND
//BY LOOKING IN THE ioapicAddresses ARRAY

extern void enableA20 ();

// address MUST be 4K aligned (page aligned)
void setupCPUNumber (uint32_t address, uint8_t cpuNumber, uint8_t apicID)
{
	asm volatile ("cli");

	logk ("Setting up a CPU right now (number: %d)\n", cpuNumber);

	uint32_t base = cpuGetAPICBase ();

	// Each processor/core capable of handling interrupts will have a Local APIC. Usually, there is only one I/O APIC having up to 24 interrupt vectors. However, there can be more than one I/O APIC.
	
	// this stuff isn't going to work, as it expects an I/O APIC for each core, not a Local APIC for each core
	uint32_t* p32 = (uint32_t*) (size_t) CPU_APICID_LOCATION;
	*p32++ = (uint32_t) apicID;			//APIC ID

	p32 = (uint32_t*) CPU_CPUID_LOCATION;
	*p32++ = (uint32_t) cpuNumber;			//cpuNumber

	uint8_t* ppp = (uint8_t*) (size_t) CPU_ID_BYTE;
	*ppp = cpuNumber;

	ppp = (uint8_t*) (size_t) CPU_READY_BYTE;
	*ppp = 0;

	//this block was not in the forum post, but from reading the wiki, it seems correct
	p32 = (uint32_t*) (size_t) (base + 0x310);
	*p32 = (*p32) | (apicID << 24);
	delayForOneTick ();

	p32 = (uint32_t*) (size_t) (base + 0x300);
	*p32 = 0x4500;
	delayForOneTick ();

	*p32 = 0x4600 | (address / 0x1000);	//OR with the page number to start execution
	
	uint16_t timeout = 0;
	while (1) {
		ppp = (uint8_t*) (size_t) CPU_READY_BYTE;
		if (*ppp) {
			//no need to send again if the CPU has started
			break;
		}
		++timeout;
		if (!timeout) {
			*p32 = 0x4600 | (address / 0x1000);	//OR with the page number to start execution
			delayForOneTick ();
			break;
		}
	}		

	delayForOneTick ();

	volatile uint8_t* volatile p = (volatile uint8_t* volatile) 0x7D00;
	uint32_t timeout2 = 0;
	while ((*p) == 0) {
		p = (volatile uint8_t* volatile) 0x7D00;
		++timeout2;
		if (timeout2 > 20000) {
			logk ("CPU WAIT TIMEOUT!\n");
			break;
		}
	}

	enableA20 ();
	asm volatile ("sti");

	ppp = (uint8_t*) (size_t) CPU_ID_BYTE;
	*ppp = 0;

	ppp = (uint8_t*) (size_t) CPU_READY_BYTE;
	*ppp = 0;
}

void setupOtherCPUs ()
{
	// This *SHOULD* set up all of the CPUs and put them in an infinte loop

	numberOfCPUsUsed = 1;

	memset ((void*) (size_t) CPU_READY_ARRAY, STATUS_BYTE_NOTSTARTED, MAX_CPUS);

	// get the CPU number from the APIC stables in the APCI (we've already scanned them,
	// we just need to save the correct byte 
	// https://wiki.osdev.org/MADT, Entry Type 0: ACPI Processor ID
	
	// load the trampoline here...

	uint8_t* a = (uint8_t*) CPU_BOOTLOADER1;
	
	logk ("About to enter the 'load the CPU' loop.\n");

	for (uint8_t i = 1; i < processorDiscoveryNumber; ++i) {
		logk ("Yes, yes, yes, yes, YES, YES, YES, YES, **YES**, **YES**, **YESS*, **YESS**!!\n");
		vfs_file_t f;

		int res = vfs_open (&f, "C:/Banana/System/Tramp.exe", vfs_file_mode_read);
		uint64_t actual;
		res |= vfs_read (&f, a, 2048, &actual);
		vfs_close (&f);

		if (res) {
			panicWithMessage ("THE_TRAMPOLINE_IS_BROKEN");
		}

		setupCPUNumber (CPU_BOOTLOADER1, processorID[i], matchingAPICID[i]);
	}
}
