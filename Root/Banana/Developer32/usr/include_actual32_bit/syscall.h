//
//  syscall.h
//  Banana libc
//
//  Created by Alex Boxall on 12/7/18.
//  Copyright © 2018 Alex Boxall. All rights reserved.
//

#ifndef syscall_h
#define syscall_h

#include <stddef.h>

extern int printf(const char* f, ...);

void __i_system_call_abort(void);
void __i_system_call_exit(int code);
int __i_system_call_system(const char* string);
char* __i_system_call_getenv(const char* name);
int __i_system_call_unlink(const char* filename);
int __i_system_call_rename(const char* old, const char* new_);
int __i_system_call_openfile(unsigned long long* fdout, const char* filename, unsigned char mode);
int __i_system_call_readfileorpipe(unsigned long long fd, unsigned char* bf, unsigned long long len);
int __i_system_call_writefileorpipe(unsigned long long fd, const unsigned char* bf, unsigned long long len);
int __i_system_call_pipe(long long unsigned int*, long long unsigned int* fdWrite);
int __i_system_call_getstream (unsigned long long* output, int type);
int __i_system_call_malloc_location (size_t* base, size_t* size);

int __i_system_call_time (unsigned long long* timeInSecs, unsigned long long* ticksSinceStart);
int __i_system_call_tell (unsigned long long* fd, unsigned long long* outputPtr);
int __i_system_call_seek (unsigned long long* fd, unsigned long long where, int fromWhence);

int __i_system_call_getcwd (int size, char* location);
int __i_system_call_setcwd (char* location);

int __i_system_call_opendir (unsigned long long* fdout, const char* filename);
int __i_system_call_readdir (unsigned long long* fd, void* direntObject);
int __i_system_call_closedir (unsigned long long* fdout);

void __i_system_call_execve(char* path, char** argv, char** envr);

void __i_system_call_wait(int pid);
int __i_system_call_spawn(char* path);
int __i_system_call_fork();

#endif /* syscall_h */
