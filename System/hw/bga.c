#include <hw/bga.h>
#include <hw/pci.h>
#include <libk/string.h>
#include <hal/display.h>

#define VBE_DISPI_TOTAL_VIDEO_MEMORY_MB 8

#define VBE_DISPI_BANK_ADDRESS          0xA0000
#define VBE_DISPI_BANK_SIZE_KB          64

#define VBE_DISPI_MAX_XRES              1024
#define VBE_DISPI_MAX_YRES              768

#define VBE_DISPI_IOPORT_INDEX          0x01CE
#define VBE_DISPI_IOPORT_DATA           0x01CF

#define VBE_DISPI_INDEX_ID              0x0
#define VBE_DISPI_INDEX_XRES            0x1
#define VBE_DISPI_INDEX_YRES            0x2
#define VBE_DISPI_INDEX_BPP             0x3
#define VBE_DISPI_INDEX_ENABLE          0x4
#define VBE_DISPI_INDEX_BANK            0x5
#define VBE_DISPI_INDEX_VIRT_WIDTH      0x6
#define VBE_DISPI_INDEX_VIRT_HEIGHT     0x7
#define VBE_DISPI_INDEX_X_OFFSET        0x8
#define VBE_DISPI_INDEX_Y_OFFSET        0x9

#define VBE_DISPI_ID0                   0xB0C0
#define VBE_DISPI_ID1                   0xB0C1
#define VBE_DISPI_ID2                   0xB0C2
#define VBE_DISPI_ID3                   0xB0C3
#define VBE_DISPI_ID4                   0xB0C4

#define VBE_DISPI_DISABLED              0x00
#define VBE_DISPI_ENABLED               0x01
#define VBE_DISPI_VBE_ENABLED           0x40
#define VBE_DISPI_NOCLEARMEM            0x80

#define VBE_DISPI_LFB_ENABLED			0x40

#define VBE_DISPI_LFB_PHYSICAL_ADDRESS  0xE0000000

uint8_t* bgaFramebuffer;

void bgaWriteRegister (unsigned short IndexValue, unsigned short DataValue)
{
	outw (VBE_DISPI_IOPORT_INDEX, IndexValue);
	outw (VBE_DISPI_IOPORT_DATA, DataValue);
}

unsigned short bgaReadRegister (unsigned short IndexValue)
{
	outw (VBE_DISPI_IOPORT_INDEX, IndexValue);
	return inw (VBE_DISPI_IOPORT_DATA);
}

int bgaAvaliable ()
{
	return bgaReadRegister (VBE_DISPI_INDEX_ID) == VBE_DISPI_ID4;
}

void bgaInit (uint8_t displayno)
{
	logk ("BGA INIT\n");
	bgaSetMode (1366, 768, 32);
}

void bgaCopyBufferTo (uint32_t* buffer, uint32_t start, uint32_t size, uint8_t displayNo)
{
	memcpy (bgaFramebuffer + start, buffer + start, size);
}

void bgaCopyBufferFrom (uint32_t* buffer, uint32_t start, uint32_t size, uint8_t displayNo)
{
	memcpy (buffer + start, bgaFramebuffer + start, size);
}

void bgaSetPixel (uint32_t location, uint32_t colour, uint8_t displayNo)
{
	uint32_t ba = location * BYTES_PER_PIXEL;

	if (location > (unsigned) (TOTAL_HEIGHT * ONE_LINE * BYTES_PER_PIXEL)) {
		return;
	}

	if (BYTES_PER_PIXEL >= 3) {
		bgaFramebuffer[ba++] = colour & 255;
		bgaFramebuffer[ba++] = (colour >> 8) & 255;
		bgaFramebuffer[ba]   = (colour >> 16) & 255;
	}
}

uint32_t bgaGetPixel (uint32_t location, uint8_t displayNo)
{
	uint32_t ba = location * BYTES_PER_PIXEL;
	uint32_t colour = 0;

	if (location > (unsigned) (TOTAL_HEIGHT * ONE_LINE * BYTES_PER_PIXEL)) {
		return 0;
	}

	if (BYTES_PER_PIXEL >= 3) {
		colour = bgaFramebuffer[ba++] << 0;
		colour |= bgaFramebuffer[ba++] << 8;
		colour |= bgaFramebuffer[ba] << 16;
	}

	return colour;
}

void bgaSetMode (uint16_t width, uint16_t height, uint8_t bitsPerPixel)
{
	/*
	PCIDeviceInfo info = getPCIInfoFromDeviceTypeAndVendor (0x12, 0x34, 0x1111);		//this line needs: bgaDevice->parent->getProperty("PCITYPE"), bgaDevice->parent->getProperty("PCIVENDOR"), etc.

	if (info.vendorID == 0xFFFF) {
		logk ("Using ISA BGA\n");
		bgaFramebuffer = (uint8_t*) (size_t) VBE_DISPI_LFB_PHYSICAL_ADDRESS;
	} else {
		logk ("Using PCI BGA\n");
		bgaFramebuffer = (uint8_t*) (size_t) info.bar[0];
	}

	logk ("BGA framebuffer is at 0x%X\n", bgaFramebuffer);

	bgaWriteRegister (VBE_DISPI_INDEX_ENABLE, VBE_DISPI_DISABLED);
	bgaWriteRegister (VBE_DISPI_INDEX_XRES, width);
	bgaWriteRegister (VBE_DISPI_INDEX_YRES, height);
	bgaWriteRegister (VBE_DISPI_INDEX_BPP, bitsPerPixel);
	bgaWriteRegister (VBE_DISPI_INDEX_ENABLE, VBE_DISPI_ENABLED | VBE_DISPI_LFB_ENABLED);

	BYTES_PER_PIXEL = bitsPerPixel / 8;
	TOTAL_HEIGHT = height;
	TOTAL_WIDTH = width;
	ONE_LINE = width;*/
}