#pragma once
#include <stdint.h>
#include <stdbool.h>

uint8_t readCMOS(uint8_t reg);
void writeCMOS(uint8_t reg, uint8_t num);
void enableNMI(bool on);