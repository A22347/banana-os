#ifdef __cplusplus
extern "C" {
#endif

	void floatingInit ();
	void floatingLoad (void*);
	void floatingStore (void*);

#ifdef __cplusplus
}
#endif