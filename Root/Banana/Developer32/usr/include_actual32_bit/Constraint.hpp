//
//  Constraint.hpp
//  Banana API (New)
//
//  Created by Alex Boxall on 20/5/18.
//  Copyright © 2018 Alex Boxall. All rights reserved.
//

#ifndef Constraint_hpp
#define Constraint_hpp

#include "RequiredImporting.hpp"

class View;

/*! \file Constraint.hpp
 \brief A header file defining the Constraint class.
 */

/*!
 \enum ConstraintDirection
 \brief Directions in which a constraint may apply
 */
typedef enum ConstraintDirection {
    ConstraintDirectionAbove,
    ConstraintDirectionBelow,
    ConstraintDirectionLeft,
    ConstraintDirectionRight
    
} ConstraintDirection;

/*!
 \enum ConstraintHook
 \brief Which part of the related object the constraint is attached to
 */
typedef enum ConstraintHook {
    ConstraintHookLeftSide,
    ConstraintHookRightSide,
    ConstraintHookTopSide,
    ConstraintHookBottomSide
    
} ConstraintHook;

/*!
 \class Constraint
 \brief Constraints allow graphical controls to be placed and resized in relation to each other.
 
 ...
 */

class Constraint {
protected:
    View* link;
    int min;
    int max;
    ConstraintDirection dir;
    ConstraintHook hook;
    
public:
    Constraint ();
    Constraint (View* linkedTo, int minDist, int maxDist, ConstraintDirection direction, ConstraintHook hook);
    
    int calculateConstraint(int startValue, bool usingY);
};

#endif /* Constraint_hpp */
