#ifndef __MATH_H__
#define __MATH_H__

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>

#define HUGE_VAL (1 << 31)
#define M_E  2.718281828459
#define M_PI 3.141592653589
#define M_LOG2E 1.44269504088896340736

	// LITTLE ENDIAN VERSION!!! SWAP LSW and MSW for BIG ENDIAN
typedef union
{
	double value;
	struct
	{
		uint32_t lsw;
		uint32_t msw;
	} parts;

} ieee_double_shape_type;

#define EXTRACT_WORDS(ix0,ix1,d)				\
do {								\
  ieee_double_shape_type ew_u;					\
  ew_u.value = (d);						\
  (ix0) = ew_u.parts.msw;					\
  (ix1) = ew_u.parts.lsw;					\
} while (0)

#define INSERT_WORDS(d,ix0,ix1)					\
do {								\
  ieee_double_shape_type iw_u;					\
  iw_u.parts.msw = (ix0);					\
  iw_u.parts.lsw = (ix1);					\
  (d) = iw_u.value;						\
} while (0)

	double acos (double x);
	double asin (double x);
	double atan (double x);
	double atan2 (double y, double x);
	double tan (double x);
	double cos (double x);
	double cosh (double x);
	double sin (double x);
	double sinh (double x);
	double tanh (double x);
	double exp (double x);
	double log (double x);
	double log2 (double x);
	double log10 (double x);
	double logf (double a);
	double modf (double x, double* integer);
	double pow (double x, double y);
	double sqrt (double x);
	double ceil (double x);
	double fabs (double x);
	double floor (double x);
	double fmod (double x, double y);

	float fmaf (float x, float y, float z);
	double fma (double x, double y, double z);
	long double fmal (long double x, long double y, long double z);

	float frexpf (float arg, int* exp);
	double frexp (double arg, int* exp);
	long double frexpl (long double arg, int* exp);

	float logbf (float arg);
	double logb (double arg);
	long double logbl (long double arg);

	float  scalbnf (float arg, int exp);
	double scalbn (double arg, int exp);
	long double scalbnl (long double arg, int exp);

	float  scalblnf (float arg, long exp);
	double scalbln (double arg, long exp);
	long double scalblnl (long double arg, long exp);

	float ldexpf (float arg, int exp);
	double ldexp (double arg, int exp);
	long double ldexpl (long double arg, int exp);

	long double ldexpll (long double arg, long exp);

#ifdef __cplusplus
}
#endif

#endif