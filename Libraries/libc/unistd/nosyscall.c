//
//  nosyscall.c
//  Banana libc
//
//  Created by Alex Boxall on 18/10/18.
//  Copyright © 2018 Alex Boxall. All rights reserved.
//

#include "unistd.h"
#include <stdint.h>

/*
void swab(const void *restrict src, void *restrict dest, ssize_t n) {
    if (n == 0) {
        return;
    }
    
    for (ssize_t i = 0; i < n >> 1; ++i) {
        uint16_t val = *((uint16_t*)src + i);
        *((uint16_t*)dest + i) = (val << 8) | ((val >> 8) & 0x00ff); // right-shift sign-extends, so force to zero
    }
    
    if (n & 1) {
        *((uint8_t*)dest + n - 1) = *((uint8_t*)src + n - 1);
    }
}
*/