#include <string.h>
#include <ctype.h>

//Give these to <stdlib.h> later as itoa and atoi
int __str_int(char* c) {
    int o = 0;
    bool negative = false;
    for (int i = 0; c[i]; ++i) {
        if (c[i] == '-') {
            negative = true;
            continue;
        }
        o *= 10;
        o += c[i] - 48;
    }
    return (negative) ? -o : o;
}

char* __int_str(int i, char b[]) {
    char const digit[] = "0123456789";
    char* p = b;
    if (i < 0) {
        *p++ = '-';
        i *= -1;
    }
    
    int shifter = i;
    do {
        ++p;
        shifter = shifter / 10;
    } while (shifter);
    
    *p = '\0';
    do {
        *--p = digit[i % 10];
        i = i / 10;
    } while (i);
    
    return b;
}

int strlen (const char* c)
{
	int i = 0;
	while (c[i]) {
		++i;
	}
	return i;
}

int strcmp (const char* s1, const char* s2)
{
	while (*s1 && (*s1 == *s2))
		s1++, s2++;
	return *(const unsigned char*) s1 - *(const unsigned char*) s2;
}

int strcasecmp (const char* s1, const char* s2)
{
	while (*s1 && (toupper(*s1) == toupper(*s2)))
		s1++, s2++;
	return *(const unsigned char*) s1 - *(const unsigned char*) s2;
}

int strncmp (const char* s1, const char* s2, size_t n)
{
	while (n--)
		if (*s1++ != *s2++)
			return *(unsigned char*) (s1 - 1) - *(unsigned char*) (s2 - 1);
	return 0;
}

int strncasecmp (const char* s1, const char* s2, size_t n)
{
	while (n--)
		if (toupper(*s1++) != toupper(*s2++))
			return *(unsigned char*) (s1 - 1) - *(unsigned char*) (s2 - 1);
	return 0;
}

void* memcpy (void* destination, const void* source, size_t num)
{
	__asm__ __volatile__ ("cld ; rep movsb" :: "S"(source), "D"(destination), "c"(num) : "flags", "memory");
	return destination;
}

char* strcpy (char* a, const char* b)
{
	for (int i = 0; b[i]; ++i) {
		a[i] = b[i];
	}
	return a;
}

char* strncpy (char* a, const char* b, size_t n)
{
	size_t i;

	for (i = 0; i < n && b[i] != '\0'; i++) {
		a[i] = b[i];
	}
		
	for (; i < n; i++) {
		a[i] = '\0';
	}

	return a;
}

char* strcat (char* dest, const char* src)
{
	char* temp = dest;
	while (*dest) ++dest;
	while ((*dest++ = *src++));
	return temp;
}

char* strncat (char *dest, const char *src, size_t n)
{
	size_t dest_len = strlen (dest);
	size_t i;

	for (i = 0; i < n && src[i]; i++)
		dest[dest_len + i] = src[i];
	dest[dest_len + i] = 0;

	return dest;
}

char* strstr (char* main, char* sub)
{
	for (int i = 0; main[i]; ++i) {
		bool failed = false;
		for (int a = 0; sub[a]; ++a) {
			if (main[i + a] != sub[a]) {
				failed = true;
				break;
			}
		}
		if (!failed) {
			return main + i;
		}
	}
	return 0;
}

void* memchr (const void* str, int c, size_t n)
{
	for (size_t i = 0; i < n; ++i) {
		if (((uint8_t*) str)[i] == c) {
			return (uint8_t*)str + i;
		}
	}
	return 0;
}

int memcmp (const void* s1, const void* s2, size_t n)
{
	const unsigned char *p1 = s1, *p2 = s2;
	while (n--)
		if (*p1 != *p2)
			return *p1 - *p2;
		else
			p1++, p2++;
	return 0;
}

void* malloc (size_t);
void free (void*);

void memmove (void *dest, const void *src, size_t n)
{
	char *csrc = (char *) src;
	char *cdest = (char *) dest;

	char *temp = malloc (n);

	for (size_t i = 0; i<n; i++)
		temp[i] = csrc[i];

	for (size_t i = 0; i<n; i++)
		cdest[i] = temp[i];

	free (temp);
}

void* memset (void* b, int c, size_t len)
{
	size_t i = 0;
	unsigned char* p = b;
	while (len > 0) {
		*p = c;
		p++;
		len--;
	}
	return b;
}

char* strchr (const char* string, int repl)
{
	for (int i = 0; string[i]; ++i) {
		if (string[i] == (char) repl) {
			return (char*) string + i;
		}
	}
	return 0;
}

char* strrchr (const char* string, int repl)
{
	for (int i = strlen (string) - 1; i; --i) {
		if (string[i] == (char) repl) {
			return (char*) string + i;
		}
	}
	return 0;
}

char* strdup (const char* s)
{
	char *d = malloc (strlen (s) + 1);
	if (!d) {
		return d;
	}
	strcpy (d, s);
	return d;
}

size_t strspn (const char *str1, const char *str2)
{
	size_t count = 0;
	for (; str1[count]; ++count) {
		bool found = false;
		for (int i = 0; str2[i]; ++i) {
			if (str2[i] == str1[count]) {
				found = true;
				break;
			}
		}
		if (!found) {
			return count;
		}
	}
	return count;
}

size_t strcspn (const char *str1, const char *str2)
{
	size_t count = 0;
	for (; str1[count]; ++count) {
		bool found = false;
		for (int i = 0; str2[i]; ++i) {
			if (str2[i] == str1[count]) {
				found = true;
				break;
			}
		}
		if (found) {
			return count;
		}
	}
	return count;
}

char* strtok (char *s, const char *delim)
{
	static char* last = 0;
	if (s) {
		last = s;
	} else if (!last) {
		return 0;
	}
	s = last + strspn (last, delim);
	last = s + strcspn (s, delim);
	if (last == s) {
		return last = 0;
	}
	last = *last ? *last = 0, last + 1 : 0;
	return s;
}

char* strpbrk (const char* str1, const char* str2)
{
	size_t count = 0;
	for (; str1[count]; ++count) {
		bool found = false;
		for (int i = 0; str2[i]; ++i) {
			if (str2[i] == str1[count]) {
				found = true;
				break;
			}
		}
		if (found) {
			return (char*) str1 + count;
		}
	}
	return (char*) str1 + count;
}

char* strerror (int n)
{
	return "unknown error";
}