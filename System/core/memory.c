#include <core/memory.h>
#include <core/spinlock.h>
#include <core/panic.h>
#include <debug/log.h>
#include <debug/profile.h>


/*

WE'RE GOING TO RENAME THIS MEMORY MANAGER 'ACID'

DO YOU KNOW WHY? BECAUSE IT SLOWLY CORRODES EVERYTHING IT TOUCHES...

WE'LL CALL OUR NEXT ONE 'ALAKI' TO CANCEL OUT THE DAMAGE

EITHER THAT, OR MAYBE WE'RE FREEING MEMORY TOO EARLY SOMEWHERE?

*/

char* blocksUsed;
int tried = 0;
uint8_t* lastFreeAddress;
uint8_t* lastMallocAddress;

//in theory it would be good to make them share the same lock, so 
//all malloc/free accesses always have exclusive use of the tables.
//however, realloc creates a problem, as it locks realloc, then calls
//both malloc and free. This means the malloc/free will be forever waiting
//for realloc to give in, which it can't until malloc/free are called.
DECLARE_LOCK (memoryLock);
DECLARE_LOCK (freeLock);
DECLARE_LOCK (reallocLock);

bool validMemory (uint8_t* address)
{
	uint64_t* rdp = (uint64_t*) RAM_TABLE_LOCATION;
	if (ramTableLength == 0) {
		logk ("No RAM table!\n");
		return true;
	}
	for (int i = 0; i < ramTableLength; ++i) {
		uint64_t addr = *(rdp + 0);
		uint64_t length = *(rdp + 1);
		uint64_t type = *(((uint32_t*) rdp) + 4);

		uint8_t* bottom = (uint8_t*) ((size_t) addr);
		uint8_t* top = (uint8_t*) ((size_t) addr) + length;

		if (address >= bottom && address < top && type != 1) {
			logk ("memory at 0x%X is bad!\n", address);

			return false;
		}

		rdp += 3;	//24 bytes / uint64_t = 3
	}

	return true;
}

uint8_t* highestMalloc = 0;
spinlock_t mallocSpinlock;

void* malloc (size_t size)
{
	lockSpinlock (&mallocSpinlock);
	//if (__internal_login_variable__loginComplete) asm ("cli");

	tried = 0;
	int stage = 0;
	uint8_t* ptr = lastFreeAddress;
	unsigned int blocksAlloced = 0;
	int fails = 0;
	while (blocksAlloced < size) {
		if ((size_t) ptr >= (size_t) MAX_KMALLOC_DATA_LOCATION - BLOCK_SIZE - 1) {
			++fails;
			ptr = (uint8_t*) (size_t) KMALLOC_DATA_LOCATION;
			lastFreeAddress = (uint8_t*) (size_t) KMALLOC_DATA_LOCATION;
			blocksAlloced = 0;

			if (fails == 2) {
				panicWithMessage("KMALLOC_EXHAUSTED");
			}
			continue;
		}
		//logk ("pointer now at %d, checking block entry %d\n", ptr, (((unsigned int) ptr + blocksAlloced) - KMALLOC_DATA_LOCATION) / BLOCK_SIZE);
		if (blocksUsed[(((size_t) ptr + blocksAlloced) - KMALLOC_DATA_LOCATION) / BLOCK_SIZE] == 0 && validMemory (ptr) && validMemory (ptr + blocksAlloced)) {
			blocksAlloced += BLOCK_SIZE;
		} else {
			ptr += blocksAlloced + BLOCK_SIZE;

			if (stage == 0) {
				stage = 1;
				ptr = lastMallocAddress;
			}

			blocksAlloced = 0;
		}
	}
	for (unsigned i = 0; i < blocksAlloced / BLOCK_SIZE; ++i) {
		if (i == blocksAlloced / BLOCK_SIZE - 1) {
			blocksUsed[((((size_t) ptr) - KMALLOC_DATA_LOCATION) / BLOCK_SIZE) + i] = 2;		//show that this is the last in the block
		} else if (i == 0) {
			blocksUsed[((((size_t) ptr) - KMALLOC_DATA_LOCATION) / BLOCK_SIZE) + i] = 3;		//show that this is the first in the block (used for saftey checks in free)

		} else {
			blocksUsed[((((size_t) ptr) - KMALLOC_DATA_LOCATION) / BLOCK_SIZE) + i] = 1;
		}
	}
	memset (ptr, 0, blocksAlloced);
	lastMallocAddress = ptr + blocksAlloced;
	freeBytesApprox -= blocksAlloced;

	unlockSpinlock (&mallocSpinlock);

	return ptr;
}

void* calloc (size_t size, size_t block)
{
	void* c = malloc (size * block);
	memset (c, 0, size * block);
	return c;
}

void free (void *blk)
{
	logk("OLD FREE CALLED ON BLOCK 0x%X\n", blk);
	return;
	void freee(void* blk);
	freee(blk);
}

void freee (void *blk)
{
	//LOCK (freeLock);
	unsigned int locator = (unsigned) ((size_t) blk);

	//must start with a block marked as '3', OR it could be the final block, and hence, a '2'
	if (blocksUsed[((locator - KMALLOC_DATA_LOCATION) / BLOCK_SIZE)] != 3 && blocksUsed[((locator - KMALLOC_DATA_LOCATION) / BLOCK_SIZE)] != 2) {
		logk("ERR: free called on non-allocated memory. error fault 14 fault 13 (just here to @@@ get people to come and look). NOT FREEING MEMORY\n");
		return;
	}

	while (blocksUsed[((locator - KMALLOC_DATA_LOCATION) / BLOCK_SIZE)]) {
		if (blocksUsed[((locator - KMALLOC_DATA_LOCATION) / BLOCK_SIZE)] == 2) {
			blocksUsed[((locator - KMALLOC_DATA_LOCATION) / BLOCK_SIZE)] = 0;
			break;
		}
		blocksUsed[((locator - KMALLOC_DATA_LOCATION) / BLOCK_SIZE)] = 0;
		locator += BLOCK_SIZE;
	}
	lastFreeAddress = blk;
	freeBytesApprox += locator - (size_t) blk + BLOCK_SIZE;
	//UNLOCK (freeLock);
}

void heapInit ()
{
	blocksUsed = (char*) INTERNAL_MALLOC_LOCATION;
	memset (blocksUsed, 0, BLOCKS);

	logk ("%d blocks...\n", BLOCKS);

	lastFreeAddress = (uint8_t*) (size_t) KMALLOC_DATA_LOCATION;
	lastMallocAddress = (uint8_t*) (size_t) KMALLOC_DATA_LOCATION;

	initSpinlock (&mallocSpinlock);
}

void* realloc (void *ptr, size_t size)
{
	//LOCK (reallocLock);
	logk ("Realloc called\n");

	// uses the same code as free to work out how many bytes long the area is 
	unsigned int locator = (unsigned) ((size_t) ptr);
	unsigned int bytes = 0;

	while (blocksUsed[((locator - KMALLOC_DATA_LOCATION) / BLOCK_SIZE)]) {
		logk ("0x%X: %d\n", locator, ((locator - KMALLOC_DATA_LOCATION) / BLOCK_SIZE));
		if (blocksUsed[((locator - KMALLOC_DATA_LOCATION) / BLOCK_SIZE)] == 2) {
			bytes += BLOCK_SIZE;
			break;
		}
		locator += BLOCK_SIZE;
		bytes += BLOCK_SIZE;
	}

	logk ("Previous size = %d\n", bytes);

	// no need to resize downward
	if (size <= bytes) {
		logk ("No change needed...\n");
		//UNLOCK (reallocLock);
		return ptr;

	} else {						//allocate new data, write to new data, free old data
		logk ("Allocating %d bytes...\n", size);
		void* a = malloc (size);
		if (!a) {
			logk ("RETURNING NULL!\n");
			//UNLOCK (reallocLock);
			return 0;
		}
		memcpy (a, ptr, bytes);
		freee (ptr);
		//UNLOCK (reallocLock);
		return a;
	}

	//UNLOCK (reallocLock);
}