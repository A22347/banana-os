
extern "C" {
	#include <stdio.h>
	#include <stdlib.h>
}

#include <stddef.h>

extern "C" void __cxa_pure_virtual()
{
	// Do nothing or print an error message.
}

void* operator new(size_t size)
{
	return malloc(size);
}

void* operator new[](size_t size)
{
	return malloc(size);
}

void operator delete(void* p)
{
	free(p);
}

void operator delete[](void* p)
{
	free(p);
}

#include <Window.hpp>
#include <Label.hpp>

void applictionBegin()
{
	printf("Hello World!\n");

	Window* root = new Window(nullptr, "Root Window!", 100, 100, 600, 400);
	Label* label = new Label(root, 50, 50, "Hello, World!");

	windowWithFocus = root;

	root->focus = true;
	root->mainloop();
}

extern "C" int main(int argc, char* argv[])
{
	applictionBegin();
	return 0;
}

