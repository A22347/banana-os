#pragma once
#include <core/kernel.h>
#include <core/objectmgr.h>
#include <core/paging.h>
#include <gui/guilink.h>

void loadTaskFromASM ();
void storeDataFromASM ();

/*
typedef int threadID_t;
typedef int processID_t;*/

extern uint8_t st_loadedBefore;
#if PLATFORM_ID == 86
extern uint32_t taskno;
extern uint64_t stackptr;
extern uint64_t stackinc;
extern uint32_t st_eax;
extern uint32_t st_ebx;
extern uint32_t st_ecx;
extern uint32_t st_edx;
extern uint32_t st_ebp;
extern uint32_t st_eip;
extern uint32_t st_esp;
extern uint32_t st_esi;
extern uint32_t st_edi;
extern uint32_t st_eflags;
extern uint16_t st_cs;
extern uint16_t st_ds;
extern uint16_t st_es;
extern uint16_t st_fs;
extern uint16_t st_gs;
extern uint16_t st_ss;
#else
extern uint32_t taskno;
extern uint64_t stackptr;
extern uint64_t stackinc;
extern uint64_t st_eax;
extern uint64_t st_ebx;
extern uint64_t st_ecx;
extern uint64_t st_edx;
extern uint64_t st_ebp;
extern uint64_t st_eip;
extern uint64_t st_esp;
extern uint64_t st_esi;
extern uint64_t st_edi;
extern uint64_t st_eflags;
extern uint64_t st_r8;
extern uint64_t st_r9;
extern uint64_t st_r10;
extern uint64_t st_r11;
extern uint64_t st_r12;
extern uint64_t st_r13;
extern uint64_t st_r14;
extern uint64_t st_r15;
extern uint16_t st_cs;
extern uint16_t st_ds;
extern uint16_t st_es;
extern uint16_t st_fs;
extern uint16_t st_gs;
extern uint16_t st_ss;
#endif

typedef uint16_t threadID_t;
typedef uint16_t processID_t;

typedef struct threadData_t
{
	size_t esp;
	size_t eip;		//only used the first time
	size_t eflags;  //only used the first time

	char cwd[256];
	bool cwdCorrect;

	volatile bool currentlyExecuting;	///<used for SMP timings
	
	bool halt;
	bool loadedBefore;

	int returnValue;

	uint64_t waitingForMillisecSinceBoot;

	int wait;								//waiting on process

	size_t kernelSwitchStack;		//to be loaded into TSS on task switch, and just before int handlers end
									//as at the start of an int handler it switches to the default kernel stack
									//stores if a task switch happened in kernel mode. also used for kernel tasks
	
	//SSE requires 512, but we'll allocate more for other things (e.g. AVX)
	//SSE requies 16 bit alignment, but we'll align on 64 just in case other things need it
	//AVX requires 1024 * 8, not sure about AVX-512
	uint8_t floatingStorage[8092]  __attribute__ ((aligned (64)));

} threadData_t;

typedef struct processData_t
{
	int forkReturnVal;

	int initialStack;
	size_t highestUnallocedAddress;
	size_t highestUnallocedSharedAddress;

	uint8_t numberOfThreads;
	threadData_t threads[2];

	threadID_t currentlyExecutingThread;
	
	bool processHalt;
	bool systask;

	char arguments[64][256];
	int numberOfArguments;

	bool driver;
	struct Device* driverDevice;

	uint8_t* smallIconData;
	uint8_t* largeIconData;

	processID_t childProcesses[16];
	int childrenCount;

	processID_t parent;

	//these are created if needed, otherwise inherited from the parent process
	InternalObjectID stdin;	
	InternalObjectID stdout;
	InternalObjectID stderr;

	bool console;

	char filename[256];
	char* userReadableName;

	size_t* addressSpace;

} processData_t;

extern processData_t* TASKS;

void internalExecve (char* path, char** args, char** envr);