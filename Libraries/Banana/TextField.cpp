#include <Banana/TextField.hpp>
#include <Banana/Core.hpp>
#define CHECK_BIT(var,pos) ((var) & (1<<(pos)))

bool TextField::autoLooseFocusOnMouseClick ()
{
	return true;
}

TextField::TextField (Window& parent, int _x, int _y, int _width) : Widget (&parent) {
    masterwindow = &parent;
	masterwindow->addWidgetChild (this);
    
    strcpy(textfieldfont, "SYSTEM");

    for (int i = 0; i < 10000; ++i) {
        text[i] = 0;
    }
    
    cursorposition = 0;
    endcurposition = 0;
    textfieldwidth = _width;
    focus = false;
    x = _x;
    y = _y;
	editable = true;
    _write();
}
	
TextField::~TextField () {
    free((void*)text);
    free((void*)textfieldfont);
}

void TextField::_write() {
    forceWindowToUpdate ();
    uint16_t pixelPointer = x;

    for (int i = 0; i < textfieldwidth - 1; ++i) {
        masterwindow->drawCharacter(' ', pixelPointer, y, 0xFFFFFF, 0, 0);
        masterwindow->drawCharacter(' ', pixelPointer, y + 8, 0xFFFFFF, 0, 0);
        pixelPointer += 4;
    }
    
    pixelPointer = x;
    for (int i = 0; i < textfieldwidth * 4; ++i) {
        masterwindow->drawPixel(pixelPointer, y, 0);
        masterwindow->drawPixel(pixelPointer, y + 1, 0x808080);
        
        masterwindow->drawPixel(pixelPointer, y + 20, 0xE0E0E0);
        masterwindow->drawPixel(pixelPointer, y + 21, 0xFFFFFF);
        ++pixelPointer;
    }
    
    for (int i = 0; i < 22; ++i) {
        masterwindow->drawPixel(x + textfieldwidth * 4 - 1, y + i, 0xFFFFFF);
        if (i < 21) {
            masterwindow->drawPixel(x, y + i, 0);
        }
        if (i && i < 20) {
            masterwindow->drawPixel(x + 1, y + i, 0x808080);
        }
        if (i && i != 21) {
            masterwindow->drawPixel(x + textfieldwidth * 4 - 2, y + i, 0xE0E0E0);
        }
    }
    
    pixelPointer = x + 4;
    uint8_t fontno = SystemCall(SC_GetFontNumberFromName, 0, 0, (void*) textfieldfont);
    
    bool between = false;
	bool didCursor = false;
    for (int i = 0; text[i]; ++i) {
        uint8_t width77 = APISystemCall(ASCGetCharacterWidth, text[i], fontno, 0);
        if (i == cursorposition && focus) {
			masterwindow->drawCharacter ('|', pixelPointer, y + 4, 0xFFFFFF, 0, 0);
			pixelPointer += 2;
            
            between ^= 1;
			didCursor = true;
        }
		if (i == endcurposition && focus) {
			between ^= 1;
		}
		masterwindow->drawCharacter (text[i], pixelPointer, y + 4, between ? 0x5462FF : 0xFFFFFF, editable ? 0 : 0x808080, fontno);
		pixelPointer += width77;
    }
	if (!didCursor) {
		masterwindow->drawCharacter ('|', pixelPointer, y + 4, 0xFFFFFF, 0, 0);
	}
}

void TextField::_clear() {
    // I'm going to leave this one blank
}

// I KNOW I SHOULDN'T USE GLOBAL VARIABLES TO DEAL WITH CLASSES, HOWEVER THIS ONE
// IS JUST THE MOST RECENT LOCATION OF THE MOUSE, AND CAN BE SHARED BETWEEN ALL TEXTFIELDS
int lastmousex = 0;
int lastmousey = 0;

void TextField::selfupdate (Event_t event) {
    int cx = SystemCall(SC_WindowPositionX, getWindowNumber(), 0, 0);
    int cy = SystemCall(SC_WindowPositionY, getWindowNumber(), 0, 0);
    
    int xpixin = 0;        //must be an int as it gets negative later
    bool useTextMouse = false;
    int xx = event.mouseXPosition - cx;
    int yy = event.mouseYPosition - cy;
    
	if (event.type == EventMouseClick || event.type == EventMouseUp || event.type == EventMouseMove) {
        lastmousex = event.mouseXPosition;
        lastmousey = event.mouseYPosition;
    }
    
	if (event.type == EventMouseClick || event.type == EventMouseUp) {
        if (yy >= y && yy <= y + 14 && xx >= x - 1 && xx <= x + textfieldwidth * 4) {
            focus = true;
            xpixin = SystemCall(SC_GetRealMouseX, 0, 0, 0);
            xpixin -= x;
            xpixin -= cx;
            xpixin -= 4;            //text starts 4 pixels into the text field

			if (xx >= x) {
				useTextMouse = true;
			}
        }
    }

    if ((lastmousey - cy) >= y && (lastmousey - cy) <= y + 14 && (lastmousex - cx) >= x && (lastmousex - cx) <= x + textfieldwidth * 4) {
        useTextMouse = true;
    }
    
	if (useTextMouse) {
		masterwindow->setCursorType(Mouse_Text);
	}

	if (focus) {
		focusedLastTime = true;
	} else if (!focus && focusedLastTime) {
		focusedLastTime = false;
		_write ();
	} else {
		focusedLastTime = false;
	}

	//0 = event!                (IMPLEMENTED)
	//1 = mouse moved           (IMPLEMENTED)
	//2 = keyboard              (IMPLEMENTED)
	//3 = mouse clicked         (IMPLEMENTED)
	//4 = right mouse clicked
	//5 = mouse release         (IMPLEMENTED)
	//6 = timer
	//7 = currently unused
	//8-15 = key
	//16-31 = mouse x
	//32-47 = mouse y

	if (event.type == EventKeyDown && focus && editable) {
        if (event.keyPressed == KEYCODE_BACKSPACE) {                    //backspace
            if (cursorposition > 0) {
                if (cursorposition != endcurposition) {
                    int lower = cursorposition > endcurposition ? endcurposition : cursorposition;
                    int heigher = cursorposition < endcurposition ? endcurposition : cursorposition;
                    int number = heigher - lower;
                    while (number--) {
                         memcpy(text - 1 + heigher, text + heigher, strlen(text + heigher) + 1);
                        --heigher;
                    }
                    
                } else {
                    memcpy(text - 1 + cursorposition, text + cursorposition, strlen(text + cursorposition) + 1);
                    //text[cursorposition] = 0;
                    --cursorposition;
                }
            }

        } else if (event.keyPressed == KEYCODE_LEFT) {             //left
            if (cursorposition > 0) {
                --cursorposition;
            }

        } else if (event.keyPressed == KEYCODE_RIGHT) {             //right
            if (cursorposition < strlen(text)) {
                ++cursorposition;
            }

        } else if (isprint(event.keyPressed)) {
            if (cursorposition != endcurposition) {
                int lower = cursorposition > endcurposition ? endcurposition : cursorposition;
                int heigher = cursorposition < endcurposition ? endcurposition : cursorposition;
                int number = heigher - lower;
                while (number--) {
                     memcpy(text - 1 + heigher, text + heigher, strlen(text + heigher) + 1);
                    --heigher;
                }
            }
            
			char* halfbuffer = (char*) malloc (strlen (text) * 4 + 512);
			memset (halfbuffer, 0, strlen (text) * 4 + 512);
            int no = strlen(text + cursorposition) + 1;
            memcpy(halfbuffer, text + cursorposition, no);
            memcpy(text + cursorposition + 1, halfbuffer, no);
            text[cursorposition++] = event.keyPressed;
			free (halfbuffer);
		} else {
			return;		//don't re-render unless a graphical/arrow/backspace/newline key was pressed
		}
        endcurposition = cursorposition;
        _write();
        
    } else if (event.type == EventMouseClick && focus) {
        uint8_t fontno = SystemCall(SC_GetFontNumberFromName, 0, 0, (void*) textfieldfont);
        int tmp = 0; 
        
        for (int i = 0; text[i]; ++i) {
            xpixin -= APISystemCall(ASCGetCharacterWidth, text[i], fontno, 0);
            if (xpixin < 0 - APISystemCall (ASCGetCharacterWidth, text[i], fontno, 0)) {        // if xpixin is less than 0, it means we have found the correct spot to put the cursor
                break;
            }
            if (tmp >= strlen(text)) {
                tmp = strlen(text);
                break;
            }
            ++tmp;
        }
        
        cursorposition = tmp;
        endcurposition = tmp;

		if (xx == x - 1) {
			cursorposition = 0;
			endcurposition = 0;
		}

        _write();
        
    } else if (event.type == EventMouseUp && focus) {
        uint8_t fontno = SystemCall(SC_GetFontNumberFromName, 0, 0, (void*) textfieldfont);
        int tmp = 0; 
        
        for (int i = 0; text[i]; ++i) {
            xpixin -= APISystemCall(ASCGetCharacterWidth, text[i], fontno, 0);
            if (xpixin < 0 - APISystemCall (ASCGetCharacterWidth, text[i], fontno, 0)) {
                break;
            }
            if (tmp >= strlen(text)) {
                break;
            }
            ++tmp;
        }
        
        endcurposition = tmp;

		if (xx == x - 1) {
			endcurposition = 0;
		}

        _write();
        
    } else if (event.type == EventKeyUp && focus) {
        char character = event.keyPressed;
        char selectedData[2048] = {0};
        int selIndex = 0;
        char* pasteInfo;
        
        switch (character) {
            case 'a':
                cursorposition = 0;
                endcurposition = strlen(text);
                _write();
                break;
                
            case 'c': 
                for (selIndex = cursorposition; selIndex < endcurposition; ++selIndex) {
                    selectedData[selIndex - cursorposition] = text[selIndex];
                }
                SystemCall (SC_Copy, 0, 0, selectedData);
                break;

			case 'x':
			{
				if (!editable) break;
				for (selIndex = cursorposition; selIndex < endcurposition; ++selIndex) {
					selectedData[selIndex - cursorposition] = text[selIndex];
				}
				SystemCall (SC_Copy, 0, 0, selectedData);

				int lower = cursorposition > endcurposition ? endcurposition : cursorposition;
				int heigher = cursorposition < endcurposition ? endcurposition : cursorposition;
				int number = heigher - lower;
				while (number--) {
					memcpy (text - 1 + heigher, text + heigher, strlen (text + heigher) + 1);
					--heigher;
				}

				break;
			}
                
            case 'v':
				if (!editable) break;
                pasteInfo = (char*) SystemCall (SC_Paste, 0, 0, 0);
                memcpy(text + cursorposition + strlen(pasteInfo), text + cursorposition, strlen(pasteInfo));
                memcpy(text + cursorposition, pasteInfo, strlen(pasteInfo));
                cursorposition += strlen(pasteInfo);
                endcurposition = cursorposition;
                _write();
                break;
                
            default:
                break;
        }
    }
}
	
void TextField::set (char* c) {
    for (int i = 0; i < 2048; ++i) {
        text[i] = 0;
    }
    strcpy(text, c);
    cursorposition = 0;
    endcurposition = 0;
    _write();
}

char* TextField::get () {
	char* ntext = (char*) malloc (512);
	strcpy (ntext, text);
    return ntext;
}