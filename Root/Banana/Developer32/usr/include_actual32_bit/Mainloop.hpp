//
//  Mainloop.hpp
//  Banana API (New)
//
//  Created by Alex Boxall on 21/10/18.
//  Copyright © 2018 Alex Boxall. All rights reserved.
//

#ifndef Mainloop_hpp
#define Mainloop_hpp

#include "Responder.hpp"
#include "Window.hpp"

/*! \file Mainloop.hpp
 \brief A header file defining the Mainloop class.
 */

/*!
 \class Mainloop
 \brief A class which handles recieving events from the kernel and passing them to windows.
 
 The Mainloop class is a class which handles recieving events from the kernel and passing them to windows.
 The windows then handle the events and pass them down the event chain. Each application should create
 one, global Mainloop object.
 */

class Mainloop {
private:
    const static int maxWindows = 32;

    Window* windows[maxWindows];
    int windowPtr;
    
protected:
    friend Window;
    void registerWindow(Window* w);
    
public:
    Mainloop();
    void begin();
};

#endif /* Mainloop_hpp */
