path = ''

while True:
    path = input('Enter path name: ').strip().upper()
    toadd = 126 - len(path)
    path += chr(0) * toadd

    type_ = '*'
    while type_ != 'S' and type_ != 'I' and type_ != 'B':
        type_ = input('String or Int? S/I: ').upper()
        
    path += type_
    if type_ == 'S':
        path += input('Enter string: ')
    elif type_ == 'I':
        i = int(input('Enter integer: '))
        path += chr(i % 0xFF)
        path += chr((i >> 8) % 0xFF)
        path += chr((i >> 16) % 0xFF)
        path += chr((i >> 24) % 0xFF)
        
    toadd = 256 - len(path)
    path += chr(0) * toadd
	
    w = input('USER.DAT? Y/n: ')
    
    if w.upper() == 'Y':
        open('../Userdata/Alex/user.dat', 'ab').write(path.encode('latin-1'))
    else:
        open('system.dat', 'ab').write(path.encode('latin-1'))
    
    print('')
