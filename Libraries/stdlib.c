#include <stdlib.h>
#include <string.h>
#include <ctype.h>

uint64_t next = 1;

unsigned long int strtoul (const char* c, char** endptr, int base)
{
	char x[] = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	unsigned long int o = 0;

	for (int i = 0; c[i]; ++i) {
		o *= base;
		for (int a = 0; a < strlen (x); ++a) {
			if (c[i] == x[a]) {
				o += a;
			}
		}
	}
	return o;
}

/*-

THE 'STRTOL' FUNCTION IS FROM THE FOLLOWING CODE UNDER THE FOLLOWING LICENSE
LOCATED AT https://github.com/gcc-mirror/gcc/blob/master/libiberty/strtol.c


THIS LICENSE APPLIES TO THE 'STRTOL' FUNCTION ONLY!


* Copyright (c) 1990 The Regents of the University of California.
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions
* are met:
* 1. Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
* 2. Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the distribution.
* 3. [rescinded 22 July 1999]
* 4. Neither the name of the University nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
* ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
* FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
* DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
* OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
* HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
* LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
* OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
* SUCH DAMAGE.
*/

long int strtol (const char *nptr, char **endptr, int base)
{
	const char *s = nptr;
	unsigned long acc;
	int c;
	unsigned long cutoff;
	int neg = 0, any, cutlim;

	do {
		c = *s++;

	} while (isspace (c));

	if (c == '-') {
		neg = 1;
		c = *s++;

	} else if (c == '+') {
		c = *s++;
	}

	if ((!base || base == 16) && c == '0' && (*s == 'x' || *s == 'X')) {
		c = s[1];
		s += 2;
		base = 16;
	}

	if (!base) {
		base = c == '0' ? 8 : 10;
	}

	cutoff = neg ? -(unsigned long) LONG_MIN : LONG_MAX;
	cutlim = cutoff % (unsigned long) base;
	cutoff /= (unsigned long) base;

	for (acc = 0, any = 0;; c = *s++) {

		if (isdigit (c)) {
			c -= '0';
		} else if (isalpha (c)) {
			c -= isupper (c) ? 'A' - 10 : 'a' - 10;
		} else {
			break;
		}

		if (c >= base) {
			break;
		}

		if (any < 0 || acc > cutoff || (acc == cutoff && c > cutlim)) {
			any = -1;
		} else {
			any = 1;
			acc *= base;
			acc += c;
		}
	}

	if (any < 0) {
		acc = neg ? LONG_MIN : LONG_MAX;

	} else if (neg) {
		acc = -acc;
	}

	if (endptr != 0) {
		*endptr = (char *) (any ? s - 1 : nptr);
	}

	return (acc);
}

long int atol (const char* c)
{
	return strtol (c, 0, 10);
}

int atoi (const char* c)
{
	int o = 0;
	bool negative = false;

	for (int i = 0; c[i]; ++i) {
		if (c[i] == ' ') {
			continue;
		}
		if (c[i] == '-') {
			negative = true;
			continue;
		}
		o *= 10;
		if (isdigit (c[i])) {
			o += c[i] - '0';
		}
	}

	return (negative) ? -o : o;
}

int abs (int x)
{
	return (x < 0) ? 0 - x : x;
}

long labs (long x)
{
	return (x < 0) ? 0 - x : x;
}

int rand ()
{
	static bool firstTime = false;
	if (firstTime) {
		next = SystemCall (SC_GetTime, 0, 0, 0);
		firstTime = false;
	}
	return (int) ((next = (164603309694725029ull * next) % 14738995463583502973ull));
}

void srand (int seed)
{
	next = seed;
}

void abort ()
{
	SystemCall (SC_Abort, -1, 0, 0);
	while (1);        //the program will still run until the next clock tick
}

void exit (int code)
{
	SystemCall (SC_Abort, code, 0, 0);
	while (1);
}

int system (const char* arg)
{
	char** commands = malloc (256 * sizeof (char*));
	char* current = malloc (600);
	memset (current, 0, 600);
	long cp = 0;
	long cmdcount = 0;

	bool quotes = false;

	uint8_t commandTypes[256] = { 0 };			// 0/1 = normal, 2 = &&, 3 = ||, 4 = |
	memset (commandTypes, 0, 256);

	SystemCall (SC_Log, 0, 0, (void*) arg);

	for (int i = 0; arg[i]; ++i) {
		if (arg[i] == '"' &&
			(arg[i - 1] != '\\' ||
			(arg[i - 1] == '\\' && arg[i - 2] == '\\'))) {
			quotes ^= 1;
		}

		if (arg[i] == ';' && !quotes) {
			if (cp) {
				commandTypes[cmdcount] = 1;
				current[cp++] = 0;
				commands[cmdcount++] = current;
				current = malloc (600);
				memset (current, 0, 600);
				cp = 0;
			}
			continue;

		} else if (((arg[i] == '&' && arg[i + 1] == '&') || (arg[i] == '|' && arg[i + 1] == '|')) && !quotes) {
			++i;
			commandTypes[cmdcount] = arg[i] == '&' ? 2 : 3;
			if (cp) {
				current[cp++] = 0;
				commands[cmdcount++] = current;
				current = malloc (1024);
				memset (current, 0, 1024);
				cp = 0;
			}
			continue;
		} else if (arg[i] == '|' && !quotes) {
			if (cp) {
				commandTypes[cmdcount] = 4;
				current[cp++] = 0;
				commands[cmdcount++] = current;
				current = malloc (600);
				memset (current, 0, 600);
				cp = 0;
			}
			continue;
		}

		current[cp++] = arg[i];
	}
	current[cp++] = 0;
	commands[cmdcount++] = current;

	int ret = 0;

	// loop through all of the commands on the line

	int pipeNextTime = 0;

	/*bool pipelineOn;
	char* heldStdout;
	int heldStdoutPointer;

	int readPipelineStdinFrom;
	int readPipelineStdinPointer;*/

	for (int i = 0; i < cmdcount; ++i) {
		if (pipeNextTime) {			//the end of a pipe
			ret = SystemCall (SC_System, 881, pipeNextTime, commands[i]);		//starts redirction of STDIN
			pipeNextTime = 0;
			yield ();

		} else {
			if (!pipeNextTime && commandTypes[i] == 4) {			//the start of a pipe
				SystemCall (SC_System, 999, 0, commands[i]);			//starts redirction of STDOUT
				pipeNextTime = SystemCall (SC_Yield, 12345, 54321, 0);

			} else {
				ret = SystemCall (SC_System, 0, 0, commands[i]);
			}
		}

		//allow effects to take place
		yield ();

		if (!ret && commandTypes[i] == 2) {			//the previous mustn't be false
			return ret;
		}

		if (ret && commandTypes[i] == 3) {			//the previous must be false
			return ret;
		}
	}

	for (int i = 0; i < cmdcount; ++i) {
		free (commands[i]);
	}
	free (commands);
	return ret;
}

uint64_t datetimeToSeconds (datetime_t d)
{
	uint8_t day = d.day;
	uint8_t month = d.month;
	uint16_t year = d.year;
	uint8_t hours = d.hour;
	uint8_t minutes = d.minute;
	uint8_t seconds = d.second;
	uint64_t output = 0;
	if (((year % 4 == 0) && !(year % 100 == 0)) || year % 400 == 0) {
		output = 1;     //give Feburary an extra day for this year
	}
	uint16_t tempYears = year;
	uint16_t leapYears = 0;
	while (tempYears > 1601) {
		if (((tempYears % 4 == 0) && !(tempYears % 100 == 0)) || tempYears % 400 == 0) {
			++leapYears;
		}
		--tempYears;
	}
	output += (year - 1601) * 365 + leapYears;

	switch (month - 1) {
	case 12:
		output += 31;
	case 11:
		output += 30;
	case 10:
		output += 31;
	case 9:
		output += 30;
	case 8:
		output += 31;
	case 7:
		output += 31;
	case 6:
		output += 30;
	case 5:
		output += 31;
	case 4:
		output += 30;
	case 3:
		output += 31;
	case 2:
		output += 28;
	case 1:
		output += 31;
	default:
		break;
	}

	output += day - 1;
	output *= 3600 * 24;
	output += seconds;
	output += minutes * 60;
	output += hours * 3600;

	return output;
}

datetime_t secondsToDatetime (uint64_t data)
{
	uint8_t seconds = 0;
	uint8_t minutes = 0;
	uint8_t hours = 0;
	uint8_t day = 0;
	uint8_t month = 1;
	uint16_t year = 1601;

	seconds = data % 60;
	minutes = data / 60 % 60;
	hours = data / 60 / 60 % 24;
	data = data / 60 / 60 / 24;

	while (data > 365) {
		data -= 365;
		if (((year % 4 == 0) && !(year % 100 == 0)) || year % 400 == 0) {
			--data;
		}
		++year;
	}
	if (data == 365) {
		++year;
		data = 0;
	}
	if (((year % 4 == 0) && !(year % 100 == 0)) || year % 400 == 0) {
		data -= 2;
	}

	int j[12] = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
	while (data > 31) {
		data -= j[month++];
	}

	day = data + 1;

	datetime_t d;
	d.second = seconds;
	d.minute = minutes;
	d.hour = hours;
	d.day = day - 1;
	d.month = month;
	d.year = year;
	return d;
}

datetime_t currentTime ()
{
	uint64_t* dd = (uint64_t*) ((size_t) SystemCall (SC_GetTime, 0, 0, 0));
	return secondsToDatetime (*dd);
}

/*uint64_t freeMemoryBase;
bool firstMalloc = true;

#define BLOCK_SIZE 64
#define MEMORY_BASE 0x01200000			//that 0x1300000 interrupt handler rubbish has been replaced but
#define MEMORY_HIGH 0x01400000			//we're allowing it to go out of bounds in case of future expansion
//so we'll do some code checks to ensure we don't do out. I would like to assign
//these values dynamically, but we assign an array based on these numbers

// WE SHOULD CONSIDER MOVING MALLOC BACK INTO SYSTEM CALL FORM, SO THAT WE DON'T NEED TO WORRY
// ABOUT LARGE, DUPLICATED 'BLOCK BUFFERS' AND IT WILL GIVE PROGRAMS A LOT MORE MEMORY TO USE
// (2MB -> up to 1GB)

#define MEMORY (MEMORY_HIGH - MEMORY_BASE)
#define BLOCKS (MEMORY / BLOCK_SIZE)

char blocksUsed[BLOCKS] = { 0 };
int tried = 0;
uint8_t* lastFreeAddress = MEMORY_BASE;
uint8_t* lastMallocAddress = MEMORY_BASE;
bool firstTime = true;*/

void* malloc (size_t size)
{
	int syscall = SystemCall (SC_Malloc, size, 0, 0);
	void* ptr = (void*) ((size_t) syscall);
	//printf ("malloc gives %d (raw = %d) (input size = %d)\n", ptr, syscall, size);
	return ptr;
	/*
	if (firstTime) {
		firstTime = false;
		memset (blocksUsed, 0, BLOCKS);
	}
	SystemCall (SC_Log, 0, 0, "MALLOC CALLED");
	tried = 0;
	int stage = 0;
	uint8_t* ptr = lastFreeAddress;
	if (ptr < SystemCall (SC_ProgramStackAreaBase, 0, 0, 0)) {
		ptr = SystemCall (SC_ProgramStackAreaBase, 0, 0, 0);
	}
	unsigned int blocksAlloced = 0;
	while (blocksAlloced < size) {
		if (blocksUsed[(((unsigned int) ptr + blocksAlloced) - MEMORY_BASE) / BLOCK_SIZE] == 0) {
			blocksAlloced += BLOCK_SIZE;
		} else {
			ptr += blocksAlloced + BLOCK_SIZE;

			if (stage == 0) {
				stage = 1;
				ptr = lastMallocAddress;
			}

			if (ptr < SystemCall (SC_ProgramStackAreaBase, 0, 0, 0)) {
				ptr = SystemCall (SC_ProgramStackAreaBase, 0, 0, 0);
			}

			blocksAlloced = 0;
		}
	}
	for (int i = 0; i < blocksAlloced / BLOCK_SIZE; ++i) {
		if (i == blocksAlloced / BLOCK_SIZE - 1) {
			blocksUsed[((((unsigned int) ptr) - MEMORY_BASE) / BLOCK_SIZE) + i] = 2;		//show that this is the last in the block
		} else {
			blocksUsed[((((unsigned int) ptr) - MEMORY_BASE) / BLOCK_SIZE) + i] = 1;
		}
	}
	lastMallocAddress = ptr + blocksAlloced;
	memset (ptr, 0, blocksAlloced * BLOCK_SIZE);	//why not?

	char b[256];
	sprintf (b, "ALLOCATING %d BYTES AT ADDRESS %d", blocksAlloced, ptr);
	SystemCall (SC_Log, 0, 0, b);
	SystemCall (SC_Log, 0, 0, "DONE!");
	return ptr;*/
}

void free (void *blk)
{
	SystemCall (SC_Free, 0, 0, blk);
	// SystemCall (SC_Log, 0, 0, "PURPOSELY NOT FREEING MEMORY (JUST TESTING THINGS)");
	return;

	/*unsigned int locator = blk;
	while (blocksUsed[((locator - MEMORY_BASE) / BLOCK_SIZE)]) {
		if (blocksUsed[((locator - MEMORY_BASE) / BLOCK_SIZE)] == 2) {
			blocksUsed[((locator - MEMORY_BASE) / BLOCK_SIZE)] = 0;
			break;
		}
		blocksUsed[((locator - MEMORY_BASE) / BLOCK_SIZE)] = 0;
		locator += BLOCK_SIZE;
	}
	lastFreeAddress = blk;
	SystemCall (SC_Log, 0, 0, "FREED MEMORY");*/
}

void* realloc (void *ptr, size_t size)
{
	return (void*) ((size_t) SystemCall (SC_Malloc, size, 0xCAFE, ptr));
	/*SystemCall (SC_Log, 0, 0, "REALLOC CALLED!!!!!");
	unsigned int locator = ptr;
	unsigned int bytes = 0;
	while (blocksUsed[((locator - MEMORY_BASE) / BLOCK_SIZE)]) {
		if (blocksUsed[((locator - MEMORY_BASE) / BLOCK_SIZE)] == 2) {
			break;
		}
		blocksUsed[((locator - MEMORY_BASE) / BLOCK_SIZE)] = 0;
		locator += BLOCK_SIZE;
		bytes += BLOCK_SIZE;
	}

	if (size <= bytes) {
		return ptr;
	} else {
		void* a = malloc (size);
		if (!a) {
			return 0;
		}
		memcpy (a, ptr, bytes);
		free (ptr);
		return a;
	}
	return 0;*/
}

void* calloc (size_t nitems, size_t size)
{
	void* ptr = malloc (nitems * size);
	//printf ("calloc gives %d\n", ptr);
	memset (ptr, 0, nitems * size);
	//printf ("then it gives %d\n", ptr);
	return ptr;
}

char* getenv (const char* name)
{
	return (char*) ((size_t) SystemCall (SC_GetEnv, 0, 0, (void*) name));
}

void swap (void *e1, void *e2, int size)
{
	void* sp = (void*) malloc (size);

	memcpy (sp, e1, size);
	memcpy (e1, e2, size);
	memcpy (e2, sp, size);
	free (sp);
}

void qsort (void* bss, size_t n, size_t size, int (*cmp)(const void*, const void*))
{
	uint8_t* base = (uint8_t*) bss;
	size_t i;
	size_t rv;
	uint8_t* mp;
	uint8_t* sp;  //for swap

	if (n <= 1) {
		return;
	}

	mp = base;

	for (i = 1; i < n; ++i) {
		rv = cmp (base, base + size*i);

		if (rv > 0) {
			mp += size;

			if (mp != base + size*i) {
				swap (base + size*i, mp, size);
			}
		}
	}

	swap (base, mp, size);
	qsort (base, (mp - base) / size, size, cmp);
	qsort (mp + size, n - 1 - (mp - base) / size, size, cmp);
}

double atod (const char* s)
{
	double rez = 0, fact = 1;
	if (*s == '-') {
		s++;
		fact = -1;
	}
	for (int point_seen = 0; *s; s++) {
		if (*s == '.') {
			point_seen = 1;
			continue;
		}
		int d = *s - '0';
		if (d >= 0 && d <= 9) {
			if (point_seen) fact /= 10.0f;
			rez = rez * 10.0f + (double) d;
		}
	}
	return rez * fact;
}

float atof (const char* s)
{
	float rez = 0, fact = 1;
	if (*s == '-') {
		s++;
		fact = -1;
	}
	for (int point_seen = 0; *s; s++) {
		if (*s == '.') {
			point_seen = 1;
			continue;
		}
		int d = *s - '0';
		if (d >= 0 && d <= 9) {
			if (point_seen) fact /= 10.0f;
			rez = rez * 10.0f + (float) d;
		}
	}
	return rez * fact;
}

div_t div (int num, int denom)
{
	div_t a;
	a.quot = num / denom;
	a.rem = num % denom;
	return a;
}

ldiv_t ldiv (long num, long denom)
{
	ldiv_t a;
	a.quot = num / denom;
	a.rem = num % denom;
	return a;
}


regs_t executeDriver (char* drivername, drivermode mode, regs_t args)
{
	// ebx - mode
	// ecx - args
	// edx - name

	// gives us a pointer to the return values

	regs_t* whereToFindIt = (regs_t*) SystemCall (SC_ExecuteDriver, mode, (uint32_t) ((size_t) &args), drivername);

	yield ();		//allow us to be put on hold

	if (whereToFindIt) {
		return *whereToFindIt;
	}

	regs_t bad;
	bad.reg_a = -1;
	bad.reg_b = -1;
	bad.reg_c = -1;
	bad.reg_d = -1;
	bad.reg_e = -1;
	bad.reg_f = -1;
	bad.reg_g = -1;
	bad.reg_h = -1;
	return bad;
}