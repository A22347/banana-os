#pragma once

#include <core/prcssthr.h>

void findNextTask ();
void abortTask (int returnValue);
void nanoSleepTaskUntil (uint64_t nanoSinceBoot);
void nanoSleepTask (uint64_t nano);
void sleepTask (uint64_t seconds);
void addDriverToList (int pid);
void removeDriverFromList (int pid);

extern int driverPIDs[256];
extern int driverCurrent;

void storeFork();
void storeExecve(char* path, char** args, char** envr);
void storeAddTask(char* path);

extern int spawnResult;