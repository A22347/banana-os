Please read the following license agreement carefully and do not skip anything. Press the PAGE DOWN or DOWN ARROW keys to scroll the document down so you can see the rest of the document. Press the PAGE UP or UP ARROW keys to scroll back up.

---

BANANA OPERATING SYSTEM AND RELATED COMPONENTS
END USER LICENSE AGREEMENT

PLEASE READ THE FOLLOWING LICENSE CAREFULLY BEFORE USING THIS SOFTWARE.
BY USING THIS SOFTWARE, YOU AGREE TO BE BOUND BY THE TERMS OF THIS LICENSE.
 
This End User License Agreement ("EULA") is a legal agreement between (a) you (either as an individual or a single entity) and (b) Alex Boxall ("AB"), which governs the use the Banana Operating System and any related software components, installation media and the data recorded on it (excluding the installation program currently being used, but including any related electronic files that it may or may not install), documentation and electronic files (the "Software"). The Software is licensed, not sold to you by AB for use only under the terms of this EULA. AB reserves all rights not explicitly granted to you by this EULA. 

BY PRESSING TYPING "I AGREE" (WITHOUT QUOTES) AND PRESSING RETURN TO AGREE TO THIS LICENSE, OR BY INSTALLING, COPYING, OR OTHERWISE USING THE THE SOFTWARE YOU AGREE TO BE BOUND BY THE THE TERMS OF THIS EULA. IF YOU DO NOT AGREE TO THE TERMS OF THIS EULA, YOU MAY NOT USE OR INSTALL THE SOFTWARE. YOU MAY, HOWEVER, RETURN THE SOFTWARE TO YOUR PLACE OF PURCHASE FOR A FULL REFUND.

1. LIMITED LICENSE. This EULA grants you a revocable, non-exclusive, limited license to use the Software as follows: You may install, use, access and display one copy of the Software on a single computer or device. To "use" this software means that the software is loaded into temporary memory (i.e., RAM) of a computer or installed on the permanent memory of a computer (i.e., hard disk, etc.). You may make one backup copy of the Software for backup and archival purposes only. The Software is licensed, not sold.

2. RESTRICTIONS ON USE. You must not remove or alter any of the copyright notices on all copies of the Software. You may not distribute copies of the Software to third parties. You may not sell or copy this software. This EULA does not grant any rights in connection with any trademarks or service marks of AB. You may not reverse engineer, decompile, or disassemble the Software, except and only to the extent that such activity is expressly permitted by applicable law notwithstanding this limitation. You may not rent, lease, or lend the Software. You may permanently transfer all of your rights under this EULA, provided the recipient agrees to the terms of this EULA. You must comply with all applicable laws regarding use of the Software. 

3. TERMINATION. This license is perpetual or until you fail to comply with the terms and conditions of this EULA or AB terminates it with or without cause. In such event, you must destroy all copies of the Software.
 
4. COPYRIGHT. All title, including but not limited to copyrights, in and to the Software and any copies thereof are owned by AB. All title and intellectual property rights in and to the content which may be accessed through use of the Software is the property of the respective content owner and may be protected by applicable copyright or other intellectual property laws and treaties. This EULA grants you no rights to use such content. All rights not expressly granted are reserved by AB.

5. NO WARRANTIES. THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

6. LIMITATION OF LIABILITY. TO THE MAXIMUM EXTENT PERMITTED BY APPLICABLE LAW, IN NO EVENT SHALL AB OR ITS SUPPLIERS BE LIABLE FOR ANY SPECIAL, INCIDENTAL, INDIRECT, OR CONSEQUENTIAL DAMAGES WHATSOEVER (INCLUDING, WITHOUT LIMITATION, DAMAGES FOR LOSS OF BUSINESS PROFITS, BUSINESS INTERRUPTION, LOSS OF BUSINESS INFORMATION, OR ANY OTHER PECUNIARY LOSS) ARISING OUT OF THE USE OF OR INABILITY TO USE THE SOFTWARE PRODUCT OR THE FAILURE TO PROVIDE SUPPORT SERVICES, EVEN IF AB HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES. IN ANY CASE, AB’S ENTIRE LIABILITY UNDER ANY PROVISION OF THIS EULA SHALL BE LIMITED TO THE THE AMOUNT ACTUALLY PAID BY YOU FOR THE SOFTWARE PRODUCT. BECAUSE SOME STATES/JURISDICTIONS DO NOT ALLOW THE EXCLUSION OR LIMITATION OF LIABILITY, THE ABOVE LIMITATION MAY NOT APPLY TO YOU.

7. EP0618540 PATENT. The Software infringes on patent EP0618540, which expires on the December 13, 2021. The Software may not be used in Europe while the patent is valid. Blame Microsoft.

8. If any section of this agreement is found to be unenforceable the remaining sections still apply.
---

(THIS ONLY APPEARS AFTER SCROLLING ALL THE WAY DOWN)
If you accept the terms of the license, agree to it in the way the license says. If you do not agree to it, press ESC and setup will close. To install Banana, you must accept the agreement.