//
//  Button.hpp
//  GUI
//
//  Created by Alex Boxall on 23/7/19.
//  Copyright © 2019 Alex Boxall. All rights reserved.
//

#ifndef Button_hpp
#define Button_hpp

#include "Font.hpp"
#include "Control.hpp"
#include "Label.hpp"

class Button;
void dwncmd (Button* self);
void upcmd (Button* self);

class Button : public Control {
private:
protected:
    friend void dwncmd (Button* self);
    friend void upcmd (Button* self);

    char* text;
    Font* font;
    bool enabled;
    void (*command)(Button* self);
    bool held;
    bool defaultButton;
    Label* label;

public:
    virtual void drawPixel (int xx, int yy, uint32_t col);
    
    void setEnable(bool enable);
    bool getEnable();
    
    
    void setDefault(bool defaultB);
    bool getDefault();
    
    const char* debugName();
    virtual Button* copy();
    
    void setFont(Font* fnt);
    Font* getFont();
    
    char* getText();
    bool canHoldFocus();
    virtual void internalDraw();

    void setText(const char* text);
    void setCommand (void (*cmd)(Button* self));
    
    virtual void keyboard (KeyState k);
    
    Button(Control* parent, int x, int y, const char* text = "", void (*cmd)(Button* self) = 0);
};

#endif /* Button_hpp */
