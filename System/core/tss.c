#include <core/tss.h>
#include <hw/acpi.h>	//getCPUNumber(), but should be moved into smp.c
#include <debug/log.h>
#include <core/gdt.h>
#include <libk/string.h>

void tssInit (size_t esp)
{
	//get TSS base
	uint16_t* memoryLocation = (uint16_t*) (size_t) (0xB000 + getCPUNumber () * 256);

	//clear
	memset (memoryLocation, 0, 0x68);
	
	//write size
	*(memoryLocation + 0x66 / 2) = 0x68;

	//write SS if needed
	if (sizeof (size_t) == 4) {
		*(memoryLocation + 0x8 / 2) = 0x10;			//kernel SS
	}

	tssWriteESP (esp);

	//get LTR value
	logk ("CPUNO = %d\n", getCPUNumber ());
	uint16_t gdtOffset = gdtTSS[getCPUNumber ()] | 3;

	//write
	asm volatile ("mov %0, %%ax; ltrw %%ax" :: "r"(gdtOffset) : "%ax");
	logk ("we didn't blow up.\n");
}

void tssWriteESP(size_t esp)
{
	size_t* memoryLocation = (size_t*) (size_t) (0xB004 + getCPUNumber() * 256);
	*memoryLocation = esp;
}