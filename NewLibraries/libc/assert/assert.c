//
//  assert.c
//  Banana libc
//
//  Created by Alex Boxall on 11/7/18.
//  Copyright © 2018 Alex Boxall. All rights reserved.
//

#ifndef _libk

#include "assert.h"
#include "stdlib.h"     //abort.h
#include "stdio.h"

void __internal_assert(_Bool condition, const char* file, const char* func, int line) {
    if (!condition) {
        fprintf(stderr, "Assertion Error: %s:%d in function '%s'\n", file, line, func);
        abort();
    }
}

#endif
