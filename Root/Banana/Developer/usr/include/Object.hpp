//
//  Object.hpp
//  GUI
//
//  Created by Alex Boxall on 29/5/19.
//  Copyright © 2019 Alex Boxall. All rights reserved.
//

#ifndef Object_hpp
#define Object_hpp

#include <stdint.h>
#include <stdbool.h>


class Object {
private:
protected:
    
public:
    Object();
    virtual Object* copy() = 0;
    virtual const char* debugName();
};

#endif /* Object_hpp */
