#pragma once

#include <stdbool.h>
#include <stdint.h>
#include <core/string.h>
#include <core/objectmgr.h>
#include <devss/devices.h>

void reorderDisks ();

extern void read_hard_disk (size_t, size_t, size_t, size_t);
extern void write_hard_disk (size_t, size_t, size_t, size_t);

struct tagHBA_PORT;

bool sata_read (struct tagHBA_PORT *port, uint32_t startl, uint32_t starth, uint32_t count, uint16_t* buf);
bool sata_write (struct tagHBA_PORT *port, uint32_t startl, uint32_t starth, uint32_t count, uint16_t* buf);

int ReadSector (char driveletter, uint32_t sector, uint16_t sectors, uint8_t* buffer);
int WriteSector (char driveletter, uint32_t sector, uint16_t sectors, uint8_t* buffer);

//pointers to built-in device driver objects
extern InternalObjectID builtinDriverGlobalObjectIDPointer_SATA;
extern InternalObjectID builtinDriverGlobalObjectIDPointer_SATAPI;
extern InternalObjectID builtinDriverGlobalObjectIDPointer_ATA;
extern InternalObjectID builtinDriverGlobalObjectIDPointer_ATAPI;
extern InternalObjectID builtinDriverGlobalObjectIDPointer_Floppy;

typedef enum DiskType
{
	DiskType_Null,
	DiskType_Floppy,
	DiskType_Nonremovable,
	DiskType_Disc,
	DiskType_Network,
	DiskType_Other,
	DiskType_Flash

} DiskType;

typedef struct DiskFormat
{
	bool mounted;

	DiskType type;
	InternalObjectID driver;

	char letter;
	char virtualdisklocation[256];
	uint8_t number;                    //this is for detecting which one of the type to use
									   //e.g. is it the first or second floppy drive? is it the third hard disk? 

	struct tagHBA_PORT* sataPort;

	Device* device;

	uint32_t filesystemDriverID;

	char cwdForThisDrive[256];		//used by the VFS driver

	uint64_t partitionOffset;		//used to make partitioned disks behave as separate disks, and so FS code doesn't need to scan partitions
	uint64_t partitionSize;			//used to ensure writes/reads are inside the boundries of the partition

} DiskFormat;

int partitionScan (char driveletter);

#define ATAPI_SECTOR_SIZE 2048

/* The default ISA IRQ numbers of the ATA controllers. */
#define ATA_IRQ_PRIMARY     0x0E
#define ATA_IRQ_SECONDARY   0x0F

/* The necessary I/O ports, indexed by "bus". */
#define ATA_DATA(x)         (x)
#define ATA_FEATURES(x)     (x+1)
#define ATA_SECTOR_COUNT(x) (x+2)
#define ATA_ADDRESS1(x)     (x+3)
#define ATA_ADDRESS2(x)     (x+4)
#define ATA_ADDRESS3(x)     (x+5)
#define ATA_DRIVE_SELECT(x) (x+6)
#define ATA_COMMAND(x)      (x+7)
#define ATA_DCR(x)          (x+0x206)   /* device control register */

/* valid values for "bus" */
#define ATA_BUS_PRIMARY     0x1F0
#define ATA_BUS_SECONDARY   0x170
/* valid values for "drive" */
#define ATA_DRIVE_MASTER    0xA0
#define ATA_DRIVE_SLAVE     0xB0

/* ATA specifies a 400ns delay after drive switching -- often
* implemented as 4 Alternative Status queries. */
#define ATA_SELECT_DELAY(bus) \
  {inb(ATA_DCR(bus));inb(ATA_DCR(bus));inb(ATA_DCR(bus));inb(ATA_DCR(bus));}

extern DiskFormat Disks[26];
extern int numberOfDisks;