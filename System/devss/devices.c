#include <devss/devices.h>
#include <debug/log.h>
#include <fs/vfs.h>
#include <libk/string.h>
#include <core/panic.h>
#include <core/memory.h>

#include <hw/acpi.h>

void yieldFromKernel();

Device* deviceTree;

void initDeviceSubsystem ()
{
	logk("initDevss.\n");
	deviceTree = malloc(sizeof(Device));
	initDeviceObject (deviceTree);

	deviceTree->onDiskDriver = false;
	deviceTree->openPointer = openACPI;
	
	openDevice (deviceTree, 0, 0, 0);
}

void pTree(Device* d, int ind)
{
	logk("| ");
	for (int i = 0; i < ind; ++i) logk(" ");
	logk(d->humanName);
	logk("\n");

	DeviceNode* ptr = d->children;
	while (ptr->next) {
		if (ptr->device == 0) return;
		pTree(ptr->device, ind + 4);
		ptr = ptr->next;
	}
}

void printTreeFrom(Device* d)
{
	logk("DEVICE TREE PRINT OUT:\n");
	pTree(d, 0);
	logk("END\n");

	logk("\n");
}

/*
TODO: Drivers should be dynamic libraries, and hence also come with a header. This way 
a child device (e.g. USBKEY) could call functions + read / write variables from it's parent,
all it has to do is include the header file, then call the function (or read / write a variable)
and the dynamic linker will call the parent function. The exception will be built in drivers
such as PCI, which will have a statically linked part of each driver (e.g. getPCIInfo, getBarAddress, etc.)

*/

void runDriver (Device* device, DriverMode mode, int a, int b, void* data)
{
	logk("DB\n0x%X\n", device);
	logk("device->humanName = %s\n", device->humanName);

	if (device->onDiskDriver) {
		logk("DC\n");

		if (device->driverLocation == 0) {
			char* t = getDriverName(device);
			if (t) {
				strcpy(device->driverLocation, t);
			} else {
				logk("Driver running failed, no driver loaded.\n");
				return;
			}
		}
		char* filename = device->driverLocation;
		
		//get the PID of the task if it is already running, OR add a new task (if it is an open call)
		//if it doesn't exist and read/write/close/ioctl is called, error.
		//if it doesn't exist and open is called, open it, then run it.

		int pid = 0;
		if (!pid) {
			/*if (mode == DriverModeOpen) {
				pid = addTask (filename);
				TASKS[pid].driver = true;
				TASKS[pid].driverDevice = device;
				device->returned = false;
				//there may be other fields to fix
			} else {
				logk ("DRIVER ERROR (DEVICES.C) in function runDriver\n");
				while (1);
			}*/
		}

		

		/*
		
		
		*/

	} else {
		switch (mode) {
		case DriverModeOpen:
			logk("open. 0x%X\n", device->openPointer);
			device->openPointer (device, a, b, data);
			break;
		case DriverModeRead:
			logk("read.\n");
			device->readPointer (device, a, b, data);
			break;
		case DriverModeWrite:
			logk("write.\n");
			device->writePointer (device, a, b, data);
			break;
		case DriverModeIoctl:
			logk("ioctl.\n");
			device->ioctlPointer (device, a, b, data);
			break;
		case DriverModeHibernate:
			logk("hibernate.\n");
			device->hibernatePointer(device, a);
			break;
		case DriverModeClose:
			logk ("ERR: Driver mode close called!\n");
			break;
		default:
			logk ("ERR: Driver mode %d called!\n", (int) mode);
			break;
		}
	}
}

char* getDriverName (Device* device)
{
	logk("Getting driver filename.\n");
	uint64_t key = device->vendorID;
	key |= device->devClass << 16;
	key |= device->subClass << 24;
	key |= (uint64_t) device->progIF << 32;

	for (int j = 0; j < 2; ++j) {
		if (j == 1) {
			key >>= 16;			//remove the vendor id
		}
		vfs_file_t f;
		int r;
		logk ("About to open database %d\n", j);
		r = vfs_open (&f, j ? "C:/Banana/System/drvdb.sys" : "C:/Banana/System/drvdbv.sys", vfs_file_mode_read);
		if (r) {
			logk ("Error %d\n", r);
			panicWithMessage ("DRIVER_DATABASE_NOT_FOUND");
		}

		char line[260];
		memset (line, 0, 260);

		while (vfs_gets (line, sizeof (line), &f) == 0) {
			uint64_t mkey = 0;
			for (int i = 7; i > -1; --i) {
				mkey <<= 8;
				mkey |= (uint8_t) line[i];
			}
			char* driver = (char*) calloc (256, 1);
			strcpy (driver, line + 8);
			logk ("Found a key of LOW = %d. Driver = %s\n", mkey & 0xFFFFFFFF, driver);

			if (mkey == key) {
				return driver;
			} else {
				freee (driver);
			}
		}

		vfs_close (&f);
	}

	logk("NOT FOUND!\n");

	return 0;
}

void initDeviceObject (Device* device)
{
	device->parent = 0;
	device->children = malloc(sizeof(DeviceNode));
	device->children->next = 0;
	device->children->device = 0;
	device->internalData = 0;
	device->internalDataSize = 0;
	memset(device->humanName, 0, 256);

	device->resources.portsUsed = 0;
	device->resources.memoryUsed = 0;
}

void openDevice (Device* device, int a, int b, void* data)
{
	runDriver (device, DriverModeOpen, a, b, data);
}

//these 3 end up calling runDriver
void readDevice (Device* device, int a, int b, void* data)
{
	runDriver (device, DriverModeRead, a, b, data);
}

void writeDevice (Device* device, int a, int b, void* data)
{
	runDriver (device, DriverModeWrite, a, b, data);
}

void ioctlDevice (Device* device, int a, int b, void* data)
{
	runDriver (device, DriverModeIoctl, a, b, data);
}

void hibernateDevice(Device* device, int a)
{
	runDriver(device, DriverModeHibernate, a, 0, 0);
}

int getLastReturnValue(Device* device)
{
	while (!device->returned) {
		yieldFromKernel();
	}
	return device->returnValue;
}

Device* getParent (Device* device)
{
	return device->parent;
}

int getNumberOfChildren (Device* device)
{
	return 0;
}

Device* getChild (Device* device, int index)
{
	return 0;
}

void addChild(Device* device, Device* childDevice)
{
	if (!device || !childDevice) {
		logk("Null child added! ERROR!\n");
		return;
	}
	childDevice->parent = device;
	DeviceNode* ptr = device->children;
	while (ptr->next) {
		ptr = ptr->next;
	}
	ptr->device = childDevice;
	ptr->next = malloc (sizeof (DeviceNode));
	ptr->next->next = 0;
	ptr->next->device = 0;
}

void removeChild (Device* device, Device* theChild)
{
	if (!device || !theChild) {
		logk("Null child added! ERROR!\n");
		return;
	}

	DeviceNode* ptr = device->children;
	DeviceNode* prev = 0;
	while (ptr->next) {
		if (ptr->device == theChild) {
			//we have found the child

			if (ptr->next) {
				//there are entries after it

				if (prev) {
					//there are entries before it

					prev->next = ptr->next;			//skip us

				} else {
					//first entry (i.e. removing ACPI for some reason)
					logk("Are you sure you want to remove this child? Oh well...\n");
					ptr->next = 0;
					ptr->device = 0;
				}
				
			} else {
				//it is the last entry
				ptr->next = 0;
				ptr->device = 0;
			}
		}
		prev = ptr;
		ptr = ptr->next;
	}
}