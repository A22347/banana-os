#include <hw/ide.h>
#include <debug/log.h>
#include <hal/diskio.h>
#include <hw/pci.h>
#include <hw/ata.h>
#include <hw/atapi.h>
#include <core/memory.h>

//this sort of stuff will go into a devss class eventually
struct IDEChannelRegisters ideChannels[2];

uint8_t ide_buf[2048] = { 0 };
volatile uint8_t ide_irq_invoked = 0;
volatile uint8_t ide_irq_invoked2 = 0;
uint8_t atapi_packet[12] = { 0xA8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

//this sort of stuff will go into a devss class eventually
struct ide_device ide_devices[4];

void sleep (int a)
{
	
}

uint8_t ide_read (uint8_t channel, uint8_t reg)
{
	uint8_t result = 0;
	if (reg > 0x07 && reg < 0x0C) {
		ide_write (channel, ATA_REG_CONTROL, 0x80 | ideChannels[channel].nIEN);
	}
	if (reg < 0x08) {
		result = inb (ideChannels[channel].base + reg - 0x00);
	} else if (reg < 0x0C) {
		result = inb (ideChannels[channel].base + reg - 0x06);
	} else if (reg < 0x0E) {
		result = inb (ideChannels[channel].ctrl + reg - 0x0A);
	} else if (reg < 0x16) {
		result = inb (ideChannels[channel].bmide + reg - 0x0E);
	}

	if (reg > 0x07 && reg < 0x0C) {
		ide_write (channel, ATA_REG_CONTROL, ideChannels[channel].nIEN);
	}

	return result;
}

void ide_write (unsigned char channel, unsigned char reg, unsigned char data)
{
	if (reg > 0x07 && reg < 0x0C) {
		ide_write (channel, ATA_REG_CONTROL, 0x80 | ideChannels[channel].nIEN);
	}
	if (reg < 0x08) {
		outb (ideChannels[channel].base + reg - 0x00, data);
	} else if (reg < 0x0C) {
		outb (ideChannels[channel].base + reg - 0x06, data);
	} else if (reg < 0x0E) {
		outb (ideChannels[channel].ctrl + reg - 0x0A, data);
	} else if (reg < 0x16) {
		outb (ideChannels[channel].bmide + reg - 0x0E, data);
	}
	if (reg > 0x07 && reg < 0x0C) {
		ide_write (channel, ATA_REG_CONTROL, ideChannels[channel].nIEN);
	}
}

void ide_read_buffer (uint8_t channel, uint8_t reg, uint32_t buffer,
					  uint32_t quads)
{
	if (reg > 0x07 && reg < 0x0C) {
		ide_write (channel, ATA_REG_CONTROL, 0x80 | ideChannels[channel].nIEN);
	}
	if (reg < 0x08) {
		insl (ideChannels[channel].base + reg - 0x00, (void*) (size_t) buffer, quads);
	} else if (reg < 0x0C) {
		insl (ideChannels[channel].base + reg - 0x06, (void*) (size_t) buffer, quads);
	} else if (reg < 0x0E) {
		insl (ideChannels[channel].ctrl + reg - 0x0A, (void*) (size_t) buffer, quads);
	} else if (reg < 0x16) {
		insl (ideChannels[channel].bmide + reg - 0x0E, (void*) (size_t) buffer, quads);
	}
	if (reg > 0x07 && reg < 0x0C) {
		ide_write (channel, ATA_REG_CONTROL, ideChannels[channel].nIEN);
	}
}

uint8_t ide_polling (uint8_t channel, uint32_t advanced_check)
{
	// Delay 400 nanosecond for BSY to be set:
	for (int i = 0; i < 4; i++) {
		ide_read (channel, ATA_REG_ALTSTATUS);		// Reading the Alternate Status port wastes 100ns; loop four times.
	}
	
	// Wait for BSY to be cleared:
	while (ide_read (channel, ATA_REG_STATUS) & ATA_SR_BSY);

	if (advanced_check) {
		unsigned char state = ide_read (channel, ATA_REG_STATUS); // Read Status Register.

		// Check For Errors:
		if (state & ATA_SR_ERR) {
			return 2;
		}

		// Check If Device fault:
		if (state & ATA_SR_DF) {
			return 1;
		}

		// Check DRQ:
		// BSY = 0; DF = 0; ERR = 0 so we should check for DRQ now.
		if ((state & ATA_SR_DRQ) == 0) {
			return 3;
		}
	}

	return 0;
}

void ide_initialize (Device* self, unsigned int BAR0, unsigned int BAR1, unsigned int BAR2, unsigned int BAR3,
					 unsigned int BAR4)
{
	logk ("init IDE\n");
	int j, k, count = 0;

	ideChannels[ATA_PRIMARY].base = (BAR0 & 0xFFFFFFFC) + 0x1F0 * (!BAR0);
	ideChannels[ATA_PRIMARY].ctrl = (BAR1 & 0xFFFFFFFC) + 0x3F6 * (!BAR1);
	ideChannels[ATA_SECONDARY].base = (BAR2 & 0xFFFFFFFC) + 0x170 * (!BAR2);
	ideChannels[ATA_SECONDARY].ctrl = (BAR3 & 0xFFFFFFFC) + 0x376 * (!BAR3);
	ideChannels[ATA_PRIMARY].bmide = (BAR4 & 0xFFFFFFFC) + 0;
	ideChannels[ATA_SECONDARY].bmide = (BAR4 & 0xFFFFFFFC) + 8;

	// Disable IRQs
	ide_write (ATA_PRIMARY, ATA_REG_CONTROL, 2);
	ide_write (ATA_SECONDARY, ATA_REG_CONTROL, 2);

	// 3- Detect ATA-ATAPI Devices:
	for (int i = 0; i < 2; i++)
		for (j = 0; j < 2; j++) {
			logk ("detecting ATA/ATAPI devices...\n");
			unsigned char err = 0, type = IDE_ATA, status;
			ide_devices[count].reserved = 0; // Assuming that no drive here.

											 // (I) Select Drive:
			ide_write (i, ATA_REG_HDDEVSEL, 0xA0 | (j << 4)); // Select Drive.
			sleep (1); // Wait 1ms for drive select to work.

					   // (II) Send ATA Identify Command:
			ide_write (i, ATA_REG_COMMAND, ATA_CMD_IDENTIFY);
			sleep (1); // This function should be implemented in your OS. which waits for 1 ms.
					   // it is based on System Timer Device Driver.

					   // (III) Polling:
			if (ide_read (i, ATA_REG_STATUS) == 0) continue; // If Status = 0, No Device.

			while (1) {
				logk ("looping...\n");
				status = ide_read (i, ATA_REG_STATUS);
				if ((status & ATA_SR_ERR)) { err = 1; break; } // If Err, Device is not ATA.
				if (!(status & ATA_SR_BSY) && (status & ATA_SR_DRQ)) break; // Everything is right.
			}

			// (IV) Probe for ATAPI Devices:

			if (err != 0) {
				unsigned char cl = ide_read (i, ATA_REG_LBA1);
				unsigned char ch = ide_read (i, ATA_REG_LBA2);

				if (cl == 0x14 && ch == 0xEB)
					type = IDE_ATAPI;
				else if (cl == 0x69 && ch == 0x96)
					type = IDE_ATAPI;
				else
					continue; // Unknown Type (may not be a device).

				ide_write (i, ATA_REG_COMMAND, ATA_CMD_IDENTIFY_PACKET);
				sleep (1);
			}

			// (V) Read Identification Space of the Device:
			ide_read_buffer (i, ATA_REG_DATA, (unsigned int) (size_t) ide_buf, 128);

			// (VI) Read Device Parameters:
			ide_devices[count].reserved = 1;
			ide_devices[count].type = type;
			ide_devices[count].channel = i;
			ide_devices[count].drive = j;
			ide_devices[count].signature = *((unsigned short *) (ide_buf + ATA_IDENT_DEVICETYPE));
			ide_devices[count].capabilities = *((unsigned short *) (ide_buf + ATA_IDENT_CAPABILITIES));
			ide_devices[count].commandSets = *((unsigned int *) (ide_buf + ATA_IDENT_COMMANDSETS));

			// (VII) Get Size:
			if (ide_devices[count].commandSets & (1 << 26))
				// Device uses 48-Bit Addressing:
				ide_devices[count].size = *((unsigned int *) (ide_buf + ATA_IDENT_MAX_LBA_EXT));
			else
				// Device uses CHS or 28-bit Addressing:
				ide_devices[count].size = *((unsigned int *) (ide_buf + ATA_IDENT_MAX_LBA));

			// (VIII) String indicates model of device (like Western Digital HDD and SONY DVD-RW...):
			for (k = 0; k < 40; k += 2) {
				ide_devices[count].model[k] = ide_buf[ATA_IDENT_MODEL + k + 1];
				ide_devices[count].model[k + 1] = ide_buf[ATA_IDENT_MODEL + k];
			}
			ide_devices[count].model[40] = 0; // Terminate String.

			count++;
		}

	// 4- Print Summary:

	for (int i = 0; i < 4; i++) {
		if (ide_devices[i].reserved == 1) {
			logk ("%d: Found %s Drive %dMB - %s\n", i,
				(const char *[])
			{
				"ATA", "ATAPI"
			}[ide_devices[i].type],						/* Type */
					ide_devices[i].size / 1024 / 2,     /* Size */
					ide_devices[i].model);

			logk ("%d, %d\n", ide_devices[i].channel, ide_devices[i].drive);

			Disks[numberOfDisks].type = ide_devices[i].type == 0 ? DiskType_Nonremovable : DiskType_Disc;
			Disks[numberOfDisks].number = numberOfDisks;
			Disks[numberOfDisks].letter = 'A' + numberOfDisks;
			Disks[numberOfDisks].partitionOffset = 0;
			Disks[numberOfDisks].partitionSize = 0;

			int oldDiskCnt = numberOfDisks;
			++numberOfDisks;

			//automatically copies disk data across
			partitionScan(Disks[oldDiskCnt].letter);
			int partitions = numberOfDisks - oldDiskCnt;

			logk("FOUND 0x%X PARTITIONS ON A IDE DISK\n", partitions);

			for (int j = 0; j < partitions; ++j) {
				Device* newdev = malloc(sizeof(Device));
				initDeviceObject(newdev);
				newdev->onDiskDriver = false;
				newdev->driverLocation = 0;
				newdev->openPointer = ide_devices[i].type == 0 ? openATA : openATAPI;
				newdev->readPointer = ide_devices[i].type == 0 ? readATA : readATAPI;
				newdev->writePointer = ide_devices[i].type == 0 ? writeATA : writeATAPI;

				Disks[j + oldDiskCnt].device = newdev;

				char nm[256];
				sprintf(nm, "%s %dMB - %s",
					(const char* [])
				{
					"ATA", "ATAPI"
				}[ide_devices[i].type],						/* Type */
						ide_devices[i].size / 1024 / 2,     /* Size */
						ide_devices[i].model);

				strcpy(newdev->humanName, nm);

				addChild(self, newdev);
				openDevice(newdev, i, 0, 0);
			}
		}
	}
}

void setupIDEControllers (Device* self, PCIDeviceInfo info)
{
	logk ("setting up IDE controllers\n");
	ide_initialize (self, info.bar[0], info.bar[1], info.bar[2], info.bar[3], info.bar[4]);
}

void openIDE(Device* self, int b, int c, void* d)
{
	setupIDEControllers(self, *((PCIDeviceInfo *)d));
}