﻿#include <hw/ahci.h>
#include <core/kernel.h>
#include <debug/log.h>
#include <hal/diskio.h>
#include <sync/spinlock.h>

#define    SATA_SIG_ATA    0x00000101    // SATA drive
#define    SATA_SIG_ATAPI    0xEB140101    // SATAPI drive
#define    SATA_SIG_SEMB    0xC33C0101    // Enclosure management bridge
#define    SATA_SIG_PM        0x96690101    // Port multiplier

#define AHCI_DEV_NULL 0
#define AHCI_DEV_SATA 1
#define AHCI_DEV_SATAPI 4
#define AHCI_DEV_SEMB 2
#define AHCI_DEV_PM 3

#define HBA_PORT_DET_PRESENT 3
#define HBA_PORT_IPM_ACTIVE 1

#define HBA_PxIS_TFES   (1 << 30)

#define HBA_PxCMD_CR            (1 << 15) /* CR - Command list Running */
#define HBA_PxCMD_FR            (1 << 14) /* FR - FIS receive Running */
#define HBA_PxCMD_FRE           (1 <<  4) /* FRE - FIS Receive Enable */
#define HBA_PxCMD_SUD           (1 <<  1) /* SUD - Spin-Up Device */
#define HBA_PxCMD_ST            (1 <<  0) /* ST - Start (command processing) */

#define ATA_CMD_READ_DMA_EX     0x25
#define ATA_CMD_WRITE_DMA_EX    0x35

HBA_PORT* validSATAPorts[32] = { 0 };
HBA_MEM* global_abar;

spinlock_t ahciLock;

void findSATADrives(PCIDeviceInfo info)
{
	spinlockInit(&ahciLock);

	HBA_MEM* abar = (HBA_MEM*) (size_t) info.bar[5];
	global_abar = abar;
	// Search disk in impelemented ports
	uint32_t pi = abar->pi;
	logk("abar->pi = %d\n", pi);
	int i = 0;
	while (i < 32) {
		if (pi & 1) {
			int dt = check_type(&abar->ports[i]);
			if (dt == AHCI_DEV_SATA) {
				logk("SATA drive found at port %d\n", i);
				validSATAPorts[i] = &abar->ports[i];
				port_rebase(&abar->ports[i], i);

				logk("SATA DISK %c FOUND", 'A' + numberOfDisks - numberOfFloppies + 2);

				Disks[numberOfDisks].sataPort = (struct tagHBA_PORT*) & abar->ports[i];
				Disks[numberOfDisks].type = DiskType_Nonremovable;        // 'C:' has an offset of 2
				Disks[numberOfDisks].driver = builtinDriverGlobalObjectIDPointer_SATA;
				Disks[numberOfDisks].number = numberOfDisks;
				Disks[numberOfDisks].letter = 'A' + numberOfDisks;

				//logk("numberOfDisks = %d\n", numberOfDisks);
				++numberOfDisks;

				//automatically copies disk data across
				partitionScan(Disks[numberOfDisks - 1].letter);


			} else if (dt == AHCI_DEV_SATAPI) {
				logk("SATAPI disc found at port %d\n", i);
				validSATAPorts[i] = &abar->ports[i];
				port_rebase(&abar->ports[i], i);

				logk("SATAPI DISK %c FOUND", 'A' + numberOfDisks - numberOfFloppies + 2);

				Disks[numberOfDisks].sataPort = (struct tagHBA_PORT*) & abar->ports[i];
				Disks[numberOfDisks].type = DiskType_Disc;        // 'C:' has an offset of 2
				Disks[numberOfDisks].driver = builtinDriverGlobalObjectIDPointer_SATAPI;
				Disks[numberOfDisks].number = numberOfDisks - numberOfFloppies + 2;
				Disks[numberOfDisks].letter = 'A' + numberOfDisks;
				++numberOfDisks;


			} else if (dt == AHCI_DEV_SEMB) {
				logk("SEMB drive found at port %d\n", i);
			} else if (dt == AHCI_DEV_PM) {
				logk("PM drive found at port %d\n", i);
			} else {
				logk("No drive found at port %d\n", i);
			}
		}

		pi >>= 1;
		i++;
	}
}

// Check device type
int check_type(HBA_PORT * port)
{
	uint32_t ssts = port->ssts;

	uint8_t ipm = (ssts >> 8) & 0x0F;
	uint8_t det = ssts & 0x0F;

	if (det != HBA_PORT_DET_PRESENT)    // Check drive status
		return AHCI_DEV_NULL;
	if (ipm != HBA_PORT_IPM_ACTIVE)
		return AHCI_DEV_NULL;

	switch (port->sig) {
	case SATA_SIG_ATAPI:
		return AHCI_DEV_SATAPI;
	case SATA_SIG_SEMB:
		return AHCI_DEV_SEMB;
	case SATA_SIG_PM:
		return AHCI_DEV_PM;
	default:
		return AHCI_DEV_SATA;
	}
}

void port_rebase(HBA_PORT * port, int portno)
{
	uint32_t AHCI_BASE = AHCI_PORT_BASE + portno * 1024;

	stop_cmd(port);    // Stop command engine

	// Command list offset: 1K*portno
	// Command list entry size = 32
	// Command list entry maxim count = 32
	// Command list maxim size = 32*32 = 1K per port
	port->clb = AHCI_BASE + (portno << 10);
	port->clbu = 0;
	memset((void*) (size_t) (port->clb), 0, 1024);

	// FIS offset: 32K+256*portno
	// FIS entry size = 256 bytes per port
	port->fb = AHCI_BASE + (32 << 10) + (portno << 8);
	port->fbu = 0;
	memset((void*) (size_t) (port->fb), 0, 256);

	// Command table offset: 40K + 8K*portno
	// Command table size = 256*32 = 8K per port
	HBA_CMD_HEADER * cmdheader = (HBA_CMD_HEADER*) (size_t) (port->clb);
	for (int i = 0; i < 32; i++) {
		cmdheader[i].prdtl = 8;    // 8 prdt entries per command table
		// 256 bytes per command table, 64+16+48+16*8
		// Command table offset: 40K + 8K*portno + cmdheader_index*256
		cmdheader[i].ctba = AHCI_BASE + (40 << 10) + (portno << 13) + (i << 8);
		cmdheader[i].ctbau = 0;
		memset((void*) (size_t) cmdheader[i].ctba, 0, 256);
	}

	start_cmd(port);    // Start command engine
}

// Start command engine
void start_cmd(HBA_PORT * port)
{
	// Wait until CR (bit15) is cleared
	while (port->cmd & HBA_PxCMD_CR);

	// Set FRE (bit4) and ST (bit0)
	port->cmd |= HBA_PxCMD_FRE;
	port->cmd |= HBA_PxCMD_ST;
}

// Stop command engine
void stop_cmd(HBA_PORT * port)
{
	// Clear ST (bit0)
	port->cmd &= ~HBA_PxCMD_ST;
	port->cmd &= ~HBA_PxCMD_FRE;

	// Wait until FR (bit14), CR (bit15) are cleared
	while (1) {
		if (port->cmd & HBA_PxCMD_FR)
			continue;
		if (port->cmd & HBA_PxCMD_CR)
			continue;
		break;
	}

	// Clear FRE (bit4)
	port->cmd &= ~HBA_PxCMD_FRE;
}

#define ATA_DEV_BUSY 0x80
#define ATA_DEV_DRQ 0x08

#include <core/memory.h>
bool sata_read(struct tagHBA_PORT* port, uint32_t startl, uint32_t starth, uint32_t count, uint16_t * actualBuf)
{
	spinlockLock(&ahciLock);

	if (count != 1) {
		logk("SATA COULD NOT 1 (%d)!\n", count);
	}
	uint16_t buf[256];

	logk("SATA READ l:0x%X  h:0x%X (cnt: 0x%X)\n", startl, starth, count);

	port->is = (uint32_t) -1;        // Clear pending interrupt bits
	int spin = 0; // Spin lock timeout counter
	int slot = find_cmdslot(port);
	if (slot == -1) {
		spinlockUnlock(&ahciLock);
		return false;
	}

	HBA_CMD_HEADER * cmdheader = (HBA_CMD_HEADER*) (size_t) port->clb;
	cmdheader += slot;
	cmdheader->cfl = sizeof(FIS_REG_H2D) / sizeof(uint32_t);    // Command FIS size
	cmdheader->w = 0;        // Read from device
	cmdheader->prdtl = (uint16_t) ((count - 1) >> 4) + 1;    // PRDT entries count

	HBA_CMD_TBL * cmdtbl = (HBA_CMD_TBL*) (size_t) (cmdheader->ctba);
	memset(cmdtbl, 0, sizeof(HBA_CMD_TBL) +
		(cmdheader->prdtl - 1) * sizeof(HBA_PRDT_ENTRY));

	// 8K bytes (16 sectors) per PRDT
	int i = 0;
	/*for (i = 0; i < cmdheader->prdtl - 1; i++) {
		cmdtbl->prdt_entry[i].dba = (uint32_t) (size_t) buf;
		cmdtbl->prdt_entry[i].dbc = 8 * 1024 - 1;    // 8K bytes
		cmdtbl->prdt_entry[i].i = 1;
		buf += 4 * 1024;    // 4K words
		count -= 16;    // 16 sectors
	}*/
	// Last entry
	cmdtbl->prdt_entry[i].dba = (uint32_t) (size_t) buf;
	cmdtbl->prdt_entry[i].dbc = (count << 9) - 1;    // 512 bytes per sector
	cmdtbl->prdt_entry[i].i = 1;

	// Setup command
	FIS_REG_H2D* cmdfis = (FIS_REG_H2D*) (&cmdtbl->cfis);

	cmdfis->fis_type = FIS_TYPE_REG_H2D;
	cmdfis->c = 1;    // Command
	cmdfis->command = ATA_CMD_READ_DMA_EX;

	cmdfis->lba0 = (uint8_t) startl;
	cmdfis->lba1 = (uint8_t) (startl >> 8);
	cmdfis->lba2 = (uint8_t) (startl >> 16);
	cmdfis->device = 1 << 6;    // LBA mode

	cmdfis->lba3 = (uint8_t) (startl >> 24);
	cmdfis->lba4 = (uint8_t) starth;
	cmdfis->lba5 = (uint8_t) (starth >> 8);

	cmdfis->countl = count & 0xFF;
	cmdfis->counth = count >> 8;

	// The below loop waits until the port is no longer busy before issuing a new command
	while ((port->tfd & (ATA_DEV_BUSY | ATA_DEV_DRQ)) && spin < 1000000) {
		spin++;
	}
	if (spin == 1000000) {
		logk("Port is hung\n");
		spinlockUnlock(&ahciLock);
		return false;
	}

	port->ci = 1 << slot;    // Issue command

	// Wait for completion
	while (1) {
		// In some longer duration reads, it may be helpful to spin on the DPS bit
		// in the PxIS port field as well (1 << 5)
		if ((port->ci & (1 << slot)) == 0)
			break;
		if (port->is & HBA_PxIS_TFES)    // Task file error
		{
			logk("Read disk error\n");
			spinlockUnlock(&ahciLock);
			return false;
		}
	}

	// Check again
	if (port->is & HBA_PxIS_TFES) {
		logk("Read disk error\n");
		spinlockUnlock(&ahciLock);
		return false;
	}

	spinlockUnlock(&ahciLock);

	memcpy(actualBuf, buf, 512 * count);
	logk("uint16_t buffer[0] = 0x%X\n", buf[0]);
	return false;
}


bool sata_write(struct tagHBA_PORT* port, uint32_t startl, uint32_t starth, uint32_t count, uint16_t * actualBuf)
{
	spinlockLock(&ahciLock);

	if (count != 1) {
		logk("SATA COULD NOT 1 (%d)!\n", count);
	}

	uint16_t buf[256];
	memcpy(buf, actualBuf, 512 * count);

	uint8_t* rw;
	
	rw = (uint8_t*) 0xB8000;
	*rw++ = 'W';
	*rw++ = 0x0F;
	*rw++ = 'B';
	*rw++ = 0x0F;

	port->is = (uint32_t) -1;        // Clear pending interrupt bits
	int spin = 0; // Spin lock timeout counter
	int slot = find_cmdslot(port);
	if (slot == -1) {
		spinlockUnlock(&ahciLock);
		return false;
	}

	HBA_CMD_HEADER * cmdheader = (HBA_CMD_HEADER*) (size_t) port->clb;
	cmdheader += slot;
	cmdheader->cfl = sizeof(FIS_REG_H2D) / sizeof(uint32_t);    // Command FIS size
	cmdheader->w = 1;        // Read from device
	cmdheader->c = 0;        // Read from device
	cmdheader->p = 0;        // Read from device
	cmdheader->prdtl = (uint16_t) ((count - 1) >> 4) + 1;    // PRDT entries count
	cmdheader->prdbc = 0;

	HBA_CMD_TBL * cmdtbl = (HBA_CMD_TBL*) (size_t) (cmdheader->ctba);
	memset(cmdtbl, 0, sizeof(HBA_CMD_TBL) +
		(cmdheader->prdtl - 1) * sizeof(HBA_PRDT_ENTRY));

	int i = 0;
	/*
	for (i = 0; i < cmdheader->prdtl - 1; i++) {
		cmdtbl->prdt_entry[i].dba = (uint32_t) (size_t) buf;
		cmdtbl->prdt_entry[i].dbau = 0;    //high dword of 64 bit buf
		cmdtbl->prdt_entry[i].dbc = 8 * 1024 - 1;    // 8K bytes
		cmdtbl->prdt_entry[i].i = 0;
		buf += 4 * 1024;    //-1;    // 4K words
		count -= 16;    // 16 sectors
	}*/
	// Last entry
	cmdtbl->prdt_entry[i].dba = (uint32_t) (size_t) buf;
	cmdtbl->prdt_entry[i].dbau = 0;    //high dword of 64 bit buf
	cmdtbl->prdt_entry[i].dbc = (count << 9) - 1;    // 512 bytes per sector
	cmdtbl->prdt_entry[i].i = 0;

	FIS_REG_H2D * cmdfis = (FIS_REG_H2D*) (&cmdtbl->cfis);

	cmdfis->fis_type = FIS_TYPE_REG_H2D;
	cmdfis->c = 1;    // Command
	cmdfis->command = ATA_CMD_WRITE_DMA_EX;
	cmdfis->control = 0x8;

	cmdfis->lba0 = (uint8_t) startl;
	cmdfis->lba1 = (uint8_t) (startl >> 8);
	cmdfis->lba2 = (uint8_t) (startl >> 16);
	cmdfis->device = 1 << 6;    // LBA mode

	cmdfis->lba3 = (uint8_t) (startl >> 24);
	cmdfis->lba4 = (uint8_t) starth;
	cmdfis->lba5 = (uint8_t) (starth >> 8);

	cmdfis->countl = count & 0xFF;
	cmdfis->counth = count >> 8;

	// The below loop waits until the port is no longer busy before issuing a new command


	//@@@
	while ((port->tfd & (ATA_DEV_BUSY | ATA_DEV_DRQ)) && spin < 1000000) {
		spin++;
	}
	if (spin == 1000000) {
		logk("Port is hung\n");
		spinlockUnlock(&ahciLock);

		return false;
	}


	port->ci = 1 << slot;    // Issue command
	// Wait for completion
	while (1) {
		// In some longer duration reads, it may be helpful to spin on the DPS bit
		// in the PxIS port field as well (1 << 5)
		if ((port->ci & (1 << slot)) == 0)
			break;
		if (port->is & HBA_PxIS_TFES)    // Task file error
		{
			logk("Write disk error A\n");
			spinlockUnlock(&ahciLock);

			return false;
		}
	}

	// Check again
	if (port->is & HBA_PxIS_TFES) {
		logk("Write disk error B\n");
		spinlockUnlock(&ahciLock);
		return false;
	}

	spinlockUnlock(&ahciLock);
	return false;
}

// Find a free command list slot
int find_cmdslot(HBA_PORT * port)
{
	// If not set in SACT and CI, the slot is free
	uint32_t slots = (port->sact | port->ci);
	int num_of_slots = (global_abar->cap & 0x0f00) >> 8; // Bit 8-12

	for (int i = 0; i < num_of_slots; i++) {
		if ((slots & 1) == 0)
			return i;
		slots >>= 1;
	}
	logk("Cannot find free command list entry\n");
	return -1;
}