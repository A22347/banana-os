bits 32

cli
jmp start

extern kernel_early
extern kernel_main_c
extern paging_setup
extern int32
global testa
global _e_systicks
global returnToLongMode
global returnToProtectedMode
global gotoProtectedMode

global GDT64
global GDT64Pointer

_e_systicks dd 0

global TSS

GDT64:                           ; Global Descriptor Table (64-bit).
    .Null: equ $ - GDT64         ; The null descriptor.
    dw 0                         ; Limit (low).
    dw 0                         ; Base (low).
    db 0                         ; Base (middle)
    db 0                         ; Access.
    db 0                         ; Granularity.
    db 0                         ; Base (high).
    .Code: equ $ - GDT64         ; The code descriptor.
    dw 0xFFFF                    ; Limit (low).
    dw 0                         ; Base (low).
    db 0                         ; Base (middle)
    db 0x9A                      ; Access (exec/read).
    db 0xCF                      ; Granularity.
    db 0                         ; Base (high).
    .Data: equ $ - GDT64         ; The data descriptor.
    dw 0xFFFF                    ; Limit (low).
    dw 0                         ; Base (low).
    db 0                         ; Base (middle)
    db 0x92                      ; Access (read/write).
    db 0xCF                      ; Granularity.
    db 0                         ; Base (high).
    
    .UCode: equ $ - GDT64         ; The code descriptor.
    dw 0xFFFF                    ; Limit (low).
    dw 0                         ; Base (low).
    db 0                         ; Base (middle)
    db 0xFA                      ; Access (exec/read).
    db 0xCF                      ; Granularity.
    db 0                         ; Base (high).
    .UData: equ $ - GDT64         ; The data descriptor.
    dw 0xFFFF                    ; Limit (low).
    dw 0                         ; Base (low).
    db 0                         ; Base (middle)
    db 0xF2                      ; Access (read/write).
    db 0xCF                      ; Granularity.
    db 0                         ; Base (high).
    
    ;TSS' address as "base", TSS' size as "limit"
    
	          
    TSS: equ $ - GDT64		; The data descriptor.
    dw 0x6A                     ; Limit (low).
    dw 0xB000                         ; Base (low).
    db 0                         ; Base (middle)
    db 10001001b                 ; Access (read/write).
    db 00000000b                 ; Granularity.
    db 0						; Base (high).
	dq 0x0000000000000000


	;CPU 1
	TSS2: equ $ - GDT64          ; The data descriptor.
    dw 0x6A                     ; Limit (low).
    dw 0xB100                         ; Base (low).
    db 0                         ; Base (middle)
    db 10001001b                 ; Access (read/write).
    db 00000000b                 ; Granularity.
    db 0                         ; Base (high).
	dq 0x0000000000000000

	; CPU 2
    TSS3: equ $ - GDT64          ; The data descriptor.
    dw 0x6A                     ; Limit (low).
    dw 0xB200                         ; Base (low).
    db 0                         ; Base (middle)
    db 10001001b                 ; Access (read/write).
    db 00000000b                 ; Granularity.
    db 0						; Base (high).
	dq 0x0000000000000000


	;CPU 3
	TSS4: equ $ - GDT64          ; The data descriptor.
    dw 0x6A                     ; Limit (low).
    dw 0xB300                         ; Base (low).
    db 0                         ; Base (middle)
    db 10001001b                 ; Access (read/write).
    db 00000000b                 ; Granularity.
    db 0                         ; Base (high).
	dq 0x0000000000000000

GDT64Pointer:                    ; The GDT-pointer.
    dw $ - GDT64 - 1             ; Limit.
    dd GDT64                     ; Base.
  

;0xA000 -> 0x3000000 -> 0x3800000 -> DATA
;0xA000 -> 0x1000000 -> 0x1800000 -> DATA

point1 dd 0xA000
point2 dd 0x1000000
point3 dd 0x1000007
point4 dd 0x1400000
point5 dd 0x1400007
point6 dd 0x1800000
point7 dd 0x1800007

paging_setupx:
	mov eax, 0
    mov ebx, [point3]    ;remember you have to ' | 3'
    mov ecx, [point1]
    
    ;lines 16 - 18 of C code
    lp1:
        mov [ecx], ebx
        mov [ecx + 4], dword 0

        add ebx, 4096
        add ecx, 4
        inc eax
        cmp eax, 1024
        je ed1
        jmp lp1
        
    ed1:

    mov eax, 0
    mov ebx, 3
    mov ecx, [point2]
    
    lp2:
        mov [ecx], ebx
        add ebx, 4096

        add ecx, 4
        inc eax
        cmp eax, 1024 * 1024
        je ed2
        jmp lp2
    
    ed2:

    ret

toput dd 0

start:
    cli
    mov	ax, 0x10		; set data segments to data selector (0x10)
    mov	ds, ax
    mov	ss, ax
    mov	es, ax
    mov	esp, 0x0800000  ;0x9FFFF0

	mov eax, 0
	mov al, byte [0x7D01]	;CPU ID
	shl eax, 20				;1 MB
	sub esp, eax			;CPU 0 = 0x500..., CPU 1 = 0x480...
	;MAXIMUM OF 4s CPU 

    mov ebp, esp

	cmp byte [0x7D01], 0		;only boot CPU 0 (the bootloader sets this byte correctly)
	jne skipLoadingGDTPointer

	mov [0x7D04], dword GDT64Pointer
    
skipLoadingGDTPointer:
	cmp byte [0x7D01], 0
	je cpu0paging
	jmp dopaging

cpu0paging:
	mov [point1], dword 0xA000
	mov [point2], dword 0x1000000
	mov [point3], dword 0x1000007
	mov [point4], dword 0x1400000
	mov [point5], dword 0x1400007
	mov [point6], dword 0x1800000
	mov [point7], dword 0x1800007

	mov [toput], dword 0xA000

dopaging:
    call paging_setupx

	mov eax, [toput]
    mov cr3, eax
    
    ;turn on PAE
    ;mov eax, cr4
    ;or eax, 1 << 5
    ;mov cr4, eax
    
    ;enable paging
    mov eax, cr0
    or eax, 1 << 31
    mov cr0, eax

	
	;although not neccessary, 
	;allow the kernel to write to readonly pages
	;(it is enabled by default, but just in case)
	;this means the kernel can write but apps can't
	mov eax, cr0
	and eax, ~(1 << 16)
	mov cr0, eax

	;mov eax, cr4
	;bit 11 crashes hardware when set,
	;move this stuff into C (using inline assembler)
	;so we can detect UMIP using CPUID (it's only avaliable on newer CPUs)
	;also, we should check CPUID for TSC as well.

	;or eax, 1 << 2			;disable RDTSC in usermode
	;or eax, 1 << 11		;UMIP enable. this disables SGDT, SIDT, SLDT, SMSW and STR in usermode
	;mov cr4, eax

    lgdt [GDT64Pointer]
    jmp GDT64.Code:next    
    
align 0x200
next:    
	cmp byte [0x7D01], 0		;only boot CPU 0 (the bootloader sets this byte correctly)
	jne secondCPULoop

    call kernel_early
    call kernel_main_c

EndlessLoop:
    cli
    hlt
    jmp EndlessLoop
 

extern doCoolStuffWithCPU2
secondCPULoop:
	call doCoolStuffWithCPU2
	jmp $