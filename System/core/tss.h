#pragma once

#ifdef __cplusplus
extern "C" {
#endif

#include <stddef.h>

	void tssInit (size_t esp);
	void tssWriteESP (size_t esp);

#ifdef __cplusplus
}
#endif