//
//  Control.hpp
//  GUI
//
//  Created by Alex Boxall on 29/5/19.
//  Copyright © 2019 Alex Boxall. All rights reserved.
//

#ifndef Control_hpp
#define Control_hpp

#include "Object.hpp"
#include "MouseStates.hpp"
#include "KeyStates.hpp"

enum ScreenScale {
    ScreenScaleNormal,
    ScreenScaleMedium,
    ScreenScaleLarge
};

extern bool highContrast;
extern ScreenScale sscale;

class Window;

class Control : public Object {
private:
protected:
    friend class Window;

    Control* parent;
    struct ChildNode {
        ChildNode* next;
        Control* child;
        ChildNode* prev;
    };
    
    ChildNode* children;
    ChildNode* lastChildNode;   //used to reset the reverse chain
    
    void addChild (Control* c);
    void deleteChild (Control* c);
    
    Control* cfocus;
    
    void resetChains();
    Control* findFocus();

    virtual bool childYieldedFocusChain();
    virtual bool childYieldedFocusChainBackwards();
    
    int x;
    int y;
    int w;
    int h;
    
    bool dirty;
    ChildNode* focusChainCurrent;       //the current location in the tab key selection control for its children
                                        //i.e. will hold child1, when tab pressed on child it will go through their children
                                        //after all children it will return to the parent's focusChainCurrent->next (this one)
                                        //etc. until all are through then it restarts at the root window

public:
    bool focus = false;
    bool forceNoFocus = false;
    
    Control (Control* parent = nullptr);
    
    virtual bool canHoldFocus() = 0;     //whether the tab key can be used to select this control
    
    void makeDirty();
    
    void setX(int xx);
    void setY(int yy);
    void setWidth(int width);
    void setHeight(int height);
    
    int getX();
    int getY();
    int getWidth();
    int getHeight();
    
    bool findAnyFocusableChild();
    
    void handleEvents(KeyState ks);
    void checkForEvents();

    virtual void drawPixel (int x, int y, uint32_t col);
    virtual void internalDraw() = 0;
    virtual void draw();
    

    virtual void mouse (MouseState m);          //called after all mouse movements / clicks
    virtual void keyboard (KeyState k);         //called after all key presses / releases
    virtual void resize ();                     //called when resized
    virtual void move ();                       //called when moved
    virtual void init ();                       //called when created
    virtual void destroy ();                    //called when about to be destroyed
    
    virtual const char* debugName();
};

#endif /* Control_hpp */
