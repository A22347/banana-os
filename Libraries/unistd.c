#include <unistd.h>

unsigned int sleep (unsigned int time)			//sleep for a number of seconds
{
	int x = SystemCall (SC_Sleep, time * 1000, 0, 0);		//seconds to milli
	yield ();
	return x;
}

int usleep (useconds_t time)		//sleep for a number of microseconds
{
	int x = SystemCall (SC_Sleep, time / 1000, 0, 0);		//micro to milli
	yield ();	
	return x;
}

long sysconf (int option)
{
	return SystemCall (SC_Sysconf, option, 0, 0);
}

void _exit (int code)
{
	exit (code);
}

//yield() is now a macro in syscall.h
void realyield ()
{
	printf ("yielding...");
	asm volatile ("int $32");
}


int dup (int fildes)
{
	return fcntl (fildes, F_DUPFD, 0);
}

int dup2 (int fildes, int fildes2)
{
	close (fildes);
	return fcntl (fildes, F_DUPFD, fildes2);
}

pid_t getpid ()
{
	return SystemCall (SC_GetPID, 0, 0, 0);
}

pid_t gettid ()
{
	//generate a unique ID based on the PID and process specific TID
	return SystemCall (SC_GetPID, 0, 0, 0) * 256 + SystemCall (SC_GetPID, 1, 0, 0);
}

int close (int fd)
{
	return fclose (&fd);
}

ssize_t read (int fd, void* buf, size_t size)
{
	return fread (buf, size, 1, &fd);
}

ssize_t write (int fd, const void* buf, size_t size)
{
	return fwrite (buf, size, 1, &fd);
}

off_t lseek (int fd, off_t offset, int whence)
{
	fseek (&fd, offset, whence);
}

int unlink (const char* filename)
{
	return remove (filename);
}

int chdir (const char *path)
{
	return SystemCall (SC_SetCwd, 0, 0, (void*) path);
}

char* getcwd (char* buffer, size_t size)
{
	char* x = (char*) SystemCall (SC_GetCwd, 0, 0, 0);

	if (buffer) {
		memset (buffer, 0, size);
		memcpy (buffer, x, size - 1);

		if (buffer[0] < 'A') {
			buffer[0] += 'A' - '0';				// 0:/ -> A:/, 2:/ -> C:/
		}
	}

	if (x[0] < 'A') {
		x[0] += 'A' - '0';
	}

	return x;
}

char* getwd (char* buffer)
{
	size_t len = strlen ((char*) SystemCall (SC_GetCwd, 0, 0, 0));
	buffer = malloc (len + 1);
	memset (buffer, 0, len + 1);
	memcpy (buffer, (char*) SystemCall (SC_GetCwd, 0, 0, 0), len);

	return buffer;
}

char* get_current_dir_name ()
{
	size_t len = strlen ((char*) SystemCall (SC_GetCwd, 0, 0, 0));
	char* buffer = malloc (len + 1);
	memset (buffer, 0, len + 1);
	memcpy (buffer, (char*) SystemCall (SC_GetCwd, 0, 0, 0), len);
	return buffer;
}

int truncate (const char* filename, off_t byte)
{
	return SystemCall (SC_TruncateFile, (int) filename, 200, &byte);
}

int ftruncate (int file, off_t byte)
{
	return SystemCall (SC_TruncateFile, file, 100, &byte);
}