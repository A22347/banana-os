#pragma once

#include <stdint.h>
#include <core/objectmgr.h>

//pointers to built-in device driver objects
extern InternalObjectID builtinDriverGlobalObjectIDPointer_VGA;
extern InternalObjectID builtinDriverGlobalObjectIDPointer_VBE;
extern InternalObjectID builtinDriverGlobalObjectIDPointer_BGA;

//this will eventually use the 'new device subsystem' (C++)
//this means we can use an InternalObjectID to access a driver
//e.g.
//	driverRead(id, data);				//in C (display.c)
//	Object::getObject(id)->read(data);	//in C++ 

//this stuff works only for display 0, the rest must use displayData_t (and ideally even display 0)
extern unsigned short TOTAL_HEIGHT;	// VISIBLE SCREEN HEIGHT
extern unsigned short ONE_LINE;		// TOTAL PER SCANLINE
extern unsigned short TOTAL_WIDTH;	// VISIBLE SCREEN WIDTH
extern unsigned int BITS_PIXEL;
extern uint8_t BYTES_PER_PIXEL;

typedef struct displayData_t
{
	uint32_t visibleHeight;
	uint32_t visibleWidth;

	uint32_t totalHeight;
	uint32_t totalWidth;

	InternalObjectID driverID;

	char displayName[15];
	uint8_t bytesPerPixel;

} displayData_t;

extern displayData_t displays[256];
extern int nextDisplay;

//depending on the hardware, this MAY NOT BE A DIRECT COPY (e.g. cannot memcpy 24bpp into 8bpp, manual copying required)
void displayCopyBufferTo (uint32_t* buffer, uint32_t start, uint32_t size, uint8_t displayNo);
void displayCopyBufferFrom (uint32_t* buffer, uint32_t start, uint32_t size, uint8_t displayNo);

void displaySetPixel (uint32_t location, uint32_t colour, uint8_t displayNo);
uint32_t displayGetPixel (uint32_t location, uint8_t displayNo);

void displayInit ();
