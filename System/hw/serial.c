#include <hw/serial.h>

//TODO: Add COM2, COM3 and COM4 support
//Addresses are found in the BIOS Data Area (also used for detection of serial ports)
//		https://wiki.osdev.org/Memory_Map_(x86)#BIOS_Data_Area_.28BDA.29
//In case you get weird values the defaults are here:
//		https://wiki.osdev.org/Serial_Ports#Port_Addresses

//this sort of stuff will go into a devss class eventually
uint16_t comPortBases[4] = { 0x3F8, 0x2F8, 0x3E8, 0x2E8 };
int comBaudRates[4];

void comSetBaudRate (int portNumber, int baud)
{
	uint16_t data = (uint16_t) ((int) 115200 / baud);
	outb (comPortBases[portNumber] + 0, data & 0xFF);
	outb (comPortBases[portNumber] + 1, data >> 8);

	comBaudRates[portNumber] = baud;
}

void serialInit ()
{
	//turns out this doesn't detect it well (the scratch register probably isn't being used correctly)
	/*
	//detection only works on legacy BIOS systems
	uint16_t* biosDataArea = (uint16_t*) (size_t) 0x400;
	
	for (int i = 0; i < 4; ++i) {
		//test if the port works/exists by writing to the 'scratch register'
		outb (comPortBases[i] + 7, 0x55);

		//check if the value is the same or different
		if (inb (comPortBases[i]) != 0x55) {
			//if it didn't work, check the BIOS's port address
			comPortBases[i] = *(biosDataArea + i);

			//test again
			outb (comPortBases[i] + 7, 0x77);
			if (inb (comPortBases[i]) != 0x77) {
				//if it didn't work, the port doesn't exist
				comPortBases[i] = 0;
			}
		}
	}
	*/

	//initialise all working ports
	for (int i = 0; i < 4; ++i) {
		if (comPortBases[i] == 0) continue;

		outb (comPortBases[i] + 1, 0x00);
		outb (comPortBases[i] + 3, 0x80);
		comSetBaudRate (i, 38400);
		outb (comPortBases[i] + 3, 0x03);
		outb (comPortBases[i] + 2, 0xC7);
		outb (comPortBases[i] + 4, 0x0B);
	}
}

bool serialIsTransmitEmpty (uint8_t portNo)
{
	return inb (comPortBases[portNo] + 5) & 0x20;
}

void serialWriteChar (uint8_t portNo, char data)
{
	if (comPortBases[portNo] == 0) return;

	while (serialIsTransmitEmpty (portNo) == 0);
	outb (comPortBases[portNo], data);
}

void serialWrite (uint8_t portNo, void* data, uint32_t length)
{
	for (uint32_t i = 0; i < length; ++i) {
		serialWriteChar (portNo, ((uint8_t*) data)[i]);
	}
}

void com1_putchar (char a)
{
	serialWriteChar (0, a);
}

