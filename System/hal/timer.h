#pragma once

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include <core/kernel.h>
#include <hw/pit.h>
#include <hw/apictimer.h>

#define MAX_ALARMS 256

	void timer_handler (struct regs *r);

	typedef uint32_t alarmID;
	typedef void (*alarmHandler) (alarmID);

	void setupTimers (unsigned hertz, unsigned intNo);
	alarmID setupAlarm (int64_t milliseconds, alarmHandler handler);

	extern uint64_t toticks;
	extern uint64_t totalMillisecs;

#ifdef __cplusplus
}
#endif