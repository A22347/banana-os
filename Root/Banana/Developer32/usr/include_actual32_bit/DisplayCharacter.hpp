//
//  DisplayCharacter.hpp
//  Banana API (New)
//
//  Created by Alex Boxall on 21/9/18.
//  Copyright © 2018 Alex Boxall. All rights reserved.
//

#ifndef DisplayCharacter_hpp
#define DisplayCharacter_hpp

#include <stdint.h>

/*! \file DisplayCharacter.hpp
 \brief A header file defining the DisplayCharacter class.
 */

/*!
 \class DisplayCharacter
 \brief A class which contains data in order to display a formatted character on the screen.
 
 The DisplayCharacter class contains all the formatting data for a single character on the screen.
 It contains data such as the font family, size, bold, italic, underline + styles, strikethroughs
 subscripts, superscripts, colours and highlights, among other things. It is used internally by
 all Views that require rendering text on the screen, such as labels and text boxes.
 
 DisplayCharacters can be combined with paragraph and page objects in order to create a complete document.
 */

class DisplayCharacter {
protected:
public:
    //around 61 bytes in full
    struct BinaryOutput {
        uint16_t characterLow           : 16;
        uint8_t characterHigh           :  5;
        uint8_t objectType              :  2;       //always 0
                                                    //0 = this
                                                    //1 = paragraph object (e.g. justify, columns)
                                                    //2 = page object (e.g. margins, orientation)
                                                    //3 = other
        
        uint8_t containsFormatting      :  1;       //0 = just write these three bytes
                                                    //1 = write everything
        
        uint8_t numberStyle             :  3;       //see Word's number style options
        uint8_t shadowDirection         :  4;       //0 = TL, 1 = T, 2 = TR, 3 = L, 4 = C, 5 = R, 6 = BL, 7=B, 8=BR
        uint8_t shadowOn                :  1;
        
        uint32_t subOrSuperscriptSize   :  7;       //in percentage of original size
        uint32_t subscriptPosition      :  7;       //in percentage of character height
                                                    //20% for a character of height 72
                                                    //72*0.2 = 14.4. 14.4 is subtracted from baseline
        uint32_t superscriptPosition    :  7;       //same as above, but ABOVE baseline
        uint32_t overline               :  1;       //uses patterns below
        uint32_t encircled              :  1;
        uint32_t hzStretchDirection     :  1;       //0 = thinner, 1 = wider
        uint32_t vtStretchDirection     :  1;       //0 = shorter, 1 = taller
        uint32_t slantDirection         :  1;       //0 = left, 1 = right
        uint32_t subscript              :  1;       //uses offsets and sizes below
        uint32_t superscript            :  1;       //uses offsets and sizes below
        uint32_t strikethrough          :  1;       //uses patterns below
        uint32_t underline              :  1;       //uses patterns below
        uint32_t underlineUsePattern    :  1;       //0 = normal underline, 1 = use pattern
        uint32_t strikeUsePattern       :  1;       //0 = normal strike, 1 = use pattern
        
        uint32_t underlinePosition      :  7;       //in percentage, similar to sub/super script
        uint32_t overlinePosition       :  7;       //in percentage, similar to sub/super script
        uint32_t strikePosition         :  7;       //in percentage, similar to sub/super script
        uint32_t bold                   :  2;       //0 = none, 1 = semibold, 2 = bold, 3 = extra bold
        uint32_t italic                 :  2;       //0 = none, 1 = right, 2 = left
        uint32_t slant                  :  7;       //represents 1 degree each from (0 - 90)
        
        uint32_t bgColour               : 32;       //in ARGB
        uint32_t fgColour               : 32;       //in ARGB
        
        uint8_t underlinePattern         [5];       //8*5 repeated bitmap, used to create underline     patterns
        uint8_t overlinePattern          [5];       //8*5 repeated bitmap, used to create overline      patterns
        uint8_t strikePattern            [5];       //8*5 repeated bitmap, used to create strikethrough patterns
        uint8_t overlineUsePattern      :  1;       //0 = normal overline, 1 = use pattern
        uint8_t smallCaps               :  1;
        uint8_t ignoreSpellCheck        :  1;
        uint8_t useUnderlineOffsets     :  1;       //0 = use default pos, 1 = use specified
        uint8_t useOverlineOffsets      :  1;       //0 = use default pos, 1 = use specified
        uint8_t useStrikeOffsets        :  1;       //0 = use default pos, 1 = use specified
        uint8_t useSubscriptOffsets     :  1;       //0 = use default pos + size, 1 = use specified
        uint8_t useSuperscriptOffsets   :  1;       //0 = use default pos + size, 1 = use specified
        
        uint32_t underlineColour        : 32;       //in ARGB
        uint32_t overlineColour         : 32;       //in ARGB
        uint32_t strikeColour           : 32;       //in ARGB

        uint64_t fontSize               : 24;       //in 0.01 pts
        uint64_t fontFamilyInternalID   : 20;       //an internal font family ID (index into Banana's structures)
        uint64_t internalFontValue      :  4;       //used to help the above, as Banana has two lookup tables
                                                    //(one for bitmap and one for TrueType)
                                                    //(4 bits so it can be extended, e.g. OpenType)
        uint64_t implementationDefLen   : 16;
        
        //encircled options (e.g. colour)
        //phonetics (e.g. with kanji)
        //combine characters
        //etc.
        
        uint32_t textBorderColour      : 32;               //in ARGB
        
        uint32_t textBorderStyle       :  6;
        uint32_t textBorderWidth       : 10;               //in 0.05 pts
        uint32_t textBorderTop         :  1;
        uint32_t textBorderLeft        :  1;
        uint32_t textBorderRight       :  1;
        uint32_t textBorderBottom      :  1;
        uint32_t encircleType          :  2;        //circle, square, triangle, diamond
        uint32_t encircleMode          :  1;        //0 = shrink text, 1 = enlarge symbol
        uint32_t characterShading      :  1;
        uint32_t encircleColour        :  6;        //2 bits per channel
        uint32_t specialCharacter      :  1;
        
        uint32_t specialCharacterType  :  4;        //e.g. page number, line number, current time, document name
        uint32_t dropCapMode           :  2;        //0 = none, 1 = dropped, 2 = in margin
        uint32_t linesToDrop           :  4;
        uint32_t distanceFromText      :  9;        //in 0.01 points from 0-5
        uint32_t engrave               :  1;
        uint32_t emboss                :  1;
        uint32_t                       : 11;

        uint8_t ligatureData             [8];       //TODO
        uint8_t implementationDefined    [8];       //implementationDefLen
                                                    //used for things like hyperlink addresses + alt text
    };
   
    BinaryOutput outputData;
    
public:
    DisplayCharacter ();
    DisplayCharacter (uint8_t* binaryInput, uint8_t binaryLength);  ///>Turns a binary input into a DisplayCharacter
    
    void displayOnScreen (int x, int y);                    ///>Relative to parent's position
    uint8_t* getBinaryRepresenation (uint8_t* outputLength); ///>Returns a binary object to write to the filesystem
    void createFromBinaryRepresenation (uint8_t* binaryInput, uint8_t binaryLength);    ///>Opposite of above
    
};


#endif /* DisplayCharacter_hpp */
