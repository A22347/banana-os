#ifndef __SYS_WAIT_H__
#define __SYS_WAIT_H__

#include <sys/types.h>

pid_t wait (int* returnValue);
pid_t waitpid (pid_t process, int* returnValue, int options);

#endif