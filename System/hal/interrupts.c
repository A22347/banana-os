#include <hal/interrupts.h>
#include <core/cpuid.h>
#include <debug/pinpoint.h>
#include <core/scheduler.h>
#include <hw/acpi.h>

extern void idt_load ();
extern void _isr0 ();
extern void _isr1 ();
extern void _isr2 ();
extern void _isr3 ();
extern void _isr4 ();
extern void _isr5 ();
extern void _isr6 ();
extern void _isr7 ();
extern void _isr8 ();
extern void _isr9 ();
extern void _isr10 ();
extern void _isr11 ();
extern void _isr12 ();
extern void _isr13 ();
extern void _isr14 ();
extern void _isr15 ();
extern void _isr16 ();
extern void _isr17 ();
extern void _isr18 ();
extern void isr96 ();
extern void gobackhome ();

extern void irq0 ();
extern void irq1 ();
extern void irq2 ();
extern void irq3 ();
extern void irq4 ();
extern void irq5 ();
extern void irq6 ();
extern void irq7 ();
extern void irq8 ();
extern void irq9 ();
extern void irq10 ();
extern void irq11 ();
extern void irq12 ();
extern void irq13 ();
extern void irq14 ();
extern void irq15 ();
extern void irq16 ();
extern void irq17 ();
extern void irq18 ();
extern void irq19 ();
extern void irq20 ();
extern void irq21 ();
extern void irq22 ();
extern void irq23 ();

extern void yieldFromKernel ();		//this WILL return at some point in the near future, but after a task switch rotation

void setupInterrupts () {
	setupIDT ();
	setupISRs ();
	setupIRQs ();
}

void io_wait ()
{
	asm volatile ("outb %%al, $0x80"::"a" (0));
}

struct idt_entry idt[256];
struct idt_ptr idtp;

void idt_set_gate (unsigned char num, unsigned long base)
{
	idt[num].base_lo = (base & 0xFFFF);
	idt[num].base_hi = (base >> 16) & 0xFFFF;
	idt[num].sel = 0x08;
	idt[num].always0 = 0;
	idt[num].flags = 0x8E;
}

void setupIDT ()
{
	idtp.limit = (sizeof (struct idt_entry) * 256) - 1;
	idtp.base = (size_t) &idt;
	memset (&idt, 0, sizeof (struct idt_entry) * 256);
	idt_load ();
}

extern void _isr0 ();
extern void _isr1 ();
extern void _isr2 ();
extern void _isr3 ();
extern void _isr4 ();
extern void _isr5 ();
extern void _isr6 ();
extern void _isr7 ();
extern void _isr8 ();
extern void _isr9 ();
extern void _isr10 ();
extern void _isr11 ();
extern void _isr12 ();
extern void _isr13 ();
extern void _isr14 ();
extern void _isr15 ();
extern void _isr16 ();
extern void _isr17 ();
extern void _isr18 ();
extern void isr96 ();
extern void gobackhome ();

extern void apic_timer_setup ();

void setupISRs ()
{
	size_t a, b, c, d;
	cpuid (1, &a, &b, &c, &d);
	hasAPIC = (d >> 9) & 1;

	logk ("The system %s APIC\n", hasAPIC ? "has an" : "doesn't have an");

	if (safeMode || forceDisableAPIC) {
		logk ("NOT USING THE APIC\n");
		hasAPIC = false;
	}

	setupInterruptChip ();

	idt_set_gate (0, (size_t) _isr0);
	idt_set_gate (1, (size_t) _isr1);
	idt_set_gate (2, (size_t) _isr2);
	idt_set_gate (3, (size_t) _isr3);
	idt_set_gate (4, (size_t) _isr4);
	idt_set_gate (5, (size_t) _isr5);
	idt_set_gate (6, (size_t) _isr6);
	idt_set_gate (7, (size_t) _isr7);
	idt_set_gate (8, (size_t) _isr8);
	idt_set_gate (9, (size_t) _isr9);
	idt_set_gate (10, (size_t) _isr10);
	idt_set_gate (11, (size_t) _isr11);
	idt_set_gate (12, (size_t) _isr12);
	idt_set_gate (13, (size_t) _isr13);
	idt_set_gate (14, (size_t) _isr14);
	idt_set_gate (15, (size_t) _isr15);
	idt_set_gate (16, (size_t) _isr16);
	idt_set_gate (17, (size_t) _isr17);
	idt_set_gate (18, (size_t) _isr18);
	idt_set_gate (96, (size_t) isr96);      //96 is 0x60
	idt[1].flags = 0xEE;     //we can call this from user mode (debug int)
	idt[3].flags = 0xEE;     //we can call this from user mode (debug int)
	idt[96].flags = 0xEE;    //we can call this from user mode
	setupSystemCalls ();
}



unsigned long read_cr2 ()
{
	unsigned long val;
	asm volatile (".att_syntax prefix");    //just to make sure
	asm volatile ("mov %%cr2, %0" : "=r"(val));
	return val;
}

int numberOfSameType = 0;
int theType = 0;

char errorCodes[32][30] = {
	"DIVISION BY ZERO",
	"DEBUG",
	"NMI",
	"BREAKPOINT",
	"OVERFLOW",
	"BOUND RANGE EXCEEDED",
	"INVALID OPCODE",
	"DEVICE NOT AVALIABLE",
	"DOUBLE FAULT",
	"COPROCESSOR SEGMENT OVERRUN",
	"INVALID TSS",
	"SEGMENT NOT PRESENT",
	"STACK-SEGMENT FAULT",
	"GENERAL PROTECTION FAULT",
	"PAGE FAULT",
	"(RESERVED EXCEPTION 15)",
	"x87 FLOATING POINT EXCEPTION",
	"ALIGNMENT CHECK",
	"MACHINE CHECK",
	"SIMD FLOATING POINT EXCEPTION",
	"VIRTUALISATION EXCEPTION",
	"(RESERVED EXCEPTION 21)",
	"(RESERVED EXCEPTION 22)",
	"(RESERVED EXCEPTION 23)",
	"(RESERVED EXCEPTION 24)",
	"(RESERVED EXCEPTION 25)",
	"(RESERVED EXCEPTION 26)",
	"(RESERVED EXCEPTION 27)",
	"(RESERVED EXCEPTION 28)",
	"(RESERVED EXCEPTION 29)",
	"SECURITY EXCEPTION",
	"(RESERVED EXCEPTION 31)"
};

extern uint32_t tasknos;
extern uint8_t doNotSendEIOThisTime;
extern uint8_t taskModeActive;

void fault_handler (struct regs *r)
{
	int OLD_TaskCurrent = TASK_Current;
	TASK_Current = PIDofEachCPU[getCPUNumber ()];

	if (unlikely (r->int_no > 15 && r->int_no != 96)) {
		logk("Fault number 0x%X\n", r->int_no);
		/*strcpy (panicErrorMessage, "HECK_KNOWS_WHAT_WENT_WRONG_THERE");
		panic ();*/
	}

	if (r->int_no != 96) {
		logk ("(CPU %d): Fault %d, Error code = 0x%X (%d), cr2 = 0x%X (%d)\
\n    eax = 0x%X (%d)\
\n    ebx = 0x%X (%d)\
\n    ecx = 0x%X (%d)\
\n    edx = 0x%X (%d)\
\n    esi = 0x%X (%d)\
\n    edi = 0x%X (%d)\
\n    esp = 0x%X (%d)\
\n    ebp = 0x%X (%d)\
\n    eip = 0x%X (%d)\n", getCPUNumber (), r->int_no, r->err_code, r->err_code, read_cr2(), read_cr2(), \
r->eax, r->eax, r->ebx, r->ebx,
r->ecx, r->ecx, r->edx, r->edx,
r->esi, r->esi, r->edi, r->edi,
r->esp, r->esp, r->ebp, r->ebp,
r->eip, r->eip);

#if PLATFORM_ID == 64
		uint64_t cr0, cr2, cr3;
		__asm__ __volatile__(
			"mov %%cr0, %%rax\n\t"
			"mov %%eax, %0\n\t"
			"mov %%cr2, %%rax\n\t"
			"mov %%eax, %1\n\t"
			"mov %%cr3, %%rax\n\t"
			"mov %%eax, %2\n\t"
			: "=m" (cr0), "=m" (cr2), "=m" (cr3)
			: /* no input */
			: "%rax"
		);
		logk("CR2 = 0x%X (%d)\n", cr2);
#elif PLATFORM_ID == 86
		uint32_t cr0, cr2, cr3;
		__asm__ __volatile__(
			"mov %%cr0, %%eax\n\t"
			"mov %%eax, %0\n\t"
			"mov %%cr2, %%eax\n\t"
			"mov %%eax, %1\n\t"
			"mov %%cr3, %%eax\n\t"
			"mov %%eax, %2\n\t"
			: "=m" (cr0), "=m" (cr2), "=m" (cr3)
			: /* no input */
			: "%eax"
		);
#endif


		//pinpoint (r);

	} else {
		numberOfSameType = 0;
	}

	if (r->int_no == (size_t) theType) {
		++numberOfSameType;
		if (numberOfSameType > 10) {
			sprintf (panicErrorMessage, "SYSTEM_HAS_LOCKED_UP_0x%X", (unsigned int) r->int_no);
			panic ();
		}
	} else {
		theType = r->int_no;
		numberOfSameType = 0;
	}

	if (likely (r->int_no == 96)) {
		//logk ("System Call %d by Task %d. 0x%X 0x%X 0x%X\n", (unsigned int) r->eax, (unsigned int) TASK_Current, r->ebx, r->ecx, r->edx);
		system_call (r);

	} else if (r->int_no == 8) {
		memcpy (panicErrorMessage, "DOUBLE_FAULT", 13);
		panic ();

	/*} else if (r->int_no == 14) {
		//page fault
		*/
	} else if (r->int_no <= 31 && taskModeActive) {
		logk ("Seems like a good time to stop a task.\n");
		logk ("We're going to yield now.\n");

		if (TASKS[TASK_Current].console) {
			filePipeStdoutWrite (TASKS[TASK_Current].stdout, strlen("* TASK CRASH *\n"), (uint8_t*) "* TASK CRASH *\n");
		}

		yieldFromKernel ();
		return;
	}

	if (!strcmp (appOrDriverRunningNameInCaseOfCrash, "C:/BANANA/SYSTEM/CRASHMSG.EXE")) {
		strcpy (panicErrorMessage, "FEI_CHANG_HAO_YOU_BRICKED_IT");
		panic ();
	}

	TASK_Current = OLD_TaskCurrent;
}

extern void irq0 ();
extern void irq1 ();
extern void irq2 ();
extern void irq3 ();
extern void irq4 ();
extern void irq5 ();
extern void irq6 ();
extern void irq7 ();
extern void irq8 ();
extern void irq9 ();
extern void irq10 ();
extern void irq11 ();
extern void irq12 ();
extern void irq13 ();
extern void irq14 ();
extern void irq15 ();
extern void irq16 ();
extern void irq17 ();
extern void irq18 ();
extern void irq19 ();
extern void irq20 ();
extern void irq21 ();
extern void irq22 ();
extern void irq23 ();

void installHandlerInternal (int irq, interruptHandler handler)
{
	logk("PIC IRQ 0x%X 0x%X->0x%X\n", irq, irq_routines[irq], handler);
	irq_routines[irq] = handler;
}

void setupIRQs ()
{
	for (int i = 0; i < 24; ++i) {
		installHandlerInternal(i, 0);
	}

	CRASH_DEBUG
	idt_set_gate (32, (size_t) irq0);		//timer_int
	idt_set_gate (33, (size_t) irq1);
	idt_set_gate (34, (size_t) irq2);
	idt_set_gate (35, (size_t) irq3);
	idt_set_gate (36, (size_t) irq4);
	idt_set_gate (37, (size_t) irq5);
	idt_set_gate (38, (size_t) irq6);
	idt_set_gate (39, (size_t) irq7);
	idt_set_gate (40, (size_t) irq8);
	idt_set_gate (41, (size_t) irq9);
	idt_set_gate (42, (size_t) irq10);
	idt_set_gate (43, (size_t) irq11);
	idt_set_gate (44, (size_t) irq12);
	idt_set_gate (45, (size_t) irq13);
	idt_set_gate (46, (size_t) irq14);
	idt_set_gate (47, (size_t) irq15);

	CRASH_DEBUG

		if (hasAPIC) {
			idt_set_gate (48, (size_t) irq16);
			idt_set_gate (49, (size_t) irq17);
			idt_set_gate (50, (size_t) irq18);
			idt_set_gate (51, (size_t) irq19);
			idt_set_gate (52, (size_t) irq20);
			idt_set_gate (53, (size_t) irq21);
			idt_set_gate (54, (size_t) irq22);
			idt_set_gate (55, (size_t) irq23);
		} else {
		}

	CRASH_DEBUG

		//because the timer interrupt has a 'special' handler
		//allow the 'timer_int' handler to be on the irq that actually handles the timer
		//idt_set_gate (32 + legacyIRQRemaps[0], (size_t) timer_int);

	idt[32].flags = 0xEE;					 //we can call this from user mode (yield)

	CRASH_DEBUG
}

void setupInterruptChip ()
{
	initDriverInterrupts ();

	if (hasAPIC) {
		disablePIC ();
		enableAPIC ();

	} else {
		remapPIC (32, 40);
	}

}

void sendEOI (int intNo)
{
	if (!hasAPIC) {
		sendEOIForPIC (intNo);
	} else {
		sendEOIForAPIC ();
	}
}

//there's got to be a way where we can have more than one handler
//per interrupt, and allow it to uninstall the correct one
//(maybe return an ID (array index) from installHardwareInt..., 
// and use that to pick the right one in the array to remove...)
void installHardwareInterruptHandler (uint8_t number, interruptHandler handler)
{
	logk("hasAPIC = %d\n", hasAPIC);
	if (hasAPIC) {
		ioapicSetIRQ (number, getCPUNumber (), number + 32, ioapicAddresses[matchingAPICID[getCPUNumber ()]]);
	}
	installHandlerInternal (number, handler);
}

void uninstallHardwareInterruptHandler (uint8_t n)
{
	irq_routines[n] = 0;
}

void printEIP ()
{
	logk ("About to return to task %d at address 0x%X\n", TASK_Current, st_eip);
}

void irq_handler (struct regs *r)
{
	int OLD_TaskCurrent = TASK_Current;
	TASK_Current = PIDofEachCPU[getCPUNumber ()];

	int intno = r->int_no;
	interruptHandler handler = irq_routines[r->int_no - 32];
	logk("IRQ %d SENT!\n", intno - 32);

	if (intno != 32) {
		lastInt = 0;
	}
	if (handler) {
		handler (r);
	}

	sendInterruptToDrivers (r->int_no - 32);

	if (unlikely (r->err_code == 99999)) {
		TASK_Current = OLD_TaskCurrent;
		return;
	}

	sendEOI (r->int_no);	

	TASK_Current = OLD_TaskCurrent;
}

DriverInterruptHandler driverInterruptHandlers[24][MAX_DRIVER_INT_SLOTS];
int nextDriverInterruptHandler[24];

void installHardwareInterruptHandlerFromDriver (uint8_t number, int driverPID, interruptHandler handler) {
	int slot = nextDriverInterruptHandler[number];

	if (slot >= MAX_DRIVER_INT_SLOTS) {
		logk ("NO MORE INTERRUPT HANDLER SLOTS AVALIABLE!\n");
		return;
	}

	driverInterruptHandlers[number][slot].driverPID = driverPID;
	driverInterruptHandlers[number][slot].handler = handler;
	nextDriverInterruptHandler[number]++;
}

void uninstallHardwareInterruptHandlerFromDriver (uint8_t number, int driverPID) {
	for (int i = 0; i < MAX_DRIVER_INT_SLOTS; ++i) {
		if (driverInterruptHandlers[number][i].driverPID == driverPID) {
			driverInterruptHandlers[number][i].driverPID = 0;
			driverInterruptHandlers[number][i].handler = 0;
			break;
		}
	}
}

void initDriverInterrupts () {
	memset (driverInterruptHandlers, 0, sizeof (driverInterruptHandlers));
	memset (nextDriverInterruptHandler, 0, sizeof (nextDriverInterruptHandler));
}

void sendInterruptToDrivers (int intno) {
	for (int i = 0; i < MAX_DRIVER_INT_SLOTS; ++i) {
		if (driverInterruptHandlers[intno][i].handler && driverInterruptHandlers[intno][i].driverPID) {
			//execute the driver
		}
	}
}