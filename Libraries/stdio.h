#ifndef __STDIO_H__
#define __STDIO_H__

#ifdef __cplusplus
extern "C" {
#endif

	extern char* leftovers;
	extern unsigned int leftoverPointer;

	extern char* virtualFileStreamAddon;
	extern int virtualFileStreamAddonPointer;

#include <syscall.h>
#include <string.h>
#include <stdarg.h>
#include <limits.h>
#include <stdlib.h>
#include <ctype.h>

	typedef unsigned long long FILE;
	typedef unsigned long long fpos_t;

	extern FILE* stdin;
	extern FILE* stdout;
	extern FILE* stderr;

	extern bool autoFlush;

#define EOF_CHARACTER '\x1A'
#define FILENAME_MAX 255
#define FOPEN_MAX 20
#define TMP_MAX 20
#define EOF 0x7FFFFFFF
#define L_tmpnam FILENAME_MAX
#define SEEK_END 3
#define SEEK_CUR 2
#define SEEK_SET 1
#define BUFSIZ 65536
#define OPEN_MAX 1023			//maximum open files
#define FOPEN_MAX OPEN_MAX		//maximum open files
#define NAME_MAX 255			//filename length

#define _IOFBF 0
#define _IOLBF 1
#define _IONBF 2

	bool __setcolour (uint32_t bg, uint32_t fg);
	bool __resetcolour ();

	int putchar (int c);
	int puts (const char* c);
	int printf (const char* format, ...);
	int fprintf (FILE* file, const char* format, ...);
	int vprintf (const char* format, va_list args);
	int vsprintf (char* buffer, const char* format, va_list);
	int vfprintf (FILE* file, const char* format, va_list list);
	int sprintf (char* buffer, const char* format, ...);

	int fflush (FILE* stream);
	int ungetc (int chr, FILE *stream);

	char* fgets (char *str, int n, FILE *stream);
	char* gets (char *str);

	int scanf (const char* format, ...);
	char* gets_s (char *s, size_t n);
	int getchar ();

	FILE* fopen (const char* filename, const char* mode);
	FILE* fdopen (int fildes, const char *mode);
	FILE* freopen (const char* filename, const char* mode, FILE* stream);
	size_t fread (void* ptr, size_t size, size_t count, FILE* file);
	size_t fwrite (const void* ptr, size_t size, size_t count, FILE* file);
	int fclose (FILE* file);

	int fputc (int c, FILE* stream);
	int putc (int c, FILE* stream);
	int getc (FILE* stream);
	int fgetc (FILE* stream);

	int fputs (const char* s, FILE* stream);

	int remove (const char* filename);
	int rename (const char* old, const char* newn);

	int fseek (FILE* stream, long offset, int origin);
	long ftell (FILE* stream);
	void rewind (FILE* stream);
	int fgetpos (FILE* stream, fpos_t* ptr);
	int fsetpos (FILE* stream, const fpos_t* ptr);
	int feof (FILE* stream);
	int ferror (FILE* stream);
	void clearerr (FILE* stream);

	char* tmpnam (char s[L_tmpnam]);
	FILE* tmpfile ();

	void perror (const char *str);
	int fileno (FILE *stream);

	int sscanf (const char* s, const char* format, ...);
	int vsscanf (const char* s, const char* format, va_list ap);

	int scanf (const char* format, ...);
	int vscanf (const char* format, va_list ap);

	int vfscanf (FILE* stream, const char* format, va_list args);
	int fscanf (FILE* stream, const char* format, ...);

	void setbuf (FILE* stream, char* buffer);
	int setvbuf (FILE* stream, char* buffer, int mode, size_t size);

#ifdef __cplusplus
}
#endif

#endif