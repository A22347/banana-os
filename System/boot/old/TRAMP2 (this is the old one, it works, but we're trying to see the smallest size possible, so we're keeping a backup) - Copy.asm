org 0x8000
bits 16
jmp main

main:
	cli													; We don't want interrupts ATM
	mov ax, 0										; We're at 0000:7c000, so set our segment registers to that
	mov ds, ax
	mov es, ax
	mov fs, ax
	mov gs, ax

	xor ax, ax											; Set our stack to 0x0000 and create it
	mov ss, ax
	xor sp, sp			;it'll loop round! to 0xFFFF

	;call enable_A20
	
	;https://forum.osdev.org/viewtopic.php?f=1&t=21364&start=0
	
	push	ax
	mov	al, 0xdd	; send enable a20 address line command to controller
	out	0x64, al
	pop	ax
	
	mov eax, GDT64Pointer
	lgdt [eax]
	
	;mov eax, [0x7D04]
	;lgdt [eax]    ;our a pointer to our main GDT pointer is in 0x7F04
	
	mov ax, 0x10
    mov ds, ax
    mov es, ax
    mov fs, ax
    mov gs, ax
    mov ss, ax
    jmp flush2
	flush2:
	
	mov eax, [0x7D08]
	lidt [eax]
	
	;jmp looping
	
	;lidt here too...
	
	cli
	mov eax, cr0 
		or al, 1       ; set PE (Protection Enable) bit in CR0 (Control Register 0)
	mov cr0, eax
	
	nop
	nop
	nop
	nop
	nop
	nop
	nop

	
	;mov byte [0x7D00], 2
	;mov byte [0x7D01], 1

	;jmp $

	jmp 0x8:PModeMain
	jmp looping


looping:
	cli
	;hlt
	jmp looping
	
enable_A20:
        cli
 
        call    a20wait
        mov     al,0xAD
        out     0x64,al
 
        call    a20wait
        mov     al,0xD0
        out     0x64,al
 
        call    a20wait2
        in      al,0x60
        push    eax
 
        call    a20wait
        mov     al,0xD1
        out     0x64,al
 
        call    a20wait
        pop     eax
        or      al,2
        out     0x60,al
 
        call    a20wait
        mov     al,0xAE
        out     0x64,al
 
        call    a20wait
        ret
 
a20wait:
        in      al,0x64
        test    al,2
        jnz     a20wait
        ret
 
 
a20wait2:
        in      al,0x64
        test    al,1
        jz      a20wait2
        ret
		

GDT64:                           ; Global Descriptor Table (64-bit).
      dd      0  
	  dd 	  0
      dw     -1, 0, 0x9A00, 0xcf
      dw     -1, 0, 0x9200, 0xcf

GDT64Pointer:                    ; The GDT-pointer.
    dw $ - GDT64 - 1             ; Limit.
    dq GDT64                     ; Base.
	
	align 64	
bits 32
PModeMain:
	;cli
    ;mov	ax, 0x10		; set data segments to data selector (0x10)
    ;mov	ds, ax
    ;mov	ss, ax
    ;mov	es, ax
    ;mov	esp, 0x10000  ;0x9FFFF0
    ;mov ebp, esp

	;jmp looping32
	jmp 0x8:0x400000
	
looping32:
	jmp looping32