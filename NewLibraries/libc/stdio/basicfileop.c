//
//  basicfileop.c
//  Banana libc
//
//  Created by Alex Boxall on 13/7/18.
//  Copyright © 2018 Alex Boxall. All rights reserved.
//

#ifndef _libk

#include "stdio.h"
#include "syscall.h"
#include "string.h"
#include "stdlib.h"

FILE* stderr;
FILE* stdin;
FILE* stdout;

FILE* __i_fopen_no_allocation (FILE* file, const char * restrict filename, const char* restrict mode) {
    unsigned long long fd = 0;
    
    unsigned char xmode = 0;
    
    _Bool read = 0, write = 0, exclusive = 0, append = 0, binary = 0, update = 0;;
    
    //look at the mode, set flags
    for (int i = 0; mode[i]; ++i) {
        switch (mode[i]) {
            case 'r':
                read = 1;
                break;
            case 'w':
                write = 1;
                break;
            case 'x':
                exclusive = 1;
                break;
            case 'a':
                append = 1;
                break;
            case 'b':
                binary = 1;
                break;
            case '+':
                update = 1;
                break;
        }
    }
    
    //create mode value
    if (update) {
        xmode |= __i_FILE_UPDATE;
        if (read) xmode |= __i_FILE_READ;
        else if (write) xmode |= __i_FILE_WRITE;        //this means OVERWRITE
        
        if (append) xmode |= __i_FILE_APPEND;
        if (binary) xmode |= __i_FILE_BINARY;
        
    } else {
        if (read) xmode |= __i_FILE_READ;
        else if (write) xmode |= __i_FILE_WRITE;
        
        if (append) xmode |= __i_FILE_APPEND;
        if (binary) xmode |= __i_FILE_BINARY;
        if (exclusive) xmode |= __i_FILE_NO_OVERWRITE;
    }
    
    //call system for object ID
    int ret = __i_system_call_openfile(&fd, filename, xmode);
    
    if (ret != 0) {
        free(file);
        return NULL;
    }
    
    //set values
    file->filename = malloc(FILENAME_MAX + 1);
    file->bufferSizeForThisFile = BUFSIZ;
    file->buffer = malloc(file->bufferSizeForThisFile);
    file->bufferPtr = 0;
    file->bufferMode = _IONBF;
    file->locked = 0;
    file->objectID = fd;
    file->openAndValid = 1;
    file->textStream = !(xmode & __i_FILE_BINARY);
    file->wideOriented = 0;
    file->filemode = xmode;
    file->lastOperation = __i_LASTOPERATION_NULL;
    file->errorIndicator = 0;
    file->seekable = 1;
    
    return file;
}

FILE *fopen(const char * restrict filename, const char * restrict mode) {
    FILE* file = malloc(sizeof(FILE));
    return __i_fopen_no_allocation(file, filename, mode);
}

FILE *freopen(const char * restrict filename, const char * restrict mode, \
                              FILE * restrict stream) {
    fclose(stream);
    stream->errorIndicator = 0;
    return __i_fopen_no_allocation(stream, filename, mode);
}

int fclose(FILE *stream) {
    if (stream->openAndValid) {
        fflush(stream);     //who cares if it fails
        free(stream);
        return 0;
    }
    return EOF;
}

int fflush(FILE *stream) {
    if (stream == NULL) {
        //flush all FILEs in existance that are write/update, non error and didn't have a read last time
        return EOF;
    }
    
    if (!stream->openAndValid)  {
        stream->errorIndicator = 1;
        return EOF;
    }
    
    //we cannot flush READ only streams
    if (!((stream->filemode & __i_FILE_WRITE) || (stream->filemode & __i_FILE_UPDATE))) {
        stream->errorIndicator = 1;
        return EOF;
    }
    
    //a file read must NOT be the last operation
    if (stream->lastOperation == __i_LASTOPERATION_READ) {
        stream->errorIndicator = 1;
        return EOF;
    }
   
    return __i_internalFileBufferFlushWrite(stream);
}

int setvbuf(FILE * restrict stream, char * restrict buf, int mode, size_t size) {
    if (!stream->openAndValid) {
        return EOF;
    }
    
    //must be the first operation done on the file
    if (stream->lastOperation != 2) {
        return EOF;
    }
    
    stream->bufferMode = mode;
    
    //flush data
    _Bool copyOfErrorIndicator = stream->errorIndicator;
    fflush(stream);
    stream->errorIndicator = copyOfErrorIndicator;
    
    if (buf != NULL) {
        //change buffer if not NULL
        free(stream->buffer);
        stream->buffer = (unsigned char*) buf;
    } else {
        //else, reallocate it to the correct size
        stream->buffer = realloc(stream->buffer, size);
    }
    stream->bufferSizeForThisFile = size;

    return EOF;
}

void setbuf(FILE * restrict stream, char * restrict buf) {
    //the standard says this function canns setvbuf with these arguments
    setvbuf(stream, buf, buf == NULL ? _IONBF : _IOFBF, BUFSIZ);
}

void clearerr(FILE *stream) {
    stream->errorIndicator = 0;
}

int ferror(FILE *stream) {
    return stream->errorIndicator;
}

int feof(FILE *stream) {
    return stream->eof;
}

int fputc(int c, FILE *stream) {
    if (!stream->openAndValid) {
        return EOF;
    }
    if (stream->wideOriented == 1) {
        return EOF;
    }
    
    stream->wideOriented = 0;
    
    char data[1];
    data[0] = (char) c;
    
    int ret = __i_internalFileWrite(stream, 1, (const unsigned char*) data);
    if (ret == EOF) {
        return EOF;
    }
    return c;
}

int fputs(const char * restrict s, FILE * restrict stream) {
    if (!stream->openAndValid) {
        return EOF;
    }
    if (stream->wideOriented == 1) {
        return EOF;
    }
    if (s == NULL || s[0] == 0) {
        return 0;
    }
    
    stream->wideOriented = 0;
    
    //no null terminator, so subtract a byte
    int ret = __i_internalFileWrite(stream, strlen(s) - 1, (const unsigned char*) s);
    if (ret == EOF) {
        return EOF;
    }
    return 0;
}

int fgetc(FILE *stream) {
    if (!stream->openAndValid) {
        return EOF;
    }
    if (stream->wideOriented == 1) {
        return EOF;
    }
    
    stream->wideOriented = 0;
    
    char data[1];
    
    int ret = __i_internalFileRead(stream, 1, (unsigned char*) data);
    if (ret == EOF) {
        //check if EOF was reached or if it was an error
        if (!stream->errorIndicator) stream->eof = 1;
        return EOF;
    }
    return data[0];
}

char *fgets(char * restrict s, int n, FILE * restrict stream) {
    for (int i = 0; i < n - 1 ; ++i) {
        int c = fgetc(stream);
        if (c == EOF) {
            //check if EOF was reached or if it was an error
            if (!stream->errorIndicator) stream->eof = 1;
            return NULL;
        }
        *(s + i) = (char) c;
        if (c == '\n') {
            break;
        }
    }
    return s;
}

#endif
