
// DELETE ALL SETBUF THINGS (bufferPointers, bufferPointerModes etc.) AND READ THIS: 
// https://stackoverflow.com/questions/17139094/what-is-the-point-of-using-the-setvbuf-function-in-c

#include <stdio.h>
#define	FA_READ				0x01
#define	FA_WRITE			0x02
#define	FA_OPEN_EXISTING	0x00
#define	FA_CREATE_NEW		0x04
#define	FA_CREATE_ALWAYS	0x08
#define	FA_OPEN_ALWAYS		0x10
#define	FA_OPEN_APPEND		0x30

FILE* stdin = (FILE*) 0;
FILE* stdout = (FILE*) 1;
FILE* stderr = (FILE*) 2;

const FILE* const stdin__ = (const FILE* const) 0;
const FILE* const stdout__ = (const FILE* const) 1;
const FILE* const stderr__ = (const FILE* const) 2;

uint64_t charsSinceLastStdoutFlush = 0;
uint64_t stdoutFlushCount = 0;
bool bufferStdout = false;
char* leftovers = 0;
unsigned int leftoverPointer = 0;

char* virtualFileStreamAddon = 0;
int virtualFileStreamAddonPointer = 0;

bool autoFlush = true;

void __print (const char* s)
{
	SystemCall (SC_Print, 0, 0, (char*) s);
}

void __putint (int i)
{
	char b[20];
	__print (__int_str (i, b));
}

char* stringifyWithBase (int i, char b[], int base)
{
	if (base > 62) {
		base = 62;
	}
	const char digit[] = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
	char* p = b;
	if (i < 0) {
		*p++ = '-';
		i *= -1;
	}
	int shifter = i;
	do { //Move to where representation ends
		++p;
		shifter = shifter / base;
	} while (shifter);
	*p = '\0';
	do { //Move back, inserting digits as u go
		*--p = digit[i % base];
		i = i / base;
	} while (i);
	return b;
}

void __putint_base (int i, int base)
{
	char b[100] = { 0 };
	__print (stringifyWithBase (i, b, base));
}

int putchar (int c)
{
	if (autoFlush) {
		SystemCall (SC_PrintChar, c, 0, 0);
	} else {
		SystemCall (SC_PrintChar, c, 77, 0);
	}
	++charsSinceLastStdoutFlush;

	if (charsSinceLastStdoutFlush > stdoutFlushCount && bufferStdout) {
		charsSinceLastStdoutFlush = 0;
		SystemCall (SC_StdinFlush, 0, 0, 0);
	}

	return c;
}

int puts (const char* c)
{
	SystemCall (SC_Print, 0, 0, (char*) c);
	SystemCall (SC_PrintChar, '\n', 0, 0);

	charsSinceLastStdoutFlush += strlen (c) + 1;

	return strlen (c) + 1;
}

bool NOECHO = false;

char grabAChar ()
{
	char cc = SystemCall (SC_GetNextPipelineCharacter, 0, 0, 0);
	if (cc) {
		NOECHO = true;
		return cc;
	}
	NOECHO = false;

	if (!leftovers) leftovers = calloc (BUFSIZ, 1);

	if (leftoverPointer) {
		char saved = leftovers[0];
		leftovers[--leftoverPointer] = 0;

		for (unsigned i = 0; i < leftoverPointer - 1; --i) {
			leftovers[i] = leftovers[i + 1];
		}
		return saved;
	}

	//SystemCall (SC_GetKeyIfSittingInBufferWaiting, 0, 0, 0);	//clear buffer
	char c = 0;
	while (true) {
		c = (char) SystemCall (SC_GetKeyIfSittingInBufferWaiting, 0, 0, 0);
		if (isprint (c) || c == '\b' || c == '\n') {
			break;
		}
	}
	return c;
}

int ungetc (int chr, FILE* file)
{
	if (!leftovers) leftovers = calloc (BUFSIZ, 1);
	if (!virtualFileStreamAddon) virtualFileStreamAddon = malloc (BUFSIZ);

	if (file == stdout__) {
		// ...

	} else if (file == stderr__) {

	} else if (file == stdin__) {
		leftovers[leftoverPointer++] = chr;

	} else {
		virtualFileStreamAddon[virtualFileStreamAddonPointer++] = chr;
	}
	return 0;
}

int getchar ()
{
	char buffer[1024] = { 0 };
	int bLoc = 0;
	char lastChar = 0;

	if (!leftovers) leftovers = calloc (BUFSIZ, 1);

	do {
		lastChar = grabAChar ();
		if (lastChar == '\b' && bLoc) {
			buffer[--bLoc] = 0;
		} else {
			if (bLoc < 1023) {
				buffer[bLoc++] = (char) lastChar;
			}
		}

		putchar ((char) lastChar);

	} while (lastChar != '\n');

	for (int i = 1; buffer[i]; ++i) {
		leftovers[leftoverPointer++] = buffer[i];
	}

	return buffer[0];
}

char* read_s (char* buffer, size_t n)
{
	size_t bLoc = 0;
	char lastChar = 0;
	bool hitLimit = false;
	bool typed = false;

	if (!leftovers) leftovers = calloc (BUFSIZ, 1);

	do {
		lastChar = grabAChar ();

		if (lastChar == EOF_CHARACTER) {
			break;
		}

		if (bLoc == 1 && lastChar == '\b' && !NOECHO) {
			putchar ((char) lastChar);
		}

		if (!hitLimit && lastChar != '\b') {
			buffer[bLoc++] = (char) lastChar;
			typed = true;
		} else if (lastChar == '\b' && bLoc) {
			buffer[--bLoc] = 0;
		}

		if (bLoc >= n) {
			hitLimit = true;
			if (lastChar != '\b') {
				leftovers[leftoverPointer++] = lastChar;
			} else {
				leftovers[--leftoverPointer] = 0;
			}
		}

		if (!NOECHO && (lastChar != '\b' || (lastChar == '\b' && bLoc))) {
			putchar ((char) lastChar);
		}

	} while (!(lastChar == '\n' && hitLimit));
	return buffer;
}

char* gets_s (char* buffer, size_t n)
{
	size_t bLoc = 0;
	char lastChar = 0;
	bool hitLimit = false;
	bool typed = false;

	if (!leftovers) leftovers = calloc (BUFSIZ, 1);

	do {
		lastChar = grabAChar ();

		if (lastChar == EOF_CHARACTER) {
			break;
		}

		if (bLoc == 1 && lastChar == '\b' && !NOECHO) {
			putchar ((char) lastChar);
		}

		if (!hitLimit && lastChar != '\b') {
			buffer[bLoc++] = (char) lastChar;
			typed = true;
		} else if (lastChar == '\b' && bLoc) {
			buffer[--bLoc] = 0;
		}

		if (bLoc >= n) {
			hitLimit = true;
			if (lastChar != '\b') {
				leftovers[leftoverPointer++] = lastChar;
			} else {
				leftovers[--leftoverPointer] = 0;
			}
		}

		if (!NOECHO && (lastChar != '\b' || (lastChar == '\b' && bLoc))) {
			putchar ((char) lastChar);
		}

	} while (lastChar != '\n');

	buffer[bLoc] = 0;		//the caller must compensate for the null char
	return buffer;
}


char* gets (char *str)
{
	printf ("The use of the `gets' function is dangerous. The program will abort at the next keypress... ");
	getchar ();
	abort ();
	return str;
}

char* A__format_str (char b[], bool spaceSignIfNeeded, int paddingNo, bool justify, bool zeroPad)
{

	int padding = paddingNo - (int) strlen (b);
	if (padding < 0) padding = 0;

	if (justify) {
		while (padding--) {
			if (zeroPad) {
				b[strlen (b)] = '0';
			} else {
				b[strlen (b)] = ' ';
			}
		}

	} else {
		char a[256] = { 0 };
		while (padding--) {
			if (zeroPad) {
				a[strlen (a)] = '0';
			} else {
				a[strlen (a)] = ' ';
			}
		}
		strcat (a, b);
		strcpy (b, a);
	}

	return b;
}

char* A__int_str (intmax_t i, char b[], int base, bool plusSignIfNeeded, bool spaceSignIfNeeded,
				  int paddingNo, bool justify, bool zeroPad)
{

	char digit[32] = { 0 };
	memset (digit, 0, 32);
	strcpy (digit, "0123456789");

	if (base == 16) {
		strcat (digit, "ABCDEF");
	} else if (base == 17) {
		strcat (digit, "abcdef");
		base = 16;
	}

	char* p = b;
	if (i < 0) {
		*p++ = '-';
		i *= -1;
	} else if (plusSignIfNeeded) {
		*p++ = '+';
	} else if (!plusSignIfNeeded && spaceSignIfNeeded) {
		*p++ = ' ';
	}

	intmax_t shifter = i;
	do {
		++p;
		shifter = shifter / base;
	} while (shifter);

	*p = '\0';
	do {
		*--p = digit[i % base];
		i = i / base;
	} while (i);

	int padding = paddingNo - (int) strlen (b);
	if (padding < 0) padding = 0;

	if (justify) {
		while (padding--) {
			if (zeroPad) {
				b[strlen (b)] = '0';
			} else {
				b[strlen (b)] = ' ';
			}
		}

	} else {
		char a[256] = { 0 };
		while (padding--) {
			if (zeroPad) {
				a[strlen (a)] = '0';
			} else {
				a[strlen (a)] = ' ';
			}
		}
		strcat (a, b);
		strcpy (b, a);
	}

	return b;
}

void displayCharacterS (char c, int* a, char* buffer)
{
	buffer[*a] = c;
	*a += 1;
}

void displayStringS (char* c, int* a, char* buffer)
{
	for (int i = 0; c[i]; ++i) {
		displayCharacterS (c[i], a, buffer);
	}
}

int vsprintf (char* buffer, const char* format, va_list list)
{
	int chars = 0;
	char intStrBuffer[256] = { 0 };

	for (int i = 0; format[i]; ++i) {

		char specifier = '\0';
		char length = '\0';

		int  lengthSpec = 0;
		int  precSpec = 0;
		bool leftJustify = false;
		bool zeroPad = false;
		bool spaceNoSign = false;
		bool altForm = false;
		bool plusSign = false;
		bool emode = false;
		int  expo = 0;

		if (format[i] == '%') {
			++i;

			bool extBreak = false;
			while (1) {

				switch (format[i]) {
				case '-':
					leftJustify = true;
					++i;
					break;

				case '+':
					plusSign = true;
					++i;
					break;

				case '#':
					altForm = true;
					++i;
					break;

				case ' ':
					spaceNoSign = true;
					++i;
					break;

				case '0':
					zeroPad = true;
					++i;
					break;

				default:
					extBreak = true;
					break;
				}

				if (extBreak) break;
			}

			while (isdigit (format[i])) {
				lengthSpec *= 10;
				lengthSpec += format[i] - 48;
				++i;
			}

			if (format[i] == '*') {
				lengthSpec = va_arg (list, int);
				++i;
			}

			if (format[i] == '.') {
				++i;
				while (isdigit (format[i])) {
					precSpec *= 10;
					precSpec += format[i] - 48;
					++i;
				}

				if (format[i] == '*') {
					precSpec = va_arg (list, int);
					++i;
				}
			} else {
				precSpec = 6;
			}

			if (format[i] == 'h' || format[i] == 'l' || format[i] == 'j' ||
				format[i] == 'z' || format[i] == 't' || format[i] == 'L') {
				length = format[i];
				++i;
				if (format[i] == 'h') {
					length = 'H';
				} else if (format[i] == 'l') {
					length = 'q';
				}
			}
			specifier = format[i];

			memset (intStrBuffer, 0, 256);

			int base = 10;
			if (specifier == 'o') {
				base = 8;
				specifier = 'u';
				if (altForm) {
					displayStringS ("0", &chars, buffer);
				}
			}
			if (specifier == 'p') {
				base = 16;
				length = 'z';
				specifier = 'u';
			}
			switch (specifier) {
			case 'X':
				base = 16;
			case 'x':
				base = base == 10 ? 17 : base;
				if (altForm) {
					displayStringS ("0x", &chars, buffer);
				}

			case 'u':
			{
				switch (length) {
				case 0:
				{
					unsigned int integer = va_arg (list, unsigned int);
					A__int_str (integer, intStrBuffer, base, plusSign, spaceNoSign, lengthSpec, leftJustify, zeroPad);
					displayStringS (intStrBuffer, &chars, buffer);
					break;
				}
				case 'H':
				{
					unsigned char integer = (unsigned char) va_arg (list, unsigned int);
					A__int_str (integer, intStrBuffer, base, plusSign, spaceNoSign, lengthSpec, leftJustify, zeroPad);
					displayStringS (intStrBuffer, &chars, buffer);
					break;
				}
				case 'h':
				{
					unsigned short int integer = va_arg (list, unsigned int);
					A__int_str (integer, intStrBuffer, base, plusSign, spaceNoSign, lengthSpec, leftJustify, zeroPad);
					displayStringS (intStrBuffer, &chars, buffer);
					break;
				}
				case 'l':
				{
					unsigned long integer = va_arg (list, unsigned long);
					A__int_str (integer, intStrBuffer, base, plusSign, spaceNoSign, lengthSpec, leftJustify, zeroPad);
					displayStringS (intStrBuffer, &chars, buffer);
					break;
				}
				case 'q':
				{
					unsigned long long integer = va_arg (list, unsigned long long);
					A__int_str (integer, intStrBuffer, base, plusSign, spaceNoSign, lengthSpec, leftJustify, zeroPad);
					displayStringS (intStrBuffer, &chars, buffer);
					break;
				}
				case 'j':
				{
					uintmax_t integer = va_arg (list, uintmax_t);
					A__int_str (integer, intStrBuffer, base, plusSign, spaceNoSign, lengthSpec, leftJustify, zeroPad);
					displayStringS (intStrBuffer, &chars, buffer);
					break;
				}
				case 'z':
				{
					size_t integer = va_arg (list, size_t);
					A__int_str (integer, intStrBuffer, base, plusSign, spaceNoSign, lengthSpec, leftJustify, zeroPad);
					displayStringS (intStrBuffer, &chars, buffer);
					break;
				}
				case 't':
				{
					ptrdiff_t integer = va_arg (list, ptrdiff_t);
					A__int_str (integer, intStrBuffer, base, plusSign, spaceNoSign, lengthSpec, leftJustify, zeroPad);
					displayStringS (intStrBuffer, &chars, buffer);
					break;
				}
				default:
					break;
				}
				break;
			}

			case 'd':
			case 'i':
			{
				switch (length) {
				case 0:
				{
					int integer = va_arg (list, int);
					A__int_str (integer, intStrBuffer, base, plusSign, spaceNoSign, lengthSpec, leftJustify, zeroPad);
					displayStringS (intStrBuffer, &chars, buffer);
					break;
				}
				case 'H':
				{
					signed char integer = (signed char) va_arg (list, int);
					A__int_str (integer, intStrBuffer, base, plusSign, spaceNoSign, lengthSpec, leftJustify, zeroPad);
					displayStringS (intStrBuffer, &chars, buffer);
					break;
				}
				case 'h':
				{
					short int integer = va_arg (list, int);
					A__int_str (integer, intStrBuffer, base, plusSign, spaceNoSign, lengthSpec, leftJustify, zeroPad);
					displayStringS (intStrBuffer, &chars, buffer);
					break;
				}
				case 'l':
				{
					long integer = va_arg (list, long);
					A__int_str (integer, intStrBuffer, base, plusSign, spaceNoSign, lengthSpec, leftJustify, zeroPad);
					displayStringS (intStrBuffer, &chars, buffer);
					break;
				}
				case 'q':
				{
					long long integer = va_arg (list, long long);
					A__int_str (integer, intStrBuffer, base, plusSign, spaceNoSign, lengthSpec, leftJustify, zeroPad);
					displayStringS (intStrBuffer, &chars, buffer);
					break;
				}
				case 'j':
				{
					intmax_t integer = va_arg (list, intmax_t);
					A__int_str (integer, intStrBuffer, base, plusSign, spaceNoSign, lengthSpec, leftJustify, zeroPad);
					displayStringS (intStrBuffer, &chars, buffer);
					break;
				}
				case 'z':
				{
					size_t integer = va_arg (list, size_t);
					A__int_str (integer, intStrBuffer, base, plusSign, spaceNoSign, lengthSpec, leftJustify, zeroPad);
					displayStringS (intStrBuffer, &chars, buffer);
					break;
				}
				case 't':
				{
					ptrdiff_t integer = va_arg (list, ptrdiff_t);
					A__int_str (integer, intStrBuffer, base, plusSign, spaceNoSign, lengthSpec, leftJustify, zeroPad);
					displayStringS (intStrBuffer, &chars, buffer);
					break;
				}
				default:
					break;
				}
				break;
			}

			case 'c':
			{
				char c = va_arg (list, int);
				char buff[8000];
				memset (buff, 0, 8000);
				buff[0] = c;

				A__format_str (buff, spaceNoSign, lengthSpec, leftJustify, zeroPad);

				displayStringS (buff, &chars, buffer);
				break;
			}

			case 's':
			{
				char* ff = va_arg (list, char*);
				char buff[65536];
				memset (buff, 0, 65536);
				strcpy (buff, ff);

				A__format_str (buff, spaceNoSign, lengthSpec, leftJustify, zeroPad);

				displayStringS (buff, &chars, buffer);
				break;
			}

			case 'n':
			{
				switch (length) {
				case 'H':
					*(va_arg (list, signed char*)) = chars;
					break;
				case 'h':
					*(va_arg (list, short int*)) = chars;
					break;

				case 0:
				{
					int* a = va_arg (list, int*);
					*a = chars;
					break;
				}

				case 'l':
					*(va_arg (list, long*)) = chars;
					break;
				case 'q':
					*(va_arg (list, long long*)) = chars;
					break;
				case 'j':
					*(va_arg (list, intmax_t*)) = chars;
					break;
				case 'z':
					*(va_arg (list, size_t*)) = chars;
					break;
				case 't':
					*(va_arg (list, ptrdiff_t*)) = chars;
					break;
				default:
					break;
				}
				break;
			}

			case 'e':
			case 'E':
				emode = true;

			case 'f':
			case 'F':
			case 'g':
			case 'G':
			{
				double floating = va_arg (list, double);

				while (emode && floating >= 10) {
					floating /= 10;
					++expo;
				}

				int form = lengthSpec - precSpec - expo - (precSpec || altForm ? 1 : 0);
				if (emode) {
					form -= 4;      // 'e+00'
				}
				if (form < 0) {
					form = 0;
				}

				A__int_str (floating, intStrBuffer, base, plusSign, spaceNoSign, form, \
							leftJustify, zeroPad);

				displayStringS (intStrBuffer, &chars, buffer);

				floating -= (int) floating;

				for (int i = 0; i < precSpec; ++i) {
					floating *= 10;
				}
				intmax_t decPlaces = (intmax_t) (floating + 0.5);

				if (precSpec) {
					displayCharacterS ('.', &chars, buffer);
					A__int_str (decPlaces, intStrBuffer, 10, false, false, 0, false, false);
					intStrBuffer[precSpec] = 0;
					displayStringS (intStrBuffer, &chars, buffer);
				} else if (altForm) {
					displayCharacterS ('.', &chars, buffer);
				}

				break;
			}


			case 'a':
			case 'A':
				//ACK! Hexadecimal floating points...
				break;

			default:
				break;
			}

			if (specifier == 'e') {
				displayStringS ("e+", &chars, buffer);
			} else if (specifier == 'E') {
				displayStringS ("E+", &chars, buffer);
			}

			if (specifier == 'e' || specifier == 'E') {
				A__int_str (expo, intStrBuffer, 10, false, false, 2, false, true);
				displayStringS (intStrBuffer, &chars, buffer);
			}

		} else {
			displayCharacterS (format[i], &chars, buffer);
		}
	}

	displayCharacterS (0, &chars, buffer);
	return chars;
}

int sprintf (char* buffer, const char* format, ...)
{
	va_list list;
	va_start (list, format);
	int i = vsprintf (buffer, format, list);
	va_end (list);
	return i;
}

void displayCharacter (char c, int* a)
{
	putchar (c);
	*a += 1;
}

void displayString (char* c, int* a)
{
	for (int i = 0; c[i]; ++i) {
		displayCharacter (c[i], a);
	}
}

int vprintf (const char* format, va_list list)
{
	autoFlush = false;

	int chars = 0;
	char intStrBuffer[256] = { 0 };

	for (int i = 0; format[i]; ++i) {
		char specifier = '\0';
		char length = '\0';

		int  lengthSpec = 0;
		int  precSpec = 0;
		bool leftJustify = false;
		bool zeroPad = false;
		bool spaceNoSign = false;
		bool altForm = false;
		bool plusSign = false;
		bool emode = false;
		int  expo = 0;

		if (format[i] == '%') {
			++i;

			bool extBreak = false;
			while (1) {

				switch (format[i]) {
				case '-':
					leftJustify = true;
					++i;
					break;

				case '+':
					plusSign = true;
					++i;
					break;

				case '#':
					altForm = true;
					++i;
					break;

				case ' ':
					spaceNoSign = true;
					++i;
					break;

				case '0':
					zeroPad = true;
					++i;
					break;

				default:
					extBreak = true;
					break;
				}

				if (extBreak) break;
			}

			while (isdigit (format[i])) {
				lengthSpec *= 10;
				lengthSpec += format[i] - 48;
				++i;
			}

			if (format[i] == '*') {
				lengthSpec = va_arg (list, int);
				++i;
			}

			if (format[i] == '.') {
				++i;
				while (isdigit (format[i])) {
					precSpec *= 10;
					precSpec += format[i] - 48;
					++i;
				}

				if (format[i] == '*') {
					precSpec = va_arg (list, int);
					++i;
				}
			} else {
				precSpec = 6;
			}

			if (format[i] == 'h' || format[i] == 'l' || format[i] == 'j' ||
				format[i] == 'z' || format[i] == 't' || format[i] == 'L') {
				length = format[i];
				++i;
				if (format[i] == 'h') {
					length = 'H';
				} else if (format[i] == 'l') {
					length = 'q';
				}
			}
			specifier = format[i];

			memset (intStrBuffer, 0, 256);

			int base = 10;
			if (specifier == 'o') {
				base = 8;
				specifier = 'u';
				if (altForm) {
					displayString ("0", &chars);
				}
			}
			if (specifier == 'p') {
				base = 16;
				length = 'z';
				specifier = 'u';
			}
			switch (specifier) {
			case 'X':
				base = 16;
			case 'x':
				base = base == 10 ? 17 : base;
				if (altForm) {
					displayString ("0x", &chars);
				}

			case 'u':
			{
				switch (length) {
				case 0:
				{
					unsigned int integer = va_arg (list, unsigned int);
					A__int_str (integer, intStrBuffer, base, plusSign, spaceNoSign, lengthSpec, leftJustify, zeroPad);
					displayString (intStrBuffer, &chars);
					break;
				}
				case 'H':
				{
					unsigned char integer = (unsigned char) va_arg (list, unsigned int);
					A__int_str (integer, intStrBuffer, base, plusSign, spaceNoSign, lengthSpec, leftJustify, zeroPad);
					displayString (intStrBuffer, &chars);
					break;
				}
				case 'h':
				{
					unsigned short int integer = va_arg (list, unsigned int);
					A__int_str (integer, intStrBuffer, base, plusSign, spaceNoSign, lengthSpec, leftJustify, zeroPad);
					displayString (intStrBuffer, &chars);
					break;
				}
				case 'l':
				{
					unsigned long integer = va_arg (list, unsigned long);
					A__int_str (integer, intStrBuffer, base, plusSign, spaceNoSign, lengthSpec, leftJustify, zeroPad);
					displayString (intStrBuffer, &chars);
					break;
				}
				case 'q':
				{
					unsigned long long integer = va_arg (list, unsigned long long);
					A__int_str (integer, intStrBuffer, base, plusSign, spaceNoSign, lengthSpec, leftJustify, zeroPad);
					displayString (intStrBuffer, &chars);
					break;
				}
				case 'j':
				{
					uintmax_t integer = va_arg (list, uintmax_t);
					A__int_str (integer, intStrBuffer, base, plusSign, spaceNoSign, lengthSpec, leftJustify, zeroPad);
					displayString (intStrBuffer, &chars);
					break;
				}
				case 'z':
				{
					size_t integer = va_arg (list, size_t);
					A__int_str (integer, intStrBuffer, base, plusSign, spaceNoSign, lengthSpec, leftJustify, zeroPad);
					displayString (intStrBuffer, &chars);
					break;
				}
				case 't':
				{
					ptrdiff_t integer = va_arg (list, ptrdiff_t);
					A__int_str (integer, intStrBuffer, base, plusSign, spaceNoSign, lengthSpec, leftJustify, zeroPad);
					displayString (intStrBuffer, &chars);
					break;
				}
				default:
					break;
				}
				break;
			}

			case 'd':
			case 'i':
			{
				switch (length) {
				case 0:
				{
					int integer = va_arg (list, int);
					A__int_str (integer, intStrBuffer, base, plusSign, spaceNoSign, lengthSpec, leftJustify, zeroPad);
					displayString (intStrBuffer, &chars);
					break;
				}
				case 'H':
				{
					signed char integer = (signed char) va_arg (list, int);
					A__int_str (integer, intStrBuffer, base, plusSign, spaceNoSign, lengthSpec, leftJustify, zeroPad);
					displayString (intStrBuffer, &chars);
					break;
				}
				case 'h':
				{
					short int integer = va_arg (list, int);
					A__int_str (integer, intStrBuffer, base, plusSign, spaceNoSign, lengthSpec, leftJustify, zeroPad);
					displayString (intStrBuffer, &chars);
					break;
				}
				case 'l':
				{
					long integer = va_arg (list, long);
					A__int_str (integer, intStrBuffer, base, plusSign, spaceNoSign, lengthSpec, leftJustify, zeroPad);
					displayString (intStrBuffer, &chars);
					break;
				}
				case 'q':
				{
					long long integer = va_arg (list, long long);
					A__int_str (integer, intStrBuffer, base, plusSign, spaceNoSign, lengthSpec, leftJustify, zeroPad);
					displayString (intStrBuffer, &chars);
					break;
				}
				case 'j':
				{
					intmax_t integer = va_arg (list, intmax_t);
					A__int_str (integer, intStrBuffer, base, plusSign, spaceNoSign, lengthSpec, leftJustify, zeroPad);
					displayString (intStrBuffer, &chars);
					break;
				}
				case 'z':
				{
					size_t integer = va_arg (list, size_t);
					A__int_str (integer, intStrBuffer, base, plusSign, spaceNoSign, lengthSpec, leftJustify, zeroPad);
					displayString (intStrBuffer, &chars);
					break;
				}
				case 't':
				{
					ptrdiff_t integer = va_arg (list, ptrdiff_t);
					A__int_str (integer, intStrBuffer, base, plusSign, spaceNoSign, lengthSpec, leftJustify, zeroPad);
					displayString (intStrBuffer, &chars);
					break;
				}
				default:
					break;
				}
				break;
			}

			case 'c':
			{
				char c = va_arg (list, int);
				char buff[8000];
				memset (buff, 0, 8000);
				buff[0] = c;

				A__format_str (buff, spaceNoSign, lengthSpec, leftJustify, zeroPad);

				displayString (buff, &chars);
				break;
			}

			case 's':
			{
				char* ff = va_arg (list, char*);
				char buff[8000];
				memset (buff, 0, 8000);
				strcpy (buff, ff);

				A__format_str (buff, spaceNoSign, lengthSpec, leftJustify, zeroPad);

				displayString (buff, &chars);
				break;
				
			}

			case 'n':
			{
				switch (length) {
				case 'H':
					*(va_arg (list, signed char*)) = chars;
					break;
				case 'h':
					*(va_arg (list, short int*)) = chars;
					break;

				case 0:
				{
					int* a = va_arg (list, int*);
					*a = chars;
					break;
				}

				case 'l':
					*(va_arg (list, long*)) = chars;
					break;
				case 'q':
					*(va_arg (list, long long*)) = chars;
					break;
				case 'j':
					*(va_arg (list, intmax_t*)) = chars;
					break;
				case 'z':
					*(va_arg (list, size_t*)) = chars;
					break;
				case 't':
					*(va_arg (list, ptrdiff_t*)) = chars;
					break;
				default:
					break;
				}
				break;
			}

			case 'e':
			case 'E':
				emode = true;

			case 'f':
			case 'F':
			case 'g':
			case 'G':
			{
				double floating = va_arg (list, double);

				while (emode && floating >= 10) {
					floating /= 10;
					++expo;
				}

				int form = lengthSpec - precSpec - expo - (precSpec || altForm ? 1 : 0);
				if (emode) {
					form -= 4;      // 'e+00'
				}
				if (form < 0) {
					form = 0;
				}

				A__int_str (floating, intStrBuffer, base, plusSign, spaceNoSign, form, \
							leftJustify, zeroPad);

				displayString (intStrBuffer, &chars);

				floating -= (int) floating;

				for (int i = 0; i < precSpec; ++i) {
					floating *= 10;
				}
				intmax_t decPlaces = (intmax_t) (floating + 0.5);

				if (precSpec) {
					displayCharacter ('.', &chars);
					A__int_str (decPlaces, intStrBuffer, 10, false, false, 0, false, false);
					intStrBuffer[precSpec] = 0;
					displayString (intStrBuffer, &chars);
				} else if (altForm) {
					displayCharacter ('.', &chars);
				}

				break;
			}


			case 'a':
			case 'A':
				//ACK! Hexadecimal floating points...
				break;

			default:
				break;
			}

			if (specifier == 'e') {
				displayString ("e+", &chars);
			} else if (specifier == 'E') {
				displayString ("E+", &chars);
			}

			if (specifier == 'e' || specifier == 'E') {
				A__int_str (expo, intStrBuffer, 10, false, false, 2, false, true);
				displayString (intStrBuffer, &chars);
			}

		} else {
			displayCharacter (format[i], &chars);
		}
	}

	autoFlush = true;
	fflush (stdout);
	return chars;
}

__attribute__ ((format (printf, 1, 2))) int printf (const char* format, ...)
{
	va_list list;
	va_start (list, format);
	int i = vprintf (format, list);
	va_end (list);
	return i;
}

int vfprintf (FILE* file, const char* format, va_list list)
{
	if (file == stdout__ || file == stderr__) {
		vprintf (format, list);

	} else if (file == stdin__) {
		//.. keyboard

	} else {
		char buffer[65536] = { 0 };
		vsprintf (buffer, format, list);
		fwrite (buffer, strlen (buffer), 1, file);
	}
	return 0;
}

int fprintf (FILE* file, const char* format, ...)
{
	va_list list;
	va_start (list, format);
	int i = vfprintf (file, format, list);
	va_end (list);
	return i;
}

int fputs (const char* s, FILE* file)
{
	fwrite ((void*) s, strlen ((char*) s), 1, file);
	return 0;
}

int fgetc (FILE* file)
{
	if (file == stdout__ || file == stderr__) {
		//.. screen

	} else if (file == stdin__) {
		return getchar ();

	} else {
		char buffer[2] = { 0 };
		fwrite ((void*) buffer, 1, 1, file);
		return buffer[0];
	}
	return 0;
}

int getc (FILE* stream)
{
	return fgetc (stream);
}

char* fgets (char *str, int n, FILE *file)
{
	if (file == stdout__ || file == stderr__) {
		//.. screen

	} else if (file == stdin__) {
		return gets_s (str, n);

	} else {
		//.. file
	}
	return 0;
}

int fflush (FILE* file)
{
	if (file == stdout__ || file == stderr__) {
		charsSinceLastStdoutFlush = 0;
		SystemCall (SC_StdinFlush, 0, 0, 0);			//this flushes stdout, believe it or not

	} else if (file == stdin__) {
		leftoverPointer = 0;

	} else {
		SystemCall (SC_FFlush, 0, *file, 0);
	}
	return 0;
}

FILE* freopen (const char* filename, const char* mode, FILE* file)
{
	fclose (file);
	return fopen ((char*) filename, (char*) mode);
}

FILE* fdopen (int fildes, const char *mode)
{
	//warning: function returns address of local variable
	return (FILE*) &fildes;
}

FILE* fopen (const char* filename, const char* mode)
{
	int flags = 0;
	switch (mode[0]) {
	case 'r':
		flags = FA_READ;
		break;
	case 'w':
		flags = FA_CREATE_ALWAYS | FA_WRITE;
		break;
	case 'a':
		flags = FA_OPEN_APPEND | FA_WRITE;
		break;
	case 'x':
		flags = FA_CREATE_NEW | FA_WRITE;
		break;
	}
	if (mode[1] == '+' || mode[2] == '+') {
		flags |= FA_WRITE | FA_READ;
	}

	int number = SystemCall (SC_NewFileOpen, 0, flags, (void*) filename);
	if (number == -1) {
		return (FILE*) -1;
	}
	FILE* ptr = malloc (sizeof (number));
	*ptr = number;
	return ptr;
}

size_t fread (void* ptr, size_t size, size_t count, FILE* file)
{
	if (!virtualFileStreamAddon) virtualFileStreamAddon = malloc (65536);

	//query 
	//
	// virtualFileStreamAddon using virtualFileStreamAddonPointer to append data to the start

	if (file == stdout__ || file == stderr__) {
		//.. screen

	} else if (file == stdin__) {
		read_s (ptr, size * count);

	} else {
		SystemCall (SC_NewFileRead, size, *file, ptr);
	}

	return 0;
}

size_t fwrite (const void* ptr, size_t size, size_t count, FILE* file)
{
	if (file == stdout__) {

	} else if (file == stderr__) {
		//.. screen

	} else if (file == stdin__) {
		//.. 

	} else {
		SystemCall (SC_NewFileWrite, size * count, *file, (void*) ptr);
	}

	return 0;
}

int fclose (FILE* file)
{
	if (file == stdout__ || file == stderr__) {
		if (file == stdout__) {
			SystemCall (SC_ExitTerminalSession, 0, 0, 0);
		}

	} else if (file == stdin__) {
		//.. 

	} else {
		SystemCall (SC_NewFileClose, 0, *file, 0);
	}
	return 0;
}

int remove (const char* filename)
{
	return SystemCall (SC_NewFileDelete, 0, 0, (void*) filename);
}

int fseek (FILE* stream, long offset, int origin)
{
	int pl;
	if (origin == SEEK_SET) {
		pl = 0;
	} else if (origin == SEEK_CUR) {
		pl = ftell (stream);
	} else if (origin == SEEK_END) {
		printf ("FSEEK USING SEEK_END!");
		//pl = SystemCall (SC_NewFileStat, 0, 0, filename);		
	} else {
		return -1;
	}

	return SystemCall (SC_SetFilePosition, pl + offset, *stream, 0);
}

long ftell (FILE* stream)
{
	return SystemCall (SC_GetFilePosition, 0, *stream, 0);
}

void rewind (FILE* stream)
{
	SystemCall (SC_SetFilePosition, 0, *stream, 0);
}

int fgetpos (FILE* stream, fpos_t* ptr)
{
	*ptr = SystemCall (SC_GetFilePosition, 0, *stream, 0);
	return 0;
}

int fsetpos (FILE* stream, const fpos_t* ptr)
{
	return SystemCall (SC_GetFilePosition, *ptr, *stream, 0);
}

int feof (FILE* stream)
{
	// if stream == __stdin...
	return SystemCall (SC_EOF, 0, *stream, 0);
}

int ferror (FILE* stream)
{
	return SystemCall (SC_FileError, 0, *stream, 0);
}

void clearerr (FILE* stream)
{
	SystemCall (SC_ClearFileError, 0, *stream, 0);
}

int fputc (int c, FILE* file)
{
	char buffer[2] = { 0 };
	buffer[0] = c;
	return SystemCall (SC_NewFileRead, 1, *file, (void*) ((size_t) c));
}

int putc (int c, FILE* stream)
{
	return fputc (c, stream);
}

void __gen_random (char *s, const int len)
{
	static const char alphanum[] =
		"0123456789"
		"ABCDEFGHIJKLMNOPQRSTUVWXYZ"
		"abcdefghijklmnopqrstuvwxyz"
		"_!#$%&'()-";

	for (int i = 0; i < len; ++i) {
		s[i] = alphanum[rand () % (sizeof (alphanum) - 1)];
	}

	s[len] = 0;
}

char* tmpnam (char s[L_tmpnam])
{
	char buf[1024] = { 0 };
	char pn[256] = { 0 };
	char randb[256] = { 0 };

	__gen_random (randb, 15);

	strcpy (pn, (char*) ((size_t) SystemCall (SC_GetProcessName, 0, 0, 0)));

	for (int i = 0; pn[i]; ++i) {
		if (!isalnum (pn[i])) {
			pn[i] = '_';
		}
	}

	strcpy (buf, "C:/Banana/Temporary Files/__");
	strcat (buf, pn);
	strcat (buf, "___");
	strcat (buf, randb);

	memcpy (s, buf, L_tmpnam - 2);
	s[L_tmpnam - 1] = 0;

	return s;
}

FILE* tmpfile ()
{
	char b[L_tmpnam] = { 0 };
	tmpnam (b);
	return fopen (b, "wb+");
}

void perror (const char *str)
{
	printf ("%s: ", str);
}

int fileno (FILE *stream)
{
	return *stream;
}





















































/*
CC0:

Creative Commons Legal Code

CC0 1.0 Universal

CREATIVE COMMONS CORPORATION IS NOT A LAW FIRM AND DOES NOT PROVIDE
LEGAL SERVICES. DISTRIBUTION OF THIS DOCUMENT DOES NOT CREATE AN
ATTORNEY-CLIENT RELATIONSHIP. CREATIVE COMMONS PROVIDES THIS
INFORMATION ON AN "AS-IS" BASIS. CREATIVE COMMONS MAKES NO WARRANTIES
REGARDING THE USE OF THIS DOCUMENT OR THE INFORMATION OR WORKS
PROVIDED HEREUNDER, AND DISCLAIMS LIABILITY FOR DAMAGES RESULTING FROM
THE USE OF THIS DOCUMENT OR THE INFORMATION OR WORKS PROVIDED
HEREUNDER.

Statement of Purpose

The laws of most jurisdictions throughout the world automatically confer
exclusive Copyright and Related Rights (defined below) upon the creator
and subsequent owner(s) (each and all, an "owner") of an original work of
authorship and/or a database (each, a "Work").

Certain owners wish to permanently relinquish those rights to a Work for
the purpose of contributing to a commons of creative, cultural and
scientific works ("Commons") that the public can reliably and without fear
of later claims of infringement build upon, modify, incorporate in other
works, reuse and redistribute as freely as possible in any form whatsoever
and for any purposes, including without limitation commercial purposes.
These owners may contribute to the Commons to promote the ideal of a free
culture and the further production of creative, cultural and scientific
works, or to gain reputation or greater distribution for their Work in
part through the use and efforts of others.

For these and/or other purposes and motivations, and without any
expectation of additional consideration or compensation, the person
associating CC0 with a Work (the "Affirmer"), to the extent that he or she
is an owner of Copyright and Related Rights in the Work, voluntarily
elects to apply CC0 to the Work and publicly distribute the Work under its
terms, with knowledge of his or her Copyright and Related Rights in the
Work and the meaning and intended legal effect of CC0 on those rights.

1. Copyright and Related Rights. A Work made available under CC0 may be
protected by copyright and related or neighboring rights ("Copyright and
Related Rights"). Copyright and Related Rights include, but are not
limited to, the following:

i. the right to reproduce, adapt, distribute, perform, display,
communicate, and translate a Work;
ii. moral rights retained by the original author(s) and/or performer(s);
iii. publicity and privacy rights pertaining to a person's image or
likeness depicted in a Work;
iv. rights protecting against unfair competition in regards to a Work,
subject to the limitations in paragraph 4(a), below;
v. rights protecting the extraction, dissemination, use and reuse of data
in a Work;
vi. database rights (such as those arising under Directive 96/9/EC of the
European Parliament and of the Council of 11 March 1996 on the legal
protection of databases, and under any national implementation
thereof, including any amended or successor version of such
directive); and
vii. other similar, equivalent or corresponding rights throughout the
world based on applicable law or treaty, and any national
implementations thereof.

2. Waiver. To the greatest extent permitted by, but not in contravention
of, applicable law, Affirmer hereby overtly, fully, permanently,
irrevocably and unconditionally waives, abandons, and surrenders all of
Affirmer's Copyright and Related Rights and associated claims and causes
of action, whether now known or unknown (including existing as well as
future claims and causes of action), in the Work (i) in all territories
worldwide, (ii) for the maximum duration provided by applicable law or
treaty (including future time extensions), (iii) in any current or future
medium and for any number of copies, and (iv) for any purpose whatsoever,
including without limitation commercial, advertising or promotional
purposes (the "Waiver"). Affirmer makes the Waiver for the benefit of each
member of the public at large and to the detriment of Affirmer's heirs and
successors, fully intending that such Waiver shall not be subject to
revocation, rescission, cancellation, termination, or any other legal or
equitable action to disrupt the quiet enjoyment of the Work by the public
as contemplated by Affirmer's express Statement of Purpose.

3. Public License Fallback. Should any part of the Waiver for any reason
be judged legally invalid or ineffective under applicable law, then the
Waiver shall be preserved to the maximum extent permitted taking into
account Affirmer's express Statement of Purpose. In addition, to the
extent the Waiver is so judged Affirmer hereby grants to each affected
person a royalty-free, non transferable, non sublicensable, non exclusive,
irrevocable and unconditional license to exercise Affirmer's Copyright and
Related Rights in the Work (i) in all territories worldwide, (ii) for the
maximum duration provided by applicable law or treaty (including future
time extensions), (iii) in any current or future medium and for any number
of copies, and (iv) for any purpose whatsoever, including without
limitation commercial, advertising or promotional purposes (the
"License"). The License shall be deemed effective as of the date CC0 was
applied by Affirmer to the Work. Should any part of the License for any
reason be judged legally invalid or ineffective under applicable law, such
partial invalidity or ineffectiveness shall not invalidate the remainder
of the License, and in such case Affirmer hereby affirms that he or she
will not (i) exercise any of his or her remaining Copyright and Related
Rights in the Work or (ii) assert any associated claims and causes of
action with respect to the Work, in either case contrary to Affirmer's
express Statement of Purpose.

4. Limitations and Disclaimers.

a. No trademark or patent rights held by Affirmer are waived, abandoned,
surrendered, licensed or otherwise affected by this document.
b. Affirmer offers the Work as-is and makes no representations or
warranties of any kind concerning the Work, express, implied,
statutory or otherwise, including without limitation warranties of
title, merchantability, fitness for a particular purpose, non
infringement, or the absence of latent or other defects, accuracy, or
the present or absence of errors, whether or not discoverable, all to
the greatest extent permissible under applicable law.
c. Affirmer disclaims responsibility for clearing rights of other persons
that may apply to the Work or any use thereof, including without
limitation any person's Copyright and Related Rights in the Work.
Further, Affirmer disclaims responsibility for obtaining any necessary
consents, permissions or other rights required for any use of the
Work.
d. Affirmer understands and acknowledges that Creative Commons is not a
party to this document and has no duty or obligation with respect to
this CC0 or use of the Work.
*/


// @@@
#define E_suppressed 1<<0
#define E_char       1<<6
#define E_short      1<<7
#define E_long       1<<8
#define E_llong      1<<9
#define E_intmax     1<<10
#define E_size       1<<11
#define E_ptrdiff    1<<12
#define E_intptr     1<<13
#define E_ldouble    1<<14
#define E_unsigned   1<<16
#define E_float      1<<17
#define E_double     1<<18



char _PDCLIB_digits[] = "0123456789abcdefghijklmnopqrstuvwxyz.";

/* Status structure required by _PDCLIB_print(). */
struct _PDCLIB_status_t
{
	/* XXX This structure is horrible now. scanf needs its own */

	int base;   /* base to which the value shall be converted   */
	unsigned int flags; /* flags and length modifiers                */
	unsigned         n;      /* print: maximum characters to be written (snprintf) */
							 /* scan:  number matched conversion specifiers  */
	unsigned         i;      /* number of characters read/written            */
	unsigned         current;/* chars read/written in the CURRENT conversion */
	unsigned         width;  /* specified field width                        */
	int              prec;   /* specified field precision                    */

	const char *     s;      /* input string for scanf */

	va_list  arg;    /* argument stack                               */
};


void * nmemchr (const void * s, int c, size_t n)
{
	const unsigned char * p = (const unsigned char *) s;
	while (n--) {
		if (*p == (unsigned char) c) {
			return (void *) p;
		}
		++p;
	}
	return NULL;
}


/* Helper function to get a character from the string or stream, whatever is
used for input. When reading from a string, returns EOF on end-of-string
so that handling of the return value can be uniform for both streams and
strings.
*/
static int GET (struct _PDCLIB_status_t * status)
{
	int rc = EOF;
	rc = (*status->s == '\0') ? EOF : (unsigned char)*((status->s)++);
	if (rc != EOF) {
		++(status->i);
		++(status->current);
	}
	return rc;
}


/* Helper function to put a read character back into the string or stream,
whatever is used for input.
*/
static void UNGET (int c, struct _PDCLIB_status_t * status)
{
	--(status->s);
	--(status->i);
	--(status->current);
}


/* Helper function to check if a character is part of a given scanset */
static bool IN_SCANSET (const char * scanlist, const char * end_scanlist, int rc)
{
	// SOLAR
	int previous = -1;
	while (scanlist != end_scanlist) {
		if ((*scanlist == '-') && (previous != -1)) {
			/* possible scangroup ("a-z") */
			if (++scanlist == end_scanlist) {
				/* '-' at end of scanlist does not describe a scangroup */
				return rc == '-';
			}
			while (++previous <= (unsigned char) *scanlist) {
				if (previous == rc) {
					return true;
				}
			}
			previous = -1;
		} else {
			/* not a scangroup, check verbatim */
			if (rc == (unsigned char) *scanlist) {
				return true;
			}
			previous = (unsigned char) (*scanlist++);
		}
	}
	return false;
}


const char * _PDCLIB_scan (const char * spec, struct _PDCLIB_status_t * status)
{
	/* generic input character */
	int rc;
	const char * orig_spec = spec;
	if (*(++spec) == '%') {
		/* %% -> match single '%' */
		rc = GET (status);
		switch (rc) {
		case EOF:
			/* input error */
			if (status->n == 0) {
				status->n = -1;
			}
			return NULL;
		case '%':
			return ++spec;
		default:
			UNGET (rc, status);
			break;
		}
	}
	/* Initializing status structure */
	status->flags = 0;
	status->base = -1;
	status->current = 0;
	status->width = 0;
	status->prec = 0;

	/* '*' suppresses assigning parsed value to variable */
	if (*spec == '*') {
		status->flags |= E_suppressed;
		++spec;
	}

	/* If a width is given, strtol() will return its value. If not given,
	strtol() will return zero. In both cases, endptr will point to the
	rest of the conversion specifier - just what we need.
	*/
	const char * prev_spec = spec;
	status->width = (int) strtol (spec, (char**) &spec, 10);
	if (spec == prev_spec) {
		status->width = UINT_MAX;
	}

	/* Optional length modifier
	We step one character ahead in any case, and step back only if we find
	there has been no length modifier (or step ahead another character if it
	has been "hh" or "ll").
	*/
	switch (*(spec++)) {
	case 'h':
		if (*spec == 'h') {
			/* hh -> char */
			status->flags |= E_char;
			++spec;
		} else {
			/* h -> short */
			status->flags |= E_short;
		}
		break;
	case 'l':
		if (*spec == 'l') {
			/* ll -> long long */
			status->flags |= E_llong;
			++spec;
		} else {
			/* l -> long */
			status->flags |= E_long;
		}
		break;
	case 'j':
		/* j -> intmax_t, which might or might not be long long */
		status->flags |= E_intmax;
		break;
	case 'z':
		/* z -> size_t, which might or might not be unsigned int */
		status->flags |= E_size;
		break;
	case 't':
		/* t -> ptrdiff_t, which might or might not be long */
		status->flags |= E_ptrdiff;
		break;
	case 'L':
		/* L -> long double */
		status->flags |= E_ldouble;
		break;
	default:
		--spec;
		break;
	}

	/* Conversion specifier */

	/* whether valid input had been parsed */
	bool value_parsed = false;

	switch (*spec) {
	case 'd':
		status->base = 10;
		break;
	case 'i':
		status->base = 0;
		break;
	case 'o':
		status->base = 8;
		status->flags |= E_unsigned;
		break;
	case 'u':
		status->base = 10;
		status->flags |= E_unsigned;
		break;
	case 'x':
		status->base = 16;
		status->flags |= E_unsigned;
		break;
	case 'f':
		status->flags |= E_float;
		status->base = 10;
		break;
	case 'F':
	case 'e':
	case 'E':
	case 'g':
	case 'G':
	case 'a':
	case 'A':
		break;
	case 'c':
	{
		char * c = va_arg (status->arg, char *);
		/* for %c, default width is one */
		//if ( status->width == SIZE_MAX )
		if (status->width == 0xFFFFFFFF) {
			status->width = 1;
		}
		/* reading until width reached or input exhausted */
		while ((status->current < status->width) &&
			((rc = GET (status)) != EOF)) {
			*(c++) = rc;
			value_parsed = true;
		}
		/* width or input exhausted */
		if (value_parsed) {
			++status->n;
			return ++spec;
		} else {
			/* input error, no character read */
			if (status->n == 0) {
				status->n = -1;
			}
			return NULL;
		}
	}
	case 's':
	{
		char * c = va_arg (status->arg, char *);
		while ((status->current < status->width) &&
			((rc = GET (status)) != EOF)) {
			if (isspace (rc)) {
				UNGET (rc, status);
				if (value_parsed) {
					/* matching sequence terminated by whitespace */
					*c = '\0';
					++status->n;
					return ++spec;
				} else {
					/* matching error */
					return NULL;
				}
			} else {
				/* match */
				value_parsed = true;
				*(c++) = rc;
			}
		}
		/* width or input exhausted */
		if (value_parsed) {
			*c = '\0';
			++status->n;
			return ++spec;
		} else {
			/* input error, no character read */
			if (status->n == 0) {
				status->n = -1;
			}
			return NULL;
		}
	}
	case '[':
	{
		const char * endspec = spec;
		bool negative_scanlist = false;
		if (*(++endspec) == '^') {
			negative_scanlist = true;
			++endspec;
		}
		spec = endspec;
		do {
			// TODO: This can run beyond a malformed format string
			++endspec;
		} while (*endspec != ']');
		// read according to scanlist, equiv. to %s above
		char * c = va_arg (status->arg, char *);
		while ((status->current < status->width) &&
			((rc = GET (status)) != EOF)) {
			if (negative_scanlist) {
				if (IN_SCANSET (spec, endspec, rc)) {
					UNGET (rc, status);
					break;
				}
			} else {
				if (!IN_SCANSET (spec, endspec, rc)) {
					UNGET (rc, status);
					break;
				}
			}
			value_parsed = true;
			*(c++) = rc;
		}
		if (value_parsed) {
			*c = '\0';
			++status->n;
			return ++endspec;
		} else {
			if (rc == EOF) {
				status->n = -1;
			}
			return NULL;
		}
	}
	case 'p':
		status->base = 16;
		// TODO: Like _PDCLIB_print, E_pointer(?)
		status->flags |= E_unsigned | E_long;
		break;
	case 'n':
	{
		int * val = va_arg (status->arg, int *);
		*val = status->i;
		return ++spec;
	}
	default:
		/* No conversion specifier. Bad conversion. */
		return orig_spec;
	}

	if (status->base != -1) {
		int dp_count = 0;
		bool dp_seen = false;
		/* er conversion */
		uintmax_t value = 0;         /* absolute value read */
		double    value_f = 0;         /* absolute value read */
		bool prefix_parsed = false;
		int sign = 0;
		while ((status->current < status->width) &&
			((rc = GET (status)) != EOF)) {
			if (isspace (rc)) {
				if (sign) {
					/* matching sequence terminated by whitespace */
					UNGET (rc, status);
					break;
				} else {
					/* leading whitespace not counted against width */
					status->current--;
				}
			} else if (!sign) {
				/* no sign parsed yet */
				switch (rc) {
				case '-':
					sign = -1;
					break;
				case '+':
					sign = 1;
					break;
				default:
					/* not a sign; put back character */
					sign = 1;
					UNGET (rc, status);
					break;
				}
			} else if (!prefix_parsed) {
				/* no prefix (0x... for hex, 0... for octal) parsed yet */
				prefix_parsed = true;
				if (rc != '0') {
					/* not a prefix; if base not yet set, set to decimal */
					if (status->base == 0) {
						status->base = 10;
					}
					UNGET (rc, status);
				} else {
					/* starts with zero, so it might be a prefix. */
					/* check what follows next (might be 0x...) */
					if ((status->current < status->width) &&
						((rc = GET (status)) != EOF)) {
						if (tolower (rc) == 'x') {
							/* 0x... would be prefix for hex base... */
							if ((status->base == 0) ||
								(status->base == 16)) {
								status->base = 16;
							} else {
								/* ...unless already set to other value */
								UNGET (rc, status);
								value_parsed = true;
							}
						} else {
							/* 0... but not 0x.... would be octal prefix */
							UNGET (rc, status);
							if (status->base == 0) {
								status->base = 8;
							}
							/* in any case we have read a zero */
							value_parsed = true;
						}
					} else {
						/* failed to read beyond the initial zero */
						value_parsed = true;
						break;
					}
				}
			} else {

				char * digitptr = nmemchr (_PDCLIB_digits, tolower (rc), status->base);
				if (rc != '.') {
					if (digitptr == NULL) {
						/* end of input item */
						UNGET (rc, status);
						break;
					} else {
						value *= status->base;
						value += digitptr - _PDCLIB_digits;

						value_f *= status->base;
						value_f += digitptr - _PDCLIB_digits;
					}
				} else {
					dp_seen = true;
				}
				if (dp_seen) dp_count++;
				value_parsed = true;
			}
		}
		/* width or input exhausted, or non-matching character */
		if (!value_parsed) {
			/* out of input before anything could be parsed - input error */
			/* FIXME: if first character does not match, value_parsed is not set - but it is NOT an input error */
			if ((status->n == 0) && (rc == EOF)) {
				status->n = -1;
			}
			return NULL;
		}
		/* convert value to target type and assign to parameter */
		if (!(status->flags & E_suppressed)) {
			switch (status->flags & (E_char | E_short | E_long | E_llong |
									 E_intmax | E_size | E_ptrdiff | E_float |
									 E_unsigned)) {
			case E_char:
				*(va_arg (status->arg, char *)) = (char) (value * sign);
				break;
			case E_char | E_unsigned:
				*(va_arg (status->arg, unsigned char *)) = (unsigned char) (value * sign);
				break;

			case E_short:
				*(va_arg (status->arg, short *)) = (short) (value * sign);
				break;
			case E_short | E_unsigned:
				*(va_arg (status->arg, unsigned short *)) = (unsigned short) (value * sign);
				break;

			case 0:
				*(va_arg (status->arg, int *)) = (int) (value * sign);
				break;
			case E_unsigned:
				*(va_arg (status->arg, unsigned int *)) = (unsigned int) (value * sign);
				break;

			case E_long:
				*(va_arg (status->arg, long *)) = (long) (value * sign);
				break;
			case E_long | E_unsigned:
				*(va_arg (status->arg, unsigned long *)) = (unsigned long) (value * sign);
				break;

			case E_llong:
				*(va_arg (status->arg, long long *)) = (long long) (value * sign);
				break;
			case E_llong | E_unsigned:
				*(va_arg (status->arg, unsigned long long *)) = (unsigned long long)(value * sign);
				break;

			case E_intmax:
				*(va_arg (status->arg, intmax_t *)) = (intmax_t) (value * sign);
				break;
			case E_float:
			{
				int n;
				for (n = 1; n < dp_count; n++) value_f = value_f / 10;
				*(va_arg (status->arg, float *)) = (float) ((value_f * sign));
			}
			break;
			case E_intmax | E_unsigned:
				*(va_arg (status->arg, uintmax_t *)) = (uintmax_t) (value * sign);
				break;

			case E_size:
				/* E_size always implies unsigned */
				*(va_arg (status->arg, size_t *)) = (size_t) (value * sign);
				break;

			case E_ptrdiff:
				/* E_ptrdiff always implies signed */
				*(va_arg (status->arg, ptrdiff_t *)) = (ptrdiff_t) (value * sign);
				break;

			default:
				//puts( "UNSUPPORTED SCANF FLAG COMBINATION" );
				return NULL; /* behaviour unspecified */
			}
			++(status->n);
		}
		return ++spec;
	}
	/* TODO: Floats. */
	return NULL;
}

int vsscanf (const char *s, const char *format, va_list ap)
{
	/* TODO: This function should interpret format as multibyte characters.  */
	struct _PDCLIB_status_t status;
	status.base = 0;
	status.flags = 0;
	status.n = 0;
	status.i = 0;
	status.current = 0;
	status.s = (char *) s;
	status.width = 0;
	status.prec = 0;

	va_copy (status.arg, ap);

	while (*format != '\0') {
		const char * rc;
		if ((*format != '%') || ((rc = _PDCLIB_scan (format, &status)) == format)) {
			/* No conversion specifier, match verbatim */
			if (isspace (*format)) {
				/* Whitespace char in format string: Skip all whitespaces */
				/* No whitespaces in input do not result in matching error */
				while (isspace (*status.s)) {
					++status.s;
					++status.i;
				}
			} else {
				/* Non-whitespace char in format string: Match verbatim */
				if (*status.s != *format) {
					if (*status.s == '\0' && status.n == 0) {
						/* Early input error */
						return EOF;
					}
					/* Matching error */
					return status.n;
				} else {
					++status.s;
					++status.i;
				}
			}
			++format;
		} else {
			/* NULL return code indicates error */
			if (rc == NULL) {
				if ((*status.s == '\n') && (status.n == 0)) {
					status.n = EOF;
				}
				break;
			}
			/* Continue parsing after conversion specifier */
			format = rc;
		}
	}
	return status.n;
}

int sscanf (const char *s, const char *format, ...)
{
	va_list list;
	va_start (list, format);
	int i = vsscanf (s, format, list);
	va_end (list);
	return i;
}

int vscanf (const char* format, va_list ap)
{
	char* buffer = malloc (4096);
	fgets (buffer, 4095, stdin);
	int ret = vsscanf (buffer, format, ap);
	free (buffer);
	return ret;
}

int scanf (const char* format, ...)
{
	va_list list;
	va_start (list, format);
	int i = vscanf (format, list);
	va_end (list);
	return i;
}















/*
* vfscanf.c --
*
*    Source code for the "vfscanf" library procedure.
*
* Copyright 1988 Regents of the University of California
* Permission to use, copy, modify, and distribute this
* software and its documentation for any purpose and without
* fee is hereby granted, provided that the above copyright
* notice appear in all copies.  The University of California
* makes no representations about the suitability of this
* software for any purpose.  It is provided "as is" without
* express or implied warranty.
*/

#define MAX_FLOAT_SIZE 350

int vfscanf (FILE* stream, const char* format, va_list args)
{
	int suppress;        /* true means scan value but don't actual
						 * modify an element of args. */
	int storeShort;        /* true means store a short value. */
	int storeLong;        /* true means store a long value. */
	int width;            /* Field width. */
	char formatChar;     /* Current character from format string.
						 * Eventually it ends up holding the format
						 * type (e.g. 'd' for decimal). */
	int streamChar;    /* Next character from stream. */
	int assignedFields;        /* Counts number of successfully-assigned
							   * fields. */
	int base;            /* Gives base for numbers:  0 means float,
						 * -1 means not a number.
						 */
	int sign;            /* true means negative sign. */
	char buf[MAX_FLOAT_SIZE + 1];
	/* Place to accumulate floating-point
	* number for processing. */
	char *ptr = (char *) 0;
	char *savedPtr, *end, *firstPtr;

	assignedFields = 0;
	streamChar = getc (stream);
	if (streamChar == EOF) {
		return(EOF);
	}

	/*
	* The main loop is to scan through the characters in format.
	* Anything but a '%' must match the next character from stream.
	* A '%' signals the start of a format field;  the formatting
	* information is parsed, the next value is scanned from the stream
	* and placed in memory, and the loop goes on.
	*/

	for (formatChar = *format; (formatChar != 0) && (streamChar != EOF);
		 format++, formatChar = *format) {

		/*
		* A white-space format character matches any number of
		* white-space characters from the stream.
		*/

		if (isspace (formatChar)) {
			while (isspace (streamChar)) {
				streamChar = getc (stream);
			}
			continue;
		}

		/*
		* Any character but % must be matched exactly by the stream.
		*/

		if (formatChar != '%') {
			if (streamChar != formatChar) {
				break;
			}
			streamChar = getc (stream);
			continue;
		}

		/*
		* Parse off the format control fields.
		*/

		suppress = false;
		storeLong = false;
		storeShort = false;
		width = -1;
		format++;
		formatChar = *format;
		if (formatChar == '*') {
			suppress = true;
			format++;
			formatChar = *format;
		}
		if (isdigit (formatChar)) {
			width = strtoul (format, &end, 10);
			format = end;
			formatChar = *format;
		}
		if (formatChar == 'l') {
			storeLong = true;
			format++;
			formatChar = *format;
		}
		if (formatChar == 'h') {
			storeShort = true;
			format++;
			formatChar = *format;
		}

		/*
		* Skip any leading blanks in the input (except for 'c' format).
		* Also, default the width to infinity, except for 'c' format.
		*/

		if ((formatChar != 'c') && (formatChar != '[')) {
			while (isspace (streamChar)) {
				streamChar = getc (stream);
			}
		}
		if ((width <= 0) && (formatChar != 'c')) {
			width = 1000000;
		}

		/*
		* Check for EOF again after parsing away the white space.
		*/
		if (streamChar == EOF) {
			break;
		}

		/*
		* Process the conversion character.  For numbers, this just means
		* turning it into a "base" number that indicates how to read in
		* a number.
		*/

		base = -1;
		switch (formatChar) {

		case '%':
			if (streamChar != '%') {
				goto done;
			}
			streamChar = getc (stream);
			break;

		case 'D':
			storeShort = false;
		case 'd':
			base = 10;
			break;

		case 'O':
			storeShort = false;
		case 'o':
			base = 8;
			break;

		case 'X':
			storeShort = false;
		case 'x':
			base = 16;
			break;

		case 'E':
		case 'F':
			storeLong = true;
		case 'e':
		case 'f':
			base = 0;
			break;

			/*
			* Characters and strings are handled in exactly the same way,
			* except that for characters the default width is 1 and spaces
			* are not considered terminators.
			*/

		case 'c':
			if (width <= 0) {
				width = 1;
			}
		case 's':
			if (suppress) {
				while ((width > 0) && (streamChar != EOF)) {
					if (isspace (streamChar) && (formatChar == 's')) {
						break;
					}
					streamChar = getc (stream);
					width--;
				}
			} else {
				ptr = va_arg (args, char *);
				while ((width > 0) && (streamChar != EOF)) {
					if (isspace (streamChar) && (formatChar == 's')) {
						break;
					}
					*ptr = streamChar;
					ptr++;
					streamChar = getc (stream);
					width--;
				}
				if (formatChar == 's') {
					*ptr = 0;
				}
				assignedFields++;
			}
			break;

		case '[':
			format++; formatChar = *format;
			if (formatChar == '^') {
				format++;
			}
			if (!suppress) {
				firstPtr = ptr = va_arg (args, char *);
			}
			savedPtr = (char*) format;
			while ((width > 0) && (streamChar != EOF)) {
				format = savedPtr;
				while (true) {
					if (*format == streamChar) {
						if (formatChar == '^') {
							goto stringEnd;
						} else {
							break;
						}
					}
					if ((*format == ']') || (*format == 0)) {
						if (formatChar == '^') {
							break;
						} else {
							goto stringEnd;
						}
					}
					format++;
				}
				if (!suppress) {
					*ptr = streamChar;
					ptr++;
				}
				streamChar = getc (stream);
				width--;
			}
		stringEnd:
			if (ptr == firstPtr) {
				goto done;
			}
			while ((*format != ']') && (*format != 0)) {
				format++;
			}
			formatChar = *format;
			if (!suppress) {
				*ptr = 0;
				assignedFields++;
			}
			break;

			/*
			* Don't ask why, but for compatibility with UNIX, a null
			* conversion character must always return EOF, and any
			* other conversion character must be treated as decimal.
			*/

		case 0:
			ungetc (streamChar, stream);
			return(EOF);

		default:
			base = 10;
			break;
		}

		/*
		* If the field wasn't a number, then everything was handled
		* in the switch statement above.  Otherwise, we still have
		* to read in a number.  This gets handled differently for
		* integers and floating-point numbers.
		*/

		if (base < 0) {
			continue;
		}

		if (streamChar == '-') {
			sign = true;
			width -= 1;
			streamChar = getc (stream);
		} else {
			sign = false;
			if (streamChar == '+') {
				width -= 1;
				streamChar = getc (stream);
			}
		}

		/*
		* If we're supposed to be parsing a floating-point number, read
		* the digits into a temporary buffer and use the conversion library
		* routine to convert them.
		*/

	#define COPYCHAR \
*ptr = streamChar; ptr++; width--; streamChar = getc(stream);

		if (base == 0) {
			if (width > MAX_FLOAT_SIZE) {
				width = MAX_FLOAT_SIZE;
			}
			ptr = buf;
			while ((width > 0) && isdigit (streamChar)) {
				COPYCHAR;
			}
			if ((width > 0) && (streamChar == '.')) {
				COPYCHAR;
			}
			while ((width > 0) && isdigit (streamChar)) {
				COPYCHAR;
			}
			if ((width > 0) && ((streamChar == 'e') || (streamChar == 'E'))) {
				COPYCHAR;
				if ((width > 0) &&
					((streamChar == '+') || (streamChar == '-'))) {
					COPYCHAR;
				}
				while ((width > 0) && isdigit (streamChar)) {
					COPYCHAR;
				}
			}
			*ptr = 0;

			if (ptr == buf) {        /* Not a valid number. */
				goto done;
			}

			if (!suppress) {
				double d;
				d = atof (buf);
				if (sign) {
					d = -d;
				}
				if (storeLong) {
					*(va_arg (args, double *)) = d;
				} else {
					*(va_arg (args, float *)) = d;
				}
				assignedFields++;
			}
		} else {
			/*
			* This is an integer.  Use special-purpose code for the
			* three supported bases in order to make it run fast.
			*/

			int i;
			int anyDigits;

			i = 0;
			anyDigits = false;
			if (base == 10) {
				while ((width > 0) && isdigit (streamChar)) {
					i = (i * 10) + (streamChar - '0');
					streamChar = getc (stream);
					anyDigits = true;
					width -= 1;
				}
			} else if (base == 8) {
				while ((width > 0) && (streamChar >= '0')
					   && (streamChar <= '7')) {
					i = (i << 3) + (streamChar - '0');
					streamChar = getc (stream);
					anyDigits = true;
					width -= 1;
				}
			} else {
				while (width > 0) {
					if (isdigit (streamChar)) {
						i = (i << 4) + (streamChar - '0');
					} else if ((streamChar >= 'a') && (streamChar <= 'f')) {
						i = (i << 4) + (streamChar + 10 - 'a');
					} else if ((streamChar >= 'A') && (streamChar <= 'F')) {
						i = (i << 4) + (streamChar + 10 - 'A');
					} else {
						break;
					}
					streamChar = getc (stream);
					anyDigits = true;
					width--;
				}
			}
			if (!anyDigits) {
				goto done;
			}
			if (sign) {
				i = -i;
			}
			if (!suppress) {
				if (storeShort) {
					*(va_arg (args, short *)) = i;
				} else {
					*(va_arg (args, int *)) = i;
				}
				assignedFields++;
			}
		}
	}

done:
	ungetc (streamChar, stream);
	if ((streamChar == EOF) && (assignedFields == 0)) {
		return(EOF);
	}
	return(assignedFields);
}

int fscanf (FILE* s, const char* format, ...)
{
	va_list list;
	va_start (list, format);
	int i = vfscanf (s, format, list);
	va_end (list);
	return i;
}

int setvbuf (FILE* stream, char* buffer, int mode, size_t size)
{
	if (stream == stdin) {
		//I can't be bothered buffering the stdin stream at the moment...
		return 0xBADCAFE;
	}
	if (!buffer) {
		buffer = malloc (size + 4);
	}

	switch (mode) {
	case _IOFBF:
		// OUT: this means that we write to 'buffer' until it is flushed or full, then we write it to the file
		// IN:  this means that anything that should go to stdin goes to here first, then when flushed or full send to stdin

		if (stream == stdout__) {
			charsSinceLastStdoutFlush = 0;
			bufferStdout = true;
			autoFlush = false;
			stdoutFlushCount = size;
		}

		break;

	case _IOLBF:
		// OUT: this means that we write to 'buffer' until it is flushed or we get a newline character, then we write it to the file
		// IN:  this means that anything that should go to stdin goes to here first, then on the same conditions as above, send to stdin

		break;

	case _IONBF:
	default:

		// this means we can do nothing! yay!
		break;
	}

	return 0;
}

void setbuf (FILE* stream, char* buffer)
{
	if (buffer) {
		setvbuf (stream, buffer, _IOFBF, BUFSIZ);
	} else {
		setvbuf (stream, 0, _IONBF, BUFSIZ);
	}
}

int rename (const char* old, const char* newn)
{
	return SystemCall (SC_Rename, 0, (int)((size_t) old), (void*) newn);
}