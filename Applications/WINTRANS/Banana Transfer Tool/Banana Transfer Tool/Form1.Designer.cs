﻿namespace Banana_Transfer_Tool
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.nextButton1 = new System.Windows.Forms.Button();
            this.cancel1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(43, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(388, 78);
            this.label1.TabIndex = 0;
            this.label1.Text = "Welcome to the Windows to Banana Transfer Tool.\r\n\r\nThis tool allows your files an" +
    "d settings to be transferred from Windows to Banana.\r\n\r\n\r\nPress Next to get star" +
    "ted.\r\n";
            this.label1.Click += new System.EventHandler(this.Label1_Click);
            // 
            // nextButton1
            // 
            this.nextButton1.Location = new System.Drawing.Point(426, 225);
            this.nextButton1.Name = "nextButton1";
            this.nextButton1.Size = new System.Drawing.Size(75, 23);
            this.nextButton1.TabIndex = 1;
            this.nextButton1.Text = "Next";
            this.nextButton1.UseVisualStyleBackColor = true;
            this.nextButton1.Click += new System.EventHandler(this.Button1_Click);
            // 
            // cancel1
            // 
            this.cancel1.Location = new System.Drawing.Point(46, 224);
            this.cancel1.Name = "cancel1";
            this.cancel1.Size = new System.Drawing.Size(75, 23);
            this.cancel1.TabIndex = 2;
            this.cancel1.Text = "Cancel";
            this.cancel1.UseVisualStyleBackColor = true;
            this.cancel1.Click += new System.EventHandler(this.Cancel1_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(540, 282);
            this.Controls.Add(this.cancel1);
            this.Controls.Add(this.nextButton1);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Windows to Banana Transfer Tool";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button nextButton1;
        private System.Windows.Forms.Button cancel1;
    }
}

