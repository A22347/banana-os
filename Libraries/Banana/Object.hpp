#ifndef Object_hpp
#define Object_hpp

#include "Banana/Core.hpp"

/*! \deprecated*/
class Object {
protected:
    void* encodeRefs[256];
    int nextEncode;
public:
    Object();
    ~Object();
    void saveObject();
    void loadObject();
    void encode(void* a);
    char* info();
};

#endif
