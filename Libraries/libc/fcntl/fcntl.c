//
//  fcntl.c
//  Banana libc
//
//  Created by Alex Boxall on 16/10/18.
//  Copyright © 2018 Alex Boxall. All rights reserved.
//

#include "sys/types.h"
#include "fcntl.h"

int creat(const char* path, mode_t mode) {
    return open(path, O_CREAT | O_WRONLY | O_TRUNC, mode);
}

int fcntl(int fildes, int cmd, ...) {
    //third argument (...) depends on the command
    
    
    return 1;
}

int open(const char* path, int oflag, ...) {
    //translate the oflag (e.g. O_CREAT, O_WRONLY, etc.) into a flag list for fopen();
    
    //the ... is used for a third argument if the file is being created,
    //otherwise it shouldn't be used
    return 1;
}

