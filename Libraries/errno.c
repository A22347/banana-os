#include <errno.h>

int errno = 0;

char errorNames[256][48] = {
	"Unknown error",
	"Argument list too long",
	"Permission denied",
	"Address in use",
	"Address not avaliable",
	"Address family not supported",
	"Resource unavaliable, try again",
	"Connection already in progress",
	"Bad file name",
	"Bad message",
	"Device or resource busy",
	"Operation cancelled",
	"No child processes",
	"Connection aborted",
	"Connection refused",
	"Connection reset",
	"Resource deadlock would occur",
	"Destination address required",
	"Mathematics argument out of domain of function",
	"File exists",
	"Bad address",
	"File too big",
	"Host is unreachable",
	"Identifier removed",
	"Illegal byte sequence",
	"Operation in progress"

	// ...
};