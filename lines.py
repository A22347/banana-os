#don't ask me why I made this program

import os
lines = 0
ext = ['c', 'h', 'cpp', 'hpp', 'asm', 's']

for path, subdirs, files in os.walk(os.getcwd()):
    for name in files:
        if name.split('.')[-1] in ext:
            j = len(open(os.path.join(path, name), 'rb').readlines())
            lines += j
        
print(lines, 'lines')
print('Windows 3 is', round(2000000 / lines, 2), 'times larger.')