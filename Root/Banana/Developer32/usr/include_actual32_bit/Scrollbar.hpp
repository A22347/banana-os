//
//  Scrollbar.hpp
//  GUI
//
//  Created by Alex Boxall on 26/7/19.
//  Copyright © 2019 Alex Boxall. All rights reserved.
//

#ifndef Scrollbar_hpp
#define Scrollbar_hpp


#include "Control.hpp"
#include "Button.hpp"
#include "Page.hpp"

class Scrollbar : public Control {
private:
protected:
    Page* linkedControl;
    Button* tbtn;
    Button* bbtn;
    int scrolled;

public:
    void doScroll(bool up);
    
    void keyboard(KeyState k);
    Scrollbar (Control* parent, Page* linked);
    
    const char* debugName();
    Scrollbar* copy();
    bool canHoldFocus();
    
    void internalDraw();
};

#endif /* Scrollbar_hpp */
