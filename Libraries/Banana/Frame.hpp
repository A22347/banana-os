#ifndef __FRAME_HPP__
#define __FRAME_HPP__

#include "Core.hpp"
#include "Widget.hpp"
#include "Window.hpp"

class Frame : public Widget, public Window
{
	friend class Window;
	friend class Widget;

protected:
	int x;
	int y;
	int w;
	int h;

public:
	Frame (Window& parent, int _x, int _y, int _width, int _height);
	
	void selfupdate (uint64_t ev);

	void setTitle (char* nTitle);
	char* getTitle ();
	void setPosition (short int nX, short int nY);      ///< Incomplete - do not use

	void movable (bool a = false);    ///< Incomplete - do not use
	bool isMovable ();                ///< Incomplete - do not use

	void closable (bool a = false);   ///< Incomplete - do not use
	bool isClosable ();               ///< Incomplete - do not use

	void display ();        ///< Properly create the window
	void kill ();           ///< Delete the window

	bool isAlive ();        ///< Incomplete - do not use

	void update ();                 ///< Updates the window

	void drawPixel (uint16_t x, uint16_t y, uint32_t col);
	void drawCharacter (char c, uint16_t x, uint16_t y, uint32_t bg = 0xFFFFFF, uint32_t fg = 0, int font = 0);

	void addWidgetChild (Widget* child);

	void setCursorType (enum MouseTypes type);
	enum MouseTypes getCursorType ();

	void write (char c, int x, int y, const char* font = "SYSTEM", int style = 0);            ///< Writes a character to the window
	void write (const char* c, int x, int y, const char* font = "SYSTEM", int style = 0);     ///< Writes a string to the window

	int getWidth ();
	int getHeight ();
	int getX ();
	int getY ();
};

#endif