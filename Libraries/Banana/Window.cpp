#include "Window.hpp"

#define CHECK_BIT(var,pos) ((var) & (1<<(pos)))

bool slantText = false;

/**
\brief Window constructor.
Sets position and size values, and sets up pointers.
*/
Window::Window (char* nTitle, int width__, int height__, int x__, int y__)
{
	widgetRoot = new WidgetChildren;
	widgetRoot->next = (WidgetChildren*) 0;
	widgetRoot->child = (Widget*) 0;
	widgetCurrent = widgetRoot;

	width = width__;
	height = height__;

	x = x__ == -1 ? 0xFFFF : x__;
	y = y__ == -1 ? 0xFFFF : y__;

	strcpy (title, nTitle);
	strcpy (showtitle, nTitle);

	while ((unsigned int) SystemCall (SC_GetWindowNumberFromWindowName, 0, 0, title) != 299999) {
		title[strlen (title)] = ' ';
		title[strlen (title)] = 0;
	}

	cursorType = Mouse_Normal;
	botherToUpdate = false;
	alive = true;
	windowno = 0;
}


Window::~Window ()
{
	//delete all pointers stored in root
}

/**
\brief Binds a widget to a window
*/
void Window::addWidgetChild (Widget* child)
{
	widgetCurrent = widgetRoot;
	while (widgetCurrent->next) widgetCurrent = widgetCurrent->next;
	widgetCurrent->child = child;
	widgetCurrent->next = new WidgetChildren;
	widgetCurrent = widgetCurrent->next;
	widgetCurrent->next = (WidgetChildren*) 0;
	widgetCurrent->child = (Widget*) 0;
	botherToUpdate = true;
}


/**
\brief Internally update the window, do not redraw GUI elements
*/
void Window::updatewindow ()
{
	APISystemCall (ASCUpdate, 1, windowno, 0);
}

/**
\brief Returns the window's internal ID. Should only be used by internal code.
*/
int Window::getWindowNumber ()
{
	return windowno;
}

/**
\brief Updates the window, used internally.
*/
void Window::_update (Event_t data)
{
	cursorType = Mouse_Normal;
	botherToUpdate = false;

	widgetCurrent = widgetRoot;
	while (widgetCurrent->next && widgetCurrent->child) {

		if (data.type == EventKeyPress && widgetCurrent->child->EventKeyPressEarlyHandler) widgetCurrent->child->EventKeyPressEarlyHandler (data);

		if (widgetCurrent->child->autoLooseFocusOnMouseClick () && (data.type == EventMouseClick || data.type == EventMouseUp)) {
			widgetCurrent->child->focus = false;        //the textfield hander can decide if it needs to regain focus
		}
		widgetCurrent->child->selfupdate (data);
		
		if (data.type == EventDragDrop			&& widgetCurrent->child->EventDragDropHandler)			widgetCurrent->child->EventDragDropHandler (data);
		if (data.type == EventDragEnter			&& widgetCurrent->child->EventDragEnterHandler)			widgetCurrent->child->EventDragEnterHandler (data);
		if (data.type == EventDragLeave			&& widgetCurrent->child->EventDragLeaveHandler)			widgetCurrent->child->EventDragLeaveHandler (data);
		if (data.type == EventEnter				&& widgetCurrent->child->EventEnterHandler)				widgetCurrent->child->EventEnterHandler (data);
		if (data.type == EventHelpRequested		&& widgetCurrent->child->EventHelpRequestedHandler)		widgetCurrent->child->EventHelpRequestedHandler (data);
		if (data.type == EventKeyDown			&& widgetCurrent->child->EventKeyDownHandler)			widgetCurrent->child->EventKeyDownHandler (data);
		if (data.type == EventKeyPress			&& widgetCurrent->child->EventKeyPressHandler)			widgetCurrent->child->EventKeyPressHandler (data);
		if (data.type == EventKeyUp				&& widgetCurrent->child->EventKeyUpHandler)				widgetCurrent->child->EventKeyUpHandler (data);
		if (data.type == EventLeave				&& widgetCurrent->child->EventLeaveHandler)				widgetCurrent->child->EventLeaveHandler (data);
		if (data.type == EventMouseClick		&& widgetCurrent->child->EventMouseClickHandler)		widgetCurrent->child->EventMouseClickHandler (data);
		if (data.type == EventMouseDown			&& widgetCurrent->child->EventMouseDownHandler)			widgetCurrent->child->EventMouseDownHandler (data);
		if (data.type == EventMouseDrag			&& widgetCurrent->child->EventMouseDragHandler)			widgetCurrent->child->EventMouseDragHandler (data);
		if (data.type == EventMouseEnter		&& widgetCurrent->child->EventMouseEnterHandler)		widgetCurrent->child->EventMouseEnterHandler (data);
		if (data.type == EventMouseHover		&& widgetCurrent->child->EventMouseHoverHandler)		widgetCurrent->child->EventMouseHoverHandler (data);
		if (data.type == EventMouseLeave		&& widgetCurrent->child->EventMouseLeaveHandler)		widgetCurrent->child->EventMouseLeaveHandler (data);
		if (data.type == EventMouseMove			&& widgetCurrent->child->EventMouseMoveHandler)			widgetCurrent->child->EventMouseMoveHandler (data);
		if (data.type == EventMouseUp			&& widgetCurrent->child->EventMouseUpHandler)			widgetCurrent->child->EventMouseUpHandler (data);
		if (data.type == EventMove				&& widgetCurrent->child->EventMoveHandler)				widgetCurrent->child->EventMoveHandler (data);
		if (data.type == EventResize			&& widgetCurrent->child->EventResizeHandler)			widgetCurrent->child->EventResizeHandler (data);
		if (data.type == EventScrollWheel		&& widgetCurrent->child->EventScrollWheelHandler)		widgetCurrent->child->EventScrollWheelHandler (data);
		if (data.type == EventWindowResize		&& widgetCurrent->child->EventWindowResizeHandler)		widgetCurrent->child->EventWindowResizeHandler (data);

		widgetCurrent = widgetCurrent->next;
	}

	//Only show the changes if: the widgets request to be updated
	if (botherToUpdate) {
		updatewindow ();
	}

	//HACK: We shouldn't need to use a 'lastTime' variable...
	if (cursorType == Mouse_Normal && lastTimeCursorType == Mouse_Normal) {
		OSArrowCursor ();
	} else if (cursorType == Mouse_HourGlass/* && lastTimeCursorType == Mouse_HourGlass*/) {
		OSWaitCursor ();
	} else if (cursorType == Mouse_Text/* && lastTimeCursorType == Mouse_Text*/) {
		OSTextCursor ();
	}

	lastTimeCursorType = cursorType;
	cursorType = Mouse_Normal;

	botherToUpdate = false;
}

/**
\brief Updates the window.
*/
void Window::update ()
{
	Event_t n;
	n.type = EventNull;
	_update (n);
}

/**
\brief Sets the window title.
*/
void Window::setTitle (char* nTitle)
{
	strcpy (showtitle, nTitle);
	SystemCall (SC_ChangeWindowName, 0, windowno, nTitle);
}

/**
\brief Returns the window title.
*/
char* Window::getTitle ()
{
	return showtitle;
}

/**
\brief Sets the position of the window.
*/
void Window::setPosition (short int nX, short int nY)
{
	x = nX;
	y = nY;
}

/**
\deprecated
*/
void Window::movable (bool a)
{
	canMove = a;
}

bool Window::isMovable ()
{
	return canMove;
}

void Window::closable (bool a)
{
	canClose = a;
}

bool Window::isClosable ()
{
	return canClose;
}

void Window::__create ()
{
	if (windowno) return;		//window displayed already

	uint32_t pos = (width & 0xFFFF) << 16;
	pos |= height & 0xFFFF;

	uint32_t xy = (x & 0xFFFF) << 16;
	xy |= y & 0xFFFF;

	windowno = APISystemCall (ASCGetNextWindow, 0, 0, 0);
	System::Log ("CREATING WINDOW, WITH NUMBER");
	for (int i = 0; i < windowno; ++i) {
		System::Log ("*");
	}
	APISystemCall (ASCWindowNew, pos, xy, title);
}

void Window::drawPixel (uint16_t _x, uint16_t _y, uint32_t _col)
{
	if (!windowno) return;		//window not displayed yet
	SystemCall (SC_PlotPixel, _col, (_x << 16) | _y, title);
}

typedef struct __WindowData
{
	uint16_t x;
	uint16_t y;
	uint32_t bg;
	uint32_t fg;
	char text;
	int fontno;

} __WindowData;

void Window::drawCharacter (char ac, uint16_t ax, uint16_t ay, uint32_t abg, uint32_t afg, int fontno)
{
	if (!windowno) return;		//window not displayed yet

	__WindowData wd;
	wd.x = ax;
	wd.y = ay;
	wd.bg = abg;
	wd.fg = afg;
	wd.text = ac;
	wd.fontno = fontno;
	APISystemCall (ASCWindowPutchar, windowno, slantText, &wd);
}

void Window::__delete ()
{
	if (!windowno) return;		//window not displayed yet
	APISystemCall (ASCWindowDelete, windowno, 0, 0);
}

void Window::display ()
{
	__create ();
}

void Window::kill ()
{
	__delete ();
}

void Window::main ()
{}

bool Window::isAlive ()
{
	if (!windowno) return false;		//window not displayed yet
	return SystemCall (SC_GetWindowNumberFromWindowName, 0, 0, title) == 299999 ? false : true;
}

void Window::write (char text, int xn, int yn, const char* font, int style)
{
	drawCharacter (text, xn, yn, SystemCall (SC_GetWindowColour, 0, 0, 0), 0, SystemCall (SC_GetFontNumberFromName, 0, 0, (void*) font));
	updatewindow ();
}

void Window::write (const char* text, int xn, int yn, const char* font, int style)
{
	//clear			: 0x1000000
	//italic + clear: 0x1000001

	uint16_t pixelPointer = xn;
	slantText = false;

	for (int i = 0; text[i]; ++i) {
		if (text[i] == '\n') {
			++yn;
			pixelPointer = xn;
			continue;
		}
		uint8_t width = APISystemCall (ASCGetCharacterWidth, text[i], SystemCall (SC_GetFontNumberFromName, 0, 0, (void*) font), 0);

		if (style == 2) {                                                   //italic
			slantText = true;
			drawCharacter (text[i], pixelPointer, yn, 0x1000001, 0, SystemCall (SC_GetFontNumberFromName, 0, 0, (void*) font));
		} else if (style == 1) {                                            //bold
			drawCharacter (text[i], pixelPointer, yn, SystemCall (SC_GetWindowColour, 0, 0, 0), 0, SystemCall (SC_GetFontNumberFromName, 0, 0, (void*) font));
			drawCharacter (text[i], pixelPointer + 1, yn, 0x1000000, 0x5A5A5A, SystemCall (SC_GetFontNumberFromName, 0, 0, (void*) font));
			drawCharacter (text[i], pixelPointer, yn, 0x1000000, 0, SystemCall (SC_GetFontNumberFromName, 0, 0, (void*) font));
		} else {
			drawCharacter (text[i], pixelPointer, yn, SystemCall (SC_GetWindowColour, 0, 0, 0), 0, SystemCall (SC_GetFontNumberFromName, 0, 0, (void*) font));
		}
		pixelPointer += width;
	}
	updatewindow ();
}

int Window::getWidth ()
{
	if (!windowno) return width;		//window not displayed yet 
	width = SystemCall (SC_WindowWidth, windowno, 0, 0);
	return width;
}

int Window::getHeight ()
{
	if (!windowno) return height;		//window not displayed yet 
	height = SystemCall (SC_WindowHeight, windowno, 0, 0);
	return height;
}

int Window::getX ()
{
	if (!windowno) return x;		//window not displayed yet 
	x = SystemCall (SC_WindowPositionX, windowno, 0, 0);
	return x;
}

int Window::getY ()
{
	if (!windowno) return y;		//window not displayed yet 
	y = SystemCall (SC_WindowPositionY, windowno, 0, 0);
	return y;
}

void Window::setCursorType (enum MouseTypes type)
{
	cursorType = type;
}

enum MouseTypes Window::getCursorType ()
{
	return cursorType;
}

#include <Banana/Label.hpp>
#include <Banana/Button.hpp>
void alertBox (char* title, char* text)
{
	int longestLineWidth = 0;
	int currentWidth = 0;

	for (int i = 0; text[i]; ++i) {
		if (text[i] == '\n') {
			if (currentWidth > longestLineWidth) {
				longestLineWidth = currentWidth;
			}
			currentWidth = 0;
			continue;
		}
		currentWidth += APISystemCall (ASCGetCharacterWidth, text[i], SystemCall (SC_GetFontNumberFromName, 0, 0, (void*) "SYSTEM"), 0);
	}
	if (currentWidth > longestLineWidth) {
		longestLineWidth = currentWidth;
	}

	Window* m = new Window (title, (longestLineWidth + 6) / 8, 9);
	m->display ();
	Label* l = new Label (*m, text, 3, 2);
	Button* b = new Button (*m, "OK", (longestLineWidth + 6) / 16 - 6, 12);
	//b->command ();
	Window window = *m;

	volatile uint64_t* eventData;
	uint64_t* original = (uint64_t*) eventData;
	uint8_t windownumber = SystemCall (SC_GetWindowNumberFromWindowName, 0, 0, window.getTitle ());

	uint16_t interruptCounter = SystemCall (SC_GetNumberOfEventsSent, 0, 0, 0);
	volatile uint64_t* eventStream = (volatile uint64_t*) SystemCall (SC_EventData, 0, 0, 0);      //this just gives a the stream's pointer

	/*while (1) {
		while (*(eventStream + interruptCounter)) {
			Event_t eventData = *(eventStream + interruptCounter);
			if (CHECK_BIT (eventData, 0) && SystemCall (SC_FocusNumber, 0, 0, 0) == windownumber) {
				int x = SystemCall (SC_WindowPositionX, windownumber, 0, 0);
				int y = SystemCall (SC_WindowPositionY, windownumber, 0, 0);

				window._update (eventData);
			}
		}
	}*/
}