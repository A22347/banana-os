#pragma once

#include <core/kernel.h>
#include <gui/terminal.h>
#include <devss/devices.h>

extern Device* powerManagerDevice;

struct RSDPDescriptor
{
	char Signature[8];
	uint8_t Checksum;
	char OEMID[6];
	uint8_t Revision;
	uint32_t RsdtAddress;

} __attribute__ ((packed));

struct RSDPDescriptor20
{
	struct RSDPDescriptor firstPart;

	uint32_t Length;
	uint64_t XsdtAddress;
	uint8_t ExtendedChecksum;
	uint8_t reserved[3];

} __attribute__ ((packed));

uint8_t* findRSDP ();
uint8_t* findRSDT (uint8_t* ptr);

struct ACPISDTHeader
{
	char Signature[4];
	uint32_t Length;
	uint8_t Revision;
	uint8_t Checksum;
	char OEMID[6];
	char OEMTableID[8];
	uint32_t OEMRevision;
	uint32_t CreatorID;
	uint32_t CreatorRevision;
} __attribute__ ((packed));

struct RSDT
{
	struct ACPISDTHeader h;
	uint32_t* PointerToOtherSDT;
};

struct XSDT
{
	struct ACPISDTHeader h;
	uint64_t* PointerToOtherSDT;
};

struct BOOTTbl
{
	struct ACPISDTHeader h;
	uint8_t cmosOffset;
	uint8_t reserved1;
	uint8_t reserved2;
	uint8_t reserved3;
};

typedef struct CachedHardwareInformation_t
{
	bool valid;
	struct
	{

		uint64_t RSDPpointer;
		uint64_t RSDTpointer;
		uint64_t MADTpointer;

		//This just a single uint64_t
		uint64_t usingXSDT : 1;

		uint64_t RSDPpointerValid : 1;
		uint64_t RSDTpointerValid : 1;
		uint64_t MADTpointerValid : 1;
		uint64_t usingXSDTValid : 1;
		uint64_t : 59;
	};

} CachedHardwareInformation_t;

extern bool usingXSDT;
extern uint8_t* RSDPpointer;
extern uint8_t* RSDTpointer;
extern uint8_t* MADTpointer;

uint8_t* findDataTable (uint8_t* ptr, char name[]);

struct MADTHeader
{
	char Signature[4];
	uint32_t Length;
	uint8_t Revision;
	uint8_t Checksum;
	char OEMID[6];
	char OEMTableID[8];
	uint32_t OEMRevision;
	uint32_t CreatorID;
	uint32_t CreatorRevision;
	uint32_t localControllerAddress;
	uint32_t flags;

	uint8_t data[2000];

} __attribute__ ((packed));

void parseMADT (uint8_t* ptr);
void scanACPI ();
uint32_t getCPUNumber ();

typedef struct GenericAddressStructure
{
	uint8_t AddressSpace;
	uint8_t BitWidth;
	uint8_t BitOffset;
	uint8_t AccessSize;
	uint64_t Address;

} GenericAddressStructure;

struct FADT
{
	struct   ACPISDTHeader h;
	uint32_t FirmwareCtrl;
	uint32_t Dsdt;

	// field used in ACPI 1.0; no longer in use, for compatibility only
	uint8_t  Reserved;

	uint8_t  PreferredPowerManagementProfile;
	uint16_t SCI_Interrupt;
	uint32_t SMI_CommandPort;
	uint8_t  AcpiEnable;
	uint8_t  AcpiDisable;
	uint8_t  S4BIOS_REQ;
	uint8_t  PSTATE_Control;
	uint32_t PM1aEventBlock;
	uint32_t PM1bEventBlock;
	uint32_t PM1aControlBlock;
	uint32_t PM1bControlBlock;
	uint32_t PM2ControlBlock;
	uint32_t PMTimerBlock;
	uint32_t GPE0Block;
	uint32_t GPE1Block;
	uint8_t  PM1EventLength;
	uint8_t  PM1ControlLength;
	uint8_t  PM2ControlLength;
	uint8_t  PMTimerLength;
	uint8_t  GPE0Length;
	uint8_t  GPE1Length;
	uint8_t  GPE1Base;
	uint8_t  CStateControl;
	uint16_t WorstC2Latency;
	uint16_t WorstC3Latency;
	uint16_t FlushSize;
	uint16_t FlushStride;
	uint8_t  DutyOffset;
	uint8_t  DutyWidth;
	uint8_t  DayAlarm;
	uint8_t  MonthAlarm;
	uint8_t  Century;

	// reserved in ACPI 1.0; used since ACPI 2.0+
	uint16_t BootArchitectureFlags;

	uint8_t  Reserved2;
	uint32_t Flags;

	// 12 byte structure; see below for details
	GenericAddressStructure ResetReg;

	uint8_t  ResetValue;
	uint8_t  Reserved3[3];

	// 64bit pointers - Available on ACPI 2.0+
	uint64_t                X_FirmwareControl;
	uint64_t                X_Dsdt;

	GenericAddressStructure X_PM1aEventBlock;
	GenericAddressStructure X_PM1bEventBlock;
	GenericAddressStructure X_PM1aControlBlock;
	GenericAddressStructure X_PM1bControlBlock;
	GenericAddressStructure X_PM2ControlBlock;
	GenericAddressStructure X_PMTimerBlock;
	GenericAddressStructure X_GPE0Block;
	GenericAddressStructure X_GPE1Block;
};

extern struct FADT* FADTpointer;
extern CachedHardwareInformation_t CachedHardwareInformation;

void openACPI (Device* self, int a, int b, void* c);

