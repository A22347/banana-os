#include <hw/pci.h>
#include <hw/ahci.h>		//to call the driver
#include <hw/ide.h>			//to call the driver
#include <libk/string.h>
#include <core/memory.h>

char PCINames[16][256][9];

Device* pciDevice;
bool onlyBootDiskDrivers = true;

void getPCIDataReady ()
{
	/*
	REMEMBER THERE ARE ALSO SUB-IDs (CALLED Prog IFs)
	THAT PROVIDE MORE INFORMATION ABOUT THE TYPE OF 
	DEVICE, BUT WE WON'T LIST THEM HERE...
	*/

	strcpy (PCINames[0][0], "NOT VGA ");
	strcpy (PCINames[0][1], "VGA     ");

	strcpy (PCINames[1][0], "SCSI BUS");
	strcpy (PCINames[1][1], "IDE     ");
	strcpy (PCINames[1][2], "FLOPPY  ");
	strcpy (PCINames[1][3], "IPI BUS ");
	strcpy (PCINames[1][4], "RAID    ");
	strcpy (PCINames[1][5], "ATA     ");
	strcpy (PCINames[1][6], "SATA    ");
	strcpy (PCINames[1][7], "SA SCSI  ");
	strcpy (PCINames[1][0x80], "DISK    ");

	strcpy (PCINames[2][0], "ETHERNET");
	strcpy (PCINames[2][1], "TOKENRNG");
	strcpy (PCINames[2][2], "FDDI CNT");		//controller
	strcpy (PCINames[2][3], "ATM CNTR");
	strcpy (PCINames[2][4], "ISDN CNT");
	strcpy (PCINames[2][5], "WORLDFLP");
	strcpy (PCINames[2][6], "PICMG214");
	strcpy (PCINames[2][0x80], "NETWORK ");

	strcpy (PCINames[3][0], "VGA     ");
	strcpy (PCINames[3][1], "XGA     ");
	strcpy (PCINames[3][2], "3D CONTR");
	strcpy (PCINames[3][0x80], "DISPLAY ");

	strcpy (PCINames[4][0], "MM VIDEO");		//MULTIMEDIA video	
	strcpy (PCINames[4][1], "MM AUDIO");		//MULTIMEDIA AUDIO	
	strcpy (PCINames[4][2], "TELEPHNY");
	strcpy (PCINames[4][3], "AUDIO DV");
	strcpy (PCINames[4][0x80], "MULTIMDA");

	strcpy (PCINames[5][0], "RAM     ");
	strcpy (PCINames[5][1], "FLASH   ");
	strcpy (PCINames[5][0x80], "MEMORY  ");

	strcpy (PCINames[6][0], "HOST BGE");
	strcpy (PCINames[6][1], "ISA BGE ");
	strcpy (PCINames[6][2], "EISA BGE");
	strcpy (PCINames[6][3], "MCA BGE ");
	strcpy (PCINames[6][4], "PCI-PCI ");
	strcpy (PCINames[6][5], "PCMCIA  ");
	strcpy (PCINames[6][6], "NUBUS   ");
	strcpy (PCINames[6][7], "CARDBUS ");
	strcpy (PCINames[6][8], "RACEway ");
	strcpy (PCINames[6][9], "PCI-PCI2");
	strcpy (PCINames[6][0x0A], "InfB-PCI");
	strcpy (PCINames[6][0x80], "BRIDGE  ");

	strcpy (PCINames[7][0], "SERIAL  ");
	strcpy (PCINames[7][1], "PARALLEL");
	strcpy (PCINames[7][2], "MULTIPRT");
	strcpy (PCINames[7][3], "MODEM   ");
	strcpy (PCINames[7][4], "IEEE 488");
	strcpy (PCINames[7][5], "SMART CRD");
	strcpy (PCINames[7][0x80], "COMMUNIC");

	strcpy (PCINames[8][0], "INTERRPT");
	strcpy (PCINames[8][1], "DMA CNTR");
	strcpy (PCINames[8][2], "SYSTIMER");
	strcpy (PCINames[8][3], "RT CLOCK");
	strcpy (PCINames[8][4], "HOT PLUG");
	strcpy (PCINames[8][0x80], "SYSDEVIC");

	strcpy (PCINames[9][0], "KEYBOARD");
	strcpy (PCINames[9][1], "DIGITIZR");
	strcpy (PCINames[9][2], "MOUSE   ");
	strcpy (PCINames[9][3], "SCANNER ");
	strcpy (PCINames[9][4], "GAMEPORT");
	strcpy (PCINames[9][0x80], "INPUT DV");

	strcpy (PCINames[0xA][0], "G DOCKNG");
	strcpy (PCINames[0xA][0x80], "O DOCKNG");

	strcpy (PCINames[0xB][0x00], "i386 CPU");
	strcpy (PCINames[0xB][0x01], "i486 CPU");
	strcpy (PCINames[0xB][0x02], "PENTIUM ");
	strcpy (PCINames[0xB][0x10], "APHA CPU");
	strcpy (PCINames[0xB][0x20], "POWERPC ");
	strcpy (PCINames[0xB][0x30], "MIPS CPU");
	strcpy (PCINames[0xB][0x40], "CO-PRCSR");

	strcpy (PCINames[0xC][0], "IEEE1394");
	strcpy (PCINames[0xC][1], "ACCESbus");
	strcpy (PCINames[0xC][2], "SSA	   ");
	strcpy (PCINames[0xC][3], "USB     ");
	strcpy (PCINames[0xC][4], "FIBRE   ");
	strcpy (PCINames[0xC][5], "SMBus   ");
	strcpy (PCINames[0xC][6], "InfiniBn");
	strcpy (PCINames[0xC][7], "IPMI    ");
	strcpy (PCINames[0xC][8], "SERCOS  ");
	strcpy (PCINames[0xC][9], "CANbus  ");

	strcpy (PCINames[0xD][0x00], "iRDA CTR");
	strcpy (PCINames[0xD][0x01], "IR CONTR");
	strcpy (PCINames[0xD][0x10], "RF CONTR");
	strcpy (PCINames[0xD][0x11], "BLUTOOTH");
	strcpy (PCINames[0xD][0x12], "BROADBND");
	strcpy (PCINames[0xD][0x20], "ETHRNT A");
	strcpy (PCINames[0xD][0x21], "ETHRNT B");
	strcpy (PCINames[0xD][0x80], "WIRELESS");

	strcpy (PCINames[0xE][0], "ITLG I/O");

	strcpy (PCINames[0xF][0x00], "TV CONTR");
	strcpy (PCINames[0xF][0x01], "AUDIO CN");
	strcpy (PCINames[0xF][0x10], "VOICE CN");
	strcpy (PCINames[0xF][0x11], "DATA CNT");

	strcpy (PCINames[0x10][0x00], "NET ENCR");	//Network and Computing Encryption/Decryption
	strcpy (PCINames[0x10][0x10], "ENT ENCR");	//Entertainment Encryption/Decryption
	strcpy (PCINames[0x10][0x80], "ENCRYPT ");	//Other Encryption/Decryption

	strcpy (PCINames[0x11][0x00], "DIPO MOD");	//DPIO Modules
	strcpy (PCINames[0x11][0x01], "PERF CNT");	//Performance Counters
	strcpy (PCINames[0x11][0x10], "CSPT&FTM");	//Communications Syncrhonization Plus Time and Frequency Test / Measurment
	strcpy (PCINames[0x11][0x20], "MANGEMNT");	//Management Card
	strcpy (PCINames[0x11][0x80], "DATA ACQ");	//Other Data Acquisition/Signal Processing Controller  
}

uint16_t pciReadWord (uint8_t bus, uint8_t slot, uint8_t func, uint8_t offset)
{
	uint32_t address;
	uint32_t lbus = bus;
	uint32_t lslot = slot;
	uint32_t lfunc = func;

	address = (uint32_t) ((lbus << 16) | (lslot << 11) |
		(lfunc << 8) | (offset & 0xfc) | ((uint32_t) 0x80000000));

	outl (0xCF8, address); 
	return (uint16_t) ((inl (0xCFC) >> ((offset & 2) * 8)) & 0xFFFF);
}

void pciWriteWord(uint8_t bus, uint8_t slot, uint8_t func, uint8_t offset, uint16_t word)
{
	uint32_t address;
	uint32_t lbus = bus;
	uint32_t lslot = slot;
	uint32_t lfunc = func;

	address = (uint32_t) ((lbus << 16) | (lslot << 11) |
		(lfunc << 8) | (offset & 0xfc) | ((uint32_t) 0x80000000));

	outl(0xCF8, address);
	uint32_t dword = inl(0xCFC);
	if (offset & 0b10) {
		dword &= 0x00FFFF;
		dword |= ((uint32_t)word) << 16;
	} else {
		dword &= 0xFFFF0000;
		dword |= word;
	}

	outl(0xCF8, address);
	outl(0xCFC, dword);
}

uint16_t getVendorID (uint8_t bus, uint8_t slot, uint8_t function)
{
	return pciReadWord (bus, slot, function, 0);
}

uint16_t getHeaderType (uint8_t bus, uint8_t slot, uint8_t function)
{
	return pciReadWord (bus, slot, function, 14) & 0xFF;			//if not 15, try 8
}

uint16_t getClassCode (uint8_t bus, uint8_t slot, uint8_t function)
{
	uint8_t subClass  = pciReadWord (bus, slot, function, 10) & 0xFF;
	uint8_t classCode = pciReadWord (bus, slot, function, 10) >> 8;

	return subClass | (classCode << 8);
}

uint8_t getProgIF (uint8_t bus, uint8_t slot, uint8_t function)
{
	return pciReadWord (bus, slot, function, 8) >> 8;
}

uint8_t getRevisionID (uint8_t bus, uint8_t slot, uint8_t function)
{
	return pciReadWord (bus, slot, function, 8) & 0xFF;
}

uint8_t getInterruptNumber (uint8_t bus, uint8_t slot, uint8_t function)
{
	return pciReadWord (bus, slot, function, 0x3C) & 0xFF;
}

uint8_t getSecondaryBus(uint8_t bus, uint8_t slot, uint8_t function)
{
	return pciReadWord(bus, slot, function, 0x18) >> 8;
}

uint32_t getBARAddress (uint8_t barNo, uint8_t bus, uint8_t slot, uint8_t function)
{
	return (pciReadWord (bus, slot, function, 0x12 + barNo * 4) << 16) | pciReadWord (bus, slot, function, 0x10 + barNo * 4);
}

void checkBus(Device* self, uint8_t bus);

void loadPCIDriverForDevice (Device* self, PCIDeviceInfo info)
{
	//should be a lookup table in file rather than if/else
	//notice how the drivers all follow a similar format...
	//e.g. void (*) (PCIDeviceInfo info);

	if (info.vendorID == 0xFFFF) return;

	if ((info.classCode == 0x06) && (info.subClass == 0x04)) {
		uint8_t secondaryBus = getSecondaryBus(info.bus, info.slot, info.function);
		logk("SECONDARY BUS AT 0x%X\n", secondaryBus);
		checkBus(self, secondaryBus);
		return;
	}

	if (onlyBootDiskDrivers) {
		if (info.classCode == 1 && info.subClass == 6) {
			Device* newdev = malloc(sizeof(Device));
			initDeviceObject(newdev);
			newdev->devClass = info.classCode;
			newdev->subClass = info.subClass;
			newdev->progIF = info.progIF;
			newdev->interrupt = info.interrrupt;
			newdev->vendorID = info.vendorID;
			newdev->onDiskDriver = false;

			//so the child can get bar numbers, etc.
			newdev->internalDataSize = sizeof(PCIDeviceInfo);
			newdev->internalData = malloc(sizeof(PCIDeviceInfo));
			memcpy(newdev->internalData, &info, sizeof(PCIDeviceInfo));

			newdev->driverLocation = 0;
			strcpy(newdev->humanName, PCINames[info.classCode][info.subClass]);

			addChild(self, newdev); 
			//openDevice(newdev, 0, 0, 0);

			findSATADrives(info);

		} else if (info.classCode == 1 && info.subClass == 1) {
			Device* newdev = malloc(sizeof(Device));
			initDeviceObject(newdev);
			newdev->devClass = info.classCode;
			newdev->subClass = info.subClass;
			newdev->progIF = info.progIF;
			newdev->interrupt = info.interrrupt;
			newdev->vendorID = info.vendorID;
			newdev->onDiskDriver = false;

			//so the child can get bar numbers, etc.
			newdev->internalDataSize = sizeof(PCIDeviceInfo);
			newdev->internalData = malloc(sizeof(PCIDeviceInfo));
			memcpy(newdev->internalData, &info, sizeof(PCIDeviceInfo));

			newdev->driverLocation = 0;
			newdev->openPointer = openIDE;
			strcpy(newdev->humanName, PCINames[info.classCode][info.subClass]);

			addChild(self, newdev);
			logk("ABOUT TO OPEN IDE!\n");
			openDevice(newdev, 0, 0, &info);
		}
		return;
	}

	if (info.classCode == 0xC && info.subClass == 3) {
		logk("USB PCI DATA!\n");
		logk("PROF IF = 0x%X\n", info.progIF);
		

	} else {
		logk("Found non-builtin device. About to load driver and initialise it. %s\nclass = 0x%X\nsbcls = 0x%X\nvendr = 0x%X\nprgif = 0x%X\n", PCINames[info.classCode][info.subClass], info.classCode, info.subClass, info.vendorID, info.progIF);
		Device* newdev = malloc(sizeof(Device));
		initDeviceObject(newdev);
		newdev->devClass = info.classCode;
		newdev->subClass = info.subClass;
		newdev->progIF = info.progIF;
		newdev->interrupt = info.interrrupt;
		newdev->vendorID = info.vendorID;
		newdev->onDiskDriver = true;
		newdev->driverLocation = 0;

		//so the child can get bar numbers, etc.
		newdev->internalDataSize = sizeof(PCIDeviceInfo);
		newdev->internalData = malloc(sizeof(PCIDeviceInfo));
		memcpy(newdev->internalData, &info, sizeof(PCIDeviceInfo));

		strcpy(newdev->humanName, PCINames[info.classCode][info.subClass]);

		addChild(self, newdev);
		openDevice(newdev, 0, 0, 0);
	}
}

void getDeviceData (Device* self, uint8_t bus, uint8_t slot, uint8_t function)
{
	uint16_t classCode = getClassCode (bus, slot, function);
	uint8_t intno = getInterruptNumber (bus, slot, function);

	PCIDeviceInfo info;
	for (int i = 0; i < 6; ++i) {
		info.bar[i] = getBARAddress (i, bus, slot, function);
	}
	info.bus = bus;
	info.slot = slot;
	info.function = function;
	info.classCode = (classCode >> 8);
	info.subClass = (classCode & 0xFF);
	info.progIF = getProgIF (bus, slot, function);
	info.vendorID = getVendorID (bus, slot, function);
	info.interrrupt = intno;

	loadPCIDriverForDevice (self, info);
}

void checkDevice (Device* self, uint8_t bus, uint8_t device)
{
	uint8_t function = 0;
	uint16_t vendorID = getVendorID (bus, device, function);

	getDeviceData (self, bus, device, function);
	
	uint8_t headerType = getHeaderType (bus, device, function);

	if ((headerType & 0x80) != 0) {
		/* It is a multi-function device, so check remaining functions */
		for (function = 1; function < 8; function++) {
			if (getVendorID (bus, device, function) != 0xFFFF) {
				getDeviceData (self, bus, device, function);
			}
		}
	}
}

void checkBus(Device* self, uint8_t bus)
{
	logk("SCANNING PCI BUS 0x%X\n", bus);
	for (uint8_t device = 0; device < 32; device++) {
		checkDevice(self, bus, device);
	}
}

void checkAllBuses(Device* self)
{
	uint8_t function;
	uint8_t bus;

	uint16_t headerType = getHeaderType(0, 0, 0);

	if ((headerType & 0x80) == 0) {
		// Single PCI host controller
		checkBus(self, 0);
	} else {
		// Multiple PCI host controllers
		for (function = 0; function < 8; function++) {
			if (getVendorID(0, 0, function) != 0xFFFF) break;
			bus = function;
			checkBus(self, bus);
		}
	}
}

void scanPCI (Device* self)
{
	getPCIDataReady();
	checkAllBuses(self);

	/*uint16_t headerType = getHeaderType (0, 0, 0);

	for (uint16_t bus = 0; bus < 256; bus++) {
		checkBus(self, bus);
	}*/
}

void initPCI ()
{
}

void openPCI (Device* self, int a, int b, void* c)
{
	pciDevice = self;

	if (a == 0) {
		onlyBootDiskDrivers = true;
		initPCI ();
		scanPCI (self);		//TODO: only SATA + ATA (and BOOTDRVs in the future)
	} else {
		onlyBootDiskDrivers = false;
		scanPCI(self);
	}
}