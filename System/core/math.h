#pragma once

#include <stdbool.h>
#include <stdint.h>
#include <libk/math.h>
#include <libk/stdlib.h>		//includes math funcs as replacement for the old math.h

uint64_t getClockTime ();