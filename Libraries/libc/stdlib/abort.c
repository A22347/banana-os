//
//  abort.c
//  Banana libc
//
//  Created by Alex Boxall on 11/7/18.
//  Copyright © 2018 Alex Boxall. All rights reserved.
//

#ifndef _libk

#include "syscall.h"
#include "stdlib.h"
#include "signal.h"

_Noreturn void abort(void) {
    raise(SIGABRT);
    __i_system_call_abort();
    while (1);
}

#endif
