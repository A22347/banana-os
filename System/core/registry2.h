#pragma once

#include <stdint.h>
#include <stdbool.h>

char* readRegistryString(char* key);
uint64_t readRegistryInteger(char* key);

void writeRegistryString(char* key, char* newValue, bool system);
void writeRegistryInteger(char* key, uint64_t newValue, bool system);

void deleteRegistryKey(char* key, bool system);
bool registryKeyExists(char* key);

bool readRegistrySuccess();