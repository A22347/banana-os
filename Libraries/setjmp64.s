
;global setjmp
;global longjmp

align 8
setjmp:
	pop rsi
	mov rdx, 0
	mov rax, 0

	mov [rdi + 0], rbx
	mov [rdi + 8], rsp
	push rsi
	mov [rdi + 16], rbp
	mov [rdi + 24], r12
	mov [rdi + 32], r13
	mov [rdi + 40], r14
	mov [rdi + 48], r15
	mov [rdi + 56], rsi

	ret

	align 8
longjmp:
	mov rax, rsi
	mov rdx, 0

	mov rbx, [rdi + 0]
	mov rsp, [rdi + 8]
	mov rbp, [rdi + 16]
	mov r12, [rdi + 24]
	mov r13, [rdi + 32]
	mov r14, [rdi + 40]
	mov r15, [rdi + 48]

	jmp qword [rdi + 56]