#ifndef Window_hpp
#define Window_hpp

#include <Banana/Widget.hpp>

// TODO: Make 'width', 'height', 'x' and 'y' automatically update.

class Widget;

/**
\class Window
\brief Windows are the main element of the GUI, all widgets are drawn onto it.
*/

class Window {
	friend class Widget;
protected:
    int width;
    int height;
    int x;
    int y;
    int windowno;
    
    char originaltitle[32];
    char title[32];
	char showtitle[32];
    bool canClose;
    bool canMove;
    bool alive;
    bool displayed;
	enum MouseTypes cursorType;
	enum MouseTypes lastTimeCursorType;
    
    bool botherToUpdate;
    
    typedef struct children {
        void* data;
        children* next;
    } children;
    
    children* root;
    children* current;
    children* travel;
    
	typedef struct WidgetChildren
	{
		Widget* child;
		WidgetChildren* next;
	} WidgetChildren;

	WidgetChildren* widgetRoot;
	WidgetChildren* widgetCurrent;

    void updatewindow();
    
    void __create();
    void __delete();

public:
	Window (char* nTitle = "New Window", int width = 60, int height = 20, int x = -1, int y = -1);        ///< Sets up window parameters
    ~Window();
    
    virtual void setTitle(char* nTitle);
	virtual char* getTitle();
	virtual void setPosition(short int nX, short int nY);      ///< Incomplete - do not use
    
	virtual void movable(bool a = false);    ///< Incomplete - do not use
	virtual bool isMovable();                ///< Incomplete - do not use
    
	virtual void closable(bool a = false);   ///< Incomplete - do not use
	virtual bool isClosable();               ///< Incomplete - do not use
    
	virtual void display();        ///< Properly create the window
	virtual void kill();           ///< Delete the window
	void main();           ///< Deprecated - do not use
    
	virtual bool isAlive();        ///< Incomplete - do not use
    
	virtual void update();                 ///< Updates the window
    void _update(Event_t data);   ///< Internal use only - do not use

	virtual void drawPixel (uint16_t x, uint16_t y, uint32_t col);
	virtual void drawCharacter (char c, uint16_t x, uint16_t y, uint32_t bg = 0xFFFFFF, uint32_t fg = 0, int font = 0);

	virtual void addWidgetChild (Widget* child);

	virtual void setCursorType (enum MouseTypes type);
	virtual enum MouseTypes getCursorType ();

	virtual void write(char c, int x, int y, const char* font = "SYSTEM", int style = 0);            ///< Writes a character to the window
	virtual void write(const char* c, int x, int y, const char* font = "SYSTEM", int style = 0);     ///< Writes a string to the window

	virtual int getWidth ();
	virtual int getHeight ();
	virtual int getX ();
	virtual int getY ();

	virtual int getWindowNumber ();
};

void alertBox (char* title, char* text);

#endif /* Window_hpp */