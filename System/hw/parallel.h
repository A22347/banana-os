#pragma once

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include <devss/devices.h>

	extern uint16_t lptPortBases[4];

	void parallelWriteChar (uint8_t portNo, uint8_t pData);
	void parallelWrite (uint8_t portNo, void* data, uint32_t length);
	void parallelInit (struct Device* self);

	void openParallel(struct Device* self, int a, int b, void* c);
	void openParallelPort(struct Device* self, int a, int b, void* c);
	void writeParallelPort(struct Device* self, int a, int b, void* c);

#ifdef __cplusplus
}
#endif