#pragma once

void fatalFileSystemError ();
extern char systemFontBuiltin[];

#include <core/string.h>
#include <core/memory.h>

#define CHARS 255
#define FONTS 1

uint8_t reverse (uint8_t b);

extern int System;            //the system font
extern uint16_t Fonts[FONTS][CHARS][CELLH];
extern uint8_t FontWidths[FONTS][CHARS];

struct FONTDATA1 {
    char chr;
    uint8_t font[14];
    uint8_t size;
};

struct FONTDATA2 {
    char signature[16];
    struct FONTDATA1 FONTD[256];
};

extern struct FONTDATA2 FONT;

void loadbuiltinfonts ();