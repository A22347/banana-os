#pragma once

#include <core/kernel.h>
#include <gui/terminal.h>
#include <fs/vfs.h>

#define MAX_REGISTRY_ENTRIES_PER_HIVE 100

typedef struct RegEntry
{
	char key[126];
	char type;
	union x
	{
		char value[129];
		uint8_t uvalue[129];
	} x;
	

} __attribute__ ((packed)) RegEntry;

extern RegEntry systemRegistry[100];
extern RegEntry userRegistry[100];

void loadRegistry (char* registryName, bool userReg);
bool QueryRegistry (RegistryQuery* query, const char* keyname);
