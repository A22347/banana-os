#pragma once

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include <devss/devices.h>

	void openPCSpeaker (Device* self, int a, int b, void* c);
	void writePCSpeaker (Device* self, int a, int b, void* c);

	void startPCSpeaker (uint32_t hertz);
	void stopPCSpeaker ();

#ifdef __cplusplus
}
#endif