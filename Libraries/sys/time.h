#ifndef __SYS_TIME_H__
#define __SYS_TIME_H__

#ifdef __cplusplus
extern "C" {
#endif

#define	DST_NONE 0
#define	DST_USA	 1
#define	DST_AUST 2
#define	DST_WET	 3
#define	DST_MET	 4
#define	DST_EET	 5
#define	DST_CAN	 6
#define	ITIMER_REAL		0
#define	ITIMER_VIRTUAL	1
#define	ITIMER_PROF		2
#include <sys/select.h>

	int getitimer (int, struct itimerval *);
	int gettimeofday (struct timeval *, void *);
	int setitimer (int, const struct itimerval *, struct itimerval *);
	int utimes (const char *, const struct timeval[2]);

#ifdef __cplusplus
}
#endif

#endif