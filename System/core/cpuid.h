#ifdef __cplusplus
extern "C" {
#endif

#include <stddef.h>

	enum cpuid_requests
	{
		CPUID_GETVENDORSTRING,
		CPUID_GETFEATURES,
		CPUID_GETTLB,
		CPUID_GETSERIAL,
		CPUID_GETEXTENDED = 7,
		CPUID_INTELEXTENDED = 0x80000001,
		CPUID_INTELFEATURES,
		CPUID_INTELBRANDSTRING,
		CPUID_INTELBRANDSTRINGMORE,
		CPUID_INTELBRANDSTRINGEND,
	};

#define CPUID_RDRAND_COMPARE_ECX (1 << 30)	//EAX=CPUID_GETFEATURES
#define CPUID_TSC_COMPARE_EDX	 (1 <<  4)	//EAX=CPUID_GETFEATURES
#define CPUID_FLAG_MSR_EDX		 (1 <<  5)  //EAX=CPUID_GETFEATURES

#define CPUID_SMEP_COMPARE_EBX   (1 <<  7)	//EAX=CPUID_GETEXTENDED
#define CPUID_RDSEED_COMPARE_EBX (1 << 18)	//EAX=CPUID_GETEXTENDED
#define CPUID_UMIP_COMPARE_ECX   (1 <<  2)	//EAX=CPUID_GETEXTENDED

#define CPUID_NXBIT_COMPARE_EDX  (1 << 20)	//EAX=CPUID_INTELEXTENDED

	void cpuid (int code, size_t* a, size_t* b, size_t* c, size_t* d);

#ifdef __cplusplus
}
#endif