#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>

int chmod(const char* file, mode_t mode) {
    return -1;
}

int fchmod(int integer, mode_t mode) {
    return -1;
}

int fstat(int integer, struct stat* stats) {
	return SystemCall (SC_Fstat, integer, 0, stats);
}

int lstat(const char* file, struct stat* stats) {
	return stat (file, stats);
}

int mkdir(const char* file, mode_t mode) {
    return -1;
}

int mkfifo(const char* file, mode_t mode) {
    return -1;
}

int mknod(const char* file, mode_t mode, dev_t device) {
    return -1;
}

int stat(const char* file, struct stat* stats) {
	FILE* f = fopen (file, "r");
	int ret = SystemCall (SC_Fstat, &f, 0, stats);
	fclose (f);
	return ret;
}

mode_t umask(mode_t mode) {
    return -1;
}