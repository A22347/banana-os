//
//  Internals.hpp
//  GUI
//
//  Created by Alex Boxall on 3/6/19.
//  Copyright © 2019 Alex Boxall. All rights reserved.
//

#ifndef Internals_hpp
#define Internals_hpp

#include <stdbool.h>
#include <stdint.h>

void loadScreenData();
void flushScreen();

void putpixel (int x, int y, uint32_t col);
void drawRect (int x1, int y1, int x2, int y2, uint32_t col);
void drawLine (int x1, int y1, int x2, int y2, uint32_t col);
void drawCircle (int radius, int x, int y, uint32_t col);

extern int screenWidth;
extern int screenHeight;
extern int scanlineWidth;
extern uint32_t* screenBuffer;

#endif /* Internals_hpp */
