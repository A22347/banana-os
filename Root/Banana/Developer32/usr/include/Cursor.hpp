//
//  Cursor.hpp
//  Banana API (New)
//
//  Created by Alex Boxall on 19/10/18.
//  Copyright © 2018 Alex Boxall. All rights reserved.
//

#ifndef Cursor_hpp
#define Cursor_hpp

/*! \file Cursor.hpp
 \brief A header file defining the Cursor enum class.
 */

/*!
 \enum Cursor
 \brief A enum class containing the different cursor types.
 */

//SYNC WITH KERNEL
enum class Cursor {
    Normal,                 ///< Standard mouse pointer (pointing left)
    Waiting,                ///< Hourglass cursor
    Text,                   ///< Text editing cursor
    //and more
};

#endif /* Cursor_hpp */
