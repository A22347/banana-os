//
//  basic.c
//  Banana libc
//
//  Created by Alex Boxall on 16/7/18.
//  Copyright © 2018 Alex Boxall. All rights reserved.
//

#include "math.h"

const int math_errhandling = MATH_ERRNO | MATH_ERREXCEPT;

//this ain't going to work with floating point powers is it...
//try to comprehend: https://stackoverflow.com/questions/24566232/computing-fractional-exponents-in-c
//or this: http://www.cplusplus.com/forum/general/137467/
//and a little bit of this: https://stackoverflow.com/questions/2882706/how-can-i-write-a-power-function-myself
double pow(double x, double y) {
    double c = 1;
    for (int i = 0; i < y; i++)
        c *= x;
    return c;
}

//this ain't going to work with floating point powers is it...
//try to comprehend: https://stackoverflow.com/questions/24566232/computing-fractional-exponents-in-c
//or this: http://www.cplusplus.com/forum/general/137467/
//and a little bit of this: https://stackoverflow.com/questions/2882706/how-can-i-write-a-power-function-myself
float powf(float x, float y) {
    float c = 1;
    for (int i = 0; i < y; i++)
        c *= x;
    return c;
}

//this ain't going to work with floating point powers is it...
//try to comprehend: https://stackoverflow.com/questions/24566232/computing-fractional-exponents-in-c
//or this: http://www.cplusplus.com/forum/general/137467/
//and a little bit of this: https://stackoverflow.com/questions/2882706/how-can-i-write-a-power-function-myself
long double powl(long double x, long double y) {
    long double c = 1;
    for (int i = 0; i < y; i++)
        c *= x;
    return c;
}

double hypot(double x, double y) {
    return sqrt(x * x + y * y);
}

float hypotf(float x, float y) {
    return sqrtf(x * x + y * y);
}

long double hypotl(long double x, long double y) {
    return sqrtl(x * x + y * y);
}

//https://www.codeproject.com/Articles/69941/Best-Square-Root-Method-Algorithm-Function-Precisi
float sqrtf(float x) {
    if (x < 0) {
        return NAN;
    }
    float i = 0;
    float x1 = 0, x2 = 0;
    while ((i * i) <= x) {
        i += 0.1f;
    }
    x1 = i;
    for (int j = 0; j < 15; ++j) {
        x2 = x;
        x2 /= x1;
        x2 += x1;
        x2 /= 2;
        x1 = x2;
    }
    return x2;
}

double sqrt(double x) {
    if (x < 0) {
        return NAN;
    }
    double i = 0;
    double x1 = 0, x2 = 0;
    while ((i * i) <= x) {
        i += 0.1;
    }
    x1 = i;
    for (int j = 0; j < 20; ++j) {      //20 iterations = better result (as double can store more precise numbers)
        x2 = x;
        x2 /= x1;
        x2 += x1;
        x2 /= 2;
        x1 = x2;
    }
    return x2;
}

long double sqrtl(long double x) {
    if (x < 0) {
        return NAN;
    }
    long double i = 0;
    long double x1 = 0, x2 = 0;
    while ((i * i) <= x) {
        i += 0.1;
    }
    x1 = i;
    for (int j = 0; j < 30; ++j) {     //30 iters. = better result (as long double can store more precise numbers)
        x2 = x;
        x2 /= x1;
        x2 += x1;
        x2 /= 2;
        x1 = x2;
    }
    return x2;
}
