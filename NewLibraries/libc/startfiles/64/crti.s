.section .init
.global _init
_init:
push %rbp
	movq %rsp, %rbp
   /* gcc will nicely put the contents of crtbegin.o .init section here. */

.section .fini
.global _fini
_fini:
push %rbp
	movq %rsp, %rbp
