This contains locale files that match the 'C' locales.
What will happen is the libc will create locales using the data that is here.
It is modelled after the Windows 'Customize Format' option, not the actual C
locale spec, but they are very similar and libc can work out where to put things.

The default locale is selected using the registry.

This is the 'locale' file format:

it takes up 176 bytes (I think...)

struct locale {
	//DATE FORMAT
	//d    = day (no leading zero)
	//dd   = day (leading zero)
	//M    = month (no leading zero)
	//MM   = month (leading zero)
	//yy   = year
	//yyyy = year
	//dddd = long day (e.g. Sunday)
	//ddd  = short day (e.g. Sun)
	//MMMM = long month (e.g. September)
	//MMM  = short month (e.g. Sept.)
	//tt   = AM / PM
	//H    = 24 hour
	//h    = 12 hour
	//mm   = minute (leading zero)
	//m    = minute (no leading zero)
	//ss   = second (leading zero)
	//s    = second (no leading zero)
	//
	//ANY OTHER CHARACTERS JUST APPEAR NORMALLY 
	//(e.g. spaces, dots, forward slashes, colon, quote mark, etc.)

	char shortDateFormat[32];
	char longDateFormat[32];
	char shortTimeFormat[32];
	char longTimeFormat[32];

	char amSymbol[8];
	char pmSymbol[8];

	char decimalSymbol[4];			//'.' for Australia
	char decimalGroupingSymbol[4];		//',' for Australia
	char negativeSign[4];			//'-' for Australia
	char listSeparator[4];			//',' for Australia
	//least significant bit goes first
	uint8_t numeralType : 5;		//different number characters
	uint8_t negativeFormat : 3;		//(1.1), -1.1, - 1.1, 1.1-, 1.1 -		(these use the decimal and negative sign listed above)
	
	uint8_t displayLeadingZeros : 1;	//0.7, .7
	uint8_t metric : 1;	
	uint8_t digitsAfterDecimal : 4;		//defaults to '2' for Australia
	uint8_t digitGrouping : 2;		//0 = none (123456789)
						//1 = every 3 (123,456,789)			(these use the grouping symbol listed above)
						//2 = every 2 except first (12,34,56,789)	(these use the grouping symbol listed above)
						//3 = first 3 (123456,789)			(these use the grouping symbol listed above)

	char currencySymbol[4];			//'$'
	char currencyDecimalSymbol[4];		//'.'
	char currencyDigitGroupingSymbol[4];	//','
	uint8_t currencyDigitsAfterDecimal : 4;	
	uint8_t currencyDigitGrouping : 2;	//see above
	uint8_t : 2;
	uint8_t currencyPositiveFormat : 2;	//$1.1, 1.1$, $ 1.1, 1.1 $			(these use the decimal and currency sign listed above)
	uint8_t currencyNegativeFormat : 4;							(these use the decimal, currency and negative sign listed above)
	uint8_t : 2;

	//currency negative formats
	//($1.1), -$1.1, $-1.1, $1.1-, (1.1$), -1.1$, 1.1-$, 1.1$-, -1.1 $,
	//-$ 1.1, 1.1 $-, $ 1.1-, $ -1.1, 1.1- $, ($ 1.1), (1.1 $)
}