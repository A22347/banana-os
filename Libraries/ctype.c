#include <ctype.h>

int c ()
{
	asm volatile ("nop; nop; int $7; int $7; int $7; nop;");
}

int isalnum(int c) {
    return isalpha(c) || isdigit(c);
}

int isalpha(int c) {
    return islower(c) || isupper(c);
}

int iscntrl(int c) {
    return (c < 32) || (c == 127);
}

int isdigit(int c) {
    return (c > 47) && (c < 58);
}

int isgraph(int c) {
    return isalnum(c) || ispunct(c);
}

int islower(int c) {
    return (c >= 'a') && (c <= 'z');
}

int isprint(int c) {
    return isgraph(c) || isspace(c);
}

int ispunct(int c) {
    return !isalnum(c) && !iscntrl(c) && !isspace(c);
}

int isspace(int c) {
    switch (c) {
        case ' ':
        case '\n':
        case '\t':
        case '\r':
        case '\f':
        case '\v':
            return true;
        default:
            return false;
    }
}

int isupper(int c) {
    return (c >= 'A') && (c <= 'Z');
}

int isxdigit(int c) {
    if (isdigit(c)) {
        return true;
    }
    switch (c) {
        case 'A':
        case 'B':
        case 'C':
        case 'D':
        case 'E':
        case 'F':
        case 'a':
        case 'b':
        case 'c':
        case 'd':
        case 'e':
        case 'f':
            return true;
        default:
            return false;
    }   
}

int tolower (int c)
{
	if (c >= 65 && c <= 90) {
		return c + 32;
	} else {
		return c;
	}
}

int toupper (int c)
{
	if (c >= 97 && c <= 122) {
		return c - 32;
	} else {
		return c;
	}
}

int toascii (int c)
{
	return c & 0x7F;
}