
#ifndef FUNCTIONS_HPP_INCLUDED
#define FUNCTIONS_HPP_INCLUDED

#include <Banana/Core.hpp>
#include <syscall.h>

extern "C++" {
	enum LogPriority
	{
		LogInformation,
		LogApplicationFault,
		LogSystem,
		LogDebug,
		LogPanic
	};

	namespace System
	{
		void Log (char* string, LogPriority priority = LogInformation);
		void Panic (char* string = "MANUAL_PANIC");
		void Shutdown ();
		void Reboot ();
		void Copy (char* copy);
		char* Paste ();
		char* Username ();
		char* GetRunningProcess ();
		int GetRunningProcessID ();

		int GetNumberOfProcesses ();
		int GetNumberOfWindows ();

		char* GetProcessFilenameFromPID (int pid);
		char* GetWindowNameFromID (int id);
	};

}

#endif
