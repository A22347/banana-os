#include <hal/floating.h>
#include <debug/log.h>
#include <core/panic.h>

#include <hw/sse.h>
#include <hw/avx.h>
#include <hw/x87.h>

/*
This is the order we will use:
AVX-512		(requires init'ing SSE)
AVX			(requires init'ing SSE)
SSE4		(requires init'ing SSE, reads/writes should call sseRead/sseWrite, only the init is different)
SSE3		(requires init'ing SSE, reads/writes should call sseRead/sseWrite, only the init is different)
SSE2		(requires init'ing SSE, reads/writes should call sseRead/sseWrite, only the init is different)
SSE
MMX
FPU
(ERROR!)
*/

typedef enum FPUTypes
{
	FPUType_Error,
	FPUType_x87,
	FPUType_MMX,
	FPUType_SSE,
	FPUType_AVX,
	FPUType_AVX512

} FPUTypes;

FPUTypes fpuType = FPUType_Error;

void floatingInit (void)
{
	if (avxAvaliable ()) {
		/*logk ("avx chosen!\n");

		fpuType = FPUType_AVX;
		avxOpen ();*/

		logk ("avx avaliable, but we're going for SSE (because our AVX driver crashes)\n");
		fpuType = FPUType_SSE;
		sseOpen ();

	} else if (sseAvaliable ()) {
		logk ("sse chosen!\n");

		fpuType = FPUType_SSE;
		sseOpen ();

	} else if (x87Avaliable ()) {
		logk ("x87 chosen!\n");

		fpuType = FPUType_x87;
		x87Open ();

	} else {
		panicWithMessage ("NO_COMPATIBLE_FPU");
	}
}

void floatingLoad (void* location)
{
	switch (fpuType) {
	case FPUType_AVX:
		avxWrite (location);
		break;
	case FPUType_SSE:
		sseWrite (location);
		break;
	case FPUType_x87:
		x87Write (location);
		break;
	default:
		panicWithMessage ("FPU_ERROR");
		break;
	}
}

void floatingStore (void* location)
{
	//doesn't work on x86 for some reason (faulty asm driver?)
	//but it does on x86_64

	switch (fpuType) {
	case FPUType_AVX:
		avxRead (location);
		break;
	case FPUType_SSE:
		sseRead (location);
		break;
	case FPUType_x87:
		x87Write (location);
		break;
	default:
		panicWithMessage ("FPU_ERROR");
		break;
	}
}