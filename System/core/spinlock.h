#pragma once

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include <stdbool.h>

	typedef struct spinlock_t
	{
		volatile uint32_t locked;
		uint32_t timer;
		uint32_t timeoutValue;
		bool oldInt;

	} spinlock_t;

	void initSpinlock (spinlock_t volatile* lock);
	void lockSpinlock (spinlock_t volatile* lock);
	void unlockSpinlock (spinlock_t volatile* lock);
	bool trySpinlock (spinlock_t volatile* lock);

	typedef struct tasklock_t
	{
		volatile uint32_t locked;
		uint32_t tasknosave;

	} tasklock_t;

	void initTasklock (tasklock_t volatile* lock);
	void lockTasklock (tasklock_t volatile* lock);
	void unlockTasklock (tasklock_t volatile* lock);

#ifdef __cplusplus
}
#endif