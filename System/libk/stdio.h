//
//  stdio.h
//  Banana libc
//
//  Created by Alex Boxall on 11/7/18.
//  Copyright © 2018 Alex Boxall. All rights reserved.
//

#ifndef stdio_h
#define stdio_h

#if defined(__cplusplus)
#define restrict			//fixes some 'fun' errors
#define _Bool bool
extern "C" {
#endif

#undef NULL
#define NULL 0
    
#define _IOFBF 0
#define _IOLBF 1
#define _IONBF 2
    
#define BUFSIZ 65536
#ifndef _libk
#define EOF -1
#endif
#define FOPEN_MAX 10
    
#define FILENAME_MAX 255
#define L_tmpnam 255
    
#define SEEK_CUR 0
#define SEEK_END 1
#define SEEK_SET 2
    
//max number of tmpfiles allowed, each will be 5 chars long
//and in range A-Z + 0-9
#define TMP_MAX (36 * 36 * 36 * 36 * 36)
    
#define __i_FILE_READ           1
#define __i_FILE_WRITE          2
#define __i_FILE_APPEND         4
#define __i_FILE_BINARY         8
#define __i_FILE_NO_OVERWRITE   16
#define __i_FILE_UPDATE         32      //read/write
    
#define __i_LASTOPERATION_WRITE 0
#define __i_LASTOPERATION_READ  1
#define __i_LASTOPERATION_NULL  2

#include <stddef.h>     //size_t
#include <stdarg.h>
   
	typedef unsigned long long fpos_t;

    typedef struct FILE {
        unsigned long long objectID;
        unsigned char bufferMode;       //see _IOFBF, _IOLBF, IONBF
        unsigned char wideOriented;     //0=byte, 1=wide, 2=none yet
        _Bool textStream;
        unsigned int locked;
        unsigned char* buffer;
        char* filename;
		size_t bufferPtr;
        _Bool openAndValid;
        unsigned char filemode;
        unsigned char lastOperation;     //0=write, 1=read, 2=nothing has happened yet
        _Bool errorIndicator;
        size_t bufferSizeForThisFile;
        _Bool eof;
        _Bool seekable;                  //0=virtual stream (thus not seekable), such as stdout/stdin/stderr
                                         //1=file stream (and thus seekable)
        
    } FILE;
    
#ifndef _libk
    extern FILE* stderr;
    extern FILE* stdin;
    extern FILE* stdout;
    
    int remove(const char *filename);
    int rename(const char *old, const char *new_);
    
    int __i_internalFileWrite (FILE* fil, size_t bytes, unsigned char* data);
    int __i_internalFileRead (FILE* fil, size_t bytes, unsigned char* data);
    int __i_internalFileBufferFlushWrite(FILE* fil);
    
    FILE *fopen(const char * restrict filename, const char * restrict mode);
    int fclose(FILE *stream);
    int fflush(FILE *stream);
    FILE *freopen(const char * restrict filename, const char * restrict mode, \
                                  FILE * restrict stream);
    void setbuf(FILE * restrict stream, char * restrict buf);
    int setvbuf(FILE * restrict stream, char * restrict buf, int mode, size_t size);
    int ferror(FILE *stream);
    int feof(FILE *stream);
    void perror(const char *s);
    
    size_t fwrite(const void * restrict ptr, size_t size, size_t nmemb, FILE * restrict stream);
    size_t fread (void * restrict ptr, size_t size, size_t nmemb, FILE * restrict stream);
    
    int fputc(int c, FILE *stream);
    int fputs(const char * restrict s, FILE * restrict stream);
    int putc(int c, FILE *stream);
    int putchar(int c);
    int puts(const char *s);
    
    void clearerr(FILE *stream);
    
    char *tmpnam(char *s);
    FILE *tmpfile(void);
	
	int fgetpos(FILE * restrict stream, fpos_t * restrict pos);
    int fseek(FILE *stream, long int offset, int whence);
    int fsetpos(FILE *stream, const fpos_t *pos);
    long int ftell(FILE *stream);
    void rewind(FILE *stream);
        
    int fgetc(FILE *stream);
    int getc(FILE *stream);
    char *fgets(char * restrict s, int n, FILE * restrict stream);
    int getchar(void);
    
    __attribute__ ((format (printf, 1, 2))) int printf(const char * restrict format, ...);
    int vprintf(const char * restrict format, va_list arg);
    int vfprintf(FILE * restrict stream, const char * restrict format, va_list arg);
    __attribute__ ((format (printf, 2, 3))) \
    int fprintf(FILE * restrict stream, const char * restrict format, ...);
   
	__attribute__ ((format (printf, 2, 3))) int asprintf (char** ret, const char * restrict format, ...);
	int vasprintf (char** ret, const char * restrict format, va_list arg);


#endif
   
    int vsnprintf(char * restrict s, size_t n, const char * restrict format, va_list list) ;
    int vsprintf(char * restrict s, const char * restrict format, va_list arg);
    __attribute__ ((format (printf, 3, 4))) \
    int snprintf(char * restrict s, size_t n, const char * restrict format, ...);
    __attribute__ ((format (printf, 2, 3))) int sprintf(char * restrict s, const char * restrict format, ...);
    

    
#if defined(__cplusplus)
}
#endif

#endif /* stdio_h */
