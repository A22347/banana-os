//
//  RequiredImporting.hpp
//  Banana API (New)
//
//  Created by Alex Boxall on 22/5/18.
//  Copyright © 2018 Alex Boxall. All rights reserved.
//

#ifndef RequiredImporting_h
#define RequiredImporting_h

//fixes some "fun" C-compatability errors
#define _Bool bool
#define restrict
#define _Noreturn

#endif /* RequiredImporting_h */
