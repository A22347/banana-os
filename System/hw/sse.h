#pragma once

#ifdef __cplusplus
extern "C" {
#endif

	int sseAvaliable ();
	void sseOpen ();
	void sseRead (void* ptr);
	void sseWrite (void* ptr);

#ifdef __cplusplus
}
#endif