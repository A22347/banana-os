#include <Banana/Button.hpp>
#include <Banana/Core.hpp>
#define CHECK_BIT(var,pos) ((var) & (1<<(pos)))

Button::Button (Window& parent, char* _text, int _x, int _y, int _width, char* _font) : Widget (&parent)
{
	masterwindow = &parent;
	masterwindow->addWidgetChild (this);

	if (_width < strlen (_text) + 3) {
		_width = strlen (_text) + 3;
	}

	buttonfont = (char*) malloc (strlen (_font) + 6);
	memcpy (buttonfont, _font, strlen (_font) + 6);

	text = (char*) malloc (strlen (_text) + 6);
	memcpy (text, _text, strlen (_text) + 6);

	currentmode = 999;
	buttonwidth = _width;
	x = _x;
	y = _y;
}

Button::~Button ()
{
	free ((void*) text);
	free ((void*) buttonfont);
}

void Button::_write (int mode)
{
	if (currentmode == mode) {
		return;
	}
	currentmode = mode;

	uint16_t pixelPointer = x;

	uint32_t outlinecolour = 0xFFFFFF;
	if (mode == 1) {
		outlinecolour = 0;
	}

	for (int i = 0; i < buttonwidth; ++i) {
		masterwindow->drawCharacter (' ', pixelPointer, y, 0xC0C0C0, 0xC0C0C0, 0);
		masterwindow->drawCharacter (' ', pixelPointer, y + 8, 0xC0C0C0, 0xC0C0C0, 0);
		pixelPointer += 4;
	}

	pixelPointer = x;
	for (int i = 0; i < buttonwidth * 4; ++i) {
		masterwindow->drawPixel (pixelPointer, y, outlinecolour);
		masterwindow->drawPixel (pixelPointer, y + 1, 0xE0E0E0);

		masterwindow->drawPixel (pixelPointer, y + 20, 0x808080);
		masterwindow->drawPixel (pixelPointer, y + 21, 0);
		++pixelPointer;
	}

	for (int i = 0; i < 22; ++i) {
		masterwindow->drawPixel (x + buttonwidth * 4 - 1, y + i, 0);
		if (i < 21) {
			masterwindow->drawPixel (x, y + i, outlinecolour);
		}
		if (i && i < 20) {
			masterwindow->drawPixel (x + 1, y + i, 0xE0E0E0);
		}
		if (i && i != 21) {
			masterwindow->drawPixel (x + buttonwidth * 4 - 2, y + i, 0x808080);
		}
	}

	uint16_t totalWidth = 0;
	uint8_t fontno = SystemCall (SC_GetFontNumberFromName, 0, 0, (void*) buttonfont);

	for (int i = 0; text[i]; ++i) {
		uint8_t width77 = APISystemCall (ASCGetCharacterWidth, text[i], fontno, 0);
		totalWidth += width77;
	}

	pixelPointer = x + (buttonwidth * 2 - totalWidth / 2);

	for (int i = 0; text[i]; ++i) {
		uint8_t width77 = APISystemCall (ASCGetCharacterWidth, text[i], fontno, 0);
		masterwindow->drawCharacter (text[i], pixelPointer, y + 4, 0xC0C0C0, 0, fontno);
		pixelPointer += width77;
	}
	forceWindowToUpdate ();
}

void Button::_clear ()
{
	uint16_t pixelPointer = x;

	for (int i = 0; i < buttonwidth * 2; ++i) {
		masterwindow->drawCharacter (' ', pixelPointer, y, 0xC0C0C0, 0xC0C0C0, 0);
		masterwindow->drawCharacter (' ', pixelPointer, y + 8, 0xC0C0C0, 0xC0C0C0, 0);
		pixelPointer += 4;
	}
	forceWindowToUpdate ();
}

void Button::command (void (*n)())
{
	buttoncommand = n;
}

void Button::selfupdate (Event_t event)
{
	int cx = SystemCall (SC_WindowPositionX, getWindowNumber(), 0, 0);
	int cy = SystemCall (SC_WindowPositionY, getWindowNumber(), 0, 0);

	if (event.type == EventMouseClick) {
		int xx = event.mouseXPosition - cx;
		int yy = event.mouseYPosition - cy;
		if (yy >= y && yy <= y + 18 && xx >= x && xx <= x + buttonwidth * 4) {
			_write (1);
		} else {
			_write ();
		}
	} else if (event.type == EventMouseUp) {
		int xx = event.mouseXPosition - cx;
		int yy = event.mouseYPosition - cy;
		_write ();
		if (yy >= y && yy <= y + 18 && xx >= x && xx <= x + buttonwidth * 4) {
			if (buttoncommand) {
				buttoncommand();
			}
		}
	} else if (event.type == EventNull) {
		_write ();
	}
}
