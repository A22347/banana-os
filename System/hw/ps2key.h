#pragma once

//all variables in the matching .c should be ISOLATED from the outside world...
//(after all, we should be able to move it to a driver if we wanted to, without anyone caring)

#include <hal/keybrd.h>
#include <devss/devices.h>

void updateKeyboardLEDs ();
void updateRepeatRates ();

void openPS2Keyboard(struct Device* self, int, int, void*);
void detectPS2(struct Device* self, int a, int b, void* c);

#define PS2_SCANCODE_backspace 0x66
#define PS2_SCANCODE_tab 0x0D
#define PS2_SCANCODE_capslock 0x58
#define PS2_SCANCODE_numberlock 0x77
#define PS2_SCANCODE_scrolllock 0x7E
#define PS2_SCANCODE_newline 0x5A
#define PS2_SCANCODE_f1 0x05
#define PS2_SCANCODE_f2 0x06
#define PS2_SCANCODE_f3 0x04
#define PS2_SCANCODE_f4 0x0C
#define PS2_SCANCODE_f5 0x03
#define PS2_SCANCODE_f6 0x0B
#define PS2_SCANCODE_f7 0x83
#define PS2_SCANCODE_f8 0x0A
#define PS2_SCANCODE_f9 0x01
#define PS2_SCANCODE_f10 0x09
#define PS2_SCANCODE_f11 0x78
#define PS2_SCANCODE_f12 0x07
#define PS2_SCANCODE_extension 0xE0
#define PS2_SCANCODE_breakcode 0xF0

extern bool caps;
extern bool ctrl;
extern bool capslk;
extern bool alt;
extern bool altR;
