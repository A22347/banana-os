#include <hw/apic.h>
#include <hal/interrupts.h>
#include <stdint.h>

/*
THE ORIGNAL CODE CAME FROM http://ethv.net/workshops/osdev/notes/notes-3.html
AND IS RELEASED UNDER THE FOLLOWING 3-CLAUSE BSD LICENSE.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

uint32_t ioapicRead (void *ioapicaddr, uint32_t reg)
{
	uint32_t volatile *ioapic = (uint32_t volatile *) ioapicaddr;
	ioapic[0] = (reg & 0xff);
	return ioapic[4];
}

void ioapicWrite (void *ioapicaddr, uint32_t reg, uint32_t value)
{
	uint32_t volatile *ioapic = (uint32_t volatile *) ioapicaddr;
	ioapic[0] = (reg & 0xff);
	ioapic[4] = value;
}

void ioapicSetIRQ (uint8_t irq, uint64_t apic_id, uint8_t vector, uint32_t base)
{
	logk("ioapicSetIRQ 0x%X\n", irq);

	const uint32_t low_index = 0x10 + irq * 2;
	const uint32_t high_index = 0x10 + irq * 2 + 1;

	uint32_t high = ioapicRead ((void*) (size_t) base, high_index);			//0xFEC00000 is a hardcoded Bochs/qemu address
																				// set APIC ID
	high &= ~0xff000000;
	high |= apic_id << 24;
	ioapicWrite ((void*) (size_t) base, high_index, high);					//0xFEC00000 is a hardcoded Bochs/qemu address

	uint32_t low = ioapicRead ((void*) (size_t) base, low_index);				//0xFEC00000 is a hardcoded Bochs/qemu address
																				// unmask the IRQ
	low &= ~(1 << 16);

	// set to physical delivery mode
	low &= ~(1 << 11);

	// set to fixed delivery mode
	low &= ~0x700;

	// set delivery vector
	low &= ~0xff;
	low |= vector;

	ioapicWrite ((void*) (size_t) base, low_index, low);				//0xFEC00000 is a hardcoded Bochs/qemu address
}

/*

END OF BSD LICENSED CODE FROM  http://ethv.net/workshops/osdev/notes/notes-3.html

*/

void enableAPIC ()
{
	logk("ENABLING APIC!\n");
	uint32_t* ptr = (uint32_t*) (size_t) (cpuGetAPICBase () + 0xf0);
	uint32_t val = *ptr;
	val |= (1 << 8);
	*ptr = val;
}

void sendEOIForAPIC ()
{
	uint32_t* ptr = (uint32_t*) (size_t) (cpuGetAPICBase () + 0xb0);
	*ptr = 1;
}

uint32_t cpuGetAPICBase ()
{
	uint64_t ret = rdmsr (IA32_APIC_BASE_MSR);
	return (ret & 0xfffff000);
}
