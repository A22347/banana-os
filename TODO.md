TODO:

- **On task switches, store all of the segment registers (CS, DS, ES, FS, GS, SS)**

- **Write support for the filesystem**

- Add buttons to the API

- Add scrollable text widgets to the API

- Write the *user.exe* application (the shell)

- Make it so the background can be an image, not a colour

- A system log format and reader				**WE WILL DO THIS SOMETIME LATER**

- **Add long filenames (make a @DIRINFO.SYS file in each folder)**

- **Add the Local APIC timer**

- Add a system call to let a program have 120ms at a time, at minimum once every 5 seconds.

- Use a different executable format

- Add hard drive booting						**WE WILL DO THIS SOMETIME LATER**

- Add right click menus (in *user.exe*)

- Get the SSE extensions working (try using *CPUID* to check if avaliable, maybe put in an API function *isSSESupported*?)				**WE WILL DO THIS SOMETIME LATER**

