#ifndef Widget_hpp
#define Widget_hpp

#include <Banana/Functions.hpp>
#include <Banana/Core.hpp>
#include <Banana/Window.hpp>

class Window;

/**
\class Widget
\brief A superclass for all user interface elements (widgets).
*/

class Widget
{
protected:
	Window * masterwindow;
	friend class Window;
	bool focus;						//not used by all widgets, but used by the system
	virtual bool autoLooseFocusOnMouseClick ();			//used for textboxes which loose focus when the mouse is clicked
														//but regain it in their handler if needed

	void forceWindowToUpdate ();
	int32_t getWindowNumber ();

public:
	EventHandler EventMouseClickHandler;
	EventHandler EventMouseDownHandler;
	EventHandler EventMouseUpHandler;
	EventHandler EventMouseMoveHandler;
	EventHandler EventMouseDragHandler;
	EventHandler EventMouseEnterHandler;
	EventHandler EventMouseLeaveHandler;
	EventHandler EventMouseHoverHandler;

	EventHandler EventMoveHandler;
	EventHandler EventResizeHandler;
	EventHandler EventWindowResizeHandler;

	EventHandler EventKeyPressHandler;
	EventHandler EventKeyDownHandler;
	EventHandler EventKeyUpHandler;
	EventHandler EventKeyPressEarlyHandler;

	EventHandler EventEnterHandler;
	EventHandler EventLeaveHandler;

	EventHandler EventHelpRequestedHandler;

	EventHandler EventDragDropHandler;
	EventHandler EventDragEnterHandler;
	EventHandler EventDragLeaveHandler;

	EventHandler EventScrollWheelHandler;

	virtual void selfupdate (Event_t event) = 0;
	virtual ~Widget ();

	Widget (Window* master);
};

#endif /* Widget_hpp */
