#include <hw/parallel.h>
#include <hw/ports.h>
#include <debug/log.h>
#include <core/memory.h>

//this file basically copies how serial.c did it

//this sort of stuff will go into a devss class eventually
uint16_t lptPortBases[4] = { 0x378, 0x278, 0x3BC, 0x000 };
//lptPortBases[0] may be 0x3BC on the occasional machine
//lptPortBases[3] is vendor specific and will require a driver per chipset,
//so we will not support LPT4 (no-one really needs 4 of them anyway)

// Sends a byte to the printer
void parallelWriteChar (uint8_t portNo, uint8_t pData)
{
	if (lptPortBases[portNo] == 0) return;

	// Wait for the printer to be receptive
	while (!(inb (lptPortBases[portNo] + 1) & 0x80)) {
		//Timer_Delay (10);
	}

	// Now put the data onto the data lines
	outb (lptPortBases[portNo] + 0, pData);

	// Now pulse the strobe line to tell the printer to read the data
	uint8_t control = inb (lptPortBases[portNo] + 2);
	outb (lptPortBases[portNo] + 2, control | 1);
	//Timer_Delay (10);
	outb (lptPortBases[portNo] + 2, control);

	// Now wait for the printer to finish processing
	while (!(inb (lptPortBases[portNo] + 1) & 0x80)) {
		//Timer_Delay (10);
	}
}

void parallelWrite (uint8_t portNo, void* data, uint32_t length)
{
	for (uint32_t i = 0; i < length; ++i) {
		parallelWriteChar (portNo, ((uint8_t*) data)[i]);
	}
}

void parallelInit (struct Device* self)
{
	//detection only works on legacy BIOS systems
	uint16_t* biosDataArea = (uint16_t*) (size_t) 0x408;

	//no LPT4 in the BIOS, as LPT4 is at a vendor specific location
	for (int i = 0; i < 3; ++i) {
		//no scratch register, so check the BIOS's port address
		lptPortBases[i] = *(biosDataArea + i);

		logk ("parallel port base is at 0x%X.\n", lptPortBases[i]);
	}

	//initialise all working ports
	for (int i = 0; i < 4; ++i) {
		if (lptPortBases[i] == 0) continue;

		Device* newdev = malloc(sizeof(Device));
		initDeviceObject(newdev);
		newdev->onDiskDriver = false;
		newdev->driverLocation = 0;
		newdev->internalDataSize = i;	//port number
		newdev->openPointer = openParallelPort;
		newdev->writePointer = writeParallelPort;
		strcpy(newdev->humanName, "PARALLEL PORT");
		addChild(self, newdev);
		openDevice(newdev, 0, 0, 0);
	}
}

void openParallel(struct Device* self, int a, int b, void* c)
{
	parallelInit(self);
}

void openParallelPort(struct Device* self, int a, int b, void* c)
{
	//We need to reset the Control Port writing a 0
	//to configure their options, controlled for each of
	//their bits:
	outl(lptPortBases[self->internalDataSize] + 2, 0);

	//Now send any value between 0 and 255 to the data port:
	outl(lptPortBases[self->internalDataSize], 0x00);
}

void writeParallelPort(struct Device* self, int a, int b, void* c)
{
	parallelWriteChar(self->internalDataSize, a);
}