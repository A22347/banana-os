#pragma once

#include <core/memory.h>
#include <fs/vfs.h>
#include <core/panic.h>

//IN: PHYSICAL PAGE FORM
extern bool flushpages;

extern void pagingUpdate ();

void remapPage (uint64_t physical, uint64_t virtualm);
void remapPages (uint64_t physical, uint64_t virtualm, uint64_t size);

void giveKernelOnlyAccessTo (uint64_t spot);
void giveKernelOnlyAccessToPages (uint64_t spot, uint64_t size);

void giveEveryoneAccessTo (uint64_t spot);
void giveEveryoneAccessToPages (uint64_t spot, uint64_t size);

void setNXBit (uint64_t spot, bool noExecute);
void setNXBitToPages (uint64_t spot, bool noExecute, uint64_t size);

void giveNooneAccessTo (uint64_t spot);
void giveNooneAccessToPages (uint64_t spot, uint64_t size);

void markAsReadOnlyByProgram (uint64_t spot);
void markAsReadOnlyByProgramToPages (uint64_t spot, uint64_t size);

uint64_t allocatePhysicalPage ();					//returns physical address of a new page
void freePhysicalPage (uint64_t physicalPage);		//frees a physical page

uint64_t virtualToPhysical (uint64_t virtualPage);

uint32_t findBestPageToSwapOut ();
void recordCurrentlyExecutingPage (uint64_t finalEIP, int processID, int threadNumber);

void memoryManagerInit ();
void setupPagingForCPU (uint32_t cpuno);

void markPageAs (bool isFree);
bool isPageFree ();

void copyAddressSpace(size_t* dest, size_t* src);

uint64_t allocatePhysicalPagesTogetherNoSwaps(int numberOfPages);

//dont use the first one, use the second one!
bool allocateMemoryForTask (int processID, int threadID, vfs_file_t* file, size_t size, size_t virtualAddr, size_t additionalNullBytes);


//IN: address form
void virtualAddressSpaceMapPage (size_t* addrSpace, uint64_t physicalPage, size_t virtualPage);

void switchToVirtualAddressSpace (size_t* addrSpace);
void switchToKernelTableIfNot ();

size_t* createVirtualAddressSpace ();

void allocateSharedMemory(int pid1, int pid2, int pages, uint64_t* outAddressForPID1, uint64_t* outAddressForPID2);