//
//  syscall.c
//  Banana libc
//
//  Created by Alex Boxall on 12/7/18.
//  Copyright © 2018 Alex Boxall. All rights reserved.
//

#include "syscall.h"
#include <stdint.h>
#include <stddef.h>
#include <stdbool.h>
#include "../../System/core/syscalldef.h"

int SystemCall (uint16_t callno, uint32_t ebx, uint32_t ecx, void* edx)
{
	int res;

	asm volatile (
		"int $96"  :				//assembly
		"=a" (res) :				//output
		"a" (callno), 				//input
		"b" (ebx), 					//input
		"c" (ecx), 					//input
		"d" (edx) :					//input
		"memory", "cc");			//clobbers	

	return res;
}

void __i_system_call_abort() {
	SystemCall (SC_ExitTerminalSession, 255, 0, 0);
    SystemCall(SC_Abort, 255, 0, 0);
}

void __i_system_call_exit(int code) {
	SystemCall (SC_ExitTerminalSession, code, 0, 0);
    SystemCall(SC_Abort, code, 0, 0);
}

int __i_system_call_system(const char* string) {
    return SystemCall(SC_System, 0, 0, (void*) string);
}

char* __i_system_call_getenv(const char* name) {
    return (char*) (size_t) SystemCall(SC_GetEnv, 0, 0, (void*) name);
}

int __i_system_call_unlink(const char* filename) {
    return SystemCall(SC_NewFileDelete, 0, 0, (void*) filename);
}

int __i_system_call_rename(const char* old, const char* new_) {
    return SystemCall(SC_Rename, 0, (size_t) old, (void*) new_);
}

int __i_system_call_openfile(unsigned long long* fdout, const char* filename, unsigned char mode) {
    //return SystemCall(SC_CreateFileObjectFromFilename, fdout, mode, filename);
    return 0;
}

int __i_system_call_readfileorpipe(unsigned long long fd, unsigned char* bf, unsigned long long len) {
    return SystemCall (SC_ReadFileOrPipeObjectFromFilename, len, (size_t) &fd, (void*) bf);
}

int __i_system_call_writefileorpipe(unsigned long long fd, const unsigned char* bf, unsigned long long len) {
    return SystemCall(SC_WriteFileOrPipeObjectFromFilename, len, (size_t) &fd, (void*) bf);
}

int __i_system_call_pipe(unsigned long long* fdRead, unsigned long long* fdWrite) {
    //return SystemCall(SC_CreatePipeObject, 0, fdRead, fdWrite);
    return 0;
}

int __i_system_call_getstream (unsigned long long* output, int type) {
    return SystemCall(SC_GetStdstreamObjectForProcess, type, 0, output);
}

int __i_system_call_malloc_location (size_t* base, size_t* size) {
    return SystemCall(SC_Malloc, 0, (size_t) base, (void*) size);
}
