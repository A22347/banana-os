productKeyString times 20 db 177
db 0
productKeyStringPtr db 0
productKeyPrintingChar db 'A', 0
productKeySep db " - ", 0

RenderKey:
	mov cx, 0
.start:
	mov bx, cx
	mov al, [productKeyString - D_OFFSET + bx]
	mov [productKeyPrintingChar - D_OFFSET], byte al
	mov si, productKeyPrintingChar - D_OFFSET
	push cx
	call puts
	pop cx

	inc cx

	cmp cx, 20
	je .end
	;repeated checks is just easier than modulo 5 then cmp ax, 0; jne .divis; 
	mov ax, cx
	cmp ax, 5
	je .divis
	cmp ax, 10
	je .divis
	cmp ax, 15
	je .divis
	jmp .nodivis
.divis:
	mov si, productKeySep - D_OFFSET
	call puts
.nodivis:
	
	jmp .start
.end:
	ret


ProductKey:
	call ResetScreen
	mov si, pkeystart - D_OFFSET
	call puts
	call RenderKey
	mov si, pkeyend - D_OFFSET
	call puts

.AwaitKeyPress:
	xor ax, ax
	int 0x16

	cmp ah, 0x1C
	je ProductKeyCheck
	cmp ah, 0x01	;ESC			;0x3D	;F3
	je shutdown
	cmp ah, 0x0E
	je .ProductKeyBackspace

	;chuck out wild values
	cmp al, 'z'
	jg ProductKey
	cmp al, '3'		;0, 1 and 2 aren't valid		'
	jl ProductKey

	cmp al, ':'
	je ProductKey
	cmp al, ';'
	je ProductKey
	cmp al, '<'
	je ProductKey
	cmp al, '='
	je ProductKey
	cmp al, '>'
	je ProductKey
	cmp al, '?'
	je ProductKey
	cmp al, '@'
	je ProductKey
	cmp al, '['
	je ProductKey
	cmp al, '\'			;'
	je ProductKey
	cmp al, ']'
	je ProductKey
	cmp al, '^'
	je ProductKey
	cmp al, '_'
	je ProductKey
	cmp al, '`'
	je ProductKey

	cmp al, 'O'
	je ProductKey
	cmp al, 'I'
	je ProductKey
	cmp al, 'Z'
	je ProductKey
	cmp al, 'o'
	je ProductKey
	cmp al, 'i'
	je ProductKey
	cmp al, 'z'
	je ProductKey


	cmp al, 'a'
	jge .MakeUpper
	jmp .EndMakeUpper

.MakeUpper:
	cmp al, 'z'
	jle .ReallyMakeUpper
	jmp .EndMakeUpper

.ReallyMakeUpper:
	and al, ~32

.EndMakeUpper:

	cmp [productKeyStringPtr - D_OFFSET], byte 20
	je ProductKey

	mov bh, 0
	mov bl, [productKeyStringPtr - D_OFFSET]
	
	mov [productKeyString - D_OFFSET + bx], al
	inc byte [productKeyStringPtr - D_OFFSET]

	jmp ProductKey

.ProductKeyBackspace:
	cmp byte [productKeyStringPtr - D_OFFSET], 0
	je ProductKey

	dec byte [productKeyStringPtr - D_OFFSET]
	mov bh, 0
	mov bl, [productKeyStringPtr - D_OFFSET]
	mov [productKeyString - D_OFFSET + bx], byte 177
	jmp ProductKey

ProductKeyCheck:
	cmp [productKeyStringPtr - D_OFFSET], byte 20
	;@@@;jne ProductKey

	mov si, ProductKeyWaitMsg - D_OFFSET
	call puts

	mov ax, 0
	mov fs, ax

	mov bx, 0

	mov cx, 20			;20 just in case
.startCopyLoop:
	mov al, [productKeyString + bx]
	mov [fs:0x600 + bx], al
	inc bx
	loop .startCopyLoop

	mov [fs:0x600 + 20], byte 0			;0x600[0] to 0x600[19] have the data, put a null at 0x600[20]

	;... copy productKeyString to a 0x600, so p-mode can read it from there

	jmp SkipGDT

.data
GDT64:                           ; Global Descriptor Table (64-bit).
      dd      0  
	  dd 	  0
      dw     -1, 0, 0x9A00, 0xcf
      dw     -1, 0, 0x9200, 0xcf

GDT64Pointer:                    ; The GDT-pointer.
    dw $ - GDT64 - 1             ; Limit.
    dq GDT64                     ; Base.
.text	

SkipGDT:
	call TestProductKey32

TestProductKey32:
	mov ax, 0
	mov fs, ax
	mov [0x51A0], sp
	mov eax, esp
	mov cr3, eax
	cli

	push es
	;copy name
	mov edi, 0x5100
	mov esi, EBX
	mov ax, 0
	mov es, ax
	rep movsb
	pop es

	call enable_A20

	xor ax, ax 
	mov ds, ax
	mov es, ax
	mov ss, ax
	mov fs, ax

	;copy drive number
	mov al, [driveNumber - D_OFFSET]
	mov byte [fs:0x5000], AL

	mov [fs:0x500], dword 0
	mov [fs:0x504], dword 0

	;32 bit p-mode code
	mov [fs:0x508], word 0xFFFF
	mov [fs:0x50A], word 0
	mov [fs:0x50C], word 0x9A00
	mov [fs:0x50E], word 0xCF
	
	;32 bit p-mode code
	mov [fs:0x510], word 0xFFFF
	mov [fs:0x512], word 0
	mov [fs:0x514], word 0x9200
	mov [fs:0x516], word 0xCF
	
	;16 bit p-mode code
	mov [fs:0x518], word 0xFFFF
	mov [fs:0x51A], word 0
	mov [fs:0x51C], word 0x9A00
	mov [fs:0x51E], word 0x0F

	;16 bit p-mode data
	mov [fs:0x520], word 0xFFFF
	mov [fs:0x522], word 0
	mov [fs:0x524], word 0x9200
	mov [fs:0x526], word 0x0F

	mov [fs:0x528], word 0x27
	mov [fs:0x52A], dword 0x500

	mov eax, 0x528
	lgdt [eax]        ; load GDT into GDTR

	mov ax, 0x10
    mov ds, ax
    mov es, ax
    mov fs, ax
    mov gs, ax
    mov ss, ax
    jmp .flush2
	.flush2:

	cli
	mov eax, cr0 
	or al, 1		; set PE (Protection Enable) bit in CR0 (Control Register 0)
	mov cr0, eax

	jmp 0x8:ProtectedModeForPK + 0x7C00

align 8
bits 32
ProtectedModeForPK:
	cli
	mov	ax, 0x10
	mov	ds, ax
	mov	ss, ax
	mov	es, ax
	mov	esp, 0x3F0000  ;4MB
	cld


	;	0x5200 used internally by C code
	;
	;IN:
	;	0x5000 = drive number	(BYTE)
	;	0x5100 = filename		(STRING)
	;	0x6000 = driver code	(CODE)
	;OUT:
	;	0x5004 = start LBA		(DWORD)
	;	0x5008 = file size		(DWORD)
	;	0x500C = success?		(DWORD)


	mov eax, 0
	call isoDriver

	;VMWARE GETS TO AT LEAST HERE

	mov ebp, [0x5004]
	mov ecx, [0x5008]
	mov edx, [0x500C]

	cli ; 8.9.2. Step 1.

	mov eax,cr0 ; 8.9.2. Step 2.
	and eax,0x7FFFFFFF	;0x7FFFFFFF
	mov cr0,eax

	;jmp 0x18:prot16 + 0x7c00 ; 8.9.2. Step 3.
	jmp 0x18:prot16pk + 0x7C00

[BITS 16]

prot16pk:
	mov ax,0x0020 ; 8.9.2. Step 4.
	mov ds,ax
	mov es,ax
	mov fs,ax
	mov gs,ax
	mov ss,ax

	mov eax,cr0 ; 8.9.2. Step 2.
	and al,0xFE	
	mov cr0,eax	;FREEZE!

	;jmp 0x50:0x50
	
	jmp word 0x7C0:real16pk		; 8.9.2. Step 7.

align 16
bits 16
real16pk:
	mov ax, 0x7C0
	mov ds, ax
	mov es, ax
	mov fs, ax
	mov gs, ax

	mov ax, 0x500
    mov ss, ax
    mov sp, 0			;will wrap around to 0xFFFF and on...
    sti

	;restore old stack (we must saved it before this whole journey)
	mov esp, 0
	mov sp, [0x51A0]
	mov eax, cr3
	mov esp, eax
	pop BX

	xor ax, AX
	mov fs, AX

	mov eax, [fs:0x5004]
	mov ecx, [fs:0x5008]
	mov edx, [fs:0x500C]

	cmp byte [fs:0x5FF], 'Y'
	je .ProductKeyIsGood
	jmp BadProductKey

.ProductKeyIsGood:
	jmp Stage555

BadProductKey:
	mov [productKeyStringPtr - D_OFFSET], byte 0

	mov cx, 20
	mov al, 177
	mov di, productKeyString - D_OFFSET
	rep stosb

	jmp ProductKey

enable_A20:
        cli
 
        call    a20wait
        mov     al,0xAD
        out     0x64,al
 
        call    a20wait
        mov     al,0xD0
        out     0x64,al
 
        call    a20wait2
        in      al,0x60
        push    eax
 
        call    a20wait
        mov     al,0xD1
        out     0x64,al
 
        call    a20wait
        pop     eax
        or      al,2
        out     0x60,al
 
        call    a20wait
        mov     al,0xAE
        out     0x64,al
 
        call    a20wait
        ret
 
a20wait:
        in      al,0x64
        test    al,2
        jnz     a20wait
        ret
 
 
a20wait2:
        in      al,0x64
        test    al,1
        jz      a20wait2
        ret
		