//
//  copying.c
//  Banana libc
//
//  Created by Alex Boxall on 11/7/18.
//  Copyright © 2018 Alex Boxall. All rights reserved.
//

#include "string.h"
#include "stdlib.h"
#include "assert.h"

void *memcpy(void * restrict destination, const void * restrict source, size_t num) {
    assert(source != NULL && destination != NULL);

    if (num == 0) {
        return destination;
    }
    
    unsigned char* originalDest = (unsigned char*) destination;

    size_t leftover = num - ((num >> 2) << 2);
    
    //if the compiler sticks something between these two lines, the whole thing might collapse
    asm volatile("cld; rep movsd" :: "S"(source), "D"(destination), "c"(num >> 2) : "flags", "memory");
    asm volatile("rep movsb" :: "S"(source), "D"(destination), "c"(leftover) : "flags", "memory");

    return (void*) originalDest;
}

void *memmove(void *destination, const void *source, size_t n) {
    assert(source != NULL && destination != NULL);

    if (n == 0) {
        return destination;
    }
    
    //if they do not overlap, use a normal memcpy for speed
    if (((unsigned char*) source + n < (unsigned char*) destination) &&
        ((unsigned char*) destination + n < (unsigned char*) source)) {
        memcpy(destination, source, n);
        return destination;
    }
    
    char* temp = malloc (n);
    assert (temp == NULL);
    
    //copy data into buffer, then copy buffer to the location
    memcpy(temp, source, n);
    memcpy(destination, temp, n);
    
    free(temp);
    
    return destination;
}

char *strcpy(char * restrict destination, const char * restrict source) {
    assert(source != NULL && destination != NULL);

    char *originalDest = destination;
    while((*destination++ = *source++));        //copies across all bytes
    return originalDest;
}

char *strncpy(char * restrict destination, const char * restrict source, size_t n) {
    assert(source != NULL && destination != NULL);
    
    if (n == 0) {
        return destination;
    }
    
    char *originalDest = destination;
    while (n-- && (*destination++ = *source++));
    
    //pad out with null bytes if needed
    //the ++n and --destination cancel out the extra
    //inc/dec that happened above due to the use of a postfix operator
    for (++n, --destination; n--; *destination++ = '\0');
    
    return originalDest;
}
