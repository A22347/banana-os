#include <core/string.h>
/*
void* memcpy (void* destination, const void* source, size_t num)
{
	const void* osource = source;
	void* odest = destination;
#if PLATFORM_ID == 64
	size_t leftover = num - ((num >> 3) << 3);
	size_t bytesdone = num - leftover;

	// the asm code automatically increments destination (remember, rep movsq increments RDI)
	asm volatile("cld ; rep movsq" :: "S"(source), "D"(destination), "c"(num >> 3) : "flags", "memory");
	//asm volatile("cld ; rep movsb" :: "S"(source), "D"(destination), "c"(leftover) : "flags", "memory");

	for (size_t i = 0; i < leftover; ++i) {
		*(((uint8_t*) odest) + bytesdone + i) = *(((uint8_t*) osource) + bytesdone + i);
	}
#else
	size_t leftover = num - ((num >> 2) << 2);
	size_t bytesdone = num - leftover;

	// the asm code automatically increments destination (remember, rep movsq increments RDI)
	asm volatile("cld ; rep movsd" :: "S"(source), "D"(destination), "c"(num >> 2) : "flags", "memory");
	//asm volatile("cld ; rep movsb" :: "S"(source), "D"(destination), "c"(leftover) : "flags", "memory");

	for (size_t i = 0; i < leftover; ++i) {
		*(((uint8_t*) odest) + bytesdone + i) = *(((uint8_t*) osource) + bytesdone + i);
	}
#endif
	return destination;
}

int isalnum (int c)
{
	return isalpha (c) || isdigit (c);
}

int isalpha (int c)
{
	return islower (c) || isupper (c);
}

int iscntrl (int c)
{
	return c < 32 || c == 127;
}

int isdigit (int c)
{
	return c > 47 && c < 58;
}

int isgraph (int c)
{
	return isalnum (c) || ispunct (c);
}

int islower (int c)
{
	return c > 96 && c < 123;
}

int isprint (int c)
{
	return isgraph (c) || isspace (c);
}

int ispunct (int c)
{
	return !isalnum (c) && !iscntrl (c) && !isspace (c);
}

int isspace (int c)
{
	switch (c) {
	case ' ':
	case '\n':
	case '\t':
	case '\r':
	case '\f':
	case '\v':
		return 1;
	default:
		return 0;
	}
	return 0;
}

int isupper (int c)
{
	return c > 64 && c < 91;
}

int isxdigit (int c)
{
	if (isdigit (c)) {
		return 1;
	}
	switch (c) {
	case 'A':
	case 'B':
	case 'C':
	case 'D':
	case 'E':
	case 'F':
	case 'a':
	case 'b':
	case 'c':
	case 'd':
	case 'e':
	case 'f':
		return 1;
	default:
		return 0;
	}
	return 0;
}

int tolower (int c)
{
	if (c >= 65 && c <= 90) {
		return c + 32;
	}
	return c;
}

int toupper (int c)
{
	if (c >= 97 && c <= 122) {
		return c - 32;
	}
	return c;
}

int strlen (const char* str)
{
	int ret = 0;
	while (str[ret++]);
	return ret;
}

int strcmp (const char* s1, const char* s2)
{
	while (*s1 && (*s1 == *s2))
		s1++, s2++;
	return *(const unsigned char*) s1 - *(const unsigned char*) s2;
}

int strcasecmp (const char* s1, const char* s2)
{
	while (*s1 && (toupper (*s1) == toupper (*s2)))
		s1++, s2++;
	return *(const unsigned char*) s1 - *(const unsigned char*) s2;
}

char* strcpy (char* dest, const char* src)
{
	char* temp = dest;
	while ((*dest++ = *src++));
	dest[strlen (src)] = 0;
	return temp;
}

char* strcat (char* dest, const char* src)
{
	char* temp = dest;
	while (*dest) ++dest;
	while ((*dest++ = *src++));
	return temp;
}

char* strstr (char* main, char* sub)
{
	for (int i = 0; main[i]; ++i) {
		bool failed = false;
		for (int a = 0; sub[a]; ++a) {
			if (main[i + a] != sub[a]) {
				failed = true;
				break;
			}
		}
		if (!failed) {
			return main + i;
		}
	}
	return 0;
}

void* memset (void* b, int c, int len)
{
	if (!b) {
		logk ("memset error! b = 0\n");
		return 0;
	}

	int d0, d1;
	__asm__ __volatile__ (
		"rep\n\t"
		"stosb"
		: "=&c" (d0), "=&D" (d1)
		: "a" (c), "1" (b), "0" (len)
		: "memory");
	return b;
}


int memcmp (const char *cs_in, const char *ct_in, size_t n)
{
	size_t i;
	const unsigned char * cs = (const unsigned char*) cs_in;
	const unsigned char * ct = (const unsigned char*) ct_in;

	for (i = 0; i < n; i++, cs++, ct++) {
		if (*cs < *ct) {
			return -1;
		} else if (*cs > *ct) {
			return 1;
		}
	}
	return 0;
}

void memmove (void *dest, void *src, size_t n)
{
	char *csrc = (char *) src;
	char *cdest = (char *) dest;

	char *temp = malloc (n);

	for (size_t i = 0; i < n; i++)
		temp[i] = csrc[i];

	for (size_t i = 0; i < n; i++)
		cdest[i] = temp[i];

	logk ("@@FR_EE MEMMOVE\n");
	freee (temp);
}*/