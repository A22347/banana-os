.section .text

.extern __i_libraryInit
.global _start
.global __start
_start:
__start:
	# Set up end of the stack frame linked list.
	movq $0, %rbp
	pushq %rbp
	pushq %rbp
	movq %rsp, %rbp

	# Run the global constructors.
	call _init

	# This inits the lib, sets up the terminal session, parses cmd line args and CALLS MAIN
	call __i_libraryInit
	
	# Call 'special stuff'
	call _fini

	movl %eax, %edi
	call exit

.size _start, . - _start
