#pragma once

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <stdarg.h>
#include <iso646.h>

#define explicit_fallthrough __attribute__ ((fallthrough));

extern uint8_t sseEnabled;

/*
THROUGH TESTING IT HAS BEEN DECIDED THAT
THE *LIKELY* CAUSE OF 32 BIT CRASHING
IS SOMEWHERE IN THE INTERRUPT HANDLING CODE,
AS IT CRASHES WHEN MOUSE INTS ARE ENABLED
BUT IF INTERRUPTS ARE OFF, IT DOESN'T CRASH
UNTIL THEY GET TURNED *ON*
*/

#ifdef THIRTYTWOBIT
#define PLATFORM "x86"
#define PLATFORM_ID 86
#else
#define PLATFORM "x86-64"
#define PLATFORM_ID 64
#endif
#define KERNEL_CODE 1

#define CURRENT_YEAR 2019                           // Change this each year!
#define TASK_CURRENT_THREAD TASKS[TASK_Next].currentlyExecutingThread

#define DECLARE_LOCK(name) volatile int name ## Locked = 0;
#define __LOCK(name)  \
	while (name ## Locked) {asm ("pause"); /*logk("CPU %d is spinning. %s, %s, %d...\n", getCPUNumber(), __FILE__, __func__, __LINE__);*/} \
	 name ## Locked = 1; /*\*/
	/*logk("CPU %d LOCKED A LOCK (%s, %d)\n", getCPUNumber(), __FILE__, __LINE__); \*/
	/*__sync_synchronize();*/

//while (!__sync_bool_compare_and_swap (& name ## Locked, 0, 1)) {logk("CPU %d is spinning. %s, %s, %d...\n", getCPUNumber(), __FILE__, __func__, __LINE__);}

#define __UNLOCK(name)  \
	/*__sync_synchronize();*/ \
	name ## Locked = 0;/*\*/
//logk ("CPU %d UNLOCKED A LOCK (%s, %d)\n", getCPUNumber (), __FILE__, __LINE__);


#define LOCK(name)  __LOCK(name);
#define UNLOCK(name) __UNLOCK(name);

#define XXXXX __SPINLOCK ## __func__
#define CREATE_LOCK error //DECLARE_LOCK(XXXXX)
#define LOCK_LOCK error?//LOCK(XXXXX)
#define UNLOCK_LOCK error!?//UNLOCK(XXXXX)

#define RAM_TABLE_LOCATION					0x00000600		//filled in by the bootloader
#define SMP_DATA_LOCATION					0x00007A00		//must be in the low MB
#define ROOT_PAGE_LOCATION					0x0000A000
#define AHCI_PORT_BASE						0x00020000		//uses at least 296KB, can be anywhere which is 1KB aligned
//STACKS HERE
#define KERNEL_LOCATION						0x00400000	
//STACKS HERE
#define INTERNAL_MALLOC_LOCATION			0x00800000		//enough to allocate 256MB worth of data, used for KERNEL MALLOCS(), NOT APPLICATIONS
#define FRAME_PAGE_ALLOCATOR_INTERNAL_DATA	0x00820000		//enough to store 1 byte per page for 4GB
#define KMALLOC_DATA_LOCATION				0x00920000		//USED FOR INTERNAL KERNEL THINGS
#define MAX_KMALLOC_DATA_LOCATION			0x00FFFFFF		//0x0B4FFFFF		
#define OTHER_PAGING_TABLES					0x01000000			//tables 1
#define PAGING_BASE							0x01800000
#define FRAME_PAGE_ALLOCATOR_START			0x02000000	//0x0B500000		//USED FOR APPS
#define PROGRAM_VIRTUAL_BASE				0x10000000
#define MAX_DATA_LOCATION					0xFFFFFFFF

/*
#define RAM_TABLE_LOCATION					0x00000600		//filled in by the bootloader
#define SMP_DATA_LOCATION					0x00007A00		//must be in the low MB
#define ROOT_PAGE_LOCATION					0x0000A000
#define AHCI_PORT_BASE						0x00020000		//uses at least 296KB, can be anywhere which is 1KB aligned
#define KERNEL_LOCATION						0x00400000	
#define INTERNAL_MALLOC_LOCATION			0x00C00000		//enough to allocate 256MB worth of data, used for KERNEL MALLOCS(), NOT APPLICATIONS
#define FRAME_PAGE_ALLOCATOR_INTERNAL_DATA	0x00E00000		//enough to store 1 byte per page for 4GB
#define OTHER_PAGING_TABLES					0x01000000			//tables 1
#define PAGING_BASE							0x01800000
#define KMALLOC_DATA_LOCATION				0x02000000			//USED FOR INTERNAL KERNEL THINGS
#define MAX_KMALLOC_DATA_LOCATION			0x027FFFFF	//0x0B4FFFFF		
#define FRAME_PAGE_ALLOCATOR_START			0x02800000	//0x0B500000		//USED FOR APPS
#define PROGRAM_VIRTUAL_BASE				0x10000000
#define MAX_DATA_LOCATION					0xFFFFFFFF
*/
#define FRAME_PAGE_ALLOCATOR_INTERNAL_DATA_SIZE (OTHER_PAGING_TABLES-FRAME_PAGE_ALLOCATOR_INTERNAL_DATA)
#define PHYSICAL_MEMORY_SIZE (MAX_DATA_LOCATION - FRAME_PAGE_ALLOCATOR_START)

#define STACK_MAX_SIZE (1024 * 1024 * 64)		//technically this is limitless
#define MAX_TASKS 512
#define MAX_THREADS_PER_TASK 8
#define LAST_ARGUMENT "__ARGUMENT_LAST__!dsfhnzf8d99%$ufus7nN)DFy98--53248l zhHSfgm00       =!!~oiu u8&&` ASU*R(e6rta"

#if PLATFORM_ID == 64
#define KERNEL_FILENAME "C:/BANANA/SYSTEM/KERNEL64.EXE"
#else
#define KERNEL_FILENAME "C:/BANANA/SYSTEM/KERNEL32.EXE"
#endif

//cool branch predition macros e.g. if (unlikely(x == 5)) {...}
#define likely(x)       __builtin_expect((x),1)
#define unlikely(x)     __builtin_expect((x),0)

#define VERSION "v0.19" 
#define CELLW 8						//8
#define CELLH 14					//14
#define TIMER_HERTZ 25	

#define CRASH_DEBUG logk ("No crashes yet on line %d, %s:%s\n", __LINE__, __FILE__, __func__);

#define MOUSEH 22
#define MOUSEW 16

#define Q(string) #string
#define assert(condition) ((void)(condition ? 0 : panicWithMessage("ASSERTION_ERROR")))

typedef struct RegistryQuery
{
	bool string;
	uint32_t returnedInt;
	char* returnedString;

} RegistryQuery;

void writeStartupText(char* text);

enum Months
{
	InvalidMonth,
	January,
	February,
	March,
	April,
	May,
	June,
	July,
	August,
	September,
	October,
	November,
	December
};

typedef struct datetime_t
{
	uint8_t day;
	uint8_t month;
	uint16_t year;
	uint8_t hour;
	uint8_t minute;
	uint8_t second;
} datetime_t;


enum vga_colour
{
	COLOUR_BLACK = 0,
	COLOUR_BLUE = 0x0000AA,
	COLOUR_GREEN = 0x00AA00,
	COLOUR_CYAN = 0x00AAAA,
	COLOUR_RED = 0xAA0000,
	COLOUR_MAGENTA = 0xAA00AA,
	COLOUR_BROWN = 0xAA5500,
	COLOUR_LIGHT_GREY = 0xAAAAAA,
	COLOUR_GREY = 0x555555,
	COLOUR_LIGHT_BLUE = 0x5555FF,
	COLOUR_LIGHT_GREEN = 0x55FF55,
	COLOUR_LIGHT_CYAN = 0x55FFFF,
	COLOUR_LIGHT_RED = 0xFF5555,
	COLOUR_LIGHT_MAGENTA = 0xFF55FF,
	COLOUR_YELLOW = 0xFFFF55,
	COLOUR_WHITE = 0xFFFFFF,
	COLOUR_CLEAR = 0	// 0x1000000,
};

struct regs
{
#if PLATFORM_ID == 64
	uint32_t gs, fs, es, ds;
	uint64_t r15, r14, r13, r12, r11, r10, r9, r8, edi, esi, ebp, esp, ebx, edx, ecx, eax;
	uint64_t int_no, err_code;
	uint64_t eip, cs, eflags, useresp, ss;
#else
	uint32_t tasknoSaveIgnoreThis, gs, fs, ed, ds;
	uint32_t edi, esi, ebp, esp, ebx, edx, ecx, eax;
	uint32_t int_no, err_code;
	uint32_t eip, cs, eflags, useresp, ss;
#endif
};

extern uint16_t ramTableLength;

extern bool debugMode;
/*
Setting to 0:
- removes debug statements to COM1
- nothing else (for now)

*/

extern bool tasksStarted;
extern bool safeMode;

extern void *irq_routines[24];

extern char timezone[256];
extern bool plusUTC;
extern uint8_t hours;
extern bool halfHour;

extern char monthStrings[13][4];

extern unsigned int LastInt;

extern volatile int* volatile PIDofEachCPU;
extern volatile int* volatile letsWait;

extern bool numberLock;
extern bool scrollLock;
extern uint8_t numberOfFloppies;

extern bool loginTaskShown;

extern bool filesystemReady;

extern uint32_t SYSTEM_PUTCHAR_COLOUR_;
extern char username[128];
extern char userDesktop[128];
extern char userDocuments[128];
extern char userFolder[128];
extern char userBin[128];
extern char userPhotos[128];
extern char userMusic[128];
extern int lastAddedPIDBySystemCommand;

extern bool readOnlyMode;
extern bool hasAPIC;

extern uint8_t processorID[256];
extern uint8_t matchingAPICID[256];
extern uint8_t processorDiscoveryNumber;

//e.g. 0xFEC..., not 0xFEE... (use cpuGetAPICBase () instead)
extern uint32_t ioapicAddresses[256];
extern uint8_t ioapicFoundInMADT[256];		//the IDs of IOAPICs found on the system (using the MADT ACPI table)
extern uint8_t ioapicDiscoveryNumber;
extern uint8_t legacyIRQRemaps[16];

extern bool __internal_login_variable__loginComplete;
extern char __internal_login_variable__loginUsername[256];

extern bool forceDisableAPIC;

extern int64_t freeBytesApprox;
extern bool lastUpdateStartMenuState;
extern int numberOfCPUsUsed;
extern bool endprocess;
extern bool kbused;
extern bool ddebug;
extern bool overtext;
extern int mousex;
extern int mousey;
extern volatile int TASK_Current;
extern volatile int TASK_Next;
extern volatile uint8_t flagForCPUTimingSetByExecutionAssemblyStub;
extern bool firstSplashScreenLoad;
extern int lastInt;
extern int slptk;
extern bool keyPressed;

extern bool systemReady;

void getFilesystemReady ();

uint32_t getCPUNumber ();

extern int syscallreturn;

extern bool systemReady;
extern bool filesystemReady;
extern bool screenNeedsUpdating;

extern volatile uint8_t* volatile otherCPUsMayExecute;
extern volatile uint8_t* volatile nextCPUToRun;

