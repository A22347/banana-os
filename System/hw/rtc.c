#include <hw/rtc.h>
#include <hw/ports.h>
#include <hw/cmos.h>

int get_update_in_progress_flag()
{
	return readCMOS(0xA) & 0x80;
}


uint64_t rtcReadTimeUTC ()
{
	unsigned char last_second;
	unsigned char last_minute;
	unsigned char last_hour;
	unsigned char last_day;
	unsigned char last_month;
	unsigned char last_year;
	unsigned char registerB;

	while (get_update_in_progress_flag ());
	uint8_t second = readCMOS(0x00);
	uint8_t minute = readCMOS(0x02);
	uint8_t hour = readCMOS(0x04);
	uint8_t day = readCMOS(0x07);
	uint8_t month = readCMOS(0x08);
	uint32_t year = readCMOS(0x09);

	do {
		last_second = second;
		last_minute = minute;
		last_hour = hour;
		last_day = day;
		last_month = month;
		last_year = year;

		while (get_update_in_progress_flag ());
		second = readCMOS(0x00);
		minute = readCMOS(0x02);
		hour = readCMOS(0x04);
		day = readCMOS(0x07);
		month = readCMOS(0x08);
		year = readCMOS(0x09);

	} while ((last_second != second) || (last_minute != minute) || (last_hour != hour) ||
		(last_day != day) || (last_month != month) || (last_year != year));

	registerB = readCMOS(0x0B);

	if (!(registerB & 0x04)) {
		second = (second & 0x0F) + ((second / 16) * 10);
		minute = (minute & 0x0F) + ((minute / 16) * 10);
		hour = ((hour & 0x0F) + (((hour & 0x70) / 16) * 10)) | (hour & 0x80);
		day = (day & 0x0F) + ((day / 16) * 10);
		month = (month & 0x0F) + ((month / 16) * 10);
		year = (year & 0x0F) + ((year / 16) * 10);
	}

	if (!(registerB & 0x02) && (hour & 0x80)) {
		hour = ((hour & 0x7F) + 12) % 24;
	}

	year += (CURRENT_YEAR / 100) * 100;
	if (year < CURRENT_YEAR) {
		year += 100;
	}

	datetime_t d;
	d.second = second;
	d.minute = minute;
	d.hour = hour;
	d.day = day;
	d.month = month;
	d.year = year;
	return datetimeToSeconds (d);
}