#include <sync/semaphore.h>

extern void yieldFromKernel();

//these are kernel functions
//it would be easy to make syscalls around them:
//
// SC_SemaphoreInit takes a pointer to a userland semaphore value
// SC_SemaphoreWait takes a pointer to a userland semaphore value
//                  and DOES NOT LEAVE THE KERNEL until it aquires
//                  the semaphore
// SC_SemaphoreSignal takes a pointer to a userland semaphore value
void semaphoreInit(semaphore_t* sema, int value) {
    sema->value = value;
    spinlockInit(&sema->spinlock);
}

void semaphoreWait(semaphore_t* sema) {
    while (1) {
        //probably should force the task to sleep
        yieldFromKernel();
        if (sema->value < 0) {
            spinlockLock(&sema->spinlock);      //multiprocessing consideration
            asm ("cli");

            //now we are the only process running, do we still have it
            //set to zero? or did someone get in before us?
            if (sema->value < 0) {
                sema->value--;
                
                spinlockUnlock(&sema->spinlock);
                asm("sti");
                return;
                
            } else {
                spinlockUnlock(&sema->spinlock);
                asm("sti");
            }
        }
    }
}

void semaphoreSignal(semaphore_t* sema) {
    //probably should force all tasks waiting on it to wake up
    spinlockLock(&sema->spinlock);
    sema->value++;
    spinlockUnlock(&sema->spinlock);
}
