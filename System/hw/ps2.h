#include <stdint.h>
#include <stdbool.h>

#include <devss/devices.h>

#define PS2_READ_INTERNAL_RAM_PORT_BASE 0x20
#define PS2_WRITE_INTERNAL_RAM_PORT_BASE 0x60
#define PS2_DISABLE_PORT_2 0xA7
#define PS2_ENABLE_PORT_2 0xA8
#define PS2_TEST_PORT_2 0xA9
#define PS2_TEST_CONTROLLER 0xAA
#define PS2_TEST_PORT_1 0xAB
#define PS2_DIAGNOSTIC_DUMP 0xAC
#define PS2_DISABLE_PORT_1 0xAD
#define PS2_ENABLE_PORT_1 0xAE
#define PS2_READ_CONTROLLER_INPUT 0xC0
#define PS2_READ_CONTROLLER_OUTPUT 0xD0
#define PS2_WRITE_BYTE_CONTOLLER_OUTPUT 0xD1
#define PS2_WRITE_BYTE_PORT_1_OUTPUT 0xD2
#define PS2_WRITE_BYTE_PORT_2_OUTPUT 0xD3
#define PS2_WRITE_BYTE_PORT_2_INPUT 0xD4

#define PS2_DATA_PORT 0x60
#define PS2_STATUS_REG 0x64
#define PS2_COMMAND_REG 0x64

#define PS2_DEVICE_INDENTIFY 0xF2
#define PS2_DEVICE_DISABLE_SCANNING 0xF5
#define PS2_DEVICE_ENABLE_SCANNING 0xF4

void openPS2(struct Device* self, int, int, void*);

void sendCommandPS2ControllerWithArg(uint8_t command, uint8_t byte);
void sendCommandPS2Controller(uint8_t command);
uint8_t getResponseFromController();

void writePS2Device(uint8_t command, bool secondPort);

void waitForACK();