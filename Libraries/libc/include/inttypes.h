//
//  inttypes.h
//  Banana libc
//
//  Created by Alex Boxall on 11/7/18.
//  Copyright © 2018 Alex Boxall. All rights reserved.
//

#ifndef inttypes_h
#define inttypes_h

#if defined(__cplusplus)
#define restrict			//fixes some 'fun' errors
extern "C" {
#endif
    
#include <stdint.h>     //the standard says to do so
    
    typedef struct imaxdiv_t {
        intmax_t quot;
        intmax_t rem;
        
    } imaxdiv_t;
    
#if defined(__cplusplus)
}
#endif

#endif /* inttypes_h */
