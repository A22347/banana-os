#pragma once

#include <core/kernel.h>		//PLATFORM_ID + the standard numerical data types
#include <fs/vfs.h>				//vfs_file_t
#include "D:\Users\Alex\Desktop\banana-os\Libraries\newlib-3.0.0\newlib\libc\sys\banana\sys\dirent.h"

typedef uint64_t InternalObjectID;

InternalObjectID createNewStdin ();
InternalObjectID createNewFileObject (char* filename, uint8_t mode);
InternalObjectID createNewDirectoryObject (char* filename);
void closeFileObject(InternalObjectID id);
bool fileObjectGetErrorBit(InternalObjectID obj);
vfs_file_t* getFileObjectFromObject(InternalObjectID id);

int readDirObject (InternalObjectID id, struct dirent* dir);
void closeDirObject (InternalObjectID id);

//the kernel is the only thing allowed to touch the actual object structure
//but interfaces will exist to extract the data from it, as well as perform
//operations on it. e.g. an object of type 'Device' might call a device driver
//when it it is read from by the system, or an object of type 'File' might
//call a vfs_ function when read from, etc.
//a 'Process' object might have ioctl to delete it, get PID, fork it, etc.

int filePipeStdoutWrite (InternalObjectID id, size_t length, uint8_t* data);
int filePipeStdinRead (InternalObjectID id, size_t length, uint8_t* buffer);

char* getStdinInternalBufferFromStdin (InternalObjectID id);
void flushStdinInternalBuffer (InternalObjectID id);

void createNewPipe(InternalObjectID* outRead, InternalObjectID* outWrite);