#include <sys/wait.h>

pid_t wait (int* returnValue) {
    SystemCall(SC_Wait, 0, 0, returnValue);
    
    time_t et = time(0); + 2;
    while (time(0) < et);
    
    return 0;    // PID of the task which closed
    
}

//wait for change in state
pid_t waitpid (pid_t process, int* returnValue, int options) {
    return 0;
}