#include <hal/clock.h>
#include <fs/vfs.h>
#include <libk/string.h>
#include <core/memory.h>
#include <core/registry2.h>
#include <core/panic.h>
#include <debug/log.h>

uint64_t datetimeToSeconds (datetime_t d)
{
	uint8_t day = d.day;
	uint8_t month = d.month;
	uint16_t year = d.year;
	uint8_t hours = d.hour;
	uint8_t minutes = d.minute;
	uint8_t seconds = d.second;
	uint64_t output = 0;
	if (((year % 4 == 0) && !(year % 100 == 0)) || year % 400 == 0) {
		output = 1;     //give Feburary an extra day for this year
	}
	uint16_t tempYears = year;
	uint16_t leapYears = 0;
	while (tempYears > 1601) {
		if (((tempYears % 4 == 0) && !(tempYears % 100 == 0)) || tempYears % 400 == 0) {
			++leapYears;
		}
		--tempYears;
	}
	output += (year - 1601) * 365 + leapYears;

	switch (month - 1) {
	case 12:
		output += 31;
		explicit_fallthrough;
	case 11:
		output += 30;
		explicit_fallthrough;
	case 10:
		output += 31;
		explicit_fallthrough;
	case 9:
		output += 30;
		explicit_fallthrough;
	case 8:
		output += 31;
		explicit_fallthrough;
	case 7:
		output += 31;
		explicit_fallthrough;
	case 6:
		output += 30;
		explicit_fallthrough;
	case 5:
		output += 31;
		explicit_fallthrough;
	case 4:
		output += 30;
		explicit_fallthrough;
	case 3:
		output += 31;
		explicit_fallthrough;
	case 2:
		output += 28;
		explicit_fallthrough;
	case 1:
		output += 31;
		explicit_fallthrough;
	default:
		break;
	}

	output += day - 1;
	output *= 3600 * 24;
	output += seconds;
	output += minutes * 60;
	output += hours * 3600;

	return output;
}

datetime_t secondsToDatetime (uint64_t data)
{
	uint8_t seconds = 0;
	uint8_t minutes = 0;
	uint8_t hours = 0;
	uint8_t day = 0;
	uint8_t month = 1;
	uint16_t year = 1601;

	seconds = data % 60;
	minutes = data / 60 % 60;
	hours = data / 60 / 60 % 24;
	data = data / 60 / 60 / 24;

	while (data > 365) {
		data -= 365;
		if (((year % 4 == 0) && !(year % 100 == 0)) || year % 400 == 0) {
			--data;
		}
		++year;
	}
	if (data == 365) {
		++year;
		data = 0;
	}
	if (((year % 4 == 0) && !(year % 100 == 0)) || year % 400 == 0) {
		data -= 2;
	}

	int j[12] = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
	while (data > 31) {
		data -= j[month++];
	}

	day = data + 1;

	datetime_t d;
	d.second = seconds;
	d.minute = minutes;
	d.hour = hours;
	d.day = day - 1;
	d.month = month;
	d.year = year;
	return d;
}

void loadTimezones ()
{
	vfs_file_t fil;
	int fr;

	memset (timezone, 0, 255);

	/*RegistryQuery query;

	if (QueryRegistry (&query, "TIMEZONE") && query.string) {
		strcpy (timezone, query.returnedString);
	} else {
		panicWithMessage ("TIMEZONE ERROR!");
	}
	free (query.returnedString);*/

	char* tz = readRegistryString("LOCAL/Timezone");
	strcpy(timezone, tz);
	freee(tz);

	logk("WE ARE IN TIMEZONE %s\n", timezone);

	char line[256];
	vfs_open (&fil, "C:/BANANA/RESOURCES/TIMEZONES.TXT", vfs_file_mode_read);
	while (vfs_gets (line, 255, &fil) == 0) {
		if (strstr (line, timezone)) {
			logk ("FOUND!! %s\n", line);
			strcpy (timezone, line);
			break;
		}
	}

	if (timezone[0] == '+') {
		plusUTC = true;
	} else if (timezone[0] == '-') {
		plusUTC = false;
	} else {
		logk ("\nBAD TIMEZONE SPEC.\n");
		return;
	}

	hours = timezone[1] - 48;
	if (timezone[2] == '.') {
		if (timezone[3] == '5') {
			halfHour = true;
		}
	} else {
		hours *= 10;
		hours += timezone[2] - 48;
		if (timezone[4] == '5') {
			halfHour = true;
		}
	}

	logk ("\nTimezone: %s (%s%d.%s)\n", timezone, plusUTC ? "+" : "-", hours, halfHour ? "5" : "0");
}

datetime_t currentTime ()
{
	return secondsToDatetime (getClockTime ());
}

uint64_t getClockTimeUTC ()
{
	return rtcReadTimeUTC ();
}

uint64_t getClockTime ()
{
	uint64_t secs = getClockTimeUTC ();
	if (plusUTC) {
		secs += (hours * 60 * 60) + (halfHour ? 30 * 60 : 0);
	} else {
		secs -= (hours * 60 * 60) + (halfHour ? 30 * 60 : 0);
	}
	datetime_t d = secondsToDatetime (secs);
	uint64_t secs2 = datetimeToSeconds (d);
	datetime_t d2 = secondsToDatetime (secs2);

	return secs;
}