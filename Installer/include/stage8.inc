Stage8:
	call ResetScreen

	mov si, writinghdd - D_OFFSET
	call puts

	mov si, writinghddmsg - D_OFFSET
	call puts

	mov cx, 17
.newlineLoop:
	mov si, Newline - D_OFFSET
	call puts
	loop .newlineLoop

	mov si, FourBlank - D_OFFSET
	call puts

	mov cx,	70
.lp4:
	mov si, Line
	sub si, D_OFFSET
	call puts
	loop .lp4

	mov si, Newline
	sub si, D_OFFSET
	call puts

	mov si, Stage678Footer - D_OFFSET
	call puts


	mov [blkcnt - D_OFFSET], word 1	;read one sector from the CDROM
	mov [db_add - D_OFFSET], word 0x7000	;stick it at 0x8000:0
	mov [db_seg - D_OFFSET], word 0
	push eax
	mov eax, [IMAGE_OFFSET]
	mov [d_lba  - D_OFFSET], eax 		;the HDD image is stored at sector 60 on the CDROM
	pop eax

	;this is done twice, (once in Stage6) but this time it will actually change the
	;values and put them onto the HDD. We do not actually need to recalculate the values
	;but I am too lazy to change it
	push BX
	push es
	mov eax, [selectedSpaceSIZE - D_OFFSET]
	;call getSizeOfDisk
	pop es
	pop bx

	push eax
	mov si, DAPACK - D_OFFSET		; address of "disk address packet"
	mov ah, 0x42		; AL is unused
	mov dl, [driveNumber - D_OFFSET]		; drive number 0 (OR the drive # with 0x80)
	int 0x13
	jc DiskErrorCDROM
	pop eax

	;do some very weird sector 0 Editing

	mov bx, 0
	mov es, bx
	mov bx, 0x7000
	add bx, 0x20

	mov [es:bx], dword eax			;number of sectors

	xor edx, edx
	mov dx, [es:0x700E]

	mov [reservedSectors - D_OFFSET], dx
	
	mov bx, 0x7000
	add bx, 0x10

	push edx
	mov dl, byte [es:bx]		;number of FATs
	mov [numberOfFATs - D_OFFSET], byte dl
	pop edx

	mov ecx, eax		;number of sectors
	sub ecx, edx		;number of sectors from the first FAT onwards
	shr ecx, 7			;div by 128 to get how many sectors needed to store the FAT
	;shr ecx, 2									;round to the nearest 4
	;inc ecx										;inc, just in case
	;shl ecx, 2									;finish rounding
	mov [sectorsPerFATNew - D_OFFSET], ecx
	
	push eax
	mov bx, 0x701C
	mov eax, [selectedSpaceLBA - D_OFFSET]
	mov [es:bx], eax
	pop eax

	mov bx, 0x7000
	add bx, 0x24
	mov edx, [es:bx]			;logical sectors per FAT (old)
	mov [sectorsPerFATOld - D_OFFSET], edx
	mov [es:bx], ecx			;logical sectors per FAT (new)
 
	mov bx, 0x7000
	add bx, 0x24

	mov ebx, 0
	mov bl, [numberOfFATs - D_OFFSET]
	mov eax, [sectorsPerFATOld - D_OFFSET]
	mul ebx										;sectorsPerFATOld * numberOfFATs
	mov edx, 0
	mov dx, [reservedSectors - D_OFFSET]			;get offset
	add eax, edx								;get (old) data offset
	mov [oldDataStartOffset - D_OFFSET], eax

	mov ebx, 0
	mov bl, [numberOfFATs - D_OFFSET]
	mov eax, [sectorsPerFATNew - D_OFFSET]
	mul ebx										;sectorsPerFATNew * numberOfFATs
	mov edx, 0
	mov dx, [reservedSectors - D_OFFSET]			;get offset
	add eax, edx								;get (new) data offset
	;shr eax, 2									;round to the nearest 4
	;inc eax										;inc, just in case
	;shl eax, 2									;finish rounding
	mov [newDataStartOffset - D_OFFSET], eax

	mov eax, [oldDataStartOffset - D_OFFSET]
	mov ebx, [newDataStartOffset - D_OFFSET]

	pusha
	mov di, 5
.RETRYC:
	mov ah, 0x43
	mov al, 0
	mov dl, byte [HDDNumber - D_OFFSET]
	mov [BLK2 - D_OFFSET], dword 4
	mov si, HDDZEROPACKET2 - D_OFFSET
	push EAX
	mov eax, [selectedSpaceLBA - D_OFFSET]
	mov [RDF2 - D_OFFSET], eax
	pop eax
	push di
	int 0x13
	pop di
	jnc .DONEC
	
	cmp di, 3
	jne .skipReset
	xor ax, ax
	int 0x13
.skipReset:

	dec di
	cmp di, 0
	je DiskErrorHDDB
	jmp .RETRYC
.DONEC:
	popa

	; - 1 skips sector 0
	mov ecx, (MAIN_DISK_IMG_MEGABYTES * 2 * 1024) / 4 - 1 ;15359

.clearLoop:
	mov di, 10		;10 retries before failure

.clearLoopSkipDI:
	mov [originalECX - D_OFFSET], ecx

	mov [blkcnt - D_OFFSET], word 1	;read one sector from the CDROM
	mov [db_add - D_OFFSET], word 0x7000	;stick it at 0x8000:0
	mov [db_seg - D_OFFSET], word 0
	mov [d_lba  - D_OFFSET], dword (MAIN_DISK_IMG_MEGABYTES * 2 * 1024) / 4	;reverse the offset - if ecx = 720, read sector 0, if ecx = 719, read sector 1 etc.
	sub [d_lba  - D_OFFSET], dword ecx	
	push eax
	mov eax, [IMAGE_OFFSET]
	add [d_lba  - D_OFFSET], eax 		;the HDD image is stored at sector 60 on the CDROM
	pop eax

	mov eax, [d_lba  - D_OFFSET]
	

	mov si, DAPACK - D_OFFSET		; address of "disk address packet"
	mov ah, 0x42		; AL is unused
	mov dl, [driveNumber - D_OFFSET]		; drive number 0 (OR the drive # with 0x80)
	pusha
	int 0x13
	jnc .ddone1
	popa

	cmp di, 3
	jne .skipReset2
	xor ax, ax
	mov dl, [driveNumber - D_OFFSET]
	int 0x13
.skipReset2:
	dec di
	cmp di, 0
	je DiskErrorCDROM
	jmp .clearLoopSkipDI


.ddone1:
	popa

	pusha
	pusha
	push ecx
	
	mov ecx, [originalECX - D_OFFSET]

	pusha
	mov di, 10		;10 retries before failing
.RETRYD:
	mov ah, 0x43
	mov al, 0
	mov dl, byte [HDDNumber - D_OFFSET]
	mov [BLK2 - D_OFFSET], dword 4			;write 4 sectors to HDD
	mov si, HDDZEROPACKET2 - D_OFFSET
	mov [RDF2 - D_OFFSET], dword 1024 * 2 * MAIN_DISK_IMG_MEGABYTES
	sub [RDF2 - D_OFFSET], ecx
	sub [RDF2 - D_OFFSET], ecx
	sub [RDF2 - D_OFFSET], ecx
	sub [RDF2 - D_OFFSET], ecx

	pusha
	mov eax, [RDF2 - D_OFFSET]			;the actual sector we are using
	cmp eax, [oldDataStartOffset - D_OFFSET]
	jae .pushSectorsAlong
	jmp .donePushSectors

.pushSectorsAlong:
	mov ecx, [RDF2 - D_OFFSET]

	mov eax, [oldDataStartOffset - D_OFFSET]
	mov ebx, [newDataStartOffset - D_OFFSET]

	add ecx, [newDataStartOffset - D_OFFSET]
	sub ecx, [oldDataStartOffset - D_OFFSET]

	mov [RDF2 - D_OFFSET], ecx

.donePushSectors:
	mov eax, [selectedSpaceLBA - D_OFFSET]
	add [RDF2 - D_OFFSET], eax
	popa
	int 0x13
	jnc .DONED

	xor ax, ax
	int 0x13

	dec di
	cmp di, 0
	je DiskErrorHDDB
	jmp .RETRYD
.DONED:
	popa
	;popa

	pop ecx

	clc

	cmp ecx, dword 1024 * 2 * MAIN_DISK_IMG_MEGABYTES
	je .skipMultiplyAndDisplay

	push ecx
	and ecx, 0xF
	cmp ecx, 0
	jne .skipMultiplyAndDisplay
	pop ecx

	;"When I wrote this, only God and I knew how it works. Now only God knows"
	mov eax, 0
	;mov edx, ecx
	;mov ecx, dword 1024 * 2 * MAIN_DISK_IMG_MEGABYTES
	;sub ecx, edx			;flips ECX over (1024 * 2 * MAIN_DISK_IMG_MEGABYTES)
	mov ecx, [originalECX - D_OFFSET]
	shl ecx, 2 
.multiplyLoop:
	add eax, 56				;this multiplies ecx by 58 using repeated addition, and puts it in eax
							;58 = length of progress bar??
	dec ecx
	cmp ecx, 0
	jne .multiplyLoop

	mov edx, 0
	mov esi, dword 1024 * 2 * MAIN_DISK_IMG_MEGABYTES
	div esi

	mov si, lineReset - D_OFFSET
	pusha
	call puts
	popa

	pusha
	mov bh, 0
	mov bl, byte [progressProgress2 - D_OFFSET]
	mov al, byte [progressCycle2 - D_OFFSET + bx]
	mov [Stage678Footer - D_OFFSET + 4], al
	inc bl
	cmp bl, 8
	jne .dontLoopAround
	mov bl, 0

.dontLoopAround:
	mov [progressProgress2 - D_OFFSET], byte bl
	popa

	mov si, Stage678Footer - D_OFFSET
	call puts

	mov ecx, eax
	mov eax, 56
	sub eax, ecx

	mov [mulresult - D_OFFSET], eax

	mov ecx, eax
	cmp ecx, 0
	je .noLoop
	cmp ecx, 56
	jge .setToECXMax
	
.outLoop:
	mov si, percentDoneBlock - D_OFFSET
	pusha
	call puts
	popa
	loop .outLoop
	popa

	pusha
	call .anotherloop
	popa

.noLoop
	popa

	mov ecx, [originalECX - D_OFFSET]

	dec ecx
	cmp ecx, 0
	jne .clearLoop

	jmp Stage9

.skipMultiplyAndDisplay:
	pop ecx
	popa
	jmp .noLoop

.setToECXMax:
	mov ecx, 56
	jmp .outLoop

.nost:
	ret

;EAX = the multiplication result
.anotherloop:
	mov eax, [mulresult - D_OFFSET]
	mov ecx, 56
	sub ecx, eax

	cmp eax, 56
	jge .nost
	
	cmp ecx, 56
	jge .AsetToECXMax	;integer underflow?

.lastcheck:

	cmp ecx, 0
	je .AnoLoop
	
.AoutLoop:
	mov si, percentLeftBlock - D_OFFSET
	pusha
	call puts
	popa
	loop .AoutLoop

	ret

.AnoLoop:
	ret

.AskipMultiplyAndDisplay:
	jmp .AnoLoop

.AsetToECXMax:
	mov ecx, 0			;it must have underflowed to get to here
	ret



