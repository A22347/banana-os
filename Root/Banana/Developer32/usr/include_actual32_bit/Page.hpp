//
//  Page.hpp
//  GUI
//
//  Created by Alex Boxall on 19/6/19.
//  Copyright © 2019 Alex Boxall. All rights reserved.
//

#ifndef Page_hpp
#define Page_hpp

#include "Control.hpp"
#include "KeyStates.hpp"

class Page : public Control {
private:
protected:
    int sx;
    int sy;
    uint32_t outline = 0x000000;
    bool outlineOn = true;
    int boundy;
    
public:
    Page (Control* parent, int x = -1, int y = -1, int w = -1, int h = -1);

    Page* copy();
    virtual void internalDraw();
    
    void setBoundY(int y);
    int getBoundY();

    void keyboard(KeyState k);
    virtual bool canHoldFocus();     //whether the tab key can be used to select this control

    uint32_t getOutlineColour();
    void setOutlineColour(uint32_t col);
    bool isOutlineOn();
    void setOutlineOn(bool on);
    
    void setScroll (int x, int y);
    int getScrollX ();
    int getScrollY ();

    void drawPixel (int xx, int yy, uint32_t col);
    
    virtual const char* debugName();
};

#endif /* Page_hpp */
