#import <NXConstStr.h>

@protocol NSObject
@required
-(BOOL) isEqual: (id) object;
-(NSUInteger) hash;

-(Class) superclass;
-(Class) class;
-(id) self;

-(id) performSelector: (SEL) aSelector;
-(id) performSelector: (SEL) aSelector withObject: (id) object;
-(id) performSelector: (SEL) aSelector withObject: (id) object withObject : (id) object;

-(BOOL) isKindOfClass: (Class) aClass;
-(BOOL) isMemberOfClass: (Class) aClass;
-(BOOL) conformsToProtocol: (Protocol*) aProtocol;

-(BOOL) respondsToSelector: (SEL) aSelector;

-(id) retain NS_AUTOMATED_REFCOUNT_UNAVAILABLE;
-(oneway void) release NS_AUTOMATED_REFCOUNT_UNAVAILABLE;
-(id) autorealese NS_AUTOMATED_REFCOUNT_UNAVAILABLE;
-(NSUInteger) retainCount NS_AUTOMATED_REFCOUNT_UNAVAILABLE;

-(NXConstStr*) description;

@optional
-(NXConstStr*) debugDescription;

@end

@protocol NSCopying
@required
-(id) copy;
@end

@protocol NSMutableCopying
@required
-(id) mutableCopy;
@end

@interface NSObject <NSObject>

+(void) load;

+(void) initialize;
-(id) init;

+(id) new;
+(id) alloc;
-(void) dealloc;

+(Class) superclass;
+(Class) class;
+(BOOL) instancesRespondToSelector: (SEL) aSelector;
+(BOOL) conformsToProtocol: (Protocol *) protocol;
-(IMP) methodForSelector: (SEL) aSelector;
-(id) forwardingTargetForSelector: (SEL) aSelector;
+(IMP) instanceMethodForSelector: (SEL) aSelector;
-(void) doesNotRecognizeSelector: (SEL) aSelector;

-(BOOL) isKindOfClass: (Class) aClass;
+(BOOL) isKindOfClass: (Class) aClass;
-(BOOL) isMemberOfClass: (Class) aClass;
-(BOOL) conformsToProtocol: (Protocol*) aProtocol;

-(NXConstStr*) description;
-(NXConstStr*) debugDescription;
+(NXConstStr*) description;
+(NXConstStr*) debugDescription;
+(BOOL) isSubclassOfClass: (Class) aClass;

-(id) self

@end