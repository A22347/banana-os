#pragma once

typedef struct spinlock_t {
    int locked;
    
} spinlock_t;

void spinlockInit(spinlock_t* lock);
void spinlockLock(spinlock_t* lock);
void spinlockUnlock(spinlock_t* lock);
int spinlockTry(spinlock_t* lock);
