#include <core/prcssthr.h>
#include <hal/floating.h>
#include <core/tss.h>
#include <debug/log.h>
#include <core/exeload.h>
#include <core/scheduler.h>
#include <debug/profile.h>

processData_t* TASKS;

void loadTaskFromASM()
{
	//START_PROFILE;

	int no = TASK_Current;
	int thread = 0;

	strcpy(appOrDriverRunningNameInCaseOfCrash, TASKS[no].filename);

	st_eip = TASKS[no].threads[0].eip;			//only used the first time it is loaded in
	st_esp = TASKS[no].threads[0].esp;			//this might be helpful (just a little, leaving it out definitely did not cause crashes and/or merged task stacks together)
	st_eflags = TASKS[no].threads[0].eflags;	//only used the first time it is loaded in
	st_loadedBefore = TASKS[no].threads[0].loadedBefore;

	TASKS[no].threads[0].loadedBefore = true;

	TASKS[no].threads[0].cwdCorrect = true;

	extern void vfs_chdir_no_check(char* dirname);
	vfs_chdir_no_check(TASKS[TASK_Current].threads[0].cwd);

	tssWriteESP(TASKS[no].threads[0].kernelSwitchStack);

	logk("Switching to task virtual address space of 0x%X\n", TASKS[no].addressSpace);
	switchToVirtualAddressSpace(TASKS[no].addressSpace);

	floatingLoad(TASKS[no].threads[0].floatingStorage);

	//END_PROFILE;
}

void storeDataFromASM()
{
	//START_PROFILE;

	int no = TASK_Current;
	int thread = 0;

	floatingStore(TASKS[no].threads[0].floatingStorage);

	//only store new cwd if it was changed
	if (TASKS[no].threads[0].cwdCorrect) {
		vfs_getcwd(TASKS[no].threads[0].cwd, 255);
	}

	TASKS[no].threads[0].esp = st_esp;
	TASKS[no].threads[0].loadedBefore = true;

	//END_PROFILE;
}

void internalExecve(char* path, char** args, char** envr)
{
	storeExecve(path, args, envr);
}