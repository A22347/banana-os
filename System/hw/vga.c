#include <hw/vga.h>

int vgaAvaliable ()
{
	return 1;
}

void vgaInit (uint8_t displayno)
{

}

void vgaCopyBufferTo (uint32_t* buffer, uint32_t start, uint32_t size, uint8_t displayNo)
{

}

void vgaCopyBufferFrom (uint32_t* buffer, uint32_t start, uint32_t size, uint8_t displayNo)
{

}

void vgaSetPixel (uint32_t location, uint32_t colour, uint8_t displayNo)
{


}

uint32_t vgaGetPixel (uint32_t location, uint8_t displayNo)
{
	return 0;
}