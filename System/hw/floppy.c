#include <hw/floppy.h>
#include <hal/diskio.h>

bool floppyirq = false;
uint8_t _CurrentDrive = 0;

void floppy_handler (struct regs *r)
{
	floppyirq = true;
}

void wait_irq ()
{
	asm ("sti");
	logk("wait_irq floppy\n");
	while (!floppyirq) {
	}
	floppyirq = false;
}

void init_dma ()
{
	outb (0x0a, 0x06);
	outb (0xd8, 0xff);
	outb (0x04, 0x00);
	outb (0x04, 0x10);
	outb (0xd8, 0xff);
	outb (0x05, 0xff);
	outb (0x05, 0x23);
	outb (0x80, 0x00);
	outb (0x0a, 0x02);
}

void read_dma ()
{
	outb (0x0a, 0x06);
	outb (0x0b, 0x56);
	outb (0x0a, 0x02);
}

void write_dma ()
{
	outb (0x0a, 0x06);
	outb (0x0b, 0x5a);
	outb (0x0a, 0x02);
}

void write_dor (uint8_t val)
{
	outb (FLPYDSK_DOR, val);
}

uint8_t read_status ()
{
	return inb (FLPYDSK_MSR);
}

void floppy_send_cmd (uint8_t cmd)
{
	for (int i = 0; i < 500; ++i) {
		if (read_status () & FLPYDSK_MSR_MASK_DATAREG) {
			return outb (FLPYDSK_FIFO, cmd);
		}
	}
}

uint8_t floppy_read_data ()
{
	for (int i = 0; i < 500; ++i) {
		if (read_status () & FLPYDSK_MSR_MASK_DATAREG) {
			return inb (FLPYDSK_FIFO);
		}
	}
	return 0;
}

void write_ccr (uint8_t val)
{
	outb (FLPYDSK_CTRL, val);
}

void motor (bool on)
{
	if (on) {
		write_dor (0 | FLPYDSK_DOR_MASK_DRIVE0_MOTOR | FLPYDSK_DOR_MASK_RESET | FLPYDSK_DOR_MASK_DMA);
		for (int i = 0; i < 20000; ++i) {
			asm ("");
		}
	} else {
		write_dor (FLPYDSK_DOR_MASK_RESET);
	}
}

void floppy_check_int (uint32_t* st0, uint32_t* cyl)
{
	floppy_send_cmd (FDC_CMD_CHECK_INT);
	*st0 = floppy_read_data ();
	*cyl = floppy_read_data ();
}

void floppy_drive_data (uint32_t stepr, uint32_t loadt, uint32_t unloadt, bool dma)
{
	uint32_t data = 0;

	floppy_send_cmd (FDC_CMD_SPECIFY);
	data = ((stepr & 0xf) << 4) | (unloadt & 0xf);
	floppy_send_cmd (data);
	data = (loadt) << 1 | (dma == false) ? 0 : 1;
	floppy_send_cmd (data);
}

int floppy_calibrate (uint32_t drive)
{
	uint32_t st0, cyl;

	if (drive >= 4)
		return -2;

	motor (true);

	for (int i = 0; i < 10; ++i) {
		floppy_send_cmd (FDC_CMD_CALIBRATE);
		floppy_send_cmd (drive);
		wait_irq ();
		floppy_check_int (&st0, &cyl);
		if (!cyl) {
			motor (false);
			return 0;
		}
	}

	motor (false);
	return -1;
}

void disable_controller ()
{
	write_dor (0);
}

void enable_controller ()
{
	write_dor (FLPYDSK_DOR_MASK_RESET | FLPYDSK_DOR_MASK_DMA);
}

int floppy_seek (uint32_t cyl, uint32_t head)
{
	uint32_t st0, cyl0;

	if (_CurrentDrive >= 4)
		return -1;

	for (int i = 0; i < 10; ++i) {
		floppy_send_cmd (FDC_CMD_SEEK);
		floppy_send_cmd ((head) << 2 | _CurrentDrive);
		floppy_send_cmd (cyl);

		wait_irq ();
		floppy_check_int (&st0, &cyl0);

		if (cyl0 == cyl) {
			return 0;
		}
	}

	return -1;
}

void floppy_reset ()
{
	uint32_t st0, cyl;

	disable_controller ();
	enable_controller ();
	wait_irq ();

	for (int i = 0; i < 4; ++i) {
		floppy_check_int (&st0, &cyl);
	}

	write_ccr (0);
	floppy_drive_data (3, 16, 240, true);

	floppy_calibrate (_CurrentDrive);
}

void floppy_read_sector_imp (uint8_t head, uint8_t track, uint8_t sector)
{
	uint32_t st0, cyl;

	read_dma ();

	floppy_send_cmd (FDC_CMD_READ_SECT | FDC_CMD_EXT_MULTITRACK | FDC_CMD_EXT_SKIP | FDC_CMD_EXT_DENSITY);
	floppy_send_cmd (head << 2 | _CurrentDrive);
	floppy_send_cmd (track);
	floppy_send_cmd (head);
	floppy_send_cmd (sector);
	floppy_send_cmd (FLPYDSK_SECTOR_DTL_512);
	floppy_send_cmd (((sector + 1) >= 18) ? 18 : sector + 1);
	floppy_send_cmd (FLPYDSK_GAP3_LENGTH_3_5);
	floppy_send_cmd (0xFF);

	wait_irq ();

	for (int j = 0; j < 7; ++j) {
		floppy_read_data ();
	}

	floppy_check_int (&st0, &cyl);
}

void floppy_write_sector_imp (uint8_t head, uint8_t track, uint8_t sector)
{
	uint32_t st0, cyl;

	write_dma ();

	floppy_send_cmd (FDC_CMD_WRITE_SECT | FDC_CMD_EXT_MULTITRACK | FDC_CMD_EXT_DENSITY | FDC_CMD_EXT_SKIP);
	floppy_send_cmd (head << 2 | _CurrentDrive);
	floppy_send_cmd (track);
	floppy_send_cmd (head);
	floppy_send_cmd (sector);
	floppy_send_cmd (FLPYDSK_SECTOR_DTL_512);
	floppy_send_cmd (((sector + 1) >= 18) ? 18 : sector + 1);
	floppy_send_cmd (FLPYDSK_GAP3_LENGTH_3_5);
	floppy_send_cmd (0xFF);

	wait_irq ();

	for (int j = 0; j < 7; ++j) {
		floppy_read_data ();
	}

	floppy_check_int (&st0, &cyl);
}

void lba2chs (int lba, int *head, int *track, int *sector)
{
	*head = (lba % (18 * 2)) / (18);
	*track = lba / (18 * 2);
	*sector = lba % 18 + 1;
}

void floppy_write_sector (int sectorLBA, uint8_t* data)
{
	if (_CurrentDrive >= 4)
		return;

	int head = 0, track = 0, sector = 1;
	lba2chs (sectorLBA, &head, &track, &sector);

	motor (true);
	if (floppy_seek (track, head) != 0)
		return;

	uint8_t* ptr;
	ptr = (uint8_t*) 0x1000;
	memcpy (ptr, data, 512);

	floppy_write_sector_imp (head, track, sector);
	motor (false);
}

uint8_t* floppy_read_sector (int sectorLBA)
{
	if (_CurrentDrive >= 4)
		return 0;

	int head = 0, track = 0, sector = 1;
	lba2chs (sectorLBA, &head, &track, &sector);

	motor (true);
	if (floppy_seek (track, head) != 0)
		return 0;

	floppy_read_sector_imp (head, track, sector);
	motor (false);

	return (uint8_t*) 0x1000;
}

void floppy_getready ()
{
	init_dma ();
	floppy_reset ();
	floppy_drive_data (13, 1, 0xf, true);
}

void setupFloppy ()
{
	installHardwareInterruptHandler (legacyIRQRemaps[6], floppy_handler);
	installHardwareInterruptHandler (6, floppy_handler);

	logk ("about to floppy_getready()\n");
	asm("sti");
	floppyirq = false;
	floppy_getready ();
}

void sector_disk_write (uint8_t* buf, uint32_t sector_number)
{
	floppy_getready ();
	floppy_write_sector (sector_number, buf);
}

void sector_disk_read (uint8_t* buff, uint32_t sector_number)
{
	floppy_getready ();
	memcpy (buff, floppy_read_sector (sector_number), 512);
}

void byte_disk_read (uint8_t* buff, uint32_t sector_number, uint32_t offset, uint32_t nobytes)
{
	floppy_getready ();
	uint8_t *sc = 0;
	sc = floppy_read_sector (sector_number);
	sc += offset;
	for (int x = 0; x < (int) nobytes; ++x) {
		*buff++ = *sc++;
	}
}

bool detectFloppy (struct Device* self)
{
	logk("DETECTING FLOPPIES!\n");
	uint8_t c;
	outb (0x70, 0x10);
	c = inb (0x71);

	uint8_t a = c >> 4;		// get the high nibble
	uint8_t b = c & 0xF;		 // get the low nibble by ANDing out the high nibble

	char *drive_type[6] = { "no floppy drive", "360kb 5.25in floppy drive", "1.2mb 5.25in floppy drive", "720kb 3.5in", "1.44mb 3.5in", "2.88mb 3.5in" };
	logk ("Floppy drive A is a:\n");
	logk (drive_type[a]);
	logk ("\nFloppy drive B is a:\n");
	logk (drive_type[b]);
	logk ("\n");

	if (a) {
		Device* newdev = malloc(sizeof(Device));
		initDeviceObject(newdev);

		Disks[0].type = DiskType_Floppy;
		Disks[0].device = newdev;
		Disks[0].letter = 'A';
		Disks[0].number = 0;
		Disks[0].partitionOffset = 0;
		Disks[0].partitionSize = 0;

		newdev->onDiskDriver = false;
		newdev->driverLocation = 0;
		newdev->internalDataSize = 0;
		newdev->openPointer = openFloppy;
		newdev->readPointer = readFloppy;
		newdev->writePointer = writeFloppy;
		strcpy(newdev->humanName, "FLOPPY DRIVE");
		addChild(self, newdev);
		openDevice(newdev, 0, 0, 0);
	}

	if (b) {
		Device* newdev = malloc(sizeof(Device));
		initDeviceObject(newdev);

		Disks[a == 0 ? 0 : 1].type = DiskType_Floppy;
		Disks[a == 0 ? 0 : 1].device = newdev;
		Disks[a == 0 ? 0 : 1].letter = 'A' + a == 0 ? 0 : 1;
		Disks[a == 0 ? 0 : 1].number = 0;
		Disks[a == 0 ? 0 : 1].partitionOffset = 0;
		Disks[a == 0 ? 0 : 1].partitionSize = 0;

		newdev->onDiskDriver = false;
		newdev->driverLocation = 0;
		newdev->openPointer = openFloppy;
		newdev->readPointer = readFloppy;
		newdev->writePointer = writeFloppy;
		newdev->internalDataSize = 1;
		strcpy(newdev->humanName, "FLOPPY DRIVE");
		addChild(self, newdev);
		openDevice(newdev, 0, 0, 0);
	}

	return a | b;
}

void openFDC(struct Device* self, int a, int b, void* c)
{
	logk("OPENING FDC!\n");
	if (detectFloppy(self)) {
		setupFloppy();
	}
}

void openFloppy(struct Device* self, int a, int b, void* c)
{

}

void readFloppy(struct Device* self, int sector, int sectors, void* b)
{
	_CurrentDrive = self->internalDataSize;
	logk("current drive = 0x%X\n\r\n", _CurrentDrive);
	uint8_t* buffer = (uint8_t*) b;
	for (int i = 0; i < sectors; ++i) {
		logk("About to read floppy sector %d of %d (LBA 0x%X)\n", i + 1, sectors, sector + i);
		sector_disk_read(buffer, sector + i);
		buffer += 512;
	}
	logk("Floppy has been read.\n");
}

void writeFloppy(struct Device* self, int sector, int sectors, void* b)
{
	_CurrentDrive = self->internalDataSize;
	logk("current drive = 0x%X\n\r\n", _CurrentDrive);
	uint8_t* buffer = (uint8_t*) b;
	for (int i = 0; i < sectors; ++i) {
		logk("About to write floppy sector %d of %d (LBA 0x%X)\n", i + 1, sectors, sector + i);
		sector_disk_write(buffer, sector + i);
		buffer += 512;
	}
	logk("Floppy has been written?\n");
}