//
//  ctype.h
//  Banana libc
//
//  Created by Alex Boxall on 11/7/18.
//  Copyright © 2018 Alex Boxall. All rights reserved.
//

#ifndef ctype_h
#define ctype_h

#if defined(__cplusplus)
#define restrict			//fixes some 'fun' errors
extern "C" {
#endif
    
    int isalnum(int c);
    int isalpha(int c);
    int isblank(int c);
    int iscntrl(int c);
    int isdigit(int c);
    int isgraph(int c);
    int islower(int c);
    int isprint(int c);
    int isspace(int c);
    int isupper(int c);
    int isxdigit(int c);
    int ispunct(int c);
    
    int tolower(int c);
    int toupper(int c);

#if defined(__cplusplus)
}
#endif

#endif /* ctype_h */
