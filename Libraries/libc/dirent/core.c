#ifndef _libk

#include "dirent.h"
#include <stdlib.h>
#include "syscall.h"

int closedir (DIR* dir)
{
	return __i_system_call_closedir ((unsigned long long*) dir);
}

DIR* opendir (const char* folder)
{
	unsigned long long* ptr = malloc (sizeof (unsigned long long));
	__i_system_call_opendir (ptr, folder);
	return ptr;
}

struct dirent* readdir (DIR* dir)
{
	struct dirent* output = malloc (sizeof(struct dirent));
	int readdirout = __i_system_call_readdir (dir, (void*) output);
	return readdirout ? NULL : output;
}

#endif