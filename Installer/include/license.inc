licenseAgreementPage db 0 
page1donebefore db 0
page2donebefore db 0
page3donebefore db 0
page4donebefore db 0
page5donebefore db 0

LicenseAgreement:
	call ResetScreen
	
	cmp [licenseAgreementPage - D_OFFSET], byte 0
	je .page0
	cmp [licenseAgreementPage - D_OFFSET], byte 1
	je .page1
	cmp [licenseAgreementPage - D_OFFSET], byte 2
	je .page2
	cmp [licenseAgreementPage - D_OFFSET], byte 3
	je .page3
	cmp [licenseAgreementPage - D_OFFSET], byte 4
	je .page4
.page0:
	mov si, license1 - D_OFFSET
	cmp [page1donebefore - D_OFFSET], byte 1
	jne .p0
	call puts
	jmp .AwaitKeyPress
	.p0:
	mov [page1donebefore - D_OFFSET], byte 1
	call putsslow
	jmp .AwaitKeyPress
.page1:
	mov si, license2 - D_OFFSET
	cmp [page2donebefore - D_OFFSET], byte 1
	jne .p1
	call puts
	jmp .AwaitKeyPress
	.p1:
	mov [page2donebefore - D_OFFSET], byte 1
	call putsslow
	jmp .AwaitKeyPress
.page2:
	mov si, license3 - D_OFFSET
	cmp [page3donebefore - D_OFFSET], byte 1
	jne .p2
	call puts
	jmp .AwaitKeyPress
	.p2:
	mov [page3donebefore - D_OFFSET], byte 1
	call putsslow
	jmp .AwaitKeyPress
.page3:
	mov si, license4 - D_OFFSET
	cmp [page4donebefore - D_OFFSET], byte 1
	jne .p3
	call puts
	jmp .AwaitKeyPress
	.p3:
	mov [page4donebefore - D_OFFSET], byte 1
	call putsslow
	jmp .AwaitKeyPress
.page4:
	mov si, license5 - D_OFFSET
	cmp [page5donebefore - D_OFFSET], byte 1
	jne .p4
	call puts
	jmp .AwaitKeyPress
	.p4:
	mov [page5donebefore - D_OFFSET], byte 1
	call putsslow
	jmp .AwaitKeyPress

.AwaitKeyPress:
	xor ax, ax
	int 0x16

	cmp ah, 0x1C
	je AGREE
	cmp ah, 0x51
	je .PDOWN
	cmp ah, 0x49
	je .PUP
	cmp ah, 0x50
	je .PDOWN
	cmp ah, 0x48
	je .PUP
	cmp ah, 0x01
	je shutdown
	jmp .AwaitKeyPress

.PDOWN:
	cmp [licenseAgreementPage - D_OFFSET], byte 4
	je LicenseAgreement
	inc byte [licenseAgreementPage - D_OFFSET]
	jmp LicenseAgreement
.PUP:
	cmp [licenseAgreementPage - D_OFFSET], byte 0
	je LicenseAgreement
	dec byte [licenseAgreementPage - D_OFFSET]
	jmp LicenseAgreement

AGREE:
	cmp [licenseAgreementPage - D_OFFSET], byte 4
	jne LicenseAgreement
	ret