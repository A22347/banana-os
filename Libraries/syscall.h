#include <stdint.h>

#ifndef __SYSCALL_HPP__
#define __SYSCALL_HPP__

#ifdef __cplusplus
extern "C" {
#endif

#define HAVE_STDBOOL_H
#include <stdint.h>
#include <stdbool.h>
#include <stdarg.h>
#ifdef _MSC_EXTENSIONS
typedef uint64_t size_t;
#else
#include <stddef.h>
#endif

void realyield ();
int printf (const char* format, ...);
#define yield(...) printf("YIELDING: %s:%d, %s", __FILE__, __LINE__, __func__); realyield()

	typedef enum driverMode
	{
		D_READ,
		D_WRITE,
		D_IOCTL

	} drivermode;

	typedef struct regs_t
	{
		uint32_t reg_a;
		uint32_t reg_b;
		uint32_t reg_c;
		uint32_t reg_d;
		uint32_t reg_e;
		uint32_t reg_f;
		uint32_t reg_g;
		uint32_t reg_h;

	} regs_t;

#include <../System/core/syscalldef.h>

int SystemCall(uint16_t callno, uint32_t ebx, uint32_t ecx, void* edx);

#ifdef __cplusplus
}
#endif
#endif