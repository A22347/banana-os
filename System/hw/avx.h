#pragma once

#ifdef __cplusplus
extern "C" {
#endif

	int avxAvaliable ();
	void avxOpen ();
	void avxRead (void* ptr);
	void avxWrite (void* ptr);

#ifdef __cplusplus
}
#endif