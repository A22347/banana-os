//
//  main.c
//  iso9660
//
//  Created by Alex Boxall on 23/8/18.
//  Copyright © 2018 Alex Boxall. All rights reserved.
//

#include <fs/iso9660/iso9660.h>
#include <hal/diskio.h>
#include <libk/string.h>
#include <core/memory.h>			//malloc
#include <debug/log.h>

uint8_t* __memmem (uint8_t* big, int bigLen, uint8_t* small, int smallLen) {
    //may have an 'off by one' error, but this won't matter, as all
    //names end in ';1'
    for (int i = 0; i < bigLen - smallLen; ++i) {
        bool yes = true;
        for (int j = 0; j < smallLen; ++j) {
            if (small[j] != big[i + j]) {
                yes = false;
                break;
            }
        }
        if (yes) {
            return big + i;
        }
    }
    return 0;
}

void readSectorFromCDROM (uint32_t sector, uint8_t* data, char driveletter) {
	ReadSector (driveletter, sector, 1, data);
}

bool readRoot (uint32_t* lbaOut, uint32_t* lenOut, char driveletter) {
    uint8_t sector[2048];
    readSectorFromCDROM(16, sector, driveletter);
    uint8_t root[34];
    memcpy(root, sector + 156, 34);
    
    *lbaOut = root[2] | (root[3] << 8) | (root[4] << 16) | (root[5] << 24);
    *lenOut = root[10] | (root[11] << 8) | (root[12] << 16) | (root[13] << 24);
    
    return true;
}

bool readRecursively (char* filename, uint32_t startSec, uint32_t startLen, \
                      uint32_t* lbaOut, uint32_t* lenOut, char driveletter) {
    
    char firstPart[256];
    memset(firstPart, 0, 256);
    bool dir = false;
    for (int i = 0; filename[i]; ++i) {
        if (filename[i] == '/') {
            filename += i + 1;
            dir = true;
            break;
        }
        firstPart[i] = filename[i];
    }
    
    uint32_t newLba, newLen;
    uint8_t* data = malloc(startLen);
    
    for (int i = 0; (uint32_t) i < (startLen + 2047) / 2048; ++i) {
        readSectorFromCDROM(startSec + i, data + i * 2048, driveletter);
    }

    uint8_t* o = __memmem(data, startLen, (uint8_t*) firstPart, strlen(firstPart));
    if (o == 0) {
        return false;
    }
    o -= 33;                    //33 = filename start, 2 = lba start, 31 = difference
    
    newLba = o[2] | (o[3] << 8) | (o[4] << 16) | (o[5] << 24);
    newLen = o[10] | (o[11] << 8) | (o[12] << 16) | (o[13] << 24);
    
    if (dir) {
        return readRecursively(filename, newLba, newLen, lbaOut, lenOut, driveletter);
    } else {
        *lbaOut = newLba;      //data
        *lenOut = newLen;      //data
        return true;
    }
    
    return false;
}

bool getFileData (char* filename, uint32_t* lbaOut, uint32_t* lenOut, char driveletter) {
    uint32_t lba = 0, len = 0;
    *lbaOut = -1;
    *lenOut = -1;
    readRoot(&lba, &len, driveletter);
    return readRecursively(filename, lba, len, lbaOut, lenOut, driveletter);
}

bool isoMount (char driveLetter)
{
	logk ("Going to attempt to mount drive %c as an ISO 9660 disk\n");

	//check that the drive is formatted with ISO 9660.
	char bf[2048];
	readSectorFromCDROM (16, (uint8_t*) bf, driveLetter);

	logk ("Checking for signature...\n");

	if (bf[1] != 'C') return false;
	if (bf[2] != 'D') return false;
	if (bf[3] != '0') return false;
	if (bf[4] != '0') return false;
	if (bf[5] != '1') return false;

	logk ("YES!\n");

	return true;
}

int isoOpen (char driveletter, isoFile_t* file, char* filename)
{
	uint32_t lbaO, lenO;
	bool res = getFileData (filename, &lbaO, &lenO, driveletter);
	if (res == false) {
		file->error = true;
		return -1;
	}
	file->error = false;
	file->seekMark = 0;
	file->fileStartLba = lbaO;
	file->fileLength = lenO;
	file->driveletter = driveletter;
	return 0;
}

int isoRead (isoFile_t* file, char* buffer, uint32_t length)
{
	//I've used Excel to 'run through' how it works, and it seems to
	//work fine. We'll just have to wait and see how it goes.

	int64_t ulength = (int64_t) length;
	uint64_t bufferInc = 0;
	uint64_t intendedSeekMark = file->seekMark + length;

	if (file->error) {
		return 1;
	}

	uint64_t startPoint = file->fileStartLba * 2048 + file->seekMark;
	if (startPoint + length > file->fileLength) {
		return 2;
	}

	//work out start
	uint8_t sectorBuffer[2048];
	readSectorFromCDROM (startPoint / 2048, sectorBuffer, file->driveletter);
	memcpy (buffer, sectorBuffer + file->seekMark % 2048, 2048 - file->seekMark % 2048);		//copy only part of the data
	bufferInc = 2048 - file->seekMark % 2048;

	//round to next sector
	ulength -= 2048 - file->seekMark % 2048;
	file->seekMark /= 2048;
	file->seekMark++;
	file->seekMark *= 2048;

	//do the whole sectors
	for (int64_t i = 0; i < ulength / 2048; ++i) {
		startPoint = file->fileStartLba * 2048 + file->seekMark;
		readSectorFromCDROM (startPoint / 2048, (uint8_t*) buffer + bufferInc, file->driveletter);
		file->seekMark += 2048;
		bufferInc += 2048;
	}

	//work out end
	startPoint = file->fileStartLba * 2048 + file->seekMark;
	readSectorFromCDROM (startPoint / 2048, sectorBuffer, file->driveletter);
	memcpy (buffer + bufferInc, sectorBuffer, ulength);		//copy only part of the data

	file->seekMark += ulength;

	//we probably made a mistake with seekMark, so set it correctly.
	file->seekMark += intendedSeekMark;
	return 0;
}
