//
//  filepos.c
//  Banana libc
//
//  Created by Alex Boxall on 24/9/18.
//  Copyright © 2018 Alex Boxall. All rights reserved.
//

#include "stdio.h"
#include "errno.h"
#include "syscall.h"

long int ftell(FILE *stream) {
	long int pos = 0;
    int ret = __i_system_call_tell ((unsigned long long*) stream, (unsigned long long*) &pos);
    if (ret) {
        errno = ETELL;
        return -1;
    }
    return pos;
}

int fgetpos(FILE * restrict stream, fpos_t * restrict pos) {
    *pos = ftell(stream);
    if (ETELL) {
        return ETELL;
    }
    return 0;       //on success
}

int fseek(FILE *stream, long int offset, int whence) {
    int ret = __i_system_call_seek ((unsigned long long*) stream, offset, whence);
    return ret;       //on success
}

int fsetpos(FILE *stream, const fpos_t *pos) {
    int ret = fseek(stream, *pos, SEEK_SET);
    if (ret) {
        errno = ESEEK;
    }
    return ret;
}

void rewind(FILE *stream) {
    (void)fseek(stream, 0L, SEEK_SET);
}
