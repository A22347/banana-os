// font found in the original source, but based off https://github.com/gnustep/libobjc/blob/master/config/sparc/generic/tconfig.h

#define STRUCTURE_SIZE_BOUNDARY 8
#define PCC_BITFIELD_TYPE_MATTERS 1
#define BITS_PER_UNIT 8
#define BITS_PER_WORD 32