//
//  Character.hpp
//  GUI
//
//  Created by Alex Boxall on 19/6/19.
//  Copyright © 2019 Alex Boxall. All rights reserved.
//

#ifndef Character_hpp
#define Character_hpp

#include "Control.hpp"
#include "Font.hpp"

class Character : public Control {
private:
protected:
    uint32_t unicodeChar;
    uint32_t fgCol;
    
    Font* font;
    
public:
    uint32_t underlineColour = 0;
    
    int getRenderedWidth();
    int getRenderedHeight();
    
    Font* getFont();
    void setFont(Font* font);

    virtual bool canHoldFocus();     //whether the tab key can be used to select this control

    Character (Control* parent, Font* font, uint32_t unicode, int x = -1, int y = -1, uint32_t fgCol = 0);
    
    Character* copy();

    virtual void internalDraw();
    virtual const char* debugName();
};

#endif /* Character_hpp */
