#include <hal/display.h>
#include <core/string.h>
#include <hw/vbe.h>
#include <hw/vga.h>
#include <hw/bga.h>
#include <hal/interrupts.h>			//yieldFromKernel

displayData_t displays[256];
int nextDisplay = 0;
unsigned short TOTAL_HEIGHT = 756;	// VISIBLE SCREEN HEIGHT
unsigned short ONE_LINE = 1024;		// TOTAL PER SCANLINE
unsigned short TOTAL_WIDTH = 1024;	// VISIBLE SCREEN WIDTH
unsigned int BITS_PIXEL = 8;
uint8_t BYTES_PER_PIXEL = 3;

InternalObjectID builtinDriverGlobalObjectIDPointer_VGA;
InternalObjectID builtinDriverGlobalObjectIDPointer_VBE;
InternalObjectID builtinDriverGlobalObjectIDPointer_BGA;

//split into hal/display.h and hw/svga.h

void displayCopyBufferTo (uint32_t* buffer, uint32_t start, uint32_t size, uint8_t displayNo)
{
	//TODO: call driverWrite(displays[displayNo].driverID, location, colour), or something similar
	vbeCopyBufferTo (buffer, start, size, displayNo);
}

void displayCopyBufferFrom (uint32_t* buffer, uint32_t start, uint32_t size, uint8_t displayNo)
{
	//TODO: call driverRead(displays[displayNo].driverID, location, colour), or something similar
	vbeCopyBufferFrom (buffer, start, size, displayNo);
}

void displaySetPixel (uint32_t location, uint32_t colour, uint8_t displayNo)
{
	//TODO: call driverWrite(displays[displayNo].driverID, location, colour), or something similar
	return vbeSetPixel (location, colour, displayNo);
}

uint32_t displayGetPixel (uint32_t location, uint8_t displayNo)
{
	//TODO: call driverRead(displays[displayNo].driverID, location), or something similar
	return vbeGetPixel (location, displayNo);
}

void displayInit ()
{
	//displays should be added to the list via pci.c
	//scan through some sort of list
	//set up the display structures
	//use the display driver id to call init
	//write to the display structures
	//etc.
	//...

	//no display drivers?
	if (bgaAvaliable ()) {
		bgaInit (nextDisplay);

		displays[nextDisplay].driverID = builtinDriverGlobalObjectIDPointer_BGA;
		displays[nextDisplay].bytesPerPixel = BYTES_PER_PIXEL;
		displays[nextDisplay].totalHeight = TOTAL_HEIGHT;
		displays[nextDisplay].totalWidth = ONE_LINE;			//ironic naming of 'ONE_LINE'
		displays[nextDisplay].visibleHeight = TOTAL_HEIGHT;
		displays[nextDisplay].visibleWidth = TOTAL_WIDTH;		//ironic naming of 'ONE_LINE'
		strcpy (displays[nextDisplay].displayName, "BGA DISPLAY");
		++nextDisplay;

	} else if (vbeAvaliable ()) {
		vbeInit (nextDisplay);

		displays[nextDisplay].driverID = builtinDriverGlobalObjectIDPointer_VBE;
		displays[nextDisplay].bytesPerPixel = BYTES_PER_PIXEL;
		displays[nextDisplay].totalHeight = TOTAL_HEIGHT;
		displays[nextDisplay].totalWidth = ONE_LINE;			//ironic naming of 'ONE_LINE'
		displays[nextDisplay].visibleHeight = TOTAL_HEIGHT;
		displays[nextDisplay].visibleWidth = TOTAL_WIDTH;		//ironic naming of 'ONE_LINE'
		strcpy (displays[nextDisplay].displayName, "VBE DISPLAY");
		++nextDisplay;

	} else if (vgaAvaliable ()) {
		vgaInit (nextDisplay);

		displays[nextDisplay].driverID = builtinDriverGlobalObjectIDPointer_VGA;
		displays[nextDisplay].bytesPerPixel = BYTES_PER_PIXEL;
		displays[nextDisplay].totalHeight = TOTAL_HEIGHT;
		displays[nextDisplay].totalWidth = ONE_LINE;			//ironic naming of 'ONE_LINE'
		displays[nextDisplay].visibleHeight = TOTAL_HEIGHT;
		displays[nextDisplay].visibleWidth = TOTAL_WIDTH;		//ironic naming of 'ONE_LINE'
		strcpy (displays[nextDisplay].displayName, "VGA DISPLAY");
		++nextDisplay;
	}

	logk ("Display 0 %dx%d %d\n", TOTAL_WIDTH, TOTAL_HEIGHT, BYTES_PER_PIXEL);
}