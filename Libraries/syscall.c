#include "syscall.h"
#include <string.h>

int SystemCall (uint16_t callno, uint32_t ebx, uint32_t ecx, void* edx)
{
	int res;
	/*__asm__ volatile(
	"int $96":
	"=a" (res),
	"+b" (ebx),
	  "+c" (ecx),
	  "+d" (edx)
	: "a"  (callno)
	: "memory", "cc");*/

	asm volatile (
		"int $96"  :				//assembly
		"=a" (res) :				//output
		"a" (callno), 				//input
		"b" (ebx), 					//input
		"c" (ecx), 					//input
		"d" (edx) :					//input
		"memory", "cc");			//clobbers	

	return res;

	//int* ress = (int*) 0x13E8;
	//return *ress;
}

/*
#include <stdint.h>

struct source_location
{
	const char *file;
	uint32_t line;
	uint32_t column;
};

struct type_descriptor
{
	uint16_t kind;
	uint16_t info;
	char name[];
};

struct type_mismatch_info
{
	struct source_location location;
	struct type_descriptor *type;
	uintptr_t alignment;
	uint8_t type_check_kind;
};

struct out_of_bounds_info
{
	struct source_location location;
	struct type_descriptor left_type;
	struct type_descriptor right_type;
};

#define is_aligned(value, alignment) !(value & (alignment - 1))

const char *mgg[] = {
	"NULL_POINTER_EXCEPTION",
	"UNALIGNED_MEMORY_ACCESS",
	"INSUFFICIENT_SIZE"
};

const char *Type_Check_Kinds[] = {
	"load of",
	"store to",
	"reference binding to",
	"member access within",
	"member call on",
	"constructor call on",
	"downcast of",
	"downcast of",
	"upcast of",
	"cast to virtual base of",
};

static void log_location (struct source_location *location)
{
	printf ("\tfile: %s\n\r\n\tline: %d\n\r\n\tcolumn: %d\n\r\n",
				 location->file, location->line, location->column);
}


void __ubsan_handle_type_mismatch (struct type_mismatch_info *type_mismatch,
								   uintptr_t pointer)
{
	struct source_location *location = &type_mismatch->location;
	int type = 0;

	if (pointer == 0) {
		printf ("Null pointer access\n\r\n");
		type = 0;

	} else if (type_mismatch->alignment != 0 &&
			   is_aligned (pointer, type_mismatch->alignment)) {
		printf ("Unaligned memory access\n\r\n");
		type = 1;

	} else {
		printf ("Insufficient size\n\r\n");
		printf ("%s address %d with insufficient space for object of type %s\n\r\n",
					 Type_Check_Kinds[type_mismatch->type_check_kind], (void *) pointer,
					 type_mismatch->type->name);
		type = 2;
	}
	log_location (location);

	SystemCall (SC_Panic, 0, 0, mgg[type]);
	//panicWithMessage (mgg[type]);
}

void __ubsan_handle_add_overflow (void* data_raw,
								  void* lhs_raw,
								  void* rhs_raw)
{
	printf ("ADD OVERFLOW...\n\r\n");

}

void __ubsan_handle_sub_overflow (void* data_raw,
								  void* lhs_raw,
								  void* rhs_raw)
{
	printf ("SUBTRACT OVERFLOW...\n\r\n");

}

void __ubsan_handle_mul_overflow (void* data_raw,
								  void* lhs_raw,
								  void* rhs_raw)
{
	printf ("MULTIPLY OVERFLOW...\n\r\n");

}

void __ubsan_handle_divrem_overflow (void* data_raw,
									 void* lhs_raw,
									 void* rhs_raw)
{
	printf ("REVISION REMAINDER OVERFLOW...\n\r\n");

}

void __ubsan_handle_shift_out_of_bounds (void* data_raw,
										 void* lhs_raw,
										 void* rhs_raw)
{
	printf ("SHIFT OUT OF BOUNDS...\n\r\n");

}

void __ubsan_handle_out_of_bounds (void* data_raw,
								   void* index_raw)
{
	printf ("OUT OF BOUNDS...\n\r\n");

}

void __ubsan_handle_negate_overflow (void* data_raw,
									 void* old_value_raw)
{
	printf ("NEGATE OVERFLOW...\n\r\n");

}

static void ubsan_abort (const struct ubsan_source_location* location,
						 const char* violation)
{}

void __ubsan_handle_builtin_unreachable (void* data_raw)
{
	printf ("BULTIN UNREACHABLE\n\r\n");
}

void __ubsan_handle_missing_return (void* data_raw)
{
	printf ("MISSING RETURN\n\r\n");
}

void __ubsan_handle_vla_bound_not_positive (void* data_raw,
											void* bound_raw)
{
	printf ("BOUND NOT POSITIVE\n\r\n");
}

void __ubsan_handle_float_cast_overflow (void* data_raw,
										 void* from_raw)
{
	printf ("CAST OVERFLOW\n\r\n");
}

void __ubsan_handle_load_invalid_value (void* data_raw,
										void* value_raw)
{
	printf ("INVALID VALUE LOAD\n\r\n");
}

void __ubsan_handle_function_type_mismatch (void* data_raw,
											void* value_raw)
{
	printf ("TYPE MISMATCH");
}

void __ubsan_handle_nonnull_return (void* data_raw)
{
	printf ("NONULL RETURN");
}

void __ubsan_handle_nonnull_arg (void* data_raw,
								 intptr_t index_raw)
{
	printf ("NONNULL ARG");
}

void __ubsan_handle_cfi_bad_icall (void* data_raw,
								   void* value_raw)
{
	printf ("CFI BAD ICALL");
}*/


int main (int argc, char** argv);
int _Nnotmain ()
{
	SystemCall (SC_LauchTerminalSession, 0, 0, 0);
	int noargs = SystemCall (SC_NumberOfArguments, 0, 0, 0);

	char* args[256];
	memset (args, 0, 256 * sizeof (char*));

	for (int i = 0; i < noargs; ++i) {
		args[i] = (char*) ((size_t) SystemCall (SC_GetArgument, i, 0, 0));
	}

	int result = main (noargs, args);

	SystemCall (SC_ExitTerminalSession, result, 0, 0);
	return result;
}