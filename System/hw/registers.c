#include <hw/registers.h>

uint64_t rdmsr (uint32_t msr_id)
{
	uint64_t msr_value;
	asm volatile ("rdmsr" : "=A" (msr_value) : "c" (msr_id));
	return msr_value;
}

#if PLATFORM_ID == 64
uint64_t getRAX () { uint64_t v; asm volatile ("\t movq %%rax,%0" : "=r"(v)); return v; }
uint64_t getRBX () { uint64_t v; asm volatile ("\t movq %%rbx,%0" : "=r"(v)); return v; }
uint64_t getRCX () { uint64_t v; asm volatile ("\t movq %%rcx,%0" : "=r"(v));  return v; }
uint64_t getRDX ()
{
	uint64_t v; asm volatile ("\t movq %%rdx,%0" : "=r"(v)); return v;
}
uint64_t getRSP ()
{
	uint64_t v; asm volatile ("\t movq %%rsp,%0" : "=r"(v)); return v;
}
uint64_t getRBP ()
{
	uint64_t v; asm volatile ("\t movq %%rbp,%0" : "=r"(v)); return v;
}
uint64_t getRSI ()
{
	uint64_t v; asm volatile ("\t movq %%rsi,%0" : "=r"(v)); return v;
}
uint64_t getRDI ()
{
	uint64_t v; asm volatile ("\t movq %%rdi,%0" : "=r"(v)); return v;
}
uint64_t getR8 ()
{
	uint64_t v; asm volatile ("\t movq %%r8,%0" : "=r"(v)); return v;
}
uint64_t getR9 ()
{
	uint64_t v; asm volatile ("\t movq %%r9,%0" : "=r"(v)); return v;
}
uint64_t getR10 ()
{
	uint64_t v; asm volatile ("\t movq %%r10,%0" : "=r"(v)); return v;
}
uint64_t getR11 ()
{
	uint64_t v; asm volatile ("\t movq %%r11,%0" : "=r"(v)); return v;
}
uint64_t getR12 ()
{
	uint64_t v; asm volatile ("\t movq %%r12,%0" : "=r"(v)); return v;
}
uint64_t getR13 ()
{
	uint64_t v; asm volatile ("\t movq %%r13,%0" : "=r"(v)); return v;
}
uint64_t getR14 ()
{
	uint64_t v; asm volatile ("\t movq %%r14,%0" : "=r"(v)); return v;
}
uint64_t getR15 ()
{
	uint64_t v; asm volatile ("\t movq %%r15,%0" : "=r"(v)); return v;
}
uint64_t getRIP ()
{
	return 0; uint64_t v; asm volatile ("\t movq %%rax,%0" : "=r"(v)); return v;
}
uint64_t getRFLAGS ()
{
	return 0; uint64_t v; asm volatile ("\t movq %%rax,%0" : "=r"(v)); return v;
}
#else
uint32_t getEAX () { uint32_t v; asm volatile ("\t movl %%eax,%0" : "=r"(v)); return v; }
uint32_t getEBX () { uint32_t v; asm volatile ("\t movl %%ebx,%0" : "=r"(v)); return v; }
uint32_t getECX () { uint32_t v; asm volatile ("\t movl %%ecx,%0" : "=r"(v));  return v; }
uint32_t getEDX ()
{
	uint32_t v; asm volatile ("\t movl %%edx,%0" : "=r"(v)); return v;
}
uint32_t getESP ()
{
	uint32_t v; asm volatile ("\t movl %%esp,%0" : "=r"(v)); return v;
}
uint32_t getEBP ()
{
	uint32_t v; asm volatile ("\t movl %%ebp,%0" : "=r"(v)); return v;
}
uint32_t getESI ()
{
	uint32_t v; asm volatile ("\t movl %%esi,%0" : "=r"(v)); return v;
}
uint32_t getEDI ()
{
	uint32_t v; asm volatile ("\t movl %%edi,%0" : "=r"(v)); return v;
}
uint32_t getEIP ()
{
	return 0; uint32_t v; asm volatile ("\t movl %%eax,%0" : "=r"(v)); return v;
}
uint32_t getEFLAGS ()
{
	return 0; uint32_t v; asm volatile ("\t movl %%eax,%0" : "=r"(v)); return v;
}
#endif



