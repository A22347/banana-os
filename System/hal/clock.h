#pragma once

#include <core/string.h>
#include <core/kernel.h>
#include <hw/rtc.h>

uint64_t datetimeToSeconds (datetime_t d);
datetime_t secondsToDatetime (uint64_t data);

void loadTimezones ();

uint64_t getClockTimeUTC ();

uint64_t getClockTime ();
datetime_t currentTime ();