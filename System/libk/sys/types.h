//
//  types.h
//  Banana libc
//
//  Created by Alex Boxall on 7/8/18.
//  Copyright © 2018 Alex Boxall. All rights reserved.
//

#ifndef xxtypes_h
#define xxtypes_h

#if defined(__cplusplus)
extern "C" {
#endif
    
typedef unsigned long ssize_t;
    
typedef signed long long blkcnt_t;
typedef signed long long blksize_t;
typedef unsigned long long clockid_t;
typedef unsigned long long dev_t;
typedef unsigned long long fsblkcnt_t;
typedef unsigned long long fsfilcnt_t;
typedef unsigned long long gid_t;
typedef unsigned long long id_t;
typedef unsigned long long ino_t;
//key_t is a struct
typedef unsigned long long mode_t;
typedef unsigned long long nlink_t;
typedef signed long long off_t;
typedef signed long long pid_t;

//lots of pthread stuff here...

//define ssize_t
typedef unsigned long long suseconds_t;
#ifndef _TIME_T_DEFINED
#define _TIME_T_DEFINED
typedef unsigned long long clock_t;
typedef unsigned long long time_t;
#endif
typedef unsigned long long timer_t;
//trace stuff...
typedef unsigned long long uid_t;
typedef unsigned long long useconds_t;

#if defined(__cplusplus)
}
#endif

#endif /* types_h */
