#include <network/ethernet.h>
#include <libk/string.h>
#include <core/memory.h>

//to be filled in by the initialisation of our ethernet hardware driver
uint32_t ourIP;
uint8_t ourMAC[6];

uint16_t switchEndian16(uint16_t nb)
{
	return (nb >> 8) | (nb << 8);
}

uint32_t switchEndian32(uint32_t nb)
{
	return ((nb >> 24) & 0xff) |
		((nb << 8) & 0xff0000) |
		((nb >> 8) & 0xff00) |
		((nb << 24) & 0xff000000);
}

EthernetPacket generateEthernetPacketFromType(uint8_t* dmac, uint8_t* smac, uint16_t type, uint8_t* payload)
{
	EthernetPacket ep;
	memcpy(ep.dmac, dmac, 6);
	memcpy(ep.smac, smac, 6);
	ep.type = type;

	uint16_t payloadLength = 0;						//depends on type I assume
	memset(ep.payload, 0, 48);						//padding
	memcpy(ep.payload, payload, payloadLength);

	return ep;
}

EthernetPacket generateEthernetPacketFromSize(uint8_t* dmac, uint8_t* smac, uint16_t size, uint8_t* payload)
{
	EthernetPacket ep;
	memcpy(ep.dmac, dmac, 6);
	memcpy(ep.smac, smac, 6);
	ep.type = size;

	memset(ep.payload, 0, 48);				//padding
	memcpy(ep.payload, payload, size);

	if (size < 48) {
		ep.type = 48;
	}

	return ep;
}

uint8_t* getBytesFromEthernetPacket(EthernetPacket ep)
{
	//switch to big endian
	ep.type = switchEndian16(ep.type);

	//copy into byte stream
	uint8_t* stream = malloc(sizeof(EthernetPacket));
	memcpy(stream, &ep, sizeof(EthernetPacket));

	return stream;
}

EthernetPacket getEthernetPacketFromBytes(uint8_t* bytes)
{
	//copy into packet
	EthernetPacket ep;
	memcpy(&ep, bytes, sizeof(EthernetPacket));

	//switch to little endian
	ep.type = switchEndian16(ep.type);

	return ep;
}