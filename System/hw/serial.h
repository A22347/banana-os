#pragma once

#include <stdarg.h>
#include <stdbool.h>
#include <stdint.h>
#include <hw/ports.h>

extern uint16_t comPortBases[4];
extern int comBaudRates[4];

void comSetBaudRate (int portNumber, int baud);
void serialInit ();
bool serialIsTransmitEmpty (uint8_t portNo);
void serialWriteChar (uint8_t portNo, char data);
void serialWrite (uint8_t portNo, void* data, uint32_t length);

void com1_putchar (char a);