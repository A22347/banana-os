//Set up stack smashing protector

#define EARLY_STAGE
#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>
#include <fs/fat/pff.h>
#include <libk/string.h>
#include <hal/diskio.h>
#include <hal/display.h>
#include <debug/log.h>
#include <core/panic.h>

int INFO_TO_WRITE_SOMEWHERE_SOON_TIMEZONE = 0;

extern int _e_systicks;                    //system ticks, from kernel init code (ECX), from bootloader (stored in ECX)
unsigned long long _e_next = 1;            //_e_randint() seed
uintptr_t __stack_chk_guard;               //secret stack number

extern uint8_t numberOfFloppies;
extern unsigned short TOTAL_HEIGHT;
extern unsigned short ONE_LINE;
extern unsigned char BYTES_PER_PIXEL;
extern unsigned short TOTAL_WIDTH;
extern uint32_t* realVideoMemory;

extern bool readOnlyMode;
extern bool comicSansMode;
extern bool safeMode;
extern bool forceDisableAPIC;

int _e_randint() {
    _e_next = _e_next * 1103515245 + 12345;
    return (unsigned int)(_e_next / 65536) % 32768;
}

void panicWithMessage (char* msg);

void __stack_chk_fail() {
	panicWithMessage ("BUFFER_OVERFLOW");
}

struct BootloaderData {
    uint16_t bytesPerScanline; //0x500
    uint16_t y;                //0x502
    uint8_t attributes;        //0x504
    uint8_t safemode;          //0x505
    uint32_t videoMemory;      //0x506
    uint8_t floppies;          //0x50A
    uint8_t harddrives;        //0x50B
    uint8_t bpp;               //0x50C
    uint8_t console;           //0x50D
	uint16_t x;				   //0x50E
	uint8_t apicEnabled;	   //0x510
	uint8_t comicSansMode;	   //0x511
	uint8_t readonly;		   //0x512	
	uint16_t lengthOfRAMTable; //0x513	

	// RAM TABLE AT 0x600+

} __attribute__ ((packed));

extern uint16_t ramTableLength;

enum vga_color_text_mode
{
	VGA_COLOR_BLACK = 0,
	VGA_COLOR_BLUE = 1,
	VGA_COLOR_GREEN = 2,
	VGA_COLOR_CYAN = 3,
	VGA_COLOR_RED = 4,
	VGA_COLOR_MAGENTA = 5,
	VGA_COLOR_BROWN = 6,
	VGA_COLOR_LIGHT_GREY = 7,
	VGA_COLOR_DARK_GREY = 8,
	VGA_COLOR_LIGHT_BLUE = 9,
	VGA_COLOR_LIGHT_GREEN = 10,
	VGA_COLOR_LIGHT_CYAN = 11,
	VGA_COLOR_LIGHT_RED = 12,
	VGA_COLOR_LIGHT_MAGENTA = 13,
	VGA_COLOR_LIGHT_BROWN = 14,
	VGA_COLOR_WHITE = 15,
};

void writeTextChar(int x, int y, uint8_t c, int fg, int bg)
{
	uint16_t* ptr = (uint16_t*) 0xB8000;
	ptr += y * 80 + x;
	*ptr = (bg << 12) | (fg << 8) | c;
}

void writeTextString(int x, int y, char* c, int fg, int bg)
{
	int initX = x;
	for (int i = 0; c[i]; ++i) {
		if (c[i] == '\n') {
			x = initX;
			++y;
			continue;
		}
		if (c[i] == '\t') {
			writeTextChar(x, y, ' ', fg, bg);
			++x;
			while (x % 4 != 1) {
				writeTextChar(x, y, ' ', fg, bg);
				++x;
			}
			continue;
		}
		writeTextChar(x, y, c[i], fg, bg);
		++x;
	}
}

void writeTextStringWindowLock(int x, int y, char* c, int fg, int bg)
{
	int initX = x;
	for (int i = 0; c[i]; ++i) {
		if (c[i] == '\n') {
			x = initX;
			++y;
			continue;
		}
		if (c[i] == '\t') {
			writeTextChar(x, y, ' ', fg, bg);
			++x;
			while (x % 4 != 1) {
				writeTextChar(x, y, ' ', fg, bg);
				++x;
			}
			continue;
		}
		if (x > 69) {
			writeTextChar(x, y, '.', fg, bg);
			writeTextChar(x+1, y, '.', fg, bg);
			writeTextChar(x+2, y, '.', fg, bg);
			return;
		}
		writeTextChar(x, y, c[i], fg, bg);
		++x;
	}
}

void displayScreen(int stage, char* esttime, char* windowtitle)
{
	for (int y = 0; y < 25; ++y) {
		for (int x = 0; x < 80; ++x) {
			writeTextChar(x, y, ' ', VGA_COLOR_LIGHT_BLUE, VGA_COLOR_LIGHT_BLUE);
			if (x < 16 || y == 0) {
				writeTextChar(x, y, ' ', VGA_COLOR_BLACK, VGA_COLOR_BLACK);
			}
		}
	}
	writeTextString(16, 0, "Banana Setup", VGA_COLOR_WHITE, VGA_COLOR_BLACK);

	writeTextString(1, 1, "Preparing to\nrun Banana\nsetup.", stage == 0 ? VGA_COLOR_LIGHT_BROWN : VGA_COLOR_WHITE, VGA_COLOR_BLACK);
	writeTextString(1, 5, "Collecting\ninfo about\nyour computer", stage == 1 ? VGA_COLOR_LIGHT_BROWN : VGA_COLOR_WHITE, VGA_COLOR_BLACK);
	writeTextString(1, 9, "Copying Banana\nfiles to your\ncomputer", stage == 2 ? VGA_COLOR_LIGHT_BROWN : VGA_COLOR_WHITE, VGA_COLOR_BLACK);
	writeTextString(1, 13, "Setting up\nhardware", stage == 3 ? VGA_COLOR_LIGHT_BROWN : VGA_COLOR_WHITE, VGA_COLOR_BLACK);
	writeTextString(1, 16, "Finalizing\nsettings.", stage == 4 ? VGA_COLOR_LIGHT_BROWN : VGA_COLOR_WHITE, VGA_COLOR_BLACK);
	
	writeTextString(1, 19, "\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4", VGA_COLOR_WHITE, VGA_COLOR_BLACK);
	writeTextString(1, 21, "Estimated time\nremaining:", VGA_COLOR_LIGHT_CYAN, VGA_COLOR_BLACK);
	writeTextString(1, 23, esttime, VGA_COLOR_LIGHT_CYAN, VGA_COLOR_BLACK);

	if (windowtitle) {
		for (int x = 20; x < 74; ++x) {
			writeTextChar(x, 3, ' ', VGA_COLOR_BLUE, VGA_COLOR_BLUE);
		}
		for (int y = 4; y < 23; ++y) {
			for (int x = 20; x < 74; ++x) {
				writeTextChar(x, y, ' ', VGA_COLOR_WHITE, VGA_COLOR_WHITE);
			}
		}

		writeTextString(20, 3, windowtitle, VGA_COLOR_WHITE, VGA_COLOR_BLUE);
	}
}

void firstRAMCheck()
{
	displayScreen(1, "18 minutes", "                     Checking RAM                     ");

	extern bool validMemory(uint8_t * address);

	writeTextString(22, 5, "Checking amount and location of memory.", VGA_COLOR_BLACK, VGA_COLOR_WHITE);
	writeTextString(22, 7, "                                        ", VGA_COLOR_LIGHT_GREY, VGA_COLOR_LIGHT_GREY);

	{
		uint64_t* rdp = (uint64_t*) RAM_TABLE_LOCATION;
		uint64_t total = 0;
		for (uint16_t i = 0; i < ramTableLength; ++i) {
			for (volatile int u = 0; u < 100000; ++u) {
				u ^= 0x77;
				u ^= 888;
				u ^= 0x77;
				u ^= 888;
			}

			for (int j = 0; j < (i * 20) / ramTableLength; ++j) {
				writeTextChar(22 + j, 7, ' ', VGA_COLOR_DARK_GREY, VGA_COLOR_DARK_GREY);
			}
			uint64_t addr = *(rdp + 0);
			uint64_t length = *(rdp + 1);
			total += length;
			rdp += 3;	//24 bytes / uint64_t = 3
		}
		freeBytesApprox = total;

		for (int j = 0; j < 20; ++j) {
			writeTextChar(22 + j, 7, ' ', VGA_COLOR_DARK_GREY, VGA_COLOR_DARK_GREY);
		}
		if (freeBytesApprox < 1024 * 1024 * 200 || ramTableLength == 0) {
			displayScreen(1, "17 minutes", "                     Checking RAM                     ");
			writeTextString(22, 5, "Your computer doesn't have enough memory to\nrun Banana.\n\nBanana needs at least 256 MB of RAM to run.\n\nPlease eject the disc and turn off your computer.", VGA_COLOR_BLACK, VGA_COLOR_WHITE);
		}

		int ptr = 42;

		uint32_t loc[10] = { \
			0x20000,
			0x400511,
			0x300067,
			0xC00000,
			0xECAAB5,
			0x1000501,
			0x140ABCD,
			0x180BCDE,
			0x20CDEF1,
			0x2808000 };

		for (int m = 0; m < 10; ++m) {
			for (volatile int u = 0; u < 100000; ++u) {
				u ^= 0x77;
				u ^= 888;
				u ^= 0x77;
				u ^= 888;
			}

			if (validMemory((void*) (size_t) loc[m])) {
				writeTextString(ptr, 7, "  ", VGA_COLOR_DARK_GREY, VGA_COLOR_DARK_GREY);
			} else {
				displayScreen(1, "17 minutes", "                     Checking RAM                     ");
				writeTextString(22, 5, "Some of the memory Banana required is damaged or non-existant.\n\nPlease eject the disc and turn off your computer.", VGA_COLOR_BLACK, VGA_COLOR_WHITE);
			}
			ptr += 2;
		}
	}
}

void timeZoneCheck()
{
	char timezones[62][64] = { \
		"- 12.0	Eniwetok, Kwajalein",
			"- 11.0	Midway Island, Samoa",
			"- 10.0	Hawaii",
			"- 9.0	Alaska",
			"- 8.0	Pacific Time (US & Canada); Tijuana",
			"- 7.0	Arizona",
			"- 7.0	Mountain Time (US & Canada)",
			"- 6.0	Central Time (US & Canada)",
			"- 6.0	Mexico City, Tegucigalpa",
			"- 5.0	Bogota, Lima, Quito",
			"- 5.0	Eastern Time (US & Canada)",
			"- 5.0	Indiana (East)",
			"- 4.0	Atlantic Time (Canada)",
			"- 4.0	Caracas, La Paz",
			"- 4.0	Santiago",
			"- 3.5	Newfoundland",
			"- 3.0	Brasilia",
			"- 3.0	Buenos Aires, Georgetown",
			"- 2.0	Mid - Atlantic",
			"- 1.0	Azores, Cape Verde Is.",
			"+ 0.0	Casablanca, Monrovia",
			"+ 0.0	Greenwich Mean Time : Dublin, Edinburgh, Lisbon, London",
			"+ 1.0	Amsterdam, Berlin, Bern, Rome, Stockholm, Vienna",
			"+ 1.0	Belgrade, Bratislava, Budapest, Ljubljana, Prague",
			"+ 1.0	Brussels, Copenhagen, Madrid, Paris, Vilnius",
			"+ 1.0	Sarajevo, Skopje, Sofia, Warsaw, Zagreb",
			"+ 2.0	Athens, Istanbul, Minsk",
			"+ 2.0	Bucharest",
			"+ 2.0	Cairo",
			"+ 2.0	Harare, Pretoria",
			"+ 2.0	Helsinki, Riga, Tallinn",
			"+ 2.0	Jerusalem",
			"+ 3.0	Baghdad, Kuwait, Riyadh",
			"+ 3.0	Moscow, St.Petersburg, Volgograd",
			"+ 3.0	Nairobi",
			"+ 3.5	Tehran",
			"+ 4.0	Abu Dhabi, Muscat",
			"+ 4.0	Baku, Tbilisi",
			"+ 4.5	Kabul",
			"+ 5.0	Ekaterinburg",
			"+ 5.0	Islamabad, Karachi, Tashkent",
			"+ 5.5	Bombay, Calcutta, Madras, New Delhi",
			"+ 6.0	Astana, Almaty, Dhaka",
			"+ 6.0	Colombo",
			"+ 7.0	Bangkok, Hanoi, Jakarta",
			"+ 8.0	Beijing, Chongqing, Hong Kong, Urumqi",
			"+ 8.0	Perth",
			"+ 8.0	Singapore",
			"+ 8.0	Taipei",
			"+ 9.0	Osaka, Sapporo, Tokyo",
			"+ 9.0	Seoul",
			"+ 9.0	Yakutsk",
			"+ 9.5	Adelaide",
			"+ 9.5	Darwin",
			"+ 10.0	Brisbane",
			"+ 10.0	Canberra, Melbourne, Sydney",
			"+ 10.0	Guam, Port Moresby",
			"+ 10.0	Hobart",
			"+ 10.0	Vladivostok",
			"+ 11.0	Magadan, Solomon Is., New Caledonia",
			"+ 12.0	Auckland, Wellington",
			"+ 12.0	Fiji, Kamchatka, Marshall Is." };

	int selected = 0;
	int scrolled = 0;
	bool rel = false;

	//displayScreen(1, "17 minutes", "                     Checking RAM                     ");
	displayScreen(1, "17 minutes", "                       Timezone                       ");
	writeTextString(22, 5, "Please select your timezone from the below list.", VGA_COLOR_BLACK, VGA_COLOR_WHITE);

	while (1) {
		for (int i = 0; i < 15; ++i) {
			if (scrolled + i >= 62) break;
			if (selected == scrolled + i) {
				writeTextStringWindowLock(22, 7 + i, timezones[scrolled + i], VGA_COLOR_WHITE, VGA_COLOR_BLUE);
			} else {
				writeTextStringWindowLock(22, 7 + i, timezones[scrolled + i], VGA_COLOR_BLACK, VGA_COLOR_WHITE);
			}
		}

		uint8_t ib = 0;
		while (1) {
			uint8_t r = inb(0x64);
			if (r & 1) {
				ib = inb(0x60);
				if (ib == 0xE0) ib = inb(0x60);
				break;
			}
		}
		if (ib == 0xF0) {
			rel = true;
			continue;
		}
		if ((ib == 0x75 && rel) || ib == 0xC8) {
			selected -= 1;
			if (selected == -1) {
				selected = 0;
			}
			if (selected - scrolled < 7) {
				scrolled -= 1;
				if (scrolled == -1) {
					scrolled = 0;
				}
			}
			
			displayScreen(1, "17 minutes", "                       Timezone                       ");
			writeTextString(22, 5, "Please select your timezone from the below list.", VGA_COLOR_BLACK, VGA_COLOR_WHITE);

		} else if ((ib == 0x72 && rel) || ib == 0xD0) {
			selected += 1;
			if (selected == 61) {
				selected = 60;
			}
			if (selected - scrolled > 8) {
				scrolled += 1;
				if (scrolled == 62 - 15) {
					scrolled = 61 - 15;
				}
			}
			displayScreen(1, "17 minutes", "                       Timezone                       ");
			writeTextString(22, 5, "Please select your timezone from the below list.", VGA_COLOR_BLACK, VGA_COLOR_WHITE);

		} else if (ib == 0x1C || ib == 0x5A) {
			INFO_TO_WRITE_SOMEWHERE_SOON_TIMEZONE = selected;
			return;
		}
		rel = false;
	}
}

void firstGo()
{
	displayScreen(1, "18 minutes", "                     Banana Setup                     ");
	
	firstRAMCheck();
	timeZoneCheck();
	//rtcCheck();

	displayScreen(1, "2 minutes ", "                     Banana Setup                     ");
	writeTextString(22, 5, "Please wait...", VGA_COLOR_BLACK, VGA_COLOR_WHITE);
	writeTextString(22, 9, "If nothing happens and there is no disk activity\nfor a few minutes, please reboot your computer.", VGA_COLOR_DARK_GREY, VGA_COLOR_WHITE);
}

void firstGoWaitReboot()
{
	displayScreen(1, "1 minute  ", "                     Banana Setup                     ");

	for (int i = 0; i < 10; ++i) {
		for (volatile int u = 0; u < 1000000; ++u) {
			u ^= 0x77;
			u ^= 888;
			u ^= 0x77;
			u ^= 888;
		}
	}

	writeTextString(22, 5, "Setup is complete.\n\nPlease press RETURN to reboot your computer.", VGA_COLOR_BLACK, VGA_COLOR_WHITE);
	uint8_t ib = 0;
retry:
	ib = 0;
	while (1) {
		uint8_t r = inb(0x64);
		if (r & 1) {
			ib = inb(0x60);
			if (ib == 0xE0) ib = inb(0x60);
			break;
		}
	}
	if (ib == 0x1C || ib == 0x5A);
	else goto retry;
	writeTextString(22, 5, "Please restart your computer.", VGA_COLOR_BLACK, VGA_COLOR_WHITE);
	uint8_t good = 0x02;
	while (good & 0x02)
		good = inb(0x64);
	outb(0x64, 0xFE);
}

void kernel_early() {
    struct BootloaderData* boot = (struct BootloaderData*) 0x500;

	logk ("Size of 'struct BootloaderData' = %d vs expected 20.\n", sizeof (struct BootloaderData));
	
	logk("bytes per scanline %d\n", boot->bytesPerScanline);
	logk("height             %d\n", boot->y);
	logk("videoMemory >> 16  %d\n", (boot->videoMemory >> 16) & 0xFFF);
	logk("videoMemory        %d\n", boot->videoMemory & 0xFFF);

	logk ("KERNEL_EARLY WAS REACHED\n");
	logk ("%d RAM tables...\n", boot->lengthOfRAMTable);
	ramTableLength = boot->lengthOfRAMTable;
    ONE_LINE = boot->bytesPerScanline;
    TOTAL_HEIGHT = boot->y;
	TOTAL_WIDTH = boot->x;
    BYTES_PER_PIXEL = boot->bpp / 8;
	realVideoMemory = (uint32_t*) ((size_t) boot->videoMemory);	
	safeMode = boot->safemode;
	forceDisableAPIC = !boot->apicEnabled;
	readOnlyMode = boot->readonly;
	logk("huh.\n");

	logk ("Floppies = %d\n", boot->floppies);

	numberOfDisks = 0;

	for (int i = 0; i < 26; ++i) {
		Disks[i].letter = '?';
	}
 
    _e_next = _e_systicks;
    __stack_chk_guard = _e_randint();

	uint8_t* statptr = (uint8_t*) 0x7000;
	logk("Statptr = %d\n", *statptr);
	uint8_t* statptr2 = (uint8_t*) 0x555;
	logk("Statptr = %d\n", *statptr2);

	if (*statptr2 == 1) {
		logk("First go.\n");
		asm("cli");
		firstGo();

		//dummy values so it won't crash on video access (we're in text mode, and the values we got from 0x500 are garbage)
		realVideoMemory = (void*) (size_t) 0x20000000;
		TOTAL_HEIGHT = 800;
		TOTAL_WIDTH = 600;
		BYTES_PER_PIXEL = 2;

	} else if (*statptr2 == 0) {
		panic();
	} else {
		logk("realVideoMemory = 0x%X\nSize = 0x%X (0x%X 0x%X 0x%X)\n", realVideoMemory, TOTAL_HEIGHT * ONE_LINE * BYTES_PER_PIXEL, TOTAL_HEIGHT, ONE_LINE, BYTES_PER_PIXEL);
		memset(realVideoMemory, 0, TOTAL_HEIGHT * ONE_LINE * BYTES_PER_PIXEL);
	}

	logk ("EXIT INIT 2");
}

#undef EARLY_STAGE