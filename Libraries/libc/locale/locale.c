//
//  locale.c
//  Banana libc
//
//  Created by Alex Boxall on 16/7/18.
//  Copyright © 2018 Alex Boxall. All rights reserved.
//

#include "locale.h"
#include "string.h"
#include <limits.h>     //standard GCC header
#include "stdlib.h"

struct lconv __i_currentLocale;

struct __i_locales {
    struct lconv locale;
    char name[16];
    _Bool init;
    
} __i_locale_array [256];

void __i_localeInit() {
    struct lconv c;
    c.decimal_point = malloc(8);
    c.thousands_sep = malloc(8);
    c.grouping = malloc(8);
    c.mon_decimal_point = malloc(8);
    c.mon_thousands_sep = malloc(8);
    c.mon_grouping = malloc(8);
    c.positive_sign = malloc(8);
    c.negative_sign = malloc(8);
    c.currency_symbol = malloc(8);
    c.int_curr_symbol = malloc(8);
    
    strcpy(c.decimal_point, ".");
    strcpy(c.thousands_sep, "");
    strcpy(c.grouping, "");
    strcpy(c.mon_decimal_point, "");
    strcpy(c.mon_thousands_sep, "");
    strcpy(c.mon_grouping, "");
    strcpy(c.positive_sign, "");
    strcpy(c.negative_sign, "");
    strcpy(c.currency_symbol, "");
    strcpy(c.int_curr_symbol, "");

    c.frac_digits = CHAR_MAX;
    c.p_cs_precedes = CHAR_MAX;
    c.n_cs_precedes = CHAR_MAX;
    c.p_sep_by_space = CHAR_MAX;
    c.n_sep_by_space = CHAR_MAX;
    c.p_sign_posn = CHAR_MAX;
    c.n_sign_posn = CHAR_MAX;
    c.int_frac_digits = CHAR_MAX;
    c.int_p_cs_precedes = CHAR_MAX;
    c.int_n_cs_precedes = CHAR_MAX;
    c.int_p_sep_by_space = CHAR_MAX;
    c.int_n_sep_by_space = CHAR_MAX;
    c.int_p_sign_posn = CHAR_MAX;
    c.int_n_sign_posn = CHAR_MAX;
    
    __i_locale_array[0].locale = c;
    __i_locale_array[0].init = 1;
    strcpy(__i_locale_array[0].name, "C");
    
    setlocale(LC_ALL, "C");
}

//these set the __i_currentLocale variable

struct lconv* __i_localeForName (const char* locale) {
    for (int i = 0; i < 256; ++i) {
        if (__i_locale_array[i].init && !strcmp(__i_locale_array[i].name, locale)) {
            return &__i_locale_array[i].locale;
        }
    }
    return NULL;
}

_Bool __i_set_COLLATES (const char* locale) {
    return 0;
}

_Bool __i_set_CTYPES (const char* locale) {
    return 0;
}

_Bool __i_set_MONETARIES (const char* locale) {
    struct lconv* lp = __i_localeForName(locale);
    if (lp == NULL) {
        return 1;
    }
    struct lconv l = *lp;
    
    __i_currentLocale.currency_symbol = l.currency_symbol;
    __i_currentLocale.mon_grouping = l.mon_grouping;
    __i_currentLocale.mon_thousands_sep = l.mon_thousands_sep;
    __i_currentLocale.mon_grouping = l.mon_grouping;
    __i_currentLocale.positive_sign = l.positive_sign;
    __i_currentLocale.negative_sign = l.negative_sign;
    __i_currentLocale.frac_digits = l.frac_digits;
    __i_currentLocale.p_cs_precedes = l.p_cs_precedes;
    __i_currentLocale.n_cs_precedes = l.n_cs_precedes;
    __i_currentLocale.p_sep_by_space = l.p_sep_by_space;
    __i_currentLocale.n_sep_by_space = l.n_sep_by_space;
    __i_currentLocale.p_sign_posn = l.p_sign_posn;
    __i_currentLocale.n_sign_posn = l.n_sign_posn;
    __i_currentLocale.int_curr_symbol = l.int_curr_symbol;
    __i_currentLocale.int_frac_digits = l.int_frac_digits;
    __i_currentLocale.int_p_cs_precedes = l.int_p_cs_precedes;
    __i_currentLocale.int_n_cs_precedes = l.int_n_cs_precedes;
    __i_currentLocale.int_p_sep_by_space = l.int_p_sep_by_space;
    __i_currentLocale.int_n_sep_by_space = l.int_n_sep_by_space;
    __i_currentLocale.int_p_sign_posn = l.int_p_sign_posn;
    __i_currentLocale.int_n_sign_posn = l.int_n_sign_posn;
    
    return 0;
}

_Bool __i_set_NUMERICS (const char* locale) {
    struct lconv* lp = __i_localeForName(locale);
    if (lp == NULL) {
        return 1;
    }
    struct lconv l = *lp;
    
    __i_currentLocale.decimal_point = l.decimal_point;
    __i_currentLocale.thousands_sep = l.thousands_sep;
    __i_currentLocale.grouping = l.grouping;
    
    return 0;
}

_Bool __i_set_TIMES (const char* locale) {
    return 0;
}

char *setlocale(int category, const char *locale) {
    _Bool res = 0;
    switch (category) {
        case LC_ALL:
            res = __i_set_COLLATES(locale);
            res |= __i_set_CTYPES(locale);
            res |= __i_set_MONETARIES(locale);
            res |= __i_set_NUMERICS(locale);
            res |= __i_set_TIMES(locale);
            break;
        case LC_COLLATE:
            res = __i_set_COLLATES(locale);
            break;
        case LC_CTYPE:
            res = __i_set_CTYPES(locale);
            break;
        case LC_MONETARY:
            res = __i_set_MONETARIES(locale);
            break;
        case LC_NUMERIC:
            res = __i_set_NUMERICS(locale);
            break;
        case LC_TIME:
            res = __i_set_TIMES(locale);
            break;
    }
    if (res) {
        return NULL;
    }
    return (char*) locale;
}

struct lconv *localeconv(void) {
    struct lconv* x = malloc(sizeof(struct lconv));
    *x = __i_currentLocale;
    return x;
}
