//
//  time.h
//  Banana libc
//
//  Created by Alex Boxall on 11/7/18.
//  Copyright © 2018 Alex Boxall. All rights reserved.
//

#ifndef time_h
#define time_h

#if defined(__cplusplus)
extern "C" {
#endif
    
#include <stddef.h>
    
#undef NULL
#define NULL 0
    
#define TIME_UTC 1      //must be non-zero
#define CLOCKS_PER_SEC 40
#ifndef _TIME_T_DEFINED
#define _TIME_T_DEFINED
    typedef unsigned long long time_t;
    typedef unsigned long long clock_t;
#endif
    
    struct timespec {
        time_t tv_sec;
        long tv_nsec;
    };
    
    struct tm {
        int tm_sec;   //seconds after the minute — [0, 60]
        int tm_min;   //minutes after the hour — [0, 59]
        int tm_hour;  //hours since midnight — [0, 23]
        int tm_mday;  //day of the month — [1, 31]
        int tm_mon;   //months since January — [0, 11]
        int tm_year;  //years since 1900
        int tm_wday;  //days since Sunday — [0, 6]
        int tm_yday;  //days since January 1 — [0, 365]
        int tm_isdst; //Daylight Saving Time flag (0=no, 1=yes, -1=maybe)
    };
    
    clock_t clock(void);
    double difftime(time_t time1, time_t time0);
    
    time_t time(time_t *timer);
    time_t mktime(struct tm *timeptr);
    int timespec_get(struct timespec *ts, int base);
    
	#ifndef _libk
    char *asctime(const struct tm *timeptr);
    char *ctime(const time_t *timer);
    struct tm *gmtime(const time_t *timer);
    struct tm *localtime(const time_t *timer);
    size_t strftime(char * restrict s,
                            size_t maxsize,
                            const char * restrict format,
                            const struct tm * restrict timeptr);
    #endif
	
#if defined(__cplusplus)
}
#endif

#endif /* time_h */
