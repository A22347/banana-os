//
//  x86.s
//  Banana libc
//
//  Created by Alex Boxall on 28/9/18.
//  Copyright © 2018 Alex Boxall. All rights reserved.
//

// from https://github.com/eblot/newlib/blob/master/newlib/libc/machine/i386/setjmp.S

/*
setjmp:
pushl    ebp
movl    esp,ebp

pushl    edi
movl    8 (ebp),edi

movl    eax,0 (edi)
movl    ebx,4 (edi)
movl    ecx,8 (edi)
movl    edx,12 (edi)
movl    esi,16 (edi)

movl    -4 (ebp),eax
movl    eax,20 (edi)

movl    0 (ebp),eax
movl    eax,24 (edi)

movl    esp,eax
addl    $12,eax
movl    eax,28 (edi)

movl    4 (ebp),eax
movl    eax,32 (edi)

popl    edi
movl    $0,eax
leave
ret

longjmp:
pushl    ebp
movl    esp,ebp

movl    8(ebp),edi
movl    12(ebp),eax
testl    eax,eax
jne    0f
incl    eax
0:
movl    eax,0(edi)

movl    24(edi),ebp

movl    28(edi),esp

pushl    32(edi)

movl    0(edi),eax
movl    4(edi),ebx
movl    8(edi),ecx
movl    12(edi),edx
movl    16(edi),esi
movl    20(edi),edi

ret
*/

/*
;setjmp:
;    push ebp
;    mov ebp, esp

;    push edi

;   mov edi, [ebp + 8]
;   mov eax, [edi + 0]
;   mov ebx, [edi + 4]
;   mov ecx, [edi + 8]
;   mov edx, [edi + 12]
;   mov esi, [edi + 16]

;   mov eax, [ebp - 4]
;   mov [edi + 20], eax

;   mov eax, [ebp]
;   mov [edi + 24], eax

;   mov eax, esp
;   add eax, 12
;   mov [edi + 28], eax

;   mov eax, [ebp + 4]
;   mov [edi + 32], eax

;   pop edi
;   mov eax, 0

;   leave
;   ret

;longjmp:
;   push ebp
;   mov ebp, esp

;    mov edi, [ebp + 8]      ;get jmp_buf
;   mov eax, [ebp + 12]     ;store retval in j->eax

;   test eax, eax
;   jne .x

;   inc eax

;.x:
;    mov [edi], eax
;    mov ebp, [edi + 24]

;    ;cli
;    mov esp, [edi + 28]

;    push [edi + 32]

;    mov eax, [edi]
;    mov ebx, [edi + 4]
;    mov ecx, [edi + 8]
;    mov edx, [edi + 12]
;    mov esi, [edi + 16]
;    mov edi, [edi + 20]
;    ;sti

;    ret
*/
