#include <core/syscalls.h>
#include <core/panic.h>
#include <core/scheduler.h>
#include <hal/clock.h>
#include <gui/guilink.h>
#include <hal/mouse.h>
#include <core/objectmgr.h>

int (*system_calls[0xFFF]) (struct regs *r);

int kernelToTerminalPipe = 0;

/*
WARNING! NEVER RETURN AN ARRAY FROM A SYSCALL AS IT WILL GO OUT OF SCOPE!
*/

//use these functions for file operations:
//InternalObjectID createNewFileWrapper (vfs_file_t file);
//vfs_file_t getFileFromFileWrapper (InternalObjectID id);

// e.g. open("HI.TXT") called
//		vfs_file_t f = vfs_open("HI.TXT")
//		id = createNewFileWrapper(f)
//		TASKS[n].fileHandlesArray.append(id)		//not really like this
//		return id

// e.g. read(f) called
//		vfs_file_t i = getFileFromFileWrapper (f)
//		return vfs_read(i, ...)

bool userlandPointer (size_t ptr)
{
	return true;
}

int Sys_GetPID (struct regs *r)
{
	if (r->ebx) {
		//return TID inside the process (unique across the process, MUST BE BELOW 256 SO THAT void (*__CancelRoutines[256][256])(void*))
		//works in pthreads (each thread gets it's own 64 cleanup items)
		return TASKS[TASK_Current].currentlyExecutingThread;
	}
	return TASK_Current;
}

int Sys_Log (struct regs *r)
{
	//if (!userlandPointer (r->edx)) return 1;
	ApplicationLog ((char*) r->edx, TASKS[TASK_Current].filename);
	return 0;
}

int Sys_Panic (struct regs *r)
{
	if (!userlandPointer (r->edx)) return 1;
	strcpy (panicErrorMessage, (char*) r->edx);
	panic ();
	return 0;
}

int Sys_GetTime (struct regs *r)
{
	//returns a pointer to a 64 bit number
	//as 64 bit numbers cannot be returned
	uint64_t* aff = malloc (8);
	*aff = getClockTime ();
	return (size_t) (aff);
}

int Sys_NumberOfArguments (struct regs *r)
{
	return TASKS[TASK_Current].numberOfArguments;
}

int Sys_GetArgument (struct regs *r)
{
	if (TASKS[TASK_Current].driver) {
		logk ("DRIVER GETTING ARGUMENTS\n");
		return 0;
	}
	if (r->edx) {
		strcpy((char*) r->edx, TASKS[TASK_Current].arguments[r->ebx]);
	}
	return (size_t) TASKS[TASK_Current].arguments[r->ebx];
}

int Sys_Username (struct regs *r)
{
	return (size_t) username;
}

int Sys_Abort (struct regs *r)
{
	logk("TASK ABORTED WITH CODE 0x%X\n", r->ebx);
	abortTask (r->ebx);
	return 0;
}

int Sys_Reboot (struct regs *r)
{
	reboot ();
	return 1;
}

int Sys_Shutdown (struct regs *r)
{
	shutdown ();
	return 1;
}

int Sys_LauchTerminalSession (struct regs* r)
{
	logk ("Terminal session started!\n");
	if (TASKS[TASK_Current].driver || !TASKS[TASK_Current].console) {
		return 0;
	}

	if (!TASKS[TASK_Current].stdin || !TASKS[TASK_Current].stdout || !TASKS[TASK_Current].stderr) {
		//this means we don't inherit the parent's stdin or stdout
		TASKS[TASK_Current].stdin = createNewStdin ();

		InternalObjectID writeEnd = 0;
		InternalObjectID readEnd = 0;

		createNewPipe(&readEnd, &writeEnd);
		TASKS[TASK_Current].stdout = writeEnd;

		//how this works is all stdouts will be pipes, but ones that come before
		//TERMINAL.SYS won't have anyone reading them
		if (kernelToTerminalPipe) {
			int one = 0x01010101;
			filePipeStdoutWrite(kernelToTerminalPipe, 4, (uint8_t*) "MSG!");
			filePipeStdoutWrite(kernelToTerminalPipe, 4, (uint8_t*) & one);
			filePipeStdoutWrite(kernelToTerminalPipe, 4, (uint8_t*) & readEnd);
		}
		
		TASKS[TASK_Current].stderr = TASKS[TASK_Current].stdout;

	} else {
		logk("NOT creating a new terminal (hopefully!)\n");
		//someone else (e.g. Sys_Spawn) is responsible for setting the values before we get here

		logk("stdout is already at 0x%X\n", TASKS[TASK_Current].stdout);
		logk("stderr is already at 0x%X\n", TASKS[TASK_Current].stderr);
		logk("stdin  is already at 0x%X\n", TASKS[TASK_Current].stdin);
	}

	TASKS[TASK_Current].console = true;
	char name[256];
	memset (name, 0, 256);
	if (TASK_Current == 1) {
		strcpy (name, "Banana Login");
	} else {
		sprintf (name, "#Terminal Session %d", TASK_Current);
	}

	logk ("The name is %s\n", name);

	return 0;
}

int Sys_ExitTerminalSession (struct regs* r)
{
	logk("Program ended with exit code 0x%X\n", r->ebx);

	//we'll just use Sys_Abort to terminate, both get called anyway
	if (TASKS[TASK_Current].driver || !TASKS[TASK_Current].console) {
		return 0;
	}

	return 0;
}

int Sys_System (struct regs* r)
{
	return system ((char*) r->edx);
}

int Sys_GetCwd (struct regs* r)
{

	//assumes the caller (getcwd()) has a buffer of at least 257)
	strcpy((char*) r->edx, TASKS[TASK_Current].threads[0].cwd);
	return 0;
}

int Sys_SetCwd (struct regs* r)
{
	if (!userlandPointer (r->edx)) return 1;
	int res = vfs_chdir ((char*) r->edx);
	vfs_getcwd (TASKS[TASK_Current].threads[0].cwd, 255);
	return res;
}

int Sys_GetEnv (struct regs* r)
{
	if (!userlandPointer (r->edx)) return 1;

	return 0;	// (size_t) GetEnvironmentVariable((char*) r->edx);
}

extern uint8_t taskModeActive;

void setupKernelInterfacePart2 ();
int Sys_BlackMagic (struct regs* r)
{
	static int guiCommsMemory = 0;

	if (TASKS[TASK_Current].systask || !__internal_login_variable__loginComplete) {
		switch (r->ebx) {
		case 43:	
			//LOGIN

			asm ("cli");
			strcpy (__internal_login_variable__loginUsername, (const char*) r->edx);
			//InnerDelete (used - 1);
			abortTask (0xFFFF);
			__internal_login_variable__loginComplete = true;

			taskModeActive = 0;

			//use 'black magic' to jump back the normal kernel code spot and exit this handler
			//and allow the func. to return normally (i.e. manipulate the stack)

			extern void postMagic ();
			postMagic ();

			break;

		case 47:		
			//HALT
			asm volatile ("sti; hlt");
			break;
		default:
			break;
		}
	} else if (1 || TASKS[TASK_Current].driver) {
		switch (r->ebx) {
		case 177:		
			//SET TERMINAL PIPE
			kernelToTerminalPipe = r->ecx;
			break;

		case 55009:
			//GET PID OF GUI EXECUTABLE
			for (int i = 0; i < TASK_Next; ++i) {
				if (TASKS[i].filename == 0) continue;		//as in the zeroth task
				if (!strcasecmp(TASKS[i].filename, "C:/Banana/System/GUI.EXE")) {
					return i;
				}
			}
			return 0;

		case 55010:
			//SEND GUI EXECUTABLE A MEMORY ADDRESS FOR A NEW TERMINAL
			guiCommsMemory = r->ecx;
			break;

		case 55011:
			//READ (+ DELETE IF SET) A MEMORY ADDRESS FOR A NEW TERMINAL
			if (r->ecx == 0) {
				return guiCommsMemory;
			} else if (r->ecx == 1) {
				int a = guiCommsMemory;
				guiCommsMemory = 0;
				return a;
			}

			break;

		default:
			break;
		}
	}  else {
		logk ("Black magic called by an application. Terminating the process.\n");
		abortTask (0xFFFF);
		return 2;
	}

	return 0;
}

int Sys_Sleep (struct regs* r)
{
	nanoSleepTask (r->ebx * 1000 * 1000);
	return 0;
}

int Sys_GetStdstreamObjectForProcess (struct regs* r)
{
	int* a = (int*) r->edx;
	if (r->ebx == 0) {
		*a = TASKS[TASK_Current].stdout;
	} else if (r->ebx == 1) {
		*a = TASKS[TASK_Current].stderr;
	} else {
		*a = TASKS[TASK_Current].stdin;
	}

	return 0;
}

int Sys_WriteFileOrPipeObjectFromFilename (struct regs* r)
{
	void parallelWriteChar(uint8_t portNo, uint8_t pData);

	for (uint32_t i = 0; i < r->ebx; ++i) {
		parallelWriteChar(0, ((uint8_t*) (size_t) r->edx)[i]);

	}
	InternalObjectID* x = (InternalObjectID*) r->ecx;
	logk("WRITING FROM ID 0x%X, SIZE 0x%X, POINTER = 0x%X\n", *x, (size_t) r->ebx, r->edx);

	int i = filePipeStdoutWrite(*x, (size_t) r->ebx, (uint8_t*) (size_t) r->edx);
	logk("WRITE RETURN = 0x%X\n", i);
	return i;
}

int Sys_ReadFileOrPipeObjectFromFilename (struct regs* r)
{
	InternalObjectID* x = (InternalObjectID*) r->ecx;
	logk("READING FROM ID 0x%X, SIZE 0x%X, POINTER = 0x%X\n", *x, (size_t) r->ebx, r->edx);

	int rt = filePipeStdinRead(*x, (size_t) r->ebx, (uint8_t*) (size_t) r->edx);

	return rt;
}

int Sys_Rename (struct regs* r)
{	
	return vfs_rename((char*) r->ecx, (char*) r->edx);
}

int Sys_NewFileDelete (struct regs* r)
{
	return vfs_unlink((char*) r->edx);
}

int Sys_OpenFileFromFilename (struct regs* r)
{
	logk("OPENING FILE: %s. ptr = 0x%X. mode = 0x%X\n", r->edx, r->ebx, r->ecx);
	uint64_t* ptr = (uint64_t*) (size_t) r->ebx;
	*ptr = createNewFileObject ((char*) r->edx, r->ecx);
	logk("OPEN FILE GAVE FILE POINTER OF 0x%X\n", *ptr);
	return fileObjectGetErrorBit(*ptr);
}

//	return SystemCall (SC_OpenDir, (size_t) fdout, 0, (void*) filename);
int Sys_OpenDir (struct regs* r)
{
	logk("OPEN DIR CALLED.\n");
	logk("FDOUT = 0x%X. fname = %s\n", r->ebx, r->edx);
	uint64_t* ptr = (uint64_t*) (size_t) r->ebx;
	*ptr = createNewDirectoryObject ((char*) r->edx);
	logk("Resulting ID = 0x%X\n", *ptr);
	logk("OPEN DIR CALL FINISHED.\n");
	return 0;
}

int Sys_MakeDir(struct regs* r)
{
	logk("MAKE DIR CALLED.\n");
	return vfs_mkdir((char*) r->edx);
}


//	return SystemCall (SC_ReadDir, 0, fd, direntObject);
int Sys_ReadDir (struct regs* r)
{
	return readDirObject (r->ecx, (struct dirent*) r->edx);
}

//	return SystemCall (SC_CloseDir, (size_t) fdout, 0, 0);
int Sys_CloseDir (struct regs* r)
{
	uint64_t* ptr = (uint64_t*) (size_t) r->ebx;
	closeDirObject (*ptr);
	return 0;
}

int Sys_CloseFile(struct regs* r)
{
	logk("CLOSING FILE!\n");
	uint64_t* ptr = (uint64_t*) (size_t) r->ebx;
	logk("POINTER IS 0x%X\n", ptr);
	logk("*POINTER IS 0x%X\n", *ptr);
	closeFileObject(*ptr);
	logk("CLOSED FILE\n");
	return 0;
}

int Sys_Time (struct regs* r)
{
	unsigned long long* ticksSinceStart = (unsigned long long*) (size_t) r->edx;
	unsigned long long* timeInSecs = (unsigned long long*) (size_t) r->ecx;

	*timeInSecs = getClockTime ();
	asm volatile ("rdtsc" : "=A"(*ticksSinceStart));

	return 0;
};

int Sys_Tell (struct regs* r)
{
	void* fd = (void*) r->edx;
	unsigned long long* outputPtr = (unsigned long long*) r->ecx;

	//int vfs_tell (vfs_file_t* file, uint64_t* output);
	return -1;
}

int Sys_Seek (struct regs* r)
{
	/*void* fd = (void*) r->edx;
	unsigned long long* wwhere = (unsigned long long*) r->ecx;
	int whence = r->ebx;

	unsigned long long position = *wwhere;

	return vfs_seek (vfs_file_t* file, wwhere);*/
	return -1;
}

int Sys_Execve(struct regs* r)
{
	char*  npath = (char*) (size_t) r->edx;
	char** nargs = (char**) (size_t) r->ecx;
	char** nenvr = (char**) (size_t) r->ebx;

	char* path = malloc(256);
	strcpy(path, npath);

	logk("NARGS = 0x%X\n", nargs);

	char** args = malloc(256 * sizeof(char*));
	memset(args, 0, 256 * sizeof(char*));
	for (int i = 0; i < 255; ++i) {
		if (nargs[i] == 0) break;
		logk("NARGS %d (0x%X %s)\n", i, nargs[i], nargs[i]);
		char* a = malloc(256);
		strcpy(a, nargs[i]);
		args[i] = a;
	}

	/*char** envr = malloc(1024 * sizeof(char*));
	memset(envr, 0, 1024 * sizeof(char*));
	for (int i = 0; i < 1023; ++i) {
		if (nenvr[i] == 0) break;
		char* a = malloc(256);
		strcpy(a, nenvr[i]);
		envr[i] = a;
	}*/

	logk("execve called with a path of %s\n", path);

	asm("cli");
	internalExecve(path, args, 0);
	asm("sti");

	logk("Sys_Execve halting\n");

	while (1) {
		logk("YIELD\n");
		yieldFromKernel();
	}

	return 0;		
}


int Sys_GUIEXE_VidScanlineWidth (struct regs* r) {
	return ONE_LINE;
}

int Sys_GUIEXE_VidVisibleWidth(struct regs* r) {
	return TOTAL_WIDTH;
}

int Sys_GUIEXE_VidHeight(struct regs* r) {
	return TOTAL_HEIGHT;
}

int Sys_GUIEXE_VidFlushBuffer(struct regs* r) {
	displayCopyBufferTo((uint32_t*) r->edx, 0, ONE_LINE * TOTAL_HEIGHT * 4, r->ecx);
	return 0;
}

int Sys_Wait (struct regs* r)
{
	TASKS[TASK_Current].threads[0].wait = r->ebx;
	return 0;
}

int Sys_Spawn (struct regs* r)
{
	logk("Task %s\n", r->edx);
	extern void storeAddTaskAsChild(char* path);
	if (r->ebx == 1) {
		storeAddTaskAsChild((char*) r->edx);
	} else {
		storeAddTask((char*) r->edx);
	}
	//addTask((char*) r->edx);
	logk("Spawn called by task 0x%X\n", TASK_Current);
	logk("Spawn: stdout is at 0x%X\n", TASKS[TASK_Current].stdout);
	logk("Spawn: stderr is at 0x%X\n", TASKS[TASK_Current].stderr);
	logk("Spawn: stdin  is at 0x%X\n", TASKS[TASK_Current].stdin);

	logk("YIELDING in Sys_Spawn\n"); 
	yieldFromKernel();
	logk("YIELD FINISHED in Sys_Spawn\n");

	logk("Task %s SHOULD have a PID of %d\n", r->edx, TASK_Next - 1);
	return spawnResult;
}

int Sys_Fork(struct regs* r)
{
	storeFork();
	logk("Forever looping? C\n");

	yieldFromKernel();
	return TASKS[TASK_Current].forkReturnVal;
}

int Sys_Pipe(struct regs* rrr)
{
	InternalObjectID* r = (InternalObjectID*) (size_t) rrr->ebx;
	InternalObjectID* w = (InternalObjectID*) (size_t) rrr->ecx;
	createNewPipe(r, w);
	return 0;
}

uint64_t p2;
int callingPIDAllocateSharedMemory;

int Sys_AllocateSharedMemory(struct regs* r)
{
	if (callingPIDAllocateSharedMemory != 0 && callingPIDAllocateSharedMemory != TASK_Current) {
		logk("RACE CONDITION ON Sys_AllocateSharedMemory. A different task tried to overwrite data! (Double check that the original task actually does call P2, or no other process will be allowed to allocated shared memory)\n");
		return 0x0;
	}

	uint64_t outAddressForPID1 = 0;
	uint64_t outAddressForPID2 = 0;

	allocateSharedMemory(TASK_Current, r->ebx, r->ecx, &outAddressForPID1, &outAddressForPID2);

	p2 = outAddressForPID2;

	callingPIDAllocateSharedMemory = TASK_Current;

	return outAddressForPID1;
}

int Sys_AllocateSharedMemoryP2(struct regs* r)
{
	if (callingPIDAllocateSharedMemory != TASK_Current) {
		logk("RACE CONDITION ON Sys_AllocateSharedMemoryP2. A different task tried to get data that wasn't theirs!\n");
		return 0x0;
	}
	int a = p2;
	p2 = 0;
	callingPIDAllocateSharedMemory = 0;
	return a;
}

int Sys_Sbrk(struct regs* r)
{
	size_t previousBreak = (size_t) TASKS[TASK_Current].highestUnallocedAddress;

	if (r->ebx == 0) {
		return previousBreak;
	}

	if (((int)r->ebx) < 0) {
		//decrease memory, not supported ATM
		return previousBreak;
	}

	if (r->ebx > 1024 * 1024 * 16) {
		logk("who the heck decided to allocated that much memory? 0x%X\n", r->ebx);
		return -1;
	}

	allocateMemoryForTask(TASK_Current, 0, 0, 0, TASKS[TASK_Current].highestUnallocedAddress, ((r->ebx + 4095) / 4096) * 4096);

	TASKS[TASK_Current].highestUnallocedAddress += ((r->ebx + 4095) / 4096) * 4096;

	return previousBreak;
}

int Sys_Seekdir(struct regs* r)
{
	return -1;
}

int Sys_Telldir(struct regs* r)
{
	return -1;
}

int Sys_FileSize(struct regs* r)
{
	//file open (fstat)
	if (r->ebx == 0) {
		return vfs_size(getFileObjectFromObject(r->ecx), (uint64_t*) (size_t) r->edx);

	//from filepath (stat)
	} else {
		logk("STAT NOT SUPPORTED YET!\n");
		return 1;
	}
}

void install_system_call (int a, int (*b) (struct regs *r))
{
	system_calls[a] = b;
}

void setupSystemCalls ()
{
	install_system_call (SC_GetPID, Sys_GetPID);
	install_system_call (SC_Log, Sys_Log);
	install_system_call (SC_Panic, Sys_Panic);
	install_system_call (SC_GetTime, Sys_GetTime);
	install_system_call (SC_NumberOfArguments, Sys_NumberOfArguments);
	install_system_call (SC_GetArgument, Sys_GetArgument);
	install_system_call (SC_Username, Sys_Username);
	install_system_call (SC_Abort, Sys_Abort);
	install_system_call (SC_Reboot, Sys_Reboot);
	install_system_call (SC_Shutdown, Sys_Shutdown);
	install_system_call (SC_LauchTerminalSession, Sys_LauchTerminalSession);
	install_system_call (SC_ExitTerminalSession, Sys_ExitTerminalSession);
	install_system_call (SC_System, Sys_System);
	install_system_call (SC_GetCwd, Sys_GetCwd);
	install_system_call (SC_SetCwd, Sys_SetCwd);
	install_system_call (SC_GetEnv, Sys_GetEnv);
	install_system_call (SC_BlackMagic, Sys_BlackMagic);
	install_system_call (SC_Sleep, Sys_Sleep);
	install_system_call (SC_GetStdstreamObjectForProcess, Sys_GetStdstreamObjectForProcess);
	install_system_call (SC_WriteFileOrPipeObjectFromFilename, Sys_WriteFileOrPipeObjectFromFilename);
	install_system_call (SC_ReadFileOrPipeObjectFromFilename, Sys_ReadFileOrPipeObjectFromFilename);
	install_system_call (SC_OpenFileFromFilename, Sys_OpenFileFromFilename);
	install_system_call (SC_CloseFile, Sys_CloseFile);

	install_system_call (SC_Rename, Sys_Rename);
	install_system_call (SC_NewFileDelete, Sys_NewFileDelete);

	install_system_call (SC_Time, Sys_Time);
	install_system_call (SC_Tell, Sys_Tell);
	install_system_call (SC_Seek, Sys_Seek);

	install_system_call (SC_OpenDir, Sys_OpenDir);
	install_system_call (SC_ReadDir, Sys_ReadDir);
	install_system_call (SC_CloseDir, Sys_CloseDir);

	install_system_call(SC_Execve, Sys_Execve);

	install_system_call (SC_GUIEXE_VidScanlineWidth, Sys_GUIEXE_VidScanlineWidth);
	install_system_call (SC_GUIEXE_VidVisibleWidth, Sys_GUIEXE_VidVisibleWidth);
	install_system_call (SC_GUIEXE_VidHeight, Sys_GUIEXE_VidHeight);
	install_system_call (SC_GUIEXE_VidFlushBuffer, Sys_GUIEXE_VidFlushBuffer);

	install_system_call(SC_Wait, Sys_Wait);
	install_system_call(SC_Spawn, Sys_Spawn);
	install_system_call(SC_Fork, Sys_Fork);

	install_system_call(SC_Pipe, Sys_Pipe);
	install_system_call(SC_AllocateSharedMemory, Sys_AllocateSharedMemory);
	install_system_call(SC_AllocateSharedMemoryP2, Sys_AllocateSharedMemoryP2);

	install_system_call(SC_Sbrk, Sys_Sbrk);

	install_system_call(SC_Telldir, Sys_Telldir);
	install_system_call(SC_Seekdir, Sys_Seekdir);

	install_system_call(SC_FileSize, Sys_FileSize);
	install_system_call(SC_MakeDir, Sys_MakeDir);

	/* ENSURE THAT SC_NewThread allows a maximum of 32 threads per process
	so that pthread structures aren't huge in the executable file (we could use malloc() in pthread.c,
	but I don't feel like working with pointers to arrays of pointers to function pointers (e.g. quadruple pointers)
	*/
}

#include <core/spinlock.h>

void system_call (struct regs *r)
{
	tasklock_t t;
	initTasklock (&t);
	lockTasklock (&t);
	int (*call) (struct regs *r) = *system_calls[r->eax];
	if (call) {
		syscallreturn = call (r);
	}
	unlockTasklock (&t);
}