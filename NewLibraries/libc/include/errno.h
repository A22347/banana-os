//
//  errno.h
//  Banana libc
//
//  Created by Alex Boxall on 11/7/18.
//  Copyright © 2018 Alex Boxall. All rights reserved.
//

#ifndef errno_h
#define errno_h

#if defined(__cplusplus)
#define restrict			//fixes some 'fun' errors
extern "C" {
#endif
    
#define EDOM 1
#define EILSEQ 2
#define ERANGE 3
#define ESIGFAIL 4
    
    extern int errno;          //must have "thread local storage duration"
        
#if defined(__cplusplus)
}
#endif

#endif /* errno_h */
