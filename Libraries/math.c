#include "math.h"
#include <math.h>

unsigned long long __fact (unsigned long long n)
{
	if (!n) {
		return 1;
	}
	return n * __fact (n - 1);
}

double tan(double x) {
	return (sin(x)/cos(x));   
}

double acos(double x) {
	return 0;
}

double asin(double x) {
	return 0;
}

double atan(double x) {
	return 0;
}

double atan2(double y, double x) {
	return 0;
}

double cos(double x) {
	while (x < -3.14159265) {
        x += 6.28318531;
    }
    while (x >  3.14159265) {
        x -= 6.28318531;
    }
    
    double y = 1;
    double s = -1;
    for (int i=2; i<=100; i+=2) {
        y+=s*(pow(x,i)/ __fact (i));
        s *= -1;
    }
    return y;
}

double cosh(double x) {
	return 0;
}

double sin(double x) {
	while (x < -3.14159265) {
        x += 6.28318531;
    }
    while (x >  3.14159265) {
        x -= 6.28318531;
    }
    
    double y = x;
    double s = -1;
    for (int i=3; i<=100; i+=2) {
        y+=s*(pow(x,i)/ __fact (i));
        s *= -1;
    }
    return y;
}

double sinh(double x) {
	return 0;
}

double tanh(double x) {
	return 0;
}

double exp(double x) {
    return pow(M_E, x);
}

long double ldexpll (long double x, long exponent)
{
	return x * pow (2, exponent);
}

float ldexpf (float x, int exponent) {
    return x * pow(2, exponent);
}

double ldexp (double x, int exponent)
{
	return x * pow (2, exponent);
}

long double ldexpl (long double x, int exponent)
{
	return x * pow (2, exponent);
}

double log (double a) {
    return logf(a);
}

double logf (double a) {
    float m, r, s, t, i, f;
    int32_t e;
    
    if ((a > 0.0f) && (a <= 3.40282347e+38f)) { // 0x1.fffffep+127
        m = frexpf (a, &e);
        if (m < 0.666666667f) {
            m = m + m;
            e = e - 1;
        }
        i = (float)e;
        /* m in [2/3, 4/3] */
        f = m - 1.0f;
        s = f * f;
        /* Compute log1p(f) for f in [-1/3, 1/3] */
        r = fmaf (-0.130187988f, f, 0.140889585f); // -0x1.0aa000p-3, 0x1.208ab8p-3
        t = fmaf (-0.121489584f, f, 0.139809534f); // -0x1.f19f10p-4, 0x1.1e5476p-3
        r = fmaf (r, s, t);
        r = fmaf (r, f, -0.166845024f); // -0x1.55b2d8p-3
        r = fmaf (r, f,  0.200121149f); //  0x1.99d91ep-3
        r = fmaf (r, f, -0.249996364f); // -0x1.fffe18p-3
        r = fmaf (r, f,  0.333331943f); //  0x1.5554f8p-2
        r = fmaf (r, f, -0.500000000f); // -0x1.000000p-1
        r = fmaf (r, s, f);
        r = fmaf (i, 0.693147182f, r); //   0x1.62e430p-1 // log(2)
    } else {
        r = a + a;  // silence NaNs if necessary
        if (a  < 0.0f) r =  0.0f / 0.0f; //  NaN
        if (a == 0.0f) r = -1.0f / 0.0f; // -Inf
    }
    return r;
}

double log10(double x) {
	return 0;
}

double log2(double x) {
    return log(x) * M_LOG2E;
}

double modf(double x, double* integer) {
    double floating = x - floor(x);
    *integer = x - floating;
    return floating;
}

double pow(double x, double y) {
	double c = 1;
    for (int i = 0; i<y; i++)
        c *= x;
    return c;
}

double sqrt(double number) {
	long i;
	double x2, y;
	const double threehalfs = 1.5F;

	x2 = number * 0.5F;
	y  = number;
	i  = *(long*) &y;
	i  = 0x5fe6eb50c7b537a9 - (i >> 1);
	y  = *(double*) &i;
	y  = y * (threehalfs - (x2 * y * y));
	
	return y;
}

double ceil(double x) {
	return (double)((uint64_t) x + 1);
}

double round(double x) {
	return (double)((uint64_t) x + 0.5);
}

double floor(double x) {
	return (double)((uint64_t) x);
}

double fabs(double x) {
	return (x < 0) ? 0 - x : x;
}

double fmod(double x, double y) {
    return (x - y * floor(x / y));
}

float fmaf (float x, float y, float z)
{
	return (x * y) + z;
}

double fma (double x, double y, double z)
{
	return (x * y) + z;
}

long double fmal (long double x, long double y, long double z)
{
	return (x * y) + z;
}

float frexpf (float value, int* exp)
{
	*exp = (value == 0) ? 0 : (int) (1 + logbf (value));
	return scalbn (value, -(*exp));
}

double frexp (double value, int* exp)
{
	*exp = (value == 0) ? 0 : (int) (1 + logb (value));
	return scalbn (value, -(*exp));
}

long double frexpl (long double value, int* exp)
{
	*exp = (value == 0) ? 0 : (int) (1 + logbl (value));
	return scalbn (value, -(*exp));
}

float scalbnf (float arg, int exp)
{
	return ldexpf (arg, exp);
}

double scalbn (double arg, int exp)
{
	return ldexp (arg, exp);
}

long double scalbnl (long double arg, int exp)
{
	return ldexpl (arg, exp);
}

float scalblnf (float arg, long exp)
{
	return ldexpll (arg, exp);
}

double scalbln (double arg, long exp)
{
	return ldexpll (arg, exp);
}

long double scalblnl (long double arg, long exp)
{
	return ldexpll (arg, exp);
}





/*
*
* FROM THIS POINT INTO THE FILE ONWARDS, THIS LICENSE IS APPLIED
*
* ====================================================
* Copyright (C) 1993 by Sun Microsystems, Inc. All rights reserved.
*
* Developed at SunPro, a Sun Microsystems, Inc. business.
* Permission to use, copy, modify, and distribute this
* software is freely granted, provided that this notice
* is preserved.
* ====================================================
*/

float logbf (float arg)
{
	return logb(arg);
}

double logb (double arg)
{
	int32_t hx, lx, ix;

	EXTRACT_WORDS (hx, lx, arg);
	hx &= 0x7FFFFFFF;

	if (hx < 0x00100000) {
		if ((hx | lx) == 0) {
			double  xx;
			INSERT_WORDS (xx, hx, lx);
			return -1. / xx;

		} else {
			if (hx == 0) {
				for (ix = -1043; lx > 0; lx <<= 1) ix -= 1;
			} else {
				for (ix = -1022, hx <<= 11; hx > 0; hx <<= 1) ix -= 1;
			}
		}

		return (double) ix;

	} else if (hx < 0x7ff00000) {
		return (hx >> 20) - 1023;

	} else if (hx > 0x7ff00000 || lx) {
		return arg;

	} else {
		return HUGE_VAL;
	}
}

long double logbl (long double arg)
{
	return logb(arg);
}