#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

//! "TE" is the "T"ext "E"ditor for Banana

#define LINE_LENGTH 512

char currentFilename[256];
bool edited;

struct node {
    struct node* next;
    char line[LINE_LENGTH];
};

struct node* current;
unsigned textBufferLength = 0;

void clearInput() {
    char c;
    while ((c = getchar()) != '\n' && c != EOF);
}

bool yesNo (char* prompt) {
    int c;
    do {
        printf("%s (Y/n): ", prompt);
        char bf[512];
        fgets(bf, 511, stdin);
        c = bf[0];

    } while (c != 'y' && c != 'Y' && c != 'n' && c != 'N');

    //clearInput();
    return c == 'Y' || c == 'y';
}

void drawLine() {
    for (int i = 0; i < 80; ++i) {
        putchar('-');
    }
}

void clear() {
    system("cls");
}

void formatFileSize (int size, char* bf) {
    if (size < 1000) {
        sprintf(bf, "%d bytes", size);
    } else if (size < 1000 * 1000) {
        sprintf(bf, "~ %.1f KB(s)", (float) size / 1024.0);
    } else if (size < 1000 * 1000 * 1000) {
        sprintf(bf, "~ %.1f MB(s)", (float) size / 1024.0 / 1024.0);
    } else {
        sprintf(bf, "~ %.1f GB(s)", (float) size / 1024.0 / 1024.0 / 1024.0);
    }
}

void save() {
    FILE* f = fopen(currentFilename, "w");
    if (f == NULL) {
        printf("     Failed to save.\n");
        return;
    }
    struct node* next = current;
    while (next->next) {
        fprintf(f, "%s", next->line);
        next = next->next;
    }

    fclose(f);

    edited = false;
    printf("     Save successful.\n");
}

void drawScreen() {
    clear();
    drawLine();

    printf(" %s%s\n", currentFilename, edited ? " (edited)" : "");
    drawLine();

    char buffer[128];
    formatFileSize(textBufferLength, buffer);
    printf(" Size: %s\n", buffer);
    drawLine();

    printf(" Type command: ");
}

void startEditor(int aimingFor, bool overwrite) {
    struct node* next = current;
    for (int i = 1; i < aimingFor; ++i) {
        printf("i = %d\n", i);
        if (next->next) {
            next = next->next;
        } else {
            printf("     The line could not be found.\n");
            return;
        }
    }

    printf("Type (without quotes) '..~' on its own line to stop typing and save.\n");
    printf("Type (without quotes) '..^' on its own line to stop typing.\n");

    struct node* oldnext = next;
    int lineno = aimingFor - 1;
    while (next->next && lineno < aimingFor - 1 + 3) {
        ++lineno;

        char formattedLine[82 * 8];
        memset(formattedLine, 0, 82 * 8);

        for (int i = 0, j = 0; next->line[i]; ++i, ++j) {
            if (next->line[i] == '\t') {
                int tabLength = 8 - (j % 8);
                memset(formattedLine + j, ' ', tabLength);
                j += tabLength - 1;     //! j is incremented on loop as well, so subtract 1 from tabLength
            } else {
                formattedLine[j] = next->line[i];
            }
        }

        int written = 0;
        while (written < strlen(formattedLine)) {
            char bf[82];
            //! 81 = 80 + null

            if (written == 0) {
                snprintf(bf, 81, "%05d %s", lineno, formattedLine + written);
            } else {
                snprintf(bf, 81, "-->   %s", formattedLine + written);
            }

            printf("%s", bf);
            written += 74;
        }

        next = next->next;
    }

    next = oldnext;

    drawLine();

    while (1) {
        char bf[LINE_LENGTH];
        fgets(bf, LINE_LENGTH - 1, stdin);
        if (!strcmp(bf, "..~\n")) {
            edited = true;
            save();
            break;
        }
        if (!strcmp(bf, "..^\n")) {
            edited = true;
            break;
        }
        strcpy(next->line, bf);
        textBufferLength += strlen(next->line);

        if (next->next && overwrite) {
            next = next->next;
        } else if (next->next) {
            struct node* oldNext = next->next;

            //!create new
            next->next = malloc(sizeof(struct node));
            next = next->next;
            next->next = oldNext;       //! but make the next one the old one
            memset(next->line, 0, LINE_LENGTH - 1);
        } else {
            //! add a new line
            next->next = malloc(sizeof(struct node));
            next = next->next;
            next->next = 0;
            memset(next->line, 0, LINE_LENGTH - 1);
        }
    }
}

void view() {
    clear();
    struct node* next = current;
    int lineno = 0;
    int fakelineno = 0;
    while (next->next) {
        ++lineno;

        char formattedLine[82 * 8];
        memset(formattedLine, 0, 82 * 8);

        for (int i = 0, j = 0; next->line[i]; ++i, ++j) {
            if (next->line[i] == '\t') {
                int tabLength = 8 - (j % 8);
                memset(formattedLine + j, ' ', tabLength);
                j += tabLength - 1;     //! j is incremented on loop as well, so subtract 1 from tabLength
            } else {
                formattedLine[j] = next->line[i];
            }
        }

        int written = 0;
        if (strlen(formattedLine) == 0) {
                --lineno;
        }
        while (written < strlen(formattedLine)) {
            char bf[82];
            //! 81 = 80 + null

            if (written == 0) {
                snprintf(bf, 81, "%05d %s", lineno, formattedLine + written);
            } else {
                snprintf(bf, 81, "-->   %s", formattedLine + written);
            }

            printf("%s", bf);
            written += 74;

            //! C:/users/mord/desktop/write file format.txt

            ++fakelineno;
            if (fakelineno == 23) {
                putchar('\n');
                bool cont = yesNo("There is more text in the document. Continue?");
                if (!cont) {
                    return;
                }
                clear();
                fakelineno = 0;
            }
        }

        next = next->next;
    }
}
void startMainloop() {
    while (1) {
        drawScreen();

        char cmd[256];
        fgets(cmd, 255, stdin);
        drawLine();

        if (!strcmp(cmd, "help\n") || !strcmp(cmd, "h\n")) {
            //! note! these commands take an INTEGER LINE or an ASTERISK (meaning the current line)
            //!       or +INTEGER or -INTEGER (meaning INTEGER relative to the current line)
            //!       this will be used for look, insert, 'j', edit, overwrite, delete, move and copy
            //!       or by using a DEFINED BOOKMARK (using the bookmark command) in the form of
            //!       $name where name is the bookmark name. Relative offsets (e.g. $name+5) also work
            puts("     h help      Prints this message");
            puts("     z size      Prints the exact file size");
            puts("     q quit      Exits the program");
            puts("     s save      Saves the document");
            puts("     o open      Close this file and open or create another");
            puts("     v view      View the file");
            puts("     l look      View the file starting at a certain line (NOT DONE)");
            puts("     * current   Gets the current line and number");
            puts("     [integer]   Sets the current line");
            puts("     i insert    Insert a newline for editing and add new lines after it");
            puts("     j           Insert a newline for editing and overwrite lines after it");
            puts("     e edit      Edit a line and add new lines after it");
            puts("     w overwrite Edit a line and overwrite lines after it");
            puts("\nThere are more commands. Press ENTER to continue");
            clearInput();       //!this will wait for at least one character
            clear();

            puts("     d delete    Deletes a line or range of lines (NOT DONE)");
            puts("     g paragraph Shows the start of each paragraph");
            puts("     m move      Move line(s) to a different location (NOT DONE)");
            puts("     c copy      Copy line(s) to a different location (NOT DONE)");
            puts("     f find      Finds the first line containing the given keyword (NOT DONE)");
            puts("     a again     Finds the next line containing the given keyword (NOT DONE)");
            puts("     r replace   Replaces the given keyword in all lines with another");
            puts("     b bookmark  Sets a bookmark for a line");

            puts("\n     Valid line numbers are:\n         Integers, the asterisk, the caret, bookmarks and an\n         asterisk, caret or bookmark with an offset");
            puts("         The asterisk (*) denotes the current line.\n         Offsets are in the form of +number or -number and\n         are relative to the current line");
            puts("         The caret (^) refers to the last line.\n          It may be offset using ^+number and ^-number");
            puts("         Bookmarks are in the form of $bookmark, and can be offset\n         using $bookmark+number and $bookmark-number");
            puts("\nPress ENTER to continue");
            clearInput();       //!this will wait for at least one character

        } else if (!strcmp(cmd, "size\n") || !strcmp(cmd, "z\n")) {
            printf("    %u bytes exactly (0x%X bytes in hex)\n", textBufferLength, textBufferLength);
            printf("    ~ %.6f KB(s)\n", (float) textBufferLength / 1024.0);
            printf("    ~ %.6f MB(s)\n", (float) textBufferLength / 1024.0 / 1024.0);
            printf("    ~ %.6f GB(s)\n", (float) textBufferLength / 1024.0 / 1024.0 / 1024.0);

            puts("\nPress ENTER to continue");
            clearInput();       //!this will wait for at least one character

        } else if (!strcmp(cmd, "quit\n") || !strcmp(cmd, "q\n")) {
            bool quit = true;
            if (edited) {
                quit = yesNo("     There are unsaved changes.\n     Quit?");
            }
            if (quit) {
                exit(0);
            }

        } else if (!strcmp(cmd, "open\n") || !strcmp(cmd, "o\n")) {
            bool quit = true;
            if (edited) {
                quit = yesNo("     There are unsaved changes.\n     Open?");
            }
            if (quit) {
                clear();
                return;
            }
        } else if (!strcmp(cmd, "save\n") || !strcmp(cmd, "s\n")) {
            save();
            puts("\nPress ENTER to continue");
            clearInput();       //!this will wait for at least one character

        } else if (!strcmp(cmd, "view\n") || !strcmp(cmd, "v\n")) {
            view();

            puts("\nEnd of document. Press ENTER to continue");
            clearInput();       //!this will wait for at least one character

        } else if (!strncmp(cmd, "edit ", 5) || !strncmp(cmd, "e ", 2) || \
                   !strncmp(cmd, "overwrite ", 10) || !strncmp(cmd, "w ", 2)) {

            bool overwrite = (!strncmp(cmd, "overwrite ", 10) || !strncmp(cmd, "w ", 2));

            startEditor(5, overwrite);
        }
    }
}

int main(int argc, char* argv[])
{
    printf("te - Banana Text Editor\n\n");

	FILE* f = fopen ("C:/test.txt", "r");
	char buffer[64];
	fgets (buffer, 16, f);
	fclose (f);

	printf ("The data was: %s\n", buffer);

    edited = false;
    memset(currentFilename, 0, 256);

    while (1) {
        bool y = yesNo("Load file?");
        printf("Filename: ");
        fgets(currentFilename, 255, stdin);
        currentFilename[strlen(currentFilename) - 1] = 0;       //! get rid of newline

        if (currentFilename[0] == 0) continue;

        current = malloc(sizeof(struct node));
        current->next = 0;
        memset(current->line, 0, LINE_LENGTH - 1);

        if (y) {
			printf ("About to open file\n");
            FILE* f = fopen(currentFilename, "r");
            if (f == NULL) {
                printf("File open error.\n\n");
                continue;
            }
			printf ("Opened!\n");
            struct node* next = current;
            while ((size_t)fgets(next->line, LINE_LENGTH, f) != EOF /*NULL*/) {
                textBufferLength += strlen(next->line);
                next->next = malloc(sizeof(struct node));
                next = next->next;
                next->next = 0;
				printf ("Huh.\n");
            }
            fclose(f);
        } else {
            edited = true;
            bool set = yesNo("Set initial content?");
            if (set) {
                startEditor(1, false);
            }
        }

        startMainloop();
    }

    return 0;
}
