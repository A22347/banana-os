//
//  setjmp.h
//  Banana libc
//
//  Created by Alex Boxall on 11/7/18.
//  Copyright © 2018 Alex Boxall. All rights reserved.
//

#ifndef setjmp_h
#define setjmp_h

#if defined(__cplusplus)
#define restrict			//fixes some 'fun' errors
extern "C" {
#endif

    
#if defined(__cplusplus)
}
#endif

#endif /* setjmp_h */
