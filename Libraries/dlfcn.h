#ifndef __DLFCN_H__
#define __DLFCN_H__

#ifdef __cplusplus
extern "C" {
#endif

	typedef struct _Dl_info
	{
		const char* dli_fname;
		void* 		dli_fbase;
		const char* dli_sname;
		void*		dli_saddr;
		int			dli_version;
		int			dli_reversed1;
		long		dli_resvered[4];

	} Dl_info;

	int dladdr (void*, Dl_info*);

#ifdef __cplusplus
}
#endif

#endif