//
//  directsysfileops.c
//  Banana libc
//
//  Created by Alex Boxall on 12/7/18.
//  Copyright © 2018 Alex Boxall. All rights reserved.
//

#ifndef _libk

#include "stdio.h"
#include "syscall.h"

int remove(const char *filename) {
    return __i_system_call_unlink(filename);
}

int rename(const char *old, const char *new_) {
    return __i_system_call_rename(old, new_);
}

#endif
