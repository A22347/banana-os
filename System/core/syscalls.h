#pragma once

#include <core/syscalldef.h>
#include <core/log.h>
#include <hal/power.h>
#include <core/cmdline.h>
#include <core/exeload.h>
#include <hal/timer.h>

extern int (*system_calls[0xFFF]) (struct regs *r);

/*
WARNING! NEVER RETURN AN ARRAY FROM A SYSCALL AS IT WILL GO OUT OF SCOPE!
*/

void install_system_call (int a, int (*b) (struct regs *r));
void setupSystemCalls ();
void system_call (struct regs *r);