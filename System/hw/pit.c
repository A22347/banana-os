#include <hw/pit.h>

void unsetupPIT ()
{
	uninstallHardwareInterruptHandler (0);
	uninstallHardwareInterruptHandler (legacyIRQRemaps[0]);
}

void setupPIT (int hz)
{
	int divisor = 1193180 / hz;
	outb (0x43, 0x36);
	outb (0x40, divisor & 0xFF);
	outb (0x40, divisor >> 8);

	installHardwareInterruptHandler (0, timer_handler);
	installHardwareInterruptHandler (legacyIRQRemaps[0], timer_handler);
}