#ifndef __SIGNAL_H__
#define __SIGNAL_H__

#ifdef __cplusplus
extern "C" {
#endif

#include <sys/types.h>
#include <pthread.h>

	typedef int sig_atomic_t;
	typedef unsigned int sigset_t;

	typedef union sigval {
		int    sival_int;    //Integer signal value.
		void  *sival_ptr;    //Pointer signal value.
	} sigval;

	typedef struct sigevent
	{
		int                     sigev_notify;            //Notification type.
		int                     sigev_signo;             //Signal number.
		union sigval            sigev_value;             //Signal value.
		void (*sigev_notify_function)(union sigval);     //Notification function.
		pthread_attr_t* sigev_notify_attributes;		 //Notification attributes.

	} sigevent;


#define SIGABRT 6
#define SIGALRM 14
#define SIGBUS  16
#define SIGCHLD 17
#define SIGCONT 18
#define SIGFPE  19
#define SIGHUP  1
#define SIGILL  20
#define SIGINT  2
#define SIGKILL 9
#define SIGPIPE 21
#define SIGPOLL 22
#define SIGPROF 23
#define SIGQUIT 3
#define SIGSEGV 24
#define SIGSTOP 25
#define SIGSYS  26
#define SIGTERM 15
#define SIGTRAP 5
#define SIGTSTP 27
#define SIGTTIN 28
#define SIGTTOU 29
#define SIGUSR1 30
#define SIGUSR2 31
#define SIGURG  31
#define SIGVTALRM 32
#define SIGXCPU 33
#define SIGXFSZ 34

#define SIG_DFL 200
#define SIG_IGN 201
#define SIG_ERR 202

	extern void (*__signalHandlers[256])(int);
	void (*signal (int sig, void (*func)(int)))(int);
	int raise (int sig);

#ifdef __cplusplus
}
#endif

#endif