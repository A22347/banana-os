//
//  Font.hpp
//  GUI
//
//  Created by Alex Boxall on 24/6/19.
//  Copyright © 2019 Alex Boxall. All rights reserved.
//

#ifndef Font_hpp
#define Font_hpp

#include "Object.hpp"

class Font : public Object {
private:
protected:
    double size;
    char* name;
    
    uint8_t* fontData;
    
public:
    bool bold = false;
    bool underline = false;
    bool italic = false;
    
    Font (double size);         //the system font
    Font (char* fontname, double size = 12.0);
    
    void loadNewDataFromFile (const char* filename);
    void loadNewDataFromName (const char* fontname);
    void setSize (double size);
    double getSize ();
    
    Font* copy();
    const char* debugName();
};

#endif /* Font_hpp */
