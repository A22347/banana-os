#pragma once

#include <stdint.h>
#include <stdbool.h>

#define MAX_SCANCODE 0x1000

//these are the internal keycodes

#define KEYCODE_BACKSPACE '\b'	// 0x08
#define KEYCODE_TAB		  '\t'	// 0x09
#define KEYCODE_NEWLINE   '\n'	// 0x0A

//normal characters in here...

#define KEYCODE_LEFT  0xF03
#define KEYCODE_RIGHT 0xF04
#define KEYCODE_UP    0xF05
#define KEYCODE_DOWN  0xF06

#define KEYCODE_F1  0xF80
#define KEYCODE_F2  0xF81
#define KEYCODE_F3  0xF82
#define KEYCODE_F4  0xF83
#define KEYCODE_F5  0xF84
#define KEYCODE_F6  0xF85
#define KEYCODE_F7  0xF86
#define KEYCODE_F8  0xF87
#define KEYCODE_F9  0xF88
#define KEYCODE_F10 0xF89
#define KEYCODE_F11 0xF8A
#define KEYCODE_F12 0xF8B
#define KEYCODE_F13 0xF8C
#define KEYCODE_F14 0xF8D
#define KEYCODE_F15 0xF8E
#define KEYCODE_F16 0xF8F

#define KEYCODE_CAPSLOCK   0xF90
#define KEYCODE_NUMBERLOCK 0xF91
#define KEYCODE_SCROLLLOCK 0xF92
#define KEYCODE_KANA	   0xF93
#define KEYCODE_COMPOSE    0xF94

#define KEYCODE_CTRL  0xFA0
#define KEYCODE_ALT   0xFA1
#define KEYCODE_SHIFT 0xFA2
#define KEYCODE_GUI	  0xFA3
#define KEYCODE_RIGHT_CTRL  0xFB0
#define KEYCODE_RIGHT_ALT   0xFB1
#define KEYCODE_RIGHT_SHIFT 0xFB2
#define KEYCODE_RIGHT_GUI	0xFB3

void sendKey (uint16_t kiScancode, bool release);
extern bool keysPressed[MAX_SCANCODE];

extern uint16_t kbdmap[MAX_SCANCODE];

extern uint8_t repeatDelay;
extern uint8_t repeatRate;

void loadKeyboardSettings ();