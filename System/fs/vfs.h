#pragma once

#include <stdbool.h>
#include <stdint.h>
#include "D:\Users\Alex\Desktop\banana-os\Libraries\newlib-3.0.0\newlib\libc\sys\banana\sys\dirent.h"

extern char currentDrive;

#define UNKNOWN_FILESYSTEM 0x7FFFFFFF

enum vfs_status_codes
{
	vfs_status_success,
	vfs_status_disk_err,
	vfs_status_no_filesystem,
	vfs_status_bad_argument,
	vfs_status_eof,

	//and more... (start with ones from this page: http://elm-chan.org/fsw/ff/doc/rc.html#ok )


	vfs_status_SOMETHING_ELSE
};

enum vfs_file_modes
{
	vfs_file_mode_read = 1,
	vfs_file_mode_write = 2,
	vfs_file_mode_append = 4,
};

typedef struct vfs_file_t
{
	char __filename[256];

	void* pointer;
	char driveLetter;

	bool initialisedAndValid;
	bool error;
	bool eof;

} vfs_file_t;

typedef struct vfs_dir_t
{
	char __filename[256];

	void* pointer;
	char driveLetter;

	bool initialisedAndValid;
	bool error;
	bool eof;

} vfs_dir_t;

typedef struct filesystem_driver_function_struct
{
	int (*mount) (char);

	int (*open) (char, void**, char*, uint8_t);					//returns a specific filesystem structure in the 'void*'
	int (*close) (void*);
	int (*read) (void*, uint8_t*, uint64_t, uint64_t*);
	int (*write) (void*, uint8_t*, uint64_t);
	int (*seek) (void*, uint64_t);
	int (*truncate) (void*);
	int (*sync) (void*);
	int (*tell) (void*, uint64_t*);
	int (*size) (void*, uint64_t*);
	int (*error) (void*, bool*);
	int (*eof) (void*, bool*);

	int (*unlink) (char*);
	int (*rmdir) (char*);

	int (*opendir) (char, void**, char*);
	int (*closedir) (void*);
	int (*readdir) (void* directory, struct dirent* out);

	int (*mkdir) (char, char* path);

	int (*exists) (char*, bool*);

} filesystem_driver_function_struct;

void vfs_init();
void vfs_reinit();
int vfs_install_filesystem(filesystem_driver_function_struct funcs);

int vfs_mount(char driveLetter);

int	vfs_open(vfs_file_t* file, char* filename, uint8_t mode);
int	vfs_close(vfs_file_t* file);
int vfs_read(vfs_file_t* file, uint8_t* buffer, uint64_t bytes, uint64_t* actualRead);
int vfs_write(vfs_file_t* file, uint8_t* buffer, uint64_t bytes);
int vfs_seek(vfs_file_t* file, uint64_t bytes);
int vfs_truncate(vfs_file_t* file);
int vfs_sync(vfs_file_t* file);
int vfs_tell(vfs_file_t* file, uint64_t* output);
int vfs_size(vfs_file_t* file, uint64_t* output);
int vfs_error(vfs_file_t* file, bool* err);
int vfs_eof(vfs_file_t* file, bool* eof);

int vfs_opendir(vfs_dir_t* dir, char* dirname);
int vfs_closedir(vfs_dir_t* dir);
int vfs_readdir(vfs_dir_t* dir, struct dirent* out);

int vfs_exists(char* filename, bool* exists);

int vfs_gets(char* buffer, int size, vfs_file_t* file);

int vfs_stat();
int vfs_unlink(char* filename);
int vfs_rmdir(char* filename);
int vfs_rename(char* old, char* newn);
int vfs_chomd();
int vfs_utime();
int vfs_mkdir(char* path);
int vfs_chdir(char* dirname);				//this MUST not use FS specific things, it is a pure VFS abstraction (even though FatFs has it's own, don't use it)
int vfs_chdrive(char driveletter);			//this MUST not use FS specific things, it is a pure VFS abstraction (even though FatFs has it's own, don't use it)
int vfs_getdrive(char* drive);				//this MUST not use FS specific things, it is a pure VFS abstraction (even though FatFs has it's own, don't use it)
int vfs_getcwd(char* c, int len);			//this MUST not use FS specific things, it is a pure VFS abstraction (even though FatFs has it's own, don't use it)
int vfs_getfree();
