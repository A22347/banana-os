#include <Banana/File.hpp>
#define	FA_READ				0x01
#define	FA_WRITE			0x02
#define	FA_OPEN_EXISTING	0x00
#define	FA_CREATE_NEW		0x04
#define	FA_CREATE_ALWAYS	0x08
#define	FA_OPEN_ALWAYS		0x10
#define	FA_OPEN_APPEND		0x30

void CreateNewFile (char* location, char* placeholderName, char* extension)
{
	SystemCall (SC_CreateDuplicationProofFile_LoadExtension, 0, 0, extension);
	SystemCall (SC_CreateDuplicationProofFile_LoadName, 0, 0, placeholderName);
	SystemCall (SC_CreateDuplicationProofFile_LoadLocation, 0, 0, location);
	SystemCall (SC_CreateDuplicationProofFile, 0, 0, 0);
}

/**
\brief Returns the location of the user's music folder.
*/
char* UserMusic() {
    char* user = (char*) SystemCall(SC_Username, 0, 0, 0);
    char* r = (char*) malloc(512);
    strcpy(r, "C:/Users/");
    strcat(r, user);
    strcat(r, "/Music");
    return r;
}

/**
\brief Returns the location of the user's trash bin.
*/
char* UserTrash() {
    char* user = (char*) SystemCall(SC_Username, 0, 0, 0);
    char* r = (char*) malloc(512);
    strcpy(r, "C:/Users/");
    strcat(r, user);
    strcat(r, "/Trash");
    return r;
}

/**
\brief Returns the location of the user's folder.
*/
char* UserFolder() {
    char* user = (char*) SystemCall(SC_Username, 0, 0, 0);
    char* r = (char*) malloc(512);
    strcpy(r, "C:/Users/");
    strcat(r, user);
    return r;
}

/**
\brief Returns the location of the user's desktop.
*/
char* UserDesktop() {
    char* user = (char*) SystemCall(SC_Username, 0, 0, 0);
    char* r = (char*) malloc(512);
    strcpy(r, "C:/Users/");
    strcat(r, user);
    strcat(r, "/Desktop");
    return r;
}

/**
\brief Returns the location of the user's music folder.
*/
char* UserPictures() {
    char* user = (char*) SystemCall(SC_Username, 0, 0, 0);
    char* r = (char*) malloc(512);
    strcpy(r, "C:/Users/");
    strcat(r, user);
    strcat(r, "/Pictures");
    return r;
}

/**
\brief Returns the location of the user's document folder.
*/
char* UserDocuments() {
    char* user = (char*) SystemCall(SC_Username, 0, 0, 0);
    char* r = (char*) malloc(512);
    strcpy(r, "C:/Users/");
    strcat(r, user);
    strcat(r, "/Documents");
    return r;
}

File::File (char* filename, char* mode) {
    strcpy(_filename, filename);
    int flags = 0;
    switch (mode[0]) {
        case 'r':
            flags = FA_READ;
            break;
        case 'w':
            flags = FA_CREATE_ALWAYS | FA_WRITE;
            break;
        case 'a':
            flags = FA_OPEN_APPEND | FA_WRITE;
            break;
    }
    number = SystemCall(SC_NewFileOpen, 0, flags, (void*) filename);
    if (number != -1) {
        valid = true;
    } else {
        valid = false;
    }
}

/**
\brief Closes the file if it is still open
*/
File::~File () {
    if (!valid) return;
    close();
}

char* File::read () {
    int length = SystemCall(SC_NewFileStat, 0, 0, (void*) _filename);
    if (length == -1) {
        valid = false;
        return 0;
    }
    char* buffer = (char*) malloc(length);
    SystemCall(SC_NewFileRead, length, number, (void*) buffer);
    return buffer;
}

void File::write (char* data) {
    if (!valid) return;
    SystemCall(SC_NewFileWrite, strlen(data), number, (void*) data);
}

int File::stat () {
    return SystemCall(SC_NewFileStat, 0, 0, (void*) _filename);
}

char* File::readline () {
    char* bf = (char*) malloc(12000);
    int err = SystemCall (SC_NewFileReadLine, 12000, number, (void*) bf);
    if (err) {
        memcpy(bf, EOF, 12);
    }
    return bf;
}

void File::erase () {
    if (!valid) return;
    valid = false;
    
    SystemCall(SC_NewFileClose, 0, number, 0);
    SystemCall(SC_NewFileDelete, 0, 0, (void*) _filename);
}

void File::close () {
    if (!valid) return;
    valid = false;
    SystemCall(SC_NewFileClose, 0, number, 0);
}