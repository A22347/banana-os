#include <core/scheduler.h>
#include <hal/timer.h>			//totalMillisecs
#include <core/paging.h>
#include <devss/devices.h>

void yieldFromKernel ();

void abortTask (int returnValue)
{
	if (TASKS[TASK_Current].console) {
		//InnerDelete (TASKS[TASK_Current].consoleWindow);
	}

	logk("TASK %d GETTING ABORTED CODE %d\n", TASK_Current, returnValue);

	TASKS[TASK_Current].processHalt = true;
	TASKS[TASK_Current].threads[0].halt = true;
	TASKS[TASK_Current].threads[0].returnValue = returnValue;

	if (TASKS[TASK_Current].driver) {
		TASKS[TASK_Current].driverDevice->returned = true;
		TASKS[TASK_Current].driverDevice->returnValue = returnValue;
	}
	
	//free all of the pages
	switchToKernelTableIfNot ();
	for (int i = 0; i < 16 * 1024 * 1024 / 4096 + (512 * 8) / 4096; ++i) {
		freePhysicalPage ((uint64_t) (size_t) TASKS[TASK_Current].addressSpace + (uint64_t) i * 4096);
	}
	logk("Task aborted\n");
}

void nanoSleepTaskUntil (uint64_t nanoSinceBoot)
{
	TASKS[TASK_Current].threads[0].waitingForMillisecSinceBoot = nanoSinceBoot / 1000 / 1000;
	logk("Forever looping? A\n");

	yieldFromKernel ();
}

void nanoSleepTask (uint64_t nano)
{
	nanoSleepTaskUntil (totalMillisecs * 1000 * 1000 + nano);
}

void sleepTask (uint64_t seconds)
{
	nanoSleepTask (seconds * 1000 * 1000 * 1000);
}

int driverPIDs[256];
int driverCurrent = 0;

void addDriverToList (int pid)
{
	for (int i = 0; i < 256; ++i) {
		if (driverPIDs[i] == 0) {
			driverPIDs[i] = pid;
		}
	}
}

void removeDriverFromList (int pid)
{
	for (int i = 0; i < 256; ++i) {
		if (driverPIDs[i] == pid) {
			driverPIDs[i] = 0;
		}
	}
}

char* storedPath;
char** storedArgs;
char** storedEnvr;
int storedPID = 0;
int spawnResult = 0;

bool storedTaskChild = 0;
int storedPIDTask = 0;
char storedPathAddTask[256];

int storedFork = 0;

void storeFork()
{
	storedFork = TASK_Current;
}

void storeAddTask(char* path)
{
	if (storedPIDTask) {
		logk("TOO MANY SPAWNS AT ONCE!\n");
	}
	strcpy(storedPathAddTask, path);
	storedPIDTask = TASK_Current;
	storedTaskChild = 0;
}

void storeAddTaskAsChild(char* path)
{
	if (storedPIDTask) {
		logk("TOO MANY SPAWNS AT ONCE!\n");
	}
	strcpy(storedPathAddTask, path);
	storedPIDTask = TASK_Current;
	storedTaskChild = 1;
	logk("storeAddTaskAsChild - TASK_Current = 0x%X, storedPIDTask = 0x%X\n", TASK_Current, storedPIDTask);
}

void storeExecve(char* path, char** args, char** envr)
{
	if (storedPID) {
		logk("TOO MANY EXECS AT ONCE!\n");
	}
	storedPID = TASK_Current;
	storedPath = path;
	storedArgs = args;
	storedEnvr = envr;
	for (int i = 0; args[i]; ++i) {
		logk("storeExecve arg %d: %s\n", i, args[i]);
	}
}

void restoreExecve()
{
	if (storedPID == 0) return;
	newAddTaskWithProcessID(storedPath, storedPID);
	TASKS[storedPID].numberOfArguments = 0;
	for (int i = 0; storedArgs[i]; ++i) {
		logk("restoreExecve arg %d: %s\n", i, storedArgs[i]);
		strcpy(TASKS[storedPID].arguments[i], storedArgs[i]);
		TASKS[storedPID].numberOfArguments++;
	}
	storedPID = 0;
}

void restoreAddTask()
{
	if (storedPIDTask == 0) {
		return;
	}
	bool res = addTask(storedPathAddTask);
	logk("Restoring add task. Path = %s. stored PID = 0x%X, TASK_Next - 1 = 0x%X\n", storedPathAddTask, storedPIDTask, TASK_Next - 1);
	if (storedTaskChild) createTaskFamily(storedPIDTask, TASK_Next - 1);

	if (res) {
		spawnResult = TASK_Next - 1;
	} else {
		spawnResult = 0;
	}

	storedPIDTask = 0;
}

void restoreFork()
{
	if (storedFork == 0) return;

	logk("RESTORING FORK\n");

	addTask(TASKS[storedFork].filename);						//create
	size_t* vas = TASKS[TASK_Next - 1].addressSpace;			//store virtual address space
	logk("CREATED FORK\n");

	TASKS[TASK_Next - 1] = TASKS[storedFork];						//copy things across
	logk("COPIED FORK\n");

	strcpy(TASKS[TASK_Next - 1].threads[0].cwd, TASKS[storedFork].threads[0].cwd);
	logk("COPIED FORK P2\n");

	TASKS[TASK_Next - 1].addressSpace = vas;					//restore virtual address space
	logk("RESTORED VAS OF FORK\n");

	copyAddressSpace(TASKS[TASK_Next - 1].addressSpace, TASKS[storedFork].addressSpace);
	logk("COPIED VAS\n");

	TASKS[storedFork].forkReturnVal = TASK_Next;				//give parent the child's PID
	TASKS[TASK_Next - 1].forkReturnVal = 0;							//give child a 0
	logk("SET VALUES FORK\n");

	storedFork = 0;
	logk("END FORK\n");
}

bool taskIsReady()
{
	if (TASKS[TASK_Current].driver) {
		return false;
	}
	if (TASKS[TASK_Current].threads[0].currentlyExecuting) {
		return false;
	}
	if (TASKS[TASK_Current].threads[0].wait) {
		int w = TASKS[TASK_Current].threads[0].wait;
		if (TASKS[w].processHalt) {
			TASKS[TASK_Current].threads[0].wait = 0;
		} else {
			return false;
		}
	}

	if (TASKS[TASK_Current].threads[0].waitingForMillisecSinceBoot < totalMillisecs) {
		//logk("waitingForMillisecSinceBoot.\n");
		//return false;
	}

	if (TASKS[TASK_Current].processHalt || TASKS[TASK_Current].threads[0].halt || !TASK_Current) {
		return false;
	}

	return true;
}

//all this needs to do is find a new value for TASK_Current 
void findNextTask ()
{
	TASKS[PIDofEachCPU[getCPUNumber ()]].threads[0].currentlyExecuting = false;
	PIDofEachCPU[getCPUNumber ()] = 0;

	switchToKernelTableIfNot();
	restoreFork();
	restoreExecve();		//process any exec()s 
	restoreAddTask();		//adds any new tasks

	uint8_t time = 0;

	/*
	TODO:
		Drivers now work like this:
		They get ran when they start (ie. exeuteDrive(someDiskDriver, 1 sector,  put it here, start here))
		This adds a new driver task, even if a driver of the same type is running.

		They are added to a special queue of driver PIDs to be executed next time it is time to findNextTask.
		One from the list is executed on the first call, then the second, etc.,  much like how normal tasks are scheduled.

		When a driver calls a special system call, it is taken off the list. It does this system call when it has been setup.

		Then drivers do not get exeuted any more unless they are responding to an interrupt. In this case, they are re-added to the list,
		and taken off again when they call the system call.

		When they are finished (however they determine that),  they exit and return data to the called and the task is halted.


		THIS MEANS DRIVER TASKS ARE NOT ADDED WHEN THEY ARE FOUND. DRIVER TASKS ARE ADDED WHEN THEY ARE NEEDED.
	*/

	bool driverFound = false;
	for (int i = 0; i < 256; ++i) {
		if (driverPIDs[driverCurrent]) {
			TASK_Current = driverPIDs[driverCurrent++];
			driverFound = true;
			break;
		}
		if (driverCurrent > 255) {
			driverCurrent = 0;
		}
	}

	if (!driverFound) {
		while (1) {
			++TASK_Current;
			if (TASK_Current >= TASK_Next) {
				TASK_Current = 1;
			}
			if (taskIsReady()) {
				break;
			}
		}
	}
	
	TASKS[TASK_Current].threads[0].currentlyExecuting = true;
	PIDofEachCPU[getCPUNumber ()] = TASK_Current;
}