#include <Banana/ListBox.hpp>
#include <Banana/Core.hpp>
#define CHECK_BIT(var,pos) ((var) & (1<<(pos)))
    
ListBox::ListBox (Window& parent, int _x, int _y, int _height, int _width) : Widget (&parent) {
    masterwindow = &parent;
    masterwindow->addWidgetChild(this);
    
    x = _x;
    y = _y;
    clickedIndex = -1;
    numberOfItems = 0;
    scrolledIndex = 0;
    focus = false;
    height = _height;
    width = _width;
    
    root = (Node*) malloc (sizeof(Node));
    root -> next = 0;
    current = root;

	int32_t* ptr = (int32_t*) malloc (20);
	*(ptr + 0) = getWindowNumber();
	*(ptr + 1) = x;
	*(ptr + 2) = y;
	*(ptr + 3) = width;
	*(ptr + 4) = height;
	SystemCall (SC_RenderTextGraphic, 0, 0, ptr);

    _write();
}

ListBox::~ListBox () {
    //we should really free all of our stuff here
}


void ListBox::_write () {
    forceWindowToUpdate ();
    
    current = root;
    for (int oo = scrolledIndex; oo < (numberOfItems < height ? numberOfItems : height + scrolledIndex); ++oo) {
        char* text = current -> text;
        
        uint16_t pixelPointer = x + 5;
        uint8_t fontno = SystemCall(SC_GetFontNumberFromName, 0, 0, (void*) "SYSTEM");

        for (int i = 0; text[i]; ++i) {
            uint8_t width = APISystemCall(ASCGetCharacterWidth, text[i], fontno, 0);
            masterwindow->drawCharacter(text[i], pixelPointer, y + oo * 16 + 4, (clickedIndex == oo) ? 0x000080 : 0xFFFFFF, 0, fontno);
            pixelPointer += width;
        }
        
        current = current -> next;
    }
}

void ListBox::selfupdate (Event_t event) {
	int cx = SystemCall (SC_WindowPositionX, getWindowNumber (), 0, 0);
	int cy = SystemCall (SC_WindowPositionY, getWindowNumber (), 0, 0);

	if (event.type == EventKeyPress) {
		int xx = event.mouseXPosition - cx;
		int yy = event.mouseYPosition - cy;
		if (yy >= y && yy <= y + height && xx >= x && xx <= x + width) {
			int yindex = yy - y;		//y index into widget
			yindex -= 4;				//border
			yindex /= 16;				//row spacing
			yindex += scrolledIndex;	//compensate for scrolling
			clickedIndex = yindex;
			_write ();

		} else {
			//if not clicked inside the widget, 
			clickedIndex = -1;
			_write ();
		}
	} else if (event.type == EventNull) {
		_write ();
	}
}

void ListBox::update () {
    _write();
	Event_t e;
	e.type = EventNull;
    masterwindow->_update(e);
}

void ListBox::_clearScr ()
{
	current = root;
	for (int oo = scrolledIndex; oo < (numberOfItems < height ? numberOfItems : height + scrolledIndex); ++oo) {
		char* text = current->text;

		uint16_t pixelPointer = x + 5;
		uint8_t fontno = SystemCall (SC_GetFontNumberFromName, 0, 0, (void*) "SYSTEM");

		for (int i = 0; text[i]; ++i) {
			uint8_t width = APISystemCall (ASCGetCharacterWidth, text[i], fontno, 0);
			masterwindow->drawCharacter (text[i], pixelPointer, y + oo * 16 + 4, 0xFFFFFF, 0xFFFFFF, fontno);
			pixelPointer += width;
		}

		current = current->next;
	}
}

void ListBox::clear () {
	_clearScr ();

    root = (Node*) malloc (sizeof(Node));
    root -> next = 0;
    current = root;
    clickedIndex = -1;
    numberOfItems = 0;
    scrolledIndex = 0;
	
    _write();
}

void ListBox::add (char* text, int position) {
    //we are ignoring position for now, we will always insert at the end
    //usually -1 will represent the last item
    
	_clearScr ();

    current = root;
    while (current -> next) current = current -> next;
    
    current -> text = (char*) malloc (strlen(text));
    strcpy(current -> text, text);
    current -> next = (Node*) malloc (sizeof(Node));
    current = current -> next;
    current -> next = 0;
    numberOfItems++;
}
