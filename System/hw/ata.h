#pragma once

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include <devss/devices.h>

	void ataReadSectors (uint8_t deviceNo, uint64_t sectorNumber, uint8_t count, void* buffer);
	void ataWriteSectors (uint8_t deviceNo, uint64_t sectorNumber, uint8_t count, void* buffer);
	void ataInitDevice (uint8_t deviceNo);

	void openATA(Device* self, int b, int c, void* d);
	void readATA(Device* self, int b, int c, void* d);
	void writeATA(Device* self, int b, int c, void* d);

#ifdef __cplusplus
}
#endif