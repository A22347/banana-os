debugSectorNumber dd 0

selectedSpaceLBA dd 0
selectedSpaceSIZE dd 0

HDDNumber db 0x80

pressEnter db " PRESS ENTER", 0

debuggingTitle db "SECTOR: ", 0
debugInfo db "    The debug menu lets you access the hard drive's contents.", 0xA, 0xD, "    Press ENTER to begin.", 0xA, 0xA, 0xD, "    The LEFT and RIGHT arrow keys on your keyboard will allow you to scroll", 0xA, 0xD, "    through the sectors on the hard drive.", 0

FirstHDDMsg db 0xD , " The first hard drive will be used.  (recommended)   ", 0
SecondHDDMsg db 0xD, " The second hard drive will be used.                 ", 0
ThirdHDDMsg db 0xD , " The third hard drive will be used.                  ", 0
FourthHDDMsg db 0xD, " The fourth hard drive will be used.                 ", 0

Bracket db ")", 0
ErrorMessageCDROM db "An error occoured reading from the installation CD-ROM.", 0xA, 0xD, "The contents of the AH register is displayed in dots between the brackets below.", 0xA, 0xA, 0xD, "(", 0
ErrorMessageHarddriveB db "An error occoured writing to the hard drive. (B)", 0xA, 0xD, "The contents of the AH register is displayed in dots between the brackets below.", 0xA, 0xA, 0xD, "(", 0
ErrorMessageHarddriveA db "An error occoured writing to the hard drive. (A)", 0xA, 0xD, "The contents of the AH register is displayed in dots between the brackets below.", 0xA, 0xA, 0xD, "(", 0
ErrorMessageReset db "An error occoured while getting the disk ready.", 0xA, 0xD, "The contents of the AH register is displayed in dots between the brackets below.", 0xA, 0xA, 0xD, "(", 0

lineReset db 0xD, 0
Stage5Msg db "    Banana will now be installed onto your hard drive.", 0xA, 0xA, 0xD, "    THIS WILL ERASE ALL DATA FROM THE SELECTED PARTITION AND IT ", 0xA, 0xD, "    CANNOT BE RECOVERED.", 0xA, 0xA, 0xD, "    To continue, press ENTER. To exit Setup, press ESC.", 0xA, 0xD, "    To change partition, press BACKSPACE.", 0xA, 0xA, 0xD, 0
BailOut_NoHardDriveMsg: db "    No hard drive. Please insert and run Setup again.", 0xA, 0xA, 0xD, 0
BailOut_NoFloppyDriveMsg: db "    Hard drive doesn't work. Please replace and run Setup again.", 0xA, 0xA, 0xD, 0
BailOut_BadHardDriveSizeMsg: db "    Hard drive too small.", 0xA, 0xA, 0xd, 0
BailOut_NotEnoughRAMMsg: db "    Not enough random access memory (RAM).", 0xA, 0xA, 0xd, 0
BailOut_Msg: db " Press ESC to exit Setup.", 0
BailOut_BadCPUMsg: db "    The CPU does not support Banana.", 0xA, 0xA, 0xd, 0


debugTitle db 0xA,     " Debug Menu", 0xA, 0xD, " ", 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, \
			205, 205, 0xA, 0xA, 0xD, 0
donetitle db 0xA,     " Installation Finished", 0xA, 0xD, " ", 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, \
			205, 205, 0xA, 0xA, 0xD, 0

donemsg db "    Banana has been successfully installed onto your hard drive.", 0xA, 0xA, 0xD, "    Thank you for chosing Banana 1.0.", 0xA, 0xA, 0xA, 0xA, 0xD, "    You may now eject the installation CD.", 0xA, 0xA, 0xD, "    Press ENTER to restart your computer and start Banana.", 0

license1 db 0xA, " License Agreement", 0xA, 0xD, " ", 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, \
			205, 205, 0xA, 0xA, 0xD, \
"    Please read the following license agreement carefully. It is 5 pages long.", 0xA, 0xD, \
"    Press the PAGE DOWN and UP keys switch between the document's pages.", 0xA, 0xA, 0xA, 0xD, \
"    BANANA OPERATING SYSTEM AND RELATED COMPONENTS", 0xA, 0xD,\
"    END USER LICENSE AGREEMENT", 0xA, 0xA, 0xD,\
"    PLEASE READ THE FOLLOWING LICENSE CAREFULLY BEFORE USING THIS SOFTWARE.", 0xA, 0xD,\
"    BY USING THIS SOFTWARE, YOU AGREE TO BE BOUND BY THE TERMS OF THIS LICENSE.", 0xA, 0xD,\
'    This End User License Agreement ("EULA") is a legal agreement between (a)', 0xA, 0xD,\
'    you (either as an individual or a single entity) and (b) Alex Boxall ', 0xA, 0xD,\
'    ("AB"), which governs the use the Banana Operating System and any related', 0xA, 0xD,\
'    software components, installation media and the data recorded on it ', 0xA, 0xD,\
'    (excluding the installation program itself, but including any', 0xA, 0xD,\
'    related electronic files that it may or may not install), documentation and', 0xA, 0xD,\
'    electronic files (the "Software"). The Software is licensed, not sold to ', 0xA, 0xD,\
'    you by AB for use only under the terms of this EULA. AB reserves all rights', 0xA, 0xD,\
'    not explicitly granted to you by this EULA. ', 0xA, 0xA, 0xD, \
"    ", 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, "  PAGE 1 / 5  ", 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 0

license2 db 0xA, " License Agreement", 0xA, 0xD, " ", 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, \
			205, 205, 0xA, 0xA, 0xD, \
'    BY AGREEING TO THIS LICENSE, OR BY INSTALLING, COPYING, OR ', 0xA, 0xD,\
'    OTHERWISE USING THE THE SOFTWARE YOU AGREE TO BE BOUND BY THE THE TERMS OF', 0xA, 0xD,\
'    THIS EULA. IF YOU DO NOT AGREE TO THE TERMS OF THIS EULA, YOU MAY NOT USE ', 0xA, 0xD,\
'    OR INSTALL THE SOFTWARE. YOU MAY, HOWEVER, RETURN THE SOFTWARE TO YOUR ', 0xA, 0xD,\
'    PLACE OF PURCHASE FOR A FULL REFUND.', 0xA, 0xA, 0xD, \
'    1. LIMITED LICENSE. This EULA grants you a revocable, non-exclusive, ', 0xA, 0xD,\
'    limited license to use the Software as follows: You may install, use, ', 0xA, 0xD,\
'    access and display one copy of the Software on a single computer or device.', 0xA, 0xD,\
'    To "use" this software means that the software is loaded into temporary ', 0xA, 0xD,\
'    memory (i.e., RAM) of a computer or installed on the permanent memory of a ', 0xA, 0xD,\
'    computer (i.e., hard disk, etc.). You may make one backup copy of the ', 0xA, 0xD,\
'    Software for backup and archival purposes only. The Software is licensed,', 0xA, 0xD,\
'    not sold.', 0xA, 0xD, 0xA, \
'    2. RESTRICTIONS ON USE. You may not remove or alter any of the copyright ', 0xA, 0xD,\
'    notices on any copies of the Software. You may not distribute copies of the', 0xA, 0xD,\
'    Software to third parties. You may not sell or copy this software. This ', 0xA, 0xD,\
0xA, "    ", 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, "  PAGE 2 / 5  ", 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 0

license3 db 0xA, " License Agreement", 0xA, 0xD, " ", 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, \
			205, 205, 0xA, 0xA, 0xD, \
'    EULA does not grant any rights in connection with any trademarks or service', 0xA, 0xD,\
'    marks of AB. You may not reverse engineer, decompile, or disassemble the ', 0xA, 0xD,\
'    Software, except and only to the extent that such activity is expressly ', 0xA, 0xD,\
'    permitted by applicable law notwithstanding this limitation. You may not ', 0xA, 0xD,\
'    rent, lease, or lend the Software. You may permanently transfer all of your', 0xA, 0xD,\
'    rights under this EULA, provided the recipient agrees to the terms of this', 0xA, 0xD,\
'    EULA. You must comply with all applicable laws regarding use of the ', 0xA, 0xD,\
'    Software. You must only install Banana using this installation program.', 0xA, 0xA, 0xD,\
'    3. TERMINATION. This license is perpetual or until you fail to comply with', 0xA, 0xD,\
'    the terms and conditions of this EULA or AB terminates it with or without ', 0xA, 0xD,\
'    cause. In such event, you must destroy all copies of the Software.', 0xA, 0xA, 0xD,\
'    4. COPYRIGHT. All title, including but not limited to copyrights, in and to', 0xA, 0xD,\
'    the Software and any copies thereof are owned by AB. All title and ', 0xA, 0xD,\
'    intellectual property rights in and to the content which may be accessed ', 0xA, 0xD,\
'    through use of the Software is the property of the respective content owner', 0xA, 0xD,\
'    and may be protected by applicable copyright or other intellectual property', 0xA, 0xD,\
0xA, "    ", 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, "  PAGE 3 / 5  ", 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 0

license4 db 0xA, " License Agreement", 0xA, 0xD, " ", 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, \
			205, 205, 0xA, 0xA, 0xD, \
'    laws and treaties. This EULA grants you no rights to use such content. ', 0xA, 0xD,\
'    All rights not expressly granted are reserved by AB.', 0xA, 0xA, 0xD,\
'    5. NO WARRANTIES. THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY', 0xA, 0xD,\
'    KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF ', 0xA, 0xD,\
'    MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. ', 0xA, 0xD,\
'    IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,', 0xA, 0xD,\
'    DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR ', 0xA, 0xD,\
'    OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE', 0xA, 0xD,\
'    USE OR OTHER DEALINGS IN THE SOFTWARE.', 0xA, 0xA, 0xD,\
'    6. LIMITATION OF LIABILITY. TO THE MAXIMUM EXTENT PERMITTED BY APPLICABLE ', 0xA, 0xD,\
'    LAW, IN NO EVENT SHALL AB OR ITS SUPPLIERS BE LIABLE FOR ANY SPECIAL, ', 0xA, 0xD,\
'    INCIDENTAL, INDIRECT, OR CONSEQUENTIAL DAMAGES WHATSOEVER (INCLUDING, ', 0xA, 0xD,\
'    WITHOUT LIMITATION, DAMAGES FOR LOSS OF BUSINESS PROFITS, BUSINESS ', 0xA, 0xD,\
'    INTERRUPTION, LOSS OF BUSINESS INFORMATION, OR ANY OTHER PECUNIARY LOSS) ', 0xA, 0xD,\
'    ARISING OUT OF THE USE OF OR INABILITY TO USE THE SOFTWARE PRODUCT OR THE ', 0xA, 0xD,\
'    FAILURE TO PROVIDE SUPPORT SERVICES, EVEN IF AB HAS BEEN ADVISED OF THE ', 0xA, 0xD,\
0xA, "    ", 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, "  PAGE 4 / 5  ", 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 0

license5 db 0xA, " License Agreement", 0xA, 0xD, " ", 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, \
			205, 205, 0xA, 0xA, 0xD, \
'    POSSIBILITY OF SUCH DAMAGES. IN ANY CASE, AB’S ENTIRE LIABILITY UNDER ANY', 0xA, 0xD,\
'    PROVISION OF THIS EULA SHALL BE LIMITED TO THE THE AMOUNT ACTUALLY PAID BY ', 0xA, 0xD,\
'    YOU FOR THE SOFTWARE PRODUCT. BECAUSE SOME STATES/JURISDICTIONS DO NOT ', 0xA, 0xD,\
'    ALLOW THE EXCLUSION OR LIMITATION OF LIABILITY, THE ABOVE LIMITATION MAY', 0xA, 0xD,\
'    NOT APPLY TO YOU.', 0xA, 0xA, 0xD,\
'    7. EP0618540 PATENT. The Software infringes on patent EP0618540, which ', 0xA, 0xD, \
'    expires on the December 13, 2021. The Software may not be used in Europe', 0xA, 0xD, \
'    while the patent is valid. Blame Microsoft.', 0xA, 0xA, 0xD,\
'    8. If any section of this agreement is found to be unenforceable the ', 0xA, 0xD, \
'    remaining sections still apply.', 0xA, 0xA, 0xD,\
"    If you do not agree to the license, press the ESC key and setup will ", 0xA, 0xD, \
"    close. If you agree to the terms of the license, press the RETURN key.", 0xA, 0xD,\
"    You must sign the agreement in order to install and use Banana.", 0xA, 0xA, 0xD,\
'     -- "Take a gulp and take a breath and go ahead and sign the scroll!" --', 0xA, 0xD,\
0xA, "    ", 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, "  PAGE 5 / 5  ", 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 0

db 0


selectInstallTypeMsg db 0xA, " Select Install Type", 0xA, 0xD, " ", 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, \
			205, 205, 0xA, 0xA, 0xD, "    Please select the installation type by pressing the matching", 0xA, 0xD, "    key on your keyboard.", 0xA, 0xD, 0xA, \
			"    1. Typical Installation (recommended)", 0xA, 0xD, \
			"          - Normal install for standard usage", 0xA, 0xD, \
			"          - Approx. 90 MB install", 0xA, 0xD, 0xA, \
			"    2. Express Installation", 0xA, 0xD, \
			"          - Minimal install for lower end hardware", 0xA, 0xD, \
			"          - Approx. 48 MB install", 0xA, 0xD, 0xA, \
			"    3. Complete Installation", 0xA, 0xD, \
			"          - Complete install of all components", 0xA, 0xD, \
			"          - Approx. 90 MB install", 0xA, 0xD, 0xA, \
			"    4. Custom Installation", 0xA, 0xD, \
			"          - Select the components you want to install", 0xA, 0xD, 0xA, 0

writingfloppymsg db "     Writing files to the floppy disk.", 0xA, 0xD, "     Do not disconnect the power supply...", 0

writinghdd db 0xA, " Writing Files", 0xA, 0xD, " ", 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, \
			205, 205, 0xA, 0xA, 0xD, 0

writinghddmsg db "     Writing files to the partition.", 0xA, 0xD, "     Do not disconnect the power supply or eject the installation CD...", 0

formattingmsg: db "     Formatting partition. Do not disconnect the power supply...", 0
formatting db 0xA, " Formatting Partition", 0xA, 0xD, " ", 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, \
			205, 205, 0xA, 0xA, 0xD, 0

BailedOut: db 0xA, " Setup Error", 0xA, 0xD, " ", 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, \
			205, 205, 0xA, 0xA, 0xD, 0
Title   db 0xA, " Banana 1.0 Setup", 0xA, 0xD, " ", 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, \
			205, 205, 0xA, 0xA, 0xD, 0


Title2   db 0xA, " Scanning System...", 0xA, 0xD, " ", 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, \
			205, 205, 0xA, 0xA, 0xD, 0
Line db 196, 0
Title2End db 0, 0xA, 0xA, 0xD, 0

ProductKeyWaitMsg db 0xA, 0xA, 0xD, "    Please wait while Setup checks the product key.", 0xA, 0xD, "    If this check doesn't finish within a few seconds and Setup ", 0xA, 0xD, "    has appeared to have frozen, your computer does not support Banana.", 0

pkeystart db 0xA, " Product Key", 0xA, 0xD, " ", 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, \
			205, 205, 0xA, 0xA, 0xD, "    Please enter your 20 digit product key.", 0xA, 0xD, "    It is found [where?]", 0xA, 0xA, 0xD, "    Note: You do not need to enter the hyphens.", \
			0xA, 0xA, 0xA, 0xD, "    ", 0
			
pkey db 177,177,177,177,177, " - ",177,177,177,177,177, " - ",177,177,177,177,177, " - ",177,177,177,177,177,0
pkeyend db 0xA, 0xA, 0xA, 0xD, "    Press ENTER to continue or ESC to quit Setup.", 0

Stage102Msg db 0xA, " GUID Partition Table", 0xA, 0xD, " ", 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, \
			205, 205, 0xA, 0xA, 0xD, "    Your computer has a GUID Parition Table (GPT). In order for", 0xA, 0xD, "    Banana to install with a GPT, UEFI support is required.", 0xA, 0xA, 0xD, "    Does this computer support UEFI? (Press one of the keys beside the answer)", 0xA, 0xA, 0xD, "      Y  : Yes, I am CERTAIN does.", 0xA, 0xD, "      N/n: No, it does not.", 0xA, 0xD, "      Q/q: I'm not sure. (recommended)", 0xA, 0xA, 0xD, "    WARNING: If you select 'YES' and the computer does not have support", 0xA, 0xD, "             for UEFI, files on the hard drive may become corrupted.", 0 

partitionSmallMsg db 0xA, " Partition Too Small", 0xA, 0xD, " ", 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, \
			205, 205, 0xA, 0xA, 0xD, "    The selected partition was too small. A partition must be 128 MB ", 0xA, 0xD, "    or greater in order install Banana. Press ENTER to continue.", 0

Stage100MsgWait db 0xA, " Finding Partitions...", 0xA, 0xD, " ", 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, \
			205, 205, 0xA, 0xA, 0xD, "    Please wait while setup searches for partitions and ", 0xA, 0xD, "    unpartitioned spaces on your computer.", 0

Stage100Msg db 0xA, " Select a Partition", 0xA, 0xD, " ", 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, \
			205, 205, 0xA, 0xA, 0xD, "    The following list shows the exisiting partitions and ", 0xA, 0xD, \
			"    unpartitioned space on your computer.", 0xA, 0xA, 0xD, "    Use the UP and DOWN arrow keys to select an item in the list.", 0xA, 0xD, "    Press ENTER to continue or ESC to exit setup.", 0xA, 0xA, 0xD, "    WARNING: Installing Banana will erase all data on the selected partition.", 0xA, 0xA, 0xD, 0

NoBlankPartitionsMessage db 0xA, " No Empty Partitions", 0xA, 0xD, " ", 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, \
			205, 205, 0xA, 0xA, 0xD, "    Only four partitions can exist at one time, so a new partition could", 0xA, 0xD, "    not be allocated. Delete one and try again.", 0xA, 0xA, 0xD, "    Press ENTER to continue.", 0


Stage101Msg db 0xA, " MBR Not Found or Corrupt", 0xA, 0xD, " ", 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, \
			205, 205, 0xA, 0xA, 0xD, "    There is no MBR on your hard drive.", 0xA, 0xD, "    Setup can create one for you if you wish.", 0xA, 0xA, 0xD, "    WARNING: ALL DATA WILL BE ERASED FROM THE PARTITION IF YOU CONTINUE.", 0xA, 0xD, "             IF YOU DO NOT WISH TO DELETE ALL OF YOUR DATA, ", 0xA, 0xD, "             PRESS ESC TO EXIT SETUP.", 0xA, 0xA, 0xA, 0xD, "    Press ENTER to continue or ESC to exit setup.", 0

pointerYes db "    > ", 0
pointerNo  db "      ", 0
times 16 db 0 ;padding

partition1 db "Partition 1     ", 0
times 16 db 0 ;padding

partition2 db "Partition 2     ", 0
times 16 db 0 ;padding

partition3 db "Partition 3     ", 0
times 16 db 0 ;padding

partition4 db "Partition 4     ", 0
times 16 db 0 ;padding

partitionnull db "-", 0xA, 0xD, 0
times 16 db 0 ;padding

partitionbootyes db ": ACTIVE", 0xA, 0xD, 0
times 16 db 0 ;padding

partitionbootno  db "", 0xA, 0xD, 0
times 16 db 0 ;padding

partitionunalloc db "Unpartitioned Space", 0
times 16 db 0 ;padding


percentDoneBlock: db 219, 0
percentLeftBlock: db 177, 0

Stage4Msg db "    Setup is now checking the following areas of your system:", 0xA, 0xA, 0xD, "    ", 0
Stage4MsgP2 db 0xA, 0xA, 0xD, 0
Stage4NumberOfTests dw 6
Stage4NoDone: dw 0

mulresult dd 0
originalECX dd 0

progressProgress db 0
progressCycle db "//--\\||////----\\\\||||////----\\\\||||////----\\\\||||////----\\\\||||////----\\\\||||////----\\\\||||////----\\\\||||////----\\\\||||       "

countA dw 512

progressProgress2 db 0
progressCycle2 db "/-\|/-\|    "

Stage4Footer0: db "     0% complete  ", 0
Stage4Footer1: db "    11% complete  ", 0
Stage4Footer2: db "    22% complete  ", 0
Stage4Footer3: db "    33% complete  ", 0
Stage4Footer4: db "    44% complete  ", 0
Stage4Footer5: db "    56% complete  ", 0
Stage4Footer6: db "    67% complete  ", 0
Stage4Footer7: db "    78% complete  ", 0
Stage4Footer8: db "    89% complete  ", 0
Stage4Footer9: db "   100% complete  ", 0
Stage678Footer db "       Progress:  ", 0

ScannedHDDScanningFloppy dw 0		;0 = none, 1 = scanning/ed floppy for workingness, 2 = scanning/ed floppy for filesystem, 3 = scanning/ed floppy for space

Newline db 0xa, 0xd, 0

FourBlank: db "    ", 0

Stage4Test1	db "Hard drive", 0xA, 0xD, 0
Stage4Test2	db "Processor", 0xA, 0xD, 0
Stage4Test3	db "Drives work", 0xA, 0xD, 0
Stage4Test4 db "File system", 0xA, 0xD, 0
Stage4Test5 db "Free space", 0xA, 0xD, 0
Stage4Test6 db "Random access memory", 0xA, 0xD, 0

ScanProgSymbol db "                 ", 0
ScanDoingSymbol db "            ", 175, "    ", 0
ScanDoneSymbol db "            ", 251, "    ", 0
NullTest db " ",  0xA, 0xD, 0
Dot db ".", 0

TESTS:
	dw (Stage4Test1 - D_OFFSET)
	dw (Stage4Test2 - D_OFFSET)
	dw (Stage4Test3 - D_OFFSET)
	dw (Stage4Test4 - D_OFFSET)
	dw (Stage4Test5 - D_OFFSET)
	dw (Stage4Test6 - D_OFFSET)
	dw NullTest - D_OFFSET

TESTSPERCENT:
	dw (Stage4Footer0 - D_OFFSET)
	dw (Stage4Footer1 - D_OFFSET)
	dw (Stage4Footer2 - D_OFFSET)
	dw (Stage4Footer3 - D_OFFSET)
	dw (Stage4Footer4 - D_OFFSET)
	dw (Stage4Footer5 - D_OFFSET)
	dw (Stage4Footer6 - D_OFFSET)
	dw (Stage4Footer7 - D_OFFSET)
	dw (Stage4Footer8 - D_OFFSET)
	dw (Stage4Footer9 - D_OFFSET)



PartitionTypeEnd db "]", 0
PartitionTypeStart db " [Type: ", 0

PartitionTypeStrings:
db "UNKNOWN", 0		;0
db "FAT12  ", 0		;1
db "XENIX  ", 0		;2
db "XENIX  ", 0		;3
db "FAT16  ", 0		;4
db "DOS 3.3", 0		;5
db "FAT16  ", 0		;6
db "NTFS?  ", 0		;7
db "COMODOR", 0		;8
db "AIX DTA", 0		;9
db "OS/2 BM", 0		;A
db "FAT32  ", 0		;B
db "FAT32  ", 0		;C
db "SILICON", 0		;D
db "FAT16  ", 0		;E
db "FAT16  ", 0		;F

db "OPUS   ", 0		;0
db "H FAT12", 0		;1
db "CONFIG ", 0		;2
db "UNKNOWN", 0		;3
db "H FAT16", 0		;4
db "UNKNOWN", 0		;5
db "H FAT16", 0		;6
db "H IFS  ", 0		;7
db "AST S.S", 0		;8
db "UNKNOWN", 0		;9
db "UNKNOWN", 0		;A
db "H FAT32", 0		;B
db "H FAT32", 0		;C
db "UNKNOWN", 0		;D
db "H FAT16", 0		;E
db "UNKNOWN", 0		;F

times 256 - 16 * 2 db 0

WelcomeP1 db "    Welcome to Setup.", 0xA, 0xA, 0xD, "    The Setup program will install Banana 1.0 on your computer.", 0xA, 0xA, 0xD, "    ", '*', " To set up Banana now, press ENTER.", 0xA, 0xA, 0xD, \
		"    ", '*', " The install will take 10-30 minutes depending on the size", 0xA, 0xD, "      and speed of your hard drive and the speed of your computer.", 0xA, 0xA, 0xD, "    ", '*', \
		" To exit Setup without installing Banana, press ESC.", 0xA, 0xA, 0xD, "    Note: If you have not backed \
up your files recently, you", 0xA, 0xD, "          might want to do so before installing Banana. To back", 0xA, 0xD, 0

WelcomeP2 db "          up your files, press ESC to exit Setup now. Then, back", 0xA, 0xD, \
		"          up your files by using a backup program.", 0xA, 0xA, 0xD, "    To continue with Setup, press ENTER.", 0

Stage3Msg: db 0xA, " Routine Check", 0xA, 0xD, " ", 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, 205, \
			205, 205, 0xA, 0xA, 0xD, "    Setup is now doing to do a routine check of your system to", 0xA, 0xD, "    ensure Banana can run on your computer.", 0xA, 0xA, 0xD, "    Press ENTER to continue or ESC to exit setup.", 0
ZeroedBuffer times 512 * 8 db 0
	

osimageString db "OSIMAGE.SYS", 0
defaultmbrstring db "DFLTMBR.BIN", 0