﻿/*
This is the Banana kernel.
Our motto is: "it might not crash"
*/

#define ___KERNEL_MODE___ 1

#include <core/kernel.h>
#include <core/string.h>
#include <core/memory.h>
#include <core/paging.h>
#include <core/registry.h>
#include <core/registry2.h>
#include <core/fileprotect.h>
#include <core/syscalldef.h>
#include <core/math.h>
#include <core/smp.h>
#include <core/log.h>
#include <core/exeload.h>
#include <core/cmdline.h>
#include <core/syscalls.h>
#include <core/scheduler.h>
#include <core/panic.h>
#include <core/gdt.h>
#include <core/tss.h>

#include <debug/log.h>
#include <debug/ubsan.h>

#include <devss/devices.h>

#include <fs/vfs.h>

//ideally, we only need to include <hal/devices.h>, that should inc everything else
#include <hal/interrupts.h>
#include <hal/diskio.h>
#include <hal/keybrd.h>
#include <hal/clock.h>
#include <hal/floating.h>
#include <hal/mouse.h>
#include <hal/power.h>

#include <util/userctrl.h>

#include <gui/fonts.h>
#include <gui/terminal.h>

bool systemReady = false;
bool filesystemReady = false;
char appOrDriverRunningNameInCaseOfCrash[128] = { 0 };
char panicErrorMessage[128] = { 0 };
volatile uint8_t* volatile otherCPUsMayExecute;
volatile uint8_t* volatile nextCPUToRun;
volatile int xxtr = 0;
bool loginTaskShown = false;
uint16_t ramTableLength = 0;
bool debugMode = false;

bool tasksStarted = false;
bool safeMode = false;

void* irq_routines[24] = {
	0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0,

	// APIC only
	0, 0, 0, 0, 0, 0
};

char timezone[256];
bool plusUTC = false;
uint8_t hours = 0;
bool halfHour = false;

char monthStrings[13][4] = {
	"???",
	"JAN",
	"FEB",
	"MAR",
	"APR",
	"MAY",
	"JUN",
	"JUL",
	"AUG",
	"SEP",
	"OCT",
	"NOV",
	"DEC"
};


volatile int* volatile PIDofEachCPU;
volatile int* volatile letsWait;

bool numberLock = false;
bool scrollLock = false;
uint8_t numberOfFloppies = 0;
uint32_t SYSTEM_PUTCHAR_COLOUR_ = 0;
char username[128];
char userDesktop[128];
char userDocuments[128];
char userFolder[128];
char userBin[128];
char userPhotos[128];
char userMusic[128];
int lastAddedPIDBySystemCommand = 0;

bool readOnlyMode = false;
bool hasAPIC = false;

uint8_t processorID[256];
uint8_t matchingAPICID[256];
uint8_t processorDiscoveryNumber = 0;

//e.g. 0xFEC..., not 0xFEE... (use cpuGetAPICBase () instead)
uint32_t ioapicAddresses[256];
uint8_t ioapicFoundInMADT[256];		//the IDs of IOAPICs found on the system (using the MADT ACPI table)
uint8_t ioapicDiscoveryNumber = 0;	//ioapicFoundInMADT[ioapicDiscoveryNumber++] = id;
uint8_t legacyIRQRemaps[16] = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 };

bool __internal_login_variable__loginComplete = false;
char __internal_login_variable__loginUsername[256] = { 0 };

bool forceDisableAPIC = false;

int64_t freeBytesApprox = 0;
unsigned int LastInt = 0;
int numberOfCPUsUsed = 0;
int mousex = 0;
int mousey = 0;
volatile int TASK_Current = 1;
volatile int TASK_Next = 1;
volatile uint8_t flagForCPUTimingSetByExecutionAssemblyStub = 0;
int lastInt = 0;

unsigned short textptr[MOUSEH] = {
	0b00111011100,
	0b00000100000,
	0b00000100000,
	0b00000100000,
	0b00000100000,
	0b00000100000,
	0b00000100000,
	0b00000100000,
	0b00000100000,
	0b00000100000,
	0b00000100000,
	0b00000100000,
	0b00000100000,
	0b00000100000,
	0b00000100000,
	0b00111011100,
	0, 0, 0,
	0, 0
};

int syscallreturn;

extern void terminal_writestring_colour(const char* data, uint32_t bg, uint32_t fg);

void getFilesystemReady()
{
	logk("Reorder.\n");

	//ensures a HDD is on drive C:, and floppy disks are on A: and B:
	reorderDisks();
	logk("Reorderd.\n"); 
	//this won't work eariler because we haven't discovered the disks (from the builtin drivers)
	vfs_init();
	logk("vfs_inited.\n");

	filesystemReady = true;

	//fixMissingCriticalFiles ();
	//backupCriticalFiles ();
	loadRegistry("C:/Banana/System/system.dat", 0);
}

void setupKernelImportant()
{
	writeStartupText("Setting up GDT");
	asm volatile ("cli");
	setupGDT();
	writeStartupText("Setting up interrupts");
	setupInterrupts();
	asm volatile ("sti");

	freeBytesApprox = MAX_KMALLOC_DATA_LOCATION - KMALLOC_DATA_LOCATION;

	writeStartupText("Setting up devices");
	logk("Setting up devices...\n");
	initDeviceSubsystem();

	vfs_reinit();		//get other disks such as floppy, USB (in the future), etc.

	if (hasAPIC) {
		tssInit(0x400000 - (getCPUNumber() << 19));
	} else {
		tssInit(0x400000);
	}

	writeStartupText("Setting up paging");
	setupPagingForCPU(0);

	vfs_file_t f;
	char bbuf[256];
	uint64_t actual;
	vfs_open(&f, u8"りんご.txt", vfs_file_mode_read);
	vfs_read(&f, (uint8_t*) bbuf, 255, &actual);
	vfs_close(&f);
	logk("%s\n", bbuf);

	vfs_chdrive('C');
	vfs_chdir("C:/");

	extern int terminal_row;
	terminal_row = 0;
}

void loadUserFilepaths()
{
	strcpy(userFolder, "C:/Users/");
	strcat(userFolder, username);

	strcpy(userDocuments, userFolder);
	strcat(userDocuments, "/Documents");

	strcpy(userDesktop, userFolder);
	strcat(userDesktop, "/Desktop");

	strcpy(userBin, userFolder);
	strcat(userBin, "/Trash");

	char regloc[256] = { 0 };
	strcpy(regloc, "C:/Banana/UserData/");
	strcat(regloc, username);
	strcat(regloc, "/user.dat");
	loadRegistry(regloc, 1);
}


void doCoolStuffWithCPU2()
{
	logk("Hello World from CPU %d!\n", getCPUNumber());
	while (1);		//CPU 0 will time out waiting for us, so no infinite loop waiting for this one to finish
}

//used by ASM
uint8_t taskModeActive = 0;
extern uint32_t taskno;

void (*afterExerunPtr) ();
void* stackPointerBeforeExerun;

void setupKernelInterface()
{
	asm volatile ("sti");

	vfs_unlink("C:/Banana/System/SHTDWNOK.sys");

	/*f_rmtree ("C:/Banana/Temporary Files");
	f_mkdir ("C:/Banana/Temporary Files");*/
	writeStartupText("Loading settings");

	loadKeyboardSettings();
	loadMouseSettings();

	/*
	if (hasAPIC) {
		logk ("Init'ing CPUs\n");
		setupOtherCPUs ();
	}*/
	asm volatile ("sti");

	loginTaskShown = true;

	extern int INFO_TO_WRITE_SOMEWHERE_SOON_TIMEZONE;

	char line[256];
	vfs_file_t fil;
	vfs_open(&fil, "C:/BANANA/RESOURCES/TIMEZONES.TXT", vfs_file_mode_read);
	int i = 0;
	while (vfs_gets(line, 255, &fil) == 0) {
		if (i == INFO_TO_WRITE_SOMEWHERE_SOON_TIMEZONE) {
			writeRegistryString("LOCAL/Timezone", line, true);
		}
		++i;
	}

	vfs_file_t f;
	vfs_open(&f, "C:/Banana/BOOTSET.SYS", vfs_file_mode_read);
	uint8_t bf[512];
	uint64_t actual;
	vfs_read(&f, bf, 512, &actual);
	vfs_close(&f);
	logk("bf[0] = %d\n", bf[0]);
	if (bf[0] == 1) {
		bf[0] = 2;

		vfs_file_t ff;
		vfs_open(&ff, "C:/Banana/BOOTSET.SYS", vfs_file_mode_write);
		vfs_write(&ff, bf, 256);
		vfs_close(&ff);

		void firstGoWaitReboot();
		firstGoWaitReboot();
	}

	writeStartupText("Loading login task");

	//get ready to start the login task
	addTask("C:/Banana/System/bnalogin.exe");
	TASK_Current = 1;
	PIDofEachCPU[0] = 1;
	loadTaskFromASM();

	taskModeActive = 1;
	TASKS[TASK_Current].systask = true;

	extern void preMagic();
	extern void exerun();

	//begin 'magical' stuff
	afterExerunPtr = && afterExerun;
	preMagic();
	logk("Calling exerun() in 3..., 2..., 1... !\n");

#if PLATFORM_ID == 86
	writeStartupText("32 bit detected, halting, exerun() will crash it");
	while (1);
#endif

	exerun();

afterExerun:
	;				//C is a weird language, this is required here

	int loc = 0;
	for (int y = 0; y < TOTAL_HEIGHT - 100; ++y) {
		for (int x = 0; x < TOTAL_WIDTH; ++x) {
			putpixelr(loc + x, 0xFFFFFF);
		}
		loc += ONE_LINE;
	}
	for (int y = 0; y < 12; ++y) {
		for (int x = 0; x < TOTAL_WIDTH; ++x) {
			putpixelr(loc + x, 0x0000C0);
		}
		loc += ONE_LINE;
	}
	for (int y = 0; y < 100 - 12; ++y) {
		for (int x = 0; x < TOTAL_WIDTH; ++x) {
			putpixelr(loc + x, 0xC0C0C0);
		}
		loc += ONE_LINE;
	}

	writeStartupText("Setting up TSS");

	if (hasAPIC) {
		tssWriteESP(0x400000 - (getCPUNumber() << 19));
	} else {
		tssWriteESP(0x400000);
	}

	logk("THIS IS A DEBUG MESSAGE 3\n");

	strcpy(username, "Alex");//////
	writeStartupText("Loading user settings");

	loadUserFilepaths();
	loadKeyboardSettings();
	loadMouseSettings();
	loadTimezones();
	setup_settings();		//display settings (e.g. screensaver)
}

void vbeCopyBufferTo(uint32_t * buffer, uint32_t start, uint32_t size, uint8_t displayNo);

extern void __i_signalInit();
extern void __i_localeInit();
extern int errno;

void writeStartupText(char* text)
{
	void writeTextString(int x, int y, char* c, int fg, int bg);
	writeTextString(22, 7, "                           ", 0, 0xF);
	writeTextString(22, 7, text, 0, 0xF);

	int datalen = strlen(text);

	int x = TOTAL_WIDTH / 2 - (datalen * 8) / 2;
	int y = TOTAL_HEIGHT - 100 + 22;

	for (int yy = y; yy < y + 20; ++yy) {
		for (int xx = 0; xx < TOTAL_WIDTH; ++xx) {
			putpixelr(yy * ONE_LINE + xx, 0xC0C0C0);
		}
	}

	for (int i = 0; i < datalen; i++) {
		terminal_putentryat_pixel_raw(text[i], 0, 0xC0C0C0, x, y);
		x += FontWidths[0][(int) text[i]] + 1;
	}


	char t[] = "Copyright (C) 2016-2019 Alex Boxall";
	datalen = strlen(t);
	x = TOTAL_WIDTH / 2 - (datalen * 7) / 2;

	for (int i = 0; i < datalen; i++) {
		terminal_putentryat_pixel_raw(t[i], 0, 0xC0C0C0, x, y + 20);
		x += FontWidths[0][(int) t[i]] + 1;
	}
}

void setupVariables()
{
	//forceDisableAPIC = true;

	//just while we haven't got the AHCI code perfected
	readOnlyMode = 0;

	strcpy(appOrDriverRunningNameInCaseOfCrash, KERNEL_FILENAME);
	toticks = 0;
	TASK_Current = 1;
	TASK_Next = 1;
	mousex = 0;
	mousey = 0;
	realx = 2;
	realy = 2;
	freeBytesApprox = 0;

	heapInit();

	logk("ABOUT TO DRAW TO SCREEN!\n");
	int loc = 0;
	for (int y = 0; y < TOTAL_HEIGHT - 100; ++y) {
		for (int x = 0; x < TOTAL_WIDTH; ++x) {
			putpixelr(loc + x, 0xFFFFFF);
		}
		loc += ONE_LINE;
	}
	for (int y = 0; y < 12; ++y) {
		for (int x = 0; x < TOTAL_WIDTH; ++x) {
			putpixelr(loc + x, 0x0000C0);
		}
		loc += ONE_LINE;
	}
	for (int y = 0; y < 100 - 12; ++y) {
		for (int x = 0; x < TOTAL_WIDTH; ++x) {
			putpixelr(loc + x, 0xC0C0C0);
		}
		loc += ONE_LINE;
	}
	logk("DRAWN!\n");
	__i_localeInit();

	otherCPUsMayExecute = malloc(1);
	*otherCPUsMayExecute = 0;
	xxtr = 0;
	nextCPUToRun = malloc(1);
	*nextCPUToRun = 0;

	PIDofEachCPU = malloc(8 * sizeof(int));
	letsWait = malloc(8 * sizeof(int));
	memset((void*) PIDofEachCPU, 0, 8 * sizeof(int));
	memset((void*) letsWait, 0, 8 * sizeof(int));

	memset(Disks, 0, sizeof(Disks));
	for (int i = 0; i < 26; ++i) {
		Disks[i].device = 0;
	}

	floatingInit();
	memoryManagerInit();

	TASKS = (void*) (size_t) allocatePhysicalPagesTogetherNoSwaps(sizeof(processData_t) * 1024 / 4096 + 1);

	loadbuiltinfonts();
	writeStartupText("Starting Banana");

	logk("CPU MODE = %d\n", PLATFORM_ID);
}

bool hardNoExec[8];

void kernel_main_c()
{
	setupVariables();

	serialInit();			//so VMWare gives correct serial data

	setupKernelImportant();
	asm volatile ("sti");

	setupKernelInterface();

	for (int i = 0; i < 26; ++i) {
		char buffer[256];

		if (Disks[i].type == DiskType_Floppy) {
			sprintf(buffer, "%c: Floppy disk\n", i + 'A');

		} else if (Disks[i].type == DiskType_Nonremovable) {
			sprintf(buffer, "%c: Hard drive\n", i + 'A');

		} else if (Disks[i].type == DiskType_Disc) {
			sprintf(buffer, "%c: CD/DVD\n", i + 'A');

		} else if (Disks[i].type == DiskType_Flash) {
			sprintf(buffer, "%c: Flash drive\n", i + 'A');

		} else if (Disks[i].type == DiskType_Network) {
			sprintf(buffer, "%c: Network drive\n", i + 'A');

		} else if (Disks[i].type == DiskType_Other) {
			sprintf(buffer, "%c: Other type of drive\n", i + 'A');
		} else {
			continue;
		}
	}

	Log("Boot successful.");

	systemReady = true;

	*otherCPUsMayExecute = 1;
	xxtr = 1;

	readRegistryString("BOOT/AutorunFilePath");
	logk("BG0 0x%X\n", readRegistryInteger("DESKTOP/BackgroundColour1"));
	logk("BG1 0x%X\n", readRegistryInteger("DESKTOP/BackgroundColour2"));

	writeStartupText("Starting programs");

	char line[256];
	vfs_file_t file;
	int res = vfs_open(&file, "C:/Banana/System/AUTORUN.TXT", vfs_file_mode_read);
	if (!res) {
		logk("OPEN!\n");
		memset(line, 0, 255);
		while (vfs_gets(line, 255, &file) == 0) {
			if (line[strlen(line) - 1] == '\n') {
				line[strlen(line) - 1] = 0;
			}
			logk("FILE: '%s'\n", line);
			addTask(line);
			line[0] = 0;
		}
		if (line[0]) {
			logk("FILE: '%s'\n", line);
			addTask(line);
		}
		vfs_close(&file);
	} else {
		logk("NOT OPEN!\n");
		addTask("C:/Banana/System/cmd.exe");
	}

	findNextTask();
	loadTaskFromASM();

	taskModeActive = 1;
	taskno = 1;

	extern void stateSwitchJumpPoint();
	stateSwitchJumpPoint();

	panicWithMessage("ERROR_SITUATION");
}
