#pragma once

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>

	int bgaAvaliable ();
	void bgaInit (uint8_t displayno);

	void bgaCopyBufferTo (uint32_t* buffer, uint32_t start, uint32_t size, uint8_t displayNo);
	void bgaCopyBufferFrom (uint32_t* buffer, uint32_t start, uint32_t size, uint8_t displayNo);

	void bgaSetPixel (uint32_t location, uint32_t colour, uint8_t displayNo);
	uint32_t bgaGetPixel (uint32_t location, uint8_t displayNo);

	void bgaSetMode (uint16_t width, uint16_t height, uint8_t bytesPerPixel);

#ifdef __cplusplus
}
#endif