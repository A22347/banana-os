//
//  complex.h
//  Banana libc
//
//  Created by Alex Boxall on 11/7/18.
//  Copyright © 2018 Alex Boxall. All rights reserved.
//

#ifndef complex_h
#define complex_h

// OPTIONAL AS PER THE STANDARD (http://www.open-std.org/jtc1/sc22/wg14/www/docs/n1570.pdf )

#if defined(__cplusplus)
#define restrict			//fixes some 'fun' errors
extern "C" {
#endif
    
    
#if defined(__cplusplus)
}
#endif

#endif /* complex_h */
