//
//  signal.c
//  Banana libc
//
//  Created by Alex Boxall on 12/7/18.
//  Copyright © 2018 Alex Boxall. All rights reserved.
//

#ifndef _libk

#include "syscall.h"
#include "signal.h"
#include "errno.h"
#include "stdlib.h"
#include "string.h"

void (*__i_signals[256])(int);

void __i_signal_dfl_handler (int sig) {
    //take a good look at 'sig'
    //and either call ign or err depending on
    //what it is
    
    __i_signal_err_handler(sig);
}

void __i_signal_err_handler (int sig) {
    // raise error

    if (sig != SIGABRT) {
        abort();
    }
}

void __i_signal_ign_handler (int sig) {
    // ignore signal
}

void (*signal(int sig, void (*func)(int)))(int) {
    if (sig < 256 && sig != SIGKILL) {
        __i_signals[sig] = func;
        return func;
    }
    
    errno = ESIGFAIL;
    return SIG_ERR;
}

int raise(int sig) {
    if (sig == SIGKILL) {
        __i_system_call_abort();
    }
    if (__i_signals[sig]) {
        __i_signals[sig](sig);
        return 0;
    }
    return 1;
}

void __i_signalInit() {
    memset(__i_signals, 0, sizeof(__i_signals));
    
    signal(SIGABRT, SIG_DFL);
    signal(SIGFPE, SIG_DFL);
    signal(SIGILL, SIG_DFL);
    signal(SIGINT, SIG_DFL);
    signal(SIGSEGV, SIG_DFL);
    signal(SIGTERM, SIG_DFL);
    signal(SIGKILL, SIG_DFL);
}

#endif
