#include <hal/keybrd.h>
#include <core/objectmgr.h>
#include <hw/ports.h>
#include <gui/guilink.h>
#include <core/registry.h>
#include <fs/vfs.h>
#include <libk/string.h>
#include <core/memory.h>
#include <core/prcssthr.h>

bool keysPressed[MAX_SCANCODE];

// This is the US keymap, here just in case something goes wrong.
uint16_t kbdmap[MAX_SCANCODE];
bool keysPressed[MAX_SCANCODE];

//these are defaults, the registry will set them correctly
uint8_t repeatDelay = 1;		//0 = 250ms, 1 = 500ms, 2 = 750ms, 3 = 1000ms
uint8_t repeatRate = 12;			//0 = 30Hz ... 31 = 2 Hz

void loadKeyboardSettings ()
{
	//US keyboard and device indepentent scancodes have a 1-to-1 mapping
	for (int i = 0; i < MAX_SCANCODE; ++i) {
		kbdmap[i] = i;
	}

	int fr;
	vfs_file_t f2;
	char location[128] = { 0 };

	RegistryQuery query;
	if (QueryRegistry (&query, "KEYBOARD/KEYMAP") && query.string) {
		strcpy (location, query.returnedString);
	}
	free (query.returnedString);

	logk ("\n%s\n", location);

	fr = vfs_open (&f2, location, vfs_file_mode_read);
	if (fr) {
		logk ("KEYMAP OPEN ERROR!\n");
	}
	uint64_t actual;
	vfs_read(&f2, (uint8_t*) kbdmap, MAX_SCANCODE * 2, &actual);
	vfs_close (&f2);


	if (QueryRegistry (&query, "KEYBOARD/RepeatDelay") && !query.string) {
		if (query.returnedInt <= 0b11) {
			repeatDelay = query.returnedInt;
		}
	}
	free (query.returnedString);

	if (QueryRegistry (&query, "KEYBOARD/RepeatRate") && !query.string) {
		if (query.returnedInt <= 0b11111) {
			repeatRate = query.returnedInt;
		}
	}
	free (query.returnedString);
}


//sent by any keyboard device at all (e.g. PS/2 or USB, both, or multiple USB/PS2, etc.)
void sendKey (uint16_t kiScancode, bool release)
{

	if (kiScancode == '\n') logk ("kiScancode is a newline char.\n");

	//here's the interesting part.
	//the keymaps are based on the device independent scancodes
	//device independent scancodes is based on the US keyboard
	//this means that the US.key contains a 1-to-1 mapping
	//with the device independent scancodes

	kiScancode = kbdmap[kiScancode];

	if (!release && ((kiScancode >= ' ' && kiScancode < 127) || kiScancode == '\t' || kiScancode == '\n')) {
		logk ("GOT SCANCODE %c/0x%X\n", kiScancode, kiScancode);

		for (int i = 0; i < TASK_Next; ++i) {
			logk ("TODO: Fix sending keyboard to terminal (keybrd.c, currently every window gets it, not just the focused, HINT: I don't think TASKS[i].terminal gets set because guiTerminalCreate isn't implemented)\n");
			if (TASKS[i].stdin && !TASKS[i].processHalt && TASKS[i].console/* && TASKS[i].terminal && TASKS[i].terminal->window == FOCUS*/) {
				//MOVE THE BODY OF THIS FUNCTION INTO A GUI FUNCTION
				char* buffer = getStdinInternalBufferFromStdin (TASKS[i].stdin);
				char a[2];
				a[0] = (char) (kiScancode & 0xFF);
				a[1] = 0;
				strcat (buffer, a);
				filePipeStdoutWrite (TASKS[i].stdout, 1, (uint8_t*) a);		//display character
				if (kiScancode == '\n') {
					flushStdinInternalBuffer (TASKS[i].stdin);
				}
				//break;
			}
		}
	}

	if (!release && !keysPressed[kiScancode]) {
		guiReactToKeyboardPress (kiScancode);
	}
	if (!release) {
		logk ("sendKey got key %c, %d\n", kiScancode & 0xFF, kiScancode);
	}
	keysPressed[kiScancode] = release;

	if (release) {
		guiReactToKeyboardUp (kiScancode);
	} else {
		guiReactToKeyboardDown (kiScancode);
	}
}