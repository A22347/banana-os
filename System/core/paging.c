//
// ABANDON ALL HOPE, YE WHO ENTER HERE
//
// Welcome to Banana's memory manager
// This file will contain both the physical memory manager and the virtual memory manager (including the goodness of the swapfile)
// Have fun!
//
// NOTE: The code gets worse and uglier as it goes on...
//

#include <core/kernel.h>
#include <core/paging.h>
#include <core/hwsecurity.h>
#include <core/cpuid.h>
#include <core/prcssthr.h>
#include <debug/log.h>
#include <debug/profile.h>

bool flushpages = true;

void switchToVirtualAddressSpace(size_t* addrSpace)
{
	size_t val;
	asm volatile ("mov %%cr3, %0" : "=r"(val));
	if (val != (size_t) addrSpace) {
		asm volatile ("mov %0, %%cr3" : : "r"(addrSpace));
	}
}

void switchToKernelTableIfNot()
{
	switchToVirtualAddressSpace((size_t*) (size_t) 0xA000);
}

//IN: ADDRESS FORM
void remapPage(uint64_t virtualm, uint64_t physical)
{
	size_t* base = 0;

	if (!hasAPIC || getCPUNumber() == 0) {
		base = (size_t*) PAGING_BASE;
	} else {
		panicWithMessage("CPU_PAGING_NOT_CORRECT");
	}

	base += virtualm / 4096;
	size_t data = *base;
	uint16_t low = data & 0xFFF;            //preserve the configuration bits
	*base = physical | low;

	//asm volatile ("invlpg (%0)" ::"r" (oldspot) : "memory");

	if (flushpages) pagingUpdate();
}

//IN: ADDRESS FORM
void remapPages(uint64_t virtualm, uint64_t physical, uint64_t size)
{
	size += 4095;
	size /= 4096;
	size *= 4096;

	flushpages = false;
	for (uint64_t i = 0; i < size / 4096; ++i) {
		remapPage(virtualm + i * 4096, physical + i * 4096);
	}
	flushpages = true;
	pagingUpdate();
}

//IN: ADDRESS FORM
void giveKernelOnlyAccessTo(uint64_t spot)
{
	size_t* base = 0;

	if (!hasAPIC || getCPUNumber() == 0) {
		base = (size_t*) PAGING_BASE;
	} else {
		panicWithMessage("CPU_PAGING_NOT_CORRECT");
	}

	base += spot / 4096;
	size_t data = *base;
	data &= ~7;
	data |= 3;
	*base = data;
	if (flushpages || safeMode) pagingUpdate();
	//asm volatile ("invlpg (%0)" ::"r" (data & (~0xFFF)) : "memory");
}

//IN: ADDRESS FORM
void giveEveryoneAccessTo(uint64_t spot)
{
	size_t* base = 0;

	if (!hasAPIC || getCPUNumber() == 0) {
		base = (size_t*) PAGING_BASE;
	} else {
		panicWithMessage("CPU_PAGING_NOT_CORRECT");
	}

	base += spot / 4096;
	size_t data = *base;
	data &= ~7;
	data |= 7;
	*base = data;
	if (flushpages || safeMode) pagingUpdate();
	//asm volatile ("invlpg (%0)" ::"r" (data & (~0xFFF)) : "memory");
}

//IN: ADDRESS FORM
void giveKernelOnlyAccessToPages(uint64_t spot, uint64_t size)
{
	size += 4095;
	size /= 4096;
	size *= 4096;

	flushpages = false;
	for (uint64_t i = 0; i < size / 4096; ++i) {
		giveKernelOnlyAccessTo(spot + i * 4096);
	}
	flushpages = true;
	pagingUpdate();
}

//IN: ADDRESS FORM
void giveEveryoneAccessToPages(uint64_t spot, uint64_t size)
{
	size += 4095;
	size /= 4096;
	size *= 4096;

	flushpages = false;
	for (uint64_t i = 0; i < size / 4096; ++i) {
		giveEveryoneAccessTo(spot + i * 4096);
	}
	flushpages = true;
	pagingUpdate();
}

//IN: ADDRESS FORM
void giveNooneAccessTo(uint64_t spot)
{
	size_t* base = 0;

	if (!hasAPIC || getCPUNumber() == 0) {
		base = (size_t*) PAGING_BASE;
	} else {
		panicWithMessage("CPU_PAGING_NOT_CORRECT");
	}

	base += spot / 4096;
	size_t data = *base;
	data &= ~7;
	*base = data;
	if (flushpages || safeMode) pagingUpdate();
	//asm volatile ("invlpg (%0)" ::"r" (data & (~0xFFF)) : "memory");
}

//IN: ADDRESS FORM
void giveNooneAccessToPages(uint64_t spot, uint64_t size)
{
	size += 4095;
	size /= 4096;
	size *= 4096;

	flushpages = false;
	for (uint64_t i = 0; i < size / 4096; ++i) {
		giveNooneAccessTo(spot + i * 4096);
	}
	flushpages = true;
	pagingUpdate();
}

//IN: ADDRESS FORM.
//Sets pages in range spot to spot+size to read only by program and NX.
//Used as a shortcut to setup pages faster
void quickInitPagesRead(uint64_t spot, uint64_t size)
{
	size += 4095;
	size /= 4096;
	size *= 4096;

	size_t* base = 0;

	if (!hasAPIC || getCPUNumber() == 0) {
		base = (size_t*) PAGING_BASE;
	} else {
		panicWithMessage("CPU_PAGING_NOT_CORRECT");
	}

	base += spot / 4096;

	size /= 4096;
	for (uint64_t i = 0; i < size; ++i) {
		*base++ &= ~8;
	}
}

//IN: ADDRESS FORM.
//Sets pages in range spot to spot+size to read only by program and NX.
//Used as a shortcut to setup pages faster
void quickInitPagesNX(uint64_t spot, uint64_t size)
{
	if (!nxAvaliable) {
		return;
	}
	logk("NX avaliable\n");
	size += 4095;
	size /= 4096;
	size *= 4096;

	size_t* base = 0;

	if (!hasAPIC || getCPUNumber() == 0) {
		base = (size_t*) PAGING_BASE;
	} else {
		panicWithMessage("CPU_PAGING_NOT_CORRECT");
	}

	base += spot / 4096;

	size /= 4096;
	for (uint64_t i = 0; i < size; ++i) {
		*base++ |= (1ULL << 63ULL);
	}
}

//IN: ADDRESS FORM
void markAsReadOnlyByProgram(uint64_t spot)
{
	size_t* base = 0;

	if (!hasAPIC || getCPUNumber() == 0) {
		base = (size_t*) PAGING_BASE;
	} else {
		panicWithMessage("CPU_PAGING_NOT_CORRECT");
	}

	base += spot / 4096;
	size_t data = *base;
	data &= ~8;
	*base = data;
	if (flushpages || safeMode) pagingUpdate();
	//asm volatile ("invlpg (%0)" ::"r" (data & (~0xFFF)) : "memory");
}

//IN: ADDRESS FORM
void markAsReadOnlyByProgramToPages(uint64_t spot, uint64_t size)
{
	size += 4095;
	size /= 4096;
	size *= 4096;

	flushpages = false;
	for (uint64_t i = 0; i < size / 4096; ++i) {
		markAsReadOnlyByProgram(spot + i * 4096);
	}
	flushpages = true;
	pagingUpdate();
}

//IN: ADDRESS FORM
void setNXBit(uint64_t spot, bool noExecute)
{
	if (!nxAvaliable) {
		return;
	}

#if PLATFORM_ID == 64
	uint64_t * base = 0;

	if (!hasAPIC || getCPUNumber() == 0) {
		base = (uint64_t*) PAGING_BASE;
	} else {
		panicWithMessage("CPU_PAGING_NOT_CORRECT");
	}

	base += spot / 4096;
	uint64_t data = *base;
	data |= (1ULL << 63ULL);
	*base = data;
	if (flushpages || safeMode) pagingUpdate();
	//asm volatile ("invlpg (%0)" ::"r" (data & (~0xFFF)) : "memory");
#endif
}

//IN: ADDRESS FORM
void setNXBitToPages(uint64_t spot, bool noExecute, uint64_t size)
{
	size += 4095;
	size /= 4096;
	size *= 4096;

	flushpages = false;
	for (uint64_t i = 0; i < size / 4096; ++i) {
		setNXBit(spot + i * 4096, noExecute);
	}
	flushpages = true;
	pagingUpdate();
}

bool allocateMemoryForTask(int processID, int threadID, vfs_file_t * file, size_t size, size_t virtualAddr, size_t additionalNullBytes)
{
	logk("allocateMemoryForTask!\n");
	logk("processID:0x%X size:0x%X virtualAddr:0x%X additionNullBytes:0x%X\n", processID, size, virtualAddr, additionalNullBytes);
	switchToKernelTableIfNot();

	size_t pagesRequired = (size + 4095) / 4096;
	size_t nullPagesReq = (additionalNullBytes + 4095) / 4096;
	size_t virtualPageNo = virtualAddr / 4096;

	logk("A VirtualAddr = 0x%X, virtualPageNo = 0x%X\n", virtualAddr, virtualPageNo);
	unsigned int br = 0;

	for (size_t i = 0; i < pagesRequired; ++i) {
		logk("B VirtualAddr = 0x%X, virtualPageNo = 0x%X, virtualPageNo * 4096 0x%X\n", virtualAddr, virtualPageNo, (size_t) virtualPageNo * 4096);

		//allocate new page
		size_t page = allocatePhysicalPage();
		uint8_t* address = (uint8_t*) page;
		uint64_t* addr64 = (uint64_t*) page;

		uint64_t actual;
		//read from file
		int res = vfs_read(file, address, 4096, &actual);
		if (res) {
			return false;
		}

		//store our physical page
		logk("C VirtualAddr = 0x%X, virtualPageNo = 0x%X, virtualPageNo * 4096 0x%X\n", virtualAddr, virtualPageNo, virtualPageNo * 4096);
		virtualAddressSpaceMapPage((size_t*) (size_t) TASKS[processID].addressSpace, page, (size_t) virtualPageNo * 4096);

		++virtualPageNo;
	}

	//now allocate any extra null bytes
	for (size_t i = 0; i < nullPagesReq; ++i) {
		//allocate new page
		size_t page = allocatePhysicalPage();
		uint8_t* address = (uint8_t*) page;
		uint64_t* addr64 = (uint64_t*) page;

		//clear it
		memset(address, 0, 4096);

		//store our physical page
		virtualAddressSpaceMapPage((size_t*) (size_t) TASKS[processID].addressSpace, page, virtualPageNo * 4096);

		++virtualPageNo;
	}

	logk("Exit Left!\n");
	return true;
}

uint64_t allocatePhysicalPagesTogetherNoSwaps(int numberOfPages)
{
	//returns physical address of new pages

	static uint8_t* scanner = (uint8_t*) (size_t) FRAME_PAGE_ALLOCATOR_INTERNAL_DATA;
	uint8_t* scannerInit = scanner;

	int inARow = 0;
	uint8_t* firstInRow = 0;
	while (1) {
		if (*scanner == 0) {
			if (!inARow) {
				firstInRow = scanner;
			}
			++inARow;

			if (inARow == numberOfPages) {
				size_t s = (size_t) firstInRow;
				s -= FRAME_PAGE_ALLOCATOR_INTERNAL_DATA;    //bytes into the internal data
				s *= 4096;                                    //actual location offset
				s += FRAME_PAGE_ALLOCATOR_START;            //actual location pointing to

				if (validMemory((uint8_t*) s)) {            //ensure memory is valid
					scanner = firstInRow;
					for (int i = 0; i < numberOfPages; ++i) {
						*scanner++ = 1;                                //mark pages as used
					}

					return s;
				}
			}

		} else {
			inARow = 0;
		}
		scanner++;
		size_t s = (size_t) scanner;
		s -= FRAME_PAGE_ALLOCATOR_INTERNAL_DATA;    //bytes into the internal data
		s *= 4096;                                    //actual location offset
		s += FRAME_PAGE_ALLOCATOR_START;            //actual location pointing to
		if (s == 0xFFFFF000) {
			//if we're on the last page, go back to the beginning
			scanner = (uint8_t*) (size_t) FRAME_PAGE_ALLOCATOR_INTERNAL_DATA;
			inARow = 0;
		}

		if (scanner == scannerInit) {
			//no pages left, swap out a physical page to the disk
			//and load a new page off the disk
			inARow = 0;
			panicWithMessage("OUT_OF_MEMORY_(SWAPPING_NOT_AVALIABLE)");
		}
	}

	return 0;
}

void allocateSharedMemory(int pid1, int pid2, int pages, uint64_t * outAddressForPID1, uint64_t * outAddressForPID2)
{
	logk("ALLOCATING SHARED MEMORY AT 0x%X (for pid1) 0x%X (for pid2)\n", TASKS[pid1].highestUnallocedSharedAddress, TASKS[pid2].highestUnallocedSharedAddress);
	switchToKernelTableIfNot();

	uint64_t physAddr = allocatePhysicalPagesTogetherNoSwaps(pages);

	*outAddressForPID1 = TASKS[pid1].highestUnallocedSharedAddress;
	*outAddressForPID2 = TASKS[pid2].highestUnallocedSharedAddress;

	for (int i = 0; i < pages; ++i) {
		virtualAddressSpaceMapPage((size_t*) (size_t) TASKS[pid1].addressSpace, physAddr + i * 4096, TASKS[pid1].highestUnallocedSharedAddress);
		TASKS[pid1].highestUnallocedSharedAddress += 4096;

		virtualAddressSpaceMapPage((size_t*) (size_t) TASKS[pid2].addressSpace, physAddr + i * 4096, TASKS[pid2].highestUnallocedSharedAddress);
		TASKS[pid2].highestUnallocedSharedAddress += 4096;
	}
}

//OUT: PHYSICAL ADDRESS FORM
uint64_t allocatePhysicalPage()
{
	//returns physical address of a new page

	static uint8_t* scanner = (uint8_t*) (size_t) FRAME_PAGE_ALLOCATOR_INTERNAL_DATA;
	uint8_t* scannerInit = scanner;

	while (1) {
		if (*scanner == 0) {
			size_t s = (size_t) scanner;
			s -= FRAME_PAGE_ALLOCATOR_INTERNAL_DATA;    //bytes into the internal data
			s *= 4096;                                    //actual location offset
			s += FRAME_PAGE_ALLOCATOR_START;            //actual location pointing to

			if (validMemory((uint8_t*) s)) {            //ensure memory is valid
				*scanner = 1;                            //mark page as used

				return s;
			}
		}
		scanner++;
		size_t s = (size_t) scanner;
		s -= FRAME_PAGE_ALLOCATOR_INTERNAL_DATA;    //bytes into the internal data
		s *= 4096;                                    //actual location offset
		s += FRAME_PAGE_ALLOCATOR_START;            //actual location pointing to
		if (s == 0xFFFFF000) {
			//if we're on the last page, go back to the beginning
			scanner = (uint8_t*) (size_t) FRAME_PAGE_ALLOCATOR_INTERNAL_DATA;
		}

		if (scanner == scannerInit) {
			//no pages left, swap out a physical page to the disk
			//and load a new page off the disk
			panicWithMessage("OUT_OF_MEMORY_(SWAPPING_NOT_AVALIABLE)");
		}
	}

	return 0;
}

//IN: PHYSICAL ADDRESS FORM
void freePhysicalPage(uint64_t physicalPage)
{
	size_t s = physicalPage;
	s -= FRAME_PAGE_ALLOCATOR_START;
	s /= 4096;                                    //bytes into the internal data
	s += FRAME_PAGE_ALLOCATOR_INTERNAL_DATA;    //actual location

	uint8_t* ptr = (uint8_t*) s;
	*ptr = 0;                                    //free page
}

//IN : VIRTUAL ADDRESS FORM
//OUT: PHYSICAL ADDRESS FORM
uint64_t virtualToPhysical(uint64_t virtualPage)
{
	size_t* pb;

	if (!hasAPIC || getCPUNumber() == 0) {
		pb = (size_t*) PAGING_BASE;
	} else {
		panicWithMessage("CPU_PAGING_NOT_CORRECT");
	}

	size_t* base = (size_t*) pb;
	//one would think that we must divide virtualPage by 8 to remove the effects of using a
	//8 bytes pointer, but it turns out you don't. This is because the actual entries are
	//8 byte in the first place, so 8 byte indexing is correct
	base += virtualPage / 4096;
	return (*base)& (~0xFFF);
}

//OUT: PHYSICAL PAGE FORM
uint32_t pageFinder = 0;
uint32_t findBestPageToSwapOut()
{
	if (pageFinder == 0) {
		pageFinder = FRAME_PAGE_ALLOCATOR_START / 4096;
	}

	//TODO: make this so it doesn't swap out kernel marked pages allocated with allocatePhysicalPagesTogetherNoSwaps

	return pageFinder++;
}

void memoryManagerInit()
{
	//say all pages are free
	memset((void*) (size_t) FRAME_PAGE_ALLOCATOR_INTERNAL_DATA, 0, FRAME_PAGE_ALLOCATOR_INTERNAL_DATA_SIZE);
}

size_t* createVirtualAddressSpaceFull()
{
	logk("Creating full address space.\n");
	//THIS IS THE 64 BIT. IT WILL NOT WORK WITH 32 BIT SYSTEMS YET.

#if PLATFORM_ID == 64
	//creates a new virtual address space and identity maps it
	//only works for the first 4GB

	//bytes 0              = root table
	//bytes 512*8          = 2nd table
	//bytes 512*8+0x400000 = 3rd table
	//bytes 512*8+0x800000 = 4th table
	
	uint64_t * addrSpace = (uint64_t*) (size_t) allocatePhysicalPagesTogetherNoSwaps(11 * 1024 * 1024 / 4096 + (512 * 8 * 2) / 4096);

	uint64_t * firstTable = addrSpace;
	uint64_t * secondTable = addrSpace + 512;        //+ 512 = +512*8 because of 8 byte pointers

	for (int i = 0; i < 512; ++i) {
		*firstTable = ((uint64_t) (size_t) secondTable) | 7;        //mark as present
		++firstTable;
		++secondTable;
	}

	firstTable = addrSpace;
	secondTable = addrSpace + 512;        //+ 512 = +512*8 because of 8 byte pointers
	uint64_t* thirdTable = addrSpace + 512 + 512;        //divide 8 'cause of 8 byte pointer math
	uint64_t * fourthTable = addrSpace + 512 + 512 + (0x200000 / 8);

	for (int i = 0; i < 512; ++i) {
		*secondTable = ((uint64_t) (size_t) thirdTable) | 7;

		for (int j = 0; j < 512; ++j) {
			*thirdTable = ((uint64_t) (size_t) fourthTable) | 7;
			fourthTable += 4096 / 8;
			++thirdTable;
		}

		++secondTable;
	}

	fourthTable = addrSpace + 512 + 512 + (0x200000 / 8);
	uint64_t location = 0;

	for (int i = 0; i < 0x100000; ++i) {            //this number (0x100000) is (0xFFFFFFFF + 1) / 4096
		*fourthTable = location | 3;                //read only for the program is 3

		if (nxAvaliable && i > INTERNAL_MALLOC_LOCATION / 4096) {
			*fourthTable |= (1ULL << 63ULL);                        //make non-kernel pages non-executable if avaliable
		}

		++fourthTable;
		location += 4096;
	}

	return (size_t*) addrSpace;

#elif PLATFORM_ID == 86

	//creates a new virtual address space and identity maps it
	//only works for the first 4GB

	//bytes 0              = root table
	//bytes 1024*4         = 2nd table

	uint32_t * addrSpace = (uint32_t*) (size_t) allocatePhysicalPagesTogetherNoSwaps(5 * 1024 * 1024 / 4096 + (512 * 8) / 4096);

	uint32_t * firstTable = addrSpace;
	uint32_t * secondTable = addrSpace + 1024;        //+ 512 = +512*8 because of 8 byte pointers

	for (int i = 0; i < 1024; ++i) {
		*firstTable = ((uint64_t) (size_t) secondTable) | 7;        //mark as present
		++firstTable;
		secondTable += 1024;
	}

	secondTable = addrSpace + 1024;        //+ 512 = +512*8 because of 8 byte pointers
	uint64_t location = 0;

	for (int i = 0; i < 1024 * 1024; ++i) {
		*secondTable = ((uint64_t) (size_t) location) | 3;        //mark as present
		++secondTable;
		location += 4096;
	}

	return (size_t*) addrSpace;

#else
#error "Invalid system type. That means no paging."
#endif
}

size_t* defaultAppPageTable = 0;
void createDefaultAppPageTable()
{
	defaultAppPageTable = createVirtualAddressSpaceFull();
}

void setupPagingForCPU(uint32_t cpuno)
{

	logk("Setting up paging\n");

	enableHardwareSecuritySystems();
	logk("enabledHardwareSecuritySystems\n");

	//untested

	quickInitPagesRead(ROOT_PAGE_LOCATION / 0x1000, (0xFFFFFFFF - ROOT_PAGE_LOCATION) / 0x1000);
	quickInitPagesNX(INTERNAL_MALLOC_LOCATION / 0x1000, (0xFFFFFFFF - INTERNAL_MALLOC_LOCATION) / 0x1000);
	pagingUpdate();

	defaultAppPageTable = 0;
	createDefaultAppPageTable();
	logk("default app page table is at 0x%X\n", defaultAppPageTable);
	//while (1);

}

size_t * createVirtualAddressSpace()
{
	if (defaultAppPageTable == 0) {
		return createVirtualAddressSpaceFull();
	}

	logk("default app page table is at 0x%X\n", defaultAppPageTable);


	//THIS IS THE 64 BIT. IT WILL NOT WORK WITH 32 BIT SYSTEMS YET.

#if PLATFORM_ID == 64
	//creates a new virtual address space and identity maps it
	//only works for the first 4GB

	//bytes 0              = root table
	//bytes 512*8          = 2nd table
	//bytes 512*8+0x400000 = 3rd table
	//bytes 512*8+0x800000 = 4th table

	uint64_t * addrSpace = (uint64_t*) (size_t) allocatePhysicalPagesTogetherNoSwaps(11 * 1024 * 1024 / 4096 + (512 * 8 * 2) / 4096);

	uint64_t * firstTable = addrSpace;
	uint64_t * secondTable = addrSpace + 512;        //+ 512 = +512*8 because of 8 byte pointers

	for (int i = 0; i < 512; ++i) {
		*firstTable = ((uint64_t) (size_t) secondTable) | 7;        //mark as present
		++firstTable;
		++secondTable;
	}

	firstTable = addrSpace;
	secondTable = addrSpace + 512;        //+ 512 = +512*8 because of 8 byte pointers
	uint64_t* thirdTable = addrSpace + 512 + 512;        //divide 8 'cause of 8 byte pointer math
	uint64_t* fourthTable = addrSpace + 512 + 512 + (0x200000 / 8);
	uint64_t* fourthTable2 = defaultAppPageTable + 512 + 512 + (0x200000 / 8);

	for (int i = 0; i < 512; ++i) {
		*secondTable = ((uint64_t) (size_t) thirdTable) | 7;

		for (int j = 0; j < 512; ++j) {
			*thirdTable = ((uint64_t) (size_t) fourthTable) | 7;
			fourthTable += 4096 / 8;
			fourthTable2 += 4096 / 8;
			++thirdTable;
		}

		++secondTable;
	}

	fourthTable = addrSpace + 512 + 512 + (0x200000 / 8);
	uint64_t location = 0;

	for (int i = 0; i < 0x100000; ++i) {            //this number (0x100000) is (0xFFFFFFFF + 1) / 4096
		*fourthTable = location | 3;                //read only for the program is 3

		if (nxAvaliable && i > INTERNAL_MALLOC_LOCATION / 4096) {
			*fourthTable |= (1ULL << 63ULL);                        //make non-kernel pages non-executable if avaliable
		}

		++fourthTable;
		location += 4096;
	}

	return (size_t*) addrSpace;

#elif PLATFORM_ID == 86

	//creates a new virtual address space and identity maps it
	//only works for the first 4GB

	//bytes 0              = root table
	//bytes 1024*4         = 2nd table

	uint32_t * addrSpace = (uint32_t*) (size_t) allocatePhysicalPagesTogetherNoSwaps(5 * 1024 * 1024 / 4096 + (512 * 8) / 4096);

	uint32_t * firstTable = addrSpace;
	uint32_t * secondTable = addrSpace + 1024;        //+ 512 = +512*8 because of 8 byte pointers

	for (int i = 0; i < 1024; ++i) {
		*firstTable = ((uint64_t) (size_t) secondTable) | 7;        //mark as present
		++firstTable;
		secondTable += 1024;
	}

	secondTable = addrSpace + 1024;        //+ 512 = +512*8 because of 8 byte pointers
	uint64_t location = 0;

	for (int i = 0; i < 1024 * 1024; ++i) {
		*secondTable = ((uint64_t) (size_t) location) | 3;        //mark as present
		++secondTable;
		location += 4096;
	}

	return (size_t*) addrSpace;

#else
#error "Invalid system type. That means no paging."
#endif
}

//IN: address form
void virtualAddressSpaceMapPage(size_t * addrSpace, uint64_t physicalPage, size_t virtualPage)
{
	//switchToKernelTableIfNot ();        //to save time, this is put at the start of allocateMemoryForTask

#if PLATFORM_ID == 64
	addrSpace += 512 + 512;
	addrSpace += (0x200000 / 8);        // div 8 because of 8 byte ptrs, get to last table
	addrSpace += virtualPage / 4096;        //no div 8 because the entries we want to get to are 8 bytes long each, and we have an offset in entry numbers, not byte numbers

	uint16_t low = *addrSpace & 0xFF;
	*addrSpace = physicalPage | low | 7; // OR 7 makes it present and usermode writable
	*addrSpace &= ~(1ULL << 63ULL);

#elif PLATFORM_ID == 86
	addrSpace += 1024;
	addrSpace += virtualPage / 4096;        //no div 4 because the entries we want to get to are 4 bytes long each, and we have an offset in entry numbers, not byte numbers

	uint16_t low = *addrSpace & 0xFF;
	*addrSpace = physicalPage | low | 7; // OR 7 makes it present and usermode writable

#else
#error "Invalid system type. That means no paging."
#endif
}

void copyAddressSpace(size_t * dest, size_t * src)
{
#if PLATFORM_ID == 64
	size_t * src4thTable = src + 512 + 0x800000 / sizeof(size_t);
	size_t* dest4thTable = dest + 512 + 0x800000 / sizeof(size_t);

	//copy all 4GBs
	//for (uint64_t i = 0; i < 1024 * 1024 * 1024 / 4096 * 4; ++i) {
	for (uint64_t i = 0; i < 0x20000000 / 4096; ++i) {
		if ((((*src4thTable) & 0xFFF) & 0b101) != 0b101) {    //not a userspace page
			++dest4thTable;
			++src4thTable;
			continue;
		}

		uint64_t newpage = allocatePhysicalPage();
		memcpy((void*) (size_t) newpage, (void*) (size_t) ((*src4thTable) & ~0xFFF), 4096);        //copy data into new page
		logk("0x%X 0x%X\n", newpage, i * 4096);
		virtualAddressSpaceMapPage(dest, newpage, i * 4096);

		logk("Data from 0x%X is going to 0x%X which corrisponds to virtual address 0x%X\n", (size_t) ((*src4thTable) & ~0xFFF), newpage, i * 4096);
		//while (1);

		++dest4thTable;
		++src4thTable;
	}
#else
	logk("Cannot 'copyAddressSpace' on 32 bit yet.\n");
#endif
}