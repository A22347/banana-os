#pragma once

#include <core/kernel.h>
#include <gui/terminal.h>

extern unsigned int realx;
extern unsigned int realy;
extern uint32_t under[MOUSEH][MOUSEW];
extern unsigned int mousexfirst;
extern unsigned int mouseyfirst;

extern int doubleClickTickRate;
extern bool leftHandedMouse;
extern int mspeed;
extern int tick;
extern double mouseSpeedX;
extern double mouseSpeedY;
extern long long tickWhenBackButtonLastClicked;
extern uint8_t mousemoveloopno;

//see this: https://rahulsharma49.deviantart.com/art/Windows-8-Cursor-For-Windows-7-Vista-XP-309283353
//see the bottom of this page: http://www.javascripter.net/faq/stylesc.htm
enum mousetype
{
	CursorNormal,
	CursorText,
	CursorWaiting,
	CursorResizeBottomRight,
	CursorResizeBottomLeft,
	CursorResizeTopRight,			//by default this is the same as CursorResizeBottomLeft
	CursorResizeTopLeft,			//by default this is the same as CursorResizeBottomRight
	CursorResizeHorizontal,
	CursorResizeVertical,
	CursorResize4Directions,
	CursorHand,
	CursorWorking,					//normal arrow + hourglass			
	CursorAlt,						//arrow pointing right
	CursorHelp,						//normal arrow + ? mark
	CursorPrecison,					//M$ Excel pointer
	CursorUp,
	CursorDown,						//by default the same as CursorUp
	CursorLeft,
	CursorRight,					//by default the same as CursorDown
	CursorUnavaliable,				//circle with a diagnal line through it (NW to SW)
	CursorNoDrop,					//hand with CursorUnavaliable next to it
	CursorPen,						//a pen
	CursorVerticalText,
	CursorCrosshair,
	CursorColResize,
	CursorRowResize,		
	Cursor___ALWAYS_LAST_			//used to determine the length of the enum
};

extern enum mousetype MouseType;

extern uint16_t mouseOutlines[Cursor___ALWAYS_LAST_][MOUSEH];
extern uint16_t mouseCells[Cursor___ALWAYS_LAST_][MOUSEH];
extern char mouseRegistryLookupStrings[Cursor___ALWAYS_LAST_][32];

#define USE_NORMAL_CURSOR  MouseType = CursorNormal;
#define USE_WAITING_CURSOR MouseType = CursorWaiting;


void restoreMouse ();
void mouse_move (int xM, int yM);
void storeMouse ();
void drawMouse ();

void loadMouseSettings ();