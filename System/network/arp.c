#include <network/arp.h>
#include <libk/string.h>
#include <debug/log.h>
#include <network/ethernet.h>
#include <core/memory.h>

ARPCache arpCache[1024];
int nextARPCache = 0;

ARPPacketIPV4 createARPPacket(uint16_t operation, uint8_t* smac, uint32_t sourceIP, uint8_t* dmac, uint32_t destIP)
{
	ARPPacketIPV4 packet;
	packet.htype = 0x0001;
	packet.ptype = 0x0800;
	packet.hlen = 6;
	packet.plen = 4;
	packet.opcode = operation;

	memcpy(packet.smac, smac, 0);
	memcpy(packet.dmac, dmac, 0);

	packet.sip = sourceIP;
	packet.dip = destIP;

	return packet;
}

ARPPacketIPV4 createARPRequestPacket(uint8_t* smac, uint32_t sourceIP, uint32_t destIP)
{
	uint8_t dummy[6] = { 0 };
	return createARPPacket(ARP_REQUEST, smac, sourceIP, dummy, destIP);
}

//TODO! create an ARP to bytes and bytes to ARP to hand endianness

ARPPacketIPV4 createARPReplayPacket(ARPPacketIPV4 request)
{
	if (request.opcode != ARP_REQUEST) {
		logk("We were sent an ARP packet which we dont support (0x%X)\n", request.opcode);
	}

	memcpy(request.dmac, request.smac, 6);
	memcpy(request.smac, ourMAC, 6);

	request.dip = request.sip;
	request.sip = ourIP;

	request.opcode = ARP_REPLY;

	return request;
}


uint8_t* getBytesFromARPPacket(ARPPacketIPV4 arp)
{
	//switch to big endian
	arp.htype = switchEndian16(arp.htype);
	arp.ptype = switchEndian16(arp.ptype);
	arp.opcode = switchEndian16(arp.opcode);
	arp.sip = switchEndian32(arp.sip);
	arp.dip = switchEndian32(arp.dip);

	//copy into byte stream
	uint8_t* stream = (uint8_t*) malloc(sizeof(ARPPacketIPV4));
	memcpy(stream, &arp, sizeof(ARPPacketIPV4));

	return stream;
}

ARPPacketIPV4 getARPPacketFromBytes(uint8_t* bytes)
{
	//copy into packet
	ARPPacketIPV4 arp;
	memcpy(&arp, bytes, sizeof(ARPPacketIPV4));

	//switch to little endian
	arp.htype = switchEndian16(arp.htype);
	arp.ptype = switchEndian16(arp.ptype);
	arp.opcode = switchEndian16(arp.opcode);
	arp.sip = switchEndian32(arp.sip);
	arp.dip = switchEndian32(arp.dip);

	return arp;
}


/*
Sending:
p = createARPRequestPacket(ourMAC, ourIP, theirIP)
b = ARPtoBytes(p)
e = createEthernetPacket(payload = b)
b = EthernetToBytes(e)
ethernetHardwareSendBytes(b)

Recieving:
(somehow)
We got Ethernet request!
b = request in bytes
e = BytesToEthernet(b)
b = e.payload
p = BytesToARP(b)

fill in p, then send it back
*/

uint8_t* getMACFromIP(uint32_t ip)
{
	for (int i = 0; i < nextARPCache; ++i) {
		if (arpCache[i].ip == ip) {
			return arpCache[i].mac;
		}
	}

	ARPPacketIPV4 packet = createARPRequestPacket(ourMAC, ourIP, ip);
	//???

	uint8_t* mac = malloc(6);
	//???

	arpCache[nextARPCache].ip = ip;
	memcpy(arpCache[nextARPCache].mac, mac, 6);
	nextARPCache++;

	return mac;
}