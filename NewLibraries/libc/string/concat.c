//
//  concat.c
//  Banana libc
//
//  Created by Alex Boxall on 12/7/18.
//  Copyright © 2018 Alex Boxall. All rights reserved.
//

#include "string.h"
#include "assert.h"

char *strcat(char * restrict destination, const char * restrict source) {
    assert(source != NULL && destination != NULL);

    char* originalDest = destination;
    
    while (*destination) ++destination;
    while ((*destination++ = *source++));
    
    return originalDest;
}

char *strncat(char * restrict destination, const char * restrict source, size_t n) {
    assert(source != NULL && destination != NULL);
    
    if (n == 0) {
        return destination;
    }
    
    char *originalDest = destination;
    while (*destination) ++destination;
    while (n-- && (*destination++ = *source++));
    *destination = 0;
    
    return originalDest;
}
