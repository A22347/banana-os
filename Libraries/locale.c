#include <locale.h>
#include <stdbool.h>

struct lconv* CURRENT_LOCALE;
bool ffirstTime = true;

char LC_ALL_NAME[256];
char LC_COLLATE_NAME[256];
char LC_CTYPE_NAME[256];
char LC_MONETARY_NAME[256];
char LC_NUMERIC_NAME[256];
char LC_MESSAGES_NAME[256];
char LC_TIME_NAME[256];

void INIT_LOCALE ()
{
	CURRENT_LOCALE = malloc (sizeof (struct lconv));
	ffirstTime = false;

	CURRENT_LOCALE->int_curr_symbol = malloc (5);
	CURRENT_LOCALE->currency_symbol = malloc (5);
	CURRENT_LOCALE->mon_decimal_point = malloc (5);
	CURRENT_LOCALE->mon_thousands_sep = malloc (5);
	CURRENT_LOCALE->mon_grouping = malloc (5);
	CURRENT_LOCALE->positive_sign = malloc (5);
	CURRENT_LOCALE->negative_sign = malloc (5);

	CURRENT_LOCALE->decimal_point = malloc (5);
	CURRENT_LOCALE->thousands_sep = malloc (5);
	CURRENT_LOCALE->grouping = malloc (5);

	setlocale (LC_ALL, "C");
}

void SET_COLLATE (const char* localename)
{
	//set flags for strcoll and other functions...
	strcpy (LC_COLLATE_NAME, localename);
}

void SET_CTYPE (const char* localename)
{
	//set flags for the ctype functions...
	strcpy (LC_CTYPE_NAME, localename);
}

void SET_MONETARY (const char* localename)
{
	// switch (localename) { ...
	strcpy (CURRENT_LOCALE->int_curr_symbol, "$   ");
	strcpy (CURRENT_LOCALE->currency_symbol, "$");
	strcpy (CURRENT_LOCALE->mon_decimal_point, ".");
	strcpy (CURRENT_LOCALE->mon_thousands_sep, ",");
	strcpy (CURRENT_LOCALE->mon_grouping, "3");
	strcpy (CURRENT_LOCALE->positive_sign, "");
	strcpy (CURRENT_LOCALE->negative_sign, "-");
	CURRENT_LOCALE->int_frac_digits = 2;
	CURRENT_LOCALE->frac_digits = 2;
	CURRENT_LOCALE->p_cs_precedes = 1;
	CURRENT_LOCALE->p_sep_by_space = 0;
	CURRENT_LOCALE->n_cs_precedes = 1;
	CURRENT_LOCALE->n_sep_by_space = 0;
	CURRENT_LOCALE->p_sign_posn = 3;
	CURRENT_LOCALE->n_sign_posn = 3;

	strcpy (LC_MONETARY_NAME, localename);
}

void SET_NUMERIC (const char* localename)
{
	strcpy (CURRENT_LOCALE->decimal_point, ".");
	strcpy (CURRENT_LOCALE->thousands_sep, ",");
	strcpy (CURRENT_LOCALE->grouping, "3");
	strcpy (LC_NUMERIC_NAME, localename);
}

void SET_MESSAGES (const char* localename)
{
	strcpy (LC_MESSAGES_NAME, localename);
}

void SET_TIME (const char* localename)
{
	strcpy (LC_TIME_NAME, localename);
}

char *setlocale (int category, const char *locale)
{
	if (ffirstTime) {
		INIT_LOCALE ();
	}

	if (locale) {
		switch (category) {
		case LC_ALL:
			SET_COLLATE (locale);
			SET_CTYPE (locale);
			SET_MESSAGES (locale);
			SET_MONETARY (locale);
			SET_NUMERIC (locale);
			SET_TIME (locale);
			strcpy (LC_ALL_NAME, locale);
			break;

		case LC_COLLATE:
			SET_COLLATE (locale);
			break;

		case LC_CTYPE:
			SET_CTYPE (locale);
			break;

		case LC_MESSAGES:
			SET_MESSAGES (locale);
			break;

		case LC_MONETARY:
			SET_MONETARY (locale);
			break;

		case LC_NUMERIC:
			SET_NUMERIC (locale);
			break;

		case LC_TIME:
			SET_TIME (locale);
			break;

		default:
			return 0;
		}
		return locale;

	} else {
		switch (category) {
		case LC_ALL:
			return LC_ALL_NAME;
		case LC_COLLATE:
			return LC_COLLATE_NAME;
		case LC_CTYPE:
			return LC_CTYPE_NAME;
		case LC_MESSAGES:
			return LC_MESSAGES_NAME;
		case LC_MONETARY:
			return LC_MONETARY_NAME;
		case LC_NUMERIC:
			return LC_NUMERIC_NAME;
		case LC_TIME:
			return LC_TIME_NAME;
		default:
			return 0;
		}
	}

	return 0;
}

struct lconv *localeconv ()
{
	if (ffirstTime) {
		INIT_LOCALE ();
	}

	return CURRENT_LOCALE;
}