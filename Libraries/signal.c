#include <signal.h>

int errno = 0;

void (*__signalHandlers[256])(int) = { 0 };

void (*signal(int sig, void (*func)(int)))(int) {
	__signalHandlers[sig] = func;
    return func;
}

int raise (int sig) {
    if (__signalHandlers[sig] == SIG_DFL) {
        switch (sig) {
            case SIGABRT:
            case SIGALRM:
			case SIGINT:
            //...
                exit(sig);
                break;
                
            case SIGCHLD:
            //...
                break;
                
            default:
                //...
                abort();
                break;
                
        }
    } else if (__signalHandlers[sig] == SIG_IGN) {
        return 0;
    } else if (__signalHandlers[sig] == SIG_ERR) {
        return -1;
    } else if (__signalHandlers[sig]) {
        __signalHandlers[sig](sig);
        return 0;
    }
    return 1;
}