//
//  main.c
//  dir
//
//  Created by Alex Boxall on 28/4/19.
//  Copyright � 2019 Alex Boxall. All rights reserved.
//

#include <stdio.h>
#include <dirent.h>
#include <unistd.h>
#include <string.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <fcntl.h>

int main(int argc, const char* argv[])
{
	struct dirent* ent = malloc(sizeof(struct dirent));
	char dirname[256];
	if (argc <= 1) {
		getcwd(dirname, 255);
	} else {
		strcpy(dirname, argv[1]);
	}

	printf("Directory of %s\n\n", dirname);
	DIR* dir;

	if ((dir = opendir(dirname)) != NULL) {
		while ((ent = readdir(dir)) != NULL) {
			//struct stat st;
			char name[256];
			memset(name, 0, 255);
			strcpy(name, dirname);
			strcat(name, "/");
			strcat(name, ent->d_name);
			//stat (name, &st);
			printf("%s %11d %s \n", ent->d_type == DT_DIR ? "<DIR>" : "     ", 0/*st.st_size*/, ent->d_name);
		}
	}

	closedir(dir);

	return 0;
}