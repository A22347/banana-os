#pragma once

#include <stdint.h>

void panic ();
void panicWithMessage (char* msg);

extern char appOrDriverRunningNameInCaseOfCrash[128];
extern char panicErrorMessage[128];