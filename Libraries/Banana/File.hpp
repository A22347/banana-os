#ifndef __FILEMANAGER_HPP__
#define __FILEMANAGER_HPP__

#include "Core.hpp"
#define EOF "EOF\n\r\n\r\n\n\n\0\0"

char* UserMusic();
char* UserTrash();
char* UserFolder();
char* UserDesktop();
char* UserPictures();
char* UserDocuments();
void CreateNewFile (char* location, char* placeholderName, char* extension);

/**
\class File
\brief A file object which can be used to read, write or get file information
*/
class File {
protected:
    int number = -1;
    bool valid = false;
    char _filename[257];
    
public:
    File (char* filename, char* mode);        ///<Open a file.
    ~File ();                                
    char* read();                ///<Return the entire contents of a file in a pointer. The pointer must be freed at some point to avoid leaking memory.
    char* readline();    ///<Return the a line of a file in a pointer. Every time it is called it will return the next line. The pointer must be freed at some point to avoid leaking memory.
    void write(char* data);    ///<Writes data to the file. In 'write' mode this will overwrite any existing data. In 'append' mode this will append the data to the end of the file.
    void close();    ///<Close the file
    int stat();        ///<Get the file size, or -1 if it doesn't exists
    void erase();        ///<Delete the file
};

#endif