#ifndef __DRIVERS_H__
#define __DRIVERS_H__

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>
#include <syscall.h>

	enum Registers
	{
		EAX,
		EBX,
		EDX,
		ESP,
		EBP,
		ESI,
		EDI
	};

	/*typedef struct regs_t
	{
		uint32_t reg_a;
		uint32_t reg_b;
		uint32_t reg_c;
		uint32_t reg_d;
		uint32_t reg_e;
		uint32_t reg_f;
		uint32_t reg_g;
		uint32_t reg_h;
		
	} regs_t;*/

	enum DriverType
	{
		INVALID_DRIVER,
		DISPLAY_DRIVER,			//the kernel will use it to draw to the screen
		SOUND_DRIVER,			//the kernel will choose one of these drivers, selected in the sound manager
		BEEPER_DRIVER,			//only one needed
		KEYBOARD_DRIVER,		//the kernel will use ALL of the keyboards combined for input
		MOUSE_DRIVER,			//the kernel will use ALL of the mice combined for input
		PRINTER_DRIVER,			//you should know this
		SCANNER_DRIVER,			//read above
		FAX_DRIVER,				//read above
		PHONE_DRIVER,			//we'll get to it later
		DISK_DRIVER,			//each disk gets one based on the manufacturer + details
		FILESYSTEM_DRIVER,		//see Quiver: Banana OS / Filesystem Drivers
		TIMER_DRIVER,			//only one needed
		CLOCK_DRIVER,			//only one needed
		INTERNET_DRIVER,		//we'll get to it later
		GAME_CONTROLLER_DRIVER,	//similar to keybord
		AUDIO_INPUT_DRIVER,		//we'll get to it later
		VIDEO_INPUT_DRIVER,		//we'll get to it later
		IMAGE_INPUT_DRIVER,		//we'll get to it later
		POWER_DRIVER,			//e.g. apm.sys / acpi.sys
		PC_DRIVER,				// eg. PC info such as the battery + screen brightness on laptops
		TOUCHSCREEN_DISPLAY_DRIVER,		//we'll get to it later
		TABLET_DRIVER,					//we'll get to it later
		GENERAL_DRIVER,					//it won't be used by the kernel, only applicatons
		BUS_DRIVER,				// 'read' gets called by the system every now and then to each of these drivers,
								// they may install / uninstall drivers based on the devices connected to the bus
	};

	uint8_t inb  (uint16_t port);
	uint16_t inw (uint16_t port);
	uint32_t inl (uint16_t port);

	void insb (uint16_t port, void *addr, size_t cnt);
	void insw (uint16_t port, void *addr, size_t cnt);
	void insl (uint16_t port, void *addr, size_t cnt);

	void outb (uint16_t port, uint8_t  val);
	void outw (uint16_t port, uint16_t val);
	void outl (uint16_t port, uint32_t val);

	void outsb (uint16_t port, const void *addr, size_t cnt);
	void outsw (uint16_t port, const void *addr, size_t cnt);
	void outsl (uint16_t port, const void *addr, size_t cnt);

	void iowait ();
	void irq_install (uint8_t irqNumber);
	void irq_uninstall (uint8_t irqNumber);

	unsigned long read_cr0 ();
	unsigned long read_cr2 ();
	unsigned long read_cr3 ();
	unsigned long read_cr4 ();

	void loadRegister (enum Registers reg, uint32_t value);
	uint32_t readRegister (enum Registers reg);

	void wrmsr (uint32_t msr_id, uint64_t msr_value);
	uint64_t rdmsr (uint32_t msr_id);

	int end (regs_t* registers);

	void cpuid (int code, uint32_t* a, uint32_t* d);

#ifdef __cplusplus
}
#endif

#endif


/*

DISPLAY_DRIVER,			//the kernel will use it to draw to the screen
SOUND_DRIVER,			//the kernel will choose one of these drivers, selected in the sound manager

BEEPER_DRIVER,			//only one needed
	read : retruns playing sound
	write: eax = hertz (0 = none)

KEYBOARD_DRIVER,		//the kernel will use ALL of the keyboards combined for input
MOUSE_DRIVER,			//the kernel will use ALL of the mice combined for input
PRINTER_DRIVER,			//you should know this
SCANNER_DRIVER,			//read above
FAX_DRIVER,				//read above
PHONE_DRIVER,			//we'll get to it later
DISK_DRIVER,			//each disk gets one based on the manufacturer + details
FILESYSTEM_DRIVER,		//see Quiver: Banana OS / Filesystem Drivers
TIMER_DRIVER,			//only one needed
CLOCK_DRIVER,			//only one needed
INTERNET_DRIVER,		//we'll get to it later
GAME_CONTROLLER_DRIVER,	//similar to keybord
AUDIO_INPUT_DRIVER,		//we'll get to it later
VIDEO_INPUT_DRIVER,		//we'll get to it later
IMAGE_INPUT_DRIVER,		//we'll get to it later
POWER_DRIVER,			//each PC gets one based on the manufacturer + details
PC_DRIVER,				// eg. PC info such as the battery + screen brightness on laptops
TOUCHSCREEN_DISPLAY_DRIVER,		//we'll get to it later
TABLET_DRIVER,					//we'll get to it later
GENERAL_DRIVER					//it won't be used by the kernel, only applicatons

*/