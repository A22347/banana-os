//
//  nosyscall.c
//  Banana libc
//
//  Created by Alex Boxall on 18/10/18.
//  Copyright © 2018 Alex Boxall. All rights reserved.
//

#include "unistd.h"
#include <stdint.h>
#include "syscall.h"
#include <stddef.h>
#include "sys/types.h"

char** environ;

int chdir (const char* a)
{
	return __i_system_call_setcwd ((char*) a);
}

char* getcwd (char* location, size_t size)
{
	__i_system_call_getcwd (size, location);
	return location;
}

int execve(const char* path, char* const args[], char* const env[])
{
	__i_system_call_execve((char*) path, (char**) args, (char**) env);
	return -1;			//we must have failed to get back here
}

int execv(const char* path, char* const args[])
{
	return execve(path, args, environ);
}

int execvp(const char* path, char* const args[])
{
	return execv(path, args);
}

pid_t fork (void)
{
	return __i_system_call_fork();
}

int pipe(int v[2])
{
	uint64_t r;
	uint64_t w;

	int ret = __i_system_call_pipe((long long unsigned int*) &r, (long long unsigned int*) &w);

	v[0] = r;
	v[1] = w;

	return ret;
}

/*
int          execl(const char*, const char*, ...);
int          execle(const char*, const char*, ...);
int          execlp(const char*, const char*, ...);
*/