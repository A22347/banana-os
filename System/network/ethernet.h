#pragma once

#include <stdbool.h>
#include <stdint.h>

struct EthernetPacket
{
	uint8_t dmac[6];
	uint8_t smac[6];
	uint16_t type;

	uint8_t payload[1550];	//the maximum is around 1536 bytes

} __attribute__((packed));

typedef struct EthernetPacket EthernetPacket;

uint16_t switchEndian16(uint16_t nb);
uint32_t switchEndian32(uint32_t nb);

EthernetPacket generateEthernetPacketFromType(uint8_t* dmac, uint8_t* smac, uint16_t type, uint8_t* payload);
EthernetPacket generateEthernetPacketFromSize(uint8_t* dmac, uint8_t* smac, uint16_t size, uint8_t* payload);
uint8_t*	   getBytesFromEthernetPacket(EthernetPacket ep);
EthernetPacket getEthernetPacketFromBytes(uint8_t* bytes);

//to be filled in by the initialisation of our ethernet hardware driver
extern uint32_t ourIP;
extern uint8_t ourMAC[6];

