#ifndef __UTILS_HPP__
#define __UTILS_HPP__

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include <stddef.h>
#include <stdbool.h>

	int __str_int (char* c);
	char* __int_str (int i, char b[]);

	int strlen (const char* c);
	int strcmp (const char* s1, const char* s2);
	int strcasecmp (const char* s1, const char* s2);
	int strncmp (const char* s1, const char* s2, size_t n);
	int strncasecmp (const char* s1, const char* s2, size_t n);
	void* memcpy (void* destination, const void* source, size_t num);
	char* strcpy (char* a, const char* b);
	char* strncpy (char* a, const char* b, size_t n);
	char* strcat (char* dest, const char* src);
	char* strncat (char* dest, const char* src, size_t n);
	char* strstr (char* main, char* sub);
	char* strchr (const char* string, int repl);
	char* strrchr (const char* string, int repl);
	char* strtok (char* s, const char* t);
	char* strpbrk (const char* cs, const char* ct);
	char* strdup (const char *s);
	size_t strspn (const char *str1, const char *str2);
	size_t strcspn (const char *str1, const char *str2);
	void* memchr (const void* str, int c, size_t n);
	int memcmp (const void* s1, const void* s2, size_t n);
	void memmove (void *dest, const void *src, size_t n);
	void* memset (void* b, int c, size_t len);
	char* strerror (int n);


#ifdef __cplusplus
}
#endif

#endif