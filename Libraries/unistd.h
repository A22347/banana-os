#ifndef __UNISTD_H___
#define __UNISTD_H___

#ifdef __cplusplus
extern "C" {
#endif

#include <syscall.h>
#include <stddef.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/types.h>
#include <limits.h>

	pid_t getpid ();

	int close (int fd);
	ssize_t read (int, void *, size_t);
	ssize_t write (int, const void *, size_t);
	off_t lseek (int, off_t, int);

	int dup (int fildes);
	int dup2 (int fildes, int fildes2);

	unsigned int sleep (unsigned int);
	int usleep (useconds_t);

	int unlink (const char *);

	int chdir (const char *path);
	char* getcwd (char* buffer, size_t size);
	char* getwd (char* buffer);
	char* get_current_dir_name ();

	int truncate (const char* filename, off_t byte);
	int ftruncate (int file, off_t byte);

	//yield() is now a macro in syscall.h
	void realyield ();

	long sysconf (int option);

	void _exit (int code);

#ifdef __cplusplus
}
#endif

#endif