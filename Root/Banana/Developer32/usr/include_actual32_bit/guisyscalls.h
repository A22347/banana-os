//
//  guisyscalls.h
//  Banana API (New)
//
//  Created by Alex Boxall on 21/10/18.
//  Copyright © 2018 Alex Boxall. All rights reserved.
//

#ifndef guisyscalls_h
#define guisyscalls_h

#if defined(__cplusplus)
extern "C" {
#endif
    
    //the typedef should be placed into
    //#include "../../System/core/syscalldef.h"
    //then actually include that file

/*! \file guisyscalls.h
 \brief Definitions for event related structures and enumerations. The same file is used in the kernel and in the GUI library.
 */


    
#include <stdint.h>
//#include "../../System/core/syscalldef.h"


    typedef enum GUIEventTypes {
        GUI_CheckEvent,
        GUI_UpdateScreen,
        GUI_SetPixel,
        GUI_GetPixel,
        GUI_SetCursor,
        GUI_CreateWindow,
        GUI_CloseIcon,
        GUI_AllowResize,
        GUI_Minimise,
        GUI_SetTitle,
        GUI_SetPosition,
        GUI_GetX,
        GUI_GetY,
        GUI_SetWidth,
        GUI_GetHeight,
        GUI_SetMinimised,
        GUI_SetMaximised,
        GUI_GetMinimised,
        GUI_GetMaximised,
        
        //time for some (not-neccessary), but optimised drawing functions
        //(which SHOULD be used instead of repeated SetPixel
        GUI_DrawRectangle,
        GUI_DrawCircle,
        GUI_DrawLine
        
    } GUIEventTypes;
    
    int GUISystemCall (uint16_t callno, uint32_t ebx, uint32_t ecx, void* edx);
    
#if defined(__cplusplus)
}
#endif
        

#endif /* guisyscalls_h */
