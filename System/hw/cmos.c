#include <hw/cmos.h>
#include <hw/ports.h>

bool nmiOn = false;

uint8_t readCMOS(uint8_t reg)
{
	outb(0x70, reg | (nmiOn ? 0 : 0x80));
	return inb(0x71);
}

void writeCMOS(uint8_t reg, uint8_t num)
{
	outb(0x70, reg | (nmiOn ? 0 : 0x80));
	outb(0x71, num);
}

void enableNMI(bool on)
{
	nmiOn = on;
}