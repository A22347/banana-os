#pragma once

#include <core/kernel.h>
#include <debug/log.h>
#include <libk/stdio.h>

void terminal_flush ();

void putpixelr (uint32_t semispot, uint32_t col);
void putpixel (uint32_t semispot, uint32_t col);

void terminal_putentryat (char c, uint32_t fg, uint32_t bg, uint8_t x, uint8_t y);
void terminal_putentryat_pixel (char c, uint32_t fg, uint32_t bg, uint16_t x, uint16_t y);
void terminal_putentryat_pixel_raw (char c, uint32_t fg, uint32_t bg, uint16_t x, uint16_t y);

void terminal_putchar_colour (char c, uint32_t fg, uint32_t bg);
void terminal_writestring_colour (const char* data, uint32_t bg, uint32_t fg);

void terminal_quickdraw (char c, uint32_t fg, uint32_t bg, uint16_t x, uint16_t y);

extern int terminal_row;
extern int terminal_column;