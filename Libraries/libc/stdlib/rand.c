//
//  rand.c
//  Banana libc
//
//  Created by Alex Boxall on 12/7/18.
//  Copyright © 2018 Alex Boxall. All rights reserved.
//

#include "stdlib.h"

static unsigned int z1 = 0xDEADBEEF, z2 = 0xCAFE55, z3 = 0xABCDEF6, z4 = 3943342897;

//http://www.iro.umontreal.ca/~simardr/rng/lfsr113.c
int rand(void) {
    unsigned int b;
    b  = ((z1 << 6) ^ z1) >> 13;
    z1 = ((z1 & 4294967294U) << 18) ^ b;
    b  = ((z2 << 2) ^ z2) >> 27;
    z2 = ((z2 & 4294967288U) << 2) ^ b;
    b  = ((z3 << 13) ^ z3) >> 21;
    z3 = ((z3 & 4294967280U) << 7) ^ b;
    b  = ((z4 << 3) ^ z4) >> 12;
    z4 = ((z4 & 4294967168U) << 13) ^ b;
    return (z1 ^ z2 ^ z3 ^ z4);
}

void srand(unsigned int seed) {
    static int n = 0;
    ++n;
    n %= 4;
    if (n == 0) z1 = seed;
    if (n == 1) z2 = seed;
    if (n == 2) z3 = seed;
    if (n == 3) z4 = seed;
    
    if (z1 <= 1) {
        z1 = 5234 + z1 * 884;
    }
    if (z2 <= 7) {
        z2 *= 435;
    }
    if (z3 <= 15) {
        z3 *= 6666;
    }
    if (z4 <= 127) {
        z4 *= 824;
    }
}
