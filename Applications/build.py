import os

for path, subdirs, files in os.walk(os.getcwd()):
    for name in files:
        if name.startswith('Make'):
            print(os.path.join(path, name))
            os.chdir(path)
            os.system('make all'.format(path))
