#include <hw/vbe.h>
#include <core/string.h>
#include <hal/display.h>
#include <debug/log.h>
#include <hw/ports.h>

uint8_t* realVideoMemory;

int vbeAvaliable ()
{
	return 1;
}

void vbeInit (uint8_t displayNo)
{
	//too bad, it has to be done in real mode

	if (displays[displayNo].bytesPerPixel == 1) {
		uint8_t foursect = 0x55;
		uint8_t eightsect = 0x24;

		outb (0x03c8, 0);

		for (int i = 0; i < 256; ++i) {
			uint16_t red = (i >> 5) & 7;
			uint16_t green = (i >> 2) & 7;
			uint16_t blue = i & 3;

			//0, 144, 170
			//0090AA

			red *= eightsect;
			green *= eightsect;
			blue *= foursect;

			logk ("COLOUR_ %d: red = %d, green = %d, blue = %d\n", i, red, green, blue);

			outb (0x03c9, (uint8_t) red & 0xFF);
			outb (0x03c9, (uint8_t) green & 0xFF);
			outb (0x03c9, (uint8_t) blue & 0xFF);
		}
	}
}

void vbeCopyBufferTo (uint32_t* buffer, uint32_t start, uint32_t size, uint8_t displayNo)
{
	if (displays[displayNo].bytesPerPixel == 4) {
		memcpy (realVideoMemory + start, buffer + start, size);

	} else if (displays[displayNo].bytesPerPixel == 3) {
		uint32_t ba = 0;
		uint32_t comp = (start + size) * 3;
		for (uint32_t i = start * 3; i < comp; i += 3) {
			realVideoMemory[i]     =  buffer[ba]		& 0xFF;		//blue
			realVideoMemory[i + 1] = (buffer[ba] >> 8)  & 0xFF;		//green
			realVideoMemory[i + 2] = (buffer[ba] >> 16) & 0xFF;		//red
			ba++;						
		}

	} else if (displays[displayNo].bytesPerPixel == 2) {
		uint32_t ba = 0;
		uint32_t comp = (start + size) * 2;

		for (uint32_t i = start * 2; i < comp; i += 2) {
			uint32_t colour = buffer[ba++];
			uint8_t red			= ((colour >> 16) & 0xFF) >> 3;
			uint8_t green		= ((colour >> 8)  & 0xFF) >> 2;
			uint8_t blue		= (colour & 0xFF)		  >> 3;
			uint16_t highcolour = (red << 11) | (green << 5) | blue;

			realVideoMemory[i]     = highcolour & 0xFF;			//mixture
			realVideoMemory[i + 1] = highcolour >> 16;			//mixture
		}

	} else if (displays[displayNo].bytesPerPixel == 1) {
		for (uint32_t i = start; i < start + size; ++i) {
			uint32_t colour = buffer[i];
			uint8_t red = ((colour >> 16) & 0xFF) >> 5;
			uint8_t green = ((colour >> 8) & 0xFF) >> 5;
			uint8_t blue = (colour & 0xFF) >> 6;
			uint8_t endcolour = (red << 5) | (green << 2) | blue;

			realVideoMemory[i] = endcolour;			//mixture
		}
	}
}

void vbeCopyBufferFrom (uint32_t* buffer, uint32_t start, uint32_t size, uint8_t displayNo)
{
	if (displays[displayNo].bytesPerPixel == 4) {
		memcpy (buffer + start, ((uint8_t*) realVideoMemory) + start, size);

	} else if (displays[displayNo].bytesPerPixel == 3) {
		

	} else if (displays[displayNo].bytesPerPixel == 2) {
		

	} else if (displays[displayNo].bytesPerPixel == 1) {
		
	}
}

void vbeSetPixel (uint32_t location, uint32_t colour, uint8_t displayNo)
{
	uint32_t ba = location * BYTES_PER_PIXEL;

	if (location > (unsigned) (TOTAL_HEIGHT * ONE_LINE * BYTES_PER_PIXEL)) {
		return;
	}

	if (BYTES_PER_PIXEL >= 3) {
		realVideoMemory[ba++] = colour & 255;
		realVideoMemory[ba++] = (colour >> 8) & 255;
		realVideoMemory[ba] = (colour >> 16) & 255;

	} else if (BYTES_PER_PIXEL == 2) {
		uint8_t red = ((colour >> 16) & 0xFF) >> 3;
		uint8_t green = ((colour >> 8) & 0xFF) >> 2;
		uint8_t blue = (colour & 0xFF) >> 3;
		uint16_t highcolour = (red << 11) | (green << 5) | blue;

		realVideoMemory[ba++] = highcolour & 255;
		realVideoMemory[ba] = (highcolour >> 8) & 255;

	} else if (BYTES_PER_PIXEL == 1) {
		uint8_t red = ((colour >> 16) & 0xFF) >> 5;
		uint8_t green = ((colour >> 8) & 0xFF) >> 5;
		uint8_t blue = (colour & 0xFF) >> 6;
		uint8_t endcolour = (red << 5) | (green << 2) | blue;

		realVideoMemory[ba] = endcolour;
	}
}

uint32_t vbeGetPixel (uint32_t location, uint8_t displayNo)
{
	uint32_t ba = location * BYTES_PER_PIXEL;
	uint32_t colour = 0;

	if (location > (unsigned) (TOTAL_HEIGHT * ONE_LINE * BYTES_PER_PIXEL)) {
		return 0;
	}

	if (BYTES_PER_PIXEL >= 3) {
		colour = realVideoMemory[ba++] << 0;
		colour |= realVideoMemory[ba++] << 8;
		colour |= realVideoMemory[ba] << 16;

	} else if (BYTES_PER_PIXEL == 2) {
		uint16_t c = realVideoMemory[ba++] << 0;
		c |= realVideoMemory[ba] << 8;

		uint8_t red = (c >> 11) & 0b11111;
		uint8_t green = (c >> 5) & 0b111111;
		uint8_t blue = c & 0b11111;
		red <<= 3;
		green <<= 2;
		blue <<= 3;

		return (red << 16) | (green << 8) | blue;

	} else if (BYTES_PER_PIXEL == 1) {
		uint8_t c = realVideoMemory[ba];

		uint8_t red = (c >> 5) & 0b111;
		uint8_t green = (c >> 2) & 0b111;
		uint8_t blue = c & 0b11;
		red <<= 5;
		green <<= 5;
		blue <<= 6;

		return (red << 16) | (green << 8) | blue;
	}

	return colour;
}