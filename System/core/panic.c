#include <core/panic.h>
#include <gui/terminal.h>
#include <libk/string.h>
#include <debug/log.h>
#include <hal/power.h>
#include <hw/ports.h>
#include <hal/display.h>

// THE MAGICAL 'panic__' wrappers are found in Filesystem/pff.h so everyone can use them

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wformat"

void panic()
{
	asm ("cli");

	terminal_row = 0;
	terminal_column = 0;

	logk ("PANIC CALLED: %s\n", panicErrorMessage);

	size_t rax = 0, rbx = 0, rcx = 0, rdx = 0, rsp = 0, rbp = 0, rsi = 0, rdi = 0, r8 = 0, r9 = 0, \
		r10 = 0, r11 = 0, r12 = 0, r13 = 0, r14 = 0, r15 = 0, rflags = 0, rip = 0;

	for (int a = 0; a < ONE_LINE * TOTAL_HEIGHT; ++a) {
		putpixelr(a, COLOUR_BLUE);
	}
	terminal_writestring_colour ("A problem has been detected and Banana has been shutdown to prevent damage to your computer.", COLOUR_BLUE, COLOUR_WHITE);
	logk ("BSOD: %s, %s\n", panicErrorMessage, appOrDriverRunningNameInCaseOfCrash[0]);
	if (appOrDriverRunningNameInCaseOfCrash[0]) {
		terminal_writestring_colour ("\n\nThe problem seems to be caused by the following file: ", COLOUR_BLUE, COLOUR_WHITE);
		terminal_writestring_colour (appOrDriverRunningNameInCaseOfCrash, COLOUR_BLUE, COLOUR_WHITE);
	}

	terminal_writestring_colour ("\n\n", COLOUR_BLUE, COLOUR_WHITE);
	terminal_writestring_colour (panicErrorMessage, COLOUR_BLUE, COLOUR_WHITE);

	terminal_writestring_colour ("\n\nIf this is the first time you've seen this stop error, restart your computer. If this screen appears again, follow these steps:\n\nCheck to make sure any new hardware or software is properly installed. If this is a new installation try reinstalling or updating Banana.\n\nIf problems continue, disable or remove any newly installed hardware or software. Disable BIOS\nmemory options such as caching or shadowing. If you need to use Safe Mode to remove or disable\ncomponents, restart your computer and hold down the F8 key to boot in Safe Mode.\n\nTechnical information:\n\n", COLOUR_BLUE, COLOUR_WHITE);
	terminal_writestring_colour ("Contact your system administator or technical support group for further assistance.\n\nPress the RETURN key to reboot your computer.\n\n\n\n\n", COLOUR_BLUE, COLOUR_WHITE);

	// convert panic() into panic__(getEax(), getEbx(), getEcx() ... ) using a macro

	asm ("cli");

	terminal_flush ();
	
	while (1);
	while (1) {
		if (inb (0x60) == 0x5A) {
			reboot ();
		}
	}
}

void panicWithMessage (char* msg)
{
	strcpy (panicErrorMessage, msg);
	panic ();
}

#pragma GCC diagnostic pop