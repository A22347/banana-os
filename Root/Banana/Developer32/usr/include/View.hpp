//
//  View.hpp
//  Banana API (New)
//
//  Created by Alex Boxall on 18/5/18.
//  Copyright © 2018 Alex Boxall. All rights reserved.
//


#ifndef View_hpp
#define View_hpp

#include "RequiredImporting.hpp"
#include <stdlib.h>
#include "Responder.hpp"
#include "Constraint.hpp"
#include "Exception.hpp"
#include "Cursor.hpp"


/*! \file View.hpp
 \brief A header file defining the View class.
 */

/*!
 \class View
 \brief Base class for all graphical controls
 
 The View class provides a base for all graphical controls,
 providing drawing and event handling methods. It also allows
 for parent and child views. Most of the time, it is not necessary
 for normal application code to use the View class. All graphical
 controls inherit from the View class.
 */

class View : public Responder {
private:
    void updateSelfAndChildren ();             ///< redraws the current view and its children recursively
    void addConstraint (Constraint* constraint, Constraint** constraintArray);
    void recursiveDestroy();
    void calculateConstraints();
    
    View (const View& other);                   ///< let's not copy
    View (View&& other) noexcept;               ///< or move...
    View& operator=(const View& other);         ///< or do this one...
    View& operator=(View&& other) noexcept;     ///< or even this one. who came up with this one?

protected:
    static const int maxConstraintsPerType = 8;
    
    Cursor _cursor;
    
    int originalXPos;
    int originalYPos;
    int originalWidth;
    int originalHeight;
    
    int xPos;
    int yPos;
    int widthPos;
    int heightPos;
    
    Constraint* xConstraints[maxConstraintsPerType];
    Constraint* yConstraints[maxConstraintsPerType];
    Constraint* widthConstraints[maxConstraintsPerType];
    Constraint* heightConstraints[maxConstraintsPerType];
    
    bool deleted = false;

    struct children__node_t {
        View* child;
        struct children__node_t* next;
    };
    
    View* parent;                              ///< parent view or nullptr if none
    bool dirty;                                ///< set to true if the view's content has been changed but not redrawn

    void addChild (View* const child);               ///< adds a child view to this view
    void handleEvents (Event event);

    
    virtual void paint ();                     ///< redraws the all views in the view tree
    
    virtual void setPixelAtPosition (int x, int y, uint32_t colour);
    virtual uint32_t getPixelAtPosition (int x, int y) const;
    
    virtual bool updateCoords();
    virtual void onRemoval();
    
public:
    struct children__node_t* children;         ///< linked list of all child views

    View (int x, int y, int width, int height);
    View (View* parent, int x, int y, int width, int height);
    
    virtual ~View ();
    
    void update();                              ///< repaints the view
    
    virtual int width();
    virtual int height();
    virtual int x();
    virtual int y();
    
    virtual void width (int w);
    virtual void height (int h);
    virtual void x (int xx);
    virtual void y (int yy);
    
    virtual void position (int x, int y);
    virtual void size (int width, int height);

    virtual Cursor cursor();
    virtual void cursor(Cursor csr);
    
    int getWidthWithoutUpdating() const;
    int getHeightWithoutUpdating() const;
    int getXWithoutUpdating() const;
    int getYWithoutUpdating() const;

    void addWidthConstraint(Constraint* constraint);
    void addHeightConstraint(Constraint* constraint);
    void addXPositionConstraint(Constraint* constraint);
    void addYPositionConstraint(Constraint* constraint);

    void destroy ();
    void setDirty ();
};

#endif /* View_hpp */
