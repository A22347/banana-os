#ifndef __STDLIB_H__
#define __STDLIB_H__

#ifdef __cplusplus
extern "C" {
#endif

#include <syscall.h>
#include <limits.h>
#define RAND_MAX ((1 >> 31))
#define EXIT_FAILURE 1
#define EXIT_SUCCESS 0
#define assert(x)

	enum Months
	{
		InvalidMonth,
		January,
		February,
		March,
		April,
		May,
		June,
		July,
		August,
		September,
		October,
		November,
		December
	};

	typedef struct datetime_t
	{
		uint8_t day;
		uint8_t month;
		uint16_t year;
		uint8_t hour;
		uint8_t minute;
		uint8_t second;

	} datetime_t;

	typedef struct div_t
	{
		int quot;
		int rem;
	} div_t;

	typedef struct ldiv_t
	{
		long quot;
		long rem;
	} ldiv_t;

	datetime_t currentTime ();
	datetime_t secondsToDatetime (uint64_t data);
	uint64_t datetimeToSeconds (datetime_t d);

	void* malloc (size_t size);
	void* calloc (size_t nitems, size_t size);
	void* realloc (void *ptr, size_t size);
	void free (void* ptr);
	void abort ();// __attribute__ ((noreturn));
	void exit (int code);// __attribute__ ((noreturn));

	int rand ();
	void srand (int seed);
	int abs (int x);
	long labs (long x);

	int system (const char* arg);
	char* getenv (const char* name);

	unsigned long int strtoul (const char* c, char** endptr, int base);
	long int strtol (const char* c, char** endptr, int base);
	long int atol (const char* c);
	int atoi (const char* c);
	float atof (const char* s);
	double atod (const char* s);

	void qsort (void* base, size_t n, size_t size, int (*cmp)(const void*, const void*));

	div_t div (int num, int denom);
	ldiv_t ldiv (long num, long denom);

	regs_t executeDriver (char* drivername, drivermode mode, regs_t args);

#ifdef __cplusplus
}
#endif

#endif