#pragma once

#include <stdint.h>
#include <devss/devices.h>

void openATAPI(Device* self, int b, int c, void* d);
void readATAPI(Device* self, int b, int c, void* d);
void writeATAPI(Device* self, int b, int c, void* d);