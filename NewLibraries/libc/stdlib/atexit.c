//
//  atexit.c
//  Banana libc
//
//  Created by Alex Boxall on 11/7/18.
//  Copyright © 2018 Alex Boxall. All rights reserved.
//

#ifndef _libk

#include "stdlib.h"

int numberOfAtExitHandlers = 0;
void (*atexitHandlers[__i_MAX_ATEXIT_HANDLERS])(void);

int numberOfAtQuickExitHandlers = 0;
void (*atQuickExitHandlers[__i_MAX_ATEXIT_HANDLERS])(void);

//registers a function to be called when exit is called
int atexit(void (*func)(void)) {
    //0 if sucessful, 1 if not
    //must support at least 32 functions
    
    if (numberOfAtExitHandlers < __i_MAX_ATEXIT_HANDLERS) {
        atexitHandlers[numberOfAtExitHandlers++] = func;
        return 0;
    }
    return 1;
}

int at_quick_exit(void (*func)(void)) {
    //0 if sucessful, 1 if not
    //must support at least 32 functions
    
    if (numberOfAtQuickExitHandlers < __i_MAX_ATEXIT_HANDLERS) {
        atQuickExitHandlers[numberOfAtQuickExitHandlers++] = func;
        return 0;
    }
    return 1;
}

#endif
