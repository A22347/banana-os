#ifndef __ASSERT_H__
#define __ASSERT_H__

#define Q(string) #string

#ifdef NDEBUG
#define assert(ignore) ((void)0)
#else
#include <stdio.h>
#define assert(condition) ((void)(condition ? 0 : (printf( __FILE__ ":%d:%s: assertion failed: " Q(condition) "\n", __LINE__, __func__), ({abort(), 0;}), 0)))
#endif

#endif