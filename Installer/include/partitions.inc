LoadMBR:
	;check for GPT
	mov ah, 0x42		;read
	mov al, 0
	mov dl, byte [HDDNumber - D_OFFSET]		;hard drive
	mov [BLK2 - D_OFFSET], dword 1			;1 sector
	mov si, HDDZEROPACKET2 - D_OFFSET
	mov [RDF2 - D_OFFSET], dword 1			;sector 1 is the GPT header
	;will load to 0x7000
	int 0x13
	mov ax, 0
	mov fs, ax
	cmp [fs:0x7000], dword 0x20494645
	je Stage102GPT


	;load MBR
	mov ah, 0x42		;read
	mov al, 0
	mov dl, byte [HDDNumber - D_OFFSET]		;hard drive
	mov [BLK2 - D_OFFSET], dword 1			;1 sector
	mov si, HDDZEROPACKET2 - D_OFFSET
	mov [RDF2 - D_OFFSET], dword 0			;sector 0 is the MBR
	;will load to 0x7000
	int 0x13
	
	cmp word [fs:0x71FE], 0xAA55
	jne Stage101

	mov ax, 0
	mov fs, ax

	mov al, [fs:0x71BE]
	mov [part1BOOT - D_OFFSET], al
	mov al, [fs:0x71BE + 4]
	mov [part1TYPE - D_OFFSET], al
	mov eax, [fs:0x71BE + 8]
	mov [part1LBA - D_OFFSET], eax
	mov eax, [fs:0x71BE + 12]
	mov [part1SIZE - D_OFFSET], eax

	mov al, [fs:0x71CE]
	mov [part2BOOT - D_OFFSET], al
	mov al, [fs:0x71CE + 4]
	mov [part2TYPE - D_OFFSET], al
	mov eax, [fs:0x71CE + 8]
	mov [part2LBA - D_OFFSET], eax
	mov eax, [fs:0x71CE + 12]
	mov [part2SIZE - D_OFFSET], eax

	mov al, [fs:0x71DE]
	mov [part3BOOT - D_OFFSET], al
	mov al, [fs:0x71DE + 4]
	mov [part3TYPE - D_OFFSET], al
	mov eax, [fs:0x71DE + 8]
	mov [part3LBA - D_OFFSET], eax
	mov eax, [fs:0x71DE + 12]
	mov [part3SIZE - D_OFFSET], eax

	mov al, [fs:0x71EE]
	mov [part4BOOT - D_OFFSET], al
	mov al, [fs:0x71EE + 4]
	mov [part4TYPE - D_OFFSET], al
	mov eax, [fs:0x71EE + 8]
	mov [part4LBA - D_OFFSET], eax
	mov eax, [fs:0x71EE + 12]
	mov [part4SIZE - D_OFFSET], eax

	;ideally these should be combined after the last partition
	mov eax, [part2LBA - D_OFFSET]
	sub eax, [part1SIZE - D_OFFSET]
	sub eax, [part1LBA - D_OFFSET]
	mov [gap1SIZE - D_OFFSET], eax
	mov eax, [part2LBA - D_OFFSET]
	sub eax, [gap1SIZE - D_OFFSET]
	mov [gap1LBA - D_OFFSET], eax

	mov eax, [part3LBA - D_OFFSET]
	sub eax, [part2SIZE - D_OFFSET]
	sub eax, [part2LBA - D_OFFSET]
	mov [gap2SIZE - D_OFFSET], eax
	mov eax, [part3LBA - D_OFFSET]
	sub eax, [gap2LBA - D_OFFSET]
	mov [gap2LBA - D_OFFSET], eax

	mov eax, [part4LBA - D_OFFSET]
	sub eax, [part3SIZE - D_OFFSET]
	sub eax, [part3LBA - D_OFFSET]
	mov [gap3SIZE - D_OFFSET], eax
	mov eax, [part4LBA - D_OFFSET]
	sub eax, [gap3LBA - D_OFFSET]
	mov [gap3LBA - D_OFFSET], eax

	call getSizeOfDisk
	sub eax, [part4SIZE - D_OFFSET]
	sub eax, [part4LBA - D_OFFSET]
	mov [gap4SIZE - D_OFFSET], eax
	call getSizeOfDisk
	sub eax, [gap4LBA - D_OFFSET]
	mov [gap4LBA - D_OFFSET], eax

	jmp .skipcombine
	cmp [part4SIZE - D_OFFSET], dword 0
	je .combine1
	jmp .ncombine1
.combine1:
	mov eax, [gap3SIZE - D_OFFSET]

	clc
	add eax, [gap4SIZE - D_OFFSET]
	jnc .overflowdone1
	sub eax, [gap4SIZE - D_OFFSET]
.overflowdone1:

	mov [gap3SIZE - D_OFFSET], eax
	mov [gap4SIZE - D_OFFSET], dword 0
.ncombine1:
	
	cmp [part3SIZE - D_OFFSET], dword 0
	je .combine2
	jmp .ncombine2
.combine2:
	mov eax, [gap2SIZE - D_OFFSET]

		clc
	add eax, [gap3SIZE - D_OFFSET]
	jnc .overflowdone2
	sub eax, [gap3SIZE - D_OFFSET]
.overflowdone2:

	mov [gap2SIZE - D_OFFSET], eax
	mov [gap3SIZE - D_OFFSET], dword 0
.ncombine2:


	cmp [part2SIZE - D_OFFSET], dword 0
	je .combine3
	jmp .ncombine3
.combine3:
	mov eax, [gap1SIZE - D_OFFSET]
	
	clc
	add eax, [gap2SIZE - D_OFFSET]
	jnc .overflowdone3
	sub eax, [gap2SIZE - D_OFFSET]
.overflowdone3:

	mov [gap1SIZE - D_OFFSET], eax
	mov [gap2SIZE - D_OFFSET], dword 0
.ncombine3:

.skipcombine:
	ret

gap1SIZE dd 0
gap2SIZE dd 0
gap3SIZE dd 0
gap4SIZE dd 0
gap1LBA dd 0
gap2LBA dd 0
gap3LBA dd 0
gap4LBA dd 0

part1BOOT db 0
part2BOOT db 0
part3BOOT db 0
part4BOOT db 0
part1TYPE db "rRaw Disk", 0
part2TYPE db "rRaw Disk", 0
part3TYPE db "rRaw Disk", 0
part4TYPE db "rRaw Disk", 0
part1LBA dd 0
part2LBA dd 0
part3LBA dd 0
part4LBA dd 0
part1SIZE dd 0
part2SIZE dd 0
part3SIZE dd 0
part4SIZE dd 0

numberAval db 0 
selectedPart db 0

;THIS WOULD BE NICE:
; scrap the current SystemID based system, and replace it with the filesystem ID string
; stored on sector 0 of each partition (offset 0x52, length=8)
; or use offset 0x3, length=3 for OEM ID
; displaying the size of each partition and space should also be implemented at some point

RenderPartitions:
	mov [numberAval - D_OFFSET], byte 0
	
	cmp [selectedPart - D_OFFSET], byte 0
	je .yy1
	.nn1:
		mov si, pointerNo - D_OFFSET
		jmp .ee1
	.yy1:
		mov si, pointerYes - D_OFFSET
	.ee1:
	call puts

	cmp [part1SIZE - D_OFFSET], dword 0
	je .nopart1
	jmp .skip1
.nopart1:
	mov si, partitionnull - D_OFFSET
	call puts
	jmp .noPart1
.skip1:
	inc byte [numberAval - D_OFFSET]
	mov si, partition1 - D_OFFSET
	call puts

	mov si, PartitionTypeStart - D_OFFSET
	call puts
	mov si, part1TYPE - D_OFFSET + 1
	call puts
	mov si, PartitionTypeEnd - D_OFFSET
	call puts
	call ShowSize1

	cmp [part1BOOT - D_OFFSET], byte 0
	je .p1NO
.p1YES:
	mov si, partitionbootyes - D_OFFSET
	jmp .p1END
.p1NO:
	mov si, partitionbootno - D_OFFSET
.p1END:
	call puts
.noPart1:

	cmp [selectedPart - D_OFFSET], byte 1
	je .yy5
	.nn5:
		mov si, pointerNo - D_OFFSET
		jmp .ee5
	.yy5:
		mov si, pointerYes - D_OFFSET
	.ee5:
	call puts
	cmp [gap1SIZE - D_OFFSET], dword 0
	je .xxx1
	.vvv1:
		mov si, partitionunalloc - D_OFFSET
		call puts
		mov si, partitionbootno - D_OFFSET
		call puts
		jmp .www1
	.xxx1:
		mov si, partitionnull - D_OFFSET
		call puts
	.www1:

	cmp [selectedPart - D_OFFSET], byte 2
	je .yy2
	.nn2:
		mov si, pointerNo - D_OFFSET
		jmp .ee2
	.yy2:
		mov si, pointerYes - D_OFFSET
	.ee2:
	call puts
	cmp [part2SIZE - D_OFFSET], dword 0
	je .nopart2
	jmp .skip2
.nopart2:
	mov si, partitionnull - D_OFFSET
	call puts
	jmp .noPart2
.skip2:
	inc byte [numberAval - D_OFFSET]
	mov si, partition2 - D_OFFSET
	call puts

	mov si, PartitionTypeStart - D_OFFSET
	call puts
	mov si, part2TYPE - D_OFFSET + 1
	call puts
	mov si, PartitionTypeEnd - D_OFFSET
	call puts

	call ShowSize2

	cmp [part2BOOT - D_OFFSET], byte 0
	je .p2NO
.p2YES:
	mov si, partitionbootyes - D_OFFSET
	jmp .p2END
.p2NO:
	mov si, partitionbootno - D_OFFSET
.p2END:
	call puts
.noPart2:
	mov si, Newline - D_OFFSET
	call puts


	cmp [selectedPart - D_OFFSET], byte 3
	je .yy6
	.nn6:
		mov si, pointerNo - D_OFFSET
		jmp .ee6
	.yy6:
		mov si, pointerYes - D_OFFSET
	.ee6:
	call puts
	cmp [gap2SIZE - D_OFFSET], dword 0
	je .xxx2
	.vvv2:
		mov si, partitionunalloc - D_OFFSET
		call puts
		mov si, partitionbootno - D_OFFSET
		call puts
		jmp .www2
	.xxx2:
		mov si, partitionnull - D_OFFSET
		call puts
	.www2:

	cmp [selectedPart - D_OFFSET], byte 4
	je .yy3
	.nn3:
		mov si, pointerNo - D_OFFSET
		jmp .ee3
	.yy3:
		mov si, pointerYes - D_OFFSET
	.ee3:
	call puts

	cmp [part3SIZE - D_OFFSET], dword 0
	je .nopart3
	jmp .skip3
.nopart3:
	mov si, partitionnull - D_OFFSET
	call puts
	jmp .noPart3
.skip3:
	inc byte [numberAval - D_OFFSET]
	mov si, partition3 - D_OFFSET
	call puts

	mov si, PartitionTypeStart - D_OFFSET
	call puts
	mov si, part3TYPE - D_OFFSET + 1
	call puts
	mov si, PartitionTypeEnd - D_OFFSET
	call puts

	call ShowSize3

	cmp [part3BOOT - D_OFFSET], byte 0
	je .p3NO
.p3YES:
	mov si, partitionbootyes - D_OFFSET
	jmp .p3END
.p3NO:
	mov si, partitionbootno - D_OFFSET
.p3END:
	call puts
.noPart3:
	mov si, Newline - D_OFFSET
	call puts



	cmp [selectedPart - D_OFFSET], byte 5
	je .yy7
	.nn7:
		mov si, pointerNo - D_OFFSET
		jmp .ee7
	.yy7:
		mov si, pointerYes - D_OFFSET
	.ee7:
	call puts
	cmp [gap3SIZE - D_OFFSET], dword 0
	je .xxx3
	.vvv3:
		mov si, partitionunalloc - D_OFFSET
		call puts
		mov si, partitionbootno - D_OFFSET
		call puts
		jmp .www3
	.xxx3:
		mov si, partitionnull - D_OFFSET
		call puts
	.www3:


	cmp [selectedPart - D_OFFSET], byte 6
	je .yy4
	.nn4:
		mov si, pointerNo - D_OFFSET
		jmp .ee4
	.yy4:
		mov si, pointerYes - D_OFFSET
	.ee4:
	call puts

	cmp [part4SIZE - D_OFFSET], dword 0
	je .nopart4
	jmp .skip4
.nopart4:
	mov si, partitionnull - D_OFFSET
	call puts
	jmp .noPart4
.skip4:
	inc byte [numberAval - D_OFFSET]
	mov si, partition4 - D_OFFSET
	call puts

	mov si, PartitionTypeStart - D_OFFSET
	call puts
	mov si, part4TYPE - D_OFFSET + 1
	call puts
	mov si, PartitionTypeEnd - D_OFFSET
	call puts
	call ShowSize4

	cmp [part4BOOT - D_OFFSET], byte 0
	je .p4NO
.p4YES:
	mov si, partitionbootyes - D_OFFSET
	jmp .p4END
.p4NO:
	mov si, partitionbootno - D_OFFSET
.p4END:
	call puts
.noPart4:
	mov si, Newline - D_OFFSET
	call puts

	;cmp byte [numberAval - D_OFFSET], 4
	;je .skipUnalloc

	cmp [selectedPart - D_OFFSET], byte 7
	je .yy8
	.nn8:
		mov si, pointerNo - D_OFFSET
		jmp .ee8
	.yy8:
		mov si, pointerYes - D_OFFSET
	.ee8:
	call puts
	cmp [gap4SIZE - D_OFFSET], dword 0
	je .xxx4
	.vvv4:
		mov si, partitionunalloc - D_OFFSET
		call puts
		mov si, partitionbootno - D_OFFSET
		call puts
		jmp .www4
	.xxx4:
		mov si, partitionnull - D_OFFSET
		call puts
	.www4:

.skipUnalloc:
	ret

Stage101:
	call ResetScreen
	mov si, Stage101Msg - D_OFFSET
	call puts
	call FlushKeybuffer
.AwaitKeyPress:
	xor ax, ax
	int 0x16

	cmp ah, 0x1C		;0x41			;F7
	je .createMSR
	cmp ah, 0x01
	je shutdown
	cmp ah, 0x48
	jmp .AwaitKeyPress

.createMSR:
	;read sample
	mov ah, 0x42
	mov al, 0
	mov dl, byte [driveNumber - D_OFFSET]
	mov si, HDDZEROPACKET2 - D_OFFSET
	mov [BLK2 - D_OFFSET], word 1					;read 1 sector

	pusha
	mov ebx, defaultmbrstring
	mov cx , 12				;must include null terminator
	mov [cdReturnSpot], byte 2
	call readFromCD
	dfltmbrread:
	mov [RDF2 - D_OFFSET], eax				;sector 59 has the sample MBR
	popa
	int 0x13

	;write sample
	mov ah, 0x43
	mov al, 0
	mov dl, byte [HDDNumber - D_OFFSET]
	mov si, HDDZEROPACKET2 - D_OFFSET
	mov [BLK2 - D_OFFSET], word 1					;write 1 sector
	mov [RDF2 - D_OFFSET], dword 0				;write to sector 0
	int 0x13

	jmp Stage100



Stage100:
	mov byte [numberAval - D_OFFSET], 0
	mov byte [selectedPart - D_OFFSET], 0
	mov dword [selectedSpaceLBA - D_OFFSET], 0
	mov dword [selectedSpaceSIZE - D_OFFSET], 0

	call ResetScreen
	mov si, Stage100MsgWait - D_OFFSET
	call puts

	call LoadMBR

Stage100P3:
	mov byte [selectedPart - D_OFFSET], 0
Stage100P2:
	call ResetScreen
	call RenderPartitions	;load the numberAval byte
	call FlushKeybuffer
.AwaitKeyPress:
	call ResetScreen
	mov si, Stage100Msg - D_OFFSET
	call puts

	call RenderPartitions

	xor ax, ax
	int 0x16

	cmp ah, 0x1C
	je EndStage100
	cmp ah, 0x1E		;'A' key
	je MakeActive
	cmp ah, 0x01
	je Stage101		;shutdown
	cmp ah, 0x48
	je .uparrow
	cmp ah, 0x50
	je .downarrow
	jmp .AwaitKeyPress

.downarrow:
	cmp [selectedPart - D_OFFSET], byte 7
	je .AwaitKeyPress
	inc byte [selectedPart - D_OFFSET]
	jmp .AwaitKeyPress
.uparrow:
	cmp [selectedPart - D_OFFSET], byte 0
	je .AwaitKeyPress
	dec byte [selectedPart - D_OFFSET]
	jmp .AwaitKeyPress

EndStage100:
	cmp [selectedPart - D_OFFSET], byte 0
	je .sp0
	cmp [selectedPart - D_OFFSET], byte 1
	je .sp1
	cmp [selectedPart - D_OFFSET], byte 2
	je .sp2
	cmp [selectedPart - D_OFFSET], byte 3
	je .sp3
	cmp [selectedPart - D_OFFSET], byte 4
	je .sp4
	cmp [selectedPart - D_OFFSET], byte 5
	je .sp5
	cmp [selectedPart - D_OFFSET], byte 6
	je .sp6
	cmp [selectedPart - D_OFFSET], byte 7
	je .sp7
	mov [selectedPart - D_OFFSET], byte 0
	jmp Stage100P2

.sp0:
	mov eax, [part1LBA - D_OFFSET]
	mov ebx, [part1SIZE - D_OFFSET]
	jmp .sp8
.sp1:
	mov eax, [gap1LBA - D_OFFSET]
	mov ebx, [gap1SIZE - D_OFFSET]
	jmp .sp8
.sp2:
	mov eax, [part2LBA - D_OFFSET]
	mov ebx, [part2SIZE - D_OFFSET]
	jmp .sp8
.sp3:
	mov eax, [gap2LBA - D_OFFSET]
	mov ebx, [gap2SIZE - D_OFFSET]
	jmp .sp8
.sp4:
	mov eax, [part3LBA - D_OFFSET]
	mov ebx, [part3SIZE - D_OFFSET]
	jmp .sp8
.sp5:
	mov eax, [gap3LBA - D_OFFSET]
	mov ebx, [gap3SIZE - D_OFFSET]
	jmp .sp8
.sp6:
	mov eax, [part4LBA - D_OFFSET]
	mov ebx, [part4SIZE - D_OFFSET]
	jmp .sp8
.sp7:
	mov eax, [gap4LBA - D_OFFSET]
	mov ebx, [gap4SIZE - D_OFFSET]
	jmp .sp8
.sp8:
	mov [selectedSpaceLBA - D_OFFSET], eax
	mov [selectedSpaceSIZE - D_OFFSET], ebx

	cmp ebx, 131072		;64 MB
	jbe partitionTooSmall

	;IF you are using a readymade partition, set the active byte
	;IF you are using an unpartitioned area, write partition values (LBA, SIZE, boot flag, etc.)

	mov al, [selectedPart - D_OFFSET]
	and al, 1

	mov ah, [selectedPart - D_OFFSET]
	shr ah, 1
	mov [selectedPart - D_OFFSET], ah			;write back the PROPER PARTITION VALUE (e.g. 0-3 not 0-7)

	cmp al, 0
	je .skipCreation

	;find a blank partition Number
	;and put in [selectedPart - D_OFFSET]

	cmp [part1SIZE - D_OFFSET], dword 0
	je .found1
	cmp [part2SIZE - D_OFFSET], dword 0
	je .found2
	cmp [part3SIZE - D_OFFSET], dword 0
	je .found3
	cmp [part4SIZE - D_OFFSET], dword 0
	je .found4
	
	call ResetScreen
	mov si, NoBlankPartitionsMessage - D_OFFSET
	call puts

.AwaitKeyPressXX:
	xor ax, ax
	int 0x16

	cmp ah, 0x1C	;ENTER
	je Stage100P3
	jmp .AwaitKeyPressXX

.found1:
	mov byte [selectedPart - D_OFFSET], 0
	jmp .endfind
.found2:
	mov byte [selectedPart - D_OFFSET], 1
	jmp .endfind
.found3:
	mov byte [selectedPart - D_OFFSET], 2
	jmp .endfind
.found4:
	mov byte [selectedPart - D_OFFSET], 3
.endfind:

	mov ax, 0
	mov fs, ax		;get segment ready

	mov ah, 0x42		;read
	mov al, 0
	mov dl, byte [HDDNumber - D_OFFSET]		;hard drive
	mov [BLK2 - D_OFFSET], dword 1			;1 sector
	mov si, HDDZEROPACKET2 - D_OFFSET
	mov [RDF2 - D_OFFSET], dword 0			;sector 0 is the MBR
	;will load to 0x7000
	int 0x13

	mov ax, 0
	mov fs, ax		;get segment ready

	mov bx, 0x71BE + 8
	mov al, [selectedPart - D_OFFSET]
	mov ah, 0
	shl ax, 4					;multiply 16 because 16 bytes per PARTITION
	add bx, ax
	mov eax, [selectedSpaceLBA - D_OFFSET]
	add eax, 128			;so we do not override sector 0
	mov dword [fs:bx], eax	;write LBA
	add bx, 4
	mov eax, [selectedSpaceSIZE - D_OFFSET]
	add eax, 128
	cmp eax, 0xFFFFF
	ja .gr
	jmp .ls
.gr:
	mov eax, 0xFFFFF
.ls:
	mov dword [fs:bx], eax	;write size

	mov ah, 0x43
	mov al, 0
	mov dl, byte [HDDNumber - D_OFFSET]		;hard drive
	mov [BLK2 - D_OFFSET], dword 1			;1 sector
	mov si, HDDZEROPACKET2 - D_OFFSET
	mov [RDF2 - D_OFFSET], dword 0
	int 0x13

	jmp Stage100

.skipCreation:
	mov ax, 0
	mov fs, ax		;get segment ready

	mov ah, 0x42		;read
	mov al, 0
	mov dl, byte [HDDNumber - D_OFFSET]		;hard drive
	mov [BLK2 - D_OFFSET], dword 1			;1 sector
	mov si, HDDZEROPACKET2 - D_OFFSET
	mov [RDF2 - D_OFFSET], dword 0			;sector 0 is the MBR
	;will load to 0x7000
	int 0x13

	;make no-one active
	mov bx, 0x71BE
	mov byte [fs:bx], 0
	add bl, 0x10
	mov byte [fs:bx], 0
	add bl, 0x10
	mov byte [fs:bx], 0
	add bl, 0x10
	mov byte [fs:bx], 0
	
	;then write the make someone active
	mov ax, 0
	mov fs, ax		;get segment ready
	mov bx, 0x71BE
	mov al, [selectedPart - D_OFFSET]
	mov ah, 0
	shl ax, 4					;multiply 16 because 16 bytes per PARTITION
	add bx, ax
	mov byte [fs:bx], 0x80	;set active flag
	add bx, 4
	mov byte [fs:bx], 0x0C	;FAT32

	mov ah, 0x43
	mov al, 0
	mov dl, byte [HDDNumber - D_OFFSET]		;hard drive
	mov [BLK2 - D_OFFSET], dword 1			;1 sector
	mov si, HDDZEROPACKET2 - D_OFFSET
	mov [RDF2 - D_OFFSET], dword 0
	;will write to 0x7000

	int 0x13

	ret





Stage102GPT:
	call ResetScreen
	mov si, Stage102Msg - D_OFFSET
	call puts

	call FlushKeybuffer

.AwaitKeyPress:
	xor ax, ax
	int 0x16

	cmp al, 'Y'
	je UEFIandGPTStuff
	cmp al, 'n'
	je Stage101
	cmp al, 'N'
	je Stage101
	cmp al, 'Q'
	je shutdown
	cmp al, 'q'
	je shutdown
	jmp .AwaitKeyPress

UEFIandGPTStuff:
	jmp $

MakeActive:
	mov al, byte [selectedPart - D_OFFSET]

	cmp byte [selectedPart - D_OFFSET], 0
	je .found
	cmp byte [selectedPart - D_OFFSET], 2
	je .found
	cmp byte [selectedPart - D_OFFSET], 4
	je .found
	cmp byte [selectedPart - D_OFFSET], 6
	je .found
	mov [selectedPart - D_OFFSET], byte 0
	jmp Stage100P2

.found:
	mov ax, 0
	mov fs, ax		;get segment ready

	mov ah, 0x42		;read
	mov al, 0
	mov dl, byte [HDDNumber - D_OFFSET]		;hard drive
	mov [BLK2 - D_OFFSET], dword 1			;1 sector
	mov si, HDDZEROPACKET2 - D_OFFSET
	mov [RDF2 - D_OFFSET], dword 0			;sector 0 is the MBR
	;will load to 0x7000
	int 0x13

	;make no-one active
	mov bx, 0x71BE
	mov byte [fs:bx], 0
	add bl, 0x10
	mov byte [fs:bx], 0
	add bl, 0x10
	mov byte [fs:bx], 0
	add bl, 0x10
	mov byte [fs:bx], 0
	
	;then write the make someone active
	mov ax, 0
	mov fs, ax		;get segment ready
	mov bx, 0x71BE
	mov al, [selectedPart - D_OFFSET]
	shr al, 1					;turn 0,2,4,6 into 0,1,2,3
	mov ah, 0
	shl ax, 4					;multiply 16 because 16 bytes per PARTITION
	add bx, ax
	mov byte [fs:bx], 0x80	;set active flag

	mov ah, 0x43
	mov al, 0
	mov dl, byte [HDDNumber - D_OFFSET]		;hard drive
	mov [BLK2 - D_OFFSET], dword 1			;1 sector
	mov si, HDDZEROPACKET2 - D_OFFSET
	mov [RDF2 - D_OFFSET], dword 0
	;will write to 0x7000

	int 0x13


	jmp Stage100


partitionTooSmall:
	call ResetScreen

	mov si, partitionSmallMsg - D_OFFSET
	call puts

.AwaitKeyPressXX:
	xor ax, ax
	int 0x16

	cmp ah, 0x1C	;ENTER
	je Stage100P2
	jmp .AwaitKeyPressXX


chardisplay db 0
dd 0
sizeintro db "[Size: ", 0
times 16 db 0 ;padding
sizeendkb db "KB]", 0
times 16 db 0 ;padding
sizeendmb db "MB]", 0
times 16 db 0 ;padding
sizeout times 16 db 0
megab db 0 
times 16 db 0 ;padding



ShowSize1:
	mov eax, [part1SIZE - D_OFFSET]
	jmp ShowSizeCommon
ShowSize2:
	mov eax, [part2SIZE - D_OFFSET]
	jmp ShowSizeCommon
ShowSize3:
	mov eax, [part3SIZE - D_OFFSET]
	jmp ShowSizeCommon
ShowSize4:
	mov eax, [part4SIZE - D_OFFSET]

ShowSizeCommon:
	mov si, sizeintro - D_OFFSET
	push eax
	call puts
	pop eax

	mov [sizeout], dword 0
	mov [sizeout + 4], dword 0

	mov [megab], byte 0
	mov di, 0
	shr eax, 1			;convert to KB (they're currently in sectors, so dividing by 2 gives KBs, don't multiply by 512 then div 1024 because it might overflow)
	cmp eax, 1024 * 4
	jb .KB

	shr eax, 10	;convert to MB
	mov [megab], byte 1

.KB:
.looping:
	mov ecx, 0
    .divisions:
        cmp eax, 10
        jl  .finish        ; IF DIVIDEND < DIVISOR
        sub eax, 10		; DIVIDEND - DIVISOR. EXAMPLE :  23 - 4.
        inc ecx      ; DIVISIONS COUNTER.
        jmp .divisions
    .finish:
	mov ebx, eax		;EAX has remainder
	mov eax, ecx		;ECX has quotent

	;mov ebx, eax
	;and bl, 7
	;shr eax, 3

	add bl, '0'			;make it nicer to display
	mov [sizeout + di], bl
	inc di
	
	cmp eax, 0
	je .donelooping

	jmp .looping

.donelooping:
	;time to reverse the string 'sizeout'

	mov ecx, 0
	mov cx, di
	mov eax, sizeout
	mov esi, eax  ; esi points to start of string
	add eax, ecx
	mov edi, eax
	dec edi       ; edi points to end of string
	shr ecx, 1    ; ecx is count (length/2)
	jz done       ; if string is 0 or 1 characters long, done
	reverseLoop:
	mov al, [esi] ; load characters
	mov bl, [edi]
	mov [esi], bl ; and swap
	mov [edi], al
	inc esi       ; adjust pointers
	dec edi
	dec ecx       ; and loop
	jnz reverseLoop
done:
	mov si, sizeout
	call puts

	cmp [megab], byte 0
	je .kbb
	mov si, sizeendmb - D_OFFSET
	jmp .endd
.kbb:
	mov si, sizeendkb - D_OFFSET
.endd:
	call puts

	ret