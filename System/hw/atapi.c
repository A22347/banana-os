#include <hw/atapi.h>
#include <hal/interrupts.h>
#include <hw/ports.h>
#include <hw/ide.h>

void ide_wait_irq()
{
	asm("sti");
	while (!ide_irq_invoked) {
		logk("ide_wait_irq\n");
	}
	ide_irq_invoked = 0;
}

void ide_irq(struct regs* r)
{
	logk("ide_irq was called\n");
	ide_irq_invoked = 1;
}

void ide_wait_irq2()
{
	asm("sti");
	while (!ide_irq_invoked2) {
		logk("ide_wait_irq2\n");
	}
	ide_irq_invoked2 = 0;
}

void ide_irq2(struct regs* r)
{
	logk("ide_irq2 was called\n");
	ide_irq_invoked2 = 1;
}

//https://forum.osdev.org/viewtopic.php?f=1&t=33511

#define ATAPI_PIO_MODE 0
#define ATAPI_DMA_MODE 1

uint8_t global = 0xFF;

void atapi_eject(uint8_t channel, uint16_t base, uint8_t drive)
{
	uint8_t select = 0xE0 | (drive << 4);
	if (global != select) {
		outb(base + 6, select);
		sleep(4); // Wait 1ms for drive select to work.
		global = select;
	}

	int timeout = 0;
	while ((inb(base + ATA_REG_STATUS) & 0x40) != 0x40) {
		if (timeout++ == 100000) break;
	}

	outb(base + ATA_REG_FEATURES, 0);

	outb(base + ATA_REG_COMMAND, 0xA0);

	uint8_t byte4 = 0b10;
	/*
	00 = Stop the disc
	01 = Start the disc and acquire the format type
	10 = eject the disk if possible
	11 = load hte disk (close tray)
	*/

	uint8_t read_cmd[12] = { 0x1B, 0, 0, 0, byte4, 0, 0, 0, 0, 0, 0, 0 };

	asm("sti");
	ide_irq_invoked = 0;
	ide_irq_invoked2 = 0;

	// Send ATAPI/SCSI command
	outsw(base + 0x0, (uint16_t*) read_cmd, 6);

	//while ((inb(base + ATA_REG_STATUS) & 0x8) != 0x08) {}
	
	timeout = 0;
	if (base == 0x1F0) {
		while (!ide_irq_invoked) {
			//alt status is at 0x3F6/0x376 and doesn't mess with interrupts
			if ((inb(base + 0x206) & 0x8) == 0x08) break;
			if (timeout++ == 200000) {
				logk("ATAPI EJECT TIMEOUT!\n");
				break;
			}
		}
		ide_irq_invoked = 0;
	} else {
		while (!ide_irq_invoked2) {
			//alt status is at 0x3F6/0x376 and doesn't mess with interrupts
			if ((inb(base + 0x206) & 0x8) == 0x08) break;
			if (timeout++ == 200000) {
				logk("ATAPI EJECT TIMEOUT!\n");
				break;
			}
		}
		ide_irq_invoked2 = 0;
	}
}

void atapi_read(uint32_t sector, uint8_t channel, uint16_t base, uint8_t drive, uint16_t* buffer)
{
	uint8_t select = 0xE0 | (drive << 4) | ((sector & 0x0F000000) >> 24);
	if (global != select) {
		outb(base + 6, select);
		sleep(4); // Wait 1ms for drive select to work.
		global = select;
	}

	int timeout = 0;
	while ((inb(base + ATA_REG_STATUS) & 0x40) != 0x40) {
		if (timeout++ == 100000) break;
	}

	// Enable IRQs
	ide_write(ATA_PRIMARY, ATA_REG_CONTROL, 0);
	ide_write(ATA_SECONDARY, ATA_REG_CONTROL, 0);

	outb(base + ATA_REG_FEATURES, 0);
	outb(base + ATA_REG_SECCOUNT0, 0);
	outb(base + ATA_REG_LBA0, 0);
	outb(base + ATA_REG_LBA1, 2048 & 0xFF);
	outb(base + ATA_REG_LBA2, 2048 >> 8);
	outb(base + ATA_REG_COMMAND, 0xA0);

	asm("sti");
	ide_irq_invoked  = 0;
	ide_irq_invoked2 = 0;

	uint8_t read_cmd[12] = { 0xA8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
	read_cmd[9] = 1;              // 1 sector
	read_cmd[2] = (sector >> 0x18) & 0xFF;   // most sig. byte of LBA
	read_cmd[3] = (sector >> 0x10) & 0xFF;
	read_cmd[4] = (sector >> 0x08) & 0xFF;
	read_cmd[5] = (sector >> 0x00) & 0xFF;   // least sig. byte of LBA

	// Send ATAPI/SCSI command
	outsw(base + 0x0, (uint16_t*) read_cmd, 6);

	timeout = 0;
	if (base == 0x1F0) {
		while (!ide_irq_invoked) {
			//alt status is at 0x3F6/0x376 and doesn't mess with interrupts
			if ((inb(base +0x206) & 0x8) == 0x08) break;
			if (timeout++ == 200000) {
				logk("ATAPI RED TIMEOUT!\n");
				break;
			}
		}
		ide_irq_invoked = 0;
	} else {
		while (!ide_irq_invoked2) {
			//alt status is at 0x3F6/0x376 and doesn't mess with interrupts
			if ((inb(base + 0x206) & 0x8) == 0x08) break;
			if (timeout++ == 200000) {
				logk("ATAPI READ TIMEOUT!\n");
				break;
			}
		}
		ide_irq_invoked2 = 0;
	}

	//Read length of input/output
	uint16_t size = ( (((uint16_t) inb(base + 5)) << 8)) | ((((uint16_t) inb(base + 4)) << 0));
	logk("SIZE IS 0x%X\n", size);

	asm("rep insw"::"c"(1024), "d"(base), "D"(buffer));// Receive Data.
	for (int i = 0; i < 1024; i++) {
		logk("BUFFER[%d] = 0x%X\n", i, buffer[i]);
	}
}

void openATAPI(Device* self, int b, int c, void* d)
{
	logk("ATAPI open device no. %d\n", b);
	self->internalDataSize = b;

	logk("HW IRQ HANDLER 14 MAPS TO 0x%X\n", legacyIRQRemaps[14]);
	logk("HW IRQ HANDLER 15 MAPS TO 0x%X\n", legacyIRQRemaps[15]);

	installHardwareInterruptHandler(legacyIRQRemaps[14], ide_irq);
	installHardwareInterruptHandler(legacyIRQRemaps[15], ide_irq2);
}

void readATAPI(Device* self, int b, int c, void* d)
{
	logk("ATAPI open device no. %d\n", self->internalDataSize);
	logk("sector 0x%X\n", b);
	logk("sectors 0x%X\n", c);
	logk("buffer 0x%X\n", d);

	atapi_read(b, ide_devices[self->internalDataSize].channel, ideChannels[ide_devices[self->internalDataSize].channel].base, ide_devices[self->internalDataSize].drive, d);
}

void writeATAPI(Device* self, int b, int c, void* d)
{
}

/*
#define ATAPI_SECTOR_SIZE 2048

#define ATA_DCR(x)          (x+0x206)   // device control register

#define ATA_IRQ_PRIMARY     0x0E
#define ATA_IRQ_SECONDARY   0x0F

#define ATA_BUS_PRIMARY     0x1F0
#define ATA_BUS_SECONDARY   0x170

#define ATA_DRIVE_MASTER    0xA0
#define ATA_DRIVE_SLAVE     0xB0

#define ATA_DELAY(bus) \
{inb(ATA_DCR(bus));inb(ATA_DCR(bus));inb(ATA_DCR(bus));inb(ATA_DCR(bus));}

int atapiReadSector(uint32_t bus, uint32_t drive, uint32_t lba, uint8_t* buffer)
{
	uint8_t read_cmd[12] = { 0xA8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
	int size = 0;

	// Select drive (only the slave-bit is set)
	outb(bus + 0xF, drive & (1 << 4));
	ATA_DELAY(bus);

	outb(bus + 0x1, 0x0);       // PIO mode
	outb(bus + 0x4, ATAPI_SECTOR_SIZE & 0xFF);
	outb(bus + 0x5, ATAPI_SECTOR_SIZE >> 8);
	outb(bus + 0x7, 0xA0);      // ATA PACKET command

	uint8_t status;

	while ((status = inb(bus + 0x7)) & 0x80) {
		logk("we're pausing...\n");
		asm volatile ("pause");
	}

	while (!((status = inb(bus + 0x7)) & 0x8) && !(status & 0x1)) {
		logk("we're pausing again...\n");
		asm volatile ("pause");
	}

	// DRQ or ERROR set
	if (status & 0x1) {
		size = -1;
		goto cleanup;
	}

	read_cmd[9] = 1;              // 1 sector
	read_cmd[2] = (lba >> 0x18) & 0xFF;   // most sig. byte of LBA
	read_cmd[3] = (lba >> 0x10) & 0xFF;
	read_cmd[4] = (lba >> 0x08) & 0xFF;
	read_cmd[5] = (lba >> 0x00) & 0xFF;   // least sig. byte of LBA

	// Send ATAPI/SCSI command
	outsw(bus + 0x0, (uint16_t*) read_cmd, 6);

	// Wait for IRQ
	if (bus == 0x1F0) {
		ide_wait_irq();
	}  else {
		ide_wait_irq2();
	}

	// Read actual size
	size = (((int) inb(bus + 0x5)) << 8) | (int) (inb(bus + 0x4));

	// This example code only supports the case where the data transfer
	// of one sector is done in one step.
	//assert(size == ATAPI_SECTOR_SIZE);

	// Read data.
	insw(bus + 0x0, buffer, size / 2);

	// The controller will send another IRQ even though we've read all
	// the data we want.  Wait for it -- so it doesn't interfere with
	// subsequent operations:
	if (bus == 0x1F0) {
		ide_wait_irq();
	} else {
		ide_wait_irq2();
	}

	// Wait for BSY and DRQ to clear, indicating Command Finished 
	while ((status = inb(bus + 0x7)) & 0x88) {
		logk("we're pausing for the 3rd time...\n");
		asm volatile ("pause");
	}

cleanup:
	return size;
}


void openATAPI(Device* self, int b, int c, void* d)
{
	logk("ATAPI open device no. %d\n", b);
	self->internalDataSize = b;

	installHardwareInterruptHandler(legacyIRQRemaps[14], ide_irq);
	installHardwareInterruptHandler(legacyIRQRemaps[15], ide_irq2);

}

void readATAPI(Device* self, int b, int c, void* d)
{
	logk("ATAPI open device no. %d\n", self->internalDataSize);
	logk("sector 0x%X\n", b);
	logk("sectors 0x%X\n", c);
	logk("buffer 0x%X\n", d);

	uint16_t bus = 0x1F0;
	if (self->internalDataSize & 0b10) {
		bus = 0x170;
	}
	uint16_t drive = 0xA0;
	if (self->internalDataSize & 0b1) {
		bus = 0xB0;
	}
	logk("BUS = 0x%X, DRIVE = 0x%X\n", bus, drive);
	for (int i = 0; i < c; ++i) {
		atapiReadSector(bus, drive, b + i, ((uint8_t*)d) + i * 2048);
	}

	//cdrom_read_sector(self->internalDataSize, b, c, d);
}

void writeATAPI(Device* self, int b, int c, void* d)
{

}
*/


/*unsigned char ide_atapi_read(unsigned char drive, unsigned int lba, unsigned char numsects,
							 uint8_t* buffer)
{
	unsigned int   channel = ide_devices[drive].channel;
	unsigned int   slavebit = ide_devices[drive].drive;
	unsigned int   bus = ideChannels[channel].base;
	unsigned int   words = 1024; // Sector Size. ATAPI drives have a sector size of 2048 bytes.
	unsigned char  err;

	logk("CHNL = 0x%X, BUS = 0x%X, SLAVE = 0x%X\n", channel, bus, slavebit);

	// Enable IRQs:
	if (bus == 0x1F0) {
		ide_write(channel, ATA_REG_CONTROL, ideChannels[channel].nIEN = ide_irq_invoked = 0x0);
	} else {
		ide_write(channel, ATA_REG_CONTROL, ideChannels[channel].nIEN = ide_irq_invoked2 = 0x0);
	}

	// (I): Setup SCSI Packet:
// ------------------------------------------------------------------
	atapi_packet[0] = ATAPI_CMD_READ;
	atapi_packet[1] = 0x0;
	atapi_packet[2] = (lba >> 24) & 0xFF;
	atapi_packet[3] = (lba >> 16) & 0xFF;
	atapi_packet[4] = (lba >> 8) & 0xFF;
	atapi_packet[5] = (lba >> 0) & 0xFF;
	atapi_packet[6] = 0x0;
	atapi_packet[7] = 0x0;
	atapi_packet[8] = 0x0;
	atapi_packet[9] = numsects;
	atapi_packet[10] = 0x0;
	atapi_packet[11] = 0x0;

	// (II): Select the drive:
// ------------------------------------------------------------------
	ide_write(channel, ATA_REG_HDDEVSEL, slavebit << 4);

	// (III): Delay 400 nanoseconds for select to complete:
// ------------------------------------------------------------------
	for (int i = 0; i < 4; i++)
		ide_read(channel, ATA_REG_ALTSTATUS); // Reading the Alternate Status port wastes 100ns.

	   // (IV): Inform the Controller that we use PIO mode:
   // ------------------------------------------------------------------
	ide_write(channel, ATA_REG_FEATURES, 0);         // PIO mode.

	   // (V): Tell the Controller the size of buffer:
   // ------------------------------------------------------------------
	ide_write(channel, ATA_REG_LBA1, (words * 2) & 0xFF);   // Lower Byte of Sector Size.
	ide_write(channel, ATA_REG_LBA2, (words * 2) >> 8);   // Upper Byte of Sector Size.

	   // (VI): Send the Packet Command:
   // ------------------------------------------------------------------
	ide_write(channel, ATA_REG_COMMAND, ATA_CMD_PACKET);      // Send the Command.

	// (VII): Waiting for the driver to finish or return an error code:
	// ------------------------------------------------------------------
	if ((err = ide_polling(channel, 1))) return err;         // Polling and return if error.

	// (VIII): Sending the packet data:
	// ------------------------------------------------------------------
	asm("rep   outsw" : : "c"(6), "d"(bus), "S"(atapi_packet));   // Send Packet Data

	logk("TESTING\n");
	// (IX): Receiving Data:
   // ------------------------------------------------------------------
	for (int i = 0; i < numsects; i++) {
		//ide_wait_irq();                  // Wait for an IRQ.
		if (bus == 0x1F0) {
			ide_wait_irq();
		} else {
			ide_wait_irq2();
		}
		if ((err = ide_polling(channel, 1)))
			return err;      // Polling and return if error.
		asm("rep insw"::"c"(words), "d"(bus), "D"(buffer));// Receive Data.
		buffer += (words * 2);
		
	}

	// (X): Waiting for an IRQ:
	// ------------------------------------------------------------------
	//ide_wait_irq();
	if (bus == 0x1F0) {
		ide_wait_irq();
	} else {
		ide_wait_irq2();
	}

	// (XI): Waiting for BSY & DRQ to clear:
	// ------------------------------------------------------------------
	while (ide_read(channel, ATA_REG_STATUS) & (ATA_SR_BSY | ATA_SR_DRQ))
		;

	return 0; // Easy, ... Isn't it?
}

//drive is AN INTEGER VALUE WHEN USED HERE AND IS AN ATA DRIVE NUMBER
void cdrom_read_sector (int driveno, uint32_t sector, uint32_t sectors, uint8_t* buffer)
{
	logk ("cdrom_read_sector called\n");
	ide_atapi_read(driveno, sector, sectors, buffer);
}

void cdrom_eject(int drive)
{
	unsigned int   channel = ide_devices[drive].channel;
	unsigned int   slavebit = ide_devices[drive].drive;
	unsigned int   bus = ideChannels[channel].base;
	unsigned int   words = 2048 / 2;               // Sector Size in Words.
	unsigned char  err = 0;
	ide_irq_invoked = 0;

	// 1: Check if the drive presents:
	// ==================================
	if (drive > 3 || ide_devices[drive].reserved == 0) return;
		//package[0] = 0x1;      // Drive Not Found!
	 // 2: Check if drive isn't ATAPI:
	 // ==================================
	else if (ide_devices[drive].type == IDE_ATA)  return;
		//package[0] = 20;         // Command Aborted.
	 // 3: Eject ATAPI Driver:
	 // ============================================
	else {
		// Enable IRQs:

		ide_write(channel, ATA_REG_CONTROL, ideChannels[channel].nIEN = ide_irq_invoked = 0x0);

		// (I): Setup SCSI Packet:
		// ------------------------------------------------------------------
		atapi_packet[0] = ATAPI_CMD_EJECT;
		atapi_packet[1] = 0x00;
		atapi_packet[2] = 0x00;
		atapi_packet[3] = 0x00;
		atapi_packet[4] = 0x02;
		atapi_packet[5] = 0x00;
		atapi_packet[6] = 0x00;
		atapi_packet[7] = 0x00;
		atapi_packet[8] = 0x00;
		atapi_packet[9] = 0x00;
		atapi_packet[10] = 0x00;
		atapi_packet[11] = 0x00;

		// (II): Select the Drive:
		// ------------------------------------------------------------------
		ide_write(channel, ATA_REG_HDDEVSEL, slavebit << 4);

		// (III): Delay 400 nanosecond for select to complete:
		// ------------------------------------------------------------------
		for (int i = 0; i < 4; i++)
			ide_read(channel, ATA_REG_ALTSTATUS); // Reading Alternate Status Port wastes 100ns.

		 // (IV): Send the Packet Command:
		 // ------------------------------------------------------------------
		ide_write(channel, ATA_REG_COMMAND, ATA_CMD_PACKET);      // Send the Command.

		// (V): Waiting for the driver to finish or invoke an error:
		// ------------------------------------------------------------------
		err = ide_polling(channel, 1);            // Polling and stop if error.

		// (VI): Sending the packet data:
		// ------------------------------------------------------------------
	  asm("rep   outsw"::"c"(6), "d"(bus), "S"(atapi_packet));// Send Packet Data
	  //ide_wait_irq();                  // Wait for an IRQ.
	  if (bus == 0x1F0) {
		  ide_wait_irq();
	  } else {
		  ide_wait_irq2();
	  }
	  err = ide_polling(channel, 1);            // Polling and get error code.
	  if (err == 3) err = 0; // DRQ is not needed here.
	}
}

void openATAPI(Device* self, int b, int c, void* d)
{
	logk("ATAPI open device no. %d\n", b);
	self->internalDataSize = b;

	logk("HW IRQ HANDLER 14 MAPS TO 0x%X\n", legacyIRQRemaps[14]);
	logk("HW IRQ HANDLER 15 MAPS TO 0x%X\n", legacyIRQRemaps[15]);

	installHardwareInterruptHandler(legacyIRQRemaps[14], ide_irq);
	installHardwareInterruptHandler(legacyIRQRemaps[15], ide_irq2);
}

void readATAPI(Device* self, int b, int c, void* d)
{
	logk("ATAPI open device no. %d\n", self->internalDataSize);
	logk("sector 0x%X\n", b);
	logk("sectors 0x%X\n", c);
	logk("buffer 0x%X\n", d);
	cdrom_read_sector(self->internalDataSize, b, c, d);
}

void writeATAPI(Device* self, int b, int c, void* d)
{
}
*/

