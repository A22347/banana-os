//
//  main.c
//  cmd
//
//  Created by Alex Boxall on 29/4/19.
//  Copyright � 2019 Alex Boxall. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <stdbool.h>
#include <sys/stat.h>
#include <fcntl.h>

#include "D:/Users/Alex/Desktop/banana-os/System/core/syscalldef.h"
#include <syscall.h>

//                0x0000000010000bd9                write

// error at 0x10000BF7

#define MAX_ARGS 10
#define MAX_ARG_LENGTH 100
#define MAX_INPUT_LENGTH (MAX_ARGS * (MAX_ARG_LENGTH + 4) + 32)

char cwd[256];

void displayCopyright()
{
	system("clear");
	printf("Banana Command Prompt v0.3.0\nCopyright Alex Boxall 2019. All rights reserved.\n\n");
}

void displayPrompt()
{
	getcwd(cwd, 255);
	printf("%s> ", cwd);
}

int main(int argc, const char* argv[])
{
	displayCopyright();
	
	strcpy(cwd, "C:/");
	//runBatch("C:/Banana/System/AUTOEXEC.BAT");

	while (1) {
		displayPrompt();

		char* input = calloc(MAX_INPUT_LENGTH, 1);
		fgets(input, MAX_INPUT_LENGTH - 1, stdin);

		char arguments[MAX_ARGS][MAX_ARG_LENGTH];
		memset(arguments, 0, MAX_ARGS * MAX_ARG_LENGTH);

		int argNo = 0;
		int argIndex = 0;
		bool fail = false;
		bool string = false;
		bool escaped = false;

		for (int i = 0; input[i]; ++i) {
			if (input[i] == '\n') break;
			if (!string && input[i] == '\\' && !escaped) {
				escaped = true;
				continue;
			}
			if ((input[i] == '"' || input[i] == '\'') && !escaped) {
				string ^= 1;
				continue;
			}
			if (input[i] == ' ' && !string && !escaped) {
				argIndex = 0;
				argNo++;
				if (argNo > MAX_ARGS) {
					printf("Too many arguments!\n\n");
					fail = true;
					break;
				}
				continue;
			}
			arguments[argNo][argIndex++] = input[i];
			escaped = false;
		}

		free(input);

		if (fail || strlen(arguments[0]) == 0) {
			continue;
		}

		if (!strcmp(arguments[0], "cd")) {
			if (arguments[1][0]) {
				int res = chdir(arguments[1]);
				if (res) {
					printf("The system cannot find the path specified.\n\n");
				}
			} else {
				printf("%s\n\n", cwd);
			}
			continue;
		} else if (!strcmp(arguments[0], "rem")) {
			continue;
		} else if (!strcmp(arguments[0], "copy")) {
			if (!arguments[1][0] || !arguments[2][0]) {
				printf("Please enter two file or directory paths.\n\n");
			} else {
				FILE* rd = fopen(arguments[1], "rb");
				if (!rd) {
					printf("The source file could not be opened.\n\n");
					continue;
				}
				FILE* wr = fopen(arguments[2], "wb");
				if (!rd) {
					printf("The destination file could not be opened.\n\n");
					continue;
				}
				int fd = fileno(rd);
				struct stat finfo;
				int res = fstat(fd, &finfo);
				if (res) {
					printf("Error reading file size.\n\n");
					continue;
				}

				off_t filesize = finfo.st_size;

				char* buffer = calloc(filesize + 1, 1);
				fread(buffer, 1, filesize, rd);
				fclose(rd);
				fwrite(buffer, filesize, 1, wr);
				fclose(wr);
				free(buffer);
			}
			continue;
		} else if (!strcmp(arguments[0], "type")) {
			if (!arguments[1][0]) {
				printf("Please enter a file path.\n\n");
			} else {
				FILE* rd = fopen(arguments[1], "r");
				if (!rd) {
					printf("The file could not be opened.\n\n");
					continue;
				}

				int fd = fileno(rd);
				struct stat finfo;
				int res = fstat(fd, &finfo);
				if (res) {
					printf("Error reading file size.\n\n");
					continue;
				}

				off_t filesize = finfo.st_size;
				
				char* buffer = calloc(filesize + 1, 1);
				int r = fread(buffer, 1, filesize, rd);
				fclose(rd);

				printf("%s\n", buffer);
				free(buffer);
			}
			continue;

		} else if (!strcmp(arguments[0], "size")) {
			if (!arguments[1][0]) {
				printf("Please enter a file path.\n\n");
			} else {
				FILE* rd = fopen(arguments[1], "rb");
				if (!rd) {
					printf("The file could not be opened.\n\n");
					continue;
				}
				int fd = fileno(rd);
				struct stat finfo;
				int res = fstat(fd, &finfo);
				if (res) {
					printf("Error reading file size.\n\n");
				} else {
					off_t filesize = finfo.st_size;
					printf("%d bytes.\n\n", filesize);
				}

				fclose(rd);
			}
			continue;

		} else if (!strcmp(arguments[0], "echo")) {
			for (int i = 1; i < argNo; ++i) {
				printf("%s%s", arguments[i][0], argNo != i + 1 ? " " : "");
			}
			continue;

		} else if (!strcmp(arguments[0], "reboot")) {
			SystemCall(SC_Reboot, 0, 0, 0);
			continue;
		} else if (!strcmp(arguments[0], "shutdown")) {
			SystemCall(SC_Shutdown, 0, 0, 0);
			continue;
		} else if (!strcmp(arguments[0], "exit")) {
			exit(0);
		}

		//TODO: arguments
		
		int pid = SystemCall(SC_Spawn, 1, 0, arguments[0]);
		if (pid) {
			SystemCall(SC_Wait, pid, 0, 0);
			continue;
		}

		//TODO: try from PATH

		//TODO: try a batch file

		int arg0Len = strlen(arguments[0]);
		if (arg0Len > 4 &&
			(arguments[0][arg0Len - 1] == 't' || arguments[0][arg0Len - 1] == 'T') &&
			(arguments[0][arg0Len - 2] == 'a' || arguments[0][arg0Len - 3] == 'A') &&
			(arguments[0][arg0Len - 3] == 'b' || arguments[0][arg0Len - 4] == 'B') &&
			(arguments[0][arg0Len - 4] == '.')) {
			printf("That is a batch file. You cannot run it today.\n\n");
			//runBatch(arguments[0]);
			continue;
		}


		//TODO: try say 'is a file' / 'is a folder'
		
		printf("'%s' is not recognised as a command, program, file or directory.\n\n", arguments[0]);
	}

	return 0;
}
