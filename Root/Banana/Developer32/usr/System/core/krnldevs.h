#pragma once

#include <stdint.h>
#include <stddef.h>
#include <stdbool.h>

#define MAX_PROPERTIES 1024
#define PROPERTY_LENGTH 16

typedef enum DriverMode
{
	DriverModeOpen,
	DriverModeRead,
	DriverModeWrite,
	DriverModeIoctl,
	DriverModeClose

} DriverMode;

struct DeviceNode;
typedef struct DeviceNode DeviceNode;

typedef size_t (*PropertyHandler)(char*, int, int, void*);

//this kind of stuff can be looked up using AML bytecode
struct PortRange
{
	uint16_t rangeStart;
	uint8_t rangeLength;	//0 = not used
	uint8_t alignment : 6;	//used by Windows, no idea what it does
	uint8_t width : 2;		//in bytes: e.g 0 = 1, 1 = 2, 2 = 4, 3 = 8
};

struct MemoryRange
{
	uint64_t rangeStart;
	uint64_t rangeLength;
};

struct ResourceList
{
	struct PortRange ports[64];
	struct MemoryRange memory[64];

	uint8_t irqs[16];
};

typedef struct Device
{
	struct Device* parent;
	DeviceNode* children;

	void* internalData;
	int internalDataSize;

	PropertyHandler unknownPropertyHandler;
	PropertyHandler propertyHandles[MAX_PROPERTIES];
	char propertyCodes[MAX_PROPERTIES][PROPERTY_LENGTH];

	bool onDiskDriver;

	//if on disk:
	char* driverLocation;

	//if not:
	void (*openPointer) (struct Device* self, int, int, void*);
	void (*readPointer) (struct Device* self, int, int, void*);
	void (*writePointer) (struct Device* self, int, int, void*);
	void (*ioctlPointer) (struct Device* self, int, int, void*);

	struct ResourceList resources;

	uint16_t vendorID;
	uint8_t devClass;
	uint8_t subClass;
	uint8_t progIF;

	int interrupt;

	char humanName[256];

} Device;

typedef struct DeviceNode
{
	DeviceNode* next;
	Device* device;

} DeviceNode;

typedef struct PropertyResult
{

	char type;		//v, b, w, d, q, B, W, D, Q, s, 1, 2, 4
	union
	{
		void* voidResult;

		uint8_t uint8Result;
		uint16_t uint16Result;
		uint32_t uint32Result;
		uint64_t uint64Result;

		int8_t int8Result;
		int16_t int16Result;
		int32_t int32Result;
		int64_t int64Result;

		size_t sizetResult;

		float floatResult;
		double doubleResult;
		double* longDoubleResult;
	};

} PropertyResult;