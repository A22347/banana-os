#include <fs/vfs.h>
#include <core/kernel.h>
#include <hal/diskio.h>
#include <libk/string.h>
#include "D:\Users\Alex\Desktop\banana-os\Libraries\newlib-3.0.0\newlib\libc\sys\banana\sys\dirent.h"
#include <core/memory.h>
#include <debug/log.h>

char currentDrive = 'C';
char cwdsForFilesystems[26][256];

filesystem_driver_function_struct filesystemDrivers[256];
uint32_t filesystemDriversPtr = 0;

int vfs_test_disk_for_filesystem(char letter, uint32_t filesystemDriverID)
{
	return filesystemDrivers[filesystemDriverID].mount(letter);
}

int vfs_install_filesystem(filesystem_driver_function_struct funcs)
{
	if (!funcs.mount) { logk("NO MOUNT FUNCTION\n"); return 1; }
	if (!funcs.open) { logk("NO OPEN FUNCTION\n"); return 1; }
	if (!funcs.read) { logk("NO READ FUNCTION\n"); return 1; }
	if (!funcs.write) { logk("NO WRITE FUNCTION\n"); return 1; }
	if (!funcs.close) { logk("NO CLOSE FUNCTION\n"); return 1; }
	if (!funcs.size) { logk("NO SIZE FUNCTION\n"); return 1; }
	if (!funcs.eof) { logk("NO EOF FUNCTION\n"); return 1; }
	if (!funcs.readdir) { logk("NO READ-D FUNCTION\n"); return 1; }
	if (!funcs.opendir) { logk("NO OPEN-D FUNCTION\n"); return 1; }
	if (!funcs.closedir) { logk("NO CLOSE-D FUNCTION\n"); return 1; }
	if (!funcs.seek) { logk("NO SEEK FUNCTION\n"); return 1; }
	if (!funcs.unlink) { logk("NO UNLINK FUNCTION\n"); return 1; }
	if (!funcs.rmdir) { logk("NO RMDIR FUNCTION\n"); return 1; }
	if (!funcs.mkdir) { logk("NO MKDIR FUNCTION\n"); return 1; }

	logk("installation of driver successful! (%d)\n", filesystemDriversPtr);

	filesystemDrivers[filesystemDriversPtr++] = funcs;
	return filesystemDriversPtr - 1;
}

#include <fs/fat/vfs_wrapper.h>

void vfs_init()
{
	// get a linked list containing open files set up

	// set up drivers
	fat_install();

	//set up disks
	for (int i = 0; i < 26; ++i) {
		if (Disks[i].letter != '?') {
			vfs_mount('A' + i);
		}
	}
}

void vfs_reinit()
{
	//set up disks that have been discovered since boot

	for (int i = 0; i < 26; ++i) {
		if (!Disks[i].mounted) {
			logk("rescanning disk %c\n", 'A' + i);
			vfs_mount('A' + i);
		}
	}
}

int vfs_mount(char driveLetter)
{
	logk("mounting drive %c\n", driveLetter);

	int res = vfs_status_SOMETHING_ELSE;

	uint32_t id = 0;
	uint32_t x = 0;

	while (res != vfs_status_success) {
		id = x;
		res = vfs_test_disk_for_filesystem(driveLetter, x);
		if (res == vfs_status_success) {
			Disks[driveLetter - 'A'].mounted = true;
			break;
		}
		++x;
		if (x >= filesystemDriversPtr) {
			res = true;
			id = UNKNOWN_FILESYSTEM;
			Disks[driveLetter - 'A'].mounted = false;
			logk("Unkwown filesystem. That's okay, though. OSes can support unformatted disks.\n");
			break;
		}
	}
	logk("found id of %d\n");
	Disks[driveLetter - 'A'].filesystemDriverID = id;

	return vfs_status_success;
}

char* fixPath(char* oldesterpath, char* cwd, bool includeDrive)
{
	char drive = 'C';
	char* oldestpath = oldesterpath;

	if (oldestpath[1] == ':') {
		drive = oldesterpath[0];
		oldestpath = oldesterpath + 2;
	} else if (cwd[1] == ':') {
		drive = cwd[0];
		cwd += 2;
	}

	char* oldpath = malloc(256);
	memset(oldpath, 0, 256);
	if (oldestpath[0] != '/') {
		strcpy(oldpath, cwd);
	}
	for (int i = 0, j = (int) strlen(oldpath); oldestpath[i]; ++i) {
		oldpath[j] = oldestpath[i] == '\\' ? '/' : oldestpath[i];
		++j;
	}

	char* new = malloc(256);
	memset(new, 0, 256);

	for (int i = 0, j = 0; oldpath[i]; ++i) {
		if (oldpath[i] == '/' && j && new[j - 1] == '/') {
			continue;
		}
		new[j] = oldpath[i];
		++j;
	}

	char* newer = malloc(256);
	memset(newer, 0, 256);

	for (int i = 0, j = 0; new[i]; ++i) {
		if (new[i] == '.' && (new[i + 1] == '/' || new[i + 1] == 0)) {
			i += 1;
			continue;
		}
		if (new[i] == '.' && new[i + 1] == '.' &&  (new[i + 2] == '/' || new[i + 2] == 0)) {
			i += 2;
			while (1) {
				if (j == 0) break;
				newer[j--] = 0;
				if (j == 0) break;
				if (newer[j] == '/') {
					break;
				}
			}
			continue;
		}
		newer[j] = new[i];
		++j;
	}

	freee(new);
	freee(oldpath);

	if (includeDrive) {
		char* out = malloc(256);
		memset(out, 0, 256);
		out[0] = drive;
		out[1] = ':';
		out[2] = 0;
		strcat(out, newer);
		freee(newer);
		return out;
	}

	return newer;
}

char* convertRelativePathToAbsolute(char* relative, char* ptrToDriveLetter, bool includeDrive)
{
	int drv = currentDrive - 'A';
	if (relative[1] == ':') drv = relative[0] - 'A';
	*ptrToDriveLetter = drv + 'A';
	char* p = fixPath(relative, cwdsForFilesystems[drv], includeDrive);
	return p;
}

#include <core/prcssthr.h>

void fixTaskCwd()
{
	/*if (TASK_Current && !TASKS[TASK_Current].threads[0].cwdCorrect) {
		TASKS[TASK_Current].threads[0].cwdCorrect = true;		//putting it before vfs_chdir stops vfs_chdir from being recursive (as THAT calls fixTaskCwd, because to change dir, it needs to be in the right dir)
		vfs_chdir(TASKS[TASK_Current].threads[0].cwd);
	}*/
}

int vfs_mkdir(char* dirname)
{
	fixTaskCwd();

	char driveLetter = 'C';
	char* filename = convertRelativePathToAbsolute(dirname, &driveLetter, false);

	uint32_t filesystemDriverID = Disks[driveLetter - 'A'].filesystemDriverID;

	if (filesystemDriverID == UNKNOWN_FILESYSTEM) {
		return vfs_status_no_filesystem;

	} else {
		void* ptr = 0;
		int res = filesystemDrivers[Disks[driveLetter - 'A'].filesystemDriverID].mkdir(driveLetter, dirname);

		switch (res) {
		case vfs_status_success:
			freee(filename);
			break;
		default:
			freee(filename);
			return res;
		}
	}

	return vfs_status_success;
}

int vfs_opendir(vfs_dir_t * outputFile, char* dirname)
{
	fixTaskCwd();

	// check if file is already open for writing, if so, cancel the open
	outputFile->error = false;
	outputFile->initialisedAndValid = true;
	outputFile->eof = false;

	char driveLetter = 'C';
	char* filename = convertRelativePathToAbsolute(dirname, &driveLetter, false);

	uint32_t filesystemDriverID = Disks[driveLetter - 'A'].filesystemDriverID;

	if (filesystemDriverID == UNKNOWN_FILESYSTEM) {
		return vfs_status_no_filesystem;

	} else {
		void* ptr = 0;
		int res = filesystemDrivers[Disks[driveLetter - 'A'].filesystemDriverID].opendir(driveLetter, &ptr, filename);

		switch (res) {
		case vfs_status_success:
			outputFile->driveLetter = driveLetter;
			outputFile->pointer = ptr;
			strcpy(outputFile->__filename, filename);
			freee(filename);
			break;
		default:
			outputFile->error = true;
			outputFile->initialisedAndValid = false;
			freee(ptr);
			freee(filename);
			return res;
		}
	}

	return vfs_status_success;
}

int vfs_closedir(vfs_dir_t * file)
{
	if (!file->initialisedAndValid || file->error) {
		return vfs_status_SOMETHING_ELSE;
	}

	file->initialisedAndValid = false;

	uint32_t filesystemDriverID = Disks[file->driveLetter - 'A'].filesystemDriverID;

	if (filesystemDriverID == UNKNOWN_FILESYSTEM) {
		return vfs_status_no_filesystem;

	} else {
		int res = filesystemDrivers[Disks[file->driveLetter - 'A'].filesystemDriverID].closedir(file->pointer);
		switch (res) {
		case vfs_status_success:
			break;
		default:
			return res;
		}
	}

	return vfs_status_success;
}

int vfs_readdir(vfs_dir_t * file, struct dirent* out)
{
	if (!file->initialisedAndValid || file->error) {
		return vfs_status_SOMETHING_ELSE;
	}

	uint32_t filesystemDriverID = Disks[file->driveLetter - 'A'].filesystemDriverID;

	if (filesystemDriverID == UNKNOWN_FILESYSTEM) {
		return vfs_status_no_filesystem;

	} else {
		int res = filesystemDrivers[Disks[file->driveLetter - 'A'].filesystemDriverID].readdir(file->pointer, out);

		switch (res) {
		case vfs_status_success:
			break;
		default:
			file->error = true;
			return res;
		}
	}

	return vfs_status_success;
}

int	vfs_open(vfs_file_t * outputFile, char* fx, uint8_t mode)
{
	fixTaskCwd();

	//mode of 0 = read
	if (mode == 0) {
		mode = 1;
	}

	// check if file is already open for writing, if so, cancel the open

	outputFile->error = false;
	outputFile->initialisedAndValid = true;
	outputFile->eof = false;

	char driveLetter = 'C';
	char* filename = convertRelativePathToAbsolute(fx, &driveLetter, false);
	logk("VFS OPENING FILE %s ON DRIVE %c\n", filename, driveLetter);

	uint32_t filesystemDriverID = Disks[driveLetter - 'A'].filesystemDriverID;

	logk("VFS OPEN CALLED ON FILE 0x%X\n", outputFile);

	if (filesystemDriverID == UNKNOWN_FILESYSTEM) {
		return vfs_status_no_filesystem;

	} else {
		void* ptr = 0;
		int res = filesystemDrivers[Disks[driveLetter - 'A'].filesystemDriverID].open(driveLetter, &ptr, filename, mode);

		switch (res) {
		case vfs_status_success:
			outputFile->driveLetter = driveLetter;
			outputFile->pointer = ptr;
			strcpy(outputFile->__filename, filename);
			break;
		default:
			logk("File open failed!\n");
			outputFile->error = true;
			outputFile->initialisedAndValid = false;
			//freee(filename);
			return res;
		}
	}

	return vfs_status_success;
}

int vfs_unlink(char* fx)
{
	fixTaskCwd();

	char driveLetter = 'C';
	char* filename = convertRelativePathToAbsolute(fx, &driveLetter, false);

	uint32_t filesystemDriverID = Disks[driveLetter - 'A'].filesystemDriverID;

	if (filesystemDriverID == UNKNOWN_FILESYSTEM) {
		return vfs_status_no_filesystem;

	} else {
		int res = filesystemDrivers[filesystemDriverID].unlink(filename);

		switch (res) {
		case vfs_status_success:
			break;
		default:
			return res;
		}
	}

	return vfs_status_success;
}

int vfs_rename(char* old, char* newn)
{
	fixTaskCwd();

	//this will work for files on different disks,
	//but will be slow if you're not changing disks

	//open old, get size
	vfs_file_t readFil;
	uint64_t inputSize = 0;
	int res = vfs_open(&readFil, old, vfs_file_mode_read);
	if (res != vfs_status_success) {
		logk("rename failure A\n");
		return res;
	}
	vfs_size(&readFil, &inputSize);

	//read old in
	uint8_t* buffer = malloc(inputSize);
	uint64_t actual;
	res = vfs_read(&readFil, buffer, inputSize, &actual);
	if (res != vfs_status_success) {
		logk("rename failure B\n");
		return res;
	}
	vfs_close(&readFil);

	//write new
	vfs_file_t writeFil;
	vfs_open(&writeFil, newn, vfs_file_mode_write);
	res = vfs_write(&writeFil, buffer, inputSize);
	if (res != vfs_status_success) {
		logk("rename failure C\n");
		return res;
	}
	vfs_close(&writeFil);

	freee(buffer);

	//unlink old
	vfs_unlink(old);

	return vfs_status_success;
}

int	vfs_close(vfs_file_t * file)
{
	logk("VFS_CLOSE!\n");
	if (!file->initialisedAndValid || file->error) {
		logk("file error!\n");
		return vfs_status_SOMETHING_ELSE;
	}

	uint32_t filesystemDriverID = Disks[file->driveLetter - 'A'].filesystemDriverID;

	if (filesystemDriverID == UNKNOWN_FILESYSTEM) {
		return vfs_status_no_filesystem;

	} else {
		int res = filesystemDrivers[Disks[file->driveLetter - 'A'].filesystemDriverID].close(file->pointer);
		switch (res) {
		case vfs_status_success:
			//freee(file->__filename);
			break;
		default:
			file->initialisedAndValid = false;
			return res;
		}
	}

	file->initialisedAndValid = false;
	return vfs_status_success;
}

int vfs_read(vfs_file_t * file, uint8_t * buffer, uint64_t bytes, uint64_t* actual)
{
	if (!file->initialisedAndValid || file->error) {
		return vfs_status_SOMETHING_ELSE;
	}

	logk("VFS READ CALLED ON FILE 0x%X\n", file);

	uint32_t filesystemDriverID = Disks[file->driveLetter - 'A'].filesystemDriverID;

	if (filesystemDriverID == UNKNOWN_FILESYSTEM) {
		return vfs_status_no_filesystem;

	} else {
		int res = filesystemDrivers[Disks[file->driveLetter - 'A'].filesystemDriverID].read(file->pointer, buffer, bytes, actual);

		switch (res) {
		case vfs_status_success:
			break;
		default:
			file->error = true;
			return res;
		}
	}

	return vfs_status_success;
}

int vfs_write(vfs_file_t * file, uint8_t * buffer, uint64_t bytes)
{
	logk("VFS WRITE CALLED!\nbuffer = %s\n", buffer);
	if (!file->initialisedAndValid || file->error) {
		return vfs_status_SOMETHING_ELSE;
	}

	uint32_t filesystemDriverID = Disks[file->driveLetter - 'A'].filesystemDriverID;

	if (filesystemDriverID == UNKNOWN_FILESYSTEM) {
		logk("error 2\n");
		return vfs_status_no_filesystem;

	} else {
		int res = filesystemDrivers[Disks[file->driveLetter - 'A'].filesystemDriverID].write(file->pointer, buffer, bytes);
		switch (res) {
		case vfs_status_success:
			break;
		default:
			logk("error 3\n");
			file->error = true;
			return res;
		}
	}

	return vfs_status_success;
}

int vfs_seek(vfs_file_t * file, uint64_t bytes)
{
	if (!file->initialisedAndValid || file->error) {
		return vfs_status_SOMETHING_ELSE;
	}

	uint32_t filesystemDriverID = Disks[file->driveLetter - 'A'].filesystemDriverID;

	if (filesystemDriverID == UNKNOWN_FILESYSTEM) {
		return vfs_status_no_filesystem;

	} else {
		int res = filesystemDrivers[Disks[file->driveLetter - 'A'].filesystemDriverID].seek(file->pointer, bytes);

		switch (res) {
		case vfs_status_success:
			break;
		default:
			file->error = true;
			return res;
		}
	}

	return vfs_status_success;
}

int vfs_size(vfs_file_t * file, uint64_t * output)
{
	if (!file->initialisedAndValid || file->error) {
		return vfs_status_SOMETHING_ELSE;
	}

	uint32_t filesystemDriverID = Disks[file->driveLetter - 'A'].filesystemDriverID;

	if (filesystemDriverID == UNKNOWN_FILESYSTEM) {
		return vfs_status_no_filesystem;

	} else {
		int res = filesystemDrivers[Disks[file->driveLetter - 'A'].filesystemDriverID].size(file->pointer, output);

		switch (res) {
		case vfs_status_success:
			break;
		default:
			file->error = true;
			return res;
		}
	}

	return vfs_status_success;
}

int vfs_eof(vfs_file_t * file, bool* eof)
{
	if (!file->initialisedAndValid || file->error) {
		return vfs_status_SOMETHING_ELSE;
	}

	uint32_t filesystemDriverID = Disks[file->driveLetter - 'A'].filesystemDriverID;

	if (filesystemDriverID == UNKNOWN_FILESYSTEM) {
		return vfs_status_no_filesystem;

	} else {
		int res = filesystemDrivers[Disks[file->driveLetter - 'A'].filesystemDriverID].eof(file->pointer, eof);

		switch (res) {
		case vfs_status_success:
			break;
		default:
			file->error = true;
			return res;
		}
	}

	return vfs_status_success;
}

int vfs_gets(char* buffer, int size, vfs_file_t * file)
{
	bool eee;
	vfs_eof(file, &eee);
	if (eee) {
		return vfs_status_eof;
	}

	uint64_t actual;

	for (int i = 0; i < size - 1; ++i) {
		char b[1];
		int ret = vfs_read(file, (uint8_t*) b, 1, &actual);
		if (ret) {
			buffer[i + 1] = 0;
			return ret;
		}

		bool eee;
		vfs_eof(file, &eee);
		if (eee) {
			buffer[i] = b[0];
			buffer[i + 1] = 0;
			break;
		}

		buffer[i] = b[0];
		if (b[0] == '\n') {
			buffer[i + 1] = 0;
			break;
		}
	}
	buffer[size + 1] = 0;
	return vfs_status_success;
}

int vfs_chdrive(char driveletter)
{
	currentDrive = driveletter;
	return 0;
}

int vfs_chdir(char* dirname)
{
	fixTaskCwd();

	logk("vfs_chdir\n");
	//convert to relative
	char dir = currentDrive;
	char* out = convertRelativePathToAbsolute(dirname, &dir, true);
	vfs_dir_t outdir;
	int res = vfs_opendir(&outdir, out);
	if (res == vfs_status_success) {
		strcpy(cwdsForFilesystems[dir - 'A'], out);
		vfs_closedir(&outdir);
	}
	freee (out);
	logk("vfs_chdird\n");

	return res;
}


void vfs_chdir_no_check(char* dirname)
{
	char dir = currentDrive;
	char* out = convertRelativePathToAbsolute(dirname, &dir, true);
	strcpy(cwdsForFilesystems[dir - 'A'], out);
	freee(out);
}

int vfs_getdrive(char* drive)
{
	fixTaskCwd();

	*drive = currentDrive;
	return 0;
}

int vfs_getcwd(char* c, int len)
{
	fixTaskCwd();

	memset(c, 0, len);
	memcpy(c, cwdsForFilesystems[(int) currentDrive - 'A'], len - 1);
	return 0;
}
