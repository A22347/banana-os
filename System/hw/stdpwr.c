#include <hw/stdpwr.h>
#include <hw/acpi.h>
#include <hw/ports.h>
#include <hw/legacy.h>

#define bit(n) (1<<(n))
#define check_flag(flags, n) ((flags) & bit(n))

extern void idt_set_gate(unsigned char num, unsigned long base);

void basicReboot()
{
	//remove fault vectors so if the ACPI method causes a fault, it reboots
	//actually, calling this will probably mess it up anyway
	asm("cli");
	for (int i = 0; i < 255; ++i) {
		idt_set_gate(i, i);
	}
	asm("sti");	//could reset here...

	for (int i = 0; i < 10; ++i) {
		if (detectLegacy()) {
			uint8_t temp;
			asm volatile ("cli");
			do {
				temp = inb(0x64);
				if (check_flag(temp, 0) != 0)
					inb(0x60);
			} while (check_flag(temp, 1) != 0);

			outb(0x64, 0xFE);		//could reset here...
			while (1) {
				asm volatile ("hlt");
			}
		}

		for (volatile int j = 0; j < 10000; j += 17) {
			j += 3;
			j ^= 0xFF;
			j -= 2;
			j ^= 0xFF;
			--j;
		}
		
		uint8_t* resetReg = (uint8_t*) (size_t) FADTpointer->ResetReg.Address;
		*resetReg = FADTpointer->ResetValue;
		*resetReg = FADTpointer->ResetValue;	//could reset here...
	}

	//give up, tell user to do it, and after that try to triple fault
}

void writeStdPower(Device* self, int b, int c, void* d)
{
	if (b == 0) {
		basicReboot();

	} else if (b == 1) {
		outw(0xB004, 0x2000);
		outw(0x604, 0x2000);
		outw(0x4004, 0x3400);

		/*
		mov ax, 0x1000
		mov ax, ss
		mov sp, 0xF000
		mov ax, 0x5307
		mov bx, 0x0001
		mov cx, 0x0003
		int 0x15
		*/
	}
}