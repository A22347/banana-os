#pragma once

#include <stdint.h>
#include <stdbool.h>

void guiReactToSecondPassed ();
void guiReactToTimerTick ();

void guiReactToKeyboardPress (uint16_t scancode);
void guiReactToKeyboardUp (uint16_t scancode);
void guiReactToKeyboardDown (uint16_t scancode);

void guiReactToMouseStart ();
void guiReactToMouseMove (int x, int y);
void guiReactToMiddleMouseButton ();
void guiReactToMouseDown ();
void guiReactToMouseDownFirstTime ();
void guiReactToMouseUp ();
void guiReactToRightClick ();
void guiReactToScrollWheel (int x, int y);
void guiReactToMouseEnd ();
void guiReactToMouseDrag ();

void* guiTerminalCreate (char* n, int w, int h);
void guiTerminalUpdate (void* t);
void guiTerminalWriteChar (void* t, char c, bool b);

int guiGetFocusedWindow ();