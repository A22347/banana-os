#pragma once

#include <core/kernel.h>
#include <devss/devices.h>
#include <stdbool.h>
#include <stdint.h>

void openPS2Mouse(struct Device* self, int, int, void*);
