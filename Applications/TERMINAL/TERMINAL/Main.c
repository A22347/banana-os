﻿
/*
TERMINAL.SYS
Default terminal driver. Accepts input from program's standard streams using pipes,
and then converts it into a screen which gets passed onto GUI.EXE
*/

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <unistd.h>
#include <syscall.h>
#include <string.h>

#include "D:/Users/Alex/Desktop/banana-os/System/core/syscalldef.h"
int SystemCall(uint16_t callno, uint32_t ebx, uint32_t ecx, void* edx);

int __i_system_call_pipe(long long unsigned int* a, long long unsigned int* b)
{
	return SystemCall(SC_Pipe, (size_t) a, (size_t) b, 0);
}

int pipe(int v[2])
{
	uint64_t r;
	uint64_t w;

	int ret = __i_system_call_pipe((long long unsigned int*) & r, (long long unsigned int*) & w);

	v[0] = r;
	v[1] = w;

	return ret;
}

typedef struct TerminalData
{
	char text[25][80];
	uint32_t fg[25][80];
	uint32_t bg[25][80];

	int xPtr;
	int yPtr;

	uint64_t readPipeDescriptor;

	struct TerminalData* next;

	bool root;
	bool needsUpdating;

} TerminalData;

TerminalData* root;

void createNewTerminalFromReadEndOfPipe(int readEnd)
{
	uint64_t pid2Addr = 0;
	uint64_t pid1Addr = 0;
	
	int guiPID = SystemCall(SC_BlackMagic, 55009, 0, 0);

	if (guiPID) {
		pid1Addr = SystemCall(SC_AllocateSharedMemory, guiPID, (sizeof(TerminalData) + 4095) / 4096, 0);
		pid2Addr = SystemCall(SC_AllocateSharedMemoryP2, 0, 0, 0);
	} else {
		pid1Addr = malloc(sizeof(TerminalData));
	}

	TerminalData* t = pid1Addr;
	memset(t->text, ' ', 25 * 80);
	memset(t->fg, 0xC0, 25 * 80 * 4);
	memset(t->bg, 0x00, 25 * 80 * 4);
	t->root = false;
	t->xPtr = 0;
	t->yPtr = 0;
	t->readPipeDescriptor = readEnd;
	t->next = 0;
	t->needsUpdating = true;

	TerminalData* c = root;
	while (c->next) {
		c = c->next;
	}
	c->next = t;

	if (guiPID) {
		SystemCall(SC_BlackMagic, 55010, pid2Addr, 0);		//send the GUI the address
		//while (SystemCall(SC_BlackMagic, 55011, 0, 0));		//wait for the GUI to accept it
	}
}

void scrollTerminalBackARow(TerminalData* t)
{
	for (int i = 0; i < 24; ++i) {
		memcpy(t->text[i], t->text[i + 1], 80);
		memcpy(t->fg[i], t->fg[i + 1], 80 * 4);
		memcpy(t->bg[i], t->bg[i + 1], 80 * 4);
	}
	memset(t->text[24], ' ', 80);
	memset(t->fg[24], 0xC0, 80 * 4);
	memset(t->bg[24], 0x00, 80 * 4);
}

void removeCursor(TerminalData* t)
{
	t->text[t->yPtr][t->xPtr] = ' ';
}

void drawCursor(TerminalData* t)
{
	t->text[t->yPtr][t->xPtr] = '_';
}


char ansiSequence[64];
int ansiSeqPtr = 0;
uint32_t defaultFg = 0xC0C0C0;
uint32_t defaultBg = 0x000000;

uint32_t colourCodesDim[8] = {
	0x000000,
	0x800000,
	0x008000,
	0x808000,
	0x000080,
	0x800080,
	0x008080,
	0xC0C0C0,
};

uint32_t colourCodesBright[8] = {
	0x808080,
	0xFF0000,
	0x00FF00,
	0xFFFF00,
	0x0000FF,
	0xFF00FF,
	0x00FFFF,
	0xFFFFFF,
};

void handleANSI(TerminalData* t)
{
	if (ansiSequence[ansiSeqPtr - 1] == 'm') {
		if (ansiSequence[0] == 'm' || ansiSequence[0] == '0') {
			defaultFg = 0xC0C0C0;
			defaultBg = 0x000000;

		} else if (ansiSequence[0] == '3' && isdigit(ansiSequence[1])) {
			defaultFg = colourCodesDim[ansiSequence[1] - '0'];

		} else if (ansiSequence[0] == '9' && isdigit(ansiSequence[1])) {
			defaultFg = colourCodesBright[ansiSequence[1] - '0'];

		} else if (ansiSequence[0] == '4' && isdigit(ansiSequence[1])) {
			defaultBg = colourCodesDim[ansiSequence[1] - '0'];

		} else if (ansiSequence[0] == '1' && ansiSequence[1] == '0' && isdigit(ansiSequence[2])) {
			defaultBg = colourCodesBright[ansiSequence[2] - '0'];
		}
	}

	memset(ansiSequence, 0, 64);
	ansiSeqPtr = 0;
}

void writeToTerminal(TerminalData* t, char* text)
{
	removeCursor(t);
	static int ansiMode = 0;
	for (int i = 0; text[i]; ++i) {
		if (ansiMode == 0 && text[i] == '\x1B') {
			ansiMode = 1;
			continue;
		} else if (ansiMode == 1 && text[i] == '[') {
			ansiMode = 2;
			continue;
		} else if (ansiMode == 2) {
			ansiSequence[ansiSeqPtr++] = text[i];

			//this means it's the last one
			if (text[i] >= 0x40) {
				handleANSI(t);
				ansiMode = 0;
			}
			continue;
		} else {
			ansiMode = 0;
		}

		if (text[i] == '\n' || t->xPtr >= 80) {
			t->yPtr++;
			t->xPtr = 0;
			if (t->yPtr >= 25) {
				t->yPtr = 24;
				scrollTerminalBackARow(t);
			}
		}

		if (text[i] == '\n') continue;
		t->text[t->yPtr][t->xPtr] = text[i];
		t->fg[t->yPtr][t->xPtr] = defaultFg;
		t->bg[t->yPtr][t->xPtr] = defaultBg;
		t->xPtr++;
	}
	drawCursor(t);
}

void updateTerminal(TerminalData* t)
{
	char buffer[1024];
	memset(buffer, 0, 1024);

	int pos = 0;

	while (1) {
		char bf[2];
		bf[1] = 0;

		int res = SystemCall(SC_ReadFileOrPipeObjectFromFilename, 1, (size_t) & (t->readPipeDescriptor), bf);

		if (res > 0) {
			buffer[pos++] = bf[0];
			
		} else {
			break;
		}
		if (pos == '\n') break;			//do the rest next time around
		if (pos == 80) break;			//do it next time around
	}

	if (strlen(buffer) && pos != 0) {
		writeToTerminal(t, buffer);
		t->needsUpdating = true;
	} else {
		t->needsUpdating = false;
	}
}

void updateTerminals()
{
	TerminalData* c = root;
	while (c) {
		if (c && !c->root) updateTerminal(c);
		c = c->next;
		if (!c) break;
	}
	if (c && !c->root) updateTerminal(c);
}

int main (int argc, char *argv[])
{
	SystemCall(SC_ExitTerminalSession, 0, 0, 0);
	root = malloc(sizeof(TerminalData));
	root->next = 0;
	root->root = true;

	memset(ansiSequence, 0, 64);
	ansiSeqPtr = 0;

	int p[2];
	pipe(p);

	uint64_t readEnd = p[0];
	uint64_t writeEnd = p[1];

	//int guiPID = SystemCall(SC_Spawn, 0, 0, "C:/Banana/System/GUI.EXE");
	SystemCall(SC_BlackMagic, 177, writeEnd, 0);
	
	char buffer[256];
	int pos = 0;

retry:
	memset(buffer, 0, 256);
	pos = 0;

	while (1) {
		char bf[2];
		bf[1] = 0;

		int res = SystemCall(SC_ReadFileOrPipeObjectFromFilename, 1, (size_t) &readEnd, bf);

		if (res > 0) {
			buffer[pos++] = bf[0];
			updateTerminals();
		} else {
			updateTerminals();
			continue;
		}
		if (pos == 1 && buffer[0] != 'M') {
			goto retry;
		}
		if (pos == 2  && buffer[1] != 'S') {
			goto retry;
		}
		if (pos == 3 && buffer[2] != 'G') {
			goto retry;
		}
		if (pos == 4 && buffer[3] != '!') {
			goto retry;
		}
		if (pos == 12) {
			int a = 0;
			a = ((int*) buffer)[1];
			switch (a) {
				case 0x01010101:
				{
					int n = ((unsigned int*) buffer)[2];
					createNewTerminalFromReadEndOfPipe(n);
					break;
				}
				
			}
			goto retry;
		}
		
		updateTerminals();
	}

	return 0;
}