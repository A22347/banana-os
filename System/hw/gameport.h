#pragma once

#include <stdint.h>
#include <stdbool.h>
#include <devss/devices.h>

void detectGameport(struct Device* parent);
void configureGameport(struct Device* device, int a, int b, void* c);
void openGameport (struct Device* device, int a, int b, void* c);
void readGameport (struct Device* device, int a, int b, void* c);
void writeGameport (struct Device* device, int a, int b, void* c);
void ioctlGameport(struct Device* device, int a, int b, void* c);
void closeGameport(struct Device* device, int a, int b, void* c);
