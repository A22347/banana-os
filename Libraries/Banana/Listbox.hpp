#ifndef __LISTBOX_HPP__
#define __LISTBOX_HPP__

#include "Core.hpp"
#include "Widget.hpp"
#include "Window.hpp"

class ListBox : public Widget {
    friend class Window;
    
protected:    
    typedef struct Node {
        char* text;
        Node* next;
        
    } Node;
    
    Node* root;
    Node* current;
    
    int clickedIndex = 0;
    int numberOfItems = 0;
    int scrolledIndex = 0;
    int height = 0;
    int width = 0;
    int x = 0;
    int y = 0;
    
    bool focus = false;
    
    void _write();
	void _clearScr ();
    
public:
    ListBox (Window& parent, int x, int y, int height = 8, int width = 12);
    ~ListBox ();
    void selfupdate (Event_t event);
	void add (char* text, int position = -1);
    void clear ();
    void update ();
};

#endif