//
//  signal.h
//  Banana libc
//
//  Created by Alex Boxall on 11/7/18.
//  Copyright © 2018 Alex Boxall. All rights reserved.
//

#ifndef signal_h
#define signal_h

#if defined(__cplusplus)
#define restrict			//fixes some 'fun' errors
extern "C" {
#endif
    
#ifndef _libk
    
#define SIG_DFL __i_signal_dfl_handler
#define SIG_ERR __i_signal_err_handler
#define SIG_IGN __i_signal_ign_handler
    
#define SIGABRT 1
#define SIGFPE 2
#define SIGILL 3
#define SIGINT 4
#define SIGSEGV 5
#define SIGTERM 6
#define SIGKILL 7
    
    typedef unsigned int sig_atomic_t;     //can be anything which is atomic (e.g. 4 byte ints on x86(_64)
    
    void (*signal(int sig, void (*func)(int)))(int);
    int raise(int sig);
    
    void __i_signal_dfl_handler (int sig);
    void __i_signal_err_handler (int sig);
    void __i_signal_ign_handler (int sig);

#endif
    
#if defined(__cplusplus)
}
#endif

#endif /* signal_h */
