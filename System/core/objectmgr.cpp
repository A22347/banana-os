#include <core/objectmgr.hpp>

uint16_t stdinKeyboardKey = 0;

extern "C" {
	#include <core/memory.h>
	#include <core/string.h>
	#include <gui/guilink.h>
	#include <hal/interrupts.h>		//blockKernelForIO ()
	//#include <gui/ansi.h>

	InternalObjectID createNewStdin ()
	{
		Stdin* s = new Stdin ();
		return s->getID ();
	}

	InternalObjectID createNewFileObject (char* filename, uint8_t mode)
	{
		FileObject* s = new FileObject (filename, mode);
		return ((FileProtocolRead*)s)->getID ();
	}

	bool fileObjectGetErrorBit(InternalObjectID id)
	{
		return ((FileObject*) (((FileProtocolRead*) (FileObject::getObject(id)))))->error;
	}

	InternalObjectID createNewDirectoryObject (char* filename)
	{
		DirObject* s = new DirObject (filename);
		return ((FileProtocolRead*) s)->getID ();
	}

	void closeDirObject (InternalObjectID id)
	{
		((DirObject*)DirObject::getObject (id))->close();
	}

	int readDirObject (InternalObjectID id, struct dirent* dir)
	{
		logk("readDirObject. id = 0x%X, dirent = 0x%X\n", id, dir);
		return ((DirObject*) DirObject::getObject (id))->read ((char*) dir, sizeof (struct dirent));
	}

	vfs_file_t* getFileObjectFromObject(InternalObjectID id)
	{
		return &(((FileObject*) (((FileProtocolRead*) (FileObject::getObject(id)))))->file);
	}

	int filePipeStdoutWrite (InternalObjectID id, size_t length, uint8_t* data)
	{
		logk("filePipeStdoutWrite called. objId = 0x%X. datalen = %d\n", id, length);
		FileObject* s = (FileObject*) FileObject::getObject (id);
		if (!s) {
			logk("'s' is bad in filePipeStdoutWrite\n");
			return -1;
		}
		logk("s is 0x%X\n", s);
		return s->write ((char*) data, length);
	}

	int filePipeStdinRead (InternalObjectID id, size_t length, uint8_t* data)
	{
		FileObject* s = (FileObject*) FileObject::getObject (id);
		if (!s) {
			logk("'s' is bad in filePipeStdinRead\n");
			return -1;
		}
		logk("s is 0x%X\n", s);
		return s->read ((char*) data, length);
	}

	void closeFileObject(InternalObjectID id)
	{
		//I love C++
		logk("CLOSING FILE WITH ID 0x%X (closeFileObject, objectmgr.cpp)\n", id);
		((FileObject*) (((FileProtocolRead*)(FileObject::getObject(id)))))->close();
	}

	char* getStdinInternalBufferFromStdin (InternalObjectID id)
	{
		Stdin* s = (Stdin*) Stdin::getObject (id);
		return s->internalBuffer;
	}

	void flushStdinInternalBuffer (InternalObjectID id)
	{
		Stdin* s = (Stdin*) Stdin::getObject (id);
		memcpy (s->buffer, s->internalBuffer, s->BUFFER_SIZE);
		memset (s->internalBuffer, 0, s->BUFFER_SIZE);
	}

	void createNewPipe(InternalObjectID* outRead, InternalObjectID* outWrite)
	{
		PipeRead* r = new PipeRead();
		PipeWrite* w = new PipeWrite();
		r->pipe = new InternalPipeBit();
		w->pipe = r->pipe;
		memset(r->pipe->buffer, 0, InternalPipeBit::BUFFER_SIZE - 1);
		r->pipe->ptr = 0;

		*outRead = r->getID();
		*outWrite = w->getID();

		strcpy(r->pipe->verify, "VERFIY PIPE");
		r->pipe->vfy2 = 0xABCDEF;
		w->pipe->vfy2 = 0xABCDEF;

		logk("NEW PIPE MADE: READ = 0x%X, WRITE = 0x%X\n", r->getID(), w->getID());
	}
}

InternalObjectID Object::nextObjectID = 1;
Object* Object::allObjects[4096];

Object::Object ()
{
	id = nextObjectID++;
	allObjects[id] = this;
	logk("New object has a 'this' location of 0x%X\n", this);
	logk("New object has location of 0x%X\n", allObjects+id);

}

int FileProtocolRead::write(char* data, size_t size)
{
	return -3;
}

int FileProtocolRead::read(char* buffer, size_t size)
{
	return -4;
}

InternalObjectID Object::getID ()
{
	return id;
}

Object* Object::getObject (InternalObjectID id)
{
	return allObjects[id];
}

bool Object::usableByThread (InternalObjectID thread)
{
	return false;
}

Stdin::Stdin ()
{
	ptr = 0;

	memset (buffer, 0, this->BUFFER_SIZE);
	memset (internalBuffer, 0, this->BUFFER_SIZE);

	this->processThisBelongsTo = TASK_Current;
}

int Stdin::read (char* outbuffer, size_t size)
{
	//this actually has to buffer it (so things like backspace will work), not just read one-by-one
	//this is because it should be able to recognise quick keystrokes
	//AND the C library requires buffering (so characters not read in
	//will get read in next time)

	//seems like 1KB is A-OK (that's the MacOS limit)
	//https://stackoverflow.com/questions/39360883/c-fgets-read-line-from-stdin-maximum-length-is-1024?rq=1

	// this function must fill 'buffer' to 'size' characters, no matter what
	// it should keep reading from TASKS[TASK_Current].keyboardBuffer[TASKS[TASK_Current].keyboardBufferPtr]]
	// until it is full.

	size_t i = 0;
	bool newlineFound = false;

	while (this->buffer[0] == 0) {
		yieldFromKernel();
	}

	for (; i < size; ++i) {
		if (this->buffer[0] == 0) {
			break;
		}

		outbuffer[i] = this->buffer[0];
		outbuffer[i + 1] = 0;

		for (int j = 0; j < this->BUFFER_SIZE - 1; ++j) {
			this->buffer[j] = this->buffer[j + 1]; // copy next element left
		}
		this->buffer[this->BUFFER_SIZE - 1] = 0;			//keep the zeros coming
	}

	return i;	//must return bytes read
}

	//vfs_file_t file;
	//char* matchingFilename;

FileObject::FileObject (char* filename, uint8_t mode)
{
	logk("New fileobject has a 'this' location of 0x%X\n", this);

	matchingFilename = (char*) malloc (256);
	strcpy (matchingFilename, filename);

	error = false;
	eof = false;

	logk("WE GOT INTO FileObject::FileObject\n");
	int res = vfs_open (&file, filename, mode);
	logk("WE OPENED THE FILE IN FileObject::FileObject\n");
	if (res) {
		logk("VFS OPEN DIDN'T OPEN SO WELL (objectmgr.cpp) err = %d\n", res);
		error = true;
		return;
	}
}

int FileObject::write (char* data, size_t size)
{
	logk("FileObject::write\ndatalen = %d, data = %s\n", size, data);
	int r = vfs_write(&file, (uint8_t*) data, size);
	logk("we should have called vfs_write\n");
	if (r) {
		logk("There was an error writing bytes!\n\r\n");
		error = true;
		return -1;
	}
	return size;			//must return bytes written
}

int FileObject::read (char* data, size_t size)
{
	uint64_t actual = 0;
	int r = vfs_read (&file, (uint8_t*) data, size, &actual);
	if (r) {
		error = true;
		return -1;
	}
	logk("%d bytes read from fileobject.\n", actual);
	for (int i = 0; i < (int) actual; ++i) {
		logk("0x%X (%c) ", data[i], data[i]);
	}
	return actual;			//must return bytes read
}

int FileObject::close()
{
	logk("ABOUT TO CALL VFS_CLOSE\n");
	int r = vfs_close(&file);
	logk("THE CLOSING OF THE FILE GAVE US A RETURN VALUE OF 0x%X\n", r);
	return r;
}

DirObject::DirObject (char* filename)
{
	matchingFilename = (char*) malloc (256);
	strcpy (matchingFilename, filename);

	error = false;
	eof = false;

	int res = vfs_opendir (&file, filename);
	if (res) {
		error = true;
		return;
	}
}

int DirObject::read (char* data, size_t size)
{
	struct dirent* dd = (struct dirent*) data;

	int r = vfs_readdir (&file, dd);

	if (r == vfs_status_eof) {
		return 1;
	}

	/*bool eof__;
	vfs_eof (&file, &eof__);
	if (eof__) {
		return ((int) -1);
	}*/

	if (r) {
		error = true;
		return r;
	}
	return 0;
}

int DirObject::close ()
{
	int r = vfs_closedir (&file);
	return r;
}

	/*int processThisBelongsTo;
	InternalPipeBit* pipe;
	*/

int PipeRead::read(char* outbuffer, size_t size)
{
	int read = 0;

	for (size_t i = 0; i < size; ++i) {

		while (this->pipe->ptr == 0) {
			return read;
		}

		outbuffer[i] = this->pipe->buffer[0];
		this->pipe->ptr -= 1;
		for (int j = 0; j < this->pipe->BUFFER_SIZE - 1; ++j) {
			this->pipe->buffer[j] = this->pipe->buffer[j + 1]; // copy next element left
		}
		this->pipe->buffer[this->pipe->BUFFER_SIZE - 1] = 0;			//keep the zeros coming
		++read;
	}

	return read;
}

int PipeWrite::write(char* abuffer, size_t size)
{
	memcpy(this->pipe->buffer + this->pipe->ptr, abuffer, size);
	this->pipe->ptr += size;

	return size;
}
