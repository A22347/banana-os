//
//  x86-64.s
//  Banana libc
//
//  Created by Alex Boxall on 28/9/18.
//  Copyright © 2018 Alex Boxall. All rights reserved.
//

// from https://github.com/eblot/newlib/blob/master/newlib/libc/machine/x86_64/setjmp.S

setjmp:
longjmp:
    ret

/*
 setjmp:
;    mov [rdi +  0], rbx
;    mov [rdi +  8], rbp
;    mov [rdi + 16], r12
;    mov [rdi + 24], r13
;    mov [rdi + 32], r14
;    mov [rdi + 40], r15

;    lea rax, [rsp + 8]
;    mov [rdi + 48], rax

;   mov rax, [rsp]
;    mov [rdi + 56], rax

;   mov rax, 0
;   ret

;longjmp:
;   mov rax, rsp        ;return value

;   mov rbp, [rdi + 8]

;   ;cli

;    mov rsp, [rdi+48]
;   push [rdi+56]
;   mov rbx, [rdi+0]
;   mov r12, [rdi+16]
;   mov r13, [rdi+24]
;   mov r14, [rdi+32]
;   mov r15, [rdi+40]

;   ;sti

;   ret

*/
