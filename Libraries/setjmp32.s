;global setjmp
;global longjmp

setjmp:
	mov edx, [esp + 4]

	pop ecx
	mov eax, 0

	mov [edx + 0], ebx
	mov [edx + 4], esp
	push ecx
	mov [edx + 8], ebp
	mov [edx + 12], esi
	mov [edx + 16], edi
	mov [edx + 20], ecx

	ret

longjmp:
	mov edx, [esp + 4]
	mov eax, [esp + 8]

	mov ebx, [edx + 0]
	mov esp, [edx + 4]
	mov ebp, [edx + 8]
	mov esi, [edx + 12]
	mov edi, [edx + 16]

	jmp dword [edx + 20]