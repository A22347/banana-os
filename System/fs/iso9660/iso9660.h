#pragma once

#ifdef __cplusplus
extern "C" {
#endif

#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>

	typedef struct isoFile_t
	{
		uint64_t fileStartLba;
		uint64_t fileLength;

		uint64_t seekMark;
		bool error;

		char driveletter;

	} isoFile_t;

	bool isoMount (char driveLetter);
	int isoOpen (char driveletter, isoFile_t* file, char* filename);
	int isoRead (isoFile_t* file, char* buffer, uint32_t length);

#ifdef __cplusplus
}
#endif