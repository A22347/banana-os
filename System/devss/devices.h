#pragma once

#include <stdint.h>
#include <stddef.h>
#include <stdbool.h>

#include <devss/devstruct.h>

extern Device* deviceTree;

void initDeviceSubsystem ();

void runDriver (Device* driver, DriverMode mode, int a, int b, void* data);

char* getDriverName (Device* device);

//sets up the Device* object to a basic, default state
void initDeviceObject (Device* device);

//calls the device driver's open command which sets up its part in the Device tree and its children
void openDevice (Device* device, int a, int b, void* data);

//these 4 end up calling runDriver
void readDevice (Device* device, int a, int b, void* data);
void writeDevice (Device* device, int a, int b, void* data);
void ioctlDevice (Device* device, int a, int b, void* data);
void hibernateDevice(Device* device, int a);		//0 = check if supported, 1 = hibernate, 2 = wake up

Device* getParent (Device* device);
int getNumberOfChildren (Device* device);
Device* getChild (Device* device, int index);

void addChild (Device* device, Device* childDevice);
void removeChild(Device* device, Device* theDevice);

void printTreeFrom(Device* d);