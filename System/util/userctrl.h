#pragma once

#include <stdint.h>
#include <stdbool.h>

void gen_random (char *s, const int len);
extern uint32_t lookup[256];

uint64_t PLACEHOLDER_hash (char* string);
void CreateNewUser (char* username, char* password, bool admin, char* clue);
void DeleteUser (char* username);

// Behold! The only union in Banana!
typedef union HashUnion_t
{
	uint64_t inthash;
	unsigned char strhash[8];

} HashUnion_t;

typedef struct readInStruct_t
{
	HashUnion_t hash;		//8 bytes
	uint8_t admin;			//1 byte
	uint8_t reserved1;
	uint8_t reserved2;
	uint8_t reserved3;
	char salt[28];			//27 bytes of salt + 1 null terminator (NULL TERMINATOR IS NOT USED IN THE HASHING, ONLY FOR STRING OPERATIONS)
	char clue[256];			//variable length

} readInStruct_t;

bool doesPasswordMatch (char* username, char* password);

/**
\brief Returns the location of the user's music folder.
*/
char* UserMusic ();

/**
\brief Returns the location of the user's trash bin.
*/
char* UserTrash ();

/**
\brief Returns the location of the user's folder.
*/
char* UserFolder ();

/**
\brief Returns the location of the user's desktop.
*/
char* UserDesktop ();

/**
\brief Returns the location of the user's music folder.
*/
char* UserPictures ();

/**
\brief Returns the location of the user's document folder.
*/
char* UserDocuments ();

void setup_settings ();