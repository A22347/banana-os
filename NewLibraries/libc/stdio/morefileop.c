//
//  morefileop.c
//  Banana libc
//
//  Created by Alex Boxall on 15/7/18.
//  Copyright © 2018 Alex Boxall. All rights reserved.
//

#ifndef _libk

#include "stdio.h"
#include "string.h"
#include "errno.h"
#include "stdlib.h"     //rand()

void perror(const char *s) {
    if (s == NULL || s[0] == 0) {
        //fprintf(stderr, "%s\n", strerror(errno));
    } else {
        //fprintf(stderr, "%s: %s\n", s, strerror(errno));
    }
}

int putc(int c, FILE *stream) {
    //The standard literally says:
    //  The putc function is equivalent to fputc, except that if it is implemented as a macro, it
    //  may evaluate stream more than once, so that argument should never be an expression
    //  with side effects.
    return fputc(c, stream);
}

int getc(FILE *stream) {
    //Same situation as above
    return fgetc(stream);
}

int getchar(void) {
    return getc(stdin);
}

int putchar(int c) {
    return putc(c, stdout);
}

int puts(const char *s) {
    int ret = fputs(s, stdout);
    if (ret) {
        return ret;
    }
    ret = putc('\n', stdout);
    return ret;                             //standard says any non-negative value
}

size_t fwrite(const void * restrict ptr, size_t size, size_t nmemb, FILE * restrict stream) {
    unsigned char* p = (unsigned char*) ptr;
    if (size == 0 || nmemb == 0) {
        return 0;
    }
    if (!stream->openAndValid) {
        return EOF;
    }
    if (stream->wideOriented == 1) {
        return EOF;
    }
    
    stream->wideOriented = 0;
    
    for (size_t el = 0; el < nmemb; ++el) {
        int ret = __i_internalFileWrite(stream, size, p);
        if (ret == EOF) {
            return el;
        }
        p += size;
    }
    
    return nmemb;
}

size_t fread (void * restrict ptr, size_t size, size_t nmemb, FILE * restrict stream) {
    unsigned char* p = (unsigned char*) ptr;
    if (size == 0 || nmemb == 0) {
        return 0;
    }
    if (!stream->openAndValid) {
        return EOF;
    }
    if (stream->wideOriented == 1) {
        return EOF;
    }
    
    stream->wideOriented = 0;
    
    for (size_t el = 0; el < nmemb; ++el) {
        int ret = __i_internalFileRead(stream, size, p);
        if (ret == EOF) {
            return el;
        }
        p += size;
    }
    
    return nmemb;
}

char *tmpnam(char *s) {
    static char* lastTime;
    static unsigned long long n = 0;
    if (s == NULL) {
        s = lastTime;
    }
    
    static char letter = 'A';
    
    snprintf(s, L_tmpnam, "C:/Banana/Temporary Files/_tmp%c_%08llX_%08llX_%08X_.tmp", letter, 0xDEADBEEFULL, n++, rand());
    lastTime = s;
    
    if (n == 0) {
        letter++;
    }
    
    return s;
}

FILE *tmpfile(void) {
    char* filename = malloc(FILENAME_MAX);
    tmpnam(filename);
    //TODO: store the filename in a linked list so it can be remove()d on exit()
    return fopen(filename, "wb+");
}

#endif
