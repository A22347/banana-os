#include <Banana/Image.hpp>

Image::Image (Window& parent, char n[256], uint16_t _x, uint16_t _y) : Widget (&parent)
{
	masterwindow = &parent;
	strcpy (image, n);
	x = _x;
	y = _y;
	width = 0;
	height = 0;

	rotation = 0;
	attributes = 0;
	xScale = 1;
	yScale = 1;

	displayed = false;
}

Image::~Image ()
{
	displayed = false;
}

void Image::display ()
{
	displayed = true;
	Event_t e;
	e.type = EventNull;
	selfupdate (e);
}

void Image::selfupdate (Event_t event)
{
	if (!displayed) {
		return;
	}
	SystemCall (SC_MakeImageAttributes, ((int) (xScale * 50)) << 16 | ((int) (yScale * 50)), attributes, (void*) ((int) (rotation * 50)));

	uint32_t data = APISystemCall (ASCMakeImage, getWindowNumber(), (x << 16) | y, image);
	width = data >> 16;
	height = data & 0xFFFF;

	char buffer[256];
	char buffer2[256];
	char buffer3[256];

	sprintf (buffer, "Width = %d (IGNORE %d)", width, 0);
	sprintf (buffer2, "Height = %d (IGNORE %d)", height, 0);
	sprintf (buffer3, "Rotation = %d (IGNORE %d)", (int)rotation, 0);

	System::Log (buffer);
	System::Log (buffer2);
	System::Log (buffer3);

	forceWindowToUpdate ();
	masterwindow->update ();
	displayed = true;
}

void Image::clear ()
{
	if (width == 0 || height == 0) {
		return;
	}

	for (int yy = 0; yy < height; ++yy) {
		for (int xx = 0; xx < width; ++xx) {
			masterwindow->drawPixel (xx + x, yy + y, 0xC0C0C0);
		}
	}

	forceWindowToUpdate ();
	masterwindow->update ();
	displayed = false;
}


void Image::setRotation (double degrees)
{
	rotation = degrees;
}

double Image::getRotation ()
{
	return rotation;
}

void Image::setXScale (double scale)
{
	xScale = scale;
}

double Image::getXScale ()
{
	return xScale;
}

void Image::setYScale (double scale)
{
	yScale = scale;
}

double Image::getYScale ()
{
	return yScale;
}


void Image::setAttributes (uint32_t attribs)
{
	attributes = attribs;
}

uint32_t Image::getAttributes ()
{
	return attributes;
}


