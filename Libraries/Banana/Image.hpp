#ifndef __IMAGE_HPP_
#define __IMAGE_HPP_

#include <Banana/Widget.hpp>
#include <Banana/Window.hpp>

enum ImageProperties
{
	IMAGE_GREYSCALE		 = 0x1,
	IMAGE_INVETED		 = 0x2,
	IMAGE_HIGHCONTRAST	 = 0x4,
	IMAGE_LOWCOLOUR_		 = 0x8,
	IMAGE_FLIPVERTICAL	 = 0x10,
	IAMGE_FLIPHORIZONTAL = 0x20
};

class Image : public Widget {
protected:
    char image[256];
    uint16_t x;
    uint16_t y;
    uint16_t width;
    uint16_t height;

	double rotation;
	double xScale;
	double yScale;
	uint32_t attributes;

	bool displayed;

public:
    Image(Window& window, char n[256], uint16_t x, uint16_t y);

	void setRotation (double degrees);
	double getRotation ();

	void setXScale (double scale);
	double getXScale ();

	void setYScale (double scale);
	double getYScale ();

	void setAttributes (uint32_t attribs);
	uint32_t getAttributes ();

    ~Image();
	void selfupdate (Event_t event);
	void display ();
    void clear();
};

#endif