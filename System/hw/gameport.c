#include <hw/gameport.h>
#include <hw/ports.h>
#include <debug/log.h>
#include <devss/devices.h>
#include <core/memory.h>

//gameport is a bit 'funky', as it has no gameport bus driver
//usually detect() is called after open(), but this time
//detect() is called by the legacy device manager, and open()
//is called if something is detected

//normal order is: configure (only first time device is found), open, detect
//gameport has   : detect, which then calls configure (only first time device is found), then open

bool joystick1 = false;
bool joystick2 = false;
Device* joy1Device;
Device* joy2Device;

void detectGameport(struct Device* parent)
{
	//detect if there is anything in the game port

	/*
	
	Reading the status of the joystick buttons is fairly simple. Just read the
	byte from the joystick port and check the status of the appropriate bit. A
	clear bit (0) means the button is pressed, a set bit (1) means it is not
	pressed. Note that the button's are not hardware debounced. Each time a
	button is pressed it's bit may "bounce" between 0 and 1 a couple of times.

	Reading the position of the stick positions is a bit more complicated. You
	must first write a dummy byte (any value will do) to the joystick port. This
	will set each axis bit to 1. You must then time how long the bit takes to
	drop back to 0, this time is roughly proportional to the position of
	the joystick axis (see Steve McGowan's discussion below).

*/

	bool foundJoy1 = 0;
	bool foundJoy2 = 0;

	int joy1Ons = 0;
	int joy1Offs = 0;
	int joy2Ons = 0;
	int joy2Offs = 0;

	for (int i = 0; i < 5000; ++i) {
		outb(0x201, 0xFF);		//write anything

		uint8_t status = inb(0x201);

		if (status & 0x3) {
			//this means either X or Y is set for joystick A, meaning something is there
			joy1Ons++;
		} else {
			joy1Offs++;
		}

		if (status & 0xC) {
			//this means either X or Y is set for joystick B, meaning something is there
			joy2Ons++;
		} else {
			joy2Offs++;
		}
	}

	//this stops us from getting '0xFF' back and thinking we have both joysticks
	//so instead it checks zeros and ones are read
	//(only 0.6% need to be 1, and 0.2% need to be zero)
	if (joy1Ons > 30 && joy1Offs > 10) foundJoy1 = true;
	if (joy2Ons > 30 && joy2Offs > 10) foundJoy2 = true;

	if ((foundJoy1 == joystick1) && (foundJoy2 == joystick2)) {
		//no change from last time
		return;
	}

	if (foundJoy1 == 0 && joystick1 == 1) {
		logk("WE LOST JOYSTICK 1!\n");
		removeChild(parent, joy1Device);
	}
	if (foundJoy2 == 0 && joystick2 == 1) {
		logk("WE LOST JOYSTICK 2!\n");
		removeChild(parent, joy2Device);
	}
	if (foundJoy1 == 1 && joystick1 == 0) {
		logk("WE GOT JOYSTICK 1!\n");

		//call configure() to work out what type it is first (it could use both joystick pins)
		//once it figures that out, it calls open
		configureGameport(parent, 0, 0, 0);

		Device* newdev = malloc(sizeof(Device));
		initDeviceObject(newdev);
		newdev->onDiskDriver = false;
		newdev->driverLocation = 0;
		newdev->openPointer = openGameport;
		strcpy(newdev->humanName, "GENERIC JOYSTICK A");
		addChild(parent, newdev);
		openDevice(newdev, 0, 0, 0); 
		
	}
	if (foundJoy2 == 1 && joystick2 == 0) {
		logk("WE GOT JOYSTICK 2!\n");

		//call configure() to work out what type it is first (it could use both joystick pins)
		//once it figures that out, it calls open
		configureGameport(parent, 1, 0, 0);

	}

	joystick1 = foundJoy1;
	joystick2 = foundJoy2;
}

void configureGameport(struct Device* parent, int a, int b, void* c)
{
	//basically this gets called when a joystick is hot-plugged in
	//OR when booted

	//check if a config file (or registry entry) exists, if so read in and check if it
	//matches the 'joystick1' and 'joystick2' settings
	//(e.g. if the file says 2x 2 axis joysticks, but only 1 is 
	//said to be plugged in, ignore it)

	//if config file couldn't be used (not existant, doesn't match)
	//should ask the user what type of joystick it is
	//store this in a variable (so read can read it correctly)
	//and save it to the config file (so on the first boot next time
	//we can load it in)

	//now open it

	Device* newdev = malloc(sizeof(Device));
	initDeviceObject(newdev);
	newdev->onDiskDriver = false;
	newdev->driverLocation = 0;
	newdev->openPointer = openGameport;
	newdev->internalDataSize = a;
	strcpy(newdev->humanName, "GENERIC JOYSTICK");
	addChild(parent, newdev);
	openDevice(newdev, 0, 0, 0);
}

void openGameport(struct Device* device, int a, int b, void* c)
{
	//there really isn't anything here
}

void readGameport(struct Device* device, int a, int b, void* c)
{
	//check the type and read
}

void writeGameport(struct Device* device, int a, int b, void* c);
void ioctlGameport(struct Device* device, int a, int b, void* c);
void closeGameport(struct Device* device, int a, int b, void* c);
