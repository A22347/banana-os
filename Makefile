all: libs install kernel jumper32 apps

#libs

install:
	cd Installer && ./build.bat
	
kernel:
	cd System && $(MAKE) all
	
libs:
	cd Libraries && (./build.bat || echo E)
	
jumper32:
	#cd D:/Users/Alex/Desktop && ./jumper32make.bat
	
apps:
	cd Applications && python build.py
	