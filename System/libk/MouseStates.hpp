//
//  MouseStates.h
//  GUI
//
//  Created by Alex Boxall on 29/5/19.
//  Copyright © 2019 Alex Boxall. All rights reserved.
//

#ifndef MouseStates_h
#define MouseStates_h

typedef struct MouseState {
    bool leftIsDown;
    bool middleIsDown;
    bool rightIsDown;
    bool button4IsDown;
    bool button5IsDown;

    bool leftWasDown;
    bool middleWasDown;
    bool rightWasDown;
    bool button4WasDown;
    bool button5WasDown;
    
    int mousex;
    int mousey;
    
} MouseState;

#endif /* MouseStates_h */
