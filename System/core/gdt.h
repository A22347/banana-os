#pragma once

#include <core/kernel.h>

typedef enum GDTAccess
{
	//note: all of these fields set
	//      the sets "must be 1" bit
	GDTAccessPresent = (1 << 7) | (1 << 4),
	GDTAccessRing0 = (1 << 4),
	GDTAccessRing3 = (3 << 5) | (1 << 4),
	GDTAccessExecutable = (1 << 3) | (1 << 4),
	GDTAccessGrowDownData = (1 << 2) | (1 << 4),		//for data segments only
	GDTAccessConformCode = (1 << 2) | (1 << 4),		//for data segments only
	GDTAccessReadableCode = (1 << 1) | (1 << 4),
	GDTAccessWritableData = (1 << 1) | (1 << 4),
	GDTAccessAccessed = (1 << 0) | (1 << 4),

} GDTAccess;

typedef enum GDTFlags
{
	GDTFlagGranularity = (1 << 3),
	GDTFlag32Bit = (1 << 2),
	GDTFlag64Bit = (1 << 1),

} GDTFlags;

typedef struct GDTEntry
{
	uint64_t limitLow : 16;
	uint64_t baseLow : 24;
	uint64_t access : 8;
	uint64_t limitHigh : 4;
	uint64_t flags : 4;
	uint64_t baseHigh : 8;

} GDTEntry;

#pragma pack(push,1)
typedef struct GDTDescriptor
{
	uint16_t size;
	size_t offset;

} GDTDescriptor;

#pragma pack(pop)


//maximum number of entries is 65536 / 8 = 8192
extern GDTEntry gdt[256];
extern uint16_t numberOfGDTEntries;
extern GDTDescriptor gdtDescriptor;

void setupGDT ();
void flushGDT ();
uint16_t addGDTEntry (size_t base, size_t limit, uint8_t access, uint8_t flags);

//to be used as though they are constants
//so instead of hardcoding 0x18 as user code,
//use 'gdtUserCode'
extern uint16_t gdtNull;
extern uint16_t gdtCode;
extern uint16_t gdtData;
extern uint16_t gdtUserCode;
extern uint16_t gdtUserData;
extern uint16_t gdtTSS[4];		//one per CPU
extern uint16_t gdtCode32;
extern uint16_t gdtData32;
