//
//  Window.hpp
//  GUI
//
//  Created by Alex Boxall on 29/5/19.
//  Copyright © 2019 Alex Boxall. All rights reserved.
//

#ifndef Window_hpp
#define Window_hpp

#include "Control.hpp"
#include <stdint.h>

class Window : public Control {
private:
protected:
    static int nextX;
    static int nextY;
public:
    Window (Window* parent, const char* title, int x = -1, int y = -1, int w = -1, int h = -1);
    virtual void internalDraw();
    virtual Window* copy();
    virtual bool canHoldFocus();     //whether the tab key can be used to select this control
    
    uint32_t* buffer;
    virtual void drawPixel (int xx, int yy, uint32_t col);
    void mainloop();
    virtual void draw();
    
    void copyToScreen();
    virtual const char* debugName();
};

extern Window* windowWithFocus;

#endif /* Window_hpp */
