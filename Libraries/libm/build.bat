cd source

set TOOLPREFIX=i386-banana-
make clean -j
make USEGCC=1 ARCH=i386  -j
cp libopenlibm.a %SYSROOT32%/usr/lib/libm.a
cp %SYSROOT32%/usr/lib/libm.a D:/Users/Alex/Desktop/banana-os/System/libk/libm32.a

set TOOLPREFIX=x86_64-banana-
make clean -j
make USEGCC=1 ARCH=x86_64 -j
cp libopenlibm.a %SYSROOT%/usr/lib/libm.a
cp %SYSROOT64%/usr/lib/libm.a D:/Users/Alex/Desktop/banana-os/System/libk/libm64.a

robocopy ../inc %SYSROOT%/usr/include/ /E
robocopy ../inc %SYSROOT32%/usr/include/ /E
robocopy ../inc D:\Users\Alex\Desktop\banana-os\System\libk /E
pause