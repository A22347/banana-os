#include <fs/fat/vfs_wrapper.h>
#include <core/memory.h>
#include <debug/log.h>
#include <core/panic.h>
#include "D:\Users\Alex\Desktop\banana-os\Libraries\newlib-3.0.0\newlib\libc\sys\banana\sys\dirent.h"

//how this works:
//		VFS decides drive numblettersers:
//		e.g.	FAT12 floppy = A:
//				             = B:
//				FAT32 hdd    = C:
//				CDFS  cd-rom = D:
//				exFAT usb    = E:
//				FAT32 usb    = F:
//
//then these variables are used to
//translate them into *FAT drive letters*
//		e.g.	FAT A: = VFS A:
//		e.g.	FAT B: = VFS C:
//		e.g.	FAT C: = VFS F:
//
//this will ONLY work if you **ACTUALLY USE THE VFS FUNCTIONS**
//

FATFS fatFilesystems[10];
char matchingVFSDriveLetter[10];
int numberOfFatFilesystems = 0;

char getFATDriveLetterFromVFSDriveLetter (char vfsDriveLetter)
{
	for (int i = 0; i < numberOfFatFilesystems; ++i) {
		if (matchingVFSDriveLetter[i] == vfsDriveLetter) {
			return 'A' + i;
		}
	}
	panicWithMessage ("BIG_FAT_ERROR!");
	return 0;
}

int fat_wrapper_open (char vfsDriveLetter, void** ptr, char* path, uint8_t mode)
{
	if (ptr == 0) {
		return vfs_status_bad_argument;
	}

	*ptr = calloc (sizeof (FIL), 1);

	//drive letter NEVER comes attached to 'path', so add it
	char newpath[256];
	memset (newpath, 0, 256);
	newpath[0] = getFATDriveLetterFromVFSDriveLetter (vfsDriveLetter);
	newpath[1] = ':';
	strcat (newpath, path);

	BYTE specificMode = 0;
	if (mode & vfs_file_mode_read)	 specificMode |= FA_READ;
	if (mode & vfs_file_mode_write)	 specificMode |= FA_CREATE_ALWAYS | FA_WRITE;
	if (mode & vfs_file_mode_append) specificMode |= FA_OPEN_APPEND | FA_WRITE;
	logk("OPEN *PTR = 0x%X\n", *ptr);
	logk("OPENING FAT PATH: %s\n", newpath);
	FRESULT r = f_open (*ptr, newpath, specificMode);
	switch (r) {
	case FR_OK:
		return vfs_status_success;
		//many more cases go here...
	default:
		logk ("fat_wrapper_open error %d\n", r);
		//nothing should be left here...
		return vfs_status_SOMETHING_ELSE;
	}
}

int fat_wrapper_read (void* ptr, uint8_t* buffer, uint64_t bytes, uint64_t* actualRead)
{
	if (ptr == 0) {
		return vfs_status_bad_argument;
	}

	UINT br;
	FRESULT r = f_read ((FIL*) ptr, buffer, bytes, &br);
	logk("READ *PTR = 0x%X\n", ptr);

	logk("%d: %s\n", br, buffer);
	*actualRead = br;

	switch (r) {
	case FR_OK:
		return vfs_status_success;
		//many more cases go here...
	default:
		logk ("READ ERROR (FATFS CODE = %d)\n", r);
		//nothing should be left here...
		return vfs_status_SOMETHING_ELSE;
	}
}

int fat_wrapper_write (void* ptr, uint8_t* buffer, uint64_t bytes)
{
	if (ptr == 0) {
		return vfs_status_bad_argument;
	}

	UINT br;
	FRESULT r = f_write ((FIL*) ptr, buffer, bytes, &br);

	switch (r) {
	case FR_OK:
		return vfs_status_success;
		//many more cases go here...
	default:
		//nothing should be left here...
		logk("WRITE ERROR (FATFS CODE = %d)\n", r);
		return vfs_status_SOMETHING_ELSE;
	}
}

int fat_wrapper_unlink(char* filename)
{
	if (filename == 0) {
		return vfs_status_bad_argument;
	}

	f_unlink(filename);

	return vfs_status_success;
}

int fat_wrapper_size (void* ptr, uint64_t* output)
{
	if (ptr == 0) {
		return vfs_status_bad_argument;
	}

	*output = f_size ((FIL*) ptr);

	return vfs_status_success;
}

int fat_wrapper_eof (void* ptr, bool* eof)
{
	if (ptr == 0) {
		return vfs_status_bad_argument;
	}

	*eof = f_eof ((FIL*) ptr);

	return vfs_status_success;
}


int fat_wrapper_seek (void* ptr, uint64_t bytes)
{
	if (ptr == 0) {
		return vfs_status_bad_argument;
	}

	FRESULT r = f_lseek ((FIL*) ptr, bytes);

	switch (r) {
	case FR_OK:
		return vfs_status_success;
		//many more cases go here...
	default:
		//nothing should be left here...
		return vfs_status_SOMETHING_ELSE;
	}
}

int fat_wrapper_close (void* ptr)
{
	if (ptr == 0) {
		logk("fat_wrapper_close invalid argument.\n");
		return vfs_status_bad_argument;
	}

	FRESULT r = f_close ((FIL*) ptr);
	logk("fat_wrapper_close gave %d as a return value.\n", r);

	switch (r) {
	case FR_OK:
		return vfs_status_success;
		//many more cases go here...
	default:
		//nothing should be left here...
		return vfs_status_SOMETHING_ELSE;
	}
}

int fat_wrapper_closedir (void* ptr)
{
	if (ptr == 0) {
		return vfs_status_bad_argument;
	}

	FRESULT r = f_closedir ((DIR*) ptr);
	logk("CLOSEDIR!\n");
	logk("ptr = 0x%X\n", ptr);
	freee(ptr);

	switch (r) {
	case FR_OK:
		return vfs_status_success;
		//many more cases go here...
	default:
		//nothing should be left here...
		return vfs_status_SOMETHING_ELSE;
	}
}

///int (*opendir) (char, void**, char*);
int fat_wrapper_opendir (char vfsDriveLetter, void** ptr, char* filename)
{
	if (ptr == 0) {
		return vfs_status_bad_argument;
	}

	*ptr = malloc (sizeof (DIR));
	logk("*ptr = 0x%X\n", *ptr);
	logk("SIZEOF DIR = 0x%X\n", sizeof(DIR));

	//drive letter NEVER comes attached to 'path', so add it
	char newpath[256];
	memset (newpath, 0, 256);
	newpath[0] = getFATDriveLetterFromVFSDriveLetter (vfsDriveLetter);
	newpath[1] = ':';
	strcat (newpath, filename);

	FRESULT r = f_opendir (*ptr, newpath);
	switch (r) {
	case FR_OK:
		return vfs_status_success;
		//many more cases go here...
	default:
		logk ("fat_wrapper_opendir error %d\n", r);
		//nothing should be left here...
		return vfs_status_SOMETHING_ELSE;
	}
}


int fat_wrapper_mkdir(char vfsDriveLetter, char* filename)
{
	//drive letter NEVER comes attached to 'path', so add it
	char newpath[256];
	memset(newpath, 0, 256);
	newpath[0] = getFATDriveLetterFromVFSDriveLetter(vfsDriveLetter);
	newpath[1] = ':';
	strcat(newpath, filename);

	FRESULT r = f_mkdir(newpath);
	switch (r) {
	case FR_OK:
		return vfs_status_success;
		//many more cases go here...
	default:
		logk("fat_wrapper_mkdir error %d\n", r);
		//nothing should be left here...
		return vfs_status_SOMETHING_ELSE;
	}
}


int fat_wrapper_mount (char letter)
{
	UINT br;
	char diskCmd[] = "A:";

	diskCmd[0] += numberOfFatFilesystems;		// letter - 'A';

	//allow 'mount' and 'open' to call the FatFS read/write functions
	//using this in order to translate back (e.g. A->C)
	matchingVFSDriveLetter[numberOfFatFilesystems] = letter;

	FATFS* fff = malloc(sizeof(FATFS));
	FRESULT r = f_mount (fff, diskCmd, 0);

	switch (r) {
	case FR_OK:
		break;
	default:
		//nothing should be left here...
		freee(fff);
		return vfs_status_SOMETHING_ELSE;
	}

	FATFS *fs;
	DWORD fre_clust;

	char opentest[256];
	memset (opentest, 0, 256);
	strcat (opentest, diskCmd);
	strcat (opentest, "/DUMMY.TXT");		//doesn't need to exist

	FIL fptest;
	r = f_open (&fptest, opentest, FA_READ);

	switch (r) {
	case FR_OK:					//if it could be read, that means it exists and the FS works
	case FR_NO_PATH:			//could not file file/dir is not actually bad, as it means we
	case FR_NO_FILE:			//actually have a file system (we didn't get FR_NO_FILESYSTEM)
		f_close (&fptest);
		logk ("For reference, VFS drive %c is now FAT drive %d (%c)\n", letter, numberOfFatFilesystems, 'A' + numberOfFatFilesystems);
		numberOfFatFilesystems++;
		f_unlink (opentest);
		return vfs_status_success;
	default:					//FR_NO_FILESYSTEM
		matchingVFSDriveLetter[numberOfFatFilesystems] = '?';
		freee(fff);
		f_mount (0, diskCmd, 0);                     // Unmount the drive
		//nothing should be left here...
		return vfs_status_SOMETHING_ELSE;
	}

	
	//many more cases go here...
}

int fat_wrapper_readdir (void* directory, struct dirent* out)
{
	if (out == 0) {
		return vfs_status_bad_argument;
	}
	UINT br;
	FILINFO fno;
	FRESULT r = f_readdir ((DIR*) directory, &fno);

	if (fno.fname[0] == 0) {
		return vfs_status_eof;
	}

	switch (r) {
	case FR_OK:
		out->d_type = (fno.fattrib & AM_DIR) ? DT_DIR : DT_REG;
		strcpy(out->d_name, fno.fname);
		return vfs_status_success;
		//many more cases go here...
	default:
		logk ("READDIR ERROR (FATFS CODE = %d)\n", r);
		//nothing should be left here...
		return vfs_status_SOMETHING_ELSE;
	}
}

void fat_install ()
{
	filesystem_driver_function_struct fatFuncs;
	memset (&fatFuncs, 0, sizeof (fatFuncs));

	fatFuncs.open = fat_wrapper_open;
	fatFuncs.read = fat_wrapper_read;
	fatFuncs.write = fat_wrapper_write;
	fatFuncs.close = fat_wrapper_close;
	fatFuncs.mount = fat_wrapper_mount;
	fatFuncs.size = fat_wrapper_size;
	fatFuncs.eof = fat_wrapper_eof;
	fatFuncs.opendir = fat_wrapper_opendir;
	fatFuncs.readdir = fat_wrapper_readdir;
	fatFuncs.closedir = fat_wrapper_closedir;
	fatFuncs.seek = fat_wrapper_seek;
	fatFuncs.unlink = fat_wrapper_unlink;
	fatFuncs.rmdir = fat_wrapper_unlink;			//uses same function
	fatFuncs.mkdir = fat_wrapper_mkdir;

	vfs_install_filesystem (fatFuncs);

	extern void initFATFS();
	initFATFS();
	//f_setcp (437);
}
