#include <Banana/Label.hpp>
#include <Banana/Core.hpp>

Label::Label (Window& parent, char* _text, int _x, int _y, char* _font) : Widget (&parent) {
    masterwindow = &parent;
    masterwindow->addWidgetChild(this);
    
    labelfont = (char*) malloc(strlen(_font) + 1);
    memcpy(labelfont, _font, strlen(_font) + 1);

    text = (char*) malloc(strlen(_text) + 1);
    memcpy(text, _text, strlen(_text) + 1);
    
    x = _x;
    y = _y;
    
    requiresupdating = true;
}

Label::~Label () {
    free((void*)text);
}

void Label::_write () {
    masterwindow->write(text, x, y, labelfont);
}

void Label::_clear () {
    uint16_t pixelPointer = x;
    uint8_t fontno = SystemCall(SC_GetFontNumberFromName, 0, 0, (void*) labelfont);
        
    for (int i = 0; text[i]; ++i) {
        uint8_t width = APISystemCall(ASCGetCharacterWidth, text[i], fontno, 0);
        masterwindow->drawCharacter(text[i], pixelPointer, y, 0xC0C0C0, 0xC0C0C0, fontno);
        pixelPointer += width;
    }
}

void Label::selfupdate (Event_t data) {
    if (requiresupdating) {
        _clear();
        _write();
        requiresupdating = false;
    }
}


void Label::setText (char* _text) {
    _clear();
    free((void*)text);
    text = (char*) malloc(strlen(_text) + 1);
	strcpy (text, _text);
    _write();
}

char* Label::getText () {
    return text;
}