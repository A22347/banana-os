#ifndef __SYS_TYPES_H__
#define __SYS_TYPES_H__

#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>
#include <../System/core/syscalldef.h>

#define MAX_THREADS_PER_PROCESS 32
#define MAX_THREAD_SPECIFIC_STORAGE_LOCATIONS 32

// some will be found in syscalldef.h for kernel/library compatibility
typedef unsigned long long clock_t;
typedef unsigned long ssize_t;
typedef unsigned long long clockid_t;
typedef unsigned long long fsblkcnt_t;
typedef unsigned long long fsfilcnt_t;
typedef unsigned long long id_t;
typedef unsigned long long key_t;		//is actually a struct
typedef long long 		   pid_t;
typedef long long 		   suseconds_t;
typedef unsigned long long timer_t;
typedef unsigned long long useconds_t;
typedef unsigned long long pthread_key_t;

typedef unsigned long long pthread_t;
typedef unsigned long long pthread_once_t;

typedef struct sched_param
{
	int _IGNORE;

} sched_param;


typedef struct pthread_attr_t
{
	int detachstate;
	size_t guardsize;
	int inheritsched;
	struct sched_param schedparam;
	int schedpolicy;
	int scope;
	void* stackaddr;
	size_t stacksize;

} pthread_attr_t;

typedef struct pthread_mutex_t
{
	pid_t owner;		//actually a thread ID
	uint8_t locked;

} pthread_mutex_t;

typedef struct pthread_mutexattr_t
{

} pthread_mutexattr_t;

typedef struct pthread_condattr_t
{

} pthread_condattr_t;

typedef struct pthread_cond_t
{
	bool blockedThreads[MAX_THREADS_PER_PROCESS];

} pthread_cond_t;

#endif