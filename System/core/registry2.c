#include <core/registry2.h>
#include <fs/vfs.h>
#include <libk/stdio.h>
#include <core/kernel.h>
#include <libk/string.h>
#include <debug/log.h>
#include <core/memory.h>

bool lastRegOpSuccess = false;

bool checkLine(char* line, char* key, bool integer)
{
	if (integer && line[0] != 'I') return false;
	if (!integer && line[0] != 'S') return false;

	char kkey[256];
	memset(kkey, 0, 256);
	strcpy(kkey, key);
	strcat(kkey, " ");		//so it matches the end of the key, so "ABCD" is not matched to key "ABC"

	// + 2 skips the 'S ' or 'I '
	return !strncasecmp(line + 2, kkey, strlen(kkey));
}

char* generalRegistryRead(char* key, bool integer)
{
	logk("Lookin' for registry key %s. (%d)\n", key, integer);
	lastRegOpSuccess = false;

	char fileSplit[256];
	char keySplit[256];

	memset(fileSplit, 0, 256);
	memset(keySplit, 0, 256);

	int i = 0;
	int j = 0;

	bool keySpl = false;

	for (; key[j]; ++i, ++j) {
		if (key[j] == '/') {
			keySpl = true;
			i = -1;			//it will be on zero the next time around
			continue;
		}
		if (keySpl) {
			keySplit[i] = key[j];
		} else {
			fileSplit[i] = key[j];
		}
	}

	char sysPath[256];
	char userPath[256];

	sprintf(sysPath, "C:/Banana/Registry/@REG/System/%s.reg", fileSplit);

	if (username[0]) {
		sprintf(userPath, "C:/Banana/Registry/@REG/Users/%s/%s.reg", username, fileSplit);
	} else {
		strcpy(userPath, sysPath);
	}

	bool lookingAtUser = false;

	vfs_file_t f;
	int res = vfs_open(&f, sysPath, vfs_file_mode_read);
	if (res) {
		res = vfs_open(&f, userPath, vfs_file_mode_read);
		if (res) {
			logk("Could not read from registry! A\n");
			return 0;
		}
		lookingAtUser = true;
	}

	char line[256];
	bool found = false;

	while (1) {
		memset(line, 0, 255);
		while (vfs_gets(line, 255, &f) == 0) {
			if (line[strlen(line) - 1] == '\n') {
				line[strlen(line) - 1] = 0;
			}
			if (checkLine(line, keySplit, integer)) {
				found = true;
				break;
			}
			line[0] = 0;
		}
		if (line[0]) {
			if (checkLine(line, keySplit, integer)) {
				found = true;
				break;
			}
		}

		if (!found && !lookingAtUser) {
			lookingAtUser = true;
			res = vfs_open(&f, userPath, vfs_file_mode_read);
			if (res) {
				logk("Could not read from registry! B\n");
				return 0;
			}
		} else {
			break;
		}
	}

	if (!found) return 0;

	vfs_close(&f);

	char* nline = malloc(256);
	strcpy(nline, line);
	return nline;
}

void generalRegistryWrite(char* key, bool system, uint64_t integer, char* newValue)
{
	lastRegOpSuccess = false;

	char fileSplit[256];
	char keySplit[256];

	memset(fileSplit, 0, 256);
	memset(keySplit, 0, 256);

	int i = 0;
	int j = 0;

	bool keySpl = false;

	for (; key[j]; ++i, ++j) {
		if (key[j] == '/') {
			keySpl = true;
			i = -1;			//it will be on zero the next time around
			continue;
		}
		if (keySpl) {
			keySplit[i] = key[j];
		} else {
			fileSplit[i] = key[j];
		}
	}

	char sysPath[256];
	char userPath[256];

	sprintf(sysPath, "C:/Banana/Registry/@REG/System/%s.reg", fileSplit);

	if (username[0]) {
		sprintf(userPath, "C:/Banana/Registry/@REG/Users/%s/%s.reg", username, fileSplit);
	} else {
		strcpy(userPath, sysPath);
	}

	vfs_file_t f;
	if (system) {
		vfs_open(&f, sysPath, vfs_file_mode_read);
	} else {
		vfs_open(&f, userPath, vfs_file_mode_read);
	}

	i = 0;
	char* lines = malloc(256 * 256);

	while (vfs_gets(lines + i * 256, 255, &f) == 0) {
		logk("REG2 WRITE: got line %s\n", lines + i * 256);
		if (checkLine(lines + i * 256, keySplit, integer)) {
			//copy new line in
			if (integer) {
				//the compiler is quite picky about which type we use
#if PLATFORM_ID == 64
				sprintf(lines + i * 256, "I %s %ld", keySplit, integer);
#else
				sprintf(lines + i * 256, "I %s %lld", keySplit, integer);
#endif
			} else {
				sprintf(lines + i * 256, "S %s %s", keySplit, newValue);
			}
		}

		++i;
	}

	vfs_close(&f);
	if (system) {
		vfs_open(&f, sysPath, vfs_file_mode_write);
	} else {
		vfs_open(&f, userPath, vfs_file_mode_write);
	}

	for (int j = 0; j < i; ++j) {
		if (lines[j * 256] == '\n' || lines[j * 256] == '\r') continue;
		logk("REG2 WRITING: got line '%s'\n", lines + j *256);
		vfs_write(&f, (uint8_t*) lines + j * 256, strlen(lines + j * 256));
	}
	vfs_close(&f);

	freee(lines);

	return;
}

bool registryKeyExists(char* key) { 
	char* res = generalRegistryRead(key, false);		//check if exists as string
	if (res) return true;
	res = generalRegistryRead(key, true);				//same for integer
	if (res) return true;
	return false;
}

char* readRegistryString(char* key)
{
	char* line = generalRegistryRead(key, false);
	if (line == 0) return 0;

	char* outline = malloc(256);
	memset(outline, 0, 255);

	int spaces = 0;
	int j = 0;
	for (int i = 0; line[i]; ++i) {
		if (line[i] == '\n') continue;
		if (spaces >= 2) {
			outline[j++] = line[i];

		} else if (line[i] == ' ') {
			spaces++;
		}
	}

	freee(line);

	lastRegOpSuccess = true;

	return outline;
}

uint64_t readRegistryInteger(char* key)
{
	char* line = generalRegistryRead(key, true);
	if (line == 0) return 0;

	uint64_t outnum = 0;

	int spaces = 0;
	bool neg = false;
	bool hex = false;
	for (int i = 0; line[i]; ++i) {
		if (spaces >= 2) {
			if (line[i] == 'x') {
				hex = true;
				continue;
			}
			if (line[i] == '-') {
				neg = true;
			}
			if (!hex) {
				if (line[i] >= '0' && line[i] <= '9') {
					outnum *= 10;
					outnum += line[i] - '0';
				}
			} else {
				if (line[i] >= '0' && line[i] <= '9') {
					outnum *= 16;
					outnum += line[i] - '0';
				} else if (line[i] >= 'A' && line[i] <= 'F') {
					outnum *= 16;
					outnum += line[i] - 'A' + 0xA;
				} else if (line[i] >= 'a' && line[i] <= 'f') {
					outnum *= 16;
					outnum += line[i] - 'a' + 0xA;
				}
			}
			
		} else if (line[i] == ' ') {
			spaces++;
		}
	}

	freee(line);

	lastRegOpSuccess = true;

	return neg ? 0 - outnum : outnum;
}

void writeRegistryString(char* key, char* newValue, bool system)
{
	generalRegistryWrite(key, system, false, newValue);
}

void writeRegistryInteger(char* key, uint64_t newValue, bool system)
{
	generalRegistryWrite(key, system, newValue, 0);
}

void deleteRegistryKey(char* key, bool system);

bool readRegistrySuccess()
{
	return lastRegOpSuccess;
}