//
//  Event.hpp
//  Banana API (New)
//
//  Created by Alex Boxall on 18/5/18.
//  Copyright © 2018 Alex Boxall. All rights reserved.
//

#ifndef Event_hpp
#define Event_hpp

#if defined(__cplusplus)
extern "C" {
#endif

#include "../../../System/core/syscalldef.h"

	typedef Event_t Event;
	typedef DragDropData_t DragDrop;

/*! \file Event.h
 \brief Definitions for event related structures and enumerations. The same file is synced with the kernel.
 */
    
#if defined(__cplusplus)
}
#endif

#endif /* Event_hpp */
