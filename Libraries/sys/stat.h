#ifndef __SYS_STAT_H__
#define __SYS_STAT_H__

#include <time.h>
#include <../System/core/syscalldef.h>


/*
LOTS OF DEFINITIONS WILL BE FOUND IN SYSCALLDEF.H FOR KERNEL COMPATIBILITY

(INCLUDING struct stat)
*/

int chmod(const char *, mode_t);
int fchmod(int, mode_t);
int fstat(int, struct stat *);
int lstat(const char *, struct stat *);
int mkdir(const char *, mode_t);
int mkfifo(const char *, mode_t);
int mknod(const char *, mode_t, dev_t);
int stat(const char *, struct stat *);
mode_t umask(mode_t);

#endif