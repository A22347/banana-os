#pragma once

#include <stdbool.h>
#include <stdint.h>

#define ARP_REQUEST 0x0001
#define ARP_REPLY	0x0002

struct ARPPacketIPV4
{
	uint16_t htype;
	uint16_t ptype;
	uint8_t  hlen;
	uint8_t  plen;
	uint16_t opcode;
	uint8_t  smac[6];
	uint32_t sip;
	uint8_t  dmac[6];
	uint32_t dip;

} __attribute__((packed));

typedef struct ARPPacketIPV4 ARPPacketIPV4;

typedef struct ARPCache
{
	uint32_t ip;
	uint8_t mac[6];

} ARPCache;

extern ARPCache arpCache[1024];
extern int nextARPCache;