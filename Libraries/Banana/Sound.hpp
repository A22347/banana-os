#ifndef __SOUND_HPP__
#define __SOUND_HPP__

#include "Core.hpp"

//VOLUME CURRENTLY IS NOT SUPPORTED AT ALL

/**
\class Sound
\brief A lightweight interface to play audio files
\note Only 8 or 16 bit WAV files can be played
*/
class Sound {
protected:
    int soundnumber;
    int varvolume;
    bool varplaying;
    bool error;
    
public:
    Sound (char* filename, bool playing = true);    ///< Load a audio file and optionally play it
    ~Sound ();
    void play();    ///< Play the audio file
    void pause();    ///< Pause the audio file
    void stop();        ///<Stop the audio file. The Sound object becomes unusable after this is called
    bool playing();    
    int volume(int volume = -1);    ///<Returns volume, and sets it if the volume isn't the default argument (of -1)
};

#endif