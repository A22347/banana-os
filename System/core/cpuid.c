#include <core/cpuid.h>

void cpuid (int code, size_t* a, size_t* b, size_t* c, size_t* d)
{
	asm volatile ("cpuid" : "=a"(*a), "=b"(*b), "=c"(*c), "=d"(*d) : "a"(code), "c"(0) : "cc");
}