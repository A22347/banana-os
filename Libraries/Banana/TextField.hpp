#ifndef __TEXTFIELD_HPP__
#define __TEXTFIELD_HPP__

#include "Core.hpp"
#include "Widget.hpp"
#include "Window.hpp"

class TextField : public Widget {
    friend class Window;
    
protected:
    Window* masterwindow;
    char text[10000];
    char textfieldfont[256];
    int x;
    int y;
    int textfieldwidth;
    void _write();
    void _clear();
    int cursorposition = 0;
    int endcurposition = 0;
	bool focusedLastTime = false;
    bool focus = false;

	bool autoLooseFocusOnMouseClick ();
    
public:
    TextField (Window& parent, int _x, int _y, int _width = 40);
    ~TextField ();
    void selfupdate (Event_t event);
	void set(char* c);
    char* get();

	bool editable = true;
};

#endif