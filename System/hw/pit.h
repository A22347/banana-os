#pragma once

#include <hal/interrupts.h>

void setupPIT (int hz);
void unsetupPIT ();