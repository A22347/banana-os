#pragma once

#include <hw/ports.h>
#include <hal/interrupts.h>
#include <devss/devices.h>

// Floppy Disk Driver - Uses Public Domain code from http://www.brokenthorn.com/Resources/OSDev20.html

enum FLPYDSK_IO {
	FLPYDSK_DOR  =	0x3f2,
	FLPYDSK_MSR	 =	0x3f4,
	FLPYDSK_FIFO =	0x3f5,
	FLPYDSK_CTRL =	0x3f7
};

enum FLPYDSK_DOR_MASK {
	FLPYDSK_DOR_MASK_DRIVE0			=	0,
	FLPYDSK_DOR_MASK_DRIVE1			=	1,
	FLPYDSK_DOR_MASK_DRIVE2			=	2,
	FLPYDSK_DOR_MASK_DRIVE3			=	3,
	FLPYDSK_DOR_MASK_RESET			=	4,
	FLPYDSK_DOR_MASK_DMA			=	8,
	FLPYDSK_DOR_MASK_DRIVE0_MOTOR	=	16,
	FLPYDSK_DOR_MASK_DRIVE1_MOTOR	=	32,
	FLPYDSK_DOR_MASK_DRIVE2_MOTOR	=	64,
	FLPYDSK_DOR_MASK_DRIVE3_MOTOR	=	128
};

enum FLPYDSK_MSR_MASK {
	FLPYDSK_MSR_MASK_DRIVE1_POS_MODE	=	1,
	FLPYDSK_MSR_MASK_DRIVE2_POS_MODE	=	2,
	FLPYDSK_MSR_MASK_DRIVE3_POS_MODE	=	4,
	FLPYDSK_MSR_MASK_DRIVE4_POS_MODE	=	8,
	FLPYDSK_MSR_MASK_BUSY			    =	16,
	FLPYDSK_MSR_MASK_DMA			    =	32,
	FLPYDSK_MSR_MASK_DATAIO			    =	64,
	FLPYDSK_MSR_MASK_DATAREG		    =	128
};

enum FLPYDSK_CMD {
	FDC_CMD_READ_TRACK	 =	2,
	FDC_CMD_SPECIFY		 =	3,
	FDC_CMD_CHECK_STAT	 =	4,
	FDC_CMD_WRITE_SECT	 =	5,
	FDC_CMD_READ_SECT	 =	6,
	FDC_CMD_CALIBRATE	 =	7,
	FDC_CMD_CHECK_INT	 =	8,
	FDC_CMD_WRITE_DEL_S	 =	9,
	FDC_CMD_READ_ID_S	 =	10,
	FDC_CMD_READ_DEL_S	 =	12,
	FDC_CMD_FORMAT_TRACK =	13,
	FDC_CMD_SEEK		 =	15
};

enum FLPYDSK_CMD_EXT {
	FDC_CMD_EXT_SKIP	    =	0x20,
	FDC_CMD_EXT_DENSITY	    =	0x40,
	FDC_CMD_EXT_MULTITRACK	=	0x80
};

enum FLPYDSK_GAP3_LENGTH {
	FLPYDSK_GAP3_LENGTH_STD      = 42,
	FLPYDSK_GAP3_LENGTH_5_14     = 32,
	FLPYDSK_GAP3_LENGTH_3_5      = 27
};

enum FLPYDSK_SECTOR_DTL {
	FLPYDSK_SECTOR_DTL_128	=	0,
	FLPYDSK_SECTOR_DTL_256	=	1,
	FLPYDSK_SECTOR_DTL_512	=	2,
	FLPYDSK_SECTOR_DTL_1024	=	4
};

extern bool floppyirq;
extern uint8_t _CurrentDrive;

bool detectFDC (struct Device* self);
void openFDC(struct Device* self, int, int, void*);
void openFloppy(struct Device* self, int, int, void*);
void readFloppy(struct Device* self, int, int, void*);
void writeFloppy(struct Device* self, int, int, void*);

void floppy_handler (struct regs *r);
void wait_irq ();
void init_dma ();

void read_dma ();
void write_dma ();
void write_dor (uint8_t val);

uint8_t read_status ();

void floppy_send_cmd (uint8_t cmd);

uint8_t floppy_read_data ();

void write_ccr (uint8_t val);

void motor (bool on);

void floppy_check_int (uint32_t* st0, uint32_t* cyl);
void floppy_drive_data (uint32_t stepr, uint32_t loadt, uint32_t unloadt, bool dma);
int floppy_calibrate (uint32_t drive);
void disable_controller ();

void enable_controller ();

int floppy_seek (uint32_t cyl, uint32_t head);

void floppy_reset ();

void floppy_read_sector_imp (uint8_t head, uint8_t track, uint8_t sector);
void floppy_write_sector_imp (uint8_t head, uint8_t track, uint8_t sector);
void lba2chs (int lba, int *head, int *track, int *sector);

void floppy_write_sector (int sectorLBA, uint8_t* data);
uint8_t* floppy_read_sector (int sectorLBA);

void floppy_getready ();

void setupFloppy ();

void sector_disk_write (uint8_t* buf, uint32_t sector_number);

void sector_disk_read (uint8_t* buff, uint32_t sector_number);

void byte_disk_read (uint8_t* buff, uint32_t sector_number, uint32_t offset, uint32_t nobytes);