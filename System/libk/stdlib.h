//
//  stdlib.h
//  Banana libc
//
//  Created by Alex Boxall on 11/7/18.
//  Copyright © 2018 Alex Boxall. All rights reserved.
//

#ifndef stdlib_h
#define stdlib_h

#if defined(__cplusplus)
#define restrict			//fixes some 'fun' errors
#define _Noreturn			//fixes some 'fun' errors
extern "C" {
#endif
    
	#include <stddef.h>
	
#ifdef _libk
	extern void* malloc (size_t size);
	extern void free (void* ptr);
#endif
	
#ifndef _libk
    
#define __i_MAX_ATEXIT_HANDLERS 256
#define MB_CUR_MAX 1            //current maximum multibyte character length, cannot
                                        //be greater than MB_LEN_MAX as defined in limits.h

    _Noreturn void abort(void);
    _Noreturn void exit(int status);
    _Noreturn void quick_exit(int status);
    _Noreturn void _Exit(int status);
    
    int atexit(void (*func)(void));
    int at_quick_exit(void (*func)(void));
    
    int system(const char *string);
    char *getenv(const char *name);
    
    void* malloc (size_t size);
    void *calloc(size_t nmemb, size_t size);
    void free (void *blk);
	void* realloc (void *ptr, size_t size);
    
#endif //!libk
    
#undef NULL
#define NULL 0
#define RAND_MAX 0x7FFFFFFF
#define EXIT_FAILURE 1
#define EXIT_SUCCESS 0
    
    typedef struct div_t {
        int quot;
        int rem;
        
    } div_t;
    
    typedef struct ldiv_t {
        long int quot;
        long int rem;
        
    } ldiv_t;
    
    typedef struct lldiv_t {
        long long int quot;
        long long int rem;
        
    } lldiv_t;
    
    div_t div(int numer, int denom);
    ldiv_t ldiv(long int numer, long int denom);
    lldiv_t lldiv(long long int numer, long long int denom);
    
    double atof(const char *nptr);
    int atoi(const char *nptr);
    long int atol(const char *nptr);
    long long int atoll(const char *nptr);
    
    double atof(const char *nptr);
    
    int atoi(const char *nptr);
    long int atol(const char *nptr);
    long long int atoll(const char *nptr);
    
    double strtod(const char * restrict nptr, char ** restrict endptr);
    float strtof(const char * restrict nptr, char ** restrict endptr);
    long double strtold(const char * restrict nptr, char ** restrict endptr);
    
    long int strtol(const char * restrict nptr, char ** restrict endptr, int base);
    long long int strtoll(const char * restrict nptr, char ** restrict endptr, int base);
    unsigned long int strtoul(const char * restrict nptr, char ** restrict endptr, int base);
    unsigned long long int strtoull(const char * restrict nptr, char ** restrict endptr, int base);
    
    int rand(void);
    void srand(unsigned int seed);

	int abs (int j);
	long int labs (long int j);
	long long int llabs (long long int j);

	void qsort (void* bss, size_t n, size_t size, int (*cmp)(const void*, const void*));
	void *bsearch (const void *key, const void *base, size_t nmemb, size_t size, \
				   int (*compar)(const void *, const void *));		//not implemented

#if defined(__cplusplus)
}
#endif

#endif /* stdlib_h */
