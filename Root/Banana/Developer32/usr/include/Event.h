//
//  Event.hpp
//  Banana API (New)
//
//  Created by Alex Boxall on 18/5/18.
//  Copyright © 2018 Alex Boxall. All rights reserved.
//

#ifndef Event_hpp
#define Event_hpp

#if defined(__cplusplus)
extern "C" {
#endif

    //the typedef's + structs should be updated in
    //#include "../../System/core/syscalldef.h"
    //then actually include that file
    
#include <stdint.h>
#include <stdbool.h>
//#include "../../System/core/syscalldef.h"


/*! \file Event.h
 \brief Definitions for event related structures and enumerations. The same file is used in the kernel and in the GUI library.
 */

/*
 
 EVENT HANDLING:
 
 KERNEL (SENDS BASIC EVENT DATA, e.g. KEY PRESSED, MOVE MOVED/CLICKED/RELEASED)
 |
 v
 APPLICATION (PASSES IT ON TO ALL WINDOWS)
 |
 |_______ WINDOW 1
 |
 |_______ WINDOW 2 (CAN CREATE NEW EVENTS (e.g. window resized), SENDS ALL EVENTS TO WIDGETS)
 |
 |________ WIDGET 1
 |
 |________ WIDGET 2
 |
 |________ WIDGET 3 (INTERPRETS EVENTS, WORKS OUT IF THEY ARE NEEDED (such as checking the mouse is in range) THEN IT
 |        CREATES ANY NEW EVENTS, THEN CALLS ALL NEEDED HANDLERS. WIDGETS CAN ALSO HAVE THEIR OWN SPECIFIC
 |     EVENTS, SUCH AS 'Scroll' FOR A SCROLLBAR, OR 'ControlAdded' FOR A FRAME
 |
 |____ this->MouseDown()
 |
 |____ this->WindowResize()
 |
 |____ this->DragDropEnter()
 
 */


/*!
 \enum EventType
 \brief The type of events that happened
 */
typedef enum EventType
{
    EventMouseClick,            ///< happens once when the user pushes the mouse button down
    EventMouseDown,             ///< happens while the mouse is down
    EventMouseUp,               ///< happens when the mouse is released
    EventMouseMove,             ///< mouse moved
    EventMouseDrag,             ///< mouse dragged
    EventMouseEnter,            ///< happens when the mouse enters the widget
    EventMouseLeave,            ///< happens when the mouse leaves the widget
    EventMouseHover,            ///< happens when the mouse stays still in the widget for a few seconds
    
    EventMove,                  ///< widget is moved
    EventResize,                ///< widget is resized
    EventWindowChanged,         ///< parent window is resized or moved
    
    EventKeyPress,              ///< happens once, when the user pushes a key down
    EventKeyDown,               ///< happens while a key is down
    EventKeyUp,                 ///< happens when a key is released
    EventKeyPressEarly,         ///< similar to KeyPress, but it happens BEFORE the widget does it's usual thing
    
    EventEnter,                 ///< focus is gained
    EventLeave,                 ///< focus is lost
    
    EventHelpRequested,         ///< user requests this help for this control
    
    EventDragDrop,              ///< drag+drop finished
    EventDragEnter,             ///< mouse hovering over widget while drag+drop in progress
    EventDragLeave,             ///< mouse moved away from hovering over widget while drag+drop in progress
    
    EventScrollWheel,           ///< when the scrollwheel 'ticks'
    
    EventSpecific,              ///< used for identifying other widget-specific events
    EventNull,                  ///< never sent by the system, used internally through the API
    
    _EventLast                  ///< used internally to measure the number of events
    
} EventType;

/*!
 \enum ScrollWheelDirection
 \brief The direction the scrollwhell is moved when an event happens
 */
typedef enum ScrollWheelDirection
{
    ScrollWheelNoChange,        //no scrollwheel movement
    ScrollWheelUp,
    ScrollWheelDown,
    ScrollWheelLeft,
    ScrollWheelRight
    
} ScrollWheelDirection;

/*!
 \enum MouseButtonType
 \brief The type of button pressed when an event happens
 */
typedef enum MouseButtonType
{
    MouseButtonLeft,
    MouseButtonMiddle,
    MouseButtonRight,
    MouseButtonOther
    
} MouseButtonType;

/*!
 \enum DragDropDataTypes
 \brief The types of data stored by a drag-and-drop event
 */
typedef enum DragDropDataTypes
{
    DragDropDataText,
    DragDropDataBitmap,
    DragDropDataFile,
    DragDropDataNullData        //nothing - used for invalid/null drag-and-drop event data
    
} DragDropDataTypes;

/*!
 \struct DragDropData
 \brief Stores data about a drag and drop event
 */
typedef struct DragDropData
{
    DragDropDataTypes type;
    uint32_t length;
    uint8_t* data;
    
} DragDropData;

/*!
 \struct Event
 \brief Stores information about an event, such as a mouse click or key press
 */
typedef struct Event
{
    bool valid;
    
    EventType type;
    
    uint32_t mouseXPosition;            ///< mouse X position relative to the top left corner of the screen
    uint32_t mouseYPosition;            ///< mouse X position relative to the top left corner of the screen
    
    uint8_t keyPressed;                 ///< the key that was pressed if it was a keyboard related event
    
    ScrollWheelDirection scrollWheelDirection;
    MouseButtonType mouseType;
    
    bool specificWindow;                ///< 0 = applies to everyone, 1 = applies to 'windowID' window
    int32_t windowID;                   ///< the window that need to receive the event
    
    /*!
     \struct specific
     \brief Specific flags for certain event types
     */
    struct specific {
        //EventWindowChanged
        int oldWindowX;
        int oldWindowY;
        int oldWindowWidth;
        int oldWindowHeight;
        
        //...
        
    } specific;
    
    DragDropData dragDropData;
    
} Event;
    
#if defined(__cplusplus)
}
#endif

#endif /* Event_hpp */
