#pragma once

extern "C" {
	#include <core/objectmgr.h>		//PLATFORM_ID + the standard numerical data types + extern "C" definitions
}

typedef uint64_t InternalObjectID;

struct objectLinkedListNode;

class Object
{
private:
	static InternalObjectID nextObjectID;
	static Object* allObjects[4096];

protected:
	InternalObjectID id;
	struct objectLinkedListNode* threadsWhichCanUse;

public:
	Object ();

	InternalObjectID getID ();
	bool usableByThread (InternalObjectID thread);

	static Object* getObject (InternalObjectID id);
};

//the kernel is the only thing allowed to touch the actual object structure
//but interfaces will exist to extract the data from it, as well as perform
//operations on it. e.g. an object of type 'Device' might call a device driver
//when it it is read from by the system, or an object of type 'File' might
//call a vfs_ function when read from, etc.
//a 'Process' object might have ioctl to delete it, get PID, fork it, etc.

typedef struct objectLinkedListNode
{
	struct objectLinkedListNode* next;
	Object* object;

} objectLinkedListNode;

void objectManagerSetup ();

class FileProtocolRead : public Object
{
public:
	virtual int write(char* data, size_t size);
	virtual int read (char* buffer, size_t size);
};

struct InternalPipeBit
{
public:
	static const int BUFFER_SIZE = 1024;

	char verify[256];
	uint32_t vfy2;

	uint32_t ptr;
	char buffer[BUFFER_SIZE];					//this is the part the application uses
};

class PipeRead: public FileProtocolRead
{
protected:
	int processThisBelongsTo;

public:
	InternalPipeBit* pipe;
	virtual int read(char* buffer, size_t size);
};

class PipeWrite: public FileProtocolRead
{
protected:
	int processThisBelongsTo;

public:
	InternalPipeBit* pipe;
	virtual int write(char* buffer, size_t size);
};

class Stdin : public FileProtocolRead
{
protected:
	int processThisBelongsTo;

public:
	static const int BUFFER_SIZE = 1024;

	uint32_t ptr;
	char buffer[BUFFER_SIZE];					//this is the part the application uses
	char internalBuffer[BUFFER_SIZE];			//kernel fills this, chucks it across at a newline
	Stdin ();

	virtual int read (char* buffer, size_t size);
};

class FileObject : public FileProtocolRead
{
protected:
	char* matchingFilename;
	bool eof;

public:
	bool error;
	vfs_file_t file;

	FileObject (char* filename, uint8_t mode);

	virtual int write (char* data, size_t size);
	virtual int read (char* buffer, size_t size);
	virtual int close();
};

class DirObject: public FileProtocolRead
{
protected:
	char* matchingFilename;
	bool eof;

public:
	bool error;
	vfs_dir_t file;

	DirObject (char* filename);

	virtual int read (char* buffer, size_t size);
	int close ();
};