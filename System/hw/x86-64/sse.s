bits 64

global sseAvaliable
global sseOpen
global sseRead
global sseWrite

sseAvaliable:
	mov rax, 1
	ret
	
extern x87Open
sseOpen:
	;now enable SSE and the like
	call x87Open
	mov rax, cr4
	or ax, 3 << 9		;set CR4.OSFXSR and CR4.OSXMMEXCPT at the same time
	mov cr4, rax
	ret
	
sseRead:
	fxsave [rdi]
	ret
	
sseWrite:
	fxrstor [rdi]
	ret