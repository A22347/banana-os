#pragma once

#include <core/kernel.h>
#include <hal/clock.h>

uint64_t rtcReadTimeUTC ();