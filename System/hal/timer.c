#include <hal/timer.h>
#include <gui/guilink.h>

uint64_t toticks;
int64_t alarms[MAX_ALARMS];
alarmHandler alarmHandlers[MAX_ALARMS];
uint64_t totalMillisecs = 0;

void scrollBootScreen ();

void timer_handler (struct regs *r)
{
	static int ticksThisSecond = 0;

	//we only need to do these things for cpu 0. other things should be put after this if-statement
	if (getCPUNumber () == 0) {
		++ticksThisSecond;
		++toticks;

		totalMillisecs += 1000 / TIMER_HERTZ;

		//stuff that happens every second
		if (ticksThisSecond >= TIMER_HERTZ) {
			ticksThisSecond = 0;
			++lastInt;
			guiReactToSecondPassed ();
		}

		//reset the fault handler counter
		numberOfSameType = 0;

		//sound the alarms! (if needed)
		for (alarmID i = 0; i < MAX_ALARMS; ++i) {
			if (alarmHandlers[i]) {
				alarms[i] -= 1000 / TIMER_HERTZ;
				if (alarms[i] < 0) {
					logk ("ALARM %d EXPIRED!\n", i);
					alarmHandlers[i] (i);
					alarmHandlers[i] = 0;
				}
			}
		}

		//update GUI
		guiReactToTimerTick ();
	}
}

void setupTimers (unsigned hertz, unsigned intNo)
{
	if (hasAPIC) {
		setupAPICTimer (TIMER_HERTZ, intNo);
	} else {
		setupPIT (TIMER_HERTZ);
	}

	//get alarm stuff ready
	memset (alarms, 0, sizeof (alarms));
	memset (alarmHandlers, 0, sizeof (alarmHandlers));
}

alarmID setupAlarm (int64_t milliseconds, alarmHandler handler)
{
	logk ("CREATING AN ALARM (%d MILLISECS)\n", milliseconds);
	logk ("HANDLER = 0x%X\n", handler);

	if (milliseconds < 0) {
		return MAX_ALARMS + 1;
	}

	//find a free alarm spot
	for (alarmID i = 0; i < MAX_ALARMS; ++i) {
		if (alarmHandlers[i] == 0) {
			alarmHandlers[i] = handler;
			alarms[i] = milliseconds;
			logk ("SPOT %d FOUND\n", i);
			return i;
		}
	}

	logk ("NO SPOT FOUND\n");
	return MAX_ALARMS + 1;
}