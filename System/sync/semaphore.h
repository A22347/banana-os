#pragma once

#include <sync/spinlock.h>

typedef struct semaphore_t {
    int value;
    spinlock_t spinlock;
    
} semaphore_t;

void semaphoreInit(semaphore_t* sema, int value);
void semaphoreWait(semaphore_t* sema);
void semaphoreSignal(semaphore_t* sema);
