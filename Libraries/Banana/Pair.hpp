#ifndef Pair_hpp
#define Pair_hpp

#include "Object.hpp"

/*! \deprecated*/
class Pair : public Object {
public:
    Pair(int a = 0, int b = 0);
    ~Pair();
    int first = 0;
    int second = 0;
    
};

#endif /* Pair_hpp */