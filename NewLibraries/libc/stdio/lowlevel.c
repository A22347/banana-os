//
//  basicfileop.c
//  Banana libc
//
//  Created by Alex Boxall on 12/7/18.
//  Copyright © 2018 Alex Boxall. All rights reserved.
//

#ifndef _libk

#include "stdio.h"
#include "string.h"
#include "syscall.h"
#include <stdlib.h>     //free (the macOS one for now)

//this file got complicated fast...

int __i_internalActualFileWrite (unsigned long long objectId, size_t len, const unsigned char* data) {
    return __i_system_call_writefileorpipe(objectId, data, len);
}

int __i_internalActualFileRead (unsigned long long objectId, size_t len, unsigned char* data) {
	//what this is supposed to do:
	//	- read EXACTLY len bytes or EOF
	//    (forget about fgets stopping before it's length arg, that calls this on single byte chunks

	// this means:
	// stdin:
	//     - will always read 'len' bytes, because the system will pause the task in order to wait for more input
	// other:
	//     - will try to read EXACTLY 'len' bytes or fail with EOF. no 'half-way' read operations

	// anyway, that is for the system to take care of. this message really doesn't need to be here,
	// so it will be repeated in the system

	return __i_system_call_readfileorpipe (objectId, data, len);
}

int __i_internalFileBufferFlushWrite(FILE* fil) {
    int ret = __i_internalActualFileWrite(fil->objectID, fil->bufferPtr, fil->buffer);
    memset(fil->buffer, 0, fil->bufferSizeForThisFile);
    fil->bufferPtr = 0;
    return ret;
}

int fullBufferWrite(FILE* fil, size_t bytes, const unsigned char* data) {
    int ret = EOF;
    
    //there has to be a faster way...
    for (size_t i = 0; i < bytes; ++i) {
        fil->buffer[fil->bufferPtr++] = data[i];
        if (fil->bufferPtr >= fil->bufferSizeForThisFile) {
            ret = __i_internalFileBufferFlushWrite(fil);
            if (ret) {
                fil->errorIndicator = 1;
                return EOF;
            }
        }
    }
    
    return ret;
}

int lineBufferWrite(FILE* fil, size_t bytes, const unsigned char* data) {
    int ret = EOF;
    
    //there has to be a faster way...
    for (size_t i = 0; i < bytes; ++i) {
        fil->buffer[fil->bufferPtr++] = data[i];
        if (fil->bufferPtr >= fil->bufferSizeForThisFile || data[i] == '\n') {
            ret = __i_internalFileBufferFlushWrite(fil);
            if (ret) {
                fil->errorIndicator = 1;
                return EOF;
            }
        }
    }
    
    return ret;
}

int __i_internalFileWrite (FILE* fil, size_t bytes, const unsigned char* data) {
    while (fil->locked);
    fil->locked = 1;
    
    if (fil->filemode & __i_FILE_READ) {
        fil->locked = 0;
        return EOF;
    }
    
    fil->lastOperation = __i_LASTOPERATION_WRITE;
    
    int ret = EOF;
    
    if (fil->bufferMode == _IONBF) {
        ret = __i_internalActualFileWrite (fil->objectID, bytes, data);
    } else if (fil->bufferMode == _IOFBF) {
        ret = fullBufferWrite(fil, bytes, data);
    } else if (fil->bufferMode == _IOLBF) {
        ret = lineBufferWrite(fil, bytes, data);
    } else {
        ret = EOF;
        fil->errorIndicator = 1;
    }
    
    fil->locked = 0;
	return ret;
}

int __i_internalFileRead (FILE* fil, size_t bytes, unsigned char* data) {
    while (fil->locked);
    fil->locked = 1;
    
    if (fil->filemode & __i_FILE_WRITE) {
        fil->locked = 0;
        return EOF;
    }
    
    fil->lastOperation = __i_LASTOPERATION_READ;

    int ret = EOF;
    
	if (fil->bufferMode == _IOFBF) {
		// ...
	} else {
		ret = __i_internalActualFileRead (fil->objectID, bytes, data);
	}
    
    fil->locked = 0;
    return ret;
}

#endif
