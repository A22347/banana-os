#include <gui/terminal.h>
#include <libk/ctype.h>
#include <hal/display.h>
#include <gui/fonts.h>
#include <hal/mouse.h>

int terminal_row = 0;
int terminal_column = 0;

void terminal_flush ()
{
}

void putpixelr (uint32_t semispot, uint32_t col)
{
	if (/*sleepMode || */semispot > ONE_LINE * TOTAL_HEIGHT) {
		return;
	}
	displaySetPixel (semispot, col, 0);
}

void putpixel (uint32_t semispot, uint32_t col)
{
	putpixelr(semispot, col);
}

void terminal_putentryat_pixel (char c, uint32_t fg, uint32_t bg, uint16_t x, uint16_t y)
{
	uint32_t index = y * ONE_LINE + x;

	for (uint32_t inc2 = 0; inc2 < CELLH; ++inc2) {
		uint32_t fontc3ll = Fonts[0][(int) c][inc2];

		for (uint32_t inc = 0; inc < CELLW; ++inc) {
			if (fontc3ll & (1 << (inc))) {
				putpixel (index + inc, fg);
			} else {
				putpixel(index + inc, bg);
			}
		}
		index += ONE_LINE;
	}
}

void terminal_putentryat_pixel_raw (char c, uint32_t fg, uint32_t bg, uint16_t x, uint16_t y)
{
	uint32_t index = y * ONE_LINE + x;

	for (uint32_t inc2 = 0; inc2 < CELLH; ++inc2) {
		uint32_t fontc3ll = Fonts[0][(int) c][inc2];

		for (uint32_t inc = 0; inc < CELLW; ++inc) {
			if (fontc3ll & (1 << (inc))) {
				putpixelr (index + inc, fg);
			} else {
				putpixelr(index + inc, bg);
			}
		}
		index += ONE_LINE;
	}
}

void terminal_putentryat (char c, uint32_t fg, uint32_t bg, uint8_t x, uint8_t y)
{
	terminal_putentryat_pixel (c, fg, bg, (uint16_t) x * CELLW, (uint16_t) y * CELLH);
}

void terminal_quickdraw (char c, uint32_t fg, uint32_t bg, uint16_t x, uint16_t y)
{
	uint32_t index = y * ONE_LINE + x;

	for (uint32_t inc2 = 0; inc2 < CELLH; ++inc2) {
		uint32_t fontc3ll = Fonts[0][(int) c][inc2];

		for (uint32_t inc = 0; inc < CELLW; ++inc) {
			if (fontc3ll & (1 << (inc))) {
				putpixel (index + inc, fg);
				putpixelr (index + inc, fg);
			} else {
				putpixel(index + inc, bg);
				putpixelr(index + inc, bg);
			}
		}
		index += ONE_LINE;
	}
}

int ct = 0;

void terminal_putchar_colour (char c, uint32_t fg, uint32_t bg)
{
	if (c == '\n') {
		++terminal_row;
		terminal_column = 0;
		return;
	}
	if (c < ' ') {
		return;
	}
	terminal_putentryat (c, fg, bg, terminal_column, terminal_row);
	if (++terminal_column >= 100) {
		terminal_column = 0;
		++terminal_row;
	}
}

void terminal_writestring_colour (const char* data, uint32_t bg, uint32_t fg)
{
	size_t datalen = strlen (data);
	for (size_t i = 0; i < datalen; i++) {
		terminal_putchar_colour (data[i], fg, bg);
	}
}
