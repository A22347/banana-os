org 0
bits 16

;mov eax, [_bpb.logicalSectorsPerFAT]
;mov ebx, 0
;mov bl, [_bpb.totalFATs]
;mul ebx
;add ax, [_bpb.clusterOfRootDir]
;add ax, [_bpb.reservedSectors]	;[fs.rootDirSector]
;sub ax, 2
;mov [fs.rootDirSector], ax

jmp _main
;jmp short _main
;nop														; We need to fill up 3 bytes as part of the BPB

_bpb:
	_bpb.oem						db "BANANAOS"		; OEM name or version
	
	;RERFERENCE ONLY! OVERWRITTEN BY JOIN.PY
	_bpb.bytesPerSector				dw 0x0200			; Bytes per Sector (512)
	_bpb.sectorsPerCluster			db 0x01				; Sectors per cluster (usually 1)
	_bpb.reservedSectors			dw 0x187E			; Reserved sectors
	_bpb.totalFATs					db 0x0002			; FAT copies
	_bpb.rootEntries				dw 0x0			; Root directory entries
	_bpb.fat12.totalSectors			dw 0			; Sectors in filesystem (0 for FAT16)
	_bpb.mediaDescriptor			db 0xf8				; (should be 0xF8 for a HDD)		Media descriptor type (f0 for floppy or f8 for HDD)
	_bpb.sectorsPerFAT				dw 0				;0x0009			; Sectors per FAT

	_bpb.sectorsPerTrack			dw 0x3F				; Sectors per track
	_bpb.headsPerCylinder			dw 0x20			; Heads per cylinder
	_bpb.hiddenSectors				dd 0x00000001		; Number of hidden sectors (0)
	_bpb.totalSectors				dd 0x20000				; Number of sectors in the filesystem

	;FAT32 EBPB
	_bpb.logicalSectorsPerFAT		dd 0x3C1
	_bpb.driveDescription			dw 0
	_bpb.version					dw 0
	_bpb.clusterOfRootDir			dd 2
	_bpb.fsInfoSectorSector			dw 1
	_bpb.sectorOfBootsectorCopy		dw 6
	times 12 db 0
	
	;times 7 db 0
	;times 11 db 0
	;times 8 db 0

	_bpb.driveNumber				db 0x80				; Sectors per FAT
	_bpb.currentHead				db 0x00				; Reserved (used to be current head)
	_bpb.signature					db 0x29				; Extended signature (indicates we have serial, label, and type)
	_bpb.serial						dd 0xb3771b01		; Serial number of partition
	_bpb.filename					db "VOLUMELABEL"	; Volume label
	_bpb.fileSystem					db "FAT32   "  ;	; Filesystem type

_data:
	fs.driveNumber db 0
	
filename				db "BOOTLOADSYS"

rootEntry dw 0

align 8
DAPACK:
	db	0x10
	db	0
blkcnt:	dw	1		; int 13 resets this to # of blocks actually read/written
db_add:	dw	0x200		; memory buffer destination address (0:7c00)
db_seg:	dw	0		; in memory page zero
d_lba:	dd	0		; put the lba to read in this spot
	dd	0		; more storage bytes only for big lbas ( > 4 bytes )
	dd 0

_main:
	; Set up our segment registers and stack

	cli													; We don't want our interrupts ATM
	mov ax, 0x07c0										; We're at 0000:7c000, so set our segment registers to that
	mov ds, ax
	mov es, ax
	mov fs, ax
	mov gs, ax

	xor ax, ax											; Set our stack to 0x0000 and create it
	mov ss, ax
	xor sp, sp			;it'll loop round!

	mov [fs.driveNumber], dl
	
	mov ax, 0x12
	int 0x10

	;mov edi, 0xA0000				  ; 
    ;mov ecx, 0x20000                  ; 0xA0000 -> 0xBFFFF
    ;xor ax, ax				     	  ; Set the value to set the screen to: Blue background, white foreground, blank spaces.
    ;rep stosb                         ; Clear the entire screen. 

	LOAD_ROOT:
     
     ; compute size of root directory and store in "cx"
     
         ; xor     cx, cx
         ; xor     dx, dx
          ;mov     ax, 0x0020                           ; 32 byte directory entry
          ;mul     WORD [_bpb.rootEntries]                ; total size of directory
         ; div     WORD [_bpb.bytesPerSector]             ; sectors used by directory
          ;xchg    ax, cx

		mov cx, 1			;root sector isn't continuous in FAT32, and I can't be bothered to use the FAT
          
     ; compute location of root directory and store in "ax"
     
        mov eax, [_bpb.logicalSectorsPerFAT]
		mov ebx, 0
		mov bl, [_bpb.totalFATs]
		mul ebx
		add ax, [_bpb.clusterOfRootDir]
		add ax, [_bpb.reservedSectors]	;[fs.rootDirSector]
		sub ax, 2

		mov [rootEntry], ax
          
     ; read root directory into memory (7C00:0200)
 

          mov     bx, 0x0200                            ; copy root dir above bootcode
          call    ReadSectors
		    
	
     ;----------------------------------------------------
     ; Find stage 2
     ;----------------------------------------------------

     ; browse root directory for binary image
          mov     cx, WORD [_bpb.rootEntries]             ; load loop counter
          mov     di, 0x0200                            ; locate first root entry
     .LOOP:
          push    cx
          mov     cx, 0x000B                            ; eleven character name
          mov     si, filename                         ; image name to find
          push    di
     rep  cmpsb                                         ; test for entry match
          pop     di
          je      LOAD_FAT
          pop     cx
          add     di, 0x0020                            ; queue next directory entry
          loop    .LOOP
          jmp     FAILURE

     ;----------------------------------------------------
     ; Load FAT
     ;----------------------------------------------------

     LOAD_FAT:
     
     ; save starting cluster of boot image
		xor edx, edx
          add     dx, WORD [di + 0x001A]
		  add dx, [rootEntry]
		  sub dx, 2
          mov     DWORD [cluster], edx                  ; file's first cluster
          
     ; compute size of FAT and store in "cx"
     
          xor     ax, ax
          mov     al, BYTE [_bpb.totalFATs]          ; number of FATs
          mul     WORD [_bpb.sectorsPerFAT]             ; sectors used by FATs
          mov     cx, ax

     ; compute location of FAT and store in "ax"

          mov     ax, WORD [_bpb.reservedSectors]       ; adjust for bootsector
          
     ; read FAT into memory (7C00:0200)

          mov     bx, 0x0200                          ; copy FAT above bootcode
		  mov cx, 64;;
		  ;jmp $
          call    ReadSectors

     ; read image file into memory (0050:0000)
     
          mov     ax, 0x0050
          mov     es, ax                              ; destination for image
          mov     bx, 0x0000                          ; destination for image
          push    bx

     ;----------------------------------------------------
     ; Load Stage 2
     ;----------------------------------------------------

     LOAD_IMAGE:
     
          mov     eax, DWORD [cluster]                  ; cluster to read
          pop     bx                                  ; buffer to read into
          xor     cx, cx
          mov     cl, BYTE [_bpb.sectorsPerCluster]     ; sectors to read
		  mov		cl, 20;;;;;
          call    ReadSectors
		  ;cli
		  ;hlt
		  mov dl, [fs.driveNumber]


		  push word 0x0;;;;;
		  push word 0x500;;;;;
		  retf

	FAILURE:
		mov ax, 0x9999
		mov bx, 0xFFFF
		jmp short $
			
cluster     dd 0x0

Print:
ret
	;pusha
	;Print.loop:
	;	lodsb									; Load the next character from SI
	;	
	;	cmp al, 0								; If the character is null,
	;	je short Print.end							; then we're done
	;	
	;	mov ah, 0eh								; Use the 'print character' function
	;	int 10h									; Call the 'video services' interrupt
	;	jmp short Print.loop							; Print the next character as well
;
	;Print.end:
	;	popa
	;	ret


; Read Sectors - Reads sectors from disk into memory
; Parameters:
;	CX - Number of sectors to read
;	AX - Starting sector to read
;	ES:BX - Address to read to
; Returns:
;	[ES:BX] - Data from disk
ReadSectors:
	mov di, 0x0005								; How many times should we retry the read?
	
	ReadSectors.loop:
		; DEBUG
		push ax
		;mov ah, 0eh
		;mov al, '-'
		;int 10h
		pop ax
		
		push ax
		push bx
		push cx
		
		push si
		
		mov si, ax
		xor eax, eax
		mov ax, si
		add eax, [_bpb.hiddenSectors]
		
		mov word [blkcnt], cx
		mov word [db_add], bx
		mov dword [d_lba], eax
		mov word [db_seg], es

		mov ah, 0x42								; Set the interrupt to the 'read sector' function
		mov si, DAPACK
		mov dl, byte[fs.driveNumber]			; The drive to read from
		cld

		int 0x13									; Call our 'disk IO' interrupt

		jnc ReadSectors.success					; If we successfully read the data, we don't have to try again
		jc $
		
		mov ah, 00h								; Set the interrupt to the 'reset disk' function
		int 13h									; Call our 'disk IO' interrupt
		pushf
		dec di									; Decrement our error counter
		pop si
		pop cx
		pop bx
		pop ax
		popf

		jnz short ReadSectors.loop					; Try again if we've failed
		jmp short ReadSectors.fail					; RED ALERT
	
	ReadSectors.success:
		; DEBUG
		;push ax
		;mov ah, 0eh
		;mov al, '_'
		;int 10h
		;pop ax
		
		pop si
		pop cx
		pop bx
		pop ax
		
		add bx, word[_bpb.bytesPerSector]		; Go to the next memory location
		inc ax									; Read from the next sector
		loop ReadSectors
		
		ret
	ReadSectors.fail:
		; DEBUG
		mov ah, 0eh
		mov al, 'R'
		int 10h
		jmp $

;times 0x1be - ($-$$) db 0
db 0x80
db 0
db 1
db 0
db 4
db 0
db 2
db 0
dd 1
dd 1

times 510 - ($-$$) db 0
dw 0xAA55