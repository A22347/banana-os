#pragma once

#include <stdint.h>
#include <devss/devices.h>
#include <hw/acpi.h>

void openPCI (Device* self, int a, int b, void* c);

//merge this with devss in the future
typedef struct PCIDeviceInfo
{
	uint8_t classCode;
	uint8_t subClass;
	uint16_t vendorID;

	uint8_t bus;
	uint8_t slot;
	uint8_t function;
	uint8_t progIF;

	uint32_t bar[6];

	uint8_t interrrupt;

} PCIDeviceInfo;


extern char PCINames[16][256][9];

void getPCIDataReady ();
uint16_t pciReadWord (uint8_t bus, uint8_t slot, uint8_t func, uint8_t offset);
uint16_t getVendorID (uint8_t bus, uint8_t slot, uint8_t function);
uint16_t getHeaderType (uint8_t bus, uint8_t slot, uint8_t function);
uint16_t getClassCode (uint8_t bus, uint8_t slot, uint8_t function);
uint8_t getProgIF (uint8_t bus, uint8_t slot, uint8_t function);
uint8_t getRevisionID (uint8_t bus, uint8_t slot, uint8_t function);
uint8_t getInterruptNumber (uint8_t bus, uint8_t slot, uint8_t function);
uint32_t getBARAddress (uint8_t barNo, uint8_t bus, uint8_t slot, uint8_t function);
void checkDevice (Device* self, uint8_t bus, uint8_t device);

void initPCI ();
void scanPCI (Device* self);