//
//  printf.c
//  Banana libc
//
//  Created by Alex Boxall on 17/7/18.
//  Copyright © 2018 Alex Boxall. All rights reserved.
//

#include "stdio.h"
#include "string.h"        //strcpy, strcat, memcpy, memset
#include "ctype.h"         //isdigit
#include "stdlib.h"		   //for malloc in vasprintf
#include <stdbool.h>
#include <stdint.h>
#include <stdarg.h>

char* __i__int_str(intmax_t i, char b[], int base, bool plusSignIfNeeded, bool spaceSignIfNeeded,
                int paddingNo, bool justify, bool zeroPad) {
    
    if (justify && zeroPad) {
        zeroPad = false;
    }
    
    char digit[32] = {0};
    memset(digit, 0, 32);
    strcpy(digit, "0123456789");
    
    if (base == 16) {
        strcat(digit, "ABCDEF");
    } else if (base == 17) {
        strcat(digit, "abcdef");
        base = 16;
    }
    
    char* p = b;
    if (i < 0) {
        *p++ = '-';
        i *= -1;
    } else if (plusSignIfNeeded) {
        *p++ = '+';
    } else if (!plusSignIfNeeded && spaceSignIfNeeded) {
        *p++ = ' ';
    }
    
    intmax_t shifter = i;
    do {
        ++p;
        shifter = shifter / base;
    } while (shifter);
    
    *p = '\0';
    do {
        *--p = digit[i % base];
        i = i / base;
    } while (i);
    
    int padding = paddingNo - (int)strlen(b);
    if (padding < 0) padding = 0;
    
    if (justify) {
        while (padding--) {
            if (zeroPad) {
                b[strlen(b)] = '0';
            } else {
                b[strlen(b)] = ' ';
            }
        }
        
    } else {
        char a[256] = {0};
        while (padding--) {
            if (zeroPad) {
                a[strlen(a)] = '0';
            } else {
                a[strlen(a)] = ' ';
            }
        }
        strcat(a, b);
        strcpy(b, a);
    }
    
    return b;
}

void __i_displayCharacter(char c, int* a, FILE* restrict stream, char* buffer, int size) {
    if (buffer) {
        if (size) {
            if (*a < size - 1) {
                buffer[*a] = c;
            } else if (*a == size - 1) {
                buffer[*a] = 0;
            }
        } else {
            buffer[*a] = c;
        }
    } else {
#ifndef _libk
        fputc(c, stream);
#endif
    }
    *a += 1;
}

void __i_displayString(char* c, int* a, FILE* restrict stream, char* buffer, int size) {
#ifndef _libk
	//fast mode.
	if (!buffer) {
		fputs(c, stream);
		*a = *a + strlen(c);
		return;
	}
#endif

    for (int i = 0; c[i]; ++i) {
        __i_displayCharacter(c[i], a, stream, buffer, size);
    }
}

int __i_internal_printf(const char * restrict format, va_list list, FILE* stream, char* buffer, int size)
{
    int chars        = 0;
    char intStrBuffer[256] = {0};

	char plainCharBuffer[1024] = { 0 };
	plainCharBuffer[0] = 0;
    
    for (int i = 0; format[i]; ++i) {
        
        char specifier   = '\0';
        char length      = '\0';
        
        int  lengthSpec  = 0;
        int  precSpec    = 0;
        bool leftJustify = false;
        bool zeroPad     = false;
        bool spaceNoSign = false;
        bool altForm     = false;
        bool plusSign    = false;
        bool emode       = false;
        int  expo        = 0;
        
        if (format[i] == '%') {
			if (strlen(plainCharBuffer)) {
				__i_displayString(plainCharBuffer, &chars, stream, buffer, size);
				plainCharBuffer[0] = 0;
			}
            ++i;

            bool extBreak = false;
            while (1) {
                
                switch (format[i]) {
                    case '-':
                        leftJustify = true;
                        ++i;
                        break;
                        
                    case '+':
                        plusSign = true;
                        ++i;
                        break;
                        
                    case '#':
                        altForm = true;
                        ++i;
                        break;
                        
                    case ' ':
                        spaceNoSign = true;
                        ++i;
                        break;
                        
                    case '0':
                        zeroPad = true;
                        ++i;
                        break;
                        
                    default:
                        extBreak = true;
                        break;
                }
                
                if (extBreak) break;
            }
            
            while (isdigit(format[i])) {
                lengthSpec *= 10;
                lengthSpec += format[i] - 48;
                ++i;
            }
            
            if (format[i] == '*') {
                lengthSpec = va_arg(list, int);
                if (lengthSpec < 0) {
                    lengthSpec = 0 - lengthSpec;
                    leftJustify = true;
                }
                ++i;
            }
            
            if (format[i] == '.') {
                ++i;
                while (isdigit(format[i])) {
                    precSpec *= 10;
                    precSpec += format[i] - 48;
                    ++i;
                }
                
                if (format[i] == '*') {
                    precSpec = va_arg(list, int);
                    if (precSpec < 0) {
                        precSpec = 6;       //defaut value
                    }
                    ++i;
                }
            } else {
                precSpec = 6;
            }

            if (format[i] == 'h' || format[i] == 'l' || format[i] == 'j' ||
                format[i] == 'z' || format[i] == 't' || format[i] == 'L') {
                length = format[i];
                ++i;
                
                if (format[i] == 'h') {
                    length = 'H';
                    ++i;
                } else if (format[i] == 'l') {
                    length = 'q';
                    ++i;
                }
            }
            specifier = format[i];
            
            memset(intStrBuffer, 0, 256);
            
            int base = 10;
            if (specifier == 'o') {
                base = 8;
                specifier = 'u';
                if (altForm) {
                    __i_displayString("0", &chars, stream, buffer, size);
                }
            }
            if (specifier == 'p') {
                base = 16;
                length = 'z';
                specifier = 'u';
            }

            switch (specifier) {
                case 'X':
                    base = 16;
					__attribute__ ((fallthrough));
                case 'x':
                    base = base == 10 ? 17 : base;
                    if (altForm) {
                        __i_displayString("0x", &chars, stream, buffer, size);
                    }
					__attribute__ ((fallthrough));
                case 'u':
                {
                    switch (length) {
                        case 0:
                        {
                            unsigned int integer = va_arg(list, unsigned int);
                            __i__int_str(integer, intStrBuffer, base, plusSign, spaceNoSign, lengthSpec, leftJustify, zeroPad);
                            __i_displayString(intStrBuffer, &chars, stream, buffer, size);
                            break;
                        }
                        case 'H':
                        {
                            unsigned char integer = (unsigned char) va_arg(list, unsigned int);
                            __i__int_str(integer, intStrBuffer, base, plusSign, spaceNoSign, lengthSpec, leftJustify, zeroPad);
                            __i_displayString(intStrBuffer, &chars, stream, buffer, size);
                            break;
                        }
                        case 'h':
                        {
                            unsigned short int integer = va_arg(list, unsigned int);
                            __i__int_str(integer, intStrBuffer, base, plusSign, spaceNoSign, lengthSpec, leftJustify, zeroPad);
                            __i_displayString(intStrBuffer, &chars, stream, buffer, size);
                            break;
                        }
                        case 'l':
                        {
                            unsigned long integer = va_arg(list, unsigned long);
                            __i__int_str(integer, intStrBuffer, base, plusSign, spaceNoSign, lengthSpec, leftJustify, zeroPad);
                            __i_displayString(intStrBuffer, &chars, stream, buffer, size);
                            break;
                        }
                        case 'q':
                        {
                            unsigned long long integer = va_arg(list, unsigned long long);
                            __i__int_str(integer, intStrBuffer, base, plusSign, spaceNoSign, lengthSpec, leftJustify, zeroPad);
                            __i_displayString(intStrBuffer, &chars, stream, buffer, size);
                            break;
                        }
                        case 'j':
                        {
                            uintmax_t integer = va_arg(list, uintmax_t);
                            __i__int_str(integer, intStrBuffer, base, plusSign, spaceNoSign, lengthSpec, leftJustify, zeroPad);
                            __i_displayString(intStrBuffer, &chars, stream, buffer, size);
                            break;
                        }
                        case 'z':
                        {
                            size_t integer = va_arg(list, size_t);
                            __i__int_str(integer, intStrBuffer, base, plusSign, spaceNoSign, lengthSpec, leftJustify, zeroPad);
                            __i_displayString(intStrBuffer, &chars, stream, buffer, size);
                            break;
                        }
                        case 't':
                        {
                            ptrdiff_t integer = va_arg(list, ptrdiff_t);
                            __i__int_str(integer, intStrBuffer, base, plusSign, spaceNoSign, lengthSpec, leftJustify, zeroPad);
                            __i_displayString(intStrBuffer, &chars, stream, buffer, size);
                            break;
                        }
                        default:
                            break;
                    }
                    break;
                }
                    
                case 'd':
                case 'i':
                {
                    switch (length) {
                        case 0:
                        {
                            int integer = va_arg(list, int);
                            __i__int_str(integer, intStrBuffer, base, plusSign, spaceNoSign, lengthSpec, leftJustify, zeroPad);
                            __i_displayString(intStrBuffer, &chars, stream, buffer, size);
                            break;
                        }
                        case 'H':
                        {
                            signed char integer = (signed char) va_arg(list, int);
                            __i__int_str(integer, intStrBuffer, base, plusSign, spaceNoSign, lengthSpec, leftJustify, zeroPad);
                            __i_displayString(intStrBuffer, &chars, stream, buffer, size);
                            break;
                        }
                        case 'h':
                        {
                            short int integer = va_arg(list, int);
                            __i__int_str(integer, intStrBuffer, base, plusSign, spaceNoSign, lengthSpec, leftJustify, zeroPad);
                            __i_displayString(intStrBuffer, &chars, stream, buffer, size);
                            break;
                        }
                        case 'l':
                        {
                            long integer = va_arg(list, long);
                            __i__int_str(integer, intStrBuffer, base, plusSign, spaceNoSign, lengthSpec, leftJustify, zeroPad);
                            __i_displayString(intStrBuffer, &chars, stream, buffer, size);
                            break;
                        }
                        case 'q':
                        {
                            long long integer = va_arg(list, long long);
                            __i__int_str(integer, intStrBuffer, base, plusSign, spaceNoSign, lengthSpec, leftJustify, zeroPad);
                            __i_displayString(intStrBuffer, &chars, stream, buffer, size);
                            break;
                        }
                        case 'j':
                        {
                            intmax_t integer = va_arg(list, intmax_t);
                            __i__int_str(integer, intStrBuffer, base, plusSign, spaceNoSign, lengthSpec, leftJustify, zeroPad);
                            __i_displayString(intStrBuffer, &chars, stream, buffer, size);
                            break;
                        }
                        case 'z':
                        {
                            size_t integer = va_arg(list, size_t);
                            __i__int_str(integer, intStrBuffer, base, plusSign, spaceNoSign, lengthSpec, leftJustify, zeroPad);
                            __i_displayString(intStrBuffer, &chars, stream, buffer, size);
                            break;
                        }
                        case 't':
                        {
                            ptrdiff_t integer = va_arg(list, ptrdiff_t);
                            __i__int_str(integer, intStrBuffer, base, plusSign, spaceNoSign, lengthSpec, leftJustify, zeroPad);
                            __i_displayString(intStrBuffer, &chars, stream, buffer, size);
                            break;
                        }
                        default:
                            break;
                    }
                    break;
                }
                    
                case 'c':
                {
                    if (length == 'l') {
                        //__i_displayCharacter(va_arg(list, wint_t), &chars, stream, buffer, size);
                    } else {
                        __i_displayCharacter(va_arg(list, int), &chars, stream, buffer, size);
                    }
                    
                    break;
                }
                    
                case 's':
                {
                    __i_displayString(va_arg(list, char*), &chars, stream, buffer, size);
                    break;
                }
                    
                case 'n':
                {
                    switch (length) {
                        case 'H':
                            *(va_arg(list, signed char*)) = chars;
                            break;
                        case 'h':
                            *(va_arg(list, short int*)) = chars;
                            break;
                            
                        case 0: {
                            int* a = va_arg(list, int*);
                            *a = chars;
                            break;
                        }
                            
                        case 'l':
                            *(va_arg(list, long*)) = chars;
                            break;
                        case 'q':
                            *(va_arg(list, long long*)) = chars;
                            break;
                        case 'j':
                            *(va_arg(list, intmax_t*)) = chars;
                            break;
                        case 'z':
                            *(va_arg(list, size_t*)) = chars;
                            break;
                        case 't':
                            *(va_arg(list, ptrdiff_t*)) = chars;
                            break;
                        default:
                            break;
                    }
                    break;
                }
                    
                case 'e':
                case 'E':
                    emode = true;
					__attribute__ ((fallthrough));
                case 'f':
                case 'F':
                case 'g':
                case 'G':
                {
                    double floating = va_arg(list, double);
                    
                    while (emode && floating >= 10) {
                        floating /= 10;
                        ++expo;
                    }
                    
                    int form = lengthSpec - precSpec - expo - (precSpec || altForm ? 1 : 0);
                    if (emode) {
                        form -= 4;      // 'e+00'
                    }
                    if (form < 0) {
                        form = 0;
                    }
                    int tempform = form;
                    if (leftJustify) {
                        form = 0;
                    }
                    __i__int_str(floating, intStrBuffer, base, plusSign, spaceNoSign, form, \
                            leftJustify, zeroPad);
                    
                    int lenIntstrbuffer = (int) strlen(intStrBuffer);
                    
                    __i_displayString(intStrBuffer, &chars, stream, buffer, size);
                    
                    floating -= (int) floating;
                    
                    for (int i = 0; i < precSpec; ++i) {
                        floating *= 10;
                    }
                    intmax_t decPlaces = (intmax_t) (floating + 0.5);
                    
                    if (precSpec) {
                        __i_displayCharacter('.', &chars, stream, buffer, size);
                        //printf("Tempform = %d\n", tempform);
                        __i__int_str(decPlaces, intStrBuffer, 10, false, false, 0, false, false);
                        intStrBuffer[precSpec] = 0;
                        __i_displayString(intStrBuffer, &chars, stream, buffer, size);
                    } else if (altForm) {
                        __i_displayCharacter('.', &chars, stream, buffer, size);
                        //++lenIntstrbuffer;
                    }
                    
                    if (tempform - lenIntstrbuffer > 0) {
                        for (int i = 0; i < tempform - lenIntstrbuffer; ++i) {
                            __i_displayCharacter(' ', &chars, stream, buffer, size);
                        }
                    }
                    
                    break;
                }
                    
                    
                case 'a':
                case 'A':
                    //ACK! Hexadecimal floating points...
                    break;
                    
                default:
                    break;
            }
            
            if (specifier == 'e') {
                __i_displayString("e+", &chars, stream, buffer, size);
            } else if (specifier == 'E') {
                __i_displayString("E+", &chars, stream, buffer, size);
            }
            
            if (specifier == 'e' || specifier == 'E') {
                __i__int_str(expo, intStrBuffer, 10, false, false, 2, false, true);
                __i_displayString(intStrBuffer, &chars, stream, buffer, size);
            }
            
        } else {
			int ln = strlen(plainCharBuffer);
			if (ln < 1000) {
				plainCharBuffer[ln] = format[i];
				plainCharBuffer[ln + 1] = 0;

			} else {
				__i_displayString(plainCharBuffer, &chars, stream, buffer, size);
				plainCharBuffer[0] = 0;

				__i_displayCharacter(format[i], &chars, stream, buffer, size);
			}
        }
    }


	if (strlen(plainCharBuffer)) {
		__i_displayString(plainCharBuffer, &chars, stream, buffer, size);
		plainCharBuffer[0] = 0;
	}

	//write null char, but don't save it to 'chars' by using a dummy
	//(only if sprintf, not printf)
	if (buffer) {
		int dummy = chars;
		__i_displayCharacter (0, &dummy, stream, buffer, size);
	}

#ifndef _libk
	if (stream == stdout) {
		fflush (stdout);
	}
#endif
    return chars;
}

int vsnprintf(char * restrict s, size_t n, const char * restrict format, va_list list) {
    return __i_internal_printf(format, list, NULL, s, (int) n);
}

int vsprintf(char * restrict s, const char * restrict format, va_list arg) {
    return vsnprintf(s, 0, format, arg);
}

__attribute__ ((format (printf, 3, 4))) \
int snprintf(char * restrict s, size_t n, const char * restrict format, ...) {
    va_list list;
    va_start (list, format);
    int i = vsnprintf (s, n, format, list);
    va_end (list);
    return i;
}

__attribute__ ((format (printf, 2, 3))) int sprintf(char * restrict s, const char * restrict format, ...) {
    va_list list;
    va_start (list, format);
    int i = vsnprintf (s, 0, format, list);
    va_end (list);
    return i;
}

int vfprintf(FILE * restrict stream, const char * restrict format, va_list list) {
    return __i_internal_printf(format, list, stream, NULL, 0);
}

#ifndef _libk
int vprintf(const char * restrict format, va_list list) {
    return vfprintf(stdout, format, list);
}
#endif

__attribute__ ((format (printf, 2, 3))) \
int fprintf(FILE * restrict stream, const char * restrict format, ...) {
    va_list list;
    va_start (list, format);
    int i = vfprintf(stream, format, list);
    va_end (list);
    return i;
}

#ifndef _libk
__attribute__ ((format (printf, 1, 2))) int printf (const char * restrict format, ...)
{
    va_list list;
    va_start (list, format);
    int i = vprintf (format, list);
    va_end (list);
    return i;
}

int vasprintf (char** ret, const char * restrict format, va_list arg)
{
	//not tested

	char* buffer = malloc (2048);			//any old size
	if (buffer == NULL) {
		return -1;
	}
	int size = vsnprintf (buffer, 2047, format, arg);
	if (size >= 2047) {
		free (buffer);
		buffer = malloc (size + 2);
		if (buffer == NULL) {
			return -1;
		}
		vsnprintf (buffer, size + 1, format, arg);
	}
	*ret = buffer;
	return 0;
}

__attribute__ ((format (printf, 2, 3))) int asprintf (char** ret, const char * restrict format, ...)
{
	//not tested

	va_list list;
	va_start (list, format);
	int i = vasprintf (ret, format, list);
	va_end (list);
	return i;
}

#endif