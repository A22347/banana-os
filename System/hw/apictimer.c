#include <hw/apictimer.h>

void setupAPICTimer (unsigned hz, unsigned irq)
{
	logk ("APIC timer setup. hertz = %d, irq = %d\n", hz, irq);
	irq += 32;	//IRQs start at 32, IRQ0 = 32, IRQn = 32+n

	if (getCPUNumber () == 0) {
		logk ("about to setup PIT\n");
		setupPIT (20);

		uint64_t oldticks = toticks;
		toticks = 0;

		*((uint32_t*) ((uint8_t*) (size_t) cpuGetAPICBase () + APIC_REGISTER_TIMER_DIV)) = 0x3;
		*((uint32_t*) ((uint8_t*) (size_t) cpuGetAPICBase () + APIC_REGISTER_TIMER_INITCNT)) = 0xFFFFFFFF;

		asm volatile ("sti");
		while (toticks < 4);

		*((uint32_t*) ((uint8_t*) (size_t) cpuGetAPICBase () + APIC_REGISTER_LVT_TIMER)) = APIC_REGISTER_LVT_MASKED;	//APIC timer is OFF!

		uint32_t ticksInASecond = (0xFFFFFFFF - *((uint32_t*) ((uint8_t*) (size_t) cpuGetAPICBase () + APIC_REGISTER_TIMER_CURRCNT))) * 5;
		uint32_t ticksInTimerHertz = ticksInASecond / TIMER_HERTZ;
		toticks += oldticks;

		*((uint32_t*) ((uint8_t*) (size_t) cpuGetAPICBase () + APIC_REGISTER_LVT_TIMER)) = irq | APIC_REGISTER_LVT_TIMER_MODE_PERIODIC;
		*((uint32_t*) ((uint8_t*) (size_t) cpuGetAPICBase () + APIC_REGISTER_TIMER_DIV)) = 0x3;
		*((uint32_t*) ((uint8_t*) (size_t) cpuGetAPICBase () + APIC_REGISTER_TIMER_INITCNT)) = ticksInTimerHertz;

		unsetupPIT ();

	} else {
		uint64_t oldticks = toticks;
		toticks = 0;
		*((uint32_t*) ((uint8_t*) (size_t) cpuGetAPICBase () + APIC_REGISTER_TIMER_DIV)) = 0x3;
		*((uint32_t*) ((uint8_t*) (size_t) cpuGetAPICBase () + APIC_REGISTER_TIMER_INITCNT)) = 0xFFFFFFFF;
		asm volatile ("sti");
		while (toticks < hz / 5);
		//CPU 0 timer is going at TIMER_HERTZ
		// 1/5 of a second should have passed

		*((uint32_t*) ((uint8_t*) (size_t) cpuGetAPICBase () + APIC_REGISTER_LVT_TIMER)) = APIC_REGISTER_LVT_MASKED;	//APIC timer is OFF!

		uint32_t ticksInASecond = (0xFFFFFFFF - *((uint32_t*) ((uint8_t*) (size_t) cpuGetAPICBase () + APIC_REGISTER_TIMER_CURRCNT))) * 5;
		uint32_t ticksInTimerHertz = ticksInASecond / hz;
		toticks += oldticks;

		*((uint32_t*) ((uint8_t*) (size_t) cpuGetAPICBase () + APIC_REGISTER_LVT_TIMER)) = irq | APIC_REGISTER_LVT_TIMER_MODE_PERIODIC;
		*((uint32_t*) ((uint8_t*) (size_t) cpuGetAPICBase () + APIC_REGISTER_TIMER_DIV)) = 0x3;
		*((uint32_t*) ((uint8_t*) (size_t) cpuGetAPICBase () + APIC_REGISTER_TIMER_INITCNT)) = ticksInTimerHertz;
	}

	installHardwareInterruptHandler (irq - 32, timer_handler);
}