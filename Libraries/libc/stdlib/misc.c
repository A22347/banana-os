//
//  misc.c
//  Banana libc
//
//  Created by Alex Boxall on 12/7/18.
//  Copyright © 2018 Alex Boxall. All rights reserved.
//

#ifndef _libk

#include "stdlib.h"
#include "syscall.h"

int system(const char *string) {
    if (string) {
        return __i_system_call_system(string);
    }
    
    //if string is NULL, check if a command processor is avaliable
    return 1;   //this means 'yes'
}

char *getenv(const char *name) {
    //returns 0 when environ. var cannot be found
    //or a string pointer when it can
    return __i_system_call_getenv(name);
}

#endif
