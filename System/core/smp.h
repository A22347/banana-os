#pragma once

#include <stdint.h>
#include <core/kernel.h>			//for memory location definitions
#include <fs/fat/pff.h>

#define MAX_CPUS 8
#define BOOT1LEN 1024 * 16

#define CPU_APICID_LOCATION ((size_t) SMP_DATA_LOCATION )
#define CPU_CPUID_LOCATION ((size_t) CPU_APICID_LOCATION + 4)
#define CPU_READY_ARRAY ((size_t) CPU_CPUID_LOCATION + 4) 					//each CPU gets a status byte (e.g. 0 = not started, 1 = bootloading, 2 = ready, etc.)

#define CPU_BOOTLOADER1 (((uint32_t)(((size_t)CPU_READY_ARRAY + (size_t)MAX_CPUS) / 4096) + 1) * 4096)			//first bootloader, on a page boundry
#define CPU_BOOTLOADER2 (CPU_BOOTLOADER1 + BOOT1LEN)			//second bootloader

#define STATUS_BYTE_NOTSTARTED 0
#define STATUS_BYTE_DONE 2

#define CPU_READY_BYTE 0x7D00
#define CPU_ID_BYTE 0x7D01
#define CPU_GDT_BYTE 0x7D04
#define CPU_IDT_BYTE 0x7D08

void delayForOneTick ();
extern void enableA20 ();

void setupCPUNumber (uint32_t address, uint8_t cpuNumber, uint8_t apicID);
void setupOtherCPUs ();