#ifndef __CHECKBUTTON_HPP__
#define __CHECKBUTTON_HPP__

#include "Core.hpp"
#include "Widget.hpp"
#include "Window.hpp"
#include "Label.hpp"
#include "Image.hpp"

class Checkbox : public Widget {

	friend class Window;

protected:
	int x;
	int y;
	void _write ();
	bool state;
	Image* image;
	Label* label;

public:
	Checkbox (Window& parent, int _x, int _y, bool _state = false, char* _text = "");
	~Checkbox ();
	void selfupdate (Event_t event);

	void setText (char* _text);
	char* getText ();
};

#endif