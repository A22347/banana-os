#include <pthread.h>
#include <unistd.h>

// https://www.thegeekstuff.com/2012/05/c-mutex-examples/
// https://www.thegeekstuff.com/2012/04/create-threads-in-linux/

void (*__CancelRoutines[MAX_THREADS_PER_PROCESS][64])(void*);
void* __CancelRountineArguments[MAX_THREADS_PER_PROCESS][64];
uint8_t __CancelRoutinePointer[MAX_THREADS_PER_PROCESS] = { 0 };

void pthread_cleanup_push (void (*routine)(void*), void *arg)
{
	pthread_t p = pthread_self ();
	__CancelRoutines[p][__CancelRoutinePointer[p]] = routine;
	__CancelRountineArguments[p][__CancelRoutinePointer[p]] = arg;
	++__CancelRoutinePointer[p];
}

void pthread_cleanup_pop (int execute)
{
	pthread_t p = pthread_self ();
	--__CancelRoutinePointer[p];
	if (execute && __CancelRoutines[p][__CancelRoutinePointer[p]]) {
		__CancelRoutines[p][__CancelRoutinePointer[p]] (__CancelRountineArguments[p][__CancelRoutinePointer[p]]);
	}
}

int pthread_equal (pthread_t tid1, pthread_t tid2)
{
	return tid1 == tid2;
}

pthread_t pthread_self ()
{
	return SystemCall (SC_GetPID, 1, 0, 0);
}

int pthread_create (pthread_t* tidp, const pthread_attr_t* attr, void *(*start_rtn)(void*), void* arg)
{
	pthread_t ret = SystemCall (SC_NewThread, (int) attr, (int) start_rtn, arg);
	if (ret) {
		*tidp = ret;
		__CancelRoutinePointer[ret] = 0;
		return 0;
	}
	return 1;
}

int pthread_attr_destroy (pthread_attr_t* attr)
{
	return 0;
}

int pthread_attr_getdetachstate (const pthread_attr_t* attr, int* val)
{
	*val = attr->detachstate;
	return 0;
}

int pthread_attr_getguardsize (const pthread_attr_t* attr, size_t* val)
{
	*val = attr->guardsize;
	return 0;
}

int pthread_attr_getinheritsched (const pthread_attr_t* attr, int* val)
{
	*val = attr->inheritsched;
	return 0;
}

int pthread_attr_getschedparam (const pthread_attr_t* attr, struct sched_param* val)
{
	*val = attr->schedparam;
	return 0;
}

int pthread_attr_getschedpolicy (const pthread_attr_t* attr, int* val)
{
	*val = attr->schedpolicy;
	return 0;
}

int pthread_attr_getscope (const pthread_attr_t* attr, int* val)
{
	*val = attr->scope;
	return 0;
}

int pthread_attr_getstackaddr (const pthread_attr_t* attr, void** val)
{
	*val = attr->stackaddr;
	return 0;
}

int pthread_attr_getstacksize (const pthread_attr_t* attr, size_t* val)
{
	*val = attr->stacksize;
	return 0;
}

int pthread_attr_init (pthread_attr_t* attr)
{
	pthread_attr_t c;
	*attr = c;				//structures can be assigned like any other variable
	return 0;
}

int pthread_attr_setdetachstate (pthread_attr_t* attr, int val)
{
	attr->detachstate = val;
	return 0;
}

int pthread_attr_setguardsize (pthread_attr_t* attr, size_t val)
{
	attr->guardsize = val;
	return 0;
}

int pthread_attr_setinheritsched (pthread_attr_t* attr, int val)
{
	attr->inheritsched = val;
	return 0;
}

int pthread_attr_setschedparam (pthread_attr_t* attr, const struct sched_param* val)
{
	attr->schedparam = *val;
	return 0;
}

int pthread_attr_setschedpolicy (pthread_attr_t* attr, int val)
{
	attr->detachstate = val;
	return 0;
}

int pthread_attr_setscope (pthread_attr_t* attr, int val)
{
	attr->scope = val;
	return 0;
}

int pthread_attr_setstackaddr (pthread_attr_t* attr, void * val)
{
	attr->stackaddr = val;
	return 0;
}

int pthread_attr_setstacksize (pthread_attr_t* attr, size_t val)
{
	attr->stacksize = val;
	return 0;
}

int __CLEANUP (pthread_t thread)
{
	if (!SystemCall (SC_ThreadIsAlive, thread, getpid (), 0)) {
		return -1;
	}
	while (__CancelRoutinePointer[thread]) {
		--__CancelRoutinePointer[thread];
		if (__CancelRoutines[thread][__CancelRoutinePointer[thread]]) {
			__CancelRoutines[thread][__CancelRoutinePointer[thread]] (__CancelRountineArguments[thread][__CancelRoutinePointer[thread]]);
		}
	}
	return 0;
}

void __TERMINATE (pthread_t tid, void* value_ptr)
{
	SystemCall (SC_AbortThread, tid, getpid (), value_ptr);
}

int pthread_cancel (pthread_t tid)
{
	// add error handling (check that the thread exists)
	// should this be executing in the target thread??
	__CLEANUP (tid);
	__TERMINATE (tid, 0);
	return 0;
}

void pthread_exit (void* value_ptr)
{
	__CLEANUP (pthread_self ());
	__TERMINATE (pthread_self (), value_ptr);
}

int pthread_join (pthread_t thread, void **value_ptr)
{
	while (SystemCall (SC_ThreadIsAlive, thread, getpid (), 0)) {
		yield ();
	}
	*value_ptr = (void*) ((size_t)SystemCall (SC_GetThreadReturnPointer, thread, getpid (), 0));
	return 0;
}


int pthread_mutex_init (pthread_mutex_t* mutex, const pthread_mutexattr_t* attr)
{
	mutex->owner = 0;
	mutex->locked = 0;
	return 0;
}

DECLARE_LOCK (_MUTEX_SPINLOCK_);
DECLARE_LOCK (_MUTEX_SPINLOCK_U_);
DECLARE_LOCK (_MUTEX_SPINLOCK_T_);

//! for now, the mutex functions are *NOT* atomic
int pthread_mutex_lock (pthread_mutex_t* mutex)
{
	return 0;
	//LOCK (_MUTEX_SPINLOCK_)

	printf ("Locking mutex!\n");

	while (mutex->locked) {
		yield ();
	}

	mutex->locked = 1;

	printf ("Locked mutex!\n");

	//UNLOCK (_MUTEX_SPINLOCK_)

	return 0;
}

int pthread_mutex_trylock (pthread_mutex_t* mtx)
{
	return 0;

	printf ("Trying mutex!\n");

	//LOCK (_MUTEX_SPINLOCK_T_)

		volatile pthread_mutex_t* volatile mutex = (volatile pthread_mutex_t* volatile) mtx;

	if (mutex->locked) {
		//UNLOCK (_MUTEX_SPINLOCK_T_)
		return 1;
	}

	mutex->locked = 1;

	//if (mutex->locked) {
		////UNLOCK (_MUTEX_SPINLOCK_T_)
		//return 1;
		/*if (mutex->owner == pthread_self ()) {
			//UNLOCK (_MUTEX_SPINLOCK_T_)
			return 0;
		} else {
			//UNLOCK (_MUTEX_SPINLOCK_T_)
			return -1;
		}*/
		//}

		//mutex->locked = 1;
		//mutex->owner = pthread_self ();

	//UNLOCK (_MUTEX_SPINLOCK_T_)

	printf ("Tried mutex!\n");

	return 0;
}

int pthread_mutex_unlock (pthread_mutex_t* mtx)
{
	return 0;
	printf ("Unlocking mutex!\n");

	volatile pthread_mutex_t* volatile mutex = (volatile pthread_mutex_t* volatile) mtx;

	//LOCK (_MUTEX_SPINLOCK_U_)

		/*
		if (mutex->owner == pthread_self ()) {	// && mutex->locked
			mutex->locked = 0;
		} else {
			//UNLOCK (_MUTEX_SPINLOCK_U_)
			return 1;
		}
		*/

		mutex->locked = 0;

	//UNLOCK (_MUTEX_SPINLOCK_U_)

		printf ("Unlocked mutex!\n");

		return 0;
}

int pthread_mutex_destroy (pthread_mutex_t* mtx)
{
	//return 0;
	printf ("Destroyed mutex!\n");

	mtx->owner = 0;
	mtx->locked = 0;
	return 0;
}

void* threadDataTable[MAX_THREADS_PER_PROCESS][MAX_THREAD_SPECIFIC_STORAGE_LOCATIONS];
int freeEntry[MAX_THREADS_PER_PROCESS] = { 0 };

int pthread_key_create (pthread_key_t* key, void (*a)(void*))
{
	assert (freeEntry[pthread_self ()] < MAX_THREAD_SPECIFIC_STORAGE_LOCATIONS);

	*key = (freeEntry[pthread_self ()])++;
	return 0;
}

int pthread_key_delete (pthread_key_t key)
{
	return 0;
}

int pthread_yield (void)
{
	yield ();
	return 0;
}

int pthread_once (pthread_once_t* control, void (*init)(void))
{
	if (*control == 0) {
		(*init)();
		*control = 1;
	}
	return 0;
}

void* pthread_getspecific (pthread_key_t key)
{
	return threadDataTable[pthread_self ()][key];
}

int pthread_setspecific (pthread_key_t key, const void* data)
{
	threadDataTable[pthread_self ()][key] = (void*) data;
	return 0;
}

int pthread_cond_broadcast (pthread_cond_t* c)
{
	for (int i = 0; i < MAX_THREADS_PER_PROCESS; ++i) {
		c->blockedThreads[i] = false;
	}
	return 0;
}

int pthread_cond_destroy (pthread_cond_t* c)
{
	return 0;
}

int pthread_cond_init (pthread_cond_t* c, const pthread_condattr_t* a)
{
	c->blockedThreads[pthread_self ()] = 0;
	return 0;
}

int pthread_cond_signal (pthread_cond_t* c)
{
	for (int i = 0; i < MAX_THREADS_PER_PROCESS; ++i) {
		if (c->blockedThreads[i]) {
			c->blockedThreads[i] = false;
			return 0;
		}
	}
	return 0;
}

DECLARE_LOCK (_COND_WAIT_SPINLOCKX_);

int pthread_cond_timedwait (pthread_cond_t* c, pthread_mutex_t* m, const struct timespec* ts)
{
	LOCK (_COND_WAIT_SPINLOCKX_);
	m->locked = 0;
	c->blockedThreads[pthread_self ()] = true;
	UNLOCK (_COND_WAIT_SPINLOCKX_);
	time_t a = time (NULL);
	while (c->blockedThreads[pthread_self ()]) {
		yield ();
		if (time (NULL) > a + ts->tv_sec) {
			return 1;
		}
	}
	return 0;
}

DECLARE_LOCK (_COND_WAIT_SPINLOCK_);

int pthread_cond_wait (pthread_cond_t* c, pthread_mutex_t* m)
{
	LOCK (_COND_WAIT_SPINLOCK_);
	m->locked = 0;
	c->blockedThreads[pthread_self ()] = true;
	UNLOCK (_COND_WAIT_SPINLOCK_);
	while (c->blockedThreads[pthread_self ()]) {
		yield ();
	}
	return 0;
}

int pthread_condattr_destroy (pthread_condattr_t* ca)
{

}

int pthread_condattr_getpshared (const pthread_condattr_t* ca, int* i)
{

}

int pthread_condattr_init (pthread_condattr_t* ca)
{

}

int pthread_condattr_setpshared (pthread_condattr_t* ca, int i)
{

}
