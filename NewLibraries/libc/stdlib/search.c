//
//  search.c
//  Banana libc
//
//  Created by Alex Boxall on 23/7/18.
//  Copyright © 2018 Alex Boxall. All rights reserved.
//

#include "stdlib.h"
#include "string.h"
#include <stdint.h>

void *bsearch(const void *key, const void *base, size_t nmemb, size_t size, \
                      int (*compar)(const void *, const void *)) {
    return 0;
}

void __i_swap (void *e1, void *e2, size_t size)
{
    void* sp = (void*) malloc (size);
    
    memcpy (sp, e1, size);
    memcpy (e1, e2, size);
    memcpy (e2, sp, size);
    
    free (sp);
}

//today we'll be using a nice, SLOW bubblesort...
void qsort (void* bss, size_t n, size_t size, int (*cmp)(const void*, const void*))
{
    for (size_t i = 0; i < n - 1; ++i) {
        for (size_t j = 0; j < n - i - 1; ++j) {
            if (cmp((uint8_t*)bss + j * size, (uint8_t*)bss + (j + 1) * size) > 0) {
                __i_swap((uint8_t*)bss + j * size, (uint8_t*)bss + (j + 1) * size, size);
            }
        }
    }
}
