#pragma once

#ifdef __cplusplus
extern "C" {
#endif

#include <stdbool.h>

	typedef struct criticalFile_t
	{
		char name[128];
		char backup[64];
		bool abort;
		bool hidden;
		bool system;

	} criticalFile_t;

	void fixMissingCriticalFiles ();
	void backupCriticalFiles ();

#ifdef __cplusplus
}
#endif