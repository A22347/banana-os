#pragma once

#include <core/kernel.h>
#include <core/string.h>
#include <core/memory.h>
#include <core/syscalldef.h>
#include <core/syscalls.h>

#include <hw/pic.h>
#include <hw/apic.h>

#include <hal/display.h>

#include <gui/terminal.h>

#define MAX_DRIVER_INT_SLOTS 8

typedef void (*interruptHandler) (struct regs*);

struct idt_entry
{
#if PLATFORM_ID == 64
	uint16_t base_lo; // offset bits 0..15
	uint16_t sel; // a code segment selector in GDT or LDT
	uint8_t always0;       // bits 0..2 holds Interrupt Stack Table offset, rest of bits zero.
	uint8_t flags; // type and attributes
	uint16_t base_hi; // offset bits 16..31
	uint32_t base_veryhigh; // offset bits 32..63
	uint32_t zero;     // reserved
#else
	uint16_t base_lo;
	uint16_t sel;        /* Our kernel segment goes here! */
	uint8_t always0;     /* This will ALWAYS be set to 0! */
	uint8_t flags;       /* Set using the above table! */
	uint16_t base_hi;
#endif
} __attribute__ ((packed));

struct idt_ptr
{
	unsigned short limit;
	size_t base;
} __attribute__ ((packed));

extern char errorCodes[32][30];
extern int numberOfSameType;

void io_wait ();

void setupInterruptChip ();
void sendEOI (int intNo);
void installHardwareInterruptHandler (uint8_t number, interruptHandler handler);
void uninstallHardwareInterruptHandler (uint8_t number);

extern void yieldFromKernel ();

void setupIDT ();
void setupISRs ();
void setupIRQs ();
void setupInterrupts ();

void sendInterruptToDrivers (int intno);
void initDriverInterrupts ();

typedef struct DriverInterruptHandler {
	int driverPID;
	interruptHandler handler;

} DriverInterruptHandler;

extern DriverInterruptHandler driverInterruptHandlers[24][MAX_DRIVER_INT_SLOTS];
extern int nextDriverInterruptHandler[24];

void installHardwareInterruptHandlerFromDriver (uint8_t number, int driverPID, interruptHandler handler);
void uninstallHardwareInterruptHandlerFromDriver (uint8_t number, int driverPID);