#include <core/fileprotect.h>
#include <fs/vfs.h>
#include <libk/stdio.h>
#include <core/memory.h>

criticalFile_t criticalFileTable[64] = {
	{.name = "C:/Banana/BANANABT"				, .backup = "BOOTLOAD.SYS", .abort = true , .hidden = true , .system = true },
	{.name = "C:/Banana/BOOTSET.SYS"			, .backup = "BOOTSET.SYS" , .abort = true , .hidden = false, .system = false},
	{.name = "C:/Banana/System/Command.exe"		, .backup = "COMMAND.EXE" , .abort = false, .hidden = false, .system = false},
	{.name = "C:/Banana/System/defaultuser.dat"	, .backup = "DEFLTUSR.DAT", .abort = false, .hidden = false, .system = false},
	{.name = "C:/Banana/System/drvdb.sys"		, .backup = "DRVDB.SYS"   , .abort = true , .hidden = false, .system = false},
	{.name = "C:/Banana/System/drvdbv.sys"		, .backup = "DRVDBV.SYS"  , .abort = true , .hidden = false, .system = false},
	{.name = "C:/Banana/System/terminal.sys"	, .backup = "TERMINAL.SYS", .abort = true , .hidden = false, .system = false},
	{.name = "C:/Banana/System/gui.exe"			, .backup = "GUI.EXE"	  , .abort = true , .hidden = false, .system = false},
	{.name = "C:/Banana/System/KERNEL32.EXE"	, .backup = "KERNEL32.EXE", .abort = true , .hidden = true , .system = true },
	{.name = "C:/Banana/System/KERNEL.EXE"		, .backup = "KERNEL.EXE"  , .abort = true , .hidden = true , .system = true },
	{.name = "C:/Banana/System/Login.exe"		, .backup = "LOGIN.EXE"   , .abort = true , .hidden = false, .system = false},
	{.name = "C:/Banana/System/TRAMP.EXE"		, .backup = "TRAMP.EXE "  , .abort = true , .hidden = true , .system = true },
};

void fixMissingCriticalFiles ()
{

}

void backupCriticalFiles ()
{
	for (int i = 0; i < 12; ++i) {
		vfs_file_t inFile;
		vfs_file_t outFile;

		char backupName[256];
		sprintf(backupName, "C:/Banana/System/@BACKUP/%s", criticalFileTable[i].backup);

		vfs_open(&inFile, criticalFileTable[i].name, vfs_file_mode_read);
		vfs_open(&outFile, backupName, vfs_file_mode_write);

		uint64_t size = 0;
		vfs_size(&inFile, &size);

		uint8_t* buffer = malloc(size + 8);

		uint64_t actual;
		vfs_read(&inFile, buffer, size, &actual);
		vfs_write(&outFile, buffer, size);

		free(buffer);

		vfs_close(&inFile);
		vfs_close(&outFile);

	}
}