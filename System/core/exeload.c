#include <core/exeload.h>
#include <hal/floating.h>
#include <libk/stdio.h>
#include <debug/log.h>

bool programExists(char* n)
{
	vfs_file_t f;
	int fr = vfs_open(&f, n, vfs_file_mode_read);
	if (fr != 0) {
		logk("\n\rattempted to add a task (%s) which didn't exist\n\r", n);
		return false;
	}
	vfs_close(&f);
	return true;
}

int correctSize(char* n)
{
	vfs_file_t f;
	char vb[5] = { 0 };
	vfs_open(&f, n, vfs_file_mode_read);
	uint64_t actual;
	vfs_read(&f, (uint8_t*) vb, 4, &actual);
	int r;

	uint64_t size;
	vfs_size(&f, &size);

	vfs_close(&f);
	if (vb[0] == 'E') {
		size /= 2;
	}
	if (vb[0] == 'E') {
		size *= 2;
	}
	return size;
}

int setupThread(uint32_t tasknumber, void* (*function)(void*), void* arg)
{
	logk("SAY GOOD-BYTE TO THREADS FOR NOW\n(EXELOAD.C, LINE %d, setupThread\n", __LINE__);
	while (1);
}

void setupTask(char* n, int pid, int progsize)
{
	TASKS[pid].threads[0].currentlyExecuting = false;
	TASKS[pid].systask = false;
	TASKS[pid].currentlyExecutingThread = 0;
	TASKS[pid].threads[0].eflags = 0x202;

	TASKS[pid].threads[0].wait = 0;
	TASKS[pid].threads[0].halt = false;
	TASKS[pid].console = true;
	TASKS[pid].numberOfThreads = 1;

	TASKS[pid].threads[0].kernelSwitchStack = (size_t) malloc(1024 * 16) + 1024 * 16;		//stacks grow downward, so set the pointer above what you want it to be

	TASKS[pid].driver = false;
	TASKS[pid].threads[0].loadedBefore = false;

	TASKS[pid].numberOfArguments = 1;
	for (int i = 0; i < 64; ++i) {
		memset(TASKS[pid].arguments[i], 0, 256);
	}
	strcpy(TASKS[pid].arguments[0], n);

	TASKS[pid].processHalt = 0;

	floatingStore(TASKS[pid].threads[0].floatingStorage);
	strcpy(TASKS[pid].filename, n);

	memset(TASKS[pid].childProcesses, 0, sizeof(TASKS[pid].childProcesses));
	TASKS[pid].childrenCount = 0;
	TASKS[pid].parent = 0;

	memset(TASKS[pid].threads[0].cwd, 0, 256);
	strcpy(TASKS[pid].threads[0].cwd, UserFolder());
	TASKS[pid].threads[0].cwdCorrect = false;
}

bool newAddTaskWithProcessID(char* n, int pid)
{
	logk("ADDING NEW TASK NAMED ( %s )\n", n);
	char header[16] = { 0 };
	unsigned char uheader[16] = { 0 };
	int programSize;

	if (!programExists(n)) return false;
	programSize = correctSize(n);
	if (!programSize) return false;

	setupTask(n, pid, programSize);

	vfs_file_t programFile;
	int byteRead;

	uint64_t actual;
	vfs_open(&programFile, n, vfs_file_mode_read);
	vfs_read(&programFile, (uint8_t*) header, 16, &actual);
	memcpy(uheader, header, 16);

	unsigned int offset = 0;
	offset |= uheader[12];
	offset |= uheader[13] << 8;
	offset |= uheader[14] << 16;
	offset |= uheader[15] << 24;

	if (!memcmp(header, "EXE", 3) && PLATFORM_ID == 86) {
		offset = 16;
		logk("Adding a 32 bit task...\n");
		logk("Program is at offset %d.\n", offset);
		loadELFWithPID(&programFile, pid);

	} else if (!memcmp(header, "EXE", 3) && PLATFORM_ID == 64) {
		logk("Adding a 64 bit task...\n");
		logk("Program is at offset %d.\n", offset);
		vfs_seek(&programFile, offset);
		loadELFWithPID(&programFile, pid);

	} else {
		logk("INVALID PROGRAM FILE %s\n", n);
		vfs_close(&programFile);
		return false;
	}

	vfs_close(&programFile);
	logk("newAddTaskWithProcessID returned\n");
	return true;
}

bool newAddTaskReplace(char* n)
{
	bool ret = newAddTaskWithProcessID(n, TASK_Current);
	if (ret) {
		++TASK_Next;
	}
	return ret;
}

bool newAddTask(char* n)
{
	bool ret = newAddTaskWithProcessID(n, TASK_Next);
	if (ret) {
		++TASK_Next;
	}
	return ret;
}

bool addTask(char* n)
{
	return newAddTask(n);
}

void createTaskFamily(processID_t parent, processID_t child)
{
	logk("creating family. parent = 0x%X, child = 0x%X\n", parent, child);
	TASKS[parent].childProcesses[TASKS[parent].childrenCount] = child;
	TASKS[parent].childrenCount++;
	logk("Parent now has %d children.\n", TASKS[parent].childrenCount);
	TASKS[child].parent = parent;
	TASKS[child].stderr = TASKS[parent].stderr;
	TASKS[child].stdout = TASKS[parent].stdout;
	TASKS[child].stdin = TASKS[parent].stdin;
	strcpy(TASKS[child].threads[0].cwd, TASKS[parent].threads[0].cwd);
}

bool addTaskWithArgumentsVA(char* filename, va_list va)
{
	bool b = addTask(filename);

	TASKS[TASK_Next - 1].numberOfArguments = 1;		// argument 0 is the filename, and is already there

													// start at argument '1', because argument '0' is the filename, and is already set

	for (int i = 1; i < 255; ++i) {                //it should break prior to this, but just in case
		char* va_chr = va_arg(va, char*);
		if (!strcmp(va_chr, LAST_ARGUMENT)) {
			break;
		}
		strcpy(TASKS[TASK_Next - 1].arguments[i], va_chr);
		TASKS[TASK_Next - 1].numberOfArguments++;
	}
	return b;
}

bool addTaskWithArguments(char* filename, ...)
{
	va_list va;
	va_start(va, filename);

	bool b = addTaskWithArgumentsVA(filename, va);

	va_end(va);
	return b;
}