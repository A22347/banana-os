#ifndef __ERRNO_H__
#define __ERRNO_H__

#ifdef __cplusplus
extern "C" {
#endif

	extern int errno;

	extern char errorNames[256][48];

#define EUNKNOWNERR 0
#define E2BIG 1
#define EACCES 2
#define EADDRINUSE 3
#define EADDRNOTAVAIL 4
#define EAFNOSUPPORT 5
#define EAGAIN 6
#define EALREADY 7
#define EBADF 8
#define EBADMSG 9
#define EBUSY 10
#define ECANCELED 11
#define ECHILD 12
#define ECONNABORTED 13
#define ECONNREFUSED 14
#define ECONNRESET 15
#define EDEADLK 16
#define EDESTADDRREQ 17
#define EDOM 18
#define EEXIST 19
#define EFAULT 20
#define EFBIG 21
#define EHOSTUNREACH 22
#define EIDRM 23
#define EILSEQ 24
#define EINPROGRESS 25
#define EINTR 26
#define EINVAL 27
#define EIO 28
#define EISCONN 29
#define EISDIR 30
#define ELOOP 31
#define EMFILE 32
#define EMLINK 33
#define EMSGSIZE 34
#define ENAMETOOLONG 35
#define ENETDOWN 36
#define ENETRESET 37
#define ENETUNREACH 38
#define ENFILE 39
#define ENOBUFS 40
#define ENODATA 41
#define ENODEV 42
#define ENOENT 43
#define ENOEXEC 44
#define ENOLCK 45
#define ENOLINK 46
#define ENOMEM 47
#define ENOMSG 48
#define ENOPROTOOPT 49
#define ENOSPC 50
#define ENOSR 51
#define ENOSTR 52
#define ENOSYS 53
#define ENOTCONN 54
#define ENOTDIR 55
#define ENOTEMPTY 56
#define ENOTRECOVERABLE 57
#define ENOTSOCK 58
#define ENOTSUP 59
#define ENOTTY 60
#define ENXIO 61
#define EOPNOTSUPP 62
#define EOTHER 63
#define EOVERFLOW 64
#define EOWNERDEAD 65
#define EPERM 66
#define EPIPE 67
#define EPROTO 68
#define EPROTONOSUPPORT 69
#define EPROTOTYPE 70
#define ERANGE 71
#define EROFS 72
#define ESPIPE 73
#define ESRCH 74
#define ETIME 75
#define ETIMEDOUT 76
#define ETXTBSY 77
#define EWOULDBLOCK 78
#define EXDEV 79

#ifdef __cplusplus
}
#endif

#endif