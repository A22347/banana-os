#ifndef __TEXT_HPP__
#define __TEXT_HPP__

#include "Core.hpp"
#include "Widget.hpp"
#include "Window.hpp"

#define TEXT_END -1
#define TEXT_CURSOR -2
#define TEXT_SEL_START TEXT_CURSOR
#define TEXT_SEL_END -3

class Text : public Widget {
    friend class Window;
    
protected:    
    char* text;

	bool contentsChanged = false;
    int height = 0;
    int width = 0;
    int storedh = 0;
    int storedw = 0;
    int x = 0;
    int y = 0;
    int lineScroll = 0;
    int cursorposition = 0;
    int endcurposition = 0;
    int malloced = 0;
    
    bool focus = false;
    
    int _indexConvert (char* index);
    void _write();

	bool autoLooseFocusOnMouseClick ();
    
public:
    Text (Window& parent, int x, int y, int width = -2, int height = -1);
	~Text ();
    void selfupdate (Event_t event);
	void set (char* text);
    char* get (int start = 0, int end = TEXT_END);
    void insert (char* text, int index);
    void erase (int start, int end);
    void clear ();
    void update ();

	bool editable = true;
};

#endif