#ifndef __SETJMP_H__
#define __SETJMP_H__

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include <stddef.h>
#include <stdbool.h>

	/*typedef struct jmp_buf
	{
		uint64_t data[8];

	} jmp_buf;*/

	typedef void* jmp_buf[4];


	//int setjmp (jmp_buf buffer);
	//int longjmp (jmp_buf buffer, int val);

	#define setjmp(a) __builtin_setjmp(a)
	#define longjmp(a, b) __builtin_longjmp(a, b)


#ifdef __cplusplus
}
#endif

#endif