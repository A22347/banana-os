DEVMSG:
[bits 32]
push ebx
dec edi
push eax
dec eax
dec ecx
inc ecx
and byte [eax+0x41],dl
push ebp
inc ebx
dec eax
inc ebp
push esp
and BYTE [eax],ch
xor dh, BYTE [eax]
xor BYTE [edx],dh
sub DWORD [eax],esp
dec ecx
push ebx
and BYTE [ebp+0x59],cl
and BYTE [edx+edx*2+0x55],dl
inc ebp
and BYTE [edi+ecx*2+0x56],cl
inc ebp
and DWORD [ecx],esp
and BYTE [ebx+0x55],dl
push eax
inc ebp
push edx
and BYTE [eax+0x41],dl
push eax
inc ebp
push edx
and BYTE [ebp+0x41],cl
push edx
dec ecx
dec edi
and BYTE [ecx+0x53],cl
and BYTE [ebp+0x4e],dl
inc esp
inc ebp
push edx
push edx
inc ecx
push esp
inc ebp
inc esp

;and BYTE cs:[ebx+0x50],dl
db 0x2E
db 0x20
db 0x53
db 0x50

dec ebp
and BYTE [esi],bh
and BYTE [esp+edx*2+0x59],dl
inc esp

;and BYTE PTR cs:[ecx+0x4c],al
db 0x2E
db 0x20
db 0x41
db 0x4C

inc ebp
pop eax

;and BYTE PTR [edx+0x4f],al
db 0x20
db 0x42
db 0x4F

pop eax
inc ecx
dec esp
dec esp
[bits 64]

global loadGDT
extern gdtDescriptor
loadGDT:
    lgdt [gdtDescriptor]
    mov ax, 0x10
    mov ds, ax
    mov es, ax
    mov fs, ax
    mov gs, ax
    mov ss, ax
    jmp flush2
flush2:
    ret

global preMagic
global postMagic

extern afterExerunPtr
magicESPSave dq 0

preMagic:
	mov rax, [magicESPSave]
	pop rax		;return address
	mov [magicESPSave], rax		;ret addr.

	push rax
    push rcx
    push rdx
    push rbx
    push rbp
	push rsi
    push rdi
    push r8
    push r9
    push r10
    push r11
    push r12
    push r13
    push r14
    push r15
	
	mov rax, [magicESPSave] ;ret addr.
	mov [magicESPSave], rsp

	jmp rax

postMagic:
	mov rsp, [magicESPSave]

	pop r15
    pop r14
    pop r13
    pop r12
    pop r11
    pop r10
    pop r9
    pop r8
    pop rdi
	pop rsi
    pop rbp
    pop rbx
    pop rdx
    pop rcx
    pop rax
	
	add rsp, 8					;get rid of the return address which called preMagic

	sti
	jmp qword [afterExerunPtr]


extern flagForCPUTimingSetByExecutionAssemblyStub
extern unlock_lock__0002

extern nextCPUToRun

%macro lockTheLock 1
%%acquireLock_%1:
    lock bts [%1], 0        ;Attempt to acquire the lock (in case lock is uncontended)
    jc %%spin_wait_%1            ;Spin if locked ( organize code such that conditional jumps are typically not taken ) 
    jmp %%__exit_%1                     ;Lock obtained
 
%%spin_wait:_%1
    test dword [%1],1      ;Is the lock free?
    jnz %%spin_wait_%1           ;no, wait
    jmp %%acquireLock_%1          ;maybe, retry ( could also repeat bts, jc, ret instructions here instead of jmp )

;%%acquireLock_%1:
;    lock bts dword [%1],0        ;Attempt to acquire the lock (in case lock is uncontended)
;    jc %%spin_with_pause_%1
;	jmp %%__exit_%1
; 
;%%spin_with_pause_%1:
;    pause                    ; Tell CPU we're spinning
;    test dword [%1],1      ; Is the lock free?
;    jnz %%spin_with_pause_%1     ; no, wait
;    jmp %%acquireLock_%1          ; retry

%%__exit_%1:
%endmacro

%macro unlockTheLock 1
    mov dword [%1], 0
	ret
%endmacro

global taskno
taskno dd 0

global enableA20
enableA20:
        cli
 
        call    a20wait
        mov     al,0xAD
        out     0x64,al
 
        call    a20wait
        mov     al,0xD0
        out     0x64,al
 
        call    a20wait2
        in      al,0x60
        push    rax
 
        call    a20wait
        mov     al,0xD1
        out     0x64,al
 
        call    a20wait
        pop     rax
        or      al,2
        out     0x60,al
 
        call    a20wait
        mov     al,0xAE
        out     0x64,al
 
        call    a20wait
        sti
        ret
 
a20wait:
        in      al,0x64
        test    al,2
        jnz     a20wait
        ret
 
 
a20wait2:
        in      al,0x64
        test    al,1
        jz      a20wait2
        ret

extern hasAPIC

; GREAT ATA REFERENCE: https://github.com/omarrx024/xos/blob/master/kernel/blkdev/ata.asm


IOWAIT:
    push rax
	push rcx
	push rdx
    mov ecx, 500
    
.looping:
    mov dx, 0x80
    mov al, 0
    out dx, al
    
    loop .looping
    
    pop rdx
	pop rcx
	pop rax
    ret

extern flagForCPUTimingSetByExecutionAssemblyStub
extern timer_handler
extern DMAOutputISR


global pagingUpdate
global turnOnPaging
extern page_directory
extern testa

pagingUpdate:
    push rax
    mov rax, cr3
    mov cr3, rax
    pop rax
    ret

global exerun
global driverrun

global st_loadedBefore
global st_eax
global st_ebx
global st_ecx
global st_edx
global st_eip
global st_esp
global st_ebp
global st_edi
global st_esi
global st_eflags

global st_r8
global st_r9
global st_r10
global st_r11
global st_r12
global st_r13
global st_r14
global st_r15

global st_cs
global st_ds
global st_es
global st_fs
global st_gs
global st_ss

stackptr dq 0x90000
stackbas dq 0x90000

; 1 per CPU
global tasknos
tasknos times 4 dd 0

stackinc dd 0

st_loadedBefore db 0
st_eax dq 0
st_ebx dq 0
st_ecx dq 0
st_edx dq 0
st_eip dq 0x8000        ;start the program at 0x8000 the first time
st_ebp dq 0
st_esp dq 0x80000       ;start the stack at 0x80000 (64KB below the kernel) the first time
st_edi dq 0
st_esi dq 0
st_r8 dq 0
st_r9 dq 0
st_r10 dq 0
st_r11 dq 0
st_r12 dq 0
st_r13 dq 0
st_r14 dq 0
st_r15 dq 0

st_cs dw 0
st_ds dw 0
st_es dw 0
st_fs dw 0
st_gs dw 0
st_ss dw 0

st_eflags dq 0
st_backupst dq 0

retloc dq 0

global saveSSERegisters
global loadSSERegisters

extern unlock_lock__0002

align 16
spot dq 0

saveSSERegisters:
    mov [spot], rdi
    fxsave [spot]
    ret

loadSSERegisters:
    mov [spot], rdi
    fxrstor [spot]
    ret


extern getCPUNumber

;only designed to load the FIRST task of the whole thing
;this should only be called ONCE, and ONLY ONCE
exerun:    
	mov [taskno], dword 1

    mov [stackptr], rsp
    mov [stackbas], rbp

    mov rbp, [st_ebp]

    mov rax, [st_eax]
    mov rbx, [st_ebx]
    mov rcx, [st_ecx]
    mov rdx, [st_edx]
    mov rsi, [st_esi]
    mov rdi, [st_edi]
    
    mov r8, [st_r8]
    mov r9, [st_r9]
    mov r10, [st_r10]
    mov r11, [st_r11]
    mov r12, [st_r12]
    mov r13, [st_r13]
    mov r14, [st_r14]
    mov r15, [st_r15]
    
    mov ax, 0x23        ;0x23
    mov ds, ax
    mov es, ax
    mov fs, ax
    mov gs, ax
    
	;loading 64 bit values directly (e.g. mov [st_eip], qword 0x10000000) are a no-no.
	mov eax, 0x10000000
	mov [st_eip], rax 
	mov eax, 0x202
	mov [st_eflags], rax

    mov rax, [st_eax]
	
    push qword 0x23        ;SS 23
    push qword [st_esp]          ;RSP
    push qword [st_eflags]    ;RFLAGS
    push qword 0x1b        ;CS 1b
    push qword [st_eip]    ;RIP

	mov byte [flagForCPUTimingSetByExecutionAssemblyStub], 1
	mov [taskModeActive], byte 1

	iretq

global tss_flush
tss_flush:
   mov ax, 0x2B
   ltr ax
   ret

global _isr0
global _isr1
global _isr2
global _isr3
global _isr4
global _isr5
global _isr6
global _isr7
global _isr8
global _isr9
global _isr10
global _isr11
global _isr12
global _isr13
global _isr14
global _isr15
global _isr16
global _isr17
global _isr18
global isr96

extern fault_handler
global timer_int
extern toticks
    
extern storeData
extern loadTask
extern tssInit

align 16
__vv dd 0

acquireLock__vv:
    lock bts dword [__vv], 0        ;Attempt to acquire the lock (in case lock is uncontended)
    jc .spin_wait            ;Spin if locked ( organize code such that conditional jumps are typically not taken ) 
    ret                      ;Lock obtained
.spin_wait:
    test dword [__vv],1      ;Is the lock free?
    jnz .spin_wait           ;no, wait
    jmp acquireLock__vv          ;maybe, retry ( could also repeat bts, jc, ret instructions here instead of jmp )

tasknoTEMP_ dd 0 

global switchTask
;refine this so it will:
;	- only switch when task mode is active
;	- in an interrupt handler, ONLY when it yields
;	- nowhere else. this will always be called by an int. handler of some sort (including syscalls)
switchTask:
	sti
	cmp [taskno], dword 1
	je skipRET				;if we are not in the position to load a task, simply iret (which is all the rest of the function does, just in a complex way)
.doret:
	ret

skipRET:
	;store context
	cli
	push qword [taskno]
	mov [taskno], dword 0
	push rax
    push rcx
    push rdx
    push rbx
    ;push rsp
    push rbp
	push rsi
    push rdi
    push r8
    push r9
    push r10
    push r11
    push r12
    push r13
    push r14
    push r15
	pushfq		;mightn't be that helpful because the IRET will restore the userland EFLAGS, but it might be helpful to restore the kernel-land EFLAGS
	mov [st_esp], rsp			;we still have to save the stack
	call storeDataFromASM
	call findNextTask
	call loadTaskFromASM
;---------------------------------------------------;
;				STATE HAS SWITCHED					;
;---------------------------------------------------;
global stateSwitchJumpPoint
stateSwitchJumpPoint:				;used to start loading tasks after the login task has finished
	cli
	;return to the kernel stack we had before the switch
	mov rsp, [st_esp]
	;restore context
	pop rax			;pops flags
	or rax, 0x0200	;enables interrupts
	push rax
	popfq			;sets interrupts to on
	pop r15
    pop r14
    pop r13
    pop r12
    pop r11
    pop r10
    pop r9
    pop r8
    pop rdi
	pop rsi
	pop rbp
    ;pop rsp
    pop rbx
    pop rdx
    pop rcx
    pop rax

	sti

	cmp [st_loadedBefore], byte 0
	jne .loadedBefore

	push ax
	mov ax, 0x23        ;0x23f
    mov ds, ax
    mov es, ax
    mov fs, ax
    mov gs, ax
	pop ax

	;st_eip and st_eflags should already be set

	;set up the IRET
    push qword 0x23        ;SS 23
    push qword [st_esp]          ;RSP
    push qword [st_eflags]    ;RFLAGS
    push qword 0x1b        ;CS 1b
    push qword [st_eip]    ;RIP

	mov byte [flagForCPUTimingSetByExecutionAssemblyStub], 1
	mov [taskno], dword 1
	iretq		;returns interrupts

.loadedBefore:	
	pop qword [taskno]
	ret						;returns to before switchTask was called

extern printEIP
raxStore dq 0
rbxStore dq 0

extern loadTaskFromASM
extern storeDataFromASM
extern findNextTask

extern taskModeActive
global yieldFromKernel

yieldFromKernel:
	cmp [taskModeActive], byte 0
	je .noyield
	call skipRET			;skips the taskno check because the kernel SHOULD KNOW when is a good time to switch tasks without needing taskno
.noyield:
	ret


global irq0
global irq1
global irq2
global irq3
global irq4
global irq5
global irq6
global irq7
global irq8
global irq9
global irq10
global irq11
global irq12
global irq13
global irq14
global irq15
global irq16
global irq17
global irq18
global irq19
global irq20
global irq21
global irq22
global irq23


irq0:
    cli
    push qword 0
    push qword 32
    jmp irq_common_stub

irq1:
    cli
    push qword 0
    push qword 33
    jmp irq_common_stub

irq2:
    cli
    push qword 0
    push qword 34
    jmp irq_common_stub

irq3:
    cli
    push qword 0
    push qword 35
    jmp irq_common_stub

irq4:
    cli
    push qword 0
    push qword 36
    jmp irq_common_stub

irq5:
    cli
    push qword 0
    push qword 37
    jmp irq_common_stub

irq6:
    cli
    push qword 0
    push qword 38
    jmp irq_common_stub

irq7:
    cli
    push qword 0
    push qword 39
    jmp irq_common_stub

irq8:
    cli
    push qword 0
    push qword 40
    jmp irq_common_stub

irq9:
    cli
    push qword 0
    push qword 41
    jmp irq_common_stub

irq10:
    cli
    push qword 0
    push qword 42
    jmp irq_common_stub

irq11:
    cli
    push qword 0
    push qword 43
    jmp irq_common_stub

irq12:
    cli
    push qword 0
    push qword 44
    jmp irq_common_stub

irq13:
    cli
    push qword 0
    push qword 45
    jmp irq_common_stub

irq14:
    cli
    push qword 0
    push qword 46
    jmp irq_common_stub

irq15:
    cli
    push qword 0
    push qword 47
    jmp irq_common_stub

irq16:
    cli
    push qword 0
    push qword 48
    jmp irq_common_stub

irq17:
    cli
    push qword 0
    push qword 49
    jmp irq_common_stub

irq18:
    cli
    push qword 0
    push qword 50
    jmp irq_common_stub

irq19:
    cli
    push qword 0
    push qword 51
    jmp irq_common_stub

irq20:
    cli
    push qword 0
    push qword 52
    jmp irq_common_stub

irq21:
    cli
    push qword 0
    push qword 53
    jmp irq_common_stub

irq22:
    cli
    push qword 0
    push qword 54
    jmp irq_common_stub

irq23:
    cli
    push qword 0
    push qword 55
    jmp irq_common_stub
global idt_load
extern idtp

align 16
splxx3 dd 0

idt_load:
	mov [0x7D08], dword idtp
    lidt [idtp]
    ret

_isr_common_stub:
	sti;;ick!
    push rax
    push rcx
    push rdx
    push rbx
    push rsp
    push rbp
    push rsi
    push rdi
    push r8
    push r9
    push r10
    push r11
    push r12
    push r13
    push r14
    push r15

    push rax
    push rax
    
    cld

    mov rdi, rsp				;give it the pointer to the address before taskno, so that the C structs don't get mucked up
    
	;save taskno, then set to zero so the handler isn't pre-empted (handlers are cooperative)
	mov eax, [taskno]
	push rax
	mov [taskno], dword 0

	;call C handler
    mov rax, fault_handler
    call rax

	;restore taskno
	pop rax
	mov [taskno], eax

	pop rax
    pop rax

    pop r15
    pop r14
    pop r13
    pop r12
    pop r11
    pop r10
    pop r9
    pop r8
    pop rdi
	pop RSI
	pop rbp
    pop rsp
    pop rbx
    pop rdx
    pop rcx
    pop rax

    add rsp, 16     ; Cleans up the pushed error code and pushed ISR number
	call switchTask			;this MUST be called, not jumped to. Only switches if taskno == 1
	iretq


extern irq_handler
irq_common_stub:
	sti;;ick!
    push rax
    push rcx
    push rdx
    push rbx
    push rsp
    push rbp
    push rsi
    push rdi
    push r8
    push r9
    push r10
    push r11
    push r12
    push r13
    push r14
    push r15

    push rax
    push rax
    
    cld 
    
    mov rdi, rsp				;give it the pointer to the address before taskno, so that the C structs don't get mucked up
	
	;save taskno, then set to zero so the handler isn't pre-empted (handlers are cooperative)
	mov eax, [taskno]
	push rax
	mov [taskno], dword 0

	;call C handler
    mov rax, irq_handler
    call rax

	;restore taskno
	pop rax
	mov [taskno], eax

	pop rax
    pop rax

    pop r15
    pop r14
    pop r13
    pop r12
    pop r11
    pop r10
    pop r9
    pop r8
    pop rdi
	pop RSI
	pop rbp
    pop rsp
    pop rbx
    pop rdx
    pop rcx
    pop rax

	call switchTask				;pre-emptive multitasking!

    add rsp, 16     ; Cleans up the pushed error code and pushed ISR number
	iretq

global _isr0
_isr0:
    cli
    push qword 0
    push qword 0
    jmp _isr_common_stub

_isr1:
    cli
    push qword 0
    push qword 1
    jmp _isr_common_stub

_isr2:
    cli
    push qword 0
    push qword 2
    jmp _isr_common_stub

_isr3:
    cli
    push qword 0
    push qword 3
    jmp _isr_common_stub

_isr4:
    cli
    push qword 0
    push qword 4
    jmp _isr_common_stub

_isr5:
    cli
    push qword 0
    push qword 5
    jmp _isr_common_stub

_isr6:
    cli
    push qword 0
    push qword 6
    jmp _isr_common_stub

_isr7:
    cli
    push qword 0
    push qword 7
    jmp _isr_common_stub

_isr8:
    cli
    push 8
    jmp _isr_common_stub

_isr9:
    cli
    push qword 0
    push qword 9
    jmp _isr_common_stub

_isr10:
    cli
    push 10
    jmp _isr_common_stub

_isr11:
    cli
    push 11
    jmp _isr_common_stub

_isr12:
    cli
    push 12
    jmp _isr_common_stub

_isr13:
    cli
    push 13
    jmp _isr_common_stub

_isr14:
    cli
    push 14
    jmp _isr_common_stub

_isr15:
    cli
    push qword 0
    push qword 15
    jmp _isr_common_stub

_isr16:
    cli
    push qword 0
    push qword 16
    jmp _isr_common_stub

_isr17:
    cli
    push qword 0
    push qword 17
    jmp _isr_common_stub

_isr18:
    cli
    push qword 0
    push qword 18
    jmp _isr_common_stub

extern syscallreturn

align 16
__v1 dd 0

acquireLock__v1:
    lock bts dword [__v1], 0        ;Attempt to acquire the lock (in case lock is uncontended)
    jc .spin_wait            ;Spin if locked ( organize code such that conditional jumps are typically not taken ) 
    ret                      ;Lock obtained
.spin_wait:
    test dword [__v1],1      ;Is the lock free?
    jnz .spin_wait           ;no, wait
    jmp acquireLock__v1          ;maybe, retry ( could also repeat bts, jc, ret instructions here instead of jmp )

extern getCPUNumber

isr96:
    sti;;ick!

    push qword 0
    push qword 96
   
    push rax
    push rcx
    push rdx
    push rbx
    push rsp
    push rbp      ;set rbp to rsp
    push rsi
    push rdi
    push r8
    push r9
    push r10
    push r11
    push r12
    push r13
    push r14
    push r15
    
    push rax
    push rax
    
    cld
    
    mov rdi, rsp

    ;save taskno, then set to zero so the handler isn't pre-empted (handlers are cooperative)
	mov eax, [taskno]
	push rax
	mov [taskno], dword 0

	;call C handler
	sti
    mov rax, fault_handler
    call rax

	;restore taskno
	pop rax
	mov [taskno], eax

    pop rax
    pop rax
    
    pop r15
    pop r14
    pop r13
    pop r12
    pop r11
    pop r10
    pop r9
    pop r8
    pop rdi
    pop rsi
    pop rbp
    pop rsp
    pop rbx
    pop rdx
    pop rcx
    pop rax
    
    add rsp, 16
	mov eax, [syscallreturn]
	call switchTask		;THIS IS THE COOPERATIVE MULTITASKING LINE! (if you put the equivalent into irq_common_stub, you get pre-emptive)
	iretq
