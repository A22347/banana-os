
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <syscall.h>

int main (int argc, char *argv[])
{
	int a[2];
	pipe(a);

	char buffer[] = "Data, data and more data.";
	__i_system_call_writefileorpipe(a[1], buffer, strlen(buffer));
	while (1) {
		char bf[2];
		bf[1] = 0;
		__i_system_call_readfileorpipe(a[0], bf, 1);
		if (bf[0]) {
			putchar(bf[0]);
		}
	}

	return 0;
}