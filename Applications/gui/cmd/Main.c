﻿#include "D:/Users/Alex/Desktop/banana-os/System/core/syscalldef.h"
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <syscall.h>
#include <stdlib.h>
#include <time.h>

#define SSFN_NOIMPLEMENTATION               /* don't include the normal renderer implementation */
#define SSFN_CONSOLEBITMAP_TRUECOLOR          /* use the special renderer for hicolor packed pixels */

#include "ssfn.h"
#include "font.h"
#include "unifont.h"

#define TASKBAR_HEIGHT 32

int SystemCall(uint16_t callno, uint32_t ebx, uint32_t ecx, void* edx);

int screenWidth;
int screenHeight;
int scanlineWidth;
uint32_t* screenBuffer;

typedef struct TerminalData
{
	volatile char text[25][80];
	volatile uint32_t fg[25][80];
	volatile uint32_t bg[25][80];

	volatile int xPtr;
	volatile int yPtr;

	volatile uint64_t readPipeDescriptor;

	volatile struct TerminalData* next;

	volatile bool root;
	volatile bool needsUpdating;

} TerminalData;

typedef struct Window
{
	struct Window* next;
	int x;
	int y;
	int width;
	int height;

	wchar_t name[256];

	bool console;
	volatile TerminalData volatile* consoleData;

} Window;

Window* root;
Window* focus = 0;

uint32_t desktopColour0 = 0xA0C0FF;// 0x80A0FF;
uint32_t desktopColour1 = 0x4080FF;
uint8_t desktopPattern[8] = { 0xAA, 0x55, 0xAA, 0x55, 0xAA, 0x55, 0xAA, 0x55 };

void memset32(void* addr, uint32_t value, uint32_t numberOf4ByteGroups)
{
	__asm ("cld ; rep stosl" :: "a"(value), "D"(addr), "c"(numberOf4ByteGroups) : "flags", "memory");
}

void drawRect(int x0, int y0, int x1, int y1, uint32_t col)
{
	while (y0 < y1) {
		memset32(screenBuffer + y0 * scanlineWidth + x0, col, x1 - x0);
		++y0;
	}
}

void drawCircle(int r, int x, int y, uint32_t col)
{
	int r2 = r * r;
	int area = r2 << 2;
	int rr = r << 1;

	for (int i = 0; i < area; i++) {
		int tx = (i % rr) - r;
		int ty = (i / rr) - r;

		if (tx * tx + ty * ty <= r2) {
			screenBuffer[scanlineWidth * (y + ty) + x + tx] = col;
		}
	}
}

void drawLine(int x0, int y0, int x1, int y1, uint32_t col)
{
	if (y0 == y1) {
		memset32(screenBuffer + y0 * scanlineWidth + x0, col, x1 - x0);
		return;
	}
	if (x0 == x1) {
		while (y0 < y1) {
			screenBuffer[scanlineWidth * y0 + x0] = col;
			++y0;
		}
		return;
	}
	int dx = abs(x1 - x0), sx = x0 < x1 ? 1 : -1;
	int dy = abs(y1 - y0), sy = y0 < y1 ? 1 : -1;
	int err = (dx > dy ? dx : -dy) / 2, e2;

	while (1) {
		screenBuffer[scanlineWidth * y0 + x0] = col;
		if (x0 == x1 && y0 == y1) break;
		e2 = err;
		if (e2 > -dx) { err -= dy; x0 += sx; }
		if (e2 < dy) { err += dx; y0 += sy; }
	}
}

int screenSize = 0;
void putpixel_(int x, int y, uint32_t col)
{
	int p = y * scanlineWidth + x;
	if (p > screenSize) {
		printf("Pixel out of range. (x = %d, y = %d, col = #%X, total = %d)\n", x, y, col, y * scanlineWidth + x);
		while (1);
		return;
	}
	screenBuffer[p] = col;
}

void drawBasicTextW(int x, int y, wchar_t* s, uint32_t col)
{
	ssfn_dst_ptr = screenBuffer;
	ssfn_dst_pitch = scanlineWidth * 4;
	ssfn_fg = col;
	ssfn_x = x;
	ssfn_y = y;

	for (wchar_t i = 0; s[i]; ++i) {
		ssfn_font = s[i] < 0x100 ? b_font_data : b_unifont_data;
		ssfn_putc(s[i]);
	}
}

void drawBasicText(int x, int y, char* s, uint32_t col, bool unifont)
{
	ssfn_dst_ptr = screenBuffer;
	ssfn_dst_pitch = scanlineWidth * 4;
	ssfn_fg = col;
	ssfn_x = x;
	ssfn_y = y;

	for (wchar_t i = 0; s[i]; ++i) {
		ssfn_font = !unifont ? b_font_data : b_unifont_data;
		ssfn_putc(s[i]);
	}
}

void clear_()
{
	for (int y = 0; y < screenHeight - TASKBAR_HEIGHT; ++y) {
		for (int x = 0; x < screenWidth; ++x) {
			bool on = desktopPattern[y & 7] & (1 << (x & 7));
			putpixel_(x, y, on ? desktopColour1 : desktopColour0);
		}
	}
	for (int x = 0; x < screenWidth; ++x) {
		putpixel_(x, screenHeight - TASKBAR_HEIGHT, 0x808080);
		putpixel_(x, screenHeight - TASKBAR_HEIGHT + 1, 0xFFFFFF);

	}
	for (int y = screenHeight - TASKBAR_HEIGHT + 2; y < screenHeight; ++y) {
		for (int x = 0; x < screenWidth; ++x) {
			putpixel_(x, y, 0xC8C8C8);
		}
	}
	
	/*time_t t = time(NULL);
	struct tm tm = *localtime(&t);

	char buffer[32];
	sprintf(buffer, "%2d:%02d %cM", tm.tm_hour > 12 ? tm.tm_hour - 12 : tm.tm_hour, tm.tm_min, tm.tm_hour > 12 ? 'P' : 'A');
	drawBasicText(screenWidth - 80, screenHeight - 24, buffer, 0, true);*/
}

void open_()
{
	focus = 0;

	screenWidth = SystemCall(SC_GUIEXE_VidVisibleWidth, 0, 0, 0);
	scanlineWidth = SystemCall(SC_GUIEXE_VidScanlineWidth, 0, 0, 0);
	screenHeight = SystemCall(SC_GUIEXE_VidHeight, 0, 0, 0);
	screenSize = scanlineWidth * screenHeight;

	screenBuffer = malloc(scanlineWidth * screenHeight * 4);
	root = 0;
}

void bringWindowToFront(Window* window)
{
	focus = window;

	return;

	//the rest of the function ends up deleting the window...

	Window* current = root;		//ends up being the previous highest window
	Window* want = 0;			//ends up on top
	Window* prev = 0;			//ends up behind 'want'
	while (current->next) {
		if (!want) prev = current;
		current = current->next;
		if (current == window && !want) {
			want = current;
			break;
		}
	}

	if (!current) return;
	if (!prev) return;
	if (!want) return;
	if (current == root) return;
	if (want == root) return;
	if (!current->next) return;
	if (!want->next) return;
	if (want != window) return;			//we have big problems in the above loop if this is executed...

	prev->next = want->next;
	want->next = 0;

	current->next = want;
}

Window* createWindow_()
{
	static int add = 0;
	if (root == 0) {
		root = malloc(sizeof(Window));
		root->next = 0;
	}

	Window* current = root;
	while (current->next) {
		current = current->next;
	}

	current->x = 100 + add % (screenWidth - 250);
	current->y = 100 + add % (screenHeight - 250);
	current->width = 600;
	current->console = false;
	current->height = 250;
	current->next = malloc(sizeof(Window));
	current->next->next = 0;
	memcpy(current->name, L"Window (窓)", sizeof(L"Window (窓)"));
	add += 50;

	bringWindowToFront(current);

	return current;
}

Window* createConsoleWindow_(TerminalData* data)
{
	Window* w = createWindow_();
	w->width = 80 * 8 + 20;
	w->height = 25 * 16 + 35;
	w->console = true;
	w->consoleData = data;
	memcpy(w->name, L"Console Application", sizeof(L"Console Application"));

	return w;
}

void draw_(Window * window)
{
	uint32_t wincol = window == focus ? 0x0000FF : 0x808080;
	uint32_t dstcol = window == focus ? 0x0080FF : 0xA0A0A0;

	double bchg = (dstcol & 0xFF) - (wincol & 0xFF);
	double gchg = ((dstcol >> 8) & 0xFF) - ((wincol >> 8) & 0xFF);
	double rchg = ((dstcol >> 16) & 0xFF) - ((wincol >> 16) & 0xFF);

	int gradWidth = window->width - 8 - strlen(window->name) * 8;
	bchg /= gradWidth;
	gchg /= gradWidth;
	rchg /= gradWidth;

	for (int y = 0; y < window->height; ++y) {
		if (y + window->y >= screenHeight - TASKBAR_HEIGHT) break;
		for (int x = 0; x < window->width; ++x) {
			if (x + window->x >= screenWidth) break;
			uint32_t col = 0xC8C8C8;
			if (x == window->width - 1) col = 0x000000;
			else if (y == 0) col = 0xE0E0E0;
			else if (y == window->height - 1) col = 0x000000;
			else if (x == 0) col = 0xE0E0E0;
			else if (y == window->height - 2) col = 0x808080;
			else if (x == window->width - 2) col = 0x808080;
			else if (x == 1) col = 0xFFFFFF;
			else if (y == 1) col = 0xFFFFFF;
			else if (x == window->width - 3) col = 0xC0C0C0;
			else if (x == 2) col = 0xC0C0C0;
			else if (y < 22) {
				col = wincol;
				if (x > 8 + strlen(window->name) * 8) {
					col |= (uint32_t) (rchg * x) << 16;
					col |= (uint32_t) (gchg * x) << 8;
					col |= (uint32_t) (bchg * x);
				}
			}

			if (window->console && x > 8 && x < 80 * 8 + 8 && y > 26 && y < 25 * 16 + 26) col = 0;
			putpixel_(window->x + x, window->y + y, col);
		}
	}

	drawBasicTextW(window->x + 21, window->y + 4, window->name, window == focus ? 0xFFFFFF : 0xC0C0C0);

	if (window->console) {
		for (int y = 0; y < 25; ++y) {
			for (int x = 0; x < 80; ++x) {
				char chr[2];
				chr[1] = 0;
				chr[0] = window->consoleData->text[y][x];
				//drawRect(window->x + 9 + x * 8, window->y + 27 + y * 16, window->x + 9 + x * 8 + 8, window->y + 27 + y * 16 + 16, window->consoleData->bg[y][x]);
				drawBasicText(window->x + 9 + x * 8, window->y + 27 + y * 16, chr, window->consoleData->fg[y][x], false);
			}
		}
		window->consoleData->needsUpdating = false;
	}
}

void generate_()
{
	clear_();

	if (root == 0) {
		return;
	}

	Window* current = root;
	while (current->next) {
		draw_(current);
		current = current->next;
	}
}

void flush_() {
	SystemCall(SC_GUIEXE_VidFlushBuffer, 0, 0, screenBuffer);
}


void getNewWindows()
{
	TerminalData* newTerminal = (TerminalData*) (size_t) SystemCall(SC_BlackMagic, 55011, 1, 0);
	if (newTerminal) {
		Window* w = createConsoleWindow_(newTerminal);
		generate_();
		flush_();
	}
}

void updateIfNeeded()
{
	Window* current = root;
	while (current && current->next) {
		if (current && current->console && current->consoleData && current->consoleData->needsUpdating) {
			generate_();
			flush_();
			break;
		}
		current = current->next;
	}
}

void loop()
{
	while (1) {
		getNewWindows();
		updateIfNeeded();
	}
}

int main(int argc, char* argv[])
{
	open_();
	clear_();
	flush_();

	remove("C:/DELTEST.TXT");
	rename("C:/guitest.exe", "C:/Banana/2guitest.exe");

	createWindow_();	
	generate_();
	flush_();

	SystemCall(SC_Spawn, 0, 0, "C:/Banana/System/cmd.exe");

	loop();

	return 0;
}