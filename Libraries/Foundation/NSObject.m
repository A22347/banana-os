#import "NSObject.h"
#import <objc.h>
#import <runtime.h>

#define RefCountPtr ((intptr_t *)((intptr_t *)self - 1))

@interface NSObject ()
{
	Class isa;
}
-(id) _unhandledSelector;
@end

@implementation NSObject
- (void)_ARCCompliantRetainRelease {}

+(void) load {
	return;
}

+(void) initialize {

}

-(id) init {
	return self;
}

+(id) new {
	return [[self alloc] init];
}

+(id) alloc {
	return class_createInstance(self, 0);
}

-(void) dealloc {
	object_dispose(self);
}

-(id) retain
{
    __sync_add_and_fetch(RefCountPtr, 1);
	return self;
}

+(id) retain
{
    return self;
}

-(oneway void) release
{
    if (__sync_sub_and_fetch(RefCountPtr, 1) == -1) {
        objc_delete_weak_refs(self);
        [self dealloc];
    }
}

+(oneway void) release
{
    return;
}

-(id) autorelease
{
    //[NSAutoreleasePool addObject:self];
    return self;
}

+(id) autorelease
{
    return self;
}

-(NSUInteger) retainCount
{
    return *RefCountPtr + 1;
}

+(NSUInteger) retainCount
{
    return 0xFFFFFFFF;		//NSUIntegerMax;
}

-(Class) superclass {
	return class_getSuperclass([self class]);
}

-(Class) class {
	return object_getClass(self);
}

+(Class) superclass {
	return class_getSuperclass(self);
}

+(Class) class {
	return self;
}

-(id) self
{
    return self;
}

+(BOOL) instancesRespondToSelector: (SEL) aSelector {
    return class_respondsToSelector(self, aSelector);
}

+(BOOL) conformsToProtocol: (Protocol *) protocol {
	return class_conformsToProtocol(self, aProtocol);
}

-(IMP) methodForSelector: (SEL) aSelector {
	return class_getMethodImplementation([self class], aSelector);
}

-(id) forwardingTargetForSelector: (SEL) aSelector {
	return nil;
}

+(IMP) instanceMethodForSelector: (SEL) aSelector {
	return class_getMethodImplementation(self, aSelector);
}

-(void) doesNotRecognizeSelector: (SEL) aSelector {
	printf("unrecognized selector sent to object!");
	abort();

	//@throw [NSException exceptionWithName:@"UnrecognizedSelectorException" \
	//format:@"+[%@ %s]: unrecognized selector sent to class %p", self, sel_getName(aSelector), self];
}

+(void) doesNotRecognizeSelector: (SEL) aSelector {
	printf("unrecognized selector sent to class!");
	abort();

	//@throw [NSException exceptionWithName:@"UnrecognizedSelectorException" \
	//format:@"+[%@ %s]: unrecognized selector sent to class %p", self, sel_getName(aSelector), self];
}

+(BOOL) isKindOfClass: (Class) aClass {
	for (Class c = object_getClass(self); c; c = class_getSuperclass(c)) {
		if (c == aClass) {
			return YES;
		}
	}
	return NO;
}

-(BOOL) isKindOfClass: (Class) aClass {
	for (Class c = [self class]; c; c = class_getSuperclass(c)) {
		if (c == aClass) {
			return YES;
		}
	}
	return NO;
}

-(BOOL) isMemberOfClass: (Class) aClass {
	return [self class] == aClass;
}

-(BOOL) conformsToProtocol: (Protocol*) aProtocol {
	return class_conformsToProtocol([self class], aProtocol);
}

-(BOOL) respondsToSelector: (SEL) aSelector {
	return class_respondsToSelector([self class], aSelector);
}

-(NXConstStr*) description {
	return @"This is an Objective-C object without a good description. At least the runtime hasn't crashed...";
}

+(NXConstStr*) description {
	return @"This is an Objective-C class without a good description. At least the runtime hasn't crashed...";
}

-(NXConstStr*) debugDescription {
	return [self description];
}

+(NXConstStr*) debugDescription {
	return [self description];
}

+(BOOL) isSubclassOfClass: (Class) aClass {
	for (Class c = self; c; c = class_getSuperclass(c)) {
		if (c == aClass) {
			return YES;
		}
	}
	return NO;
}

-(id) performSelector: (SEL) aSelector
{
    return objc_msgSend(self, aSelector);
}

-(id) performSelector: (SEL) aSelector withObject: (id) object
{
    return objc_msgSend(self, aSelector, object);
}

-(id) performSelector: (SEL) aSelector withObject: (id) object1 withObject: (id) object2
{
    return objc_msgSend(self, aSelector, object1, object2);
}

@end