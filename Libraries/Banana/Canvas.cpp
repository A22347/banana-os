#include <Banana/Canvas.hpp>

/*
struct CanvasObject {
    int id;
    bool circle;
    int points;
    int pointlocations[MAX_POINTS];
    uint32_t outlineCol;
    uint32_t fillCol;
};

typedef int Canvas_ID;

class Canvas  {
    friend class Window;
    
protected:
    Window* masterwindow;
    int x;
    int y;
    int width;
    int height;
    int scrolledx;
    int scrolledy;
    
    int autoresizex;
    int autoresizey;
    
    struct Node {
        CanvasObject* objectPointer;
        Node* next;
    };
    
    Node* root;
    Node* current;
    
    void _internalDrawLine (int x1, int y1, int x2, int y2);
    void _internalDrawCircle (int x1, int y1, int radius);
    void _internalFillArea (int x1, int y1, uint32_t colour);
*/
    
Canvas::Canvas (Window& parent, int _x, int _y, int _width, int _height) {
    masterwindow = &parent;
    masterwindow->addTextFieldChild(this);
    
    if (_width < 0) {
        width = masterwindow->width + width;    //remember 'width' is negative
        autoresizex = _width;
    } else width = _width;
    
    if (_height < 0) {
        height = masterwindow->height + height; //remember 'height' is negative
        autoresizex = _height;
    } else height = _height;
}

Canvas::~Canvas () {
}

void Canvas::selfupdate (Event_t event) {
    if (_width < 0) {
        width = masterwindow->width + width;    //remember 'width' is negative
        autoresizex = _width;
    } else width = _width;
    
    if (_height < 0) {
        height = masterwindow->height + height; //remember 'height' is negative
        autoresizex = _height;
    } else height = _height;
    
    current = root;
    while (current -> next) {
        if (current -> objectPointer -> circle) {
            _internalDrawCircle (current -> objectPointer -> pointlocations[0], \
                                 current -> objectPointer -> pointlocations[1], 
                                 current -> objectPointer -> pointlocations[2]);    //this is the radius
        } else {
            for (int i = 0; i < current -> objectPointer -> points - 2; i += 2) {
                _internalDrawLine (current -> objectPointer -> pointlocations[i],     \
                                   current -> objectPointer -> pointlocations[i + 1], \
                                   current -> objectPointer -> pointlocations[i + 2], \
                                   current -> objectPointer -> pointlocations[i + 3]);
            }
        }
        
        if (current -> objectPointer -> fillCol <= 0xFFFFFF &&\
                current -> objectPointer -> pointlocations[0] + 1 < current -> objectPointer -> pointlocations[2] &&\
                current -> objectPointer -> pointlocations[1] + 1 < current -> objectPointer -> pointlocations[3]   \
        ) {
            _internalFillArea (\
                current -> objectPointer -> pointlocations[0] + 1, \
                current -> objectPointer -> pointlocations[1] + 1, \
                current -> objectPointer -> fillCol);
        }
    
        current = current -> next;
    }
}

void Canvas::repaint() {
    selfupdate (0);
}

void Canvas::setScroll (int sx, int sy);
int Canvas::getScrollX ();
int Canvas::getScrollY ();
    
int Canvas::createLine (int x1, int y1, int x2, int y2, uint32_t fill, uint32_t outline) { 
    
}

int* Canvas::itemCoords (int id);
void Canvas::itemMove (int id, int x, int y);
void Canvas::itemConfigure (int id, int noOfConfigs ...);
