#include <hw/ps2mouse.h>
#include <hw/ps2.h>
#include <gui/guilink.h>
#include <hw/ports.h>
#include <hal/interrupts.h>
#include <debug/log.h>
#include <hal/mouse.h>

uint8_t mouseMode;					//0 = normal, 3 = with scrollwheel, 4 = with scrollwheel + 4th and 5th button
unsigned char cycle = 0;
uint8_t mouse_bytes[4] = { 0 };
bool mousewasdown = 0;
bool mouseisdown = 0;

void ps2_mouse_handler (struct regs *r)
{
	logk("PS2 mouse int.\n");
	uint8_t status = inb (0x64);
	if ((status & 1) == 0) return;	//no data
	if ((status & 0x20) == 0) return;	//keyboard data

	mouse_bytes[cycle++] = inb(0x60);

	if (!(mouse_bytes[0] & 0x8)) {			//if the first mouse packet happens to be out of alignment (e.g. 2nd/3rd packet), reset it here
												//until we get it right (this code will be executed on each packet send until we get it right
		cycle = 0;
		return;
	}

	if ((mouseMode == 0 && cycle >= 3) || ((mouseMode == 3 || mouseMode == 4) && cycle >= 4)) {
		//logk ("TODO: timeSinceLastInteraction (ps2mouse.c)\n");
		//timeSinceLastInteraction = 0;
		cycle = 0;

		if ((mouse_bytes[0] & 0x80) || (mouse_bytes[0] & 0x40)) {
			cycle = 0;
			mouse_bytes[0] = 0;
			mouse_bytes[1] = 0;
			mouse_bytes[2] = 0;
			mouse_bytes[3] = 0;
			mousewasdown = 0;
			mouseisdown = 0;
			return;
		}

		guiReactToMouseStart ();

		guiReactToMouseMove (mouse_bytes[1] - ((mouse_bytes[0] << 4) & 0x100), \
						  0 - (mouse_bytes[2] - ((mouse_bytes[0] << 3) & 0x100)));

		if (mouse_bytes[0] & 0x4) {
			guiReactToMiddleMouseButton ();
		}

		//right click thing comes last
		if (mouse_bytes[0] & 0x1) {
			mouseisdown = 1;
			guiReactToMouseDown ();

			if (!mousewasdown) {
				guiReactToMouseDownFirstTime ();
			} else {
				guiReactToMouseDrag ();
			}

			mousewasdown = 1;

		} else {
			if (mousewasdown) {
				guiReactToMouseUp ();
				mousewasdown = 0;
			}
			mouseisdown = 0;
		}

		if (mouse_bytes[0] & 0x2) {
			guiReactToRightClick ();
		}
		
		if (mouseMode == 3 || mouseMode == 4) {
			uint8_t zMovement = mouse_bytes[3] & 0xF;
			//only one direction may be changed at a time
			//0x0 = nothing
			//0x1 = up			0x2 = right
			//0xF = down		0xE = left

			switch (zMovement) {
			case 1:
				guiReactToScrollWheel (0, -1);
				break;
			case 2:
				guiReactToScrollWheel (1, 0);
				break;
			case 0xE:
				guiReactToScrollWheel (-1, 0);
				break;
			case 0xF:
				guiReactToScrollWheel (0, 1);
				break;
			default:
				break;
			}
		}

		if (mouseMode == 4) {
			bool button4 = mouse_bytes[3] & 0b10000;
			bool button5 = mouse_bytes[3] & 0b100000;
			logk ("button 4: %d\nbutton 5: %d\n", button4, button5);
		}

		guiReactToMouseEnd ();
	}
}

void openPS2Mouse(struct Device* self, int a, int b, void* c)
{

	//set defaults
	writePS2Device(0xF6, true);
	waitForACK();

	//check for scrollwheel by sending "magic" sampling rate values
	writePS2Device(0xF3, true);
	waitForACK();
	writePS2Device(200, true);
	waitForACK();
	writePS2Device(0xF3, true);
	waitForACK();
	writePS2Device(100, true);
	waitForACK();
	writePS2Device(0xF3, true);
	waitForACK();
	writePS2Device(80, true);
	waitForACK();
	writePS2Device(0xF2, true);
	waitForACK();

	mouseMode = getResponseFromController();
	logk("mouseMode = %d\n", mouseMode);

	if (mouseMode == 3) {
		logk("scrollwheel set up!\n");

		//3 means the scrollwheel has been enabled
		//now we can check for the 4th and 5th mouse buttons

		writePS2Device(0xF3, true);
		waitForACK();
		writePS2Device(200, true);
		waitForACK();
		writePS2Device(0xF3, true);
		waitForACK();
		writePS2Device(200, true);
		waitForACK();
		writePS2Device(0xF3, true);
		waitForACK();
		writePS2Device(80, true);
		waitForACK();
		writePS2Device(0xF2, true);
		waitForACK();				//ACK
		mouseMode = getResponseFromController();

		if (mouseMode == 4) {
			logk("4th and 5th buttons set up!\n");

		} else if (mouseMode != 3) {
			logk("strange mouseID value of trying to setup the 4th and 5th buttons %d\n", mouseMode);

			//reset defaults if we got some wonky value
			writePS2Device(0xF6, true);
			waitForACK();

			mouseMode = 0;
		}

	} else if (mouseMode != 0) {

		logk("strange mouseID value of %d after trying to setup the scrollwheel\n", mouseMode);

		//reset defaults if we got some wonky value
		writePS2Device(0xF6, true);
		waitForACK();

		mouseMode = 0;
	}
	logk("...A\n");

	installHardwareInterruptHandler(legacyIRQRemaps[12], ps2_mouse_handler);
	cycle = 0;
	logk("...B\n");

	//enable streaming
	writePS2Device(PS2_DEVICE_ENABLE_SCANNING, true);
	logk("...C\n");

	asm("sti");
}