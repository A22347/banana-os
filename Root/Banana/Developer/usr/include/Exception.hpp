//
//  Exception.hpp
//  Banana API (New)
//
//  Created by Alex Boxall on 22/5/18.
//  Copyright © 2018 Alex Boxall. All rights reserved.
//

#ifndef Exception_hpp
#define Exception_hpp

#include "RequiredImporting.hpp"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>


/*! \file Exception.hpp
 \brief A header file defining the Exception class.
 */


#ifdef __PRETTY_FUNCTION__
    #define CreateException(name, msg, ...) \
    (Exception(name, __LINE__, __FILE__, __PRETTY_FUNCTION__, msg, ##__VA_ARGS__))
#else
    #ifdef __func__
        #define CreateException(name, msg, ...) \
        (Exception(name, __LINE__, __FILE__, __func__, msg, ##__VA_ARGS__))
    #else
        #ifdef __FUNCTION__
            #define CreateException(name, msg, ...) \
            (Exception(name, __LINE__, __FILE__, __FUNCTION__, msg, ##__VA_ARGS__))
        #else
            #define CreateException(name, msg, ...) \
            (new Exception(name, __LINE__, __FILE__, nullptr, msg, ##__VA_ARGS__))
        #endif
    #endif
#endif

class Exception {
protected:
    char* errorMessage;
    char* errorName;
    int line;
    char* file;
    char* function;
    
public:
    Exception (const char* _errorName = nullptr, int line = -1, const char* file = nullptr,
               const char* function = nullptr, const char* _errorMessageFormat = nullptr, ...);
    ~Exception();
    
    char* fullMessage() const;
    
    int getLine() const;
    char* getFile() const;
    char* getFunction() const;
    char* getMessage() const;
    char* getName() const;
};

#endif /* Exception_hpp */
