#ifndef Label_hpp
#define Label_hpp

#include "Core.hpp"
#include "Widget.hpp"
#include "Window.hpp"

class Label : public Widget {
    friend class Window;
    
protected:
    char* text;
    char* labelfont;
    int x;
    int y;
    bool requiresupdating;
    void _write();
    void _clear();
    
public:
    Label (Window& parent, char* text, int _x, int _y, char* font = "SYSTEM");
    ~Label ();
    void selfupdate (Event_t event);
    void setText (char* n);
    char* getText ();
    
};

#endif