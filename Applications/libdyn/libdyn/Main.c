
#include <stdbool.h>
#include <stdint.h>

int dynamicVariable = 0;
int dynamicVariable2;

void internalDynamicCall(int i)
{
	dynamicVariable = i;
	dynamicVariable2 = i + 2;
}

void dynamicTest()
{
	internalDynamicCall(5);
}