#include "Widget.hpp"

Widget::~Widget () {}

bool Widget::autoLooseFocusOnMouseClick ()
{
	return true;
}

void Widget::forceWindowToUpdate ()
{
	masterwindow->botherToUpdate = true;
}

int32_t Widget::getWindowNumber ()
{
	return masterwindow->windowno;
}

Widget::Widget (Window* master)
{
	masterwindow = master;
}