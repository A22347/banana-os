#ifndef __BANANA_HPP__
#define __BANANA_HPP__

#include <Banana/Core.hpp>
#include <Banana/Object.hpp>
#include <Banana/Pair.hpp>
#include <Banana/Widget.hpp>
#include <Banana/Window.hpp>
#include <Banana/Frame.hpp>
#include <Banana/Image.hpp>
#include <Banana/Label.hpp>
#include <Banana/Functions.hpp>
#include <Banana/Button.hpp>
#include <Banana/TextField.hpp>
#include <Banana/Sound.hpp>
#include <Banana/ListBox.hpp>
#include <Banana/File.hpp>
#include <Banana/Canvas.hpp>
#include <Banana/Text.hpp>
#include <Banana/Checkbox.hpp>

#endif