#include <core/gdt.h>
#include <debug/log.h>

//maximum number of entries is 65536 / 8 = 8192
GDTEntry gdt[256];
uint16_t numberOfGDTEntries = 0;
GDTDescriptor gdtDescriptor;

uint16_t gdtNull;
uint16_t gdtCode;
uint16_t gdtData;
uint16_t gdtUserCode;
uint16_t gdtUserData;
uint16_t gdtTSS[4];		//one per CPU
uint16_t gdtCode32;
uint16_t gdtData32;

void setupGDT ()
{
	numberOfGDTEntries = 0;

	gdtNull		= addGDTEntry (0, 0, 0, 0);
	gdtCode		= addGDTEntry (0, PLATFORM_ID == 64 ? 0 : 0xFFFFFF, (PLATFORM_ID == 64 ? GDTFlag64Bit : 0) | GDTAccessPresent				   | GDTAccessExecutable | GDTAccessReadableCode, GDTFlagGranularity | (PLATFORM_ID == 64 ? GDTFlag64Bit : GDTFlag32Bit));
	gdtData		= addGDTEntry (0, PLATFORM_ID == 64 ? 0 : 0xFFFFFF, (PLATFORM_ID == 64 ? GDTFlag64Bit : 0) | GDTAccessPresent				   | GDTAccessWritableData, GDTFlagGranularity | (PLATFORM_ID == 64 ? GDTFlag64Bit : GDTFlag32Bit));
	gdtUserCode = addGDTEntry (0, PLATFORM_ID == 64 ? 0 : 0xFFFFFF, (PLATFORM_ID == 64 ? GDTFlag64Bit : 0) | GDTAccessPresent | GDTAccessRing3 | GDTAccessExecutable | GDTAccessReadableCode, GDTFlagGranularity | (PLATFORM_ID == 64 ? GDTFlag64Bit : GDTFlag32Bit));
	gdtUserData = addGDTEntry (0, PLATFORM_ID == 64 ? 0 : 0xFFFFFF, (PLATFORM_ID == 64 ? GDTFlag64Bit : 0) | GDTAccessPresent | GDTAccessRing3 | GDTAccessWritableData, GDTFlagGranularity | (PLATFORM_ID == 64 ? GDTFlag64Bit : GDTFlag32Bit));

	//one per CPU
	for (int i = 0; i < 4; ++i) {
		gdtTSS[i] = addGDTEntry (0xB000 + i * 0x100, 0x68, 0b10001001, PLATFORM_ID == 64 ? 0 : 0x40);		//a 'special' entry which doesn't set the "always one" bit
		addGDTEntry (0, 0, 0, 0);
	}

#if PLATFORM_ID == 64
	gdtCode32 = addGDTEntry (0, 0xFFFFFF, GDTAccessPresent | GDTAccessExecutable | GDTAccessReadableCode, GDTFlagGranularity | GDTFlag32Bit);
	gdtData32 = addGDTEntry (0, 0xFFFFFF, GDTAccessPresent | GDTAccessWritableData, GDTFlagGranularity | GDTFlag32Bit);
#else
	gdtCode32 = gdtCode;
	gdtData32 = gdtData;
#endif

	flushGDT ();
}

extern void loadGDT ();

void flushGDT ()
{
	logk ("The GDT is at 0x%X. sizeof(GDTEntry) = %d, sizeof(GDTDescriptor) = %d\n", gdt, sizeof(GDTEntry), sizeof (GDTDescriptor));
	gdtDescriptor.size = numberOfGDTEntries * 8 - 1;
	gdtDescriptor.offset = (size_t) (void*) gdt;

	loadGDT ();
}

uint16_t addGDTEntry (size_t base, size_t limit, uint8_t access, uint8_t flags)
{
	logk ("adding GDT entry. numberOfGDTEntries = %d\n", numberOfGDTEntries);
	gdt[numberOfGDTEntries].baseLow = base & 0xFFFFFF;
	gdt[numberOfGDTEntries].baseHigh = (base >> 24) & 0xFF;

	gdt[numberOfGDTEntries].limitLow = limit & 0xFFFF;
	gdt[numberOfGDTEntries].limitHigh = (limit >> 16) & 0xF;

	gdt[numberOfGDTEntries].access = access;
	gdt[numberOfGDTEntries].flags = flags;

	numberOfGDTEntries++;

	//only automatically flush if we have the basic entries (null, code + data)
	if (numberOfGDTEntries > 3) {
		flushGDT ();
	}

	return (numberOfGDTEntries - 1) * 8;
}