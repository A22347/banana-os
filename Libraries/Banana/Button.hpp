#ifndef __BUTTON_HPP__
#define __BUTTON_HPP__

#include "Core.hpp"
#include "Widget.hpp"
#include "Window.hpp"

/*
MODES for Button._write()

0: Normal
1: Pressed In
2: Has Focus
3: Pressed In and Has Focus
*/

/** \class Button
    \brief A user interface element that defines an area on the screen that can be used to trigger actions.

    Buttons are a elements used to perform actions within your application. 
    When a user clicks a button, it calls the method associated with the button. 
*/
class Button : public Widget {
    friend class Window;
    
protected:
    char* text;
	char* buttonfont;
    int x;
    int y;
    int buttonwidth;
    void _write(int mode = 0);
    void _clear();
    int currentmode = 0;
    void (*buttoncommand) ();

public:
    Button (Window& parent, char* text, int _x, int _y, int _width = 20, char* font = "SYSTEM");
    ~Button ();
    void selfupdate (Event_t event);
    void command (void (*n)());
};

#endif