#include <Banana/Checkbox.hpp>
#include <Banana/Core.hpp>
#define CHECK_BIT(var,pos) ((var) & (1<<(pos)))

Checkbox::Checkbox (Window& parent, int _x, int _y, bool _state, char* _text) : Widget (&parent)
{
	masterwindow = &parent;
	masterwindow->addWidgetChild (this);

	state = _state;

	x = _x;
	y = _y;

	label = new Label (*masterwindow, _text, x + 3, y);
}

Checkbox::~Checkbox ()
{
	if (image) {
		delete image;
	}
	delete label;
}

void Checkbox::_write ()
{
	if (image) {
		delete image;
	}
	image = new Image (*masterwindow, state ? (char*) "C:/Banana/Icons/chboxon.bmp" : (char*) "C:/Banana/Icons/chboxoff.bmp", x, y);
	image->display ();
}

void Checkbox::setText (char* _text)
{
	label->setText (_text);
}

char* Checkbox::getText ()
{
	return label->getText ();
}

void Checkbox::selfupdate (Event_t event)
{
	int cx = SystemCall (SC_WindowPositionX, getWindowNumber(), 0, 0);
	int cy = SystemCall (SC_WindowPositionY, getWindowNumber(), 0, 0);

	if (event.type == EventMouseUp) {			// 3 is press, 5 is release
		int xx = event.mouseXPosition - cx;
		int yy = event.mouseYPosition - cy;
		
		if (yy == y && xx == x) {
			state ^= 1;
			_write ();
			forceWindowToUpdate ();
		}

	} else {
		_write ();
	}
}
