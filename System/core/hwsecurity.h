#ifdef __cplusplus
extern "C" {
#endif

#include <stdbool.h>

	void enableHardwareSecuritySystems ();
	extern bool nxAvaliable;

#ifdef __cplusplus
}
#endif