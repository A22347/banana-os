#include <core/spinlock.h>
#include <debug/log.h>
#include <stddef.h>
#include <core/kernel.h>		//for PLATFORM_ID

extern uint32_t taskno;
extern uint8_t taskModeActive;

void initTasklock (tasklock_t volatile* lock)
{
	lock->locked = 0;
}

void lockTasklock (tasklock_t volatile* lock)
{
	spinlock_t sl;
	initSpinlock (&sl);
	lockSpinlock (&sl);
	lock->locked = 1;
	lock->tasknosave = taskno;
	taskno = 0;
	unlockSpinlock (&sl);
}

void unlockTasklock (tasklock_t volatile* lock)
{
	spinlock_t sl;
	initSpinlock (&sl);
	lockSpinlock (&sl);
	if (lock->locked) {
		taskno = lock->tasknosave;
	}
	lock->locked = 0;
	unlockSpinlock (&sl);
}

void initSpinlock (spinlock_t volatile* lock)
{
	lock->locked = 0;
	lock->timer = 0;
	lock->timeoutValue = 0x10000;
}

void yieldFromKernel ();

/*
Why do we have to disable interrupts in spinlocks?

It is because of this:
	Program A locks a spinlock
	Program A gets interrupted
	The interrupt handler tries to lock the same spinlock
	The interrupt handler yields
	Program B executes
	Program A's interrupt handler tries, fails and yields
	Program B executes
	Program A's interrupt handler tries, fails and yields
	Program B executes
	Program A's interrupt handler tries, fails and yields

See the problem? Execution never gets back to the main part of Program A because the interrupt handler is waiting for the spinlock,
but the spinlock can only be unlocked when it gets back to the main part of Program A.
*/

void lockSpinlock (spinlock_t volatile* lock)
{
	while (!__sync_bool_compare_and_swap (&lock->locked, 0, 1)) {
		asm ("pause");
		++lock->timer;
		logk("Forever looping? B\n");

		yieldFromKernel ();	

		if (lock->timeoutValue && lock->timer > lock->timeoutValue) {
			logk ("spinlock at address 0x%X has timed out...\n", (size_t) lock);
		}
	}

	size_t b = 0;
#if PLATFORM_ID == 86
	__asm__ __volatile__ ("pushf \n\t"
						  "pop %%eax\n\t"
						  "movl %%eax, %0\n\t"
						  :"=r"(b)
						  :
						  : "%eax"
	);
#elif PLATFORM_ID == 64
	__asm__ __volatile__ ("pushf \n\t"
						  "pop %%rax\n\t"
						  "movq %%rax, %0\n\t"
						  :"=r"(b)
						  :
						  : "%rax"
	);
#else
#error "Unsupported platform!"
#endif

	lock->oldInt = b & 0x200;		//store interrupt enable flag

	asm ("cli");
}

void unlockSpinlock (spinlock_t volatile* lock)
{
	//__sync_lock_release (&lock->locked);
	lock->timer = 0;
	lock->locked = 0;

	if (lock->oldInt) {
		asm ("sti");
	}
}

bool trySpinlock (spinlock_t volatile* lock)
{
	if (__sync_bool_compare_and_swap (&lock->locked, 0, 1)) {
		return 0;
	}

	return 1;
}