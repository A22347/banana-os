//
//  Colour.hpp
//  Banana API (New)
//
//  Created by Alex Boxall on 20/10/18.
//  Copyright © 2018 Alex Boxall. All rights reserved.
//

#ifndef Colour_hpp
#define Colour_hpp

#include "RequiredImporting.hpp"
#include <stdint.h>

/*! \file Colour.hpp
 \brief A header file defining the Colour class.
 */

/*!
 \class Colour
 \brief The Colour class represents a ARGB colour.
 */

class Colour {
protected:
    uint32_t _argb;
    
public:
    Colour (uint32_t argb);
    Colour (char* name);
    
    void setFromName (char* name);
    
    void argb(uint32_t argb);
    uint32_t argb();
};

#endif /* Colour_hpp */
