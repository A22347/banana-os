#include <semaphore.h>

int sem_destroy (sem_t* sem)
{
	SystemCall (SC_SemaphoreDestroy, *sem, 0, 0);
}

int sem_init (sem_t* sem, int pshared, unsigned value)
{
	int n = SystemCall (SC_SemaphoreInit, pshared, value, (void*) 0);
	if (n) {
		*sem = n;
		return 0;
	}
	return -1;
}

int sem_post (sem_t* sem)
{
	return SystemCall (SC_SemaphorePost, *sem, 0, 0);
}

int sem_trywait (sem_t* sem)
{
	return SystemCall (SC_SemaphoreTryWait, *sem, 0, 0);
}

int sem_wait (sem_t* sem)
{
	int x = SystemCall (SC_SemaphoreWait, *sem, 0, 0);			//will put us on hold if needed
	if (x == 1) {
		yield ();
	}
	return 0;
}