#ifndef __FLOAT_H__
#define __FLOAT_H__

#ifdef __cplusplus
extern "C" {
#endif

#define FLT_ROUNDS 2            //round toward negative infinity
#define FLT_EVAL_METHOD -1      //indeterminable
#define FLT_RADIX 2
#define DECIMAL_DIG 17
#define FLT_MANT_DIG 24
#define FLT_EPSILON 1.19209290E-07F // decimal constant
#define FLT_EPSILON 0X1P-23F // hex constant
#define FLT_DECIMAL_DIG 9
#define FLT_DIG 6
#define FLT_MIN_EXP -125
#define FLT_MIN 1.17549435E-38F // decimal constant
#define FLT_MIN 0X1P-126F // hex constant
#define FLT_TRUE_MIN 1.40129846E-45F // decimal constant
#define FLT_TRUE_MIN 0X1P-149F // hex constant
#define FLT_HAS_SUBNORM -1
#define FLT_MIN_10_EXP -37
#define FLT_MAX_EXP +128
#define FLT_MAX 3.40282347E+38F // decimal constant
#define FLT_MAX 0X1.fffffeP127F // hex constant
#define FLT_MAX_10_EXP +38
#define DBL_MANT_DIG 53
#define DBL_EPSILON 2.2204460492503131E-16 // decimal constant
#define DBL_EPSILON 0X1P-52 // hex constant
#define DBL_DECIMAL_DIG 17
#define DBL_DIG 15
#define DBL_MIN_EXP -1021
#define DBL_MIN 2.2250738585072014E-308 // decimal constant
#define DBL_MIN 0X1P-1022 // hex constant
#define DBL_TRUE_MIN 4.9406564584124654E-324 // decimal constant
#define DBL_TRUE_MIN 0X1P-1074 // hex constant
#define DBL_HAS_SUBNORM -1
#define DBL_MIN_10_EXP -307
#define DBL_MAX_EXP +1024
#define DBL_MAX 1.7976931348623157E+308 // decimal constant
#define DBL_MAX 0X1.fffffffffffffP1023 //

#ifdef __cplusplus
}
#endif

#endif