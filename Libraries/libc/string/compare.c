//
//  compare.c
//  Banana libc
//
//  Created by Alex Boxall on 12/7/18.
//  Copyright © 2018 Alex Boxall. All rights reserved.
//

#include "string.h"
#include "assert.h"
#include "ctype.h"

int memcmp(const void *s1, const void *s2, size_t n) {
    if (n == 0) {
        return 0;
    }
    
    const unsigned char * in1 = (const unsigned char*) s1;
    const unsigned char * in2 = (const unsigned char*) s2;
    
    for (size_t i = 0; i < n; i++, in1++, in2++) {
        if (*in1 < *in2) {
            return -1;
        } else if (*in1 > *in2) {
            return 1;
        }
    }
    return 0;
}

int strcmp(const char *s1, const char *s2) {
    assert(s1 != NULL && s2 != NULL);

    while (*s1 && (*s1 == *s2)) {
        s1++;
        s2++;
    }
    
    return *(const unsigned char*) s1 - *(const unsigned char*) s2;
}

int strcoll(const char *s1, const char *s2) {
    //what this does is:
    // - looks at the current LOCALE
    // - using implementation specific value(s)
    //   it remaps characters (or sets of characters)
    //   in a different order
    // - then it compares
    
    // For example, "ch" in Czech follows "h" and precedes "i",
    // and "dzs" in Hungarian follows "dz" and precedes "g".
    
    // strcoll compares using these remapped values

    return strcmp(s1, s2);
}

int strncmp(const char *s1, const char *s2, size_t n) {
    assert(s1 != NULL && s2 != NULL);
    
    if (n == 0) {
        return 0;
    }
    
    while (--n && *s1 && (*s1 == *s2)) {
        s1++;
        s2++;
    }
    
    return *(const unsigned char*) s1 - *(const unsigned char*) s2;
}

size_t strxfrm(char * restrict s1, const char * restrict s2, size_t n) {
    /* SHORT ANSWER:
       It transforms a string so that strcmp and strcoll would behave the same
     */
    
    /* LONG ANSWER:
     The strxfrm function transforms the string pointed to by s2 and places the resulting
     string into the array pointed to by s1. The transformation is such that if the strcmp
     function is applied to two transformed strings, it returns a value greater than, equal to, or
     less than zero, corresponding to the result of the strcoll function applied to the same
     two original strings. No more than n characters are placed into the resulting array
     pointed to by s1, including the terminating null character. If n is zero, s1 is permitted to
     be a null pointer. If copying takes place between objects that overlap, the behavior is
     undefined.
     
     Returns:
     3 The strxfrm function returns the length of the transformed string (not including the
     terminating null character). If the value returned is n or more, the contents of the array
     pointed to by s1 are indeterminate.
     
     4 EXAMPLE The value of the following expression is the size of the array needed to hold the
     transformation of the string pointed to by s.
     1 + strxfrm(NULL, s, 0)
     */
    
    assert(0);
    return 0;
}

int strcasecmp(const char *s1, const char *s2) {
    assert(s1 != NULL && s2 != NULL);
    
    while (*s1 && (tolower(*s1) == tolower(*s2))) {
        s1++;
        s2++;
    }
    
    return *(const unsigned char*) s1 - *(const unsigned char*) s2;
}

int strncasecmp(const char *s1, const char *s2, size_t n) {
    assert(s1 != NULL && s2 != NULL);
    
    if (n == 0) {
        return 0;
    }
    
    while (--n && *s1 && (tolower(*s1) == tolower(*s2))) {
        s1++;
        s2++;
    }
    
    return *(const unsigned char*) s1 - *(const unsigned char*) s2;
}
