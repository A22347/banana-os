#ifndef __SYS_SELECT_H__
#define __SYS_SELECT_H__

#ifdef __cplusplus
extern "C" {
#endif

#include <time.h>
#include <signal.h>
#define FD_SETSIZE 64		 //Maximum number of file descriptors in an fd_set structure.

	typedef struct fd_set
	{
		unsigned int count;
		int fd[FD_SETSIZE];
	} fd_set;

	void FD_CLR (int, fd_set *);
	int  FD_ISSET (int, fd_set *);
	void FD_SET (int, fd_set *);
	void FD_ZERO (fd_set *);

	int pselect (int, fd_set *restrict, fd_set *restrict, fd_set *restrict, const struct timespec *restrict, const sigset_t *restrict);
	int select (int, fd_set *restrict, fd_set *restrict, fd_set *restrict, struct timeval *restrict);

#ifdef __cplusplus
}
#endif

#endif