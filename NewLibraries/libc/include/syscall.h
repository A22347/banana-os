//
//  syscall.h
//  Banana libc
//
//  Created by Alex Boxall on 12/7/18.
//  Copyright © 2018 Alex Boxall. All rights reserved.
//

#ifndef syscall_h
#define syscall_h

#if defined(__cplusplus)
#define restrict			//fixes some 'fun' errors
extern "C" {
#endif

#include <stddef.h>

void __i_system_call_abort(void);
void __i_system_call_exit(int code);
int __i_system_call_system(const char* string);
char* __i_system_call_getenv(const char* name);
int __i_system_call_unlink(const char* filename);
int __i_system_call_rename(const char* old, const char* new_);
int __i_system_call_openfile(unsigned long long* fdout, const char* filename, unsigned char mode);
int __i_system_call_readfileorpipe(unsigned long long fd, unsigned char* bf, unsigned long long len);
int __i_system_call_writefileorpipe(unsigned long long fd, const unsigned char* bf, unsigned long long len);
int __i_system_call_pipe(unsigned long long* fdRead, unsigned long long* fdWrite);
int __i_system_call_getstream (unsigned long long* output, int type);
int __i_system_call_malloc_location (size_t* base, size_t* size);

#if defined(__cplusplus)
}
#endif

#endif /* syscall_h */
