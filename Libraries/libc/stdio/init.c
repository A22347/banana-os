//
//  init.c
//  Banana libc
//
//  Created by Alex Boxall on 12/7/18.
//  Copyright © 2018 Alex Boxall. All rights reserved.
//

#ifndef _libk

#include "stdio.h"
#include "syscall.h"
#include "stdlib.h"

void __i_stdioInit(void) {
    stdout = malloc(sizeof(FILE));
    stdout->filename = "(INTERNAL_STDOUT)";
    stdout->bufferSizeForThisFile = BUFSIZ;
    stdout->buffer = malloc(stdout->bufferSizeForThisFile);
    stdout->bufferPtr = 0;
    stdout->bufferMode = _IONBF;
    stdout->locked = 0;
    stdout->openAndValid = 1;
    stdout->textStream = 1;
    stdout->wideOriented = 0;
    stdout->filemode = __i_FILE_WRITE;
    stdout->lastOperation = __i_LASTOPERATION_NULL;
    stdout->errorIndicator = 0;
    stdout->seekable = 0;
	stdout->objectID = 2;
    __i_system_call_getstream (&stdout->objectID, 0);

    stderr = malloc(sizeof(FILE));
    stderr->filename = "(INTERNAL_STDERR)";
    stderr->bufferSizeForThisFile = BUFSIZ;
    stderr->buffer = malloc(stderr->bufferSizeForThisFile);
    stderr->bufferPtr = 0;
    stderr->bufferMode = _IONBF;
    stderr->locked = 0;
    stderr->objectID = 0;   //SystemCall(SC_GetStderrObjectForThisProcess, 0, 0, 0);
    stderr->openAndValid = 1;
    stderr->textStream = 1;
    stderr->wideOriented = 0;
    stderr->filemode = __i_FILE_WRITE;
    stderr->lastOperation = __i_LASTOPERATION_NULL;
    stderr->errorIndicator = 0;
    stderr->seekable = 0;
    __i_system_call_getstream (&stderr->objectID, 1);

    stdin = malloc(sizeof(FILE));
    stdin->filename = "(INTERNAL_STDIN)";
    stdin->bufferSizeForThisFile = BUFSIZ;
    stdin->buffer = malloc(stdin->bufferSizeForThisFile);
    stdin->bufferPtr = 0;
    stdin->bufferMode = _IONBF;
    stdin->locked = 0;
    stdin->objectID = 0;   //SystemCall(SC_GetStdinObjectForThisProcess, 0, 0, 0);
    stdin->openAndValid = 1;
    stdin->textStream = 1;
    stdin->wideOriented = 0;
    stdin->filemode = __i_FILE_READ;
    stdin->lastOperation = __i_LASTOPERATION_NULL;
    stdin->errorIndicator = 0;
    stdin->seekable = 0;
	stdin->objectID = 1;
    __i_system_call_getstream (&stdin->objectID, 2);
}

#endif
