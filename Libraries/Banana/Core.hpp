#ifndef Core_hpp
#define Core_hpp

#include <stdint.h>
#include <stdbool.h>
#include <stdarg.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#define EVENT_MOUSE_MOVE    0b11
#define EVENT_KEY_PRESS     0b101
#define EVENT_MOUSE_CLICK   0b1001
#define EVENT_RIGHT_CLICK   0b10001
#define EVENT_MOUSE_RELEASE 0b100001
#define EVENT_TIMER         0b1000001
#define EVENT_UNUSED	    0b10000001

#define KEYCODE_LEFT  0x03
#define KEYCODE_RIGHT 0x04
#define KEYCODE_UP    0x05
#define KEYCODE_DOWN  0x06

#define KEYCODE_BACKSPACE 0x08	// '\b'
#define KEYCODE_TAB		  0x09	// '\t'
#define KEYCODE_NEWLINE   0x0A	// '\n'

#define KEYCODE_F1  0x0E
#define KEYCODE_F2  0x0F
#define KEYCODE_F3  0x10
#define KEYCODE_F4  0x11
#define KEYCODE_F5  0x12
#define KEYCODE_F6  0x13
#define KEYCODE_F7  0x14
#define KEYCODE_F8  0x15
#define KEYCODE_F9  0x16
#define KEYCODE_F10 0x17
#define KEYCODE_F11 0x18
#define KEYCODE_F12 0x19

#define KEYCODE_CTRL  0x1D
#define KEYCODE_ALT   0x1E
#define KEYCODE_SHIFT 0x1F

extern void* malloc(size_t size);
extern void free(void* pointer);

enum TextStyles {
    textNORMAL,
    textBOLD,
    textITALIC
};

enum MouseTypes
{
	Mouse_Normal,
	Mouse_Text,
	Mouse_HourGlass,
	Mouse_Hand,
	Mouse_ResizeBottomRight
};

int APISystemCall(uint16_t callno, uint32_t ebx, uint32_t ecx, void* edx);
void OSPanic(char* error = "MANUAL_PANIC");
void OSWaitCursor ();
void OSTextCursor ();
void OSArrowCursor ();

#include <driverkit.h>

extern "C" bool executeDriverFromName (char* driver, regs_t reg, uint8_t mode, void (*callback)(regs_t*));

class ApplicationSuperClass
{
	/*virtual void init () = 0;
	virtual void main () = 0;
	virtual void exit () = 0;*/
};

extern ApplicationSuperClass* thisApplication;

#endif /* Core_hpp */
