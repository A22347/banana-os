//
//  libinit.c
//  Banana libc
//
//  Created by Alex Boxall on 12/7/18.
//  Copyright © 2018 Alex Boxall. All rights reserved.
//

#include "errno.h"
#include "signal.h"
#include "stdio.h"
#include "locale.h"
#include "string.h"
#include "stdlib.h"
#include <stdint.h>
#include <stdbool.h>
int SystemCall (int, int, int, void*);
#include <../System/core/syscalldef.h>
#include "syscall.h"

void __i_signalInit(void);
void __i_stdioInit(void);
void __i_localeInit(void);
void __i_allocInit(void);

int main (int argc, char *argv[]);
extern void _fini ();

extern void exit(int);
int __i_libraryInit () {
	//ensure stdioInit() can get streams by doing this first
	SystemCall (SC_LauchTerminalSession, 0, 0, 0);

    errno = 0;
    
    __i_allocInit();
    __i_stdioInit();
    __i_signalInit();

	//locale crashes it, I've got a feeling malloc() is to fault...
	//so "Hello, World!" is as far as we can really get it at the moment...
    //__i_localeInit();

	int noargs = SystemCall (SC_NumberOfArguments, 0, 0, 0);

	char aa[64][256];
	memset(aa, 0, sizeof(aa));

	char* a[64];

	int i;
	for (i = 0; i < noargs; ++i) {
		char* b = (char*) (size_t) SystemCall(SC_GetArgument, i, 0, aa[i]);
		a[i] = aa[i];
	}

	int retval = main (noargs, a);
	exit(retval);
	return 0;
}
