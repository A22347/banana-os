#pragma once

#include <stdint.h>
#include <stdbool.h>
#include <devss/devices.h>

void openLegacy(struct Device* self, int, int, void*);
bool detectLegacy();